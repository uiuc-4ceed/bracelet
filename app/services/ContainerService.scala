package services
import api.Permission.Permission
import models._
import scala.collection.mutable.ListBuffer
import scala.util.Try

/**
  * Generic collection service.
  *
  */
trait ContainerService {
  /**
    * Get container.
    */
  def get(id: UUID): Option[Container]

  /**
    * Create container
    */
  def insert(container: Container): Option[String]
}
