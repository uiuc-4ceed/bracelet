package services

/**
  * Created by telgama2 on 10/4/17.
  */


import models._
import com.mongodb.casbah.Imports._
import models.FileStatus.FileStatus
import play.api.libs.json.{JsArray, JsObject, JsValue}

trait AnalysisSessionService {

  def get(id: UUID): Option[AnalysisSession]
  //def getContainer(sessionId: UUID, containerId: UUID): Option[AnalysisSession]
  def insert(session: AnalysisSession): Option[String]
  def update(session: AnalysisSession)
}