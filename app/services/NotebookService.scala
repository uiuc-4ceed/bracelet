package services
import models._
import com.mongodb.casbah.Imports._
import models.FileStatus.FileStatus
import play.api.libs.json.{JsArray, JsObject, JsValue}
/**
  * Created by telgama2 on 12/14/17.
  */
trait NotebookService {
  def get(id: UUID): Option[Notebook]
  //def getContainer(sessionId: UUID, containerId: UUID): Option[AnalysisSession]
  def insert(notebook: Notebook): Option[String]
  def remove(id: UUID)
}

