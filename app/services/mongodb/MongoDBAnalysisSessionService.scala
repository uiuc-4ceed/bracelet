package services.mongodb

import api.Permission
import api.Permission.Permission
import com.novus.salat.dao.{ModelCompanion, SalatDAO}
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.WriteConcern
import models._
import com.mongodb.casbah.commons.MongoDBObject
import java.text.SimpleDateFormat
import org.bson.types.ObjectId
import play.api.Logger
import util.Formatters
import scala.collection.mutable.ListBuffer
import scala.util.Try
import services._
import javax.inject.{Singleton, Inject}
import scala.util.Failure
import scala.util.Success
import MongoContext.context
import play.api.Play._
/**
  * Created by telgama2 on 10/10/17.
  */

@Singleton
class MongoDBAnalysisSessionService @Inject() extends AnalysisSessionService {
  /**
    * Get AnalysisSession.
    */
  def get(id: UUID): Option[AnalysisSession] = {
    AnalysisSession.findOneById(new ObjectId(id.stringify))
  }
  def insert(session: AnalysisSession): Option[String] = {
    AnalysisSession.insert(session).map(_.toString)
  }
  /**
    * Return a list of the requested Analysis Sessions
    */
  def list(): List[AnalysisSession] = {
    (for (session <- AnalysisSession.find(MongoDBObject())) yield session).toList
  }

  /**
    * Return a list of the requested Analysis Sessions for a specific user
    */
  def list(userID: UUID): List[AnalysisSession] = {
    (for (session <- AnalysisSession.find(MongoDBObject("userID" -> new ObjectId(userID.stringify)))) yield session).toList
  }

  /**
    * Return a list of the requested Analysis Sessions for a specific user
    */
  def listRunningSessions(userID: UUID): List[AnalysisSession] = {
    (for (session <- AnalysisSession.find(MongoDBObject("userID" -> new ObjectId(userID.stringify)) ++ ("status" -> "Running") )) yield session).toList
  }

  /**
    * Return a list of all running Analysis Sessions
    */
  def listRunningSessions(): List[AnalysisSession] = {
    (for (session <- AnalysisSession.find(MongoDBObject("status" -> "Running") )) yield session).toList
  }

  //def getContainer(sessionId: UUID, containerId: UUID): Option[AnalysisSession]

  /**
    * Updated analysis session.
    */
  def update(session: AnalysisSession) {
    AnalysisSession.save(session)
  }

  /**
    * Return sessions with a specific port
    */
  def findSessionWithPort(port: String): List[AnalysisSession] = {
    (for (session <- AnalysisSession.find(MongoDBObject( ("port" -> port))  ++ ("status" -> "Running") )) yield session).toList
  }
}

object AnalysisSession extends ModelCompanion[AnalysisSession, ObjectId] {
  val dao = current.plugin[MongoSalatPlugin] match {
    case None => throw new RuntimeException("No MongoSalatPlugin");
    case Some(x) => new SalatDAO[AnalysisSession, ObjectId](collection = x.collection("analysissessions")) {}
  }
}

