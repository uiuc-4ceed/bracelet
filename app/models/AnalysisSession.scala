package models

import java.sql.Time
import java.util.Date

/**
  * Created by telgama2 on 10/3/17.
  */
/**
  * Analyze data on Jupiter
  *
  *
  */
case class AnalysisSession(
      id: UUID = UUID.generate,
      sessionName: String,
      userID: UUID,
      userEmail: Option[String],
      url: String,
      containerID: String,
      port: String="",
      startDate: Date= new Date(),
      reservedHours: Int=0,
      reservedMinutes: Int=0,
      status: String = "Running"
      //containerId : Container
)

