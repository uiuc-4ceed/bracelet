package api
import java.io._
import java.net.URL
import java.security.{DigestInputStream, MessageDigest}
import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import api.Permission.Permission
import java.util.zip._

import javax.inject.{Inject, Singleton}
import controllers.{Previewers, Utils}
import jsonutils.JsonUtil
import models.{File, _}
import org.apache.commons.codec.binary.Hex
import org.json.JSONObject
import play.api.Logger
import play.api.Play.{configuration, current}
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.iteratee.Enumerator
import play.api.libs.json._
import play.api.libs.json.Json._
import play.api.mvc.AnyContent
import services._
import _root_.util.{FileUtils, JSONLD, License}
import _root_.util.{JSONLD, License}
import views.html.dataset

import scala.collection.immutable.List
import scala.concurrent.{ExecutionContext, Future}
import scala.collection.mutable.ListBuffer
import org.apache.commons.io.input.CountingInputStream
import services.mongodb.{MongoDBAnalysisSessionService, MongoDBNotebookService}

import scala.sys.process.ProcessLogger

class Notebook {

}

@Singleton
class  Notebooks @Inject()(
                           sessions: MongoDBAnalysisSessionService, notebooks: MongoDBNotebookService) extends ApiController {


  def submit() = PrivateServerAction { implicit request =>
    Logger.debug("------- in Notebook.submit ---------")
    implicit val user = request.user
    user match {
      case Some(clowderUser) => {

        val json = request.body.asFormUrlEncoded//.getOrElse("notebookName", null) //request.body.asFormUrlEncoded("notebookName", null).toString(

        json match {
          case Some(resultMap: Map[String, String]) => {
            Logger.debug("------- json transformed to map ---------")
            if (resultMap.contains("notebookName")) {
              val notebookName = resultMap("notebookName")(0)
              val htmlNotebookContent = resultMap("htmlNotebookContent")(0)
              val notebookContent = resultMap("notebookContent")(0)




              Logger.debug("Name inside=" + notebookName)

              var notebookRecord = Notebook(name = notebookName, userID = clowderUser.id, htmlfileContent = htmlNotebookContent, notebookfileContent = notebookContent);
              notebooks.insert(notebookRecord)

              Ok(toJson("Done submitting Notebook"))
            }
            else {
              BadRequest("notebook name is not defined")
            }
          }
          case None => BadRequest("Could not fulfill notebook sumbission request")
        }
      }
      case None => BadRequest("No user defined")
    }
  }


        //val name = notebookName("notebookName")

        //var json_result = request.body.asJson.get
        //Logger.debug("------- after json_result ---------")
        //Logger.debug(request.body.asJson.get.toString())
        //val stock = json.as["notebookName"]
        //Logger.debug(stock)
        /*
        json_result match {
          case Some(resultMap: Map[String, String]) => {
            Logger.debug("------- json transformed to map ---------")
            if (resultMap.contains("notebookName")) {
              val name = resultMap("notebookName")
              Logger.debug("Name inside=" + name)
              Ok(toJson("Done submitting Notebook"))
            }
            else {
              BadRequest("Could not find container ID in the response.")
            }
          }
          case None => BadRequest("Could not create a container.")
        }
        */
        //val htmlNotebookContent = request.body.asFormUrlEncoded.getOrElse("htmlNotebookContent", null).toString()
        //val notebookContent = request.body.asFormUrlEncoded.getOrElse("notebookContent", null).toString()

        /*
        var sessionList = sessions.list(clowderUser.id);
        val sessionId = sessionList(0).id

        var notebookRecord = Notebook(name = notebookName, userID = clowderUser.id, analysisSessionID = sessionId, htmlfileContent = htmlNotebookContent, notebookfileContent = notebookContent);
        notebooks.insert(notebookRecord)

        Ok(toJson("Done submitting Notebook"))
        */


}
