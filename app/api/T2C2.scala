package api

import java.io.FileInputStream
import java.util.Date
import javax.inject.Inject
import _root_.util.License
import api.Permission._
// import com.wordnik.swagger.annotations.{ApiOperation, Api}
import controllers.routes
import models._
import play.api.Logger
import play.api.libs.json._
import play.api.libs.json.Json.toJson
import play.api.mvc.Action
import services._

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

/**
  * Created by todd_n on 4/21/16.
  */
// @Api(value = "/api/t2c2", description = "Controller for t2c2 routes.")
class T2C2 @Inject() (events : EventService, vocabularies : VocabularyService, vocabularyTermService : VocabularyTermService, datasets : DatasetService, collections: CollectionService)  extends ApiController{



  // @ApiOperation(value = "Bulk delete",
  //   notes = "This bulk deletes",
  //   responseClass = "None", multiValueResponse=true, httpMethod = "DELETE")
  def bulkDelete() = PrivateServerAction (parse.json){ implicit request =>
    implicit val user = request.user
    user match {
      case Some(u) => {
        val collectionIds = (request.body \ "collectionIds").asOpt[List[String]].getOrElse(List.empty[String])
        val datasetIds = (request.body \ "datasetIds").asOpt[List[String]].getOrElse(List.empty[String])
        val fileIds  = (request.body \ "fileIds").asOpt[List[String]].getOrElse(List.empty[String])
        for (colId <- collectionIds){
          collections.get(UUID(colId)) match {
            case Some(col) => {
              if (col.author.id == u.id){
                collections.delete(col.id)
              }

            }
          }
        }
        for (dsId <- datasetIds){
          datasets.get(UUID(dsId)) match {
            case Some(dataset) => {
              if (dataset.author.id == u.id){
                datasets.removeDataset(dataset.id)
              }
            }
          }
        }
        for (fId <- fileIds){
          files.get(UUID(fId)) match {
            case Some(file) => {
              if (file.author.id == u.id){
                files.removeFile(file.id)
              }
            }
          }
        }
        Ok(toJson("done"))
      }
      case None => {
        BadRequest("No user supplied")
      }
    }
  }

  // @ApiOperation(value = "List users in a space",
  //   notes = "This will check for Permission.ViewDataset",
  //   responseClass = "None", multiValueResponse=true, httpMethod = "GET")
  def getUsersOfSpace(spaceId : UUID)  = PrivateServerAction{ implicit request =>

    var usersOfSpace : List[User] = List.empty[User]
    request.user match {
      case Some(user) => {
        spaces.get(spaceId) match {
          case Some(space) => {
            usersOfSpace = spaces.getUsersInSpace(spaceId)
          }
          case None => {
            usersOfSpace = List.empty[User]
          }
        }
      }
      case None =>{
        usersOfSpace = List.empty[User]
      }

    }
    val usersJson = for (u <- usersOfSpace)
      yield userToJSON(u)

    Ok(toJson(usersJson))
  }


  // @ApiOperation(value = "List users in a space",
  //   notes = "This will give the number of users in a space",
  //   responseClass = "None", multiValueResponse=true, httpMethod = "GET")
  def getNumUsersOfSpace(spaceId : UUID)  = PrivateServerAction{ implicit request =>

    var usersOfSpace : List[User] = List.empty[User]
    request.user match {
      case Some(user) => {
        spaces.get(spaceId) match {
          case Some(space) => {
            usersOfSpace = spaces.getUsersInSpace(spaceId)
          }
          case None => {
            usersOfSpace = List.empty[User]
          }
        }
      }
      case None =>{
        usersOfSpace = List.empty[User]
      }

    }
    val usersJson = for (u <- usersOfSpace)
      yield userToJSON(u)

    Ok(toJson(usersJson.size))
  }

  // @ApiOperation(value = "List all datasets the user can view",
  //   notes = "This will check for Permission.ViewDataset",
  //   responseClass = "None", multiValueResponse=true, httpMethod = "GET")
  def listMyDatasets(limit : Int) = PrivateServerAction { implicit request =>
    implicit val user = request.user
    user match {
      case Some(u) => {
        Ok(toJson(datasets.listUser(limit,request.user,true,u)))
      }
      case None => BadRequest
    }
  }

  // @ApiOperation(value = "List all datasets the user can view",
  //   notes = "This will check for Permission.ViewDataset",
  //   responseClass = "None", multiValueResponse=true, httpMethod = "GET")
  def listMySpaces(limit : Int) = PrivateServerAction { implicit request =>
    implicit val user = request.user
    val user_spaces = user match {
      case Some(u) => {
        for (uSpace <- spaces.listUser(limit,request.user,true,u))
          yield spaceToJson(uSpace)
      }
      case None => List.empty
    }
    Ok(toJson(user_spaces))
  }


  // @ApiOperation(value = "List all datasets in a collection", notes = "Returns list of datasets and descriptions.", responseClass = "None", httpMethod = "GET")
  def getDatasetsInCollectionWithColId(collectionId : UUID) = PermissionAction(Permission.ViewCollection, Some(ResourceRef(ResourceRef.collection, collectionId))){implicit request=>
    var datasets_incollection = datasets.listCollection(collectionId.stringify)

    var dataset_name_collectionid = for (dataset <- datasets_incollection)
      yield(Json.obj("id"->dataset.id,"name"->dataset.name,"collection"->collectionId))
    Ok(toJson(dataset_name_collectionid))
  }

  // @ApiOperation(value = "Get all collections with dataset ids",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getAllCollectionsWithDatasetIds() = PermissionAction(Permission.ViewCollection) { implicit request =>
    val all_collections_list = request.user match {
      case Some(usr) => {
        for (collection <- collections.listAllCollections(usr, false, 0))
          yield jsonCollection(collection, request.user)
      }
      case None => List.empty
    }
    Ok(toJson(all_collections_list))
  }

  // @ApiOperation(value = "Get all collections with dataset ids",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getAllCollectionsWithDatasetIdsAndFiles() = PermissionAction(Permission.ViewCollection) { implicit request =>
    val all_collections_list = request.user match {
      case Some(usr) => {
        for (collection <- collections.listAllCollections(usr, false, 0))
          yield jsonCollectionDatasetFiles(collection, request.user)
      }
      case None => List.empty
    }
    Ok(toJson(all_collections_list))
  }

  // @ApiOperation(value = "Get all collections with dataset ids",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getAllCollectionsForTree() = PermissionAction(Permission.ViewCollection) { implicit request =>
    val all_collections_list : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    request.user match {
      case Some(usr) => {
        for (collection <- collections.listAllCollections(usr, false, 0))
          if (collections.hasRoot(collection)){
            val currentJson = jsonCollectionForTree(collection, request.user,false)
            all_collections_list += currentJson
          }

      }
      case None => List.empty
    }
    Ok(toJson(all_collections_list.toList))
  }

  // @ApiOperation(value = "Get all collections with dataset ids",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getAllCollectionsForFullTree() = PermissionAction(Permission.ViewCollection) { implicit request =>
    val all_collections_list : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    request.user match {
      case Some(usr) => {
        for (collection <- collections.listAllCollections(usr, false, 0))
          if (collections.hasRoot(collection)){
            val currentJson = jsonCollectionForTree(collection, request.user,true)
            all_collections_list += currentJson
          }

      }
      case None => List.empty
    }
    Ok(toJson(all_collections_list.toList))
  }

  // @ApiOperation(value = "Get all collections with dataset ids",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getLevelOfTree(currentId : Option[String], currentType : String) = PermissionAction(Permission.ViewCollection) { implicit request =>
    request.user match {
      case Some(usr) => {
        val result = getChildrenOfNode(currentId,currentType, request.user)
        Ok(toJson(result))

      }
      case None => BadRequest("No user supplied")
    }
  }

  // @ApiOperation(value = "Get all collections with dataset ids",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getLevelOfTreeSharedWithMe(currentId : Option[String], currentType : String) = PermissionAction(Permission.ViewCollection) { implicit request =>
    request.user match {
      case Some(usr) => {
        val result = getChildrenOfNodeSharedWithMe(currentId,currentType, request.user)
        Ok(toJson(result))

      }
      case None => BadRequest("No user supplied")
    }
  }

  // @ApiOperation(value = "Get all collections with dataset ids",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getLevelOfTreeSharedWithOthers(currentId : Option[String], currentType : String) = PermissionAction(Permission.ViewCollection) { implicit request =>
    request.user match {
      case Some(usr) => {
        val result = getChildrenOfNodeSharedWithOthers(currentId,currentType, request.user)
        Ok(toJson(result))

      }
      case None => BadRequest("No user supplied")
    }
  }

  def getChildrenOfNodeNotShared(nodeId : Option[String], nodeType : String, user : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    nodeId match{
      case Some(nId) => {
        if (nodeType == "collection"){
          collections.get(UUID(nId)) match  {
            case Some(col) => {
              var col_children = getChildrenOfCollectionNotShared(col,user)
              col_children
            }
            case None => {
              children.toList
            }
          }
        } else if (nodeType == "dataset"){
          datasets.get(UUID(nodeId.get)) match {
            case Some(ds) => {
              if (ds.spaces.isEmpty){
                var ds_children = getChildrenOfDataset(ds,user)
                ds_children
              } else {
                children.toList
              }
            }
            case None => {
              children.toList
            }
          }
        } else {
          children.toList
        }
      }
      case None =>{
        val result = getCurrentLevelOfTreeNotShared(nodeId,nodeType, user)
        result

      }
    }
  }

  def getChildrenOfNodeInSpace(nodeId : Option[String], nodeType : String, user : Option[User], space : ProjectSpace) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    nodeId match{
      case Some(nId) => {
        if (nodeType == "collection"){
          collections.get(UUID(nId)) match  {
            case Some(col) => {
              if (col.spaces.contains(space.id)){
                var col_children = getChildrenOfCollectionInSpace(col,user,space)
                col_children
              } else {
                children.toList
              }

            }
            case None => {
              children.toList
            }
          }
        } else if (nodeType == "dataset"){
          datasets.get(UUID(nodeId.get)) match {
            case Some(ds) => {
              if (ds.spaces.contains(space.id)){
                var ds_children = getChildrenOfDataset(ds,user)
                ds_children
              } else {
                children.toList
              }
            }
            case None => {
              children.toList
            }
          }
        } else {
          children.toList
        }
      }
      case None =>{
        val result = getCurrentLevelOfTreeInSpace(nodeId,nodeType, user,space)
        result

      }
    }
  }

  def getChildrenOfNode(nodeId : Option[String], nodeType : String, user : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    nodeId match{
      case Some(nId) => {
        if (nodeType == "collection"){
          collections.get(UUID(nId)) match  {
            case Some(col) => {
              if ((col.author.id == user.get.id) && (col.trash == false)){
                var col_children = getChildrenOfCollection(col,user)
                col_children
              } else {
                children.toList
              }
            }
            case None => {
              children.toList
            }
          }
        } else if (nodeType == "dataset"){
          datasets.get(UUID(nodeId.get)) match {
            case Some(ds) => {
              if (ds.author.id == user.get.id){
                var ds_children = getChildrenOfDataset(ds,user)
                ds_children
              } else{
                children.toList
              }
            }
            case None => {
              children.toList
            }
          }
        } else {
          children.toList
        }
      }
      case None =>{
        val result = getCurrentLevelOfTree(nodeId,nodeType, user)
        result
      }
    }
  }

  def getChildrenOfNodeSharedWithOthers(nodeId : Option[String], nodeType : String, user : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    nodeId match{
      case Some(nId) => {
        if (nodeType == "collection"){
          collections.get(UUID(nId)) match  {
            case Some(col) => {
              if (col.trash){
                children.toList
              } else {
                if (collectionSharedWithOthers(col,user.get)) {
                  var col_children = getChildrenOfCollectionSharedWithOthers(col, user)
                  col_children
                } else {
                  children.toList
                }
              }
            }
            case None => {
              children.toList
            }
          }
        } else if (nodeType == "dataset"){
          datasets.get(UUID(nodeId.get)) match {
            case Some(ds) => {
              if (ds.trash){
                children.toList
              } else {
                var ds_children = getChildrenOfDatasetSharedWithOthers(ds,user)
                ds_children
              }

            }
            case None => {
              children.toList
            }
          }
        } else {
          children.toList
        }
      }
      case None =>{
        val result = getCurrentLevelOfTreeSharedWithOthers(nodeId,nodeType, user)
        result
      }
    }
  }

  def getChildrenOfNodeSharedWithMe(nodeId : Option[String], nodeType : String, user : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    nodeId match{
      case Some(nId) => {
        if (nodeType == "collection"){
          collections.get(UUID(nId)) match  {
            case Some(col) => {
              if (col.trash){
                children.toList
              } else {
                var col_children = getChildrenOfCollectionSharedWithMe(col,user)
                col_children
              }
            }
            case None => {
              children.toList
            }
          }
        } else if (nodeType == "dataset"){
          datasets.get(UUID(nodeId.get)) match {
            case Some(ds) => {
              if (ds.trash){
                children.toList
              } else {
                var ds_children = getChildrenOfDatasetSharedWithMe(ds,user)
                ds_children
              }
            }
            case None => {
              children.toList
            }
          }
        } else {
          children.toList
        }
      }
      case None =>{
        val result = getCurrentLevelOfTreeSharedWithMe(nodeId,nodeType, user)
        result
      }
    }
  }

  def getCurrentLevelOfTreeSharedWithOthers(currentId : Option[String], currentType : String, user : Option[User]) : List[JsValue] = {
    var level : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    if (currentType == "collection"){
      currentId match {
        //not root level
        case Some(col_id) => {
          collections.get(UUID(col_id)) match {
            case Some(col) => {
              if (col.trash){
                level.toList
              } else {
                val children = getChildrenOfCollectionSharedWithOthers( col,user)
                val hasChildren = if (children.size > 0){
                  true
                } else {
                  false
                }
                val currentType = if (col.parent_collection_ids.isEmpty){
                  "root"
                } else {
                  "file"
                }
                var data = Json.obj("data"->col.thumbnail_id)
                val current = Json.obj("id"->col.id,"text"->col.name,"type"->"collection","children"->hasChildren,"data"->data)
                level += current
                level.toList
              }
            }
            case None => {
              val current = Json.obj("status"->"error")
              level += current
              level.toList
            }
          }
        }
        //root level
        case None => {
          var allCols = collections.listAllCollections(user.get,false,0)
          for (collection <- (collections.listAllCollections(user.get, false, 0)).filter((c : Collection) =>((c.author.id == user.get.id) && (c.trash == false)))) {
            if (collectionSharedWithOthers(collection,user.get)){
              if (collections.hasRoot(collection) || (!hasParentInSharedSpace(collection))){
                var hasChildren = false
                if (collection.child_collection_ids.size > 0 || collection.datasetCount > 0){
                  hasChildren = true
                }
                var data = Json.obj("thumbnail_id"->collection.thumbnail_id)
                val current = Json.obj("id"->collection.id,"text"->collection.name,"type"->"collection","children"->hasChildren,"data"->data)
                level += current
              }

            }
          }
          val orphanDatasets = getOrphanDatasetsSharedWithOthers(user.get)
          for (orphan <- orphanDatasets){
            var hasChildren = false
            if ((orphan.files.size > 0 )&& (orphan.author.id == user.get.id)){
              hasChildren = true
            }
            var data = Json.obj("thumbnail_id"->orphan.thumbnail_id)
            var currentJson = Json.obj("id"->orphan.id,"text"->orphan.name,"type"->"dataset","children"->hasChildren,"icon"->"glyphicon glyphicon-briefcase","data"->data)
            level += currentJson
          }
          level.toList
        }
      }
    } else if (currentType == "dataset"){
      currentId match {
        case Some(ds_id) => {
          datasets.get(UUID(ds_id)) match {
            case Some(ds) =>{
              if (datasetSharedWithOthers(ds, user.get)){
                val children = getChildrenOfDatasetSharedWithOthers( ds, user)
                val hasChildren = if (children.size > 0){
                  true
                } else {
                  false
                }
                val current = Json.obj("id"->ds.id,"text"->ds.name,"type"->"file","children"->hasChildren,"data"->"test")
                level += current
                level.toList
              } else {
                level.toList
              }
            }
            case None => {
              val current = Json.obj("status"->"error")
              level += current
              level.toList
            }
          }

        }
        case None => {
          val current = Json.obj("status"->"error")
          level += current
          level.toList
        }
      }
    } else {
      val current = Json.obj("status"->"error")
      level += current
      level.toList
    }
  }

  def getCurrentLevelOfTreeSharedWithMe(currentId : Option[String], currentType : String, user : Option[User]) : List[JsValue] = {
    var level : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    if (currentType == "collection"){
      currentId match {
        //not root level
        case Some(col_id) => {
          collections.get(UUID(col_id)) match {
            case Some(col) => {
              if (col.trash){
                level.toList
              } else {
                val children = getChildrenOfCollectionSharedWithMe( col,user)
                val hasChildren = if (children.size > 0){
                  true
                } else {
                  false
                }
                val currentType = if (col.parent_collection_ids.isEmpty){
                  "root"
                } else {
                  "file"
                }
                var data = Json.obj("data"->col.thumbnail_id)
                val current = Json.obj("id"->col.id,"text"->col.name,"type"->"collection","children"->hasChildren,"data"->data)
                level += current
                level.toList
              }
            }
            case None => {
              val current = Json.obj("status"->"error")
              level += current
              level.toList
            }
          }
        }
        //root level
        case None => {
          var allCols = collections.listAllCollections(user.get,false,0).filter((c : Collection) => (c.author.id!= user.get.id))
          for (collection <- allCols) {
            //TODO check that the user is NOT the owner here
            if (collections.hasRoot(collection) && (collection.trash == false)) {
              var hasChildren = false
              if (collection.child_collection_ids.size > 0 || collection.datasetCount > 0){
                hasChildren = true
              }
              var data = Json.obj("thumbnail_id"->collection.thumbnail_id)
              val current = Json.obj("id"->collection.id,"text"->collection.name,"type"->"collection","children"->hasChildren,"data"->data)
              level += current
            }
          }
          val orphanDatasets = getOrphanDatasetsSharedWithMe(user.get)
          for (orphan <- orphanDatasets){
            var hasChildren = false
            if ((orphan.files.size > 0 )&& (orphan.author.id != user.get.id)){
              hasChildren = true
            }
            var data = Json.obj("thumbnail_id"->orphan.thumbnail_id)
            var currentJson = Json.obj("id"->orphan.id,"text"->orphan.name,"type"->"dataset","children"->hasChildren,"icon"->"glyphicon glyphicon-briefcase","data"->data)
            level += currentJson
          }
          //TODO get all datasets shared that are not in a collection!!!
          level.toList
        }
      }
    } else if (currentType == "dataset"){
      currentId match {
        case Some(ds_id) => {
          datasets.get(UUID(ds_id)) match {
            case Some(ds) =>{
              val children = getChildrenOfDatasetSharedWithMe( ds, user)
              val hasChildren = if (children.size > 0){
                true
              } else {
                false
              }
              val current = Json.obj("id"->ds.id,"text"->ds.name,"type"->"file","children"->hasChildren,"data"->"test")
              level += current
              level.toList
            }
            case None =>
              val current = Json.obj("status"->"error")
              level += current
              level.toList
          }

        }
        case None => {
          val current = Json.obj("status"->"error")
          level += current
          level.toList
        }
      }
    } else {
      val current = Json.obj("status"->"error")
      level += current
      level.toList
    }
  }

  def getCurrentLevelOfTree(currentId : Option[String], currentType : String, user : Option[User]) : List[JsValue] = {
    var level : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    if (currentType == "collection"){
      currentId match {
        //not root level
        case Some(col_id) => {
          collections.get(UUID(col_id)) match {
            case Some(col) => {
              if ((col.author.id == user.get.id) && (col.trash == false)){
                val children = getChildrenOfCollection( col,user)
                val hasChildren = if (children.size > 0){
                  true
                } else {
                  false
                }
                val currentType = if (col.parent_collection_ids.isEmpty){
                  "root"
                } else {
                  "file"
                }
                var data = Json.obj("data"->col.thumbnail_id)
                val current = Json.obj("id"->col.id,"text"->col.name,"type"->"collection","children"->hasChildren,"data"->data)
                level += current
                level.toList
              } else {
                val current = Json.obj("status"->"error")
                level += current
                level.toList
              }

            }
            case None => {
              val current = Json.obj("status"->"error")
              level += current
              level.toList
            }
          }
        }
        //root level
        case None => {
          for (collection <- ((collections.listAllCollections(user.get, false, 0))).filter((c : models.Collection)=> ( (c.author.id == user.get.id) && (c.trash == false)))) {
            if (collectionIsRootHasNoParentBelongingToMe(collection,user.get)) {
              val hasChildren = if (collection.child_collection_ids.size > 0 || collection.datasetCount > 0){
                true
              } else {
                false
              }
              var data = Json.obj("thumbnail_id"->collection.thumbnail_id)
              val current = Json.obj("id"->collection.id,"text"->collection.name,"type"->"collection","children"->hasChildren,"data"->data)
              level += current
            }
          }
          //TODO fix this
          val orphanDatasets = getOrphanDatasetsMine(user.get)
          for (orphan <- orphanDatasets){
            var hasChildren = false
            if (orphan.files.size > 0 ){
              hasChildren = true
            }
            var data = Json.obj("thumbnail_id"->orphan.thumbnail_id)
            var currentJson = Json.obj("id"->orphan.id,"text"->orphan.name,"type"->"dataset","children"->hasChildren,"icon"->"glyphicon glyphicon-briefcase","data"->data)
            level += currentJson
          }
          level.toList
        }
      }
    } else if (currentType == "dataset"){
      currentId match {
        case Some(ds_id) => {
          datasets.get(UUID(ds_id)) match {
            case Some(ds) =>{
              if ((ds.author.id == user.get.id) && (ds.trash == false)){
                val children = getChildrenOfDataset( ds, user)
                val hasChildren = if (children.size > 0){
                  true
                } else {
                  false
                }
                val current = Json.obj("id"->ds.id,"text"->ds.name,"type"->"file","children"->hasChildren,"data"->"test")
                level += current
                level.toList
              } else {
                val current = Json.obj("status"->"error")
                level += current
                level.toList
              }
            }
            case None =>
              val current = Json.obj("status"->"error")
              level += current
              level.toList
          }

        }
        case None => {
          val current = Json.obj("status"->"error")
          level += current
          level.toList
        }
      }
    } else {
      val current = Json.obj("status"->"error")
      level += current
      level.toList
    }
  }

  def getCurrentLevelOfTreeInSpace(currentId : Option[String], currentType : String, user : Option[User], space : ProjectSpace) : List[JsValue] = {
    var level : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    if (currentType == "collection"){
      currentId match {
        //not root level
        case Some(col_id) => {
          collections.get(UUID(col_id)) match {
            case Some(col) => {
              if (col.spaces.contains(space.id)){
                val children = getChildrenOfCollectionInSpace( col,user,space)
                val hasChildren = if (children.size > 0){
                  true
                } else {
                  false
                }
                val currentType = if (col.parent_collection_ids.isEmpty){
                  "root"
                } else {
                  "file"
                }
                val current = Json.obj("id"->col.id,"text"->col.name,"type"->"collection","children"->hasChildren)
                level += current
                level.toList
              } else {
                level.toList
              }
            }
            case None => {
              val current = Json.obj("status"->"error")
              level += current
              level.toList
            }
          }
        }
        //root level
        case None => {
          for (collection <- collections.listAllCollections(user.get, false, 0)) {
            if (collections.hasRoot(collection) && collection.root_spaces.contains(space.id)) {
              val current_children = getChildrenOfCollectionInSpace(collection,user,space)
              val hasChildren = if (current_children.size > 0 ){
                true
              } else {
                false
              }
              val current = Json.obj("id"->collection.id,"text"->collection.name,"type"->"collection","children"->hasChildren)
              level += current
            }
          }
          level.toList
        }
      }
    } else if (currentType == "dataset"){
      currentId match {
        case Some(ds_id) => {
          datasets.get(UUID(ds_id)) match {
            case Some(ds) =>{
              if (ds.spaces.contains(space.id)){
                val children = getChildrenOfDataset( ds, user)
                val hasChildren = if (children.size > 0){
                  true
                } else {
                  false
                }
                val current = Json.obj("id"->ds.id,"text"->ds.name,"type"->"file","children"->hasChildren)
                level += current
                level.toList
              } else {
                level.toList
              }
            }
            case None =>
              val current = Json.obj("status"->"error")
              level += current
              level.toList
          }

        }
        case None => {
          val current = Json.obj("status"->"error")
          level += current
          level.toList
        }
      }
    } else {
      val current = Json.obj("status"->"error")
      level += current
      level.toList
    }
  }

  def getCurrentLevelOfTreeNotShared(currentId : Option[String], currentType : String, user : Option[User]) : List[JsValue] = {
    var level : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    if (currentType == "collection"){
      currentId match {
        //not root level
        case Some(col_id) => {
          collections.get(UUID(col_id)) match {
            case Some(col) => {
              val children = getChildrenOfCollectionNotShared( col,user)
              val hasChildren = if (children.size > 0){
                true
              } else {
                false
              }
              val currentType = if (col.parent_collection_ids.isEmpty){
                "root"
              } else {
                "file"
              }
              val current = Json.obj("id"->col.id,"text"->col.name,"type"->"collection","children"->hasChildren)
              level += current
              level.toList
            }
            case None => {
              val current = Json.obj("status"->"error")
              level += current
              level.toList
            }
          }
        }
        //root level
        case None => {
          for (collection <- collections.listAllCollections(user.get, false, 0)) {
            if (collections.hasRoot(collection) && collection.spaces.isEmpty) {
              val hasChildren = if (collection.child_collection_ids.size > 0 ){
                true
              } else {
                false
              }
              val current = Json.obj("id"->collection.id,"text"->collection.name,"type"->"collection","children"->hasChildren)
              level += current
            }
          }
          level.toList
        }
      }
    } else if (currentType == "dataset"){
      currentId match {
        case Some(ds_id) => {
          datasets.get(UUID(ds_id)) match {
            case Some(ds) =>{
              if (ds.spaces.isEmpty){
                val children = getChildrenOfDataset( ds, user)
                val hasChildren = if (children.size > 0){
                  true
                } else {
                  false
                }
                val current = Json.obj("id"->ds.id,"text"->ds.name,"type"->"file","children"->hasChildren)
                level += current
                level.toList
              } else {
                level.toList
              }
            }
            case None =>
              val current = Json.obj("status"->"error")
              level += current
              level.toList
          }

        }
        case None => {
          val current = Json.obj("status"->"error")
          level += current
          level.toList
        }
      }
    } else {
      val current = Json.obj("status"->"error")
      level += current
      level.toList
    }
  }

  def getChildrenOfCollectionSharedWithOthers(collection : Collection, user : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    val datasetsInCollection = datasets.listCollection(collection.id.stringify, user)
    for (ds <- datasetsInCollection){
      if (datasetSharedWithOthers(ds,user.get)){
        var hasChildren = false
        if ((ds.files.size > 0 )&& (ds.spaces.length>0)){
          hasChildren = true
        }
        var data = Json.obj("thumbnail_id"->ds.thumbnail_id)
        var currentJson = Json.obj("id"->ds.id,"text"->ds.name,"type"->"dataset","children"->hasChildren,"icon"->"glyphicon glyphicon-briefcase","data"->data)
        children += currentJson
      }
    }
    for (child_id <- collection.child_collection_ids){
      collections.get(child_id) match {
        case Some(child) => {
          if (collectionSharedWithOthers(child,user.get) && (child.trash == false)){
            var hasChildren = false
            if (child.author.id != user.get.id){
              if (!child.child_collection_ids.isEmpty || child.datasetCount > 0){
                hasChildren = true
              }
            }
            var data = Json.obj("thumbnail_id"->child.thumbnail_id)
            var currentJson = Json.obj("id"->child.id,"text"->child.name,"type"->"collection", "children"->hasChildren,"data"->data)
            children += currentJson
          }
        }
      }
    }
    children.toList
  }

  def getChildrenOfCollectionSharedWithMe(collection : Collection, user : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    val datasetsInCollection = datasets.listCollection(collection.id.stringify, user).filter((d : Dataset) => (d.author != user.get))
    for (ds <- datasetsInCollection){
      var hasChildren = false
      if ((ds.files.size > 0 )&& (ds.author.id != user.get.id) && (ds.trash == false)){
        hasChildren = true
      }
      var data = Json.obj("thumbnail_id"->ds.thumbnail_id)
      var currentJson = Json.obj("id"->ds.id,"text"->ds.name,"type"->"dataset","children"->hasChildren,"icon"->"glyphicon glyphicon-briefcase","data"->data)
      children += currentJson
    }
    for (child_id <- collection.child_collection_ids){
      collections.get(child_id) match {
        case Some(child) => {
          var hasChildren = false
          if ((child.author.id != user.get.id) && (child.trash == false)){
            if (!child.child_collection_ids.isEmpty || child.datasetCount > 0){
              hasChildren = true
              var data = Json.obj("thumbnail_id"->child.thumbnail_id)
              var currentJson = Json.obj("id"->child.id,"text"->child.name,"type"->"collection", "children"->hasChildren,"data"->data)
              children += currentJson
            }
          }
        }
      }
    }
    children.toList
  }

  def getChildrenOfCollection(collection : Collection, user : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    val datasetsInCollection = (datasets.listCollection(collection.id.stringify, user)).filter(( d : models.Dataset) => ((d.author.id == user.get.id) && (d.trash == false)))
    for (ds <- datasetsInCollection){
      val hasChildren = if (ds.files.size > 0){
         true
      } else {
        false
      }
      var data = Json.obj("thumbnail_id"->ds.thumbnail_id)
      var currentJson = Json.obj("id"->ds.id,"text"->ds.name,"type"->"dataset","children"->hasChildren,"icon"->"glyphicon glyphicon-briefcase","data"->data)
      children += currentJson
    }
    for (child_id <- collection.child_collection_ids){
      collections.get(child_id) match {
        case Some(child) => {
          if ((child.author.id == user.get.id) && (child.trash == false)){
            val hasChildren = if (!child.child_collection_ids.isEmpty || child.datasetCount > 0){
              true
            } else {
              false
            }
            var data = Json.obj("thumbnail_id"->child.thumbnail_id)
            var currentJson = Json.obj("id"->child.id,"text"->child.name,"type"->"collection", "children"->hasChildren,"data"->data)
            children += currentJson
          }
        }
      }
    }
    children.toList
  }

  def getChildrenOfCollectionInSpace(collection : Collection, user : Option[User], space : ProjectSpace) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    /*
    val datasetsInCollection = datasets.listCollection(collection.id.stringify, user)
    for (ds <- datasetsInCollection){
      if (ds.spaces.contains(space.id)){
        val hasChildren = if (ds.files.size > 0){
          true
        } else {
          false
        }
        var currentJson = Json.obj("id"->ds.id,"text"->ds.name,"type"->"dataset","children"->hasChildren,"icon"->"glyphicon glyphicon-briefcase")
        children += currentJson}

    }
    */
    for (child_id <- collection.child_collection_ids){
      collections.get(child_id) match {
        case Some(child) => {
          if (child.spaces.contains(space.id)){
            val hasChildren = if (!child.child_collection_ids.isEmpty || child.datasetCount > 0){
              true
            } else {
              false
            }
            var currentJson = Json.obj("id"->child.id,"text"->child.name,"type"->"collection", "children"->hasChildren)
            children += currentJson
          }
        }
      }
    }
    children.toList
  }

  def getChildrenOfCollectionNotShared(collection : Collection, user : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    //val datasetsInCollection = datasets.listCollection(collection.id.stringify, user)
    /*
    for (ds <- datasetsInCollection){
      if (ds.spaces.isEmpty){
        val hasChildren = if (ds.files.size > 0){
          true
        } else {
          false
        }
        var currentJson = Json.obj("id"->ds.id,"text"->ds.name,"type"->"dataset","children"->hasChildren,"icon"->"glyphicon glyphicon-briefcase")
        children += currentJson
      }
    }
    */
    for (child_id <- collection.child_collection_ids){
      collections.get(child_id) match {
        case Some(child) => {
          if (child.spaces.isEmpty){
            val hasChildren = if (!child.child_collection_ids.isEmpty ){
              true
            } else {
              false
            }
            var currentJson = Json.obj("id"->child.id,"text"->child.name,"type"->"collection", "children"->hasChildren)
            children += currentJson
          }
        }
      }
    }
    children.toList
  }

  def getChildrenOfDatasetSharedWithOthers(dataset : Dataset, user  : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    var files_inside = dataset.files
    for (f <- files_inside){
      files.get(f) match {
        case Some(file) => {
          file.thumbnail_id match {
            case Some(thumbnail) =>{
              var data = Json.obj("thumbnail_id"->file.thumbnail_id)
              var currentJson = Json.obj("id"->file.id,"text"->file.filename,"type"->"file","icon" -> "glyphicon glyphicon-file","data"->data)
              children += currentJson
            }
            case None => {
              var data = Json.obj("thumbnail_id"->file.thumbnail_id)
              var currentJson = Json.obj("id"->file.id,"text"->file.filename,"type"->"file","icon" -> "glyphicon glyphicon-file","data"->data)
              children += currentJson
            }
          }

        }
      }
    }
    children.toList
  }

  def getChildrenOfDatasetSharedWithMe(dataset : Dataset, user  : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    var files_inside = dataset.files
    for (f <- files_inside){
      files.get(f) match {
        case Some(file) => {
          file.thumbnail_id match {
            case Some(thumbnail) =>{
              var data = Json.obj("thumbnail_id"->file.thumbnail_id)
              var currentJson = Json.obj("id"->file.id,"text"->file.filename,"type"->"file","icon" -> "glyphicon glyphicon-file","data"->data)
              children += currentJson
            }
            case None => {
              var data = Json.obj("thumbnail_id"->file.thumbnail_id)
              var currentJson = Json.obj("id"->file.id,"text"->file.filename,"type"->"file","icon" -> "glyphicon glyphicon-file","data"->data)
              children += currentJson
            }
          }

        }
      }
    }
    children.toList
  }

  def getChildrenOfDataset(dataset : Dataset, user  : Option[User]) : List[JsValue] = {
    var children : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    var files_inside = dataset.files
    for (f <- files_inside){
      files.get(f) match {
        case Some(file) => {
          file.thumbnail_id match {
            case Some(thumbnail) =>{
              var data = Json.obj("thumbnail_id"->file.thumbnail_id)
              var currentJson = Json.obj("id"->file.id,"text"->file.filename,"type"->"file","icon" -> "glyphicon glyphicon-file","data"->data)
              children += currentJson
            }
            case None => {
              var data = Json.obj("thumbnail_id"->file.thumbnail_id)
              var currentJson = Json.obj("id"->file.id,"text"->file.filename,"type"->"file","icon" -> "glyphicon glyphicon-file","data"->data)
              children += currentJson
            }
          }

        }
      }
    }
    children.toList
  }

  // @ApiOperation(value = "Get all collections",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getAllCollectionsOfUser() = PermissionAction(Permission.ViewCollection) { implicit request =>
    val all_collections_list = request.user match {
      case Some(usr) => {
        for (collection <- collections.listAllCollections(usr, false, 0))
          yield jsonCollection(collection,request.user)
      }
      case None => List.empty
    }
    Ok(toJson(all_collections_list))
  }

  // @ApiOperation(value = "Get vocabulary of last dataset",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getTemplateFromLastDataset(limit : Int ) = PermissionAction(Permission.ViewDataset){ implicit request =>
    request.user match {
      case Some(user) => {
        val last_datasets = datasets.listUser(limit,Some(user),false,user)

        var last_templates : List[Vocabulary] = List.empty
        for (each_dataset <- last_datasets){
          val current_templates = vocabularies.getByDatasetId(each_dataset.id)
          last_templates = last_templates ++ current_templates
        }

        val json_templates : List[JsValue] = for (each <- last_templates)
          yield jsonVocabulary(each)


        Ok(toJson(json_templates))
      }
      case None => BadRequest((toJson("no user")))
    }
  }

  def jsonCollection(collection: Collection, user : Option[User]): JsValue = {
    val datasetsInCollection = datasets.listCollection(collection.id.stringify, user)
    val datasetIds = for (dataset<-datasetsInCollection)
      yield (dataset.name +":"+ dataset.id)
    toJson(Map("id" -> collection.id.toString, "name" -> collection.name, "description" -> collection.description,
      "created" -> collection.created.toString,"author"-> collection.author.email.toString,
      "child_collection_ids"-> collection.child_collection_ids.toString, "parent_collection_ids" -> collection.parent_collection_ids.toString,
      "dataset_ids"->datasetIds.mkString(","),
      "childCollectionsCount" -> collection.childCollectionsCount.toString, "datasetCount"-> collection.datasetCount.toString, "spaces" -> collection.spaces.toString))
  }

  def jsonCollectionDatasetFiles(collection: Collection, user : Option[User]): JsValue = {
    val collectionDatasets = getDatasetsOfCollection(collection,user)
    Json.obj("id" -> collection.id.toString, "name" -> collection.name, "description" -> collection.description,
      "created" -> collection.created.toString,"author"-> collection.author.email.toString,
      "child_collection_ids"-> collection.child_collection_ids.mkString(","), "parent_collection_ids" -> collection.parent_collection_ids.mkString(","),
      "datasets"->collectionDatasets,"type"->"collection",
      "childCollectionsCount" -> collection.childCollectionsCount.toString, "datasetCount"-> collection.datasetCount.toString, "spaces" -> collection.spaces.toString)
  }




  def jsonCollectionForTree(collection : Collection, user : Option[User], fullTree : Boolean) : JsValue = {
    val collectionDatasets = getDatasetsOfCollection(collection,user)
    val child_collection_jsons : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    val nextLevelJsons = getNextLevelCollectionJsons(collection.child_collection_ids,user,fullTree)
    Json.obj("id" -> collection.id.toString, "text" -> collection.name,
      "children"-> nextLevelJsons, "parent_collection_ids" -> collection.parent_collection_ids.mkString(","),
      "datasets"->collectionDatasets,"type"->"collection", "spaces" -> collection.spaces.mkString(","))

  }

  def getNextLevelCollectionJsons(child_ids : List[UUID], user : Option[User], fullTree : Boolean) : List[JsValue] = {
    var next_level : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    for (child_id <- child_ids){
      collections.get(child_id) match {
        case Some(child_col) => {
          if (fullTree){
            val currentJson = getChildCollectionJsonWithNextLevel(child_col,user)
            next_level += currentJson
          } else {
            val currentJson = getChildCollectionJson(child_col,user)
            next_level += currentJson
          }
        }
      }
    }
    next_level.toList
  }

  def getChildCollectionJsonWithNextLevel(child_collection : Collection, user : Option[User]) : JsValue = {
    val collectionDatasets = getDatasetsOfCollection(child_collection,user)
    val nextLevelJsons = getNextLevelCollectionJsons(child_collection.child_collection_ids,user,true)
    Json.obj("id" -> child_collection.id.toString, "name" -> child_collection.name,
      "childCollections"-> nextLevelJsons, "parent_collection_ids" -> child_collection.parent_collection_ids.mkString(","),
      "datasets"->collectionDatasets,"type"->"collection", "spaces" -> child_collection.spaces.mkString(","))
  }


  def getChildCollectionJson(child_collection : Collection, user : Option[User]) : JsValue = {
    val collectionDatasets = getDatasetsOfCollection(child_collection,user)
    //val nextLevelJsons = getNextLevelCollectionJsons(child_collection.child_collection_ids,user,false)
    Json.obj("id" -> child_collection.id.toString, "text" -> child_collection.name, "parent_collection_ids" -> child_collection.parent_collection_ids.mkString(","),
      "datasets"->collectionDatasets,"type"->"collection", "spaces" -> child_collection.spaces.mkString(","))
  }
  def getDatasetsOfCollection(collection : Collection, user : Option[User]) : List[JsValue] = {
    var collectionDatasets : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    val datasetsInCollection = datasets.listCollection(collection.id.stringify, user)
    for (ds <- datasetsInCollection){
      var current = jsonDataset(ds,user)
      collectionDatasets += current
    }
    collectionDatasets.toList
  }

  def jsonDataset(dataset : Dataset, user : Option[User]) : JsValue = {
    val datasetFiles = getFiles(dataset)
    Json.obj("dataset_id"->dataset.id,"dataset_name"-> dataset.name,"dataset_files"->datasetFiles,"type"->"dataset")
  }

  def getFiles(dataset : Dataset) : List[JsValue] = {
    var datasetFiles : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    val dataset_file_ids = dataset.files
    for (dataset_file_id <- dataset_file_ids){
      files.get(dataset_file_id) match {
        case Some(f) => {
          datasetFiles += jsonFile(f)
        }
        case None =>
      }
    }
    datasetFiles.toList
  }

  def jsonFile(file : models.File) : JsValue = {
    Json.obj("fileid"->file.id,"filename"->file.filename,"type"->"file")
  }

  // @ApiOperation(value = "move 'keys' to terms",
  //   notes = "",
  //   responseClass = "None", httpMethod = "PUT")
  def moveKeysToTermsTemplates() = Action {
    val allVocabularies : List[Vocabulary] = vocabularies.listAll()
    for (eachVocab <- allVocabularies){
      moveKeysToTerms(eachVocab)
    }
    Ok("finished")
  }

  def moveKeysToTerms(vocabulary : Vocabulary) = {
      val keys : List[String] = vocabulary.keys
      val author = vocabulary.author
      var termsToAdd : ListBuffer[UUID] = ListBuffer.empty[UUID]
      for (key <- keys) {
        val current_term = VocabularyTerm(author = author,key = key, units = "",default_value = "", description = "")
        vocabularyterms.insert(current_term) match {
          case Some(id) => {
            Logger.info("Vocabulary Term inserted")
            termsToAdd += UUID(id)
          }
          case None => Logger.error("Could not insert vocabulary term")
        }
      }
      //edit vocabulary here
      for (term <- termsToAdd){
        vocabularies.addVocabularyTerm(vocabulary.id,term)
      }

  }

  // @ApiOperation(value = "Find youngest child collection",
  //   notes = "",
  //   responseClass = "None", httpMethod = "POST")
  def findYoungestChild() = PermissionAction(Permission.ViewCollection)(parse.json) { implicit request =>
    var youngestChildId = ""
    val uuid_strings = (request.body \ "ids").asOpt[List[String]].getOrElse(List.empty[String])
    var current_collections : ListBuffer[Collection] = ListBuffer.empty[Collection]
    for (each_uuid_string <- uuid_strings){
        val current : Collection = collections.get(UUID(each_uuid_string)).get
        current_collections += current
      }

    for (each_collection <- current_collections.toList){
      val current_name = each_collection.name
      if (!hasChildInList(each_collection.id,current_collections.toList)){
        youngestChildId = each_collection.id.stringify
      }
    }
    Ok(toJson(youngestChildId))
  }

  def hasParentInSharedSpace(collection : models.Collection) : Boolean = {
    var hasParentInSharedSpace = false
    var colSpaces = collection.spaces
    for (space <- colSpaces){
      if (hasParentInSpace(collection.id,space)){
        if (spaceSharedWithOthers(space)){
          hasParentInSharedSpace = true
        }
      }
    }
    return hasParentInSharedSpace
  }

  def hasParentInSpace(collectionId : UUID, spaceId: UUID) : Boolean = {
    collections.get(collectionId) match {
      case Some(collection) => {
        spaces.get(spaceId) match {
          case Some(space) => {
            for (parentId <- collection.parent_collection_ids) {
              collections.get(parentId) match {
                case Some(parentCollection) => {
                  if (parentCollection.spaces.contains(spaceId) && (parentCollection.trash == false)) {
                    return true
                  }
                }
                case None => return false
              }
            }
          }
          case None =>
        }
      }
      case None =>
    }
    return false
  }

  private def hasChildInList(collectionId : UUID, collectionList : List[Collection]) ={
    val descendants = collections.getAllDescendants(collectionId)
    val inters = descendants.intersect(collectionList)
    if (descendants.intersect(collectionList).isEmpty){
       false
    } else {
      true
    }
  }


  // @ApiOperation(value = "Get key values from last dataset",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getKeysValuesFromLastDataset() = PermissionAction(Permission.ViewDataset) { implicit request =>
    implicit val user = request.user
    val lastDataset : List[Dataset] = datasets.listAccess(1,Set[Permission](Permission.AddResourceToDataset),user,false, false,false)
    val keyValues = getKeyValuePairsFromDataset(lastDataset(0))
    val asMap  = Json.toJson(keyValues)
    //
    Ok(asMap)
  }

  // @ApiOperation(value = "Attach vocabulary to dataset",
  //   notes = "",
  //   responseClass = "None", httpMethod = "PUT")
  def attachVocabToDataset(vocabId : UUID, datasetId : UUID) = PermissionAction(Permission.AddResourceToDataset) {implicit request =>
    request.user match{
      case Some(user) => {
        datasets.get(datasetId) match {
          case Some(dataset) => {
            vocabularies.get(vocabId) match {
              case Some(vocab) => {
                vocabularies.attachToDataset(datasetId, vocabId)
                Ok(toJson("added"))
              }
              case None => Ok(toJson("No vocabulary found with id " + vocabId))
            }
          }
          case None => Ok(toJson("No dataset found with id " + datasetId))
        }
      }
      case None=> Ok(toJson("No user supplied"))
    }
  }

  // @ApiOperation(value = "Detach vocabulary from dataset",
  //   notes = "",
  //   responseClass = "None", httpMethod = "PUT")
  def detachVocabFromDataset(vocabId : UUID, datasetId : UUID) = PermissionAction(Permission.AddResourceToDataset) {implicit request =>
    request.user match{
      case Some(user) => {
        datasets.get(datasetId) match {
          case Some(dataset) => {
            vocabularies.get(vocabId) match {
              case Some(vocab) => {
                //vocabularies.detachFromDataset(datasetId, vocabId)
                Ok(toJson("not implemented"))
              }
              case None => Ok(toJson("No vocabulary found with id " + vocabId))
            }
          }
          case None => Ok(toJson("No dataset found with id " + datasetId))
        }
      }
      case None=> Ok(toJson(("No user supplied")))
    }
  }

  // @ApiOperation(value = "Attach vocabulary to dataset",
  //   notes = "",
  //   responseClass = "None", httpMethod = "PUT")
  def attachVocabToFile(vocabId : UUID, fileId : UUID) = PermissionAction(Permission.EditFile) {implicit request =>
    request.user match{
      case Some(user) => {
        files.get(fileId) match {
          case Some(file) => {
            vocabularies.get(vocabId) match {
              case Some(vocab) => {
                vocabularies.attachToFile(fileId, vocabId)
                Ok(toJson("added"))
              }
              case None => Ok(toJson("No vocabulary found with id " + vocabId))
            }
          }
          case None => Ok(toJson("No file found with id " + fileId))
        }
      }
      case None=> Ok(toJson("No user supplied"))
    }
  }

  // @ApiOperation(value = "Detach vocabulary from dataset",
  //   notes = "",
  //   responseClass = "None", httpMethod = "PUT")
  def detachVocabFromFile(vocabId : UUID, fileId: UUID) = PermissionAction(Permission.EditFile) {implicit request =>
    request.user match{
      case Some(user) => {
        files.get(fileId) match {
          case Some(file) => {
            vocabularies.get(vocabId) match {
              case Some(vocab) => {
                vocabularies.detachFromFile(fileId, vocabId)
                Ok(toJson("not implemented"))
              }
              case None => Ok(toJson("No vocabulary found with id " + vocabId))
            }
          }
          case None => Ok(toJson("No file found with id " + fileId))
        }
      }
      case None=> Ok(toJson(("No user supplied")))
    }
  }




  /**
    * Create new dataset with no file required. However if there are comma separated file IDs passed in, add all of those as existing
    * files. This is to facilitate multi-file-uploader usage for new files, as well as to allow multiple existing files to be
    * added as part of dataset creation.
    *
    * A JSON document is the payload for this endpoint. Required elements are name, description, and space. Optional element is
    * existingfiles, which will be a comma separated String of existing file IDs to be added to the new dataset.
    */
  // @ApiOperation(value = "Create new dataset with no file",
  //   notes = "New dataset requiring zero files based on values of fields in attached JSON. Returns dataset id as JSON object. Requires name, description, and space. Optional list of existing file ids to add.",
  //   responseClass = "None", httpMethod = "POST")
  def createEmptyDataset() = PermissionAction(Permission.CreateDataset)(parse.json) { implicit request =>
    (request.body \ "name").asOpt[String].map { name =>

      val description = (request.body \ "description").asOpt[String].getOrElse("")
      val templateId = (request.body \ "templateId").asOpt[String].getOrElse("")
      val access =
        if(play.Play.application().configuration().getBoolean("verifySpaces")){
          //verifySpaces == true && access set to trial if not specified otherwise
          (request.body \ "access").asOpt[String].getOrElse(DatasetStatus.TRIAL.toString)
        } else {
          (request.body \ "access").asOpt[String].getOrElse(DatasetStatus.DEFAULT.toString)
        }
      var d : Dataset = null
      implicit val user = request.user
      user match {
        case Some(identity) => {
          (request.body \ "space").asOpt[List[String]] match {
            case None | Some(List("default"))=> {
              d = Dataset(name = name, description = description, created = new Date(), author = identity, licenseData = License.fromAppConfig(), status = access)
            }

            case Some(space) => {
              var spaceList: List[UUID] = List.empty
              space.map {
                aSpace => if (spaces.get(UUID(aSpace)).isDefined) {
                  spaceList = UUID(aSpace) :: spaceList

                } else {
                  BadRequest(toJson("Bad space = " + aSpace))
                }
              }
              d = Dataset(name = name, description = description, created = new Date(), author = identity, licenseData = License.fromAppConfig(), spaces = spaceList, status = access)

            }

          }
        }
        case None => InternalServerError("User Not found")
      }
      events.addObjectEvent(request.user, d.id, d.name, "create_dataset")

      datasets.insert(d) match {
        case Some(id) => {
          //In this case, the dataset has been created and inserted. Now notify the space service and check
          //for the presence of existing files.
          Logger.debug("About to call addDataset on spaces service")
          d.spaces.map( spaceId => spaces.get(spaceId)).flatten.map{ s =>
            spaces.addDataset(d.id, s.id)
            events.addSourceEvent(request.user, d.id, d.name, s.id, s.name, "add_dataset_space")
          }
          //Add this dataset to a collection if needed
          (request.body \ "collection").asOpt[List[String]] match {
            case None | Some(List("default"))=>
            case Some(collectionList) => {
              collectionList.map{c => collections.addDataset(UUID(c), d.id)}
            }
          }
          if (templateId != ""){
            vocabularies.get(UUID(templateId)) match{
              case Some(vocab) => {
                vocabularies.addDatasetId(d.id, vocab.id)
              }
              case None => Logger.error("No vocab with id : " + templateId)
            }
          }
          //Below call is not what is needed? That already does what we are doing in the Dataset constructor...
          //Items from space model still missing. New API will be needed to update it most likely.
          (request.body \ "existingfiles").asOpt[String].map { fileString =>
            var idArray = fileString.split(",").map(_.trim())
            for (anId <- idArray) {
              datasets.get(UUID(id)) match {
                case Some(dataset) => {
                  files.get(UUID(anId)) match {
                    case Some(file) => {
                      //attachExistingFileHelper(UUID(id), UUID(anId), dataset, file, request.user)
                      Ok(toJson(Map("status" -> "success")))
                    }
                    case None => {
                      Logger.error("Error getting file" + anId)
                      BadRequest(toJson(s"The given file id $anId is not a valid ObjectId."))
                    }
                  }
                }
                case None => {
                  Logger.error("Error getting dataset" + id)
                  BadRequest(toJson(s"The given dataset id $id is not a valid ObjectId."))
                }
              }
            }
            Ok(toJson(Map("id" -> id)))
          }.getOrElse(Ok(toJson(Map("id" -> id))))
        }
        case None => Ok(toJson(Map("status" -> "error")))
      }
    }.getOrElse(BadRequest(toJson("Missing parameter [name]")))
  }

  // @ApiOperation(value = "Attach vocabulary to dataset",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getVocabByDatasetId(datasetId : UUID)  = PermissionAction(Permission.ViewVocabulary){implicit request=>
    val vocabularies_withdatasetId = vocabularies.getByDatasetId(datasetId)
    val result = for (each<-vocabularies_withdatasetId) yield jsonVocabulary(each)
    Ok(toJson(result))
  }


  def getVocabByFileId(fileId : UUID)  = PermissionAction(Permission.ViewVocabulary){implicit request=>
    val vocabularies_withdatasetId = vocabularies.getByFileId(fileId)
    val result = for (each<-vocabularies_withdatasetId) yield jsonVocabulary(each)
    Ok(toJson(result))
  }

  // @ApiOperation(value = "post folder",
  //   notes = "",
  //   responseClass = "None", httpMethod = "POST")
  def postDragDrop()  = PrivateServerAction (parse.multipartFormData) {implicit request=>
    val user = request.user
    val a = request.body.file("file").getOrElse(None)
    user match {
      case Some(identity) => {
        // store file
        request.body.file("file").map { f =>
          val file = files.save(new FileInputStream(f.ref.file), f.filename, f.contentType, identity, "datasetLevel")

          val uploadedFile = f
          file match {
            case Some(f) => {
              val option_user = users.findByIdentity(identity)
              events.addObjectEvent(option_user, f.id, f.filename, "upload_file")
              val https = controllers.Utils.https(request)
              val retMap = Map("files" ->
                Seq(
                  toJson(
                    Map(
                      "name" -> toJson(f.filename),
                      "size" -> toJson(uploadedFile.ref.file.length()),
                      "deleteUrl" -> toJson(api.routes.Files.removeFile(f.id).absoluteURL(https)),
                      "deleteType" -> toJson("POST")
                    )
                  )
                )
              )
              Ok(toJson(retMap))
            }
            case None => Ok(toJson("something went wrong"))
          }
        }
        Ok(toJson("did not return something "))
      }
      case None => BadRequest(toJson("No user supplied"))
    }

  }

  // @ApiOperation(value = "Attach vocabulary to dataset",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getDatasetWithAttachedVocab(datasetId : UUID) = PermissionAction(Permission.ViewDataset) {implicit request =>
    val datasetVocab = vocabularies.getByDatasetId(datasetId)

    val datasetVocabJson : JsValue = if (!datasetVocab.isEmpty){
      jsonVocabulary(datasetVocab(0))
    } else {
      Json.obj("id"->"none")
    }


    datasets.get(datasetId) match {
      case Some(dataset) =>{
        Ok(Json.obj("id" -> dataset.id.toString, "name" -> dataset.name, "description" -> dataset.description,
          "created" -> dataset.created.toString, "authorId" -> dataset.author.id,"template"->datasetVocabJson))
      }
      case None => BadRequest(toJson("none"))
    }
  }

  private def getKeyValuePairsFromDataset1(dataset : Dataset): Map[String,String] = {
    var key_value_pairs : Map[String,String] = Map.empty[String,String]
    key_value_pairs = key_value_pairs + ("dataset_name" -> dataset.name)
    key_value_pairs = key_value_pairs + ("dataset_id" -> dataset.id.toString())
    var terms : Map[String,String] = Map.empty[String,String]
    val description = dataset.description
    val keyValues = description.split("\n")
    for (pair <- keyValues){
      var currentPair = pair.replace("{","")
      currentPair = currentPair.replace("}","")
      val listPair = currentPair.split(":")
      val key = listPair(0)
      val value = listPair(1)
      //key_value_pairs = key_value_pairs + (key -> value)
      terms = terms + (key -> value)

    }
    //key_value_pairs = key_value_pairs + ("terms"-> terms.toList)
    return key_value_pairs
  }

  private def getKeyValuePairsFromDataset(dataset : Dataset): JsValue = {

    //key_value_pairs = key_value_pairs + ("dataset_name" -> dataset.name)
    //key_value_pairs = key_value_pairs + ("dataset_id" -> dataset.id.toString())
    val description = dataset.description
    val keyValues = description.split("\n")
    var terms_map : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    for (pair <- keyValues){
      var currentPair = pair.replace("{","")
      currentPair = currentPair.replace("}","")
      val listPair = currentPair.split(":")
      val first = listPair(0)
      val second = listPair(1)
      val current_term : JsValue = JsObject(Seq("key"->JsString(first),"default_value"->JsString(second)))
      terms_map += current_term

    }
    val key_value_pairs = Json.obj("dataset_name" -> dataset.name,"dataset_id" -> dataset.id.toString,"terms"->terms_map.toList)
    return key_value_pairs
  }

  // @ApiOperation(value = "Get key values from last dataset",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getKeysValuesFromLastDatasets(limit : Int) = PermissionAction(Permission.AddResourceToDataset) { implicit request =>
    implicit val user = request.user
    var result : ListBuffer[Map[String,String]] = ListBuffer.empty[Map[String,String]]
    val lastDatasets : List[Dataset] = datasets.listUser(limit,user,false,user.get)
    for (each <- lastDatasets){
      try {
        val currentKeyValues = getKeyValuePairsFromDataset1(each)
        result += currentKeyValues
      } catch {
        case e : Exception => Logger.error("could not get key values for " + each.id)
      }
    }
    val asMap  = Json.toJson(result)
    Ok(asMap)
  }

  // @ApiOperation(value = "Get key values from dataset id",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getKeysValuesFromDatasetId( id : UUID) = PermissionAction(Permission.ViewDataset) { implicit request =>
    implicit val user = request.user
    var result : JsValue = null
    datasets.get(id) match {
      case Some(dataset) => {
        try {
          result = getKeyValuePairsFromDataset(dataset)
        } catch {
          case e : Exception => Logger.error("could not get key values for " + id)
        }
      }
      case None => Logger.error("No dataset found for id " + id)
    }


    val asMap  = Json.toJson(result)
    Ok(asMap)
  }

  // @ApiOperation(value = "Get users you can upload on behalf of",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getWhoICanUploadFor() = PrivateServerAction {implicit request =>
    implicit val user = request.user
    user match {
      case Some(u) => {
        var spacesUserAccess = spaces.listAccess(0,Set[Permission](Permission.AddResourceToSpace,Permission.EditSpace),user,
        true,true,false,false).filter( (p : ProjectSpace) => (p.creator != u.id))
        var spacesUploader : ListBuffer[ProjectSpace] = ListBuffer.empty[ProjectSpace]
        var spacesViewer : ListBuffer[ProjectSpace] = ListBuffer.empty[ProjectSpace]
        for ( s <- spacesUserAccess){
          var role = users.getUserRoleInSpace(u.id,s.id)
          role match {
            case Some(r) => {
              if (r.name == "Uploader"){
                spacesUploader += s
              } else if (r.name == "Viewer"){
                spacesViewer += s
              }
            }
            case None =>
          }

        }

        var proxyUsers : ListBuffer[User] = ListBuffer.empty[User]
        for (su <- spacesViewer){
           var u : User = users.get(su.creator).get
          proxyUsers +=u
        }
        //var spacesUploader = spacesUserAccess.filter( (p : ProjectSpace) => ( users.getUserRoleInSpace(u.id,p.id).get.name == "Uploader"))
        //var spacesViewer = spacesUserAccess.filter( (p : ProjectSpace) => ( users.getUserRoleInSpace(u.id,p.id).get.name == "Viewer"))
        Ok("")

      }
      case None => {
        Ok("")
      }
    }
  }

  // @ApiOperation(value = "Get spaces of user ",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getSpacesOfUser(userId : UUID) = PermissionAction(Permission.AddResourceToSpace) {implicit request =>
    implicit val user = request.user
    user match {
      case Some(currentUser) => {
        users.get(userId) match {
          case Some(user) => {
            var userSpaces = spaces.listUser(0,Some(user),true,user)

            var result = userSpaces.map( (f : (ProjectSpace)) => spaceToJson(f))
            Ok(toJson(result))
          }
          case None => BadRequest
        }
      }
      case None => BadRequest
    }
  }

  // @ApiOperation(value = "Get spaces user can edit",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getSpacesUserHasAccessTo(userId : UUID)= PermissionAction(Permission.AddResourceToSpace) {implicit request =>
    implicit val user = request.user
    user match {
      case Some(currentUser) => {
        users.get(userId) match {
          case Some(user) => {
            var userSpaces = spaces.listAccess(0,Set[Permission](Permission.AddResourceToSpace),Some(user),false,true,false,false)

            var result = userSpaces.map( (f : (ProjectSpace)) => spaceToJson(f))
            Ok(toJson(result))
          }
          case None => BadRequest
        }
      }
      case None => BadRequest
    }
  }

  def spaceToJson(space: ProjectSpace) = {
    toJson(Map("id" -> space.id.stringify,
      "name" -> space.name,
      "description" -> space.description,
      "created" -> space.created.toString))
  }

  private def userToJSON(user: User): JsValue = {
    var profile: Map[String, JsValue] = Map.empty
    if(user.profile.isDefined) {
      if(user.profile.get.biography.isDefined) {
        profile  = profile + ("biography" -> Json.toJson(user.profile.get.biography))
      }
      if(user.profile.get.institution.isDefined) {
        profile = profile + ( "institution" -> Json.toJson(user.profile.get.institution))
      }
      if(user.profile.get.orcidID.isDefined) {
        profile = profile + ("orcidId" -> Json.toJson(user.profile.get.orcidID))
      }
      if(user.profile.get.position.isDefined) {
        profile = profile + ("position" -> Json.toJson(user.profile.get.position))
      }
      if(user.profile.get.institution.isDefined) {
        profile = profile + ("institution" -> Json.toJson(user.profile.get.institution))
      }

    }
    Json.obj(

      "@context" -> Json.toJson(
        Map(
          "firstName" -> Json.toJson("http://schema.org/Person/givenName"),
          "lastName" -> Json.toJson("http://schema.org/Person/familyName"),
          "email" -> Json.toJson("http://schema.org/Person/email"),
          "affiliation" -> Json.toJson("http://schema.org/Person/affiliation")
        )
      ),
      "id" -> user.id.stringify,
      "firstName" -> user.firstName,
      "lastName" -> user.lastName,
      "fullName" -> user.fullName,
      "email" -> user.email,
      "avatar" -> user.getAvatarUrl(),
      "profile" -> Json.toJson(profile)
    )


  }

  private def getKeyValuePairsFromDatasetNoNameId(dataset : Dataset): Map[String,String] = {
    var key_value_pairs : Map[String,String] = Map.empty[String,String]
    val description = dataset.description
    val keyValues = description.split("\n")
    for (pair <- keyValues){
      var currentPair = pair.replace("{","")
      currentPair = currentPair.replace("}","")
      val listPair = currentPair.split(":")
      val first = listPair(0)
      val second = listPair(1)
      key_value_pairs = key_value_pairs + (first -> second)

    }
    return key_value_pairs
  }

  // @ApiOperation(value = "Make template public",
  //   notes = "",
  //   responseClass = "None", httpMethod = "PUT")
  def makeTemplatePublic(id : UUID) = PermissionAction(Permission.EditVocabulary, Some(ResourceRef(ResourceRef.vocabulary, id))) { implicit request=>
    implicit val user = request.user
    vocabularies.get(id) match {
      case Some(vocabulary) => {
        vocabularies.makePublic(id)
        Ok(toJson(Map("status"->"success")))
      }
      case None =>  BadRequest("No template found")
    }
  }

  // @ApiOperation(value = "Make template private",
  //   notes = "",
  //   responseClass = "None", httpMethod = "PUT")
  def makeTemplatePrivate(id : UUID) = PermissionAction(Permission.EditVocabulary, Some(ResourceRef(ResourceRef.vocabulary, id))) { implicit request=>
    implicit val user = request.user
    vocabularies.get(id) match {
      case Some(vocabulary) => {
        vocabularies.makePublic(id)
        Ok(toJson(Map("status"->"success")))
      }
      case None =>  BadRequest("No template found")
    }
  }

  // @ApiOperation(value = "get id name from template from tag",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getVocabIdNameFromTag(tag : String) = PermissionAction(Permission.ViewVocabulary){implicit request=>
    val user = request.user
    val tags = List(tag)
    var result : List[JsValue] = List.empty[JsValue]
    val vocabs_with_tag = vocabularies.findByTag(tags,true)
    result  = for (vocab <- vocabs_with_tag)
      yield Json.obj("template_id"->vocab.id,"name"->vocab.name)
    Ok(toJson(result))
  }

  // @ApiOperation(value = "Get vocabulary",
  //   notes = "This will check for Permission.ViewVocabulary",
  //   responseClass = "None", httpMethod = "GET")
  def getVocabulary(id: UUID) = PrivateServerAction { implicit request =>
    request.user match {
      case Some(user) => {
        vocabularies.get(id) match {
          case Some(vocab) => {
            if ((vocab.author.get.identityId.userId == user.identityId.userId) || vocab.isPublic){
              Ok(jsonVocabulary(vocab))
            }
            else {
              BadRequest(toJson("No permission to view vocabulary"))
            }
          }
          case None => BadRequest(toJson("No vocabulary matches id"))
        }
      }
      case None => BadRequest(toJson("no user supplied"))
    }
    vocabularies.get(id) match {
      case Some(vocab) => Ok(jsonVocabulary(vocab))
      case None => BadRequest(toJson("vocabulary not found"))
    }
  }

  // @ApiOperation(value = "Get all collections not in a space",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getAllCollectionsOfUserNotSharedInSpace() = PermissionAction(Permission.ViewCollection) { implicit request =>
    var all_collections_json : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
    request.user match {
      case Some(usr) => {
        val all_collections_list : List[Collection] = collections.listAllCollections(usr,false,0)
        for (collection <- all_collections_list){
          val spacesOfCol = collection.spaces
          if (spacesOfCol.isEmpty){
            val currentJson = jsonCollection(collection,request.user)
            all_collections_json += currentJson
          }
        }
      }
      case None =>
    }


    Ok(toJson(all_collections_json.toList))
  }

  // @ApiOperation(value = "Get level of tree in a not shared",
  //   notes = "",
  //   responseClass = "None", httpMethod = "GET")
  def getLevelOfTreeNotSharedInSpace(currentId : Option[String], currentType : String) = PermissionAction(Permission.ViewCollection) { implicit request =>
    request.user match {
      case Some(usr) => {
        val result = getChildrenOfNodeNotShared(currentId,currentType, request.user)
        Ok(toJson(result))

      }
      case None => BadRequest("No user supplied")
    }
  }

  // @ApiOperation(value = "Get all collections not in a space",
  //   notes = "",
    // responseClass = "None", httpMethod = "GET")
  def getLevelOfTreeInSpace(currentId : Option[String], currentType : String, spaceId : String) = PermissionAction(Permission.ViewCollection) { implicit request =>
    request.user match {
      case Some(usr) => {
        spaces.get(UUID(spaceId)) match {
          case Some(space) => {
            val result = getChildrenOfNodeInSpace(currentId,currentType, request.user,space)
            Ok(toJson(result))
          }
          case None => BadRequest("No space supplied")
        }


      }
      case None => BadRequest("No user supplied")
    }
  }
    // @ApiOperation(value = "List UUIDs of all collections in a space",
    // notes = "",
    // responseClass = "List", httpMethod = "GET")
  def listCollectionsCanEdit(spaceId: UUID, limit: Integer) = PermissionAction(Permission.AddResourceToSpace, Some(ResourceRef(ResourceRef.space, spaceId))) { implicit request =>
    val all_collections_list : ListBuffer[JsValue] = ListBuffer.empty[JsValue]
      request.user match {
      case Some(usr) => {
        val collectionsUserHasAccess  : List[Collection] = collections.listAllCollections(usr,false,0)
        for (collection <- collectionsUserHasAccess){
          if (collection.spaces.contains(spaceId)){
            all_collections_list += jsonCollection(collection,request.user)
          }
        }
      }
      case None => List.empty
    }
    Ok(toJson(all_collections_list.toList))
  }

  def jsonVocabulary(vocabulary: Vocabulary): JsValue = {
    val terms = getVocabularyTerms(vocabulary)
    val author = vocabulary.author.get.identityId.userId
    Json.obj("id" -> vocabulary.id.stringify, "author" -> author, "name" -> vocabulary.name, "terms" -> terms, "keys" -> vocabulary.keys.mkString(","),"tags"->vocabulary.tags.mkString(","), "attached_dataset"->vocabulary.attached_dataset,
      "description" -> vocabulary.description, "isPublic" -> vocabulary.isPublic.toString,
      "master"->vocabulary.master, "spaces" -> vocabulary.spaces.mkString(","))
  }

  def getVocabularyTerms(vocabulary: Vocabulary): List[VocabularyTerm] = {
    var vocab_terms: ListBuffer[VocabularyTerm] = ListBuffer.empty
    if (!vocabulary.terms.isEmpty) {
      for (term <- vocabulary.terms) {
        val current_term = vocabularyTermService.get(term) match {
          case Some(vocab_term) => {
            vocab_terms += vocab_term
          }
        }
      }
    }

    vocab_terms.toList
  }

  def datasetSharedWithOthers(dataset : Dataset, user : User): Boolean = {
    val spacesOfDataset = dataset.spaces
    for (space <- spacesOfDataset){
      if (spaceSharedWithOthers(space)){
        if (dataset.author.id == user.id){
          return true
        } else {
          return false
        }
      }
    }
    return false
  }

  def collectionSharedWithOthers(collection : Collection, user : User): Boolean = {
    val spacesOfCollection = collection.spaces
    for (space <- spacesOfCollection){
      if (spaceSharedWithOthers(space)){
        if (collection.author.id == user.id){
          return true
        } else {
          return false
        }

      }
    }
    return false
  }

  def collectionIsRootHasNoParentBelongingToMe(collection : Collection, user : User): Boolean ={
    var isRootHasNoParentBelongingToMe = false
    if (collections.hasRoot(collection)){
      isRootHasNoParentBelongingToMe = true
      var parent_ids = collection.parent_collection_ids
      for (each <- parent_ids){
        collections.get(each) match {
          case Some(parent) => {
            if (parent.author.id == user.id){
              if (parent.trash){
                isRootHasNoParentBelongingToMe = true
              } else {
                isRootHasNoParentBelongingToMe = false
              }
            }
          }
          case None => {

          }
        }
      }
    } else {
      var parent_ids = collection.parent_collection_ids
      var allParentsTrash = true
      for (each <- parent_ids){
        collections.get(each) match {
          case Some(parent) => {
            if (parent.author.id == user.id){
              if (parent.trash == false){
                allParentsTrash = false
              }
            }
          }
          case None => {

          }
        }
      }
      if (allParentsTrash){
        isRootHasNoParentBelongingToMe = true
      }
    }
    return isRootHasNoParentBelongingToMe
  }

  def spaceSharedWithOthers(spaceId : UUID): Boolean ={
    spaces.get(spaceId) match {
      case Some(space) => {
        if (space.userCount > 1){
          return true
        } else {
          return false
        }
      }
      case None => false
    }
  }

  def spaceSharedWithUser(spaceId : UUID, user : User) : Boolean = {
    spaces.get(spaceId) match {
      case Some(space) =>{
        val allowedPermissions = Set(Permission.ViewSpace,Permission.EditSpace)
        val userSpaces = spaces.listAccess(0, allowedPermissions,Some(user),false,true,false,false)
        if (userSpaces.contains(space)){
          return true
        } else {
          return false
        }
      }
      case None => false
    }
  }

  def datasetHasParentInSpaceSharedWithMe(dataset : models.Dataset, user : User) : Boolean = {
    var hasParentInSpaceSharedWithMe = false
    val collectionsOfDataset = dataset.collections
    for (col_id <- collectionsOfDataset){
      collections.get(col_id) match {
        case Some(col) => {
          val spacesOfCollection = col.spaces
          for (space <- spacesOfCollection){
            if (spaceSharedWithUser(space,user)){
              hasParentInSpaceSharedWithMe = true
            }
          }
        }
        case None => {
          Logger.info("Did not find collection")
          datasets.removeCollection(dataset.id,col_id)
        }
      }
    }

    return hasParentInSpaceSharedWithMe
  }

  def datasetHasParentCreatedByMe(dataset : models.Dataset, user : User) : Boolean = {
    var hasParentCreatedByMe = false
    val collectionsOfDataset = dataset.collections
    for (col_id <- collectionsOfDataset){
      collections.get(col_id) match {
        case Some(col) => {
          if (col.author.id == user.id){
            val trash = col.trash
            if (col.trash == true){
              hasParentCreatedByMe = false
            } else {
              hasParentCreatedByMe = true
            }
          }
        }
        case None => {
          Logger.info("Did not find collection")
          datasets.removeCollection(dataset.id,col_id)
        }
      }
    }

    return hasParentCreatedByMe
  }

  def getOrphanDatasetsSharedWithMe(user : User) : List[models.Dataset] = {
    var orphans : ListBuffer[Dataset] = ListBuffer.empty[Dataset]

    val allowedPermissions = Set(Permission.ViewDataset,Permission.EditDataset)
    val sharedDatasets = datasets.listAccess(0,allowedPermissions,Some(user),false,true,false).filter((d : Dataset) => (d.author.id != user.id))
    for (dataset <- sharedDatasets){
      if (!datasetHasParentInSpaceSharedWithMe(dataset,user)){
        orphans+=dataset
      }
    }
    orphans.toList
  }

  def getOrphanDatasetsMine(user : User) : List[models.Dataset] = {
    var orphans : ListBuffer[Dataset] = ListBuffer.empty[Dataset]

    val myDatasets = datasets.listUser(0,Some(user),false,user)
    for (dataset <- myDatasets){
      //TODO - change this
      if (!datasetHasParentCreatedByMe(dataset,user)){
        orphans+=dataset
      }
    }
    orphans.toList
  }

  def getOrphanDatasetsSharedWithOthers(user : User) : List[models.Dataset] = {
    var orphans : ListBuffer[Dataset] = ListBuffer.empty[Dataset]
    val myDatasets = datasets.listUser(0,Some(user),false,user)
    for (dataset <- myDatasets){
      //TODO - change this
      if (!datasetHasParentCreatedByMe(dataset,user)){
        var spacesOfDataset = dataset.spaces
        for (space <- spacesOfDataset) {
          if (spaceSharedWithOthers(space)) {
            orphans += dataset
          }
        }
      }
    }
    orphans.toList
  }
}
