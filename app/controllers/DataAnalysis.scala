package controllers

import java.io.{FileInputStream, FileOutputStream, IOException, InputStream}
import java.io.PrintWriter
import java.nio.file.Files
import java.util.zip.{ZipEntry, ZipInputStream}
import javax.activation.MimetypesFileTypeMap
import play.api.Logger

import scala.sys.process._
import api.Permission._
import fileutils.FilesUtils
import models._
import org.apache.commons.lang.StringEscapeUtils._
import util.{FileUtils, Formatters, RequiredFieldsConfig}
import java.text.SimpleDateFormat
import java.util.Date
import javax.inject.{Inject, Singleton}

import api.{Permission, routes}
import services.{CollectionService, DatasetService, _}

import scala.util.parsing.json.JSON
import scala.collection.immutable.List
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import services._
import services.mongodb.{MongoDBAnalysisSessionService, MongoDBContainerService, MongoDBNotebookService}
import org.apache.commons.lang.StringEscapeUtils
import scala.util.control.Breaks._
import play.api.Play.configuration


/**
  * Jupyter Notebook Integration
  */
@Singleton
class DataAnalysis @Inject()(containers: MongoDBContainerService, sessions: MongoDBAnalysisSessionService, notebooks: MongoDBNotebookService) extends SecuredController {

  val portRange = ( configuration(play.api.Play.current).getInt("startPort").getOrElse(8888),   configuration(play.api.Play.current).getInt("endPort").getOrElse(8891))
  val containerImage =  configuration(play.api.Play.current).getString("containerImage").getOrElse("jupyter/tensorflow-notebook")
  val jupyterHost =  configuration(play.api.Play.current).getString("jupyterHost").getOrElse("4ceed.illinois.edu")

  /**
    * Utils
    */
  def extensor(orig: String, ext: String) = (orig.split('.') match {
    case xs@Array(x) => xs
    case y => y.init
  }) :+ ext mkString "."

  def run_remote_command(containerId:String ,cmd: String): String = {
    var out2 = new StringBuilder
    var err2 = new StringBuilder
    val exitcodeConvert = Seq("/usr/bin/curl", "-X", "POST", "--unix-socket", "/var/run/docker.sock", "-H", "Content-Type: application/json", "http://localhost/containers/" + containerId + "/exec", "-d", "{\"AttachStdin\": true,\"AttachStdout\": true,\"AttachStderr\": true,\"Cmd\":" + cmd + "}").!(ProcessLogger(out2 append _, err2 append _))
    Logger.debug("Exit code for getting an execution instance for command " + cmd + " " + exitcodeConvert.toString())
    Logger.debug("StdOut for getting an execution instance for command " + cmd + " " + out2.toString())
    Logger.debug("StdErr for getting an execution instance for command " + cmd + " " + err2.toString())

    var jsonOut = scala.util.parsing.json.JSON.parseFull(out2.toString())
    var execInstanceId = ""
    jsonOut match {
      case Some(resultMap: Map[String, String]) => {
        if (resultMap.contains("Id")) {
          execInstanceId = resultMap("Id")
          Logger.debug("Execution Instance ID=" + execInstanceId)
          var exitcodeExecConvert = Seq("/usr/bin/curl", "-X", "POST", "--unix-socket", "/var/run/docker.sock", "-H", "Content-Type: application/json", "http://localhost/exec/" + execInstanceId + "/start", "-d", "{\"Detach\": false, \"Tty\":true}").!(ProcessLogger(out2 append _ + "\n", err2 append _))
          Logger.debug("Exit code for execution command " + cmd + " " + exitcodeExecConvert.toString())
          Logger.debug("StdOut for execution of command " + cmd + " " + out2.toString())
          Logger.debug("StdErr for execution of command " + cmd + " " + err2.toString())

          return out2.toString()
        }
        else {
          InternalServerError("Execution instance ID not found .")
          return ""
        }
      }
      case None => InternalServerError("Could not convert notebook to html.")
        return ""
    }
  }
  def find_changed_notebooks(containerId:String ,ext: String): List[String] = {
    var out = new StringBuilder
    var err = new StringBuilder
    val exitcodeConvert = Seq("/usr/bin/curl", "-X", "GET", "--unix-socket", "/var/run/docker.sock", "-H", "Content-Type: application/json", "http://localhost/containers/" + containerId + "/changes").!(ProcessLogger(out append _, err append _))
    Logger.debug("Exit code finding the changes in container " + exitcodeConvert.toString())
    Logger.debug("StdOut for finding the changes in container " + out.toString())
    Logger.debug("StdErr for finding the changes in container " + err.toString())

    var jsonOut = scala.util.parsing.json.JSON.parseFull(out.toString())
    var listNotebooks: List[String] = List()
    jsonOut match {
      case Some(listPaths: List[Map[String, String]]) => {
        for (x <- listPaths) {
          if (x.contains("Path")) {
            if(x("Path").contains(ext) && !x("Path").contains(".ipynb_checkpoints")) {
              Logger.debug("Found a path for a chaaanged notebook " + x("Path"))
              listNotebooks = listNotebooks :+ x("Path")
            }
          }
          else {
            InternalServerError("Could not find a Path keyword in the docker diff results .")
          }
        }
      }
      case None => InternalServerError("Could not convert notebook to html.")
    }
    Logger.debug("Changed notebooks " + listNotebooks)
    return listNotebooks
  }

  def stop_container(containerId:String) = {
    var out = new StringBuilder
    var err = new StringBuilder
    val exitcodeConvert = Seq("/usr/bin/curl", "-X", "POST", "--unix-socket", "/var/run/docker.sock", "-H", "Content-Type: application/json", "http://localhost/containers/" + containerId + "/stop").!(ProcessLogger(out append _, err append _))
    Logger.debug("Exit code for stopping container" + exitcodeConvert.toString())
    Logger.debug("StdOut for stopping container " + out.toString())
    Logger.debug("StdErr for stopping container " + err.toString())
  }


  /**
    * List Active Data Analysis Sessions
    */
  def listSessions() = PermissionAction(Permission.ViewFile)  { implicit request =>

    implicit val user = request.user
    user match {
      case Some(clowderUser) => {
        Logger.debug("Listing Active Data Analysis Sessions")
        var sessionList = sessions.list(clowderUser.id);
        Ok(views.html.listsession(sessionList))
      }
      case None => InternalServerError("No user defined")
    }

  }

  def list = PermissionAction(Permission.ViewFile) { implicit request =>
    implicit val user = request.user

    user match {
      case Some(clowderUser) => {
        var sessionList = sessions.list(clowderUser.id);
        Ok(views.html.listsession(sessionList))
      }
      case None => InternalServerError("No user defined")
    }

  }

  def terminate(id: UUID) = PermissionAction(Permission.ViewFile) { implicit request =>
    implicit val user = request.user
    Logger.debug("Terminating Session")

    user match {
      case Some(clowderUser) => {
        // get session and container ID
        var sessionToTerminate = sessions.get(id)
        sessionToTerminate match {
          case Some(session) => {

            //mark analysis session as stopped
            var updatedSession = AnalysisSession(id=session.id, sessionName=session.sessionName, userID= session.userID, userEmail=session.userEmail, url = session.url, containerID=session.containerID, reservedHours=session.reservedHours, reservedMinutes=session.reservedMinutes, status="TERMINATED");
            sessions.update(updatedSession)

            val containerId = session.containerID

            //find out what notebooks have been created
            val notebookPaths = find_changed_notebooks(containerId,"ipynb")

            //copy and save notebook and html files
            var out2 = new StringBuilder
            var err2 = new StringBuilder
            var notebookName = ""
            for (i <- 0 until notebookPaths.length) {
              var notebookPath = notebookPaths(i)
              if (notebookPath.endsWith("ipynbA") || notebookPath.endsWith("ipynbC") ) {
                notebookPath = notebookPath.dropRight(1)
              }
              Logger.debug("notebook= " + notebookPath)

              //copy ipynb file
              var cmd = "[\"cat\", \"" + notebookPath + "\" ]"
              var stdout = run_remote_command(containerId, cmd)
              notebookName = (notebookPath splitAt ((notebookPath lastIndexOf '/') + 1))._2
              Logger.debug("IPYNB Notebook name " + notebookName)
              var notebookContent = stdout.toString().substring(stdout.toString().indexOf('}')+9);
              //new PrintWriter(notebookName) { write(notebookContent); close }

              //convert ipynb to html
              cmd = "[\"jupyter\", \"nbconvert\", \"" + notebookPath + "\" , \"--to\", \"html\"]"
              run_remote_command(containerId, cmd)

              //copy html file
              val htmlNotebook = extensor(notebookPath, "html")
              cmd = "[\"cat\", \"" + htmlNotebook + "\" ]"
              stdout = run_remote_command(containerId, cmd)
              notebookName = (notebookPath splitAt ((notebookPath lastIndexOf '/') + 1))._2
              val htmlLocalNotebook = extensor(notebookName, "html")
              Logger.debug("HTML Notebook name " + htmlLocalNotebook)
              var htmlNotebookContent = stdout.toString().substring(stdout.toString().indexOf('\n')+1);
              //new PrintWriter(htmlLocalNotebook  + "_before") { write(htmlNotebookContent); close }
              htmlNotebookContent=htmlNotebookContent.replaceAll("[^\\x0A\\x0D\\x20-\\x7E]", "")
              //new PrintWriter(htmlLocalNotebook) { write(htmlNotebookContent); close }

              //save notebook to database
              notebookName = (notebookPath splitAt ((notebookPath lastIndexOf '/') + 1))._2
              var notebookRecord = Notebook(name=notebookName, userID=clowderUser.id, htmlfile=htmlNotebook, notebookfile=notebookPath, htmlfileContent=htmlNotebookContent, notebookfileContent=notebookContent);
              notebooks.insert(notebookRecord)

            }

            //mark analysis session as stopped
            //var updatedSession = AnalysisSession(id=session.id, sessionName=session.sessionName, userID= session.userID, userEmail=session.userEmail, url = session.url, containerID=session.containerID, reservedHours=session.reservedHours, reservedMinutes=session.reservedMinutes, status="TERMINATED");
            //sessions.update(updatedSession)

            //Stop container
            stop_container(containerId)

            var sessionList = sessions.list(clowderUser.id);
            Ok(views.html.listsession(sessionList))

          }
          case None => InternalServerError("No session with id" + id.toString())
        }
      }
      case None => InternalServerError("No user with id" + id.toString())
    }
  }


  /**
    * Open Reservation Interface
    */

 /*
  def reserveSession()  = UserAction(needActive=true) { implicit request =>
    implicit val user = request.user
    Logger.debug("Starting Reserve session interface")

    user match {
      case Some(clowderUser) => {
        Ok(views.html.reservesession("", ""))
      }
      case None => InternalServerError("No user defined")
    }
  }
 */

  /**
    * Start a docker containter
    */
  /*
  def startContainer(nameOfSession: String, hours: String, minutes: String) = PermissionAction(Permission.ViewFile) { implicit request =>
    implicit val user = request.user
    Logger.debug("Starting Docker container")
    Logger.debug("NAAME OF SESSION" + nameOfSession)


    user match {
      case Some(clowderUser) => {
        Logger.debug("USER=" + clowderUser.email)
        var userID = clowderUser.id
        var email = clowderUser.email

        try {
          //validate input
          var reservedHours= hours.toInt
          var reservedMinutes =  minutes.toInt


          try {
            var sessionList = sessions.listRunningSessions(clowderUser.id);

            if(sessionList.isEmpty) {

              //check if there is an available port
              var newPort = ""
              var allSessions = sessions.listRunningSessions()
              if (allSessions.isEmpty) {
                newPort =  portRange._1.toString()//"8888"
              }
              else {
                breakable { for (i <- portRange._1 to portRange._2) {
                  Logger.debug("searching for port")
                  val sessionsWithPort = sessions.findSessionWithPort(i.toString())
                  Logger.debug("passed db searching of port searching")

                  if (sessionsWithPort.isEmpty) {
                    Logger.debug("Port " + i.toString() + " is free")
                    newPort = i.toString()
                    break
                  }
                }
                } }
              Logger.debug("New Port = " + newPort)
              if (!newPort.isEmpty) {
                //create a container instance
                val out = new StringBuilder
                val err = new StringBuilder

                var portBindings = " \"PortBindings\": { \"<port>/tcp\": [{ \"HostPort\": \"<port>\" }] }"
                portBindings = portBindings.replace("<port>", newPort)
                Logger.debug("After Port Bindings " + portBindings )

                var exposedPorts = "{\"" + newPort + "/tcp\":{}}"
                var commandToChagePort = "[\"start.sh\",\"jupyter\",\"notebook\",\"--port=" + newPort + "\"]"


                val exitcodeCreateContainer = Seq("/usr/bin/curl", "-X", "POST", "--unix-socket", "/var/run/docker.sock", "-d", "{\"Cmd\":" + commandToChagePort + ",\"Image\":\"" + containerImage + "\", \"ExposedPorts\":" + exposedPorts + ",\"User\":\"root\",\"HostConfig\":{ \"AutoRemove\":true , \"NetworkMode\":\"default\"," + portBindings +  " }}", "-H", "Content-Type: application/json", "http://localhost/containers/create").!(ProcessLogger(out append _, err append _))

                Logger.debug("Create container exit code " + exitcodeCreateContainer.toString())
                Logger.debug("Create container output " + out.toString())
                Logger.debug("Create container error " + err.toString())
                val result = scala.util.parsing.json.JSON.parseFull(out.toString())
                var id = ""
                result match {
                  case Some(resultMap: Map[String, String]) => {
                    if (resultMap.contains("Id")) {
                      id = resultMap("Id")
                      Logger.debug("ID=" + id)
                      val exitcodeStartContainer = Seq("/usr/bin/curl", "-X", "POST", "--unix-socket", "/var/run/docker.sock", "-H", "Content-Type: application/json", "http://localhost/containers/" + id + "/start").!(ProcessLogger(out append _, err append _))
                      Thread.sleep(5000)

                      //define variables to append stdout and stderr of the container logs
                      val exitcode = Seq("/usr/bin/curl", "-X", "GET", "--unix-socket", "/var/run/docker.sock", "-H", "Content-Type: application/json", "http://localhost/containers/" + id + "/logs?stdout=1&stderr=1").!(ProcessLogger(out append _, err append _))
                      Logger.debug("OUT=" + out.toString())
                      Logger.debug("ERR=" + err.toString())

                      //parse the last line that contains the url and token
                      val r = "http".r
                      val httpOccurrences = r.findAllMatchIn(out.toString()).map(_.start).toList
                      Logger.debug("Occurences=" + httpOccurrences.toString())
                      val secondoccurrenceHttp = httpOccurrences(httpOccurrences.length - 1)
                      val lengthOfSubstring = out.toString().length - secondoccurrenceHttp
                      var url = out.toString().drop(secondoccurrenceHttp).take(lengthOfSubstring)

                      //put the name of the jupyter host in the url (this could be localhost or 4ceed.illinois.edu and it is configurable in the application.conf)
                      url = url.substring(0,url.indexOf('(')) + jupyterHost + url.substring(url.indexOf(')')+1)

                      Logger.debug("URL=" + url)

                      var session = AnalysisSession(sessionName = nameOfSession, userID = userID, userEmail = email, url = url, containerID = id, port = newPort, reservedHours = hours.toInt, reservedMinutes = minutes.toInt);
                      sessions.insert(session)


                      Ok(views.html.reservesession(url, "Jupyter server started, you can access it through the following URL"))

                    }
                    else {
                      InternalServerError("Could not find container ID in the response.")
                    }
                  }
                  case None => InternalServerError("Could not create a container.")
                }
              }
              else {
                InternalServerError("Not Enough resources to reserve your session, please try again later")
              }

            }
            else {
              InternalServerError("You already have an open session, you can access it here " + sessionList(0).url)
            }
          } catch {
            case e: Exception =>  {
              InternalServerError("Could not reserve a session, internal server error occured")
            }
          }
        } catch {
          case e: Exception =>  {
            InternalServerError("Hours and minutes must be numbers")
          }
        }
      }
      case None => InternalServerError("No user defined")
    }
  }
  */
}
