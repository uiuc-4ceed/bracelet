// @SOURCE:/Users/stevek/4ceed-clowder/conf/routes
// @HASH:8de3ef234bb2a5fe5ca89814b4a202b0871d3dd3
// @DATE:Thu Aug 29 17:10:48 CDT 2019

package securesocial.controllers;

public class routes {
public static final securesocial.controllers.ReverseRegistration Registration = new securesocial.controllers.ReverseRegistration();
public static final securesocial.controllers.ReverseProviderController ProviderController = new securesocial.controllers.ReverseProviderController();
public static final securesocial.controllers.ReversePasswordChange PasswordChange = new securesocial.controllers.ReversePasswordChange();
public static final securesocial.controllers.ReverseLoginPage LoginPage = new securesocial.controllers.ReverseLoginPage();
public static class javascript {
public static final securesocial.controllers.javascript.ReverseRegistration Registration = new securesocial.controllers.javascript.ReverseRegistration();
public static final securesocial.controllers.javascript.ReverseProviderController ProviderController = new securesocial.controllers.javascript.ReverseProviderController();
public static final securesocial.controllers.javascript.ReversePasswordChange PasswordChange = new securesocial.controllers.javascript.ReversePasswordChange();
public static final securesocial.controllers.javascript.ReverseLoginPage LoginPage = new securesocial.controllers.javascript.ReverseLoginPage();
}
public static class ref {
public static final securesocial.controllers.ref.ReverseRegistration Registration = new securesocial.controllers.ref.ReverseRegistration();
public static final securesocial.controllers.ref.ReverseProviderController ProviderController = new securesocial.controllers.ref.ReverseProviderController();
public static final securesocial.controllers.ref.ReversePasswordChange PasswordChange = new securesocial.controllers.ref.ReversePasswordChange();
public static final securesocial.controllers.ref.ReverseLoginPage LoginPage = new securesocial.controllers.ref.ReverseLoginPage();
}
}
          