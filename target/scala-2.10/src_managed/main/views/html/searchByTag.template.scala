
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object searchByTag extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template8[String,List[AnyRef],String,String,Int,Option[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(tag: String, items: List[AnyRef], prev: String, next: String, limit: Int, mode: Option[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters.ellipsize


Seq[Any](format.raw/*1.161*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Tagged Items")/*5.22*/ {_display_(Seq[Any](format.raw/*5.24*/("""
	"""),_display_(Seq[Any](/*6.3*/util/*6.7*/.masonry())),format.raw/*6.17*/("""
    <script src=""""),_display_(Seq[Any](/*7.19*/routes/*7.25*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*7.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*8.19*/routes/*8.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*8.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*9.19*/routes/*9.25*/.Assets.at("javascripts/spaceModify.js"))),format.raw/*9.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*10.19*/routes/*10.25*/.Assets.at("javascripts/fileListProcess.js"))),format.raw/*10.69*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*11.19*/routes/*11.25*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*11.72*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*12.19*/routes/*12.25*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*12.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*13.19*/routes/*13.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*13.67*/("""" type="text/javascript"></script>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h1>Tag: """),_display_(Seq[Any](/*17.23*/tag)),format.raw/*17.26*/("""</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10 col-md-10 col-sm-10"></div>
        <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="btn-group btn-group-sm pull-right">
                <button type="button" class="btn btn-default active" href="#tile-view" id="tile-view-btn"><span class="glyphicon glyphicon-th-large"></span></button>
                <button type="button" class="btn btn-default" href="#list-view" id="list-view-btn"><span class="glyphicon glyphicon-th-list"></span></button>
            </div>
            <script type="text/javascript" language="javascript">
                var viewMode = '"""),_display_(Seq[Any](/*28.34*/mode/*28.38*/.getOrElse("tile"))),format.raw/*28.56*/("""';
                $.cookie.raw = true;
                $.cookie.json = true;
                $(function() """),format.raw/*31.30*/("""{"""),format.raw/*31.31*/("""
                    $('#tile-view-btn').click(function() """),format.raw/*32.58*/("""{"""),format.raw/*32.59*/("""
                        $('#tile-view').removeClass('hidden');
                        $('#list-view').addClass('hidden');
                        $('#tile-view-btn').addClass('active');
                        $('#list-view-btn').removeClass('active');
                        viewMode = "tile";
                        updatePage();
                        $.cookie('view-mode', 'tile', """),format.raw/*39.55*/("""{"""),format.raw/*39.56*/(""" path: '/' """),format.raw/*39.67*/("""}"""),format.raw/*39.68*/(""");
                        $('#masonry').masonry().masonry("""),format.raw/*40.57*/("""{"""),format.raw/*40.58*/("""
                            itemSelector: '.post-box',
                            columnWidth: '.post-box',
                            transitionDuration: 4
                        """),format.raw/*44.25*/("""}"""),format.raw/*44.26*/(""");
                    """),format.raw/*45.21*/("""}"""),format.raw/*45.22*/(""");
                    $('#list-view-btn').click(function() """),format.raw/*46.58*/("""{"""),format.raw/*46.59*/("""
                        $('#tile-view').addClass('hidden');
                        $('#list-view').removeClass('hidden');
                        $('#list-view-btn').addClass('active');
                        $('#tile-view-btn').removeClass('active');
                        viewMode = "list";
                        updatePage();
                        //Utilizing library from https://github.com/carhartl/jquery-cookie/tree/v1.4.1
                        $.cookie("view-mode", "list", """),format.raw/*54.55*/("""{"""),format.raw/*54.56*/(""" path: '/' """),format.raw/*54.67*/("""}"""),format.raw/*54.68*/(""");
                    """),format.raw/*55.21*/("""}"""),format.raw/*55.22*/(""");
                    //updatePage();
                """),format.raw/*57.17*/("""}"""),format.raw/*57.18*/(""");

                $(document).ready(function() """),format.raw/*59.46*/("""{"""),format.raw/*59.47*/("""
                    //Set the cookie, for the case when it is passed in by the parameter
                    $.cookie("view-mode", viewMode, """),format.raw/*61.53*/("""{"""),format.raw/*61.54*/(""" path: '/' """),format.raw/*61.65*/("""}"""),format.raw/*61.66*/(""");
                    if (viewMode == "list") """),format.raw/*62.45*/("""{"""),format.raw/*62.46*/("""
                        $('#tile-view').addClass('hidden');
                        $('#list-view').removeClass('hidden');
                        $('#list-view-btn').addClass('active');
                        $('#tile-view-btn').removeClass('active');
                    """),format.raw/*67.21*/("""}"""),format.raw/*67.22*/("""
                    else """),format.raw/*68.26*/("""{"""),format.raw/*68.27*/("""
                        $('#tile-view').removeClass('hidden');
                        $('#list-view').addClass('hidden');
                        $('#tile-view-btn').addClass('active');
                        $('#list-view-btn').removeClass('active');
                    """),format.raw/*73.21*/("""}"""),format.raw/*73.22*/("""
                    updatePage();
                """),format.raw/*75.17*/("""}"""),format.raw/*75.18*/(""");

                //Function to unify the changing of the href for the next/previous links. Called on button activation for
                //viewMode style, as well as on initial load of page.
                function updatePage() """),format.raw/*79.39*/("""{"""),format.raw/*79.40*/("""
                    $('#nextlink').attr('href', """"),_display_(Seq[Any](/*80.51*/(routes.Tags.search(tag, next, limit)))),format.raw/*80.89*/("""");
                    $('#prevlink').attr('href', """"),_display_(Seq[Any](/*81.51*/(routes.Tags.search(tag, prev, limit)))),format.raw/*81.89*/("""");
                """),format.raw/*82.17*/("""}"""),format.raw/*82.18*/("""
            </script>
        </div>
    </div>

    """),format.raw/*87.23*/("""
    <div class="row hidden" id="tile-view">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div id="masonry">
            """),_display_(Seq[Any](/*91.14*/items/*91.19*/.map/*91.23*/ { item =>_display_(Seq[Any](format.raw/*91.33*/("""
                """),_display_(Seq[Any](/*92.18*/item/*92.22*/ match/*92.28*/ {/*93.21*/case dataset: models.Dataset =>/*93.52*/ {_display_(Seq[Any](format.raw/*93.54*/("""
                        """),_display_(Seq[Any](/*94.26*/datasets/*94.34*/.tile(dataset,None,Map.empty[UUID,Int],"col-lg-3 col-md-3 col-sm-3",false,false,routes.Datasets.dataset(dataset.id,None),false))),format.raw/*94.161*/("""

                    """)))}/*97.21*/case file: models.File =>/*97.46*/ {_display_(Seq[Any](format.raw/*97.48*/("""
                        """),_display_(Seq[Any](/*98.26*/files/*98.31*/.tile(file,"col-lg-3 col-md-3 col-sm-3",routes.Files.file(file.id),false))),format.raw/*98.104*/("""
                    """)))}})),format.raw/*100.19*/("""
            """)))})),format.raw/*101.14*/("""
            </div>
        </div>
    </div>

    """),format.raw/*106.23*/("""
    <div class="row hidden" id="list-view">
        <div class="col-lg-12 col-md-12 col-sm-12">
        """),_display_(Seq[Any](/*109.10*/items/*109.15*/.map/*109.19*/ { item =>_display_(Seq[Any](format.raw/*109.29*/("""
            """),_display_(Seq[Any](/*110.14*/item/*110.18*/ match/*110.24*/ {/*111.17*/case dataset: models.Dataset =>/*111.48*/ {_display_(Seq[Any](format.raw/*111.50*/("""
                    """),_display_(Seq[Any](/*112.22*/datasets/*112.30*/.listitem(dataset,None,Map.empty[UUID, Int],routes.Datasets.dataset(dataset.id,None), false))),format.raw/*112.122*/("""

                """)))}/*115.17*/case file: models.File =>/*115.42*/ {_display_(Seq[Any](format.raw/*115.44*/("""
                    """),_display_(Seq[Any](/*116.22*/files/*116.27*/.listitem(file,Map.empty[UUID,Int],routes.Files.file(file.id),None, None, None, false))),format.raw/*116.113*/("""
                """)))}})),format.raw/*118.14*/("""
        """)))})),format.raw/*119.10*/("""
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <ul class="pager">
                """),_display_(Seq[Any](/*126.18*/if(prev != "")/*126.32*/ {_display_(Seq[Any](format.raw/*126.34*/("""
                    <li class="previous"><a class="btn btn-link" id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
                """)))})),format.raw/*128.18*/("""
                """),_display_(Seq[Any](/*129.18*/if(next != "")/*129.32*/ {_display_(Seq[Any](format.raw/*129.34*/("""
                    <li class ="next"><a class="btn btn-link" id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
                """)))})),format.raw/*131.18*/("""
            </ul>
        </div>
    </div>
""")))})))}
    }
    
    def render(tag:String,items:List[AnyRef],prev:String,next:String,limit:Int,mode:Option[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(tag,items,prev,next,limit,mode)(flash,user)
    
    def f:((String,List[AnyRef],String,String,Int,Option[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (tag,items,prev,next,limit,mode) => (flash,user) => apply(tag,items,prev,next,limit,mode)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/searchByTag.scala.html
                    HASH: 1775f699541c3286fdb7ff5051410678d01f13ce
                    MATRIX: 678->1|972->160|1000->203|1036->205|1064->225|1103->227|1140->230|1151->234|1182->244|1236->263|1250->269|1313->311|1401->364|1415->370|1476->410|1564->463|1578->469|1639->509|1728->562|1743->568|1809->612|1898->665|1913->671|1982->718|2071->771|2086->777|2150->819|2239->872|2254->878|2318->920|2486->1052|2511->1055|3210->1718|3223->1722|3263->1740|3398->1847|3427->1848|3513->1906|3542->1907|3960->2297|3989->2298|4028->2309|4057->2310|4144->2369|4173->2370|4385->2554|4414->2555|4465->2578|4494->2579|4582->2639|4611->2640|5132->3133|5161->3134|5200->3145|5229->3146|5280->3169|5309->3170|5392->3225|5421->3226|5498->3275|5527->3276|5697->3418|5726->3419|5765->3430|5794->3431|5869->3478|5898->3479|6201->3754|6230->3755|6284->3781|6313->3782|6616->4057|6645->4058|6724->4109|6753->4110|7015->4344|7044->4345|7131->4396|7191->4434|7281->4488|7341->4526|7389->4546|7418->4547|7500->4619|7677->4760|7691->4765|7704->4769|7752->4779|7806->4797|7819->4801|7834->4807|7845->4830|7885->4861|7925->4863|7987->4889|8004->4897|8154->5024|8196->5068|8230->5093|8270->5095|8332->5121|8346->5126|8442->5199|8498->5240|8545->5254|8625->5323|8768->5429|8783->5434|8797->5438|8846->5448|8897->5462|8911->5466|8927->5472|8939->5491|8980->5522|9021->5524|9080->5546|9098->5554|9214->5646|9253->5682|9288->5707|9329->5709|9388->5731|9403->5736|9513->5822|9565->5854|9608->5864|9795->6014|9819->6028|9860->6030|10095->6232|10150->6250|10174->6264|10215->6266|10443->6461
                    LINES: 20->1|24->1|26->4|27->5|27->5|27->5|28->6|28->6|28->6|29->7|29->7|29->7|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|33->11|33->11|33->11|34->12|34->12|34->12|35->13|35->13|35->13|39->17|39->17|50->28|50->28|50->28|53->31|53->31|54->32|54->32|61->39|61->39|61->39|61->39|62->40|62->40|66->44|66->44|67->45|67->45|68->46|68->46|76->54|76->54|76->54|76->54|77->55|77->55|79->57|79->57|81->59|81->59|83->61|83->61|83->61|83->61|84->62|84->62|89->67|89->67|90->68|90->68|95->73|95->73|97->75|97->75|101->79|101->79|102->80|102->80|103->81|103->81|104->82|104->82|109->87|113->91|113->91|113->91|113->91|114->92|114->92|114->92|114->93|114->93|114->93|115->94|115->94|115->94|117->97|117->97|117->97|118->98|118->98|118->98|119->100|120->101|125->106|128->109|128->109|128->109|128->109|129->110|129->110|129->110|129->111|129->111|129->111|130->112|130->112|130->112|132->115|132->115|132->115|133->116|133->116|133->116|134->118|135->119|142->126|142->126|142->126|144->128|145->129|145->129|145->129|147->131
                    -- GENERATED --
                */
            