
package views.html.users

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object followingUsers extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[List[scala.Tuple4[UUID, String, String, String]],String,Int,Int,Int,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(followedUsers: List[(UUID, String, String, String)], name: String, prev: Int, next: Int, limit: Int)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.139*/("""

"""),_display_(Seq[Any](/*3.2*/main("Following Users")/*3.25*/ {_display_(Seq[Any](format.raw/*3.27*/("""
    <div class="row">
        <div class="col-md-12">
            <h1>Users """),_display_(Seq[Any](/*6.24*/name)),format.raw/*6.28*/(""" is Following</h1>
        </div>
    </div>
    <div class="row">
    """),_display_(Seq[Any](/*10.6*/for(userInfo <- followedUsers) yield /*10.36*/ {_display_(Seq[Any](format.raw/*10.38*/("""
       """),_display_(Seq[Any](/*11.9*/users/*11.14*/.tile(userInfo, "col-lg-2 col-md-2 col-sm-3 col-xs-4", true))),format.raw/*11.74*/("""
    """)))})),format.raw/*12.6*/("""

    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="pager">
                    <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
                """),_display_(Seq[Any](/*19.18*/if(prev >= 0)/*19.31*/ {_display_(Seq[Any](format.raw/*19.33*/("""
                    <li class="previous"><a id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
                """)))})),format.raw/*21.18*/("""
                """),_display_(Seq[Any](/*22.18*/if(next >= 0)/*22.31*/ {_display_(Seq[Any](format.raw/*22.33*/("""
                    <li class ="next"><a id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
                """)))})),format.raw/*24.18*/("""
            </ul>
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        var removeIndicator = true;
        $(document).ready(function() """),format.raw/*30.38*/("""{"""),format.raw/*30.39*/("""
           $('#nextlink').attr('href', """"),_display_(Seq[Any](/*31.42*/(routes.Users.getFollowing( next, limit)))),format.raw/*31.83*/("""");
	       $('#prevlink').attr('href', """"),_display_(Seq[Any](/*32.39*/(routes.Users.getFollowing(prev, limit)))),format.raw/*32.79*/("""");
        """),format.raw/*33.9*/("""}"""),format.raw/*33.10*/(""");
    </script>
    <script src=""""),_display_(Seq[Any](/*35.19*/routes/*35.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*35.67*/("""" type="text/javascript"></script>
""")))})),format.raw/*36.2*/("""
"""))}
    }
    
    def render(followedUsers:List[scala.Tuple4[UUID, String, String, String]],name:String,prev:Int,next:Int,limit:Int,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(followedUsers,name,prev,next,limit)(user)
    
    def f:((List[scala.Tuple4[UUID, String, String, String]],String,Int,Int,Int) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (followedUsers,name,prev,next,limit) => (user) => apply(followedUsers,name,prev,next,limit)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/users/followingUsers.scala.html
                    HASH: aea6dd5a84e1258b2b8eecb8dc07459b088f90ea
                    MATRIX: 683->1|915->138|952->141|983->164|1022->166|1135->244|1160->248|1267->320|1313->350|1353->352|1397->361|1411->366|1493->426|1530->432|1823->689|1845->702|1885->704|2098->885|2152->903|2174->916|2214->918|2420->1092|2624->1268|2653->1269|2731->1311|2794->1352|2872->1394|2934->1434|2973->1446|3002->1447|3073->1482|3088->1488|3152->1530|3219->1566
                    LINES: 20->1|23->1|25->3|25->3|25->3|28->6|28->6|32->10|32->10|32->10|33->11|33->11|33->11|34->12|41->19|41->19|41->19|43->21|44->22|44->22|44->22|46->24|52->30|52->30|53->31|53->31|54->32|54->32|55->33|55->33|57->35|57->35|57->35|58->36
                    -- GENERATED --
                */
            