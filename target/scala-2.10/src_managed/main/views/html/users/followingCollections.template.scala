
package views.html.users

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object followingCollections extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template10[List[models.Collection],Int,Int,Int,Option[String],Option[String],Option[String],Option[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(collectionsList: List[models.Collection], prev: Int, next: Int, limit: Int, mode: Option[String], space: Option[String], title: Option[String], owner:Option[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.231*/("""

"""),_display_(Seq[Any](/*3.2*/main("Following Collections")/*3.31*/ {_display_(Seq[Any](format.raw/*3.33*/("""

    """),_display_(Seq[Any](/*5.6*/util/*5.10*/.masonry())),format.raw/*5.20*/("""

    <script src=""""),_display_(Seq[Any](/*7.19*/routes/*7.25*/.Assets.at("javascripts/collectionListProcess.js"))),format.raw/*7.75*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*8.19*/routes/*8.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*8.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*9.19*/routes/*9.25*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*9.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*10.19*/routes/*10.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*10.67*/("""" type="text/javascript"></script>
    <div class="row">
        <div class="col-md-12">
            <h1>"""),_display_(Seq[Any](/*13.18*/title)),format.raw/*13.23*/("""</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
        </div>
        <div class="col-md-1">
        """),_display_(Seq[Any](/*21.10*/if(user.isDefined)/*21.28*/ {_display_(Seq[Any](format.raw/*21.30*/("""
            <a id="create-collection" class="btn btn-primary btn-sm pull-right" href=""""),_display_(Seq[Any](/*22.88*/routes/*22.94*/.Collections.newCollection(space))),format.raw/*22.127*/("""" title="Create a new collection"> <span class="glyphicon glyphicon-ok"></span> Create</a>
        """)))})),format.raw/*23.10*/("""

        </div>
        <div class="col-md-1">
            <div class="btn-group btn-group-sm pull-right">
                <button type="button" class="btn btn-default active" href="#tile-view" id="tile-view-btn"><span class="glyphicon glyphicon-th-large"></span></button>
                <button type="button" class="btn btn-default" href="#list-view" id="list-view-btn"><span class="glyphicon glyphicon-th-list"></span></button>
            </div>
            <script>
            var removeIndicator = true;
	        var viewMode = '"""),_display_(Seq[Any](/*33.27*/mode/*33.31*/.getOrElse("tile"))),format.raw/*33.49*/("""';
	        $.cookie.raw = true;
	        $.cookie.json = true;
            $(function() """),format.raw/*36.26*/("""{"""),format.raw/*36.27*/("""
                $('#tile-view-btn').click(function() """),format.raw/*37.54*/("""{"""),format.raw/*37.55*/("""
                  $('#tile-view').removeClass('hidden');
                  $('#list-view').addClass('hidden');
                  $('#tile-view-btn').addClass('active');
                  $('#list-view-btn').removeClass('active');
                  viewMode = "tile";
                  updatePage();
                  $.cookie('view-mode', 'tile', """),format.raw/*44.49*/("""{"""),format.raw/*44.50*/(""" path: '/' """),format.raw/*44.61*/("""}"""),format.raw/*44.62*/(""");
                  $('#masonry').masonry().masonry("""),format.raw/*45.51*/("""{"""),format.raw/*45.52*/("""
                      itemSelector: '.post-box',
                      columnWidth: '.post-box',
                      transitionDuration: 4
                  """),format.raw/*49.19*/("""}"""),format.raw/*49.20*/(""");
                """),format.raw/*50.17*/("""}"""),format.raw/*50.18*/(""");
                $('#list-view-btn').click(function() """),format.raw/*51.54*/("""{"""),format.raw/*51.55*/("""
                  $('#tile-view').addClass('hidden');
                  $('#list-view').removeClass('hidden');
                  $('#list-view-btn').addClass('active');
                  $('#tile-view-btn').removeClass('active');
                  viewMode = "list";
                  updatePage();
                  //Utilizing library from https://github.com/carhartl/jquery-cookie/tree/v1.4.1
                  $.cookie("view-mode", "list", """),format.raw/*59.49*/("""{"""),format.raw/*59.50*/(""" path: '/' """),format.raw/*59.61*/("""}"""),format.raw/*59.62*/(""");
                """),format.raw/*60.17*/("""}"""),format.raw/*60.18*/(""");
            """),format.raw/*61.13*/("""}"""),format.raw/*61.14*/(""");

            $(document).ready(function() """),format.raw/*63.42*/("""{"""),format.raw/*63.43*/("""
            	//Set the cookie, for the case when it is passed in by the parameter
                $.cookie("view-mode", viewMode, """),format.raw/*65.49*/("""{"""),format.raw/*65.50*/(""" path: '/' """),format.raw/*65.61*/("""}"""),format.raw/*65.62*/(""");
                if (viewMode == "list") """),format.raw/*66.41*/("""{"""),format.raw/*66.42*/("""
                    $('#tile-view').addClass('hidden');
                    $('#list-view').removeClass('hidden');
                    $('#list-view-btn').addClass('active');
                    $('#tile-view-btn').removeClass('active');
                """),format.raw/*71.17*/("""}"""),format.raw/*71.18*/("""
                else """),format.raw/*72.22*/("""{"""),format.raw/*72.23*/("""
                    $('#tile-view').removeClass('hidden');
                    $('#list-view').addClass('hidden');
                    $('#tile-view-btn').addClass('active');
                    $('#list-view-btn').removeClass('active');
                """),format.raw/*77.17*/("""}"""),format.raw/*77.18*/("""
                updatePage();
            """),format.raw/*79.13*/("""}"""),format.raw/*79.14*/(""");

            //Function to unify the changing of the href for the next/previous links. Called on button activation for
            //viewMode style, as well as on initial load of page.
            function updatePage() """),format.raw/*83.35*/("""{"""),format.raw/*83.36*/("""
                $('#nextlink').attr('href', """"),_display_(Seq[Any](/*84.47*/(routes.Collections.followingCollections(next, limit, "")))),format.raw/*84.105*/("""");
                $('#prevlink').attr('href', """"),_display_(Seq[Any](/*85.47*/(routes.Collections.followingCollections(prev, limit, "")))),format.raw/*85.105*/("""");
            """),format.raw/*86.13*/("""}"""),format.raw/*86.14*/("""
        </script>
        </div>
    </div>


    <div class="row hidden" id="tile-view">
        <div class="col-md-12">
            <div id="masonry">
            """),_display_(Seq[Any](/*95.14*/collectionsList/*95.29*/.map/*95.33*/ { collection =>_display_(Seq[Any](format.raw/*95.49*/("""
                """),_display_(Seq[Any](/*96.18*/collections/*96.29*/.tile(collection, routes.Collections.followingCollections(prev+1, limit, ""), None, "col-lg-3 col-md-3 col-sm-3", true))),format.raw/*96.148*/("""
            """)))})),format.raw/*97.14*/("""
            </div>
        </div>
    </div>


    <div class="row hidden" id="list-view">
        <div class="col-md-12">
        """),_display_(Seq[Any](/*105.10*/collectionsList/*105.25*/.map/*105.29*/ { collection =>_display_(Seq[Any](format.raw/*105.45*/("""
          """),_display_(Seq[Any](/*106.12*/collections/*106.23*/.listitem(collection, routes.Collections.followingCollections(prev+1, limit, "")))),format.raw/*106.104*/("""
        """)))})),format.raw/*107.10*/("""
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="pager">
                """),_display_(Seq[Any](/*114.18*/if(prev >=0)/*114.30*/ {_display_(Seq[Any](format.raw/*114.32*/("""
                    <li class="previous"><a id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
                """)))})),format.raw/*116.18*/("""
                """),_display_(Seq[Any](/*117.18*/if(next >=0)/*117.30*/ {_display_(Seq[Any](format.raw/*117.32*/("""
                    <li class ="next"><a id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
                """)))})),format.raw/*119.18*/("""
            </ul>
        </div>
    </div>

""")))})),format.raw/*124.2*/("""
"""))}
    }
    
    def render(collectionsList:List[models.Collection],prev:Int,next:Int,limit:Int,mode:Option[String],space:Option[String],title:Option[String],owner:Option[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(collectionsList,prev,next,limit,mode,space,title,owner)(flash,user)
    
    def f:((List[models.Collection],Int,Int,Int,Option[String],Option[String],Option[String],Option[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (collectionsList,prev,next,limit,mode,space,title,owner) => (flash,user) => apply(collectionsList,prev,next,limit,mode,space,title,owner)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/users/followingCollections.scala.html
                    HASH: 1a21e895b3ded5bd8758603a8b1062d83ca9cd69
                    MATRIX: 737->1|1061->230|1098->233|1135->262|1174->264|1215->271|1227->275|1258->285|1313->305|1327->311|1398->361|1486->414|1500->420|1561->460|1649->513|1663->519|1726->561|1815->614|1830->620|1894->662|2036->768|2063->773|2241->915|2268->933|2308->935|2432->1023|2447->1029|2503->1062|2635->1162|3209->1700|3222->1704|3262->1722|3379->1811|3408->1812|3490->1866|3519->1867|3895->2215|3924->2216|3963->2227|3992->2228|4073->2281|4102->2282|4290->2442|4319->2443|4366->2462|4395->2463|4479->2519|4508->2520|4981->2965|5010->2966|5049->2977|5078->2978|5125->2997|5154->2998|5197->3013|5226->3014|5299->3059|5328->3060|5487->3191|5516->3192|5555->3203|5584->3204|5655->3247|5684->3248|5967->3503|5996->3504|6046->3526|6075->3527|6358->3782|6387->3783|6458->3826|6487->3827|6737->4049|6766->4050|6849->4097|6930->4155|7016->4205|7097->4263|7141->4279|7170->4280|7373->4447|7397->4462|7410->4466|7464->4482|7518->4500|7538->4511|7680->4630|7726->4644|7896->4777|7921->4792|7935->4796|7990->4812|8039->4824|8060->4835|8165->4916|8208->4926|8375->5056|8397->5068|8438->5070|8652->5251|8707->5269|8729->5281|8770->5283|8977->5457|9056->5504
                    LINES: 20->1|23->1|25->3|25->3|25->3|27->5|27->5|27->5|29->7|29->7|29->7|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|35->13|35->13|43->21|43->21|43->21|44->22|44->22|44->22|45->23|55->33|55->33|55->33|58->36|58->36|59->37|59->37|66->44|66->44|66->44|66->44|67->45|67->45|71->49|71->49|72->50|72->50|73->51|73->51|81->59|81->59|81->59|81->59|82->60|82->60|83->61|83->61|85->63|85->63|87->65|87->65|87->65|87->65|88->66|88->66|93->71|93->71|94->72|94->72|99->77|99->77|101->79|101->79|105->83|105->83|106->84|106->84|107->85|107->85|108->86|108->86|117->95|117->95|117->95|117->95|118->96|118->96|118->96|119->97|127->105|127->105|127->105|127->105|128->106|128->106|128->106|129->107|136->114|136->114|136->114|138->116|139->117|139->117|139->117|141->119|146->124
                    -- GENERATED --
                */
            