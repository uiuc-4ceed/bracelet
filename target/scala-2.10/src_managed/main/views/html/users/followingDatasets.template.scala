
package views.html.users

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object followingDatasets extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template12[List[models.Dataset],Map[UUID, Int],Int,Int,Int,Option[String],Option[String],Option[String],Option[String],List[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(datasetsList: List[models.Dataset], comments: Map[UUID, Int], prev: Int, next: Int, limit: Int, mode: Option[String], space: Option[String], title: Option[String], owner:Option[String], userSelections: List[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.281*/("""

"""),_display_(Seq[Any](/*3.2*/main("Following Datasets")/*3.28*/ {_display_(Seq[Any](format.raw/*3.30*/("""

    """),_display_(Seq[Any](/*5.6*/util/*5.10*/.masonry())),format.raw/*5.20*/("""
    <script src=""""),_display_(Seq[Any](/*6.19*/routes/*6.25*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*6.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*7.19*/routes/*7.25*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*7.72*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*8.19*/routes/*8.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*8.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*9.19*/routes/*9.25*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*9.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*10.19*/routes/*10.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*10.67*/("""" type="text/javascript"></script>
    <div class="row">
        <div class="col-md-12">
            <h1>"""),_display_(Seq[Any](/*13.18*/title)),format.raw/*13.23*/("""</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
        </div>
        <div class="col-md-1">
        """),_display_(Seq[Any](/*21.10*/if(user.isDefined)/*21.28*/ {_display_(Seq[Any](format.raw/*21.30*/("""
            <a id="create-dataset" class="btn btn-primary btn-sm pull-right" href=""""),_display_(Seq[Any](/*22.85*/routes/*22.91*/.Datasets.newDataset(space, None))),format.raw/*22.124*/("""" title="Create a new dataset"> <span class="glyphicon glyphicon-ok"></span> Create</a>
        """)))})),format.raw/*23.10*/("""

        </div>
        <div class="col-md-1">
            <div class="btn-group btn-group-sm pull-right">
                <button type="button" class="btn btn-default active" href="#tile-view" id="tile-view-btn"><span class="glyphicon glyphicon-th-large"></span></button>
                <button type="button" class="btn btn-default" href="#list-view" id="list-view-btn"><span class="glyphicon glyphicon-th-list"></span></button>
            </div>
            <script type="text/javascript" language="javascript">
            var removeIndicator = true;
	        var viewMode = '"""),_display_(Seq[Any](/*33.27*/mode/*33.31*/.getOrElse("tile"))),format.raw/*33.49*/("""';
	        $.cookie.raw = true;
	        $.cookie.json = true;
            $(function() """),format.raw/*36.26*/("""{"""),format.raw/*36.27*/("""
                $('#tile-view-btn').click(function() """),format.raw/*37.54*/("""{"""),format.raw/*37.55*/("""
                  $('#tile-view').removeClass('hidden');
                  $('#list-view').addClass('hidden');
                  $('#tile-view-btn').addClass('active');
                  $('#list-view-btn').removeClass('active');
                  viewMode = "tile";
                  updatePage();
                  $.cookie('view-mode', 'tile', """),format.raw/*44.49*/("""{"""),format.raw/*44.50*/(""" path: '/' """),format.raw/*44.61*/("""}"""),format.raw/*44.62*/(""");
                  $('#masonry').masonry().masonry("""),format.raw/*45.51*/("""{"""),format.raw/*45.52*/("""
                      itemSelector: '.post-box',
                      columnWidth: '.post-box',
                      transitionDuration: 4
                  """),format.raw/*49.19*/("""}"""),format.raw/*49.20*/(""");
                """),format.raw/*50.17*/("""}"""),format.raw/*50.18*/(""");
                $('#list-view-btn').click(function() """),format.raw/*51.54*/("""{"""),format.raw/*51.55*/("""
                  $('#tile-view').addClass('hidden');
                  $('#list-view').removeClass('hidden');
                  $('#list-view-btn').addClass('active');
                  $('#tile-view-btn').removeClass('active');
                  viewMode = "list";
                  updatePage();
                  //Utilizing library from https://github.com/carhartl/jquery-cookie/tree/v1.4.1
                  $.cookie("view-mode", "list", """),format.raw/*59.49*/("""{"""),format.raw/*59.50*/(""" path: '/' """),format.raw/*59.61*/("""}"""),format.raw/*59.62*/(""");
                """),format.raw/*60.17*/("""}"""),format.raw/*60.18*/(""");
            """),format.raw/*61.13*/("""}"""),format.raw/*61.14*/(""");

            $(document).ready(function() """),format.raw/*63.42*/("""{"""),format.raw/*63.43*/("""
            	//Set the cookie, for the case when it is passed in by the parameter
                $.cookie("view-mode", viewMode, """),format.raw/*65.49*/("""{"""),format.raw/*65.50*/(""" path: '/' """),format.raw/*65.61*/("""}"""),format.raw/*65.62*/(""");
                if (viewMode == "list") """),format.raw/*66.41*/("""{"""),format.raw/*66.42*/("""
                    $('#tile-view').addClass('hidden');
                    $('#list-view').removeClass('hidden');
                    $('#list-view-btn').addClass('active');
                    $('#tile-view-btn').removeClass('active');
                """),format.raw/*71.17*/("""}"""),format.raw/*71.18*/("""
                else """),format.raw/*72.22*/("""{"""),format.raw/*72.23*/("""
                    $('#tile-view').removeClass('hidden');
                    $('#list-view').addClass('hidden');
                    $('#tile-view-btn').addClass('active');
                    $('#list-view-btn').removeClass('active');
                """),format.raw/*77.17*/("""}"""),format.raw/*77.18*/("""
            	updatePage();
            """),format.raw/*79.13*/("""}"""),format.raw/*79.14*/(""");

            //Function to unify the changing of the href for the next/previous links. Called on button activation for
            //viewMode style, as well as on initial load of page.
	        function updatePage() """),format.raw/*83.32*/("""{"""),format.raw/*83.33*/("""
	            $('#nextlink').attr('href', """"),_display_(Seq[Any](/*84.44*/(routes.Datasets.followingDatasets(next, limit, "")))),format.raw/*84.96*/("""");
	            $('#prevlink').attr('href', """"),_display_(Seq[Any](/*85.44*/(routes.Datasets.followingDatasets(prev, limit, "")))),format.raw/*85.96*/("""");
	        """),format.raw/*86.10*/("""}"""),format.raw/*86.11*/("""
        </script>
        </div>
    </div>

    <div class="row hidden" id="tile-view">
        <div class="col-md-12">
            <div id="masonry">
            """),_display_(Seq[Any](/*94.14*/datasetsList/*94.26*/.map/*94.30*/ { dataset =>_display_(Seq[Any](format.raw/*94.43*/("""
                """),_display_(Seq[Any](/*95.18*/if(userSelections.indexOf(dataset.id.toString()) != -1)/*95.73*/ {_display_(Seq[Any](format.raw/*95.75*/("""
                    """),_display_(Seq[Any](/*96.22*/datasets/*96.30*/.tile(dataset, space, comments, "col-lg-3 col-md-3 col-sm-3", true, false, routes.Datasets.followingDatasets(prev+1, limit, ""), true))),format.raw/*96.164*/("""
                """)))}/*97.19*/else/*97.24*/{_display_(Seq[Any](format.raw/*97.25*/("""
                    """),_display_(Seq[Any](/*98.22*/datasets/*98.30*/.tile(dataset, None, comments, "col-lg-3 col-md-3 col-sm-3", true, false, routes.Datasets.followingDatasets(prev+1, limit, ""), false))),format.raw/*98.164*/(""")
                """)))})),format.raw/*99.18*/("""
            """)))})),format.raw/*100.14*/("""
            </div>
        </div>
    </div>

    <div class="row hidden" id="list-view">
        <div class="col-md-12">
        """),_display_(Seq[Any](/*107.10*/datasetsList/*107.22*/.map/*107.26*/ { dataset =>_display_(Seq[Any](format.raw/*107.39*/("""
            """),_display_(Seq[Any](/*108.14*/if(userSelections.indexOf(dataset.id.toString()) != -1)/*108.69*/ {_display_(Seq[Any](format.raw/*108.71*/("""
                """),_display_(Seq[Any](/*109.18*/datasets/*109.26*/.listitem(dataset, None, comments, routes.Datasets.followingDatasets(prev+1, limit, ""), true))),format.raw/*109.120*/("""
            """)))}/*110.15*/else/*110.20*/{_display_(Seq[Any](format.raw/*110.21*/("""
                """),_display_(Seq[Any](/*111.18*/datasets/*111.26*/.listitem(dataset, None, comments, routes.Datasets.followingDatasets(prev+1, limit, ""), false))),format.raw/*111.121*/("""
            """)))})),format.raw/*112.14*/("""
        """)))})),format.raw/*113.10*/("""
        </div>
    </div>

    """),_display_(Seq[Any](/*117.6*/util/*117.10*/.masonry())),format.raw/*117.20*/("""

    <div class="row">
        <div class="col-md-12">
            <ul class="pager">
                    <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
                """),_display_(Seq[Any](/*123.18*/if(prev >= 0)/*123.31*/ {_display_(Seq[Any](format.raw/*123.33*/("""
                    <li class="previous"><a id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
                """)))})),format.raw/*125.18*/("""
                """),_display_(Seq[Any](/*126.18*/if(next >= 0)/*126.31*/ {_display_(Seq[Any](format.raw/*126.33*/("""
                    <li class ="next"><a id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
                """)))})),format.raw/*128.18*/("""
            </ul>
        </div>
    </div>


""")))})),format.raw/*134.2*/("""
"""))}
    }
    
    def render(datasetsList:List[models.Dataset],comments:Map[UUID, Int],prev:Int,next:Int,limit:Int,mode:Option[String],space:Option[String],title:Option[String],owner:Option[String],userSelections:List[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(datasetsList,comments,prev,next,limit,mode,space,title,owner,userSelections)(flash,user)
    
    def f:((List[models.Dataset],Map[UUID, Int],Int,Int,Int,Option[String],Option[String],Option[String],Option[String],List[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (datasetsList,comments,prev,next,limit,mode,space,title,owner,userSelections) => (flash,user) => apply(datasetsList,comments,prev,next,limit,mode,space,title,owner,userSelections)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/users/followingDatasets.scala.html
                    HASH: 514abf0c0e3eca983da2972d4f97f37d9d4e5d80
                    MATRIX: 759->1|1133->280|1170->283|1204->309|1243->311|1284->318|1296->322|1327->332|1381->351|1395->357|1458->399|1546->452|1560->458|1628->505|1716->558|1730->564|1791->604|1879->657|1893->663|1956->705|2045->758|2060->764|2124->806|2266->912|2293->917|2471->1059|2498->1077|2538->1079|2659->1164|2674->1170|2730->1203|2859->1300|3478->1883|3491->1887|3531->1905|3648->1994|3677->1995|3759->2049|3788->2050|4164->2398|4193->2399|4232->2410|4261->2411|4342->2464|4371->2465|4559->2625|4588->2626|4635->2645|4664->2646|4748->2702|4777->2703|5250->3148|5279->3149|5318->3160|5347->3161|5394->3180|5423->3181|5466->3196|5495->3197|5568->3242|5597->3243|5756->3374|5785->3375|5824->3386|5853->3387|5924->3430|5953->3431|6236->3686|6265->3687|6315->3709|6344->3710|6627->3965|6656->3966|6724->4006|6753->4007|7000->4226|7029->4227|7109->4271|7183->4323|7266->4370|7340->4422|7381->4435|7410->4436|7612->4602|7633->4614|7646->4618|7697->4631|7751->4649|7815->4704|7855->4706|7913->4728|7930->4736|8087->4870|8124->4889|8137->4894|8176->4895|8234->4917|8251->4925|8408->5059|8459->5078|8506->5092|8675->5224|8697->5236|8711->5240|8763->5253|8814->5267|8879->5322|8920->5324|8975->5342|8993->5350|9111->5444|9145->5459|9159->5464|9199->5465|9254->5483|9272->5491|9391->5586|9438->5600|9481->5610|9550->5643|9564->5647|9597->5657|9880->5903|9903->5916|9944->5918|10158->6099|10213->6117|10236->6130|10277->6132|10484->6306|10564->6354
                    LINES: 20->1|23->1|25->3|25->3|25->3|27->5|27->5|27->5|28->6|28->6|28->6|29->7|29->7|29->7|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|35->13|35->13|43->21|43->21|43->21|44->22|44->22|44->22|45->23|55->33|55->33|55->33|58->36|58->36|59->37|59->37|66->44|66->44|66->44|66->44|67->45|67->45|71->49|71->49|72->50|72->50|73->51|73->51|81->59|81->59|81->59|81->59|82->60|82->60|83->61|83->61|85->63|85->63|87->65|87->65|87->65|87->65|88->66|88->66|93->71|93->71|94->72|94->72|99->77|99->77|101->79|101->79|105->83|105->83|106->84|106->84|107->85|107->85|108->86|108->86|116->94|116->94|116->94|116->94|117->95|117->95|117->95|118->96|118->96|118->96|119->97|119->97|119->97|120->98|120->98|120->98|121->99|122->100|129->107|129->107|129->107|129->107|130->108|130->108|130->108|131->109|131->109|131->109|132->110|132->110|132->110|133->111|133->111|133->111|134->112|135->113|139->117|139->117|139->117|145->123|145->123|145->123|147->125|148->126|148->126|148->126|150->128|156->134
                    -- GENERATED --
                */
            