
package views.html.users

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listUsers extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[List[scala.Tuple4[UUID, String, String, String]],String,String,Int,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(listUsers: List[(UUID, String, String, String)], prev: String, next: String, limit: Int)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.127*/("""

"""),_display_(Seq[Any](/*3.2*/main("Users")/*3.15*/ {_display_(Seq[Any](format.raw/*3.17*/("""
    <div class="row">
        <div class="col-md-12">
            <h1>Users</h1>
        </div>
    </div>
    <div class="row">
    """),_display_(Seq[Any](/*10.6*/for(userInfo <- listUsers) yield /*10.32*/ {_display_(Seq[Any](format.raw/*10.34*/("""
        """),_display_(Seq[Any](/*11.10*/users/*11.15*/.tile(userInfo, "col-lg-2 col-md-2 col-sm-3 col-xs-4", true))),format.raw/*11.75*/("""
    """)))})),format.raw/*12.6*/("""

    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="pager">
                    <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
                """),_display_(Seq[Any](/*19.18*/if(prev != "")/*19.32*/ {_display_(Seq[Any](format.raw/*19.34*/("""
                    <li class="previous"><a id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span>Previous</a></li>
                """)))})),format.raw/*21.18*/("""
                """),_display_(Seq[Any](/*22.18*/if(next != "")/*22.32*/ {_display_(Seq[Any](format.raw/*22.34*/("""
                    <li class ="next"><a id="nextlink" title="Page forwards" href="#">Next<span class="glyphicon glyphicon-chevron-right"></span></a></li>
                """)))})),format.raw/*24.18*/("""
            </ul>
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        $(document).ready(function() """),format.raw/*29.38*/("""{"""),format.raw/*29.39*/("""
           $('#nextlink').attr('href', """"),_display_(Seq[Any](/*30.42*/(routes.Users.getUsers("a", next, limit)))),format.raw/*30.83*/("""");
	       $('#prevlink').attr('href', """"),_display_(Seq[Any](/*31.39*/(routes.Users.getUsers("b", prev, limit)))),format.raw/*31.80*/("""");
        """),format.raw/*32.9*/("""}"""),format.raw/*32.10*/(""");
    </script>
    <script src=""""),_display_(Seq[Any](/*34.19*/routes/*34.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*34.67*/("""" type="text/javascript"></script>

""")))})),format.raw/*36.2*/("""

"""))}
    }
    
    def render(listUsers:List[scala.Tuple4[UUID, String, String, String]],prev:String,next:String,limit:Int,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(listUsers,prev,next,limit)(user)
    
    def f:((List[scala.Tuple4[UUID, String, String, String]],String,String,Int) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (listUsers,prev,next,limit) => (user) => apply(listUsers,prev,next,limit)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/users/listUsers.scala.html
                    HASH: 025e0e1b7d1088effcac3893be3bbe7b4eb4976a
                    MATRIX: 677->1|897->126|934->129|955->142|994->144|1164->279|1206->305|1246->307|1292->317|1306->322|1388->382|1425->388|1718->645|1741->659|1781->661|1993->841|2047->859|2070->873|2110->875|2315->1048|2483->1188|2512->1189|2590->1231|2653->1272|2731->1314|2794->1355|2833->1367|2862->1368|2933->1403|2948->1409|3012->1451|3080->1488
                    LINES: 20->1|23->1|25->3|25->3|25->3|32->10|32->10|32->10|33->11|33->11|33->11|34->12|41->19|41->19|41->19|43->21|44->22|44->22|44->22|46->24|51->29|51->29|52->30|52->30|53->31|53->31|54->32|54->32|56->34|56->34|56->34|58->36
                    -- GENERATED --
                */
            