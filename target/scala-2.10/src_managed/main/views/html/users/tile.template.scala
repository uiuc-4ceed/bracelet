
package views.html.users

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[scala.Tuple4[UUID, String, String, String],String,Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(userInfo: (UUID, String, String, String), classes: String, showFollow: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.118*/("""

<div class="post-box """),_display_(Seq[Any](/*3.23*/classes)),format.raw/*3.30*/("""" id=""""),_display_(Seq[Any](/*3.37*/userInfo/*3.45*/._1)),format.raw/*3.48*/("""-tile">
    <div class = "img_wrapper">
        <div class = "img_hover">
            <a href=""""),_display_(Seq[Any](/*6.23*/routes/*6.29*/.Profile.viewProfileUUID(userInfo._1))),format.raw/*6.66*/("""">
            """),_display_(Seq[Any](/*7.14*/userInfo/*7.22*/._2)),format.raw/*7.25*/("""
            </a>
            """),_display_(Seq[Any](/*9.14*/if(showFollow)/*9.28*/ {_display_(Seq[Any](format.raw/*9.30*/("""
                <div>
                    <h4>
                    """),_display_(Seq[Any](/*12.22*/if(user.get.followedEntities.filter(x => (x.id == userInfo._1)).nonEmpty)/*12.95*/ {_display_(Seq[Any](format.raw/*12.97*/("""
<!--                         <button id="followButton" type="button"
                        class="btn-link"
                        data-toggle="button"
                        aria-pressed="true"
                        autocomplete="off"
                        objectId = """"),_display_(Seq[Any](/*18.38*/userInfo/*18.46*/._1.stringify)),format.raw/*18.59*/(""""
                        objectType = "user">
                            <span class='glyphicon glyphicon-star-empty'></span> Unfollow
                        </button>
 -->                    """)))}/*22.27*/else/*22.32*/{_display_(Seq[Any](format.raw/*22.33*/("""
<!--                         <button id="followButton" type="button"
                        class="btn-link"
                        data-toggle="button"
                        aria-pressed="false"
                        autocomplete="off"
                        objectId = """"),_display_(Seq[Any](/*28.38*/userInfo/*28.46*/._1.stringify)),format.raw/*28.59*/(""""
                        objectType = "user">
                            <span class='glyphicon glyphicon-star'></span>Follow
                        </button>
 -->                    """)))})),format.raw/*32.26*/("""
                    </h4>
                </div>
            """)))})),format.raw/*35.14*/("""
        </div>
        <img src="""),_display_(Seq[Any](/*37.19*/userInfo/*37.27*/._4)),format.raw/*37.30*/(""" class="img_thumbnail">
    </div>
</div>"""))}
    }
    
    def render(userInfo:scala.Tuple4[UUID, String, String, String],classes:String,showFollow:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(userInfo,classes,showFollow)(user)
    
    def f:((scala.Tuple4[UUID, String, String, String],String,Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (userInfo,classes,showFollow) => (user) => apply(userInfo,classes,showFollow)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/users/tile.scala.html
                    HASH: de9363742065154b56cfb2e21d40aca90c3cb9f6
                    MATRIX: 663->1|874->117|933->141|961->148|1003->155|1019->163|1043->166|1174->262|1188->268|1246->305|1297->321|1313->329|1337->332|1403->363|1425->377|1464->379|1569->448|1651->521|1691->523|2007->803|2024->811|2059->824|2274->1021|2287->1026|2326->1027|2643->1308|2660->1316|2695->1329|2914->1516|3009->1579|3079->1613|3096->1621|3121->1624
                    LINES: 20->1|23->1|25->3|25->3|25->3|25->3|25->3|28->6|28->6|28->6|29->7|29->7|29->7|31->9|31->9|31->9|34->12|34->12|34->12|40->18|40->18|40->18|44->22|44->22|44->22|50->28|50->28|50->28|54->32|57->35|59->37|59->37|59->37
                    -- GENERATED --
                */
            