
package views.html.users

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object followingFiles extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[List[models.File],Map[UUID, Int],Int,Int,Int,Option[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(filesList: List[models.File], comments: Map[UUID, Int], prev: Int, next: Int, limit: Int, mode: Option[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.150*/("""

"""),_display_(Seq[Any](/*3.2*/main("Following Files")/*3.25*/ {_display_(Seq[Any](format.raw/*3.27*/("""

    """),_display_(Seq[Any](/*5.6*/util/*5.10*/.masonry())),format.raw/*5.20*/("""
    <script src=""""),_display_(Seq[Any](/*6.19*/routes/*6.25*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*6.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*7.19*/routes/*7.25*/.Assets.at("javascripts/fileListProcess.js"))),format.raw/*7.69*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*8.19*/routes/*8.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*8.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*9.19*/routes/*9.25*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*9.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*10.19*/routes/*10.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*10.67*/("""" type="text/javascript"></script>
    <div class="row">
        <div class="col-md-12">
            <h1>Following Files</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-11">
        </div>
        <div class="col-md-1">
            <div class="btn-group btn-group-sm pull-right">
                <button type="button" class="btn btn-default active" href="#tile-view" id="tile-view-btn"><span class="glyphicon glyphicon-th-large"></span></button>
                <button type="button" class="btn btn-default" href="#list-view" id="list-view-btn"><span class="glyphicon glyphicon-th-list"></span></button>
            </div>
            <script type="text/javascript" language="javascript">
            var removeIndicator = true;
	        var viewMode = '"""),_display_(Seq[Any](/*27.27*/mode/*27.31*/.getOrElse("tile"))),format.raw/*27.49*/("""';
	        $.cookie.raw = true;
	        $.cookie.json = true;
            $(function() """),format.raw/*30.26*/("""{"""),format.raw/*30.27*/("""
                $('#tile-view-btn').click(function() """),format.raw/*31.54*/("""{"""),format.raw/*31.55*/("""
                  $('#tile-view').removeClass('hidden');
                  $('#list-view').addClass('hidden');
                  $('#tile-view-btn').addClass('active');
                  $('#list-view-btn').removeClass('active');
                  viewMode = "tile";
                  updatePage();
                  $.cookie('view-mode', 'tile', """),format.raw/*38.49*/("""{"""),format.raw/*38.50*/(""" path: '/' """),format.raw/*38.61*/("""}"""),format.raw/*38.62*/(""");
                  console.log("setting tile " + $.cookie('view-mode'));
                  $('#masonry').masonry().masonry("""),format.raw/*40.51*/("""{"""),format.raw/*40.52*/("""
                      itemSelector: '.post-box',
                      columnWidth: '.post-box',
                      transitionDuration: 4
                  """),format.raw/*44.19*/("""}"""),format.raw/*44.20*/(""");
                """),format.raw/*45.17*/("""}"""),format.raw/*45.18*/(""");
                $('#list-view-btn').click(function() """),format.raw/*46.54*/("""{"""),format.raw/*46.55*/("""
                  $('#tile-view').addClass('hidden');
                  $('#list-view').removeClass('hidden');
                  $('#list-view-btn').addClass('active');
                  $('#tile-view-btn').removeClass('active');
                  viewMode = "list";
                  updatePage();
                  //Utilizing library from https://github.com/carhartl/jquery-cookie/tree/v1.4.1
                  $.cookie("view-mode", "list", """),format.raw/*54.49*/("""{"""),format.raw/*54.50*/(""" path: '/' """),format.raw/*54.61*/("""}"""),format.raw/*54.62*/(""");
                  console.log("setting list " + $.cookie('view-mode'));
                """),format.raw/*56.17*/("""}"""),format.raw/*56.18*/(""");
            """),format.raw/*57.13*/("""}"""),format.raw/*57.14*/(""");

            $(document).ready(function() """),format.raw/*59.42*/("""{"""),format.raw/*59.43*/("""
            	console.log("on load, cookies are " + $.cookie('view-mode'));
                console.log("on load, viewMode is " + viewMode);
                //Set the cookie, for the case when it is passed in by the parameter
                $.cookie("view-mode", viewMode, """),format.raw/*63.49*/("""{"""),format.raw/*63.50*/(""" path: '/' """),format.raw/*63.61*/("""}"""),format.raw/*63.62*/(""");
                if (viewMode == "list") """),format.raw/*64.41*/("""{"""),format.raw/*64.42*/("""
                    $('#tile-view').addClass('hidden');
                    $('#list-view').removeClass('hidden');
                    $('#list-view-btn').addClass('active');
                    $('#tile-view-btn').removeClass('active');
                """),format.raw/*69.17*/("""}"""),format.raw/*69.18*/("""
                else """),format.raw/*70.22*/("""{"""),format.raw/*70.23*/("""
                    $('#tile-view').removeClass('hidden');
                    $('#list-view').addClass('hidden');
                    $('#tile-view-btn').addClass('active');
                    $('#list-view-btn').removeClass('active');
                """),format.raw/*75.17*/("""}"""),format.raw/*75.18*/("""
            	updatePage();
            """),format.raw/*77.13*/("""}"""),format.raw/*77.14*/(""");

            //Function to unify the changing of the href for the next/previous links. Called on button activation for
            //viewMode style, as well as on initial load of page.
	        function updatePage() """),format.raw/*81.32*/("""{"""),format.raw/*81.33*/("""
	            $('#nextlink').attr('href', """"),_display_(Seq[Any](/*82.44*/(routes.Files.followingFiles(next, limit, "")))),format.raw/*82.90*/("""");
	            $('#prevlink').attr('href', """"),_display_(Seq[Any](/*83.44*/(routes.Files.followingFiles(prev, limit, "")))),format.raw/*83.90*/("""");
	        """),format.raw/*84.10*/("""}"""),format.raw/*84.11*/("""
        </script>
        </div>
    </div>

    <div class="row hidden" id="tile-view">
        <div class="col-md-12">
            <div id="masonry">
            """),_display_(Seq[Any](/*92.14*/filesList/*92.23*/.map/*92.27*/ { file =>_display_(Seq[Any](format.raw/*92.37*/("""
                """),_display_(Seq[Any](/*93.18*/files/*93.23*/.tile(file, "col-lg-3 col-md-3 col-sm-3", routes.Files.followingFiles(prev+1, limit,""), true))),format.raw/*93.117*/("""
            """)))})),format.raw/*94.14*/("""
            </div>
        </div>
    </div>

    <div class="row hidden" id="list-view">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*101.14*/filesList/*101.23*/.map/*101.27*/ { file =>_display_(Seq[Any](format.raw/*101.37*/("""
                """),_display_(Seq[Any](/*102.18*/files/*102.23*/.listitem(file, comments, routes.Files.followingFiles(prev+1, limit, ""), None, None, None, false))),format.raw/*102.121*/("""
            """)))})),format.raw/*103.14*/("""
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="pager">
                    <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
                """),_display_(Seq[Any](/*111.18*/if(prev >= 0)/*111.31*/ {_display_(Seq[Any](format.raw/*111.33*/("""
                    <li class="previous"><a id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
                """)))})),format.raw/*113.18*/("""
                """),_display_(Seq[Any](/*114.18*/if(next >=0)/*114.30*/ {_display_(Seq[Any](format.raw/*114.32*/("""
                    <li class ="next"><a id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
                """)))})),format.raw/*116.18*/("""
            </ul>
        </div>
    </div>

""")))})),format.raw/*121.2*/("""
"""))}
    }
    
    def render(filesList:List[models.File],comments:Map[UUID, Int],prev:Int,next:Int,limit:Int,mode:Option[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(filesList,comments,prev,next,limit,mode)(user)
    
    def f:((List[models.File],Map[UUID, Int],Int,Int,Int,Option[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (filesList,comments,prev,next,limit,mode) => (user) => apply(filesList,comments,prev,next,limit,mode)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/users/followingFiles.scala.html
                    HASH: e39e9c938121e9a383e6128666349835e6709b59
                    MATRIX: 675->1|918->149|955->152|986->175|1025->177|1066->184|1078->188|1109->198|1163->217|1177->223|1240->265|1328->318|1342->324|1407->368|1495->421|1509->427|1570->467|1658->520|1672->526|1735->568|1824->621|1839->627|1903->669|2727->1457|2740->1461|2780->1479|2897->1568|2926->1569|3008->1623|3037->1624|3413->1972|3442->1973|3481->1984|3510->1985|3663->2110|3692->2111|3880->2271|3909->2272|3956->2291|3985->2292|4069->2348|4098->2349|4571->2794|4600->2795|4639->2806|4668->2807|4787->2898|4816->2899|4859->2914|4888->2915|4961->2960|4990->2961|5292->3235|5321->3236|5360->3247|5389->3248|5460->3291|5489->3292|5772->3547|5801->3548|5851->3570|5880->3571|6163->3826|6192->3827|6260->3867|6289->3868|6536->4087|6565->4088|6645->4132|6713->4178|6796->4225|6864->4271|6905->4284|6934->4285|7136->4451|7154->4460|7167->4464|7215->4474|7269->4492|7283->4497|7400->4591|7446->4605|7619->4741|7638->4750|7652->4754|7701->4764|7756->4782|7771->4787|7893->4885|7940->4899|8249->5171|8272->5184|8313->5186|8527->5367|8582->5385|8604->5397|8645->5399|8852->5573|8931->5620
                    LINES: 20->1|23->1|25->3|25->3|25->3|27->5|27->5|27->5|28->6|28->6|28->6|29->7|29->7|29->7|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|49->27|49->27|49->27|52->30|52->30|53->31|53->31|60->38|60->38|60->38|60->38|62->40|62->40|66->44|66->44|67->45|67->45|68->46|68->46|76->54|76->54|76->54|76->54|78->56|78->56|79->57|79->57|81->59|81->59|85->63|85->63|85->63|85->63|86->64|86->64|91->69|91->69|92->70|92->70|97->75|97->75|99->77|99->77|103->81|103->81|104->82|104->82|105->83|105->83|106->84|106->84|114->92|114->92|114->92|114->92|115->93|115->93|115->93|116->94|123->101|123->101|123->101|123->101|124->102|124->102|124->102|125->103|133->111|133->111|133->111|135->113|136->114|136->114|136->114|138->116|143->121
                    -- GENERATED --
                */
            