
package views.html.users

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object followingSpaces extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template10[List[models.ProjectSpace],String,Int,Option[String],Boolean,Option[String],Int,Int,Option[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(spacesList: List[models.ProjectSpace], date: String, limit: Int, owner: Option[String], showAll: Boolean, mode: Option[String], prev: Int, next: Int, title: Option[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages


Seq[Any](format.raw/*1.211*/("""
"""),_display_(Seq[Any](/*3.2*/main("Following Spaces")/*3.26*/ {_display_(Seq[Any](format.raw/*3.28*/("""

    """),_display_(Seq[Any](/*5.6*/util/*5.10*/.masonry())),format.raw/*5.20*/("""

    <script src=""""),_display_(Seq[Any](/*7.19*/routes/*7.25*/.Assets.at("javascripts/spaceListProcess.js"))),format.raw/*7.70*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*8.19*/routes/*8.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*8.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*9.19*/routes/*9.25*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*9.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*10.19*/routes/*10.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*10.67*/("""" type="text/javascript"></script>
    <div class="row">
        <div class="col-md-12">
            <h1>"""),_display_(Seq[Any](/*13.18*/title)),format.raw/*13.23*/("""</h1>
        </div>
    </div>

    <div class="row">
        <div class="btn-toolbar">
            <div class="btn-group btn-group-sm pull-right">
                <button type="button" class="btn btn-default active" href="#tile-view" id="tile-view-btn"><span class="glyphicon glyphicon-th-large"></span></button>
                <button type="button" class="btn btn-default" href="#list-view" id="list-view-btn"><span class="glyphicon glyphicon-th-list"></span></button>
            </div>
            """),_display_(Seq[Any](/*23.14*/if(user.isDefined)/*23.32*/{_display_(Seq[Any](format.raw/*23.33*/("""
                <a class="btn btn-primary btn-sm pull-right" id="create-space" href=""""),_display_(Seq[Any](/*24.87*/routes/*24.93*/.Spaces.newSpace)),format.raw/*24.109*/("""" title="Create a new """),_display_(Seq[Any](/*24.132*/Messages("space.title"))),format.raw/*24.155*/(""""> <span class="glyphicon glyphicon-ok"></span> Create</a>
            """)))})),format.raw/*25.14*/("""
        </div>
    </div>
    <script>
                var removeIndicator = true;
                var viewMode = '"""),_display_(Seq[Any](/*30.34*/mode/*30.38*/.getOrElse("tile"))),format.raw/*30.56*/("""';
                $.cookie.raw = true;
                $.cookie.json = true;
                $(function() """),format.raw/*33.30*/("""{"""),format.raw/*33.31*/("""
                    $('#tile-view-btn').click(function() """),format.raw/*34.58*/("""{"""),format.raw/*34.59*/("""
                        $('#tile-view').removeClass ('hidden');
                        $('#list-view').addClass ('hidden');
                        $('#tile-view-btn').addClass('active');
                        $('#list-view-btn').removeClass ('active');
                        viewMode = "tile";
                        updatePage();
                        $.cookie('view-mode', 'tile', """),format.raw/*41.55*/("""{"""),format.raw/*41.56*/(""" path: '/' """),format.raw/*41.67*/("""}"""),format.raw/*41.68*/(""");
                        $('#masonry').masonry().masonry( """),format.raw/*42.58*/("""{"""),format.raw/*42.59*/("""
                            itemSelector : '.post-box',
                            columnWidth : '.post-box',
                            transitionDuration : 4
                        """),format.raw/*46.25*/("""}"""),format.raw/*46.26*/(""");
                    """),format.raw/*47.21*/("""}"""),format.raw/*47.22*/(""");
                    $('#list-view-btn').click(function() """),format.raw/*48.58*/("""{"""),format.raw/*48.59*/("""
                        $('#tile-view').addClass('hidden');
                        $('#list-view' ).removeClass('hidden');
                        $('#list-view-btn').addClass('active');
                        $('#tile-view-btn').removeClass('active');
                        viewMode = "list" ;
                        updatePage();
                        //Utilizing library from https://github.com/carhartl/jquery-cookie/tree/v1.4.1
                        $.cookie("view-mode", "list", """),format.raw/*56.55*/("""{"""),format.raw/*56.56*/(""" path: '/' """),format.raw/*56.67*/("""}"""),format.raw/*56.68*/(""");
                    """),format.raw/*57.21*/("""}"""),format.raw/*57.22*/(""");
                    $('#myspace').click(function()"""),format.raw/*58.51*/("""{"""),format.raw/*58.52*/("""
                        var path;
                        if(window.location.search == "")"""),format.raw/*60.57*/("""{"""),format.raw/*60.58*/("""
                            path = location.pathname+'?showAll=false'
                        """),format.raw/*62.25*/("""}"""),format.raw/*62.26*/(""" else """),format.raw/*62.32*/("""{"""),format.raw/*62.33*/("""
                            path = location.pathname+location.search+'&showAll=false'
                        """),format.raw/*64.25*/("""}"""),format.raw/*64.26*/("""
                        window.location.assign( path);
                    """),format.raw/*66.21*/("""}"""),format.raw/*66.22*/(""");

                     $('#allspace').click(function()"""),format.raw/*68.53*/("""{"""),format.raw/*68.54*/("""
                        // this will leave a semi-colon in the path. but it should be a problem. Otherwise, we have to care about
                        // '/spaces?showAll=false;when=.....', where we should remove the one char after showAll not the one before.
                        var path=(location.pathname+location.search).replace('showAll=false', '');
                        window.location.assign( path);
                     """),format.raw/*73.22*/("""}"""),format.raw/*73.23*/(""");

                """),format.raw/*75.17*/("""}"""),format.raw/*75.18*/(""");

                $(document).ready (function () """),format.raw/*77.48*/("""{"""),format.raw/*77.49*/("""
                    //Set the cookie, for the case when it is passed in by the parameter
                    $.cookie("view-mode", viewMode, """),format.raw/*79.53*/("""{"""),format.raw/*79.54*/(""" path: '/' """),format.raw/*79.65*/("""}"""),format.raw/*79.66*/(""");
                    if (viewMode == "list") """),format.raw/*80.45*/("""{"""),format.raw/*80.46*/("""
                        $('#tile-view').addClass('hidden');
                        $('#list-view').removeClass('hidden');
                        $('#list-view-btn').addClass('active');
                        $('#tile-view-btn').removeClass('active');
                    """),format.raw/*85.21*/("""}"""),format.raw/*85.22*/("""
                    else """),format.raw/*86.26*/("""{"""),format.raw/*86.27*/("""
                        $('#tile-view').removeClass('hidden');
                        $('#list-view').addClass('hidden');
                        $('#tile-view-btn').addClass('active');
                        $('#list-view-btn').removeClass('active');
                    """),format.raw/*91.21*/("""}"""),format.raw/*91.22*/("""
                    updatePage();
                """),format.raw/*93.17*/("""}"""),format.raw/*93.18*/(""");

                //Function to unify the changing of the href for the next/previous links. Called on button activation for
                //viewMode style, as well as on initial load of page.
                function updatePage() """),format.raw/*97.39*/("""{"""),format.raw/*97.40*/("""
                    $('#nextlink').attr('href', """"),_display_(Seq[Any](/*98.51*/(routes.Spaces.followingSpaces(next, limit, "")))),format.raw/*98.99*/("""");
                    $('#prevlink').attr('href', """"),_display_(Seq[Any](/*99.51*/(routes.Spaces.followingSpaces(prev, limit, "")))),format.raw/*99.99*/("""");
                """),format.raw/*100.17*/("""}"""),format.raw/*100.18*/("""
            </script>

    <div class="row hidden" id="tile-view">
        <div class="col-md-12">
            <div id="masonry">
            """),_display_(Seq[Any](/*106.14*/spacesList/*106.24*/.map/*106.28*/ { space =>_display_(Seq[Any](format.raw/*106.39*/("""
                """),_display_(Seq[Any](/*107.18*/spaces/*107.24*/.tile(space, "col-lg-3 col-md-3 col-sm-3 col-xs-12", routes.Spaces.followingSpaces(prev+1, limit, ""), true))),format.raw/*107.132*/("""
            """)))})),format.raw/*108.14*/("""
            </div>
        </div>
    </div>

    <div class="row hidden" id="list-view">
        <div class="col-md-12">
        """),_display_(Seq[Any](/*115.10*/spacesList/*115.20*/.map/*115.24*/ { space =>_display_(Seq[Any](format.raw/*115.35*/("""
            """),_display_(Seq[Any](/*116.14*/spaces/*116.20*/.listItem(space, routes.Spaces.followingSpaces(prev+1, limit, "")))),format.raw/*116.86*/("""
        """)))})),format.raw/*117.10*/("""
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="pager">
                """),_display_(Seq[Any](/*124.18*/if(prev >= 0)/*124.31*/ {_display_(Seq[Any](format.raw/*124.33*/("""
                    <li class="previous"><a id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
                """)))})),format.raw/*126.18*/("""
                """),_display_(Seq[Any](/*127.18*/if(next >= 0)/*127.31*/ {_display_(Seq[Any](format.raw/*127.33*/("""
                    <li class ="next"><a id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
                """)))})),format.raw/*129.18*/("""
            </ul>
        </div>
    </div>

""")))})))}
    }
    
    def render(spacesList:List[models.ProjectSpace],date:String,limit:Int,owner:Option[String],showAll:Boolean,mode:Option[String],prev:Int,next:Int,title:Option[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(spacesList,date,limit,owner,showAll,mode,prev,next,title)(user)
    
    def f:((List[models.ProjectSpace],String,Int,Option[String],Boolean,Option[String],Int,Int,Option[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (spacesList,date,limit,owner,showAll,mode,prev,next,title) => (user) => apply(spacesList,date,limit,owner,showAll,mode,prev,next,title)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/users/followingSpaces.scala.html
                    HASH: 87c427ea30ef01812756da17939cc95fce74c841
                    MATRIX: 715->1|1049->210|1085->243|1117->267|1156->269|1197->276|1209->280|1240->290|1295->310|1309->316|1375->361|1463->414|1477->420|1538->460|1626->513|1640->519|1703->561|1792->614|1807->620|1871->662|2013->768|2040->773|2581->1278|2608->1296|2647->1297|2770->1384|2785->1390|2824->1406|2884->1429|2930->1452|3034->1524|3187->1641|3200->1645|3240->1663|3375->1770|3404->1771|3490->1829|3519->1830|3940->2223|3969->2224|4008->2235|4037->2236|4125->2296|4154->2297|4369->2484|4398->2485|4449->2508|4478->2509|4566->2569|4595->2570|5118->3065|5147->3066|5186->3077|5215->3078|5266->3101|5295->3102|5376->3155|5405->3156|5524->3247|5553->3248|5676->3343|5705->3344|5739->3350|5768->3351|5907->3462|5936->3463|6040->3539|6069->3540|6153->3596|6182->3597|6649->4036|6678->4037|6726->4057|6755->4058|6834->4109|6863->4110|7033->4252|7062->4253|7101->4264|7130->4265|7205->4312|7234->4313|7537->4588|7566->4589|7620->4615|7649->4616|7952->4891|7981->4892|8060->4943|8089->4944|8351->5178|8380->5179|8467->5230|8537->5278|8627->5332|8697->5380|8746->5400|8776->5401|8957->5545|8977->5555|8991->5559|9041->5570|9096->5588|9112->5594|9244->5702|9291->5716|9460->5848|9480->5858|9494->5862|9544->5873|9595->5887|9611->5893|9700->5959|9743->5969|9910->6099|9933->6112|9974->6114|10188->6295|10243->6313|10266->6326|10307->6328|10514->6502
                    LINES: 20->1|24->1|25->3|25->3|25->3|27->5|27->5|27->5|29->7|29->7|29->7|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|35->13|35->13|45->23|45->23|45->23|46->24|46->24|46->24|46->24|46->24|47->25|52->30|52->30|52->30|55->33|55->33|56->34|56->34|63->41|63->41|63->41|63->41|64->42|64->42|68->46|68->46|69->47|69->47|70->48|70->48|78->56|78->56|78->56|78->56|79->57|79->57|80->58|80->58|82->60|82->60|84->62|84->62|84->62|84->62|86->64|86->64|88->66|88->66|90->68|90->68|95->73|95->73|97->75|97->75|99->77|99->77|101->79|101->79|101->79|101->79|102->80|102->80|107->85|107->85|108->86|108->86|113->91|113->91|115->93|115->93|119->97|119->97|120->98|120->98|121->99|121->99|122->100|122->100|128->106|128->106|128->106|128->106|129->107|129->107|129->107|130->108|137->115|137->115|137->115|137->115|138->116|138->116|138->116|139->117|146->124|146->124|146->124|148->126|149->127|149->127|149->127|151->129
                    -- GENERATED --
                */
            