
package views.html.users

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object followers extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[List[scala.Tuple4[UUID, String, String, String]],String,UUID,Int,Int,Int,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(followers: List[(UUID, String, String, String)], name: String, profileId: UUID, prev: Int, next: Int, limit: Int)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.152*/("""

"""),_display_(Seq[Any](/*3.2*/main("Followers")/*3.19*/ {_display_(Seq[Any](format.raw/*3.21*/("""
    <div class="row">
        <div class="col-md-12">
            <h1>"""),_display_(Seq[Any](/*6.18*/name)),format.raw/*6.22*/("""'s Followers</h1>
        </div>
    </div>
    <div class="row">

        """),_display_(Seq[Any](/*11.10*/for(row <- followers) yield /*11.31*/ {_display_(Seq[Any](format.raw/*11.33*/("""
            """),_display_(Seq[Any](/*12.14*/users/*12.19*/.tile(row, "col-lg-2 col-md-2 col-sm-3 col-xs-4", false))),format.raw/*12.75*/("""
        """)))})),format.raw/*13.10*/("""
        """),_display_(Seq[Any](/*14.10*/if(followers.size < 1)/*14.32*/ {_display_(Seq[Any](format.raw/*14.34*/("""
            <div class="text-center col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <h2>This area will show users that follow you. Ask other users to follow you <a class="btn-link" href=""""),_display_(Seq[Any](/*16.120*/routes/*16.126*/.Profile.viewProfileUUID(profileId))),format.raw/*16.161*/("""">here</a></h2>
            </div>
        """)))})),format.raw/*18.10*/("""
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="pager">
                    <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
                """),_display_(Seq[Any](/*24.18*/if(prev >= 0)/*24.31*/ {_display_(Seq[Any](format.raw/*24.33*/("""
                    <li class="previous"><a id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
                """)))})),format.raw/*26.18*/("""
                """),_display_(Seq[Any](/*27.18*/if(next >= 0)/*27.31*/ {_display_(Seq[Any](format.raw/*27.33*/("""
                    <li class ="next"><a id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
                """)))})),format.raw/*29.18*/("""
            </ul>
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        $(document).ready(function() """),format.raw/*34.38*/("""{"""),format.raw/*34.39*/("""
                $('#nextlink').attr('href', """"),_display_(Seq[Any](/*35.47*/(routes.Users.getFollowers( next, limit)))),format.raw/*35.88*/("""");
	            $('#prevlink').attr('href', """"),_display_(Seq[Any](/*36.44*/(routes.Users.getFollowers(prev, limit)))),format.raw/*36.84*/("""");

        """),format.raw/*38.9*/("""}"""),format.raw/*38.10*/(""");
    </script>
""")))})),format.raw/*40.2*/("""
"""))}
    }
    
    def render(followers:List[scala.Tuple4[UUID, String, String, String]],name:String,profileId:UUID,prev:Int,next:Int,limit:Int,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(followers,name,profileId,prev,next,limit)(user)
    
    def f:((List[scala.Tuple4[UUID, String, String, String]],String,UUID,Int,Int,Int) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (followers,name,profileId,prev,next,limit) => (user) => apply(followers,name,profileId,prev,next,limit)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/users/followers.scala.html
                    HASH: 65be4f7e0231e4c278713d9ccf22559ba84cde8d
                    MATRIX: 683->1|928->151|965->154|990->171|1029->173|1136->245|1161->249|1273->325|1310->346|1350->348|1400->362|1414->367|1492->423|1534->433|1580->443|1611->465|1651->467|1886->665|1902->671|1960->706|2036->750|2328->1006|2350->1019|2390->1021|2603->1202|2657->1220|2679->1233|2719->1235|2925->1409|3093->1549|3122->1550|3205->1597|3268->1638|3351->1685|3413->1725|3453->1738|3482->1739|3531->1757
                    LINES: 20->1|23->1|25->3|25->3|25->3|28->6|28->6|33->11|33->11|33->11|34->12|34->12|34->12|35->13|36->14|36->14|36->14|38->16|38->16|38->16|40->18|46->24|46->24|46->24|48->26|49->27|49->27|49->27|51->29|56->34|56->34|57->35|57->35|58->36|58->36|60->38|60->38|62->40
                    -- GENERATED --
                */
            