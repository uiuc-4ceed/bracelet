
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object noPermissions extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(msg: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.51*/("""

"""),_display_(Seq[Any](/*3.2*/main("No Permissions")/*3.24*/ {_display_(Seq[Any](format.raw/*3.26*/("""
    <div class="page-header">
        <h1>Invalid Permissions</h1>
    </div>
    <div class="row">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*9.14*/if(msg != null)/*9.29*/{_display_(Seq[Any](format.raw/*9.30*/("""
                """),_display_(Seq[Any](/*10.18*/msg)),format.raw/*10.21*/("""
            """)))}/*11.14*/else/*11.18*/{_display_(Seq[Any](format.raw/*11.19*/("""            
                You do not have the required permissions to view that page.
            """)))})),format.raw/*13.14*/(""" 
        </div>
    </div>
""")))})),format.raw/*16.2*/("""
"""))}
    }
    
    def render(msg:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(msg)(user)
    
    def f:((String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (msg) => (user) => apply(msg)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:23 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/noPermissions.scala.html
                    HASH: 483285dd752460724d5f7ad51ddd1fef39916552
                    MATRIX: 615->1|758->50|795->53|825->75|864->77|1045->223|1068->238|1106->239|1160->257|1185->260|1218->274|1231->278|1270->279|1404->381|1464->410
                    LINES: 20->1|23->1|25->3|25->3|25->3|31->9|31->9|31->9|32->10|32->10|33->11|33->11|33->11|35->13|38->16
                    -- GENERATED --
                */
            