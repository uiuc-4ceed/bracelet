
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object selected extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[scala.collection.mutable.ListBuffer[Dataset],Option[securesocial.core.Identity],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(selectedDatasets: scala.collection.mutable.ListBuffer[Dataset])(implicit ident: Option[securesocial.core.Identity], user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.145*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Selected Datasets")/*5.27*/ {_display_(Seq[Any](format.raw/*5.29*/("""
<script src=""""),_display_(Seq[Any](/*6.15*/routes/*6.21*/.Assets.at("javascripts/jquery-ui-1.10.3.custom.min.js"))),format.raw/*6.77*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*7.15*/routes/*7.21*/.Assets.at("javascripts/select-bulk.js"))),format.raw/*7.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*8.15*/routes/*8.21*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*8.61*/("""" type="text/javascript"></script>

<div class="page-header">
	<h1>Selections</h1>
	<div id="select-count">
		You currently have <b>"""),_display_(Seq[Any](/*13.26*/selectedDatasets/*13.42*/.length)),format.raw/*13.49*/("""</b> dataset(s) selected.
	</div>
</div>

Bulk operations are still subject to individual dataset permissions; if you do not have permission to perform the action on one of your selections, it will not be included in the operation.

"""),_display_(Seq[Any](/*19.2*/if(user.isDefined)/*19.20*/ {_display_(Seq[Any](format.raw/*19.22*/("""
	<div class="row">
		<div class="col-xs-12">
			<hr/>
				"""),_display_(Seq[Any](/*23.6*/if(selectedDatasets.length==0)/*23.36*/ {_display_(Seq[Any](format.raw/*23.38*/("""
					<a id='download-url' href="#" onclick="downloadAllSelections();"
					class="btn btn-link disabled" title="Download all selected files as zip" role="button">
						<span class="glyphicon glyphicon-download-alt"></span> Download All</a>
					<a id='delete-url' href="#" onclick="confirmDeleteSelected();"
					class="btn btn-link disabled" title="Delete all selected datasets" role="button">
						<span class="glyphicon glyphicon-trash"></span> Delete All</a>
					<a id='tag-url' href="#" onclick="showTagPopup();"
					class="btn btn-link" title="Apply a tag to all selected datasets" role="button">
						<span class="glyphicon glyphicon-tag"></span> Tag All</a>
				""")))}/*33.7*/else/*33.12*/{_display_(Seq[Any](format.raw/*33.13*/("""
					<a id='download-url' href="#" onclick="downloadAllSelections();"
					class="btn btn-link" title="Download all selected files as zip" role="button">
						<span class="glyphicon glyphicon-download-alt"></span> Download All</a>
					<a id='delete-url' href="#" onclick="confirmDeleteSelected();"
					class="btn btn-link" title="Delete all selected datasets" role="button">
						<span class="glyphicon glyphicon-trash"></span> Delete All</a>
					<a id='tag-url' href="#" onclick="showTagPopup();"
					class="btn btn-link" title="Apply a tag to all selected datasets" role="button">
						<span class="glyphicon glyphicon-tag"></span> Tag All</a>
				""")))})),format.raw/*43.6*/("""
				<a id='clear-url' href="#" onclick="clearSelections();"
				class="btn btn-link" title="Clear all selected datasets" role="button">
					<span class="glyphicon glyphicon-erase"></span> Clear Selections</a>
			<hr/>
		</div>
	</div>
""")))})),format.raw/*50.2*/("""

<div class="row">
	<div class="col-md-12">
		<h2>Selected Datasets</h2>
		"""),_display_(Seq[Any](/*55.4*/for(d <- selectedDatasets) yield /*55.30*/ {_display_(Seq[Any](format.raw/*55.32*/("""
			"""),_display_(Seq[Any](/*56.5*/datasets/*56.13*/.tile(d, None, Map.empty, "col-lg-3 col-md-3 col-sm-3", false, false, routes.Application.index(), true))),format.raw/*56.116*/("""
		""")))})),format.raw/*57.4*/("""
	</div>
	<!-- TODO: Old code for drag+drop relationship modification; revisit eventually -->
	<!-- div class="row">
		<div class="col-md-6">
			<div class="col-md-12">
				<h3>Add Relationship</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div id="relate1" class="droppable">
				  <p>Drop here</p>
				</div>
			</div>
			<div class="col-md-4" style="vertical-align: middle;">
		      <select id ="relate" class="form-control">
			    <option>describes</option>
			    <option>duplicates</option>
			    <option>references</option>
			    <option>relates</option>
			 </select>
			</div>
			<div class="col-md-4">
				<div id="relate2" class="droppable">
				  <p>Drop here</p>
				</div>
			</div>
		</div>
	    <div class="row">
			<div class="col-md-12">
				<button type="button" class="btn btn-primary btn-lg">Create</button>
			</div>
		</div>
	</div>
</div>
<script>
	/*
$(function() """),format.raw/*95.14*/("""{"""),format.raw/*95.15*/("""
    $( ".draggable" ).draggable("""),format.raw/*96.33*/("""{"""),format.raw/*96.34*/("""

    // brings the item back to its place when dragging is over
    revert:true,

    // once the dragging starts, we decrease the opactiy of other items
    // Appending a class as we do that with CSS
    drag:function () """),format.raw/*103.22*/("""{"""),format.raw/*103.23*/("""
        $(this).addClass("active");
        $(this).closest("#product").addClass("active");
    """),format.raw/*106.5*/("""}"""),format.raw/*106.6*/(""",

    // removing the CSS classes once dragging is over.
    stop:function () """),format.raw/*109.22*/("""{"""),format.raw/*109.23*/("""
        $(this).removeClass("active").closest("#product").removeClass("active");
    """),format.raw/*111.5*/("""}"""),format.raw/*111.6*/("""
"""),format.raw/*112.1*/("""}"""),format.raw/*112.2*/(""");
    $( "#relate1" ).droppable("""),format.raw/*113.31*/("""{"""),format.raw/*113.32*/("""
      drop: function( event, ui ) """),format.raw/*114.35*/("""{"""),format.raw/*114.36*/("""
       var draggableId = ui.draggable.attr("id");
       var title = ui.draggable.find("a").text();
        $(this)
          .find("p")
            .html(title);
       console.log("Dropped " + draggableId + " into box 1");
      """),format.raw/*121.7*/("""}"""),format.raw/*121.8*/("""
    """),format.raw/*122.5*/("""}"""),format.raw/*122.6*/(""");
    $( "#relate2" ).droppable("""),format.raw/*123.31*/("""{"""),format.raw/*123.32*/("""
      drop: function( event, ui ) """),format.raw/*124.35*/("""{"""),format.raw/*124.36*/("""
       var draggableId = ui.draggable.attr("id");
       var title = ui.draggable.find("a").text();
        $(this)
          .find("p")
            .html(title);
       console.log("Dropped " + draggableId + " into box 2");
      """),format.raw/*131.7*/("""}"""),format.raw/*131.8*/("""
    """),format.raw/*132.5*/("""}"""),format.raw/*132.6*/(""");
  """),format.raw/*133.3*/("""}"""),format.raw/*133.4*/(""");
  */
  </script>
	</div-->
</div>

<script>
	function updateMessage() """),format.raw/*140.27*/("""{"""),format.raw/*140.28*/("""
		// Rather than reload whole page, just count the number of tiles that haven't been removed for now
		var found = 0;
		"""),_display_(Seq[Any](/*143.4*/for(d <- selectedDatasets) yield /*143.30*/ {_display_(Seq[Any](format.raw/*143.32*/("""
				if (document.getElementById(""""),_display_(Seq[Any](/*144.35*/d/*144.36*/.id)),format.raw/*144.39*/("""-tile")) found += 1;
			""")))})),format.raw/*145.5*/("""
		document.getElementById("select-count").innerHTML = "You currently have <b>"+found+"</b> dataset(s) selected."

		if (found==0) """),format.raw/*148.17*/("""{"""),format.raw/*148.18*/("""
			$("a#download-url").addClass("disabled");
			$("a#delete-url").addClass("disabled");
		"""),format.raw/*151.3*/("""}"""),format.raw/*151.4*/(""" else """),format.raw/*151.10*/("""{"""),format.raw/*151.11*/("""
			$("a#download-url").removeClass("disabled");
			$("a#delete-url").removeClass("disabled");
		"""),format.raw/*154.3*/("""}"""),format.raw/*154.4*/("""
	"""),format.raw/*155.2*/("""}"""),format.raw/*155.3*/("""
</script>

""")))})),format.raw/*158.2*/("""
"""))}
    }
    
    def render(selectedDatasets:scala.collection.mutable.ListBuffer[Dataset],ident:Option[securesocial.core.Identity],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(selectedDatasets)(ident,user)
    
    def f:((scala.collection.mutable.ListBuffer[Dataset]) => (Option[securesocial.core.Identity],Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (selectedDatasets) => (ident,user) => apply(selectedDatasets)(ident,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:30 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/selected.scala.html
                    HASH: 1c9594bd6fc68e69c17e50a14dd85af8004f6684
                    MATRIX: 683->1|943->144|971->169|1007->171|1040->196|1079->198|1129->213|1143->219|1220->275|1304->324|1318->330|1379->370|1463->419|1477->425|1538->465|1707->598|1732->614|1761->621|2030->855|2057->873|2097->875|2192->935|2231->965|2271->967|2966->1645|2979->1650|3018->1651|3708->2310|3977->2548|4089->2625|4131->2651|4171->2653|4211->2658|4228->2666|4354->2769|4389->2773|5338->3694|5367->3695|5428->3728|5457->3729|5710->3953|5740->3954|5865->4051|5894->4052|6002->4131|6032->4132|6146->4218|6175->4219|6204->4220|6233->4221|6295->4254|6325->4255|6389->4290|6419->4291|6679->4523|6708->4524|6741->4529|6770->4530|6832->4563|6862->4564|6926->4599|6956->4600|7216->4832|7245->4833|7278->4838|7307->4839|7340->4844|7369->4845|7471->4918|7501->4919|7659->5041|7702->5067|7743->5069|7815->5104|7826->5105|7852->5108|7909->5133|8069->5264|8099->5265|8218->5356|8247->5357|8282->5363|8312->5364|8437->5461|8466->5462|8496->5464|8525->5465|8570->5478
                    LINES: 20->1|24->1|26->4|27->5|27->5|27->5|28->6|28->6|28->6|29->7|29->7|29->7|30->8|30->8|30->8|35->13|35->13|35->13|41->19|41->19|41->19|45->23|45->23|45->23|55->33|55->33|55->33|65->43|72->50|77->55|77->55|77->55|78->56|78->56|78->56|79->57|117->95|117->95|118->96|118->96|125->103|125->103|128->106|128->106|131->109|131->109|133->111|133->111|134->112|134->112|135->113|135->113|136->114|136->114|143->121|143->121|144->122|144->122|145->123|145->123|146->124|146->124|153->131|153->131|154->132|154->132|155->133|155->133|162->140|162->140|165->143|165->143|165->143|166->144|166->144|166->144|167->145|170->148|170->148|173->151|173->151|173->151|173->151|176->154|176->154|177->155|177->155|180->158
                    -- GENERATED --
                */
            