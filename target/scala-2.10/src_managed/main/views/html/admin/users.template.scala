
package views.html.admin

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object users extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[List[String],List[User],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(configAdmins: List[String], users: List[User])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {
def /*8.2*/printRow/*8.10*/(u: User):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*8.23*/("""
    <tr id=""""),_display_(Seq[Any](/*9.14*/u/*9.15*/.id)),format.raw/*9.18*/("""">
        <td><a href=""""),_display_(Seq[Any](/*10.23*/routes/*10.29*/.Profile.viewProfileUUID(u.id))),format.raw/*10.59*/("""">"""),_display_(Seq[Any](/*10.62*/u/*10.63*/.fullName)),format.raw/*10.72*/("""</a></td>
        <td>"""),_display_(Seq[Any](/*11.14*/u/*11.15*/.email.getOrElse(""))),format.raw/*11.35*/("""</td>
        <td>"""),_display_(Seq[Any](/*12.14*/u/*12.15*/.identityId.providerId)),format.raw/*12.37*/("""</td>
        """),_display_(Seq[Any](/*13.10*/u/*13.11*/.lastLogin/*13.21*/ match/*13.27*/ {/*14.10*/case Some(logindate) =>/*14.33*/ {_display_(Seq[Any](format.raw/*14.35*/(""" 
        		<td>"""),_display_(Seq[Any](/*15.16*/dateFormatter(logindate))),format.raw/*15.40*/("""</td>
	        """)))}/*17.10*/case None =>/*17.22*/ {_display_(Seq[Any](format.raw/*17.24*/("""
    	    	<td>never</td>
    	    """)))}})),format.raw/*20.10*/("""
        """),_display_(Seq[Any](/*21.10*/if(user.fold("")(_.id.stringify) == u.id.stringify)/*21.61*/ {_display_(Seq[Any](format.raw/*21.63*/("""
            """),_display_(Seq[Any](/*22.14*/if(!(u.status==UserStatus.Inactive))/*22.50*/ {_display_(Seq[Any](format.raw/*22.52*/("""
                <td><input class="active" type="checkbox" id="active-"""),_display_(Seq[Any](/*23.71*/u/*23.72*/.id)),format.raw/*23.75*/("""" data-original=true disabled checked title="Can not change yourself"></td>
            """)))}/*24.15*/else/*24.20*/{_display_(Seq[Any](format.raw/*24.21*/("""
                <td><input class="active" type="checkbox" id="active-"""),_display_(Seq[Any](/*25.71*/u/*25.72*/.id)),format.raw/*25.75*/("""" data-original=false disabled ></td>
            """)))})),format.raw/*26.14*/("""
        """)))}/*27.11*/else/*27.16*/{_display_(Seq[Any](format.raw/*27.17*/("""
            """),_display_(Seq[Any](/*28.14*/if(!(u.status==UserStatus.Inactive))/*28.50*/ {_display_(Seq[Any](format.raw/*28.52*/("""
                <td><input class="active" type="checkbox" id="active-"""),_display_(Seq[Any](/*29.71*/u/*29.72*/.id)),format.raw/*29.75*/("""" data-original=true checked></td>
            """)))}/*30.15*/else/*30.20*/{_display_(Seq[Any](format.raw/*30.21*/("""
                <td><input class="active" type="checkbox" id="active-"""),_display_(Seq[Any](/*31.71*/u/*31.72*/.id)),format.raw/*31.75*/("""" data-original=false></td>
            """)))})),format.raw/*32.14*/("""
        """)))})),format.raw/*33.10*/("""
        """),_display_(Seq[Any](/*34.10*/if(user.fold("")(_.id.stringify) == u.id.stringify)/*34.61*/ {_display_(Seq[Any](format.raw/*34.63*/("""
            <td><input type="checkbox" id="admin-"""),_display_(Seq[Any](/*35.51*/u/*35.52*/.id)),format.raw/*35.55*/("""" data-original=true checked disabled title="Can not change yourself"></td>
        """)))}/*36.11*/else/*36.16*/{_display_(Seq[Any](format.raw/*36.17*/("""
            """),_display_(Seq[Any](/*37.14*/if(configAdmins.contains(u.email.getOrElse("")))/*37.62*/ {_display_(Seq[Any](format.raw/*37.64*/("""
                <td><input type="checkbox" id="admin-"""),_display_(Seq[Any](/*38.55*/u/*38.56*/.id)),format.raw/*38.59*/("""" data-original=true checked disabled title="Specified in custom.conf"></td>
            """)))}/*39.15*/else/*39.20*/{_display_(Seq[Any](format.raw/*39.21*/("""
                """),_display_(Seq[Any](/*40.18*/if(u.status==UserStatus.Admin)/*40.48*/ {_display_(Seq[Any](format.raw/*40.50*/("""
                        <td><input type="checkbox" id="admin-"""),_display_(Seq[Any](/*41.63*/u/*41.64*/.id)),format.raw/*41.67*/("""" data-original=true checked></td>
                """)))}/*42.19*/else/*42.24*/{_display_(Seq[Any](format.raw/*42.25*/("""
                        <td><input type="checkbox" id="admin-"""),_display_(Seq[Any](/*43.63*/u/*43.64*/.id)),format.raw/*43.67*/("""" data-original=false></td>
                """)))})),format.raw/*44.18*/("""
            """)))})),format.raw/*45.14*/("""
        """)))})),format.raw/*46.10*/("""
    </tr>
""")))};def /*3.2*/dateFormatter/*3.15*/(date: java.util.Date) = {{
    val formatter = new java.text.SimpleDateFormat("MMM d, yyyy")
    formatter.format(date)
}};
Seq[Any](format.raw/*1.85*/("""

"""),format.raw/*6.2*/("""
    
"""),format.raw/*48.2*/("""

"""),_display_(Seq[Any](/*50.2*/main("Users")/*50.15*/ {_display_(Seq[Any](format.raw/*50.17*/("""
    <div class="page-header" xmlns="http://www.w3.org/1999/html">
        <h1>Users</h1>
    </div>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" href="#collapseInactive" aria-expanded="true" aria-controls="collapseInactive">
                        Inactive Users
                    </a>
                </h4>
            </div>
            <div id="collapseInactive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th class="col-md-3">Fullname</th>
                                <th class="col-md-3">Email</th>
                                <th class="col-md-2">Provider</th>
                                <th class="col-md-2">Last Login</th>
                                <th class="col-md-1">Active</th>
                                <th class="col-md-1">Admin</th>
                            </tr>
                        </thead>
                        <tbody id="inactive-users">
                        """),_display_(Seq[Any](/*78.26*/users/*78.31*/.filter(_.status==UserStatus.Inactive).map(printRow))),format.raw/*78.83*/("""
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" href="#collapseAdmins" aria-expanded="false" aria-controls="collapseAdmins">
                        Admins
                    </a>
                </h4>
            </div>
            <div id="collapseAdmins" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th class="col-md-3">Fullname</th>
                                <th class="col-md-3">Email</th>
                                <th class="col-md-2">Provider</th>
                                <th class="col-md-2">Last Login</th>
                                <th class="col-md-1">Active</th>
                                <th class="col-md-1">Admin</th>
                            </tr>
                        </thead>
                        <tbody id="admin-users">
                        """),_display_(Seq[Any](/*107.26*/users/*107.31*/.filter(u => u.status== UserStatus.Admin).map(printRow))),format.raw/*107.86*/("""
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" href="#collapseActive" aria-expanded="false" aria-controls="collapseActive">
                        Active Users
                    </a>
                </h4>
            </div>
            <div id="collapseActive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th class="col-md-3">Fullname</th>
                                <th class="col-md-3">Email</th>
                                <th class="col-md-2">Provider</th>
                                <th class="col-md-2">Last Login</th>
                                <th class="col-md-1">Active</th>
                                <th class="col-md-1">Admin</th>
                            </tr>
                        </thead>
                        <tbody id="active-users">
                        """),_display_(Seq[Any](/*136.26*/users/*136.31*/.filter(u => u.status==UserStatus.Active).map(printRow))),format.raw/*136.86*/("""
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <button class="btn btn-primary" onclick="return submitUsers();"><span class="glyphicon glyphicon-saved"></span> Submit</button>

    <br/>
    <br/>

    <script>
            $(".active").click(function() """),format.raw/*151.43*/("""{"""),format.raw/*151.44*/("""
                var input = $(this)[0];
                var parts = input.id.split("-", 2);
                $("#admin-" + parts[1]).prop('disabled', !input.checked);
            """),format.raw/*155.13*/("""}"""),format.raw/*155.14*/(""");
            var users = """),format.raw/*156.25*/("""{"""),format.raw/*156.26*/(""" active: [], inactive: [], admin: [], unadmin: [] """),format.raw/*156.76*/("""}"""),format.raw/*156.77*/(""";
            function submitUsers() """),format.raw/*157.36*/("""{"""),format.raw/*157.37*/("""

                var sendit = false;

                $(".active:not(:checked)").each(function() """),format.raw/*161.60*/("""{"""),format.raw/*161.61*/("""
                    var input = $(this)[0];
                    var parts = input.id.split("-", 2);
                    var admin = $("#admin-" + parts[1]);
                    admin.prop('checked', false);
                    admin.prop('disabled', true);
                """),format.raw/*167.17*/("""}"""),format.raw/*167.18*/(""");

                $("input[type=checkbox]").each(function() """),format.raw/*169.59*/("""{"""),format.raw/*169.60*/("""
                    var input = $(this)[0];
                    if (input.getAttribute("data-original") != input.checked.toString()) """),format.raw/*171.90*/("""{"""),format.raw/*171.91*/("""
                        sendit = true;
                        var parts = input.id.split("-", 2);
                        if (parts[0] == "admin") """),format.raw/*174.50*/("""{"""),format.raw/*174.51*/("""
                            if (input.checked) """),format.raw/*175.48*/("""{"""),format.raw/*175.49*/("""
                                users.admin.push(parts[1]);
                            """),format.raw/*177.29*/("""}"""),format.raw/*177.30*/(""" else """),format.raw/*177.36*/("""{"""),format.raw/*177.37*/("""
                                users.unadmin.push(parts[1]);
                            """),format.raw/*179.29*/("""}"""),format.raw/*179.30*/("""
                        """),format.raw/*180.25*/("""}"""),format.raw/*180.26*/(""" else if (parts[0] == "active") """),format.raw/*180.58*/("""{"""),format.raw/*180.59*/("""
                            if (input.checked) """),format.raw/*181.48*/("""{"""),format.raw/*181.49*/("""
                                users.active.push(parts[1]);
                            """),format.raw/*183.29*/("""}"""),format.raw/*183.30*/(""" else """),format.raw/*183.36*/("""{"""),format.raw/*183.37*/("""
                                users.inactive.push(parts[1]);
                            """),format.raw/*185.29*/("""}"""),format.raw/*185.30*/("""
                        """),format.raw/*186.25*/("""}"""),format.raw/*186.26*/(""" else """),format.raw/*186.32*/("""{"""),format.raw/*186.33*/("""
                            logger.error("Not sure what to do with " + parts[0]);
                        """),format.raw/*188.25*/("""}"""),format.raw/*188.26*/("""
                        input.setAttribute("data-original", input.checked.toString());
                    """),format.raw/*190.21*/("""}"""),format.raw/*190.22*/("""
                """),format.raw/*191.17*/("""}"""),format.raw/*191.18*/(""");

                if (sendit) """),format.raw/*193.29*/("""{"""),format.raw/*193.30*/("""
                    $.ajax("""),format.raw/*194.28*/("""{"""),format.raw/*194.29*/("""
                        url:  """"),_display_(Seq[Any](/*195.33*/api/*195.36*/.routes.Admin.users().url)),format.raw/*195.61*/("""",
                        data: JSON.stringify(users),
                        type: "POST",
                        contentType: "application/json"
                    """),format.raw/*199.21*/("""}"""),format.raw/*199.22*/(""").done(function () """),format.raw/*199.41*/("""{"""),format.raw/*199.42*/("""
                        users.active.forEach(function(u) """),format.raw/*200.58*/("""{"""),format.raw/*200.59*/("""
                            $('#active-users').append($('#'+u))
                        """),format.raw/*202.25*/("""}"""),format.raw/*202.26*/(""");
                        users.unadmin.forEach(function(u) """),format.raw/*203.59*/("""{"""),format.raw/*203.60*/("""
                            $('#active-users').append($('#'+u))
                        """),format.raw/*205.25*/("""}"""),format.raw/*205.26*/(""");
                        users.inactive.forEach(function(u) """),format.raw/*206.60*/("""{"""),format.raw/*206.61*/("""
                            $('#inactive-users').append($('#'+u))
                        """),format.raw/*208.25*/("""}"""),format.raw/*208.26*/(""");
                        users.admin.forEach(function(u) """),format.raw/*209.57*/("""{"""),format.raw/*209.58*/("""
                            $('#admin-users').append($('#'+u))
                        """),format.raw/*211.25*/("""}"""),format.raw/*211.26*/(""");
                        users = """),format.raw/*212.33*/("""{"""),format.raw/*212.34*/(""" active: [], inactive: [], admin: [], unadmin: [] """),format.raw/*212.84*/("""}"""),format.raw/*212.85*/(""";
                        notify("Users successfully updated.", "success", false, 5000);
                    """),format.raw/*214.21*/("""}"""),format.raw/*214.22*/(""").fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*214.70*/("""{"""),format.raw/*214.71*/("""
                        console.error("The following error occured: " + textStatus, errorThrown);
                        notify("Could not update users : " + errorThrown, "error");
                    """),format.raw/*217.21*/("""}"""),format.raw/*217.22*/(""");
                """),format.raw/*218.17*/("""}"""),format.raw/*218.18*/(""" else """),format.raw/*218.24*/("""{"""),format.raw/*218.25*/("""
                    notify("No changes made.", "success", 5000);
                """),format.raw/*220.17*/("""}"""),format.raw/*220.18*/("""

                return false;
            """),format.raw/*223.13*/("""}"""),format.raw/*223.14*/("""
    </script>
""")))})),format.raw/*225.2*/("""
"""))}
    }
    
    def render(configAdmins:List[String],users:List[User],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(configAdmins,users)(user)
    
    def f:((List[String],List[User]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (configAdmins,users) => (user) => apply(configAdmins,users)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/admin/users.scala.html
                    HASH: bb3729ae7d257d981d6b6b762f1f9a7d7b84981b
                    MATRIX: 630->1|790->229|806->237|899->250|948->264|957->265|981->268|1042->293|1057->299|1109->329|1148->332|1158->333|1189->342|1248->365|1258->366|1300->386|1355->405|1365->406|1409->428|1460->443|1470->444|1489->454|1504->460|1515->472|1547->495|1587->497|1640->514|1686->538|1721->564|1742->576|1782->578|1851->625|1897->635|1957->686|1997->688|2047->702|2092->738|2132->740|2239->811|2249->812|2274->815|2382->905|2395->910|2434->911|2541->982|2551->983|2576->986|2659->1037|2688->1048|2701->1053|2740->1054|2790->1068|2835->1104|2875->1106|2982->1177|2992->1178|3017->1181|3084->1230|3097->1235|3136->1236|3243->1307|3253->1308|3278->1311|3351->1352|3393->1362|3439->1372|3499->1423|3539->1425|3626->1476|3636->1477|3661->1480|3765->1566|3778->1571|3817->1572|3867->1586|3924->1634|3964->1636|4055->1691|4065->1692|4090->1695|4199->1786|4212->1791|4251->1792|4305->1810|4344->1840|4384->1842|4483->1905|4493->1906|4518->1909|4589->1962|4602->1967|4641->1968|4740->2031|4750->2032|4775->2035|4852->2080|4898->2094|4940->2104|4974->87|4995->100|5147->84|5175->222|5208->2116|5246->2119|5268->2132|5308->2134|6753->3543|6767->3548|6841->3600|8199->4921|8214->4926|8292->4981|9657->6309|9672->6314|9750->6369|10121->6711|10151->6712|10359->6891|10389->6892|10445->6919|10475->6920|10554->6970|10584->6971|10650->7008|10680->7009|10807->7107|10837->7108|11140->7382|11170->7383|11261->7445|11291->7446|11454->7580|11484->7581|11662->7730|11692->7731|11769->7779|11799->7780|11917->7869|11947->7870|11982->7876|12012->7877|12132->7968|12162->7969|12216->7994|12246->7995|12307->8027|12337->8028|12414->8076|12444->8077|12563->8167|12593->8168|12628->8174|12658->8175|12779->8267|12809->8268|12863->8293|12893->8294|12928->8300|12958->8301|13094->8408|13124->8409|13261->8517|13291->8518|13337->8535|13367->8536|13428->8568|13458->8569|13515->8597|13545->8598|13615->8631|13628->8634|13676->8659|13875->8829|13905->8830|13953->8849|13983->8850|14070->8908|14100->8909|14218->8998|14248->8999|14338->9060|14368->9061|14486->9150|14516->9151|14607->9213|14637->9214|14757->9305|14787->9306|14875->9365|14905->9366|15022->9454|15052->9455|15116->9490|15146->9491|15225->9541|15255->9542|15393->9651|15423->9652|15500->9700|15530->9701|15762->9904|15792->9905|15840->9924|15870->9925|15905->9931|15935->9932|16046->10014|16076->10015|16149->10059|16179->10060|16227->10076
                    LINES: 20->1|22->8|22->8|24->8|25->9|25->9|25->9|26->10|26->10|26->10|26->10|26->10|26->10|27->11|27->11|27->11|28->12|28->12|28->12|29->13|29->13|29->13|29->13|29->14|29->14|29->14|30->15|30->15|31->17|31->17|31->17|33->20|34->21|34->21|34->21|35->22|35->22|35->22|36->23|36->23|36->23|37->24|37->24|37->24|38->25|38->25|38->25|39->26|40->27|40->27|40->27|41->28|41->28|41->28|42->29|42->29|42->29|43->30|43->30|43->30|44->31|44->31|44->31|45->32|46->33|47->34|47->34|47->34|48->35|48->35|48->35|49->36|49->36|49->36|50->37|50->37|50->37|51->38|51->38|51->38|52->39|52->39|52->39|53->40|53->40|53->40|54->41|54->41|54->41|55->42|55->42|55->42|56->43|56->43|56->43|57->44|58->45|59->46|61->3|61->3|65->1|67->6|69->48|71->50|71->50|71->50|99->78|99->78|99->78|128->107|128->107|128->107|157->136|157->136|157->136|172->151|172->151|176->155|176->155|177->156|177->156|177->156|177->156|178->157|178->157|182->161|182->161|188->167|188->167|190->169|190->169|192->171|192->171|195->174|195->174|196->175|196->175|198->177|198->177|198->177|198->177|200->179|200->179|201->180|201->180|201->180|201->180|202->181|202->181|204->183|204->183|204->183|204->183|206->185|206->185|207->186|207->186|207->186|207->186|209->188|209->188|211->190|211->190|212->191|212->191|214->193|214->193|215->194|215->194|216->195|216->195|216->195|220->199|220->199|220->199|220->199|221->200|221->200|223->202|223->202|224->203|224->203|226->205|226->205|227->206|227->206|229->208|229->208|230->209|230->209|232->211|232->211|233->212|233->212|233->212|233->212|235->214|235->214|235->214|235->214|238->217|238->217|239->218|239->218|239->218|239->218|241->220|241->220|244->223|244->223|246->225
                    -- GENERATED --
                */
            