
package views.html.admin

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object customize extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[String,String,String,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(theme: String, displayName: String, welcomeMessage: String, googleAnalytics: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.123*/("""
"""),_display_(Seq[Any](/*2.2*/main("Customize")/*2.19*/ {_display_(Seq[Any](format.raw/*2.21*/("""
    <div class="page-header">
        <h1>Customize</h1>
    </div>

    <div class="row">
        <div class="col-md-12">
            This page allows you to change the look and feel of Clowder. You can change the theme used,
            common icons used as well as change the name, welcome message and other items show on every
            page.
        </div>
    </div>
    <br/>

    <form role="form-horizontal">
        <legend>Change look and feel</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="themeSelect">Select a Theme</label>
                    <select id="themeSelect" class="form-control">
                    """),_display_(Seq[Any](/*23.22*/for(t <- services.AppConfiguration.themes) yield /*23.64*/ {_display_(Seq[Any](format.raw/*23.66*/("""
                        <option value=""""),_display_(Seq[Any](/*24.41*/t)),format.raw/*24.42*/("""">"""),_display_(Seq[Any](/*24.45*/t/*24.46*/.replaceAll(".min.css", ""))),format.raw/*24.73*/("""</option>
                    """)))})),format.raw/*25.22*/("""
                    </select>
                </div>
            </div>
        </div>

        <legend>Welcome text</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="displayNameField">Display name</label>
                    <input class="form-control" id="displayNameField" name="displayNameField" maxlength="20" type="text" value=""""),_display_(Seq[Any](/*36.130*/(displayName))),format.raw/*36.143*/("""">
                    <span id="displayNameField_error" style="display:none;">Must have a display name</span>
                </div>

                <div class="form-group">
                    <label for="welcomingField">Welcoming message</label>
                    <textarea class="form-control" name="welcomingField" id="welcomingField" rows="4" style="resize: none">"""),_display_(Seq[Any](/*42.125*/(welcomeMessage))),format.raw/*42.141*/("""</textarea>
                </div>
            </div>
        </div>

        <legend>Tracking</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="googleAnalytics">Google Analytics Code</label>
                    <input class="form-control" id="googleAnalytics" name="googleAnalytics" type="text" value=""""),_display_(Seq[Any](/*52.113*/(googleAnalytics))),format.raw/*52.130*/(""""></input>
                </div>
            </div>
        </div>


        <legend>Logos</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="GLOBALfaviconOpt">Favicon used</label>
                    """),_display_(Seq[Any](/*63.22*/logoSelect("GLOBAL", "favicon", Some("images/favicon.png"), None))),format.raw/*63.87*/("""
                </div>
                <div class="form-group">
                    <label for="GLOBALlogoOpt">Site Logo</label>
                    """),_display_(Seq[Any](/*67.22*/logoSelect("GLOBAL", "logo", None, Some("Show display name")))),format.raw/*67.83*/("""
                </div>
            </div>
        </div>

        <br/>
        <button class="btn btn-primary" onclick="return submitForm();"><span class="glyphicon glyphicon-saved"></span> Submit</button>
        <br/>
    </form>
    <br/>

    <script src=""""),_display_(Seq[Any](/*78.19*/routes/*78.25*/.Assets.at("javascripts/maxlength.min.js"))),format.raw/*78.67*/(""""></script>

    <script language="javascript">
        $(function() """),format.raw/*81.22*/("""{"""),format.raw/*81.23*/("""
            $("#themeSelect").val('"""),_display_(Seq[Any](/*82.37*/theme)),format.raw/*82.42*/("""');

            $('#displayNameField').maxlength("""),format.raw/*84.46*/("""{"""),format.raw/*84.47*/("""
                alwaysShow: true,
                threshold: 10,
                validate: true,
                placement: 'top'
            """),format.raw/*89.13*/("""}"""),format.raw/*89.14*/(""");
        """),format.raw/*90.9*/("""}"""),format.raw/*90.10*/(""");

        function submitForm() """),format.raw/*92.31*/("""{"""),format.raw/*92.32*/("""
            var displayName = $("#displayNameField").val().trim();
            if (displayName == "") """),format.raw/*94.36*/("""{"""),format.raw/*94.37*/("""
                $("#displayNameField_error").css("display","inline");
                return false;
            """),format.raw/*97.13*/("""}"""),format.raw/*97.14*/(""" else """),format.raw/*97.20*/("""{"""),format.raw/*97.21*/("""
                $("#displayNameField_error").css("display","none");
            """),format.raw/*99.13*/("""}"""),format.raw/*99.14*/("""
            var welcomeMessage = $("#welcomingField").val().trim();
            var theme = $("#themeSelect").val().trim();
            var googleAnalytics = $("#googleAnalytics").val().trim();

            $.ajax("""),format.raw/*104.20*/("""{"""),format.raw/*104.21*/("""
                url:  """"),_display_(Seq[Any](/*105.25*/api/*105.28*/.routes.Admin.updateConfiguration)),format.raw/*105.61*/("""",
                data: JSON.stringify("""),format.raw/*106.38*/("""{"""),format.raw/*106.39*/("""
                    displayName: displayName,
                    welcomeMessage: welcomeMessage,
                    theme: theme,
                    googleAnalytics: googleAnalytics
                """),format.raw/*111.17*/("""}"""),format.raw/*111.18*/("""),
                type: "POST",
                contentType: "application/json"
            """),format.raw/*114.13*/("""}"""),format.raw/*114.14*/(""").done(function() """),format.raw/*114.32*/("""{"""),format.raw/*114.33*/("""
                uploadGLOBALlogoFile(function() """),format.raw/*115.49*/("""{"""),format.raw/*115.50*/("""
                    uploadGLOBALfaviconFile(function() """),format.raw/*116.56*/("""{"""),format.raw/*116.57*/("""
                        location.reload();
                    """),format.raw/*118.21*/("""}"""),format.raw/*118.22*/(""");
                """),format.raw/*119.17*/("""}"""),format.raw/*119.18*/(""");
            """),format.raw/*120.13*/("""}"""),format.raw/*120.14*/(""").fail(function(jqXHR) """),format.raw/*120.37*/("""{"""),format.raw/*120.38*/("""
                console.error("The following error occured: " + jqXHR.responseText);
                notify("The application preferences was not updated", "error");
            """),format.raw/*123.13*/("""}"""),format.raw/*123.14*/(""");

            return false;
        """),format.raw/*126.9*/("""}"""),format.raw/*126.10*/("""
    </script>
""")))})),format.raw/*128.2*/("""
"""))}
    }
    
    def render(theme:String,displayName:String,welcomeMessage:String,googleAnalytics:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(theme,displayName,welcomeMessage,googleAnalytics)(user)
    
    def f:((String,String,String,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (theme,displayName,welcomeMessage,googleAnalytics) => (user) => apply(theme,displayName,welcomeMessage,googleAnalytics)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/admin/customize.scala.html
                    HASH: 9a2b6e72d28da01432f4adf527838330f900f800
                    MATRIX: 638->1|854->122|890->124|915->141|954->143|1715->868|1773->910|1813->912|1890->953|1913->954|1952->957|1962->958|2011->985|2074->1016|2540->1445|2576->1458|2987->1832|3026->1848|3460->2245|3500->2262|3831->2557|3918->2622|4105->2773|4188->2834|4487->3097|4502->3103|4566->3145|4663->3214|4692->3215|4765->3252|4792->3257|4870->3307|4899->3308|5070->3451|5099->3452|5137->3463|5166->3464|5228->3498|5257->3499|5388->3602|5417->3603|5558->3716|5587->3717|5621->3723|5650->3724|5759->3805|5788->3806|6032->4021|6062->4022|6124->4047|6137->4050|6193->4083|6262->4123|6292->4124|6523->4326|6553->4327|6675->4420|6705->4421|6752->4439|6782->4440|6860->4489|6890->4490|6975->4546|7005->4547|7098->4611|7128->4612|7176->4631|7206->4632|7250->4647|7280->4648|7332->4671|7362->4672|7569->4850|7599->4851|7665->4889|7695->4890|7743->4906
                    LINES: 20->1|23->1|24->2|24->2|24->2|45->23|45->23|45->23|46->24|46->24|46->24|46->24|46->24|47->25|58->36|58->36|64->42|64->42|74->52|74->52|85->63|85->63|89->67|89->67|100->78|100->78|100->78|103->81|103->81|104->82|104->82|106->84|106->84|111->89|111->89|112->90|112->90|114->92|114->92|116->94|116->94|119->97|119->97|119->97|119->97|121->99|121->99|126->104|126->104|127->105|127->105|127->105|128->106|128->106|133->111|133->111|136->114|136->114|136->114|136->114|137->115|137->115|138->116|138->116|140->118|140->118|141->119|141->119|142->120|142->120|142->120|142->120|145->123|145->123|148->126|148->126|150->128
                    -- GENERATED --
                */
            