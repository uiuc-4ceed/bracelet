
package views.html.admin

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tos extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[String,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(tosText: String, tosHtml: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.72*/("""

"""),_display_(Seq[Any](/*3.2*/main("Terms of Service")/*3.26*/ {_display_(Seq[Any](format.raw/*3.28*/("""
    <div class="page-header">
        <h1>Terms of Service</h1>
    </div>

    <div class="row">
        <div class="col-md-12">
            This page allows you to change the Terms of Service. When you save the Terms of Service all users
            will need to agreee to the new terms of service. If you leave the text blank it will default to
            the default Terms of Service as provided by Clowder. You can use @@NAME in the text which will
            be replaced with the server name (currently """),_display_(Seq[Any](/*13.58*/services/*13.66*/.AppConfiguration.getDisplayName)),format.raw/*13.98*/(""").
        </div>
    </div>
    <br/>

    <form role="form-horizontal">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="tosText">Terms Of Service</label>
                    <textarea class="form-control" id="tosText" rows="20">"""),_display_(Seq[Any](/*23.76*/(tosText))),format.raw/*23.85*/("""</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="tosHtml">Format text as HTML</label>
                    <input type="checkbox" class="form-control" id="tosHTML" """),_display_(Seq[Any](/*31.79*/(tosHtml))),format.raw/*31.88*/(""" />
                </div>
            </div>
        </div>

        <br/>
        <button class="btn btn-primary" onclick="return submitForm();"><span class="glyphicon glyphicon-saved"></span> Submit</button>
        <br/>
    </form>

    <script language="javascript">
            function submitForm() """),format.raw/*42.35*/("""{"""),format.raw/*42.36*/("""
                var tosHtml = $("#tosHTML").is(':checked');
                var tosText = $("#tosText").val().trim();

                $.ajax("""),format.raw/*46.24*/("""{"""),format.raw/*46.25*/("""
                    url:  """"),_display_(Seq[Any](/*47.29*/api/*47.32*/.routes.Admin.updateConfiguration)),format.raw/*47.65*/("""",
                    data: JSON.stringify("""),format.raw/*48.42*/("""{"""),format.raw/*48.43*/("""
                        tosHtml: tosHtml,
                        tosText: tosText
                    """),format.raw/*51.21*/("""}"""),format.raw/*51.22*/("""),
                    type: "POST",
                    contentType: "application/json"
                """),format.raw/*54.17*/("""}"""),format.raw/*54.18*/(""").done(function() """),format.raw/*54.36*/("""{"""),format.raw/*54.37*/("""
                    notify("The terms of service have been updated", "success");
                """),format.raw/*56.17*/("""}"""),format.raw/*56.18*/(""").fail(function(jqXHR) """),format.raw/*56.41*/("""{"""),format.raw/*56.42*/("""
                    console.error("The following error occured: " + jqXHR.responseText);
                    notify("The terms of service are not updated", "error");
                """),format.raw/*59.17*/("""}"""),format.raw/*59.18*/(""");

                return false;
            """),format.raw/*62.13*/("""}"""),format.raw/*62.14*/("""
    </script>
""")))})),format.raw/*64.2*/("""
"""))}
    }
    
    def render(tosText:String,tosHtml:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(tosText,tosHtml)(user)
    
    def f:((String,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (tosText,tosHtml) => (user) => apply(tosText,tosHtml)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/admin/tos.scala.html
                    HASH: fc96e870a1c33538e286f7aeeb6d834d88b9282e
                    MATRIX: 618->1|782->71|819->74|851->98|890->100|1439->615|1456->623|1510->655|1864->973|1895->982|2250->1301|2281->1310|2616->1617|2645->1618|2816->1761|2845->1762|2910->1791|2922->1794|2977->1827|3049->1871|3078->1872|3210->1976|3239->1977|3372->2082|3401->2083|3447->2101|3476->2102|3602->2200|3631->2201|3682->2224|3711->2225|3922->2408|3951->2409|4025->2455|4054->2456|4101->2472
                    LINES: 20->1|23->1|25->3|25->3|25->3|35->13|35->13|35->13|45->23|45->23|53->31|53->31|64->42|64->42|68->46|68->46|69->47|69->47|69->47|70->48|70->48|73->51|73->51|76->54|76->54|76->54|76->54|78->56|78->56|78->56|78->56|81->59|81->59|84->62|84->62|86->64
                    -- GENERATED --
                */
            