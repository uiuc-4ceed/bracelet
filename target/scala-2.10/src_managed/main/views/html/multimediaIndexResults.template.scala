
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object multimediaIndexResults extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[scala.collection.mutable.ArrayBuffer[String],String,String,Integer,scala.collection.mutable.HashMap[String, scala.collection.mutable.ArrayBuffer[scala.Tuple4[String, String, Double, String]]],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(keysArray:scala.collection.mutable.ArrayBuffer[String],name: String, id: String, size:Integer, results: scala.collection.mutable.HashMap[String, scala.collection.mutable.ArrayBuffer[(String,String,Double,String)]]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import models.IncrementCounter


Seq[Any](format.raw/*1.217*/(""" 
"""),format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*4.2*/main("Search Results")/*4.24*/ {_display_(Seq[Any](format.raw/*4.26*/("""
	<div class="page-header">
		<h1>Search Results For <medium>"""),_display_(Seq[Any](/*6.35*/name)),format.raw/*6.39*/("""</medium></h1>
	</div>
	"""),_display_(Seq[Any](/*8.3*/if(size == 0)/*8.16*/ {_display_(Seq[Any](format.raw/*8.18*/("""
	<div class="row">
		<div class="col-md-12">
			No results found. Sorry!
		</div>
	</div>
	""")))})),format.raw/*14.3*/("""
	
<div class="container" > 
	<div class="row">
	
	"""),_display_(Seq[Any](/*19.3*/for(index<-keysArray) yield /*19.24*/{_display_(Seq[Any](format.raw/*19.25*/("""
	<div class="col-md-4">
		
	<table id='tablec' class="table table-bordered table-hover">
	<thead>
		<tr>
	    <th colspan="3"><a href="http://localhost:8080/api/v1/index/"""),_display_(Seq[Any](/*25.67*/index)),format.raw/*25.72*/("""">IndexID: """),_display_(Seq[Any](/*25.84*/index)),format.raw/*25.89*/("""</a></th>
		</tr>
		<tr>
		<th id='rank'>Rank</th>
		<th id='ID'>ID</th>
		<th id='prox'>Proximity</th>
		</tr>
	</thead>
	<tbody>
		"""),_display_(Seq[Any](/*34.4*/results/*34.11*/.get(index).map/*34.26*/ { case (list) =>_display_(Seq[Any](format.raw/*34.43*/("""
			"""),_display_(Seq[Any](/*35.5*/defining(new IncrementCounter)/*35.35*/{ i=>_display_(Seq[Any](format.raw/*35.40*/("""
		   <tr>
		 	
			"""),_display_(Seq[Any](/*38.5*/list/*38.9*/.map/*38.13*/ { case (id,link, distance, fname) =>_display_(Seq[Any](format.raw/*38.50*/("""
			<tr>
			 <td>"""),_display_(Seq[Any](/*40.10*/{i.count+=1;i.count})),format.raw/*40.30*/("""</td> 
			<td><a href="""),_display_(Seq[Any](/*41.17*/routes/*41.23*/.Files.file(UUID(id)))),format.raw/*41.44*/("""> """),_display_(Seq[Any](/*41.47*/fname)),format.raw/*41.52*/("""</a></td>
			<td>"""),_display_(Seq[Any](/*42.9*/distance)),format.raw/*42.17*/("""</td>
			</tr>	
			""")))})),format.raw/*44.5*/("""
			
		""")))})),format.raw/*46.4*/("""
	""")))})),format.raw/*47.3*/("""	
    </tbody>
    </table>
	</div>
	
	""")))})),format.raw/*52.3*/("""
	</div>
	</div>
	""")))})),format.raw/*55.3*/("""
	"""))}
    }
    
    def render(keysArray:scala.collection.mutable.ArrayBuffer[String],name:String,id:String,size:Integer,results:scala.collection.mutable.HashMap[String, scala.collection.mutable.ArrayBuffer[scala.Tuple4[String, String, Double, String]]]): play.api.templates.HtmlFormat.Appendable = apply(keysArray,name,id,size,results)
    
    def f:((scala.collection.mutable.ArrayBuffer[String],String,String,Integer,scala.collection.mutable.HashMap[String, scala.collection.mutable.ArrayBuffer[scala.Tuple4[String, String, Double, String]]]) => play.api.templates.HtmlFormat.Appendable) = (keysArray,name,id,size,results) => apply(keysArray,name,id,size,results)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/multimediaIndexResults.scala.html
                    HASH: 0ce6bd0e077d89e340e16cdfa724ef86f51756c7
                    MATRIX: 789->1|1130->216|1158->250|1194->252|1224->274|1263->276|1360->338|1385->342|1444->367|1465->380|1504->382|1628->475|1715->527|1752->548|1791->549|1999->721|2026->726|2074->738|2101->743|2270->877|2286->884|2310->899|2365->916|2405->921|2444->951|2487->956|2542->976|2554->980|2567->984|2642->1021|2696->1039|2738->1059|2797->1082|2812->1088|2855->1109|2894->1112|2921->1117|2974->1135|3004->1143|3055->1163|3094->1171|3128->1174|3199->1214|3249->1233
                    LINES: 20->1|24->1|25->3|26->4|26->4|26->4|28->6|28->6|30->8|30->8|30->8|36->14|41->19|41->19|41->19|47->25|47->25|47->25|47->25|56->34|56->34|56->34|56->34|57->35|57->35|57->35|60->38|60->38|60->38|60->38|62->40|62->40|63->41|63->41|63->41|63->41|63->41|64->42|64->42|66->44|68->46|69->47|74->52|77->55
                    -- GENERATED --
                */
            