
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object advancedsearch extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/():play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

implicit def /*3.2*/implicitFieldConstructor/*3.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.4*/("""
"""),format.raw/*3.75*/("""

"""),_display_(Seq[Any](/*5.2*/main("Clowder")/*5.17*/ {_display_(Seq[Any](format.raw/*5.19*/("""

<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*7.46*/routes/*7.52*/.Assets.at("stylesheets/dragdrop.css"))),format.raw/*7.90*/("""">

<div class="page-header">
    <h1>Multimedia Advanced Search</h1>
  </div>


<div class="row">
      
        <div class="col-md-12">
			
	"""),_display_(Seq[Any](/*18.3*/form(action = routes.Files.upload, 'enctype -> "multipart/form-data", 'class -> "form-horizontal")/*18.101*/ {_display_(Seq[Any](format.raw/*18.103*/("""
	    <fieldset>
	      <legend>Select an Image and Search</legend>
	     <input type="file" id="query" name="File">
	    </fieldset>
        <div class="form-actions">
			<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
        </div>
	  """)))})),format.raw/*26.5*/("""
	  
"""),format.raw/*30.17*/("""
	      <legend> Drop an Image and Search </legend>
	      <br>
	      <div id="filedrag"> Drop Box</div>
	      <output id="list"></output>
"""),format.raw/*35.21*/("""
	  <div class="form-actions">
		  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
        </div>
	 """),format.raw/*39.11*/("""
	  
   """),_display_(Seq[Any](/*41.5*/form(action = routes.Search.searchbyURL(""), 'enctype -> "multipart/form-data", 'class -> "form-horizontal")/*41.113*/ {_display_(Seq[Any](format.raw/*41.115*/("""
	    <fieldset>
	      <br>
	 	    <label><h4>Paste your query image URL</h4></label>
    	<input type="text" placeholder="Paste your URL here" name="query">
	    <br>
	    <br>
	     <label><h4>Filter by</h4></label>	
	    <select id="tag" onchange="">
		<option value="invalid">--None--</option>
		<option value="">People</option>
		<option value="">Nature</option>
		<option value=""> Technology</option>
		<option value="">Healthcare</option>
		</select>
		</fieldset>	
		<br>
		<br>
	   <div style="text-align: center;">
		   <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
       </div>
                     
	  """)))})),format.raw/*63.5*/("""
	  		
			   
 """),_display_(Seq[Any](/*66.3*/form(action = routes.Files.upload, 'enctype -> "multipart/form-data", 'class -> "form-horizontal")/*66.101*/ {_display_(Seq[Any](format.raw/*66.103*/("""
	    <fieldset>
	   	<br>
		<div>
		<legend><h4>Choose your methods and there weights</h4></legend>	
		<label class="checkbox">
		 <input type="checkbox"> Color Histogram
		 <input type="text" placeholder="weight">
		 </label>
		 <label class="checkbox">
       	 <input type="checkbox"> Texture
       	 <input type="text" placeholder="weight">
     	 </label>
     	</div>
	   	<br>
	   	<br>
	   	<div>
	   	<legend><h4>Choose your methods and their weights</h4></legend>	
	   	<table class="table table-bordered">
	   	<thead>
    	<tr>
      	<th>Select</th>
     	 <th>Methods</th>
     	 <th>Weight</th>
    	</tr>
  		</thead>
  		<tbody>
    	<tr>
      	<td><label class="checkbox">
       	 <input type="checkbox"> 
       	 <label>
       	 </td>
      	<td>Color Histogram</td>
      	<td><input type="text" placeholder="weight"> </td>
   		 </tr>
   		 <tr>
      	<td><label class="checkbox">
       	 <input type="checkbox"> 
       	 <label>
       	 </td>
      	<td>Texture</td>
      	<td><input type="text" placeholder="weight"> </td>
   		 </tr>
  		</tbody>
	   	</table>
	   	</div>
	   	<br>
		 <label><h4>Keywords</h4></label>	
		<input type="text" placeholder="Type your keywords here">
		</fieldset>	
		<br>
		<br>
	   <div style="text-align: center;">
		   <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
       </div>
       
       <br>
       <br>
      <div>
       <h3> Sketch Your Image and Search</h3>
	</div>
	  """)))})),format.raw/*127.5*/("""
	  	
	</div>
      
	</div>		
    <script src=""""),_display_(Seq[Any](/*132.19*/routes/*132.25*/.Assets.at("javascripts/file-browser/views/dragdrop.js"))),format.raw/*132.81*/("""" type="text/javascript"></script>
  
""")))})))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:30 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/advancedsearch.scala.html
                    HASH: 35252695c9b76e07bf2630caa67418d05577bb29
                    MATRIX: 589->1|693->22|725->46|803->3|831->95|868->98|891->113|930->115|1012->162|1026->168|1085->206|1264->350|1372->448|1413->450|1746->752|1779->854|1948->1015|2139->1186|2183->1195|2301->1303|2342->1305|3056->1988|3107->2004|3215->2102|3256->2104|4802->3618|4888->3667|4904->3673|4983->3729
                    LINES: 20->1|23->3|23->3|24->1|25->3|27->5|27->5|27->5|29->7|29->7|29->7|40->18|40->18|40->18|48->26|50->30|55->35|59->39|61->41|61->41|61->41|83->63|86->66|86->66|86->66|147->127|152->132|152->132|152->132
                    -- GENERATED --
                */
            