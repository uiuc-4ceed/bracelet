
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object dtsExtension extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[String,String,String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(baseUrl: String, hostname: String, extensionHostUrl: String):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import collection.JavaConverters._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.63*/("""
"""),format.raw/*4.75*/("""
"""),_display_(Seq[Any](/*5.2*/main("Extension")/*5.19*/ {_display_(Seq[Any](format.raw/*5.21*/("""
<center>
    <h1>Data Tilling Service (DTS)</h1>
    <h2 style="color:gray;">Download and Drag & Drop the extension to Chrome->Extension Settings</h2>
    <a href=""""),_display_(Seq[Any](/*9.15*/{extensionHostUrl + "/chrome/dtsextension.crx"})),format.raw/*9.62*/("""">
      <img src=""""),_display_(Seq[Any](/*10.18*/baseUrl)),format.raw/*10.25*/("""/assets/javascripts/DTSbookmarklet/browndog-large-transparent.png" width="200" alt="DTS">
    </a>
</center>
""")))})),format.raw/*13.2*/("""
"""))}
    }
    
    def render(baseUrl:String,hostname:String,extensionHostUrl:String): play.api.templates.HtmlFormat.Appendable = apply(baseUrl,hostname,extensionHostUrl)
    
    def f:((String,String,String) => play.api.templates.HtmlFormat.Appendable) = (baseUrl,hostname,extensionHostUrl) => apply(baseUrl,hostname,extensionHostUrl)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/dtsExtension.scala.html
                    HASH: 88bb104a72c1cc5e8ca8299b6846acf7862fb703
                    MATRIX: 608->1|807->117|839->141|918->62|946->190|982->192|1007->209|1046->211|1247->377|1315->424|1371->444|1400->451|1541->561
                    LINES: 20->1|25->4|25->4|26->1|27->4|28->5|28->5|28->5|32->9|32->9|33->10|33->10|36->13
                    -- GENERATED --
                */
            