
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object newTemplate extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[String,List[models.ProjectSpace],Boolean,Boolean,Option[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(errorString: String, spaces: List[models.ProjectSpace], isNameRequired: Boolean, isDescriptionRequired: Boolean, spaceId: Option[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.203*/("""

"""),format.raw/*4.75*/("""

"""),_display_(Seq[Any](/*6.2*/main("Create Template")/*6.25*/ {_display_(Seq[Any](format.raw/*6.27*/("""
  <!-- Custom items for the create collection workflow -->
<script src=""""),_display_(Seq[Any](/*8.15*/routes/*8.21*/.Assets.at("javascripts/vocabulary-create.js"))),format.raw/*8.67*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*9.15*/routes/*9.21*/.Assets.at("javascripts/template-create.js"))),format.raw/*9.65*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*10.15*/routes/*10.21*/.Assets.at("javascripts/t2c2CreateTemplate.js"))),format.raw/*10.68*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*11.15*/routes/*11.21*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*11.66*/("""" language="javascript"></script>
<script type="text/javascript" language="javascript">
    //Global so that the javascript for the collection creation can reference this.
    var isNameRequired = """),_display_(Seq[Any](/*14.27*/isNameRequired)),format.raw/*14.41*/(""";
    var isDescRequired = """),_display_(Seq[Any](/*15.27*/isDescriptionRequired)),format.raw/*15.48*/(""";
    var isKeysRequired = true;
</script>
<script src=""""),_display_(Seq[Any](/*18.15*/routes/*18.21*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*18.63*/("""" type="text/javascript"></script>
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*19.31*/routes/*19.37*/.Assets.at("stylesheets/chosen.css"))),format.raw/*19.73*/("""">

<div class="row">
    <div class="col-md-12">

        <div class="page-header">
            <h1>Templates</h1>
        </div>
        <div>
            <span id="status" class="success hiddencomplete alert alert-success" role="alert">A Status Message</span>
            """),_display_(Seq[Any](/*29.14*/if(errorString != null)/*29.37*/{_display_(Seq[Any](format.raw/*29.38*/("""
            <span class="error alert alert-danger" id="messageerror">"""),_display_(Seq[Any](/*30.71*/errorString)),format.raw/*30.82*/("""</span>
            """)))}/*31.14*/else/*31.18*/{_display_(Seq[Any](format.raw/*31.19*/("""
            <span class="error hiddencomplete alert alert-danger" id="messageerror">An Error Message</span>
            """)))})),format.raw/*33.14*/("""
        </div>

        <div class="row">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active" ><a href="#createTemplate" aria-controls="createTemplate" role="tab" data-toggle="tab" class="createTab"><b>Create Template</b></a></li>
                <li role="presentation" class="updateTab" ><a href="#updateTemplate" aria-controls="updateTemplate" role="tab" data-toggle="tab"><b>Update Template</b></a></li>
                <li role="presentation" class="deleteTab"><a href="#deleteTemplate" aria-controls="deleteTemplate" role="tab" data-toggle="tab"><b>Delete Template</b></a></li>                       
            </ul>
        </div>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="createTemplate">
            <h3> Create New Template</h3>

            <div class="top-padding">
                <form id="formCreateTemplate" method="get" action="">
                <div class="form-group">
                    <div class="showTemplates">
                        <label>Load another template to start with (optional):</label><br /><br />


                        <label>My Templates:</label><br />
                        <select class="form-control templates">
                        </select>
                        <br />

                        <label>Global Templates:</label><br />
                        <select class="form-control globalTemplates">
                        </select>
                        <br />
                    </div>
                    <label>Choose a name for your template:</label>
                    <input type="text" class="form-control datasetName" placeholder="<Example> Sample Name, PECVD Oxide, Diffusion" required><br />
                    <label>Create tags to describe your template: (optional)</label>                
                    <span><a style="font-height:12px;" href="#" data-toggle="popover" title="Template Tags" data-trigger="hover" data-content="Tags allow users to search personal and global templates through descriptive tags." title=" ">
                    <span class="glyphicon glyphicon-question-sign"></a></span> 
                    <br />
                    <input type="text" class="form-control tagName" placeholder="<Example> SEM, Diffusion"><br />
                    <input type="button" value="Add New Field" class="btn btn-success btnAdd"/>     
                    <button class="btn btn-danger clearTemplate" type="button">Clear Template</button>
                    <br /><br />
                    <label>Share this template with others?</label> <input type="checkbox" class="checkShareTemplate"><br />

                    <div class="metaDataSettings">                    
                    </div>  
                    <div class="templateData">
                        <!--Template boxes will be added here -->
                        <br />
                    </div>
                    <br />
                    <div class="form-group" id="btnTemplate">   
                        <input type="button" value="Create Template" class="btn btn-success createTemplate form-control">
                    </div>
                </div>
                </form>
            </div>    
        </div>
        <div role="tabpanel" class="tab-pane" id="updateTemplate">
            <h3> Update Template</h3>

            <div class="top-padding">
                <form id="formUpdateTemplate" action="">
                <div class="form-group">
                    <div class="showTemplates">
                        <label>My Templates:</label><br />
                        <select class="form-control templates">
                        </select>
                        <br />
                    </div>
                    <div class="updatePanel">
                        <label>Template name:</label>
                        <input type="text" class="form-control datasetName" placeholder="<Example> Sample Name, PECVD Oxide, Diffusion" required><br />
                        <label>Template tags (separate by comma):</label>                
                        <span><a style="font-height:12px;" href="#" data-toggle="popover" title="Template Tags" data-trigger="hover" data-content="Tags allow users to search personal and global templates through descriptive tags." title="">
                        <span class="glyphicon glyphicon-question-sign"></a></span> 
                        <br />
                        <input type="text" class="form-control tagName" placeholder="<Example> SEM, Diffusion" required><br />
                        <input type="button" value="Add New Field" class="btn btn-success btnAdd"/>     
                        <button class="btn btn-danger clearTemplate" type="button">Clear Template</button>
                        <br /><br />
                        <label>Share this template with others?</label> <input type="checkbox" class="checkShareTemplate"><br />

                        <div class="metaDataSettings">                    
                        </div>  
                        <div class="templateData">
                            <!--Template boxes will be added here -->
                            <br />
                        </div>
                        <br />
                        <div class="form-group" id="btnTemplate">   
                            <input type="button" value="Update Template" class="btn btn-success form-control updateTemplate">
                        </div>
                    </div>
                </div>
                </form>
            </div>    
        </div>
        <div role="tabpanel" class="tab-pane" id="deleteTemplate">
            <h3> Delete Template</h3>

            <div class="top-padding">
                <form id="formDeleteTemplate" action="" method="delete">
                <div class="form-group">
                    <div class="showTemplates">
                        <label>Load templates:</label><br />
                        <select class="form-control templates required">
                        </select>
                        <br />
                    </div>

                    <div class="form-group" id="btnTemplate">   
                        <input type="button" id="deleteTemplate" value="Delete this Template" class="btn btn-danger deleteTemplate form-control">
                    </div>
                </div>
                </form>
            </div>    
        </div>
    </div>        
</div>

    <form id="vocabularycreate" action='"""),_display_(Seq[Any](/*154.42*/routes/*154.48*/.Vocabularies.submit)),format.raw/*154.68*/("""' method="POST" enctype="multipart/form-data">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript>Javascript is required in order to use the uploader to create a new collection.</noscript>

        <input type="hidden" name="name" id="hiddenname" value="not set">
        <input type="hidden" name="keys" id="hiddenkeys" value="not set">
        <input type="hidden" name="description" id="hiddendescription" value="not set">
        <input type="hidden" name="space" id="hiddenspace" value="not set">
    </form>
<!--     </div>
</div> -->
<script language="javascript">
    $(".chosen-select").chosen("""),format.raw/*166.32*/("""{"""),format.raw/*166.33*/("""
        width: "100%",
        placeholder_text_multiple: "Select spaces for this template."
    """),format.raw/*169.5*/("""}"""),format.raw/*169.6*/(""");
</script>
<script language = "javascript">
    var idNameObj = """),format.raw/*172.21*/("""{"""),format.raw/*172.22*/("""}"""),format.raw/*172.23*/(""";

    function isUndefined(value) """),format.raw/*174.33*/("""{"""),format.raw/*174.34*/("""
        return typeof value === 'undefined';
    """),format.raw/*176.5*/("""}"""),format.raw/*176.6*/("""

    function getName(id) """),format.raw/*178.26*/("""{"""),format.raw/*178.27*/("""
        return idNameObj[id];
    """),format.raw/*180.5*/("""}"""),format.raw/*180.6*/("""

    $(document).ready(function() """),format.raw/*182.34*/("""{"""),format.raw/*182.35*/("""
        """),_display_(Seq[Any](/*183.10*/for(space <- spaces) yield /*183.30*/ {_display_(Seq[Any](format.raw/*183.32*/("""
            idNameObj[""""),_display_(Seq[Any](/*184.25*/space/*184.30*/.name)),format.raw/*184.35*/(""""] = """"),_display_(Seq[Any](/*184.42*/space/*184.47*/.id)),format.raw/*184.50*/("""";

        """)))})),format.raw/*186.10*/("""
        $(".chosen-choices").addClass("form-control");
    """),format.raw/*188.5*/("""}"""),format.raw/*188.6*/(""");

    $(".chosen-select").chosen().change(function(event, params) """),format.raw/*190.65*/("""{"""),format.raw/*190.66*/("""
    var targetId = this.getAttribute("id");
    if(!isUndefined(params.selected)) """),format.raw/*192.39*/("""{"""),format.raw/*192.40*/("""
        var addedId = params.selected;
        """),_display_(Seq[Any](/*194.10*/for(space <- spaces) yield /*194.30*/ {_display_(Seq[Any](format.raw/*194.32*/("""
            var currentId = """"),_display_(Seq[Any](/*195.31*/space)),format.raw/*195.36*/("""";
            if(currentId != targetId) """),format.raw/*196.39*/("""{"""),format.raw/*196.40*/("""
                $("#"""),_display_(Seq[Any](/*197.22*/space/*197.27*/.id)),format.raw/*197.30*/(""" option [value="+addedId +']').remove();
                $('#"""),_display_(Seq[Any](/*198.22*/space/*198.27*/.id)),format.raw/*198.30*/("""').trigger("chosen:updated");
            """),format.raw/*199.13*/("""}"""),format.raw/*199.14*/("""
        """)))})),format.raw/*200.10*/("""
        $('ul.chosen-choices li.search-field').css("width", "0");

    """),format.raw/*203.5*/("""}"""),format.raw/*203.6*/("""
    else if (!isUndefined(params.deselected)) """),format.raw/*204.47*/("""{"""),format.raw/*204.48*/("""
        var removedId = params.deselected;
        """),_display_(Seq[Any](/*206.10*/for(space <- spaces) yield /*206.30*/ {_display_(Seq[Any](format.raw/*206.32*/("""
            var currentId = """"),_display_(Seq[Any](/*207.31*/space)),format.raw/*207.36*/("""";
            if(currentId != targetId) """),format.raw/*208.39*/("""{"""),format.raw/*208.40*/("""
                $('#"""),_display_(Seq[Any](/*209.22*/space/*209.27*/.id)),format.raw/*209.30*/("""').prepend($("<option></option>").attr("value", removedId).text(getName(removedId)));
                $('#"""),_display_(Seq[Any](/*210.22*/space/*210.27*/.id)),format.raw/*210.30*/("""').trigger("chosen:updated");
            """),format.raw/*211.13*/("""}"""),format.raw/*211.14*/("""
        """)))})),format.raw/*212.10*/("""
    """),format.raw/*213.5*/("""}"""),format.raw/*213.6*/("""
    """),format.raw/*214.5*/("""}"""),format.raw/*214.6*/(""")

    if("""),_display_(Seq[Any](/*216.9*/spaceId/*216.16*/.isDefined)),format.raw/*216.26*/(""") """),format.raw/*216.28*/("""{"""),format.raw/*216.29*/("""
        $('#"""),_display_(Seq[Any](/*217.14*/spaceId)),format.raw/*217.21*/("""').prop('selected','selected');
        $('#spaceid').trigger("chosen:updated");
    """),format.raw/*219.5*/("""}"""),format.raw/*219.6*/("""
  </script>
""")))})),format.raw/*221.2*/("""
"""))}
    }
    
    def render(errorString:String,spaces:List[models.ProjectSpace],isNameRequired:Boolean,isDescriptionRequired:Boolean,spaceId:Option[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(errorString,spaces,isNameRequired,isDescriptionRequired,spaceId)(flash,user)
    
    def f:((String,List[models.ProjectSpace],Boolean,Boolean,Option[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (errorString,spaces,isNameRequired,isDescriptionRequired,spaceId) => (flash,user) => apply(errorString,spaces,isNameRequired,isDescriptionRequired,spaceId)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:27 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/newTemplate.scala.html
                    HASH: 75fa57628cb0314e35e6f1fabb524f533bd4d06e
                    MATRIX: 689->1|992->222|1024->246|1104->202|1133->295|1170->298|1201->321|1240->323|1349->397|1363->403|1430->449|1514->498|1528->504|1593->548|1678->597|1693->603|1762->650|1847->699|1862->705|1929->750|2163->948|2199->962|2263->990|2306->1011|2399->1068|2414->1074|2478->1116|2579->1181|2594->1187|2652->1223|2964->1499|2996->1522|3035->1523|3142->1594|3175->1605|3215->1626|3228->1630|3267->1631|3421->1753|10050->8345|10066->8351|10109->8371|10787->9020|10817->9021|10943->9119|10972->9120|11067->9186|11097->9187|11127->9188|11191->9223|11221->9224|11299->9274|11328->9275|11384->9302|11414->9303|11477->9338|11506->9339|11570->9374|11600->9375|11647->9385|11684->9405|11725->9407|11787->9432|11802->9437|11830->9442|11874->9449|11889->9454|11915->9457|11961->9470|12049->9530|12078->9531|12175->9599|12205->9600|12317->9683|12347->9684|12433->9733|12470->9753|12511->9755|12579->9786|12607->9791|12677->9832|12707->9833|12766->9855|12781->9860|12807->9863|12906->9925|12921->9930|12947->9933|13018->9975|13048->9976|13091->9986|13191->10058|13220->10059|13296->10106|13326->10107|13416->10160|13453->10180|13494->10182|13562->10213|13590->10218|13660->10259|13690->10260|13749->10282|13764->10287|13790->10290|13934->10397|13949->10402|13975->10405|14046->10447|14076->10448|14119->10458|14152->10463|14181->10464|14214->10469|14243->10470|14290->10481|14307->10488|14340->10498|14371->10500|14401->10501|14452->10515|14482->10522|14595->10607|14624->10608|14670->10622
                    LINES: 20->1|23->4|23->4|24->1|26->4|28->6|28->6|28->6|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|33->11|33->11|33->11|36->14|36->14|37->15|37->15|40->18|40->18|40->18|41->19|41->19|41->19|51->29|51->29|51->29|52->30|52->30|53->31|53->31|53->31|55->33|176->154|176->154|176->154|188->166|188->166|191->169|191->169|194->172|194->172|194->172|196->174|196->174|198->176|198->176|200->178|200->178|202->180|202->180|204->182|204->182|205->183|205->183|205->183|206->184|206->184|206->184|206->184|206->184|206->184|208->186|210->188|210->188|212->190|212->190|214->192|214->192|216->194|216->194|216->194|217->195|217->195|218->196|218->196|219->197|219->197|219->197|220->198|220->198|220->198|221->199|221->199|222->200|225->203|225->203|226->204|226->204|228->206|228->206|228->206|229->207|229->207|230->208|230->208|231->209|231->209|231->209|232->210|232->210|232->210|233->211|233->211|234->212|235->213|235->213|236->214|236->214|238->216|238->216|238->216|238->216|238->216|239->217|239->217|241->219|241->219|243->221
                    -- GENERATED --
                */
            