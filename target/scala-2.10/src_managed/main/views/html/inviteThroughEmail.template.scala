
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object inviteThroughEmail extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[String,String,String,Option[String],RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(token: String, spaceName: String, inviter: String, message: Option[String])(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider


Seq[Any](format.raw/*1.111*/("""
"""),format.raw/*3.1*/("""<html>
    <body>

        <p>Hello,</p>

        <p>You have been invited to space """),_display_(Seq[Any](/*8.44*/spaceName)),format.raw/*8.53*/(""" by """),_display_(Seq[Any](/*8.58*/inviter)),format.raw/*8.65*/(""" at <a href=""""),_display_(Seq[Any](/*8.79*/routes/*8.85*/.Application.index().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*8.146*/("""">"""),_display_(Seq[Any](/*8.149*/services/*8.157*/.AppConfiguration.getDisplayName)),format.raw/*8.189*/("""</a>. Please follow this
            <a href=""""),_display_(Seq[Any](/*9.23*/securesocial/*9.35*/.core.providers.utils.RoutesHelper.signUp(token).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.124*/("""">link</a> to accept this invitation.
        </p>
        <p>"""),_display_(Seq[Any](/*11.13*/message)),format.raw/*11.20*/("""</p>
    </body>
</html>"""))}
    }
    
    def render(token:String,spaceName:String,inviter:String,message:Option[String],request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(token,spaceName,inviter,message)(request)
    
    def f:((String,String,String,Option[String]) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (token,spaceName,inviter,message) => (request) => apply(token,spaceName,inviter,message)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:24 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/inviteThroughEmail.scala.html
                    HASH: b04c187853db08e31eab2b7b1b74c35a6e3dadf0
                    MATRIX: 643->1|889->110|916->154|1036->239|1066->248|1106->253|1134->260|1183->274|1197->280|1280->341|1319->344|1336->352|1390->384|1472->431|1492->443|1603->532|1702->595|1731->602
                    LINES: 20->1|24->1|25->3|30->8|30->8|30->8|30->8|30->8|30->8|30->8|30->8|30->8|30->8|31->9|31->9|31->9|33->11|33->11
                    -- GENERATED --
                */
            