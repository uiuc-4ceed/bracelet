
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object extractorsServersIP extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[String],Integer,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dtsservers:List[String], size:Integer):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import collection.JavaConverters._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.41*/("""
"""),format.raw/*4.75*/("""

"""),_display_(Seq[Any](/*6.2*/main("Extractor Servers")/*6.27*/{_display_(Seq[Any](format.raw/*6.28*/("""
	<div class="page-header">
		<h1>Servers Running Extractors</h1>
	</div>
	"""),_display_(Seq[Any](/*10.3*/if(size == 0)/*10.16*/ {_display_(Seq[Any](format.raw/*10.18*/("""
		<div class="row">
			<div class="col-md-12">
			No Extractors found. Sorry!
			</div>
		</div>
	""")))}/*16.3*/else/*16.7*/{_display_(Seq[Any](format.raw/*16.8*/("""
		<div class="container" > 
			<div class="row-fluid" >
				<div>
					<table id='tablec'  style="table-layout: fixed;width: 30%" cellpadding="10"  cellspacing="0" border="1">
						<thead>
								<tr>
									<th>Extractor Servers IP</th>
								</tr>
						</thead>
						<tbody>
							"""),_display_(Seq[Any](/*27.9*/for(req <- dtsservers) yield /*27.31*/{_display_(Seq[Any](format.raw/*27.32*/("""
								<tr>
	
								<td>"""),_display_(Seq[Any](/*30.14*/req)),format.raw/*30.17*/("""</td>
	
								</tr>	
		
							""")))})),format.raw/*34.9*/("""
						</tbody>
					</table>
				</div>
		 </div>
	 </div>
	""")))})),format.raw/*40.3*/("""
""")))})),format.raw/*41.2*/("""
"""))}
    }
    
    def render(dtsservers:List[String],size:Integer): play.api.templates.HtmlFormat.Appendable = apply(dtsservers,size)
    
    def f:((List[String],Integer) => play.api.templates.HtmlFormat.Appendable) = (dtsservers,size) => apply(dtsservers,size)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/extractorsServersIP.scala.html
                    HASH: e26194e9c293b59e1e7e51c3f71007fa928f457f
                    MATRIX: 615->1|792->95|824->119|903->40|931->168|968->171|1001->196|1039->197|1150->273|1172->286|1212->288|1330->388|1342->392|1380->393|1709->687|1747->709|1786->710|1851->739|1876->742|1941->776|2034->838|2067->840
                    LINES: 20->1|25->4|25->4|26->1|27->4|29->6|29->6|29->6|33->10|33->10|33->10|39->16|39->16|39->16|50->27|50->27|50->27|53->30|53->30|57->34|63->40|64->41
                    -- GENERATED --
                */
            