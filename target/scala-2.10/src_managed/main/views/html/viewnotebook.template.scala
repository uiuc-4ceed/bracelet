
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object viewnotebook extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[String,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(filename:String, fileContent: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import scala.io.Source

def /*7.2*/injectHtmlFromFile/*7.20*/(sourceCodeFilename: String):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*7.52*/(""" """),_display_(Seq[Any](/*7.54*/{
Html(Source.fromString(sourceCodeFilename).mkString)
}))))};
Seq[Any](format.raw/*1.76*/("""



"""),format.raw/*6.1*/("""
"""),format.raw/*9.3*/("""

"""),_display_(Seq[Any](/*11.2*/main("ViewNotebooks")/*11.23*/ {_display_(Seq[Any](format.raw/*11.25*/("""
<div class="page-header">
    <h1>"""),_display_(Seq[Any](/*13.10*/filename)),format.raw/*13.18*/("""</h1>
</div>
<div class="row">
    <div class="col-md-12">
        """),_display_(Seq[Any](/*17.10*/injectHtmlFromFile(fileContent))),format.raw/*17.41*/("""
    </div>
</div>
""")))})))}
    }
    
    def render(filename:String,fileContent:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(filename,fileContent)(user)
    
    def f:((String,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (filename,fileContent) => (user) => apply(filename,fileContent)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/viewnotebook.scala.html
                    HASH: 6037a15fe34f15ed32e90464a746ded3d9e3b6bc
                    MATRIX: 621->1|795->105|821->123|933->155|970->157|1060->75|1090->103|1117->214|1155->217|1185->238|1225->240|1297->276|1327->284|1431->352|1484->383
                    LINES: 20->1|23->7|23->7|25->7|25->7|28->1|32->6|33->9|35->11|35->11|35->11|37->13|37->13|41->17|41->17
                    -- GENERATED --
                */
            