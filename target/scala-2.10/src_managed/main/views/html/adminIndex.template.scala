
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object adminIndex extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.40*/("""
"""),_display_(Seq[Any](/*2.2*/main("Administration")/*2.24*/ {_display_(Seq[Any](format.raw/*2.26*/("""
	<div class="page-header">
		<h1>Index Administration</h1>
	</div>	
		
	<legend>Create an Index</legend>
  <div class="container-fluid"> 
  <!-- Indexer name, file type, adapter row -->       
	 <div class="row">
    <div class="col-sm-4">
      <h5>Choose a Name for Index (optional)</h5>
			<input class="form-control input-sm" type="text" id="nameTextArea" onchange="setNameForIndex()">
    </div>
    
    <div class="col-sm-4">
      <h5>Choose file type</h5>
					<select id="fileTypeDropDown" onchange="setFileTypeAndAvailableAdapters()">
					<option value="invalid">--File Type--</option>					
					<option value="image">Image</option>
					<option value="jpeg">Image-jpeg</option>
					<option value="png">Image-png</option>
					<option value="pdf">PDF</option>
					<option value="doc">Doc</option>
					<option value="rtf">RTF</option>
					<option value="sectionCensus">Section of Census Image</option>
					<option value="sectionFace">Face extractor section</option>
					<option value="sectionCinemetrics">Cinemetrics extractor section</option>
					</select>
    </div>
    
    <div class="col-sm-4">
    	<h5>Choose an Adapter</h5>
				<select id="adapterDropDown" onchange="setAdapterTypeAndAvailableExtractors()">
					<option value="invalid">--Adapter--</option>
				</select>	
    </div>
  </div>
  
  <!-- Extractor, Measure, Indexer row -->
  <div class="row">
    <div class="col-sm-4">
      <h5>Choose an Extractor</h5>
        <select id="extractorDropDown" onchange="setExtractorTypeAndAvailableMeasures()">
          <option value="invalid">--Extractor--</option>
        </select> 
    </div>
    
    <div class="col-sm-4" >
        <h5>Choose a Measure</h5>
        <select id="measureDropDown" onchange="setMeasureType()">
          <option value="invalid">--Measure--</option>
        </select>       
    </div>
    
    <div class="col-sm-4">
      <h5>Choose an Indexer </h5>
      <select id="indexerDropDown" onchange="setIndexerState()">
         <option value="invalid">--Indexer--</option>
      </select>       
    </div>
  </div>
  
  <!--  Create button row -->  
  <div class="row">
    <div class="col-sm-4" >
        <button type="submit" id="create" class="btn btn-primary btn-sm btn-margins"><span class="glyphicon glyphicon-ok"></span> Create</button>
    </div>
  </div>
  
  <!--  Feedback row -->
  <div class="row" >
   <div class="col-sm-4" id="createIndexFeedback">  
    <!-- Placeholder for feedback message -->
   </div>
  </div>
 </div>
  <br><br>
<!------------------------------------->
<!------    Build an index       ------>
<!------------------------------------->
<legend> Build an Index</legend>  
<div class="row">
	   <div class="col-md-12">
	   	<div class="col-md-3">
				<h5>Choose an Index </h5>
				<select id="indexDropDownList" onchange="setIndexState()">
					<option value="invalid">--Index--</option>
				</select>
				
				<div style="text-align: left;">
                    <button type="submit" id="build" class="btn btn-default btn-sm btn-margins"><span class="glyphicon glyphicon-cog"></span> Build Index</button>
				</div>
				
				<div id="buildmsg"style="text-align: left;">
      					<!-- Placeholder for build results message -->
        </div>
		</div>		
	</div>	
</div>
<br><br>

<!------------------------------------->
<!------    List all indexes     ------>
<!------------------------------------->
<legend> List All Indexes</legend>
 <div class="container-fluid"> 
  <div class="row">
    <div class="col-md-12">
	    <div style="text-align: left;">
            <button type="submit" id="list" class="btn btn-default btn-sm btn-margins"><span class="glyphicon glyphicon-list-alt"></span> List Indexes</button>
      </div>
      <div id="listIndexesTable">
          <!-- Placeholder for table with info about indexes -->       
      </div>
    </div>
   </div>
</div>
<br><br>

<!------------------------------------->
<!------    Delete one index     ------>
<!------------------------------------->
<legend> Delete an Index</legend>
<div class="row">
 	<div class="col-md-12">
		<div class="col-md-3">
			<h5>Choose an Index </h5>
			<select id="indexDel" onchange="setIndexDelState()">
				<option value="invalid">--Index--</option>
			</select>

			<div style="text-align: left;">
                <button type="submit" id="delete" class="btn btn-danger btn-sm btn-margins"><span class="glyphicon glyphicon-trash"></span> Delete Index</button>
			</div>
			<br>
			<div id="deletemsg" style="text-align: left;">
				<!-- Placeholder for message with results of delete index -->
      </div>
		</div>		
	</div>
</div>
<br><br>

<!------------------------------------->
<!------    Delete all indexes   ------>
<!------------------------------------->
<legend> Delete All Indexes</legend>
 <div class="container-fluid"> 
  <div class="row">
    <div class="col-md-12">
	   <div style="text-align: left;">
           <button type="submit" id="deleteAll" class="btn btn-danger btn-sm btn-margins"><span class="glyphicon glyphicon-trash"></span> Delete All</button>
     </div>
    
     <div id="deleteallmsg" style="text-align: left;">
        <!-- Placeholder for message with results of delete ALL indexes -->         
     </div>
   </div>  
  </div>
</div>
    <br><br>
        <!----------------------------------------------------->
        <!------  Reindex all resources in elasticsearch ------>
        <!----------------------------------------------------->
    <legend> Reindex All Resources in Elasticsearch</legend>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div style="text-align: left;">
                    <button type="submit" id="reindexElasticsearch" class="btn btn-default btn-sm btn-margins"><span class="glyphicon glyphicon-sort-by-alphabet"></span> Reindex</button>
                </div>

                <div id="reindexmsg" style="text-align: left;">
                        <!-- Placeholder for message with results of delete ALL indexes -->
                </div>
            </div>
        </div>
    </div>
	
<script src=""""),_display_(Seq[Any](/*181.15*/routes/*181.21*/.Assets.at("javascripts/adminIndex.js"))),format.raw/*181.60*/("""" type="text/javascript"></script>
    
""")))})),format.raw/*183.2*/("""
"""))}
    }
    
    def render(user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply()(user)
    
    def f:(() => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = () => (user) => apply()(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:27 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/adminIndex.scala.html
                    HASH: d69d716e06bcf9ddb66bfa675fd9f3a16742f147
                    MATRIX: 605->1|737->39|773->41|803->63|842->65|7010->6196|7026->6202|7088->6241|7161->6282
                    LINES: 20->1|23->1|24->2|24->2|24->2|203->181|203->181|203->181|205->183
                    -- GENERATED --
                */
            