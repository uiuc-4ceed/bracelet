
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object searchMultimediaIndex extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[UUID,models.Preview,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(section_id: UUID, preview: models.Preview)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.SearchResult


Seq[Any](format.raw/*1.81*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Search Results")/*5.24*/ {_display_(Seq[Any](format.raw/*5.26*/("""

<script src=""""),_display_(Seq[Any](/*7.15*/routes/*7.21*/.Assets.at("javascripts/spin.min.js"))),format.raw/*7.58*/("""" type="text/javascript"></script>

<div class="page-header">
  <h1>Search</h1>
</div>

<div data-spy="affix" data-offset-top="0" class="col-md-4">
<div class="row">
  <div class="col-md-4">
		<h2>Query</h2> 
		</div>
		</div>
<div class="row">
  <div class="col-md-8">
		<img src=""""),_display_(Seq[Any](/*21.14*/api/*21.17*/.routes.Previews.download(preview.id))),format.raw/*21.54*/(""""
					  		class="img-thumbnail"></img>
  </div>
</div>
</div>

<div id="descriptors"></div>

<script>

$(document).ready(function() """),format.raw/*31.30*/("""{"""),format.raw/*31.31*/("""		

	// Send AJAX request to get similar sections
	var objSearch = jsRoutes.api.Search.searchMultimediaIndex('"""),_display_(Seq[Any](/*34.62*/section_id)),format.raw/*34.72*/("""')
	var request = $.ajax("""),format.raw/*35.23*/("""{"""),format.raw/*35.24*/("""
		url: objSearch.url,
	  	type: 'GET',
	  	dataType: "json"
	"""),format.raw/*39.2*/("""}"""),format.raw/*39.3*/(""");

	// Start spinner wheel
	var spinner = new Spinner().spin($("#descriptors")[0]);

	// Process response
	request.done(function (response, textStatus, jqXHR)"""),format.raw/*45.53*/("""{"""),format.raw/*45.54*/("""

		// Stop spinner wheel
		spinner.stop();

		// Iterate through each feature descriptor
		for (var descriptor in response) """),format.raw/*51.36*/("""{"""),format.raw/*51.37*/("""

			// Display descriptor name
			$("#descriptors").append(
				"<div class='row'>" +
					"<div id='" + descriptor + "' class='col-md-4 col-md-offset-4'>" +
						"<h2>" + descriptor + "</h2>" + 
					"</div>" +
				"</div>");

			// Iterate through each section in a feature descriptor
			for (var section in response[descriptor]) """),format.raw/*62.46*/("""{"""),format.raw/*62.47*/("""

				// Copy relevant values to local variables
				var section_id = response[descriptor][section]["section_id"];
				var preview_id = response[descriptor][section]["preview_id"];
				var distance = Number(response[descriptor][section]["distance"]).toFixed(4); // Truncating distance to four decimal places				

				// Display section details
				$("#" + descriptor).append(
					"<div class='row'>" + 
						"<div class='col-md-12'>" +
							"<a href='" + jsRoutes.controllers.Files.fileBySection(section_id).url + " '>" +							
				  				"<img src='" + jsRoutes.api.Previews.download(preview_id).url + "' class='img-thumbnail'></img>" +
				    		"</a>" + 
				  			"<p>Distance: " + distance.toString() + "</p>" +
						"</div>" + 
					"</div>");
			"""),format.raw/*79.4*/("""}"""),format.raw/*79.5*/("""
		"""),format.raw/*80.3*/("""}"""),format.raw/*80.4*/("""
	"""),format.raw/*81.2*/("""}"""),format.raw/*81.3*/(""");
"""),format.raw/*82.1*/("""}"""),format.raw/*82.2*/(""");

</script>
""")))})),format.raw/*85.2*/("""
"""))}
    }
    
    def render(section_id:UUID,preview:models.Preview,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(section_id,preview)(user)
    
    def f:((UUID,models.Preview) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (section_id,preview) => (user) => apply(section_id,preview)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/searchMultimediaIndex.scala.html
                    HASH: 8e907867bb0f83eaad582bbe6b1eac4b48935377
                    MATRIX: 636->1|841->80|869->115|905->117|935->139|974->141|1025->157|1039->163|1097->200|1416->483|1428->486|1487->523|1648->656|1677->657|1824->768|1856->778|1909->803|1938->804|2027->866|2055->867|2242->1026|2271->1027|2424->1152|2453->1153|2815->1487|2844->1488|3628->2245|3656->2246|3686->2249|3714->2250|3743->2252|3771->2253|3801->2256|3829->2257|3875->2272
                    LINES: 20->1|24->1|26->4|27->5|27->5|27->5|29->7|29->7|29->7|43->21|43->21|43->21|53->31|53->31|56->34|56->34|57->35|57->35|61->39|61->39|67->45|67->45|73->51|73->51|84->62|84->62|101->79|101->79|102->80|102->80|103->81|103->81|104->82|104->82|107->85
                    -- GENERATED --
                */
            