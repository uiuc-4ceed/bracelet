
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object toolManager extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[play.api.libs.json.JsObject,List[UUID],scala.collection.mutable.Map[UUID, services.ToolInstance],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(toolList: play.api.libs.json.JsObject, instances: List[UUID], instanceMap: scala.collection.mutable.Map[UUID, services.ToolInstance])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import play.api.Play.current

import services.ToolManagerPlugin

import scala.io.Source

import scala.xml.parsing.XhtmlParser

import play.api.libs.json._


Seq[Any](format.raw/*1.172*/("""

"""),format.raw/*9.1*/("""

"""),_display_(Seq[Any](/*11.2*/main("Tool Manager")/*11.22*/ {_display_(Seq[Any](format.raw/*11.24*/("""
  <link rel="stylesheet" href=""""),_display_(Seq[Any](/*12.33*/routes/*12.39*/.Assets.at("stylesheets/tableborder.css"))),format.raw/*12.80*/("""" />

  <div class="row">
    <div class="col-md-12">
      <h2>Available analysis environments</h2>
    </div>
  </div>

  <div>
    <a class="btn btn-link" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample" href="#tools-list">
      <span class="glyphicon glyphicon-collapse-down" title="Expand tools list"></span> Expand list
    </a>
    <div class="collapse" id="tools-list" aria-hidden="true">
      <ul>
        """),_display_(Seq[Any](/*26.10*/for(apiPath <- toolList.keys) yield /*26.39*/ {_display_(Seq[Any](format.raw/*26.41*/("""
          <li>
            <b>"""),_display_(Seq[Any](/*28.17*/{
                var x = (toolList \ apiPath \ "name")
                x.toString.replace("\"","")
            })),format.raw/*31.14*/("""</b></br>&nbsp;&nbsp;&nbsp;&nbsp;
            """),_display_(Seq[Any](/*32.14*/{
              var x = (toolList \ apiPath \ "description")
              x.toString.replace("\"","")
            })),format.raw/*35.14*/("""
          </li>
        """)))})),format.raw/*37.10*/("""
      </ul>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <h2>Active instances</h2>
    </div>
  </div>

  <div>
  """),_display_(Seq[Any](/*49.4*/instances/*49.13*/.map/*49.17*/ { instanceID =>_display_(Seq[Any](format.raw/*49.33*/("""
    """),_display_(Seq[Any](/*50.6*/instanceMap/*50.17*/.get(instanceID)/*50.33*/ match/*50.39*/ {/*51.7*/case Some(inst) =>/*51.25*/ {_display_(Seq[Any](format.raw/*51.27*/("""
          <div class = "panel panel-default" id=""""),_display_(Seq[Any](/*52.51*/instanceID)),format.raw/*52.61*/("""-listitem">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12">

                  <h2>
                    """),_display_(Seq[Any](/*58.22*/if(inst.url == "")/*58.40*/ {_display_(Seq[Any](format.raw/*58.42*/("""
                      """),_display_(Seq[Any](/*59.24*/inst/*59.28*/.name)),format.raw/*59.33*/(""" (not yet ready)
                    """)))}/*60.23*/else/*60.28*/{_display_(Seq[Any](format.raw/*60.29*/("""
                      <a href=""""),_display_(Seq[Any](/*61.33*/inst/*61.37*/.url.replace("\"",""))),format.raw/*61.58*/("""" target="_blank">"""),_display_(Seq[Any](/*61.77*/inst/*61.81*/.name)),format.raw/*61.86*/("""</a>
                    """)))})),format.raw/*62.22*/("""
                  </h2>

                  <div class ="row">
                    <div class="col-md-6 col-sm-6 col-lg-6">
                      <ul class="list-unstyled">
                        """),_display_(Seq[Any](/*68.26*/inst/*68.30*/.owner/*68.36*/ match/*68.42*/ {/*69.27*/case Some(u) =>/*69.42*/ {_display_(Seq[Any](format.raw/*69.44*/("""
                            <li>"""),_display_(Seq[Any](/*70.34*/Messages("owner.label"))),format.raw/*70.57*/(""": <a href= """"),_display_(Seq[Any](/*70.70*/routes/*70.76*/.Profile.viewProfileUUID(u.id))),format.raw/*70.106*/(""""> """),_display_(Seq[Any](/*70.110*/u/*70.111*/.fullName)),format.raw/*70.120*/(""" </a></li>
                          """)))}/*72.27*/case None =>/*72.39*/ {}})),format.raw/*73.26*/("""

                        <li>Environment: <b>"""),_display_(Seq[Any](/*75.46*/Html(inst.toolName))),format.raw/*75.65*/("""</b></li>
                        <li>Created """),_display_(Seq[Any](/*76.38*/inst/*76.42*/.created.format("MMM dd, yyyy"))),format.raw/*76.73*/("""</li>
                        <li>Updated """),_display_(Seq[Any](/*77.38*/inst/*77.42*/.updated.format("MMM dd, yyyy"))),format.raw/*77.73*/("""</li>

                        <li>
                          """),_display_(Seq[Any](/*80.28*/if(inst.uploadHistory.keys.size > 0)/*80.64*/ {_display_(Seq[Any](format.raw/*80.66*/("""
                            <a class="btn btn-link" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample" href="#datasets-"""),_display_(Seq[Any](/*81.147*/inst/*81.151*/.id)),format.raw/*81.154*/("""">
                              <span class="glyphicon glyphicon-collapse-down" title=""""),_display_(Seq[Any](/*82.87*/inst/*82.91*/.uploadHistory.keys.size)),format.raw/*82.115*/(""" datasets"></span>
                              Expand upload history
                            </a>
                            <div class="collapse" id="datasets-"""),_display_(Seq[Any](/*85.65*/inst/*85.69*/.id)),format.raw/*85.72*/("""" aria-hidden="true">
                              <div class="well"> """),_display_(Seq[Any](/*86.51*/{
                                var dsList = ""
                                instanceMap.get(instanceID) match {
                                  case Some(instance) => {
                                    for((dsId, (dsName, upTime, uploaderId)) <- instance.uploadHistory) {
                                      val dsURL = controllers.routes.Datasets.dataset(dsId).url

                                      dsList += "<b>" + upTime + "</b>: <a href=" + dsURL + ">" + dsName + "</a></br>"
                                    }
                                  }
                                  case None => {}
                                }
                                Html(dsList)
                              })),format.raw/*99.32*/("""
                              </div>
                            </div>
                          """)))})),format.raw/*102.28*/("""
                        </li>
                      </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-6">
                      <ul class="list-unstyled">
                          <li>
                            <a id='download-dataset-url' href="#" onclick="removeInstance('"""),_display_(Seq[Any](/*109.93*/inst/*109.97*/.toolPath)),format.raw/*109.106*/("""','"""),_display_(Seq[Any](/*109.110*/instanceID)),format.raw/*109.120*/("""')" class="btn btn-link"role="button">
                              <span class="glyphicon glyphicon-stop"></span>
                              Stop Instance
                            </a>
                          </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        """)))}/*122.7*/case None =>/*122.19*/ {}})),format.raw/*123.6*/("""
  """)))})),format.raw/*124.4*/("""
  </div>

  <script>
    function removeInstance(toolPath, instanceID) """),format.raw/*128.51*/("""{"""),format.raw/*128.52*/("""
      // Get current status from ToolManagerPlugin and refresh sidebar table
      var request = new XMLHttpRequest();
      request.onreadystatechange = function() """),format.raw/*131.47*/("""{"""),format.raw/*131.48*/("""
        if (request.readyState == 4 && request.status == 200) """),format.raw/*132.63*/("""{"""),format.raw/*132.64*/("""
          location.reload();
        """),format.raw/*134.9*/("""}"""),format.raw/*134.10*/("""
      """),format.raw/*135.7*/("""}"""),format.raw/*135.8*/("""

      request.open("GET", jsRoutes.controllers.ToolManager.removeInstance(toolPath, instanceID).url, true);
      request.send();
    """),format.raw/*139.5*/("""}"""),format.raw/*139.6*/("""
  </script>
""")))})),format.raw/*141.2*/("""

"""))}
    }
    
    def render(toolList:play.api.libs.json.JsObject,instances:List[UUID],instanceMap:scala.collection.mutable.Map[UUID, services.ToolInstance],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(toolList,instances,instanceMap)(user)
    
    def f:((play.api.libs.json.JsObject,List[UUID],scala.collection.mutable.Map[UUID, services.ToolInstance]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (toolList,instances,instanceMap) => (user) => apply(toolList,instances,instanceMap)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:25 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/toolManager.scala.html
                    HASH: 8e188a4bf1de649f822124dcd11abedcec217136
                    MATRIX: 703->1|1140->171|1168->346|1206->349|1235->369|1275->371|1344->404|1359->410|1422->451|1904->897|1949->926|1989->928|2057->960|2192->1073|2275->1120|2413->1236|2471->1262|2652->1408|2670->1417|2683->1421|2737->1437|2778->1443|2798->1454|2823->1470|2838->1476|2848->1485|2875->1503|2915->1505|3002->1556|3034->1566|3256->1752|3283->1770|3323->1772|3383->1796|3396->1800|3423->1805|3480->1844|3493->1849|3532->1850|3601->1883|3614->1887|3657->1908|3712->1927|3725->1931|3752->1936|3810->1962|4044->2160|4057->2164|4072->2170|4087->2176|4098->2205|4122->2220|4162->2222|4232->2256|4277->2279|4326->2292|4341->2298|4394->2328|4435->2332|4446->2333|4478->2342|4535->2407|4556->2419|4582->2448|4665->2495|4706->2514|4789->2561|4802->2565|4855->2596|4934->2639|4947->2643|5000->2674|5099->2737|5144->2773|5184->2775|5368->2922|5382->2926|5408->2929|5533->3018|5546->3022|5593->3046|5797->3214|5810->3218|5835->3221|5943->3293|6698->4026|6831->4126|7187->4445|7201->4449|7234->4458|7276->4462|7310->4472|7723->4873|7745->4885|7771->4894|7807->4898|7908->4970|7938->4971|8133->5137|8163->5138|8255->5201|8285->5202|8351->5240|8381->5241|8416->5248|8445->5249|8609->5385|8638->5386|8684->5400
                    LINES: 20->1|34->1|36->9|38->11|38->11|38->11|39->12|39->12|39->12|53->26|53->26|53->26|55->28|58->31|59->32|62->35|64->37|76->49|76->49|76->49|76->49|77->50|77->50|77->50|77->50|77->51|77->51|77->51|78->52|78->52|84->58|84->58|84->58|85->59|85->59|85->59|86->60|86->60|86->60|87->61|87->61|87->61|87->61|87->61|87->61|88->62|94->68|94->68|94->68|94->68|94->69|94->69|94->69|95->70|95->70|95->70|95->70|95->70|95->70|95->70|95->70|96->72|96->72|96->73|98->75|98->75|99->76|99->76|99->76|100->77|100->77|100->77|103->80|103->80|103->80|104->81|104->81|104->81|105->82|105->82|105->82|108->85|108->85|108->85|109->86|122->99|125->102|132->109|132->109|132->109|132->109|132->109|144->122|144->122|144->123|145->124|149->128|149->128|152->131|152->131|153->132|153->132|155->134|155->134|156->135|156->135|160->139|160->139|162->141
                    -- GENERATED --
                */
            