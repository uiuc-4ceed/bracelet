
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listnotebook extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[models.Notebook],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(notebookList: List[models.Notebook])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.75*/("""

"""),_display_(Seq[Any](/*3.2*/main("Notebooks")/*3.19*/ {_display_(Seq[Any](format.raw/*3.21*/("""
<div class="page-header">
    <h1>List of Saved Notebooks</h1>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover">

            <thead>

            <tr>
                <th>Name</th>
                <th>Actions</th>

                <th>
            </tr>
            </thead>

            <tbody>
            """),_display_(Seq[Any](/*22.14*/notebookList/*22.26*/.map/*22.30*/ { notebook =>_display_(Seq[Any](format.raw/*22.44*/("""


            <tr>
                <!--
                <td>
                    """),_display_(Seq[Any](/*28.22*/notebook/*28.30*/.name)),format.raw/*28.35*/("""

                    <a href=""""),_display_(Seq[Any](/*30.31*/controllers/*30.42*/.routes.Notebook.viewNotebook(notebook.id))),format.raw/*30.84*/("""">View</a>
                    <a href=""""),_display_(Seq[Any](/*31.31*/controllers/*31.42*/.routes.Notebook.editNotebook(notebook.id))),format.raw/*31.84*/("""">Edit</a>
                </td>
                -->

                <td><a href=""""),_display_(Seq[Any](/*35.31*/controllers/*35.42*/.routes.Notebook.viewNotebook(notebook.id))),format.raw/*35.84*/("""">"""),_display_(Seq[Any](/*35.87*/notebook/*35.95*/.name)),format.raw/*35.100*/("""</a></td>
                <td>
                    <a href=""""),_display_(Seq[Any](/*37.31*/controllers/*37.42*/.routes.Notebook.viewNotebook(notebook.id))),format.raw/*37.84*/("""">View</a> &nbsp;
                    <a href=""""),_display_(Seq[Any](/*38.31*/controllers/*38.42*/.routes.Notebook.deleteNotebook(notebook.id))),format.raw/*38.86*/("""">Delete</a>
                </td>


            </tr>
            """)))})),format.raw/*43.14*/("""
            </tbody>
        </table>
    </div>
</div>
""")))})))}
    }
    
    def render(notebookList:List[models.Notebook],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(notebookList)(user)
    
    def f:((List[models.Notebook]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (notebookList) => (user) => apply(notebookList)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:28 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/listnotebook.scala.html
                    HASH: e58e6d107bb502cfc147443a7bdbc2af5d5f4a0c
                    MATRIX: 629->1|796->74|833->77|858->94|897->96|1304->467|1325->479|1338->483|1390->497|1509->580|1526->588|1553->593|1621->625|1641->636|1705->678|1782->719|1802->730|1866->772|1986->856|2006->867|2070->909|2109->912|2126->920|2154->925|2251->986|2271->997|2335->1039|2419->1087|2439->1098|2505->1142|2605->1210
                    LINES: 20->1|23->1|25->3|25->3|25->3|44->22|44->22|44->22|44->22|50->28|50->28|50->28|52->30|52->30|52->30|53->31|53->31|53->31|57->35|57->35|57->35|57->35|57->35|57->35|59->37|59->37|59->37|60->38|60->38|60->38|65->43
                    -- GENERATED --
                */
            