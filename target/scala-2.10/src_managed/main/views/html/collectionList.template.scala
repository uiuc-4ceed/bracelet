
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object collectionList extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template15[List[models.Collection],String,String,Int,Option[String],Option[String],Option[String],Option[String],Option[String],Option[String],String,String,Boolean,play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(collectionsList: List[models.Collection], prev: String, next: String, limit: Int, mode: Option[String], space: Option[String], spaceName: Option[String], title: Option[String], owner:Option[String], ownerName: Option[String], when: String, date: String, showTrash : Boolean = false)(implicit flash: play.api.mvc.Flash,  user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.349*/("""
"""),_display_(Seq[Any](/*3.2*/main( Messages("collections.title"))/*3.38*/ {_display_(Seq[Any](format.raw/*3.40*/("""

"""),_display_(Seq[Any](/*5.2*/util/*5.6*/.masonry())),format.raw/*5.16*/("""
    
<script src=""""),_display_(Seq[Any](/*7.15*/routes/*7.21*/.Assets.at("javascripts/collectionListProcess.js"))),format.raw/*7.71*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*8.15*/routes/*8.21*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*8.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*9.15*/routes/*9.21*/.Assets.at("javascripts/spaceModify.js"))),format.raw/*9.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*10.15*/routes/*10.21*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*10.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*11.15*/routes/*11.21*/.Assets.at("javascripts/follow-button.js"))),format.raw/*11.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*12.15*/routes/*12.21*/.Assets.at("javascripts/displayPanels.js"))),format.raw/*12.63*/("""" type="text/javascript"></script>

    <div class="row">
        <ol class="breadcrumb">
            """),_display_(Seq[Any](/*16.14*/(owner, ownerName)/*16.32*/ match/*16.38*/ {/*17.17*/case (Some(o), Some(n)) =>/*17.43*/ {_display_(Seq[Any](format.raw/*17.45*/("""
                    <li> <span class="glyphicon glyphicon-user"></span> <a href=""""),_display_(Seq[Any](/*18.83*/routes/*18.89*/.Profile.viewProfileUUID(UUID(o)))),format.raw/*18.122*/(""""> """),_display_(Seq[Any](/*18.126*/n)),format.raw/*18.127*/("""</a></li>
                """)))}/*20.17*/case (_, _) =>/*20.31*/ {}})),format.raw/*21.14*/("""
            """),_display_(Seq[Any](/*22.14*/(space, spaceName)/*22.32*/ match/*22.38*/ {/*23.17*/case (Some(s), Some(sn)) =>/*23.44*/ {_display_(Seq[Any](format.raw/*23.46*/("""
                    <li><span class="glyphicon glyphicon-hdd"></span> <a href=""""),_display_(Seq[Any](/*24.81*/routes/*24.87*/.Spaces.getSpace(UUID(s)))),format.raw/*24.112*/(""""> """),_display_(Seq[Any](/*24.116*/sn)),format.raw/*24.118*/("""</a></li>
                """)))}/*26.17*/case (_,_) =>/*26.30*/ {}})),format.raw/*27.14*/("""

            <li><span class="glyphicon glyphicon-th-large"> </span> Collections</li>
        </ol>
    </div>
<div class="row">
    <div class="col-md-12">
        <h1>"""),_display_(Seq[Any](/*34.14*/Html(title.getOrElse("Collections")))),format.raw/*34.50*/("""</h1>
        """),_display_(Seq[Any](/*35.10*/if(showTrash)/*35.23*/{_display_(Seq[Any](format.raw/*35.24*/("""
            <p>"""),_display_(Seq[Any](/*36.17*/Messages("collection.trash.message", Messages("collections.title").toLowerCase,  Messages("datasets.title").toLowerCase))),format.raw/*36.137*/("""</p>

        """)))}/*38.11*/else/*38.16*/{_display_(Seq[Any](format.raw/*38.17*/("""
            <p>"""),_display_(Seq[Any](/*39.17*/Messages("collection.list.message", Messages("collections.title").toLowerCase,  Messages("datasets.title").toLowerCase))),format.raw/*39.136*/("""</p>
        """)))})),format.raw/*40.10*/("""
    </div>
</div>

<div class="row">
    <div class="btn-toolbar">
    <!-- pull-right class stacks things to the right - first item to the farthest right -->
        <div class="btn-group btn-group-sm pull-right">
            <button type="button" class="btn btn-default active" href="#tile-view" id="tile-view-btn"><span class="glyphicon glyphicon-th-large"></span></button>
            <button type="button" class="btn btn-default" href="#list-view" id="list-view-btn"><span class="glyphicon glyphicon-th-list"></span></button>
        </div>
        <div class="btn-group btn-group-sm pull-right" id="number-displayed-dropdown">
            <select id="numPageItems" class="form-control" onchange="getValue()">
                <option value="12">12</option>
                <option value="24">24</option>
                <option value="48">48</option>
                <option value="96">96</option>
            </select>
        </div>

"""),_display_(Seq[Any](/*60.2*/if(play.Play.application().configuration().getBoolean("sortInMemory"))/*60.72*/ {_display_(Seq[Any](format.raw/*60.74*/("""
  """),_display_(Seq[Any](/*61.4*/space/*61.9*/ match/*61.15*/ {/*62.5*/case Some(s) =>/*62.20*/ {_display_(Seq[Any](format.raw/*62.22*/("""
          <div class="btn-group btn-group-sm pull-right">
              <label class="sortchoice" for="js-sort-single">Sort By:
              <select class="js-sort-single">
                <option value="dateN">Newest</option>
                <option value="dateO">Oldest</option>
                <option value="titleA">Title (A-Z)</option>
                <option value="titleZ">Title (Z-A)</option>
                <option value="sizeL">Size (L)</option>
                <option value="sizeS">Size (S)</option>
              </select>
              </label>
          </div>
<script type="text/javascript">
  $(function() """),format.raw/*76.16*/("""{"""),format.raw/*76.17*/("""
	$(".js-sort-single").select2("""),format.raw/*77.31*/("""{"""),format.raw/*77.32*/("""minimumResultsForSearch: Infinity"""),format.raw/*77.65*/("""}"""),format.raw/*77.66*/(""");
  	//Set starting value based on cookie
  	var order = 'dateN';
  	if($.cookie('sort-order') != null) """),format.raw/*80.39*/("""{"""),format.raw/*80.40*/("""
  	  //removing quotes from around cookie value 
  	  order = $.cookie('sort-order').replace(/['"]+/g, '');
  	"""),format.raw/*83.4*/("""}"""),format.raw/*83.5*/("""
    $('.js-sort-single').val(order).trigger("change");

    var currentSpace = null;
    currentSpace = '"""),_display_(Seq[Any](/*87.22*/s)),format.raw/*87.23*/("""';

    $(".js-sort-single").on('select2:select', function (evt) """),format.raw/*89.62*/("""{"""),format.raw/*89.63*/("""
         $(window).trigger("sortchange");
    """),format.raw/*91.5*/("""}"""),format.raw/*91.6*/(""");
    $(window).on('sortchange', function() """),format.raw/*92.43*/("""{"""),format.raw/*92.44*/("""
      var sort = $(".js-sort-single").val();
      //Update cookie
      $.cookie('sort-order', sort, """),format.raw/*95.36*/("""{"""),format.raw/*95.37*/("""path: '/'"""),format.raw/*95.46*/("""}"""),format.raw/*95.47*/(""");
      //Go get the list sorted the new way
      """),_display_(Seq[Any](/*97.8*/if(prev != "")/*97.22*/ {_display_(Seq[Any](format.raw/*97.24*/("""
      var request = jsRoutes.controllers.Collections.sortedListInSpace(currentSpace, """),_display_(Seq[Any](/*98.87*/prev/*98.91*/.toInt)),format.raw/*98.97*/(""", """),_display_(Seq[Any](/*98.100*/limit)),format.raw/*98.105*/(""");
      """)))}/*99.9*/else/*99.14*/{_display_(Seq[Any](format.raw/*99.15*/("""
      var request = jsRoutes.controllers.Collections.sortedListInSpace(currentSpace, 0, """),_display_(Seq[Any](/*100.90*/limit)),format.raw/*100.95*/(""");
      """)))})),format.raw/*101.8*/("""
      window.location = request.url;
    """),format.raw/*103.5*/("""}"""),format.raw/*103.6*/(""");
  """),format.raw/*104.3*/("""}"""),format.raw/*104.4*/(""");
</script>
    """)))}/*107.5*/case None =>/*107.17*/ {}})),format.raw/*108.6*/("""
""")))})),format.raw/*109.2*/("""
        """),_display_(Seq[Any](/*110.10*/user/*110.14*/ match/*110.20*/ {/*111.13*/case Some(u) =>/*111.28*/ {_display_(Seq[Any](format.raw/*111.30*/("""
                """),_display_(Seq[Any](/*112.18*/(space, owner)/*112.32*/ match/*112.38*/ {/*113.21*/case (Some(s), Some(o)) =>/*113.47*/ {_display_(Seq[Any](format.raw/*113.49*/("""
                        """),_display_(Seq[Any](/*114.26*/if(Permission.checkPermission(Permission.CreateCollection, ResourceRef(ResourceRef.space, UUID(s))))/*114.126*/ {_display_(Seq[Any](format.raw/*114.128*/("""
                            """),_display_(Seq[Any](/*115.30*/if(o.equalsIgnoreCase(u.id.stringify))/*115.68*/ {_display_(Seq[Any](format.raw/*115.70*/("""

                                <a id="create-collection" href=""""),_display_(Seq[Any](/*117.66*/routes/*117.72*/.Collections.newCollection(space))),format.raw/*117.105*/("""" class="btn btn-primary btn-sm pull-right" title=""""),_display_(Seq[Any](/*117.157*/Messages("create.title", Messages("collection.title")))),format.raw/*117.211*/("""">
                                    <span class="glyphicon glyphicon-ok"></span>  """),_display_(Seq[Any](/*118.84*/Messages("create.title", ""))),format.raw/*118.112*/("""</a>
                            """)))})),format.raw/*119.30*/("""
                        """)))})),format.raw/*120.26*/("""
                    """)))}/*122.21*/case (Some(s), _) =>/*122.41*/ {_display_(Seq[Any](format.raw/*122.43*/("""
                        """),_display_(Seq[Any](/*123.26*/if(Permission.checkPermission(Permission.CreateCollection, ResourceRef(ResourceRef.space, UUID(s))))/*123.126*/ {_display_(Seq[Any](format.raw/*123.128*/("""
                                <a id="create-collection" href=""""),_display_(Seq[Any](/*124.66*/routes/*124.72*/.Collections.newCollection(space))),format.raw/*124.105*/("""" class="btn btn-primary btn-sm pull-right" title=""""),_display_(Seq[Any](/*124.157*/Messages("create.title", Messages("collection.title")))),format.raw/*124.211*/("""">
                                    <span class="glyphicon glyphicon-ok"></span>  """),_display_(Seq[Any](/*125.84*/Messages("create.title", ""))),format.raw/*125.112*/("""</a>
                        """)))})),format.raw/*126.26*/("""
                    """)))}/*128.21*/case (_, Some(o)) =>/*128.41*/ {_display_(Seq[Any](format.raw/*128.43*/("""
                        """),_display_(Seq[Any](/*129.26*/if(o.equalsIgnoreCase(u.id.stringify))/*129.64*/ {_display_(Seq[Any](format.raw/*129.66*/("""
                            """),_display_(Seq[Any](/*130.30*/if(showTrash)/*130.43*/{_display_(Seq[Any](format.raw/*130.44*/("""
                                <a id="clear-trash" onclick="confirmClearTrash('collection', '"""),_display_(Seq[Any](/*131.96*/(routes.Collections.list("",owner=Some(user.get.id.stringify),showTrash=true)))),format.raw/*131.174*/("""')"
                                class="btn btn-primary btn-sm pull-right" href="#"> <span class="glyphicon glyphicon-trash"></span>  """),_display_(Seq[Any](/*132.135*/Messages("cleartrash.title", Messages("collections.title")))),format.raw/*132.194*/("""</a>
                                
                            """)))}/*134.31*/else/*134.36*/{_display_(Seq[Any](format.raw/*134.37*/("""
                                <a id="create-collection" href=""""),_display_(Seq[Any](/*135.66*/routes/*135.72*/.Collections.newCollection(space))),format.raw/*135.105*/("""" class="btn btn-primary btn-sm pull-right" title=""""),_display_(Seq[Any](/*135.157*/Messages("create.title", Messages("collection.title")))),format.raw/*135.211*/("""">
                                    <span class="glyphicon glyphicon-ok"></span>  """),_display_(Seq[Any](/*136.84*/Messages("create.title", ""))),format.raw/*136.112*/("""</a>
                            """)))})),format.raw/*137.30*/("""

                        """)))})),format.raw/*139.26*/("""
                    """)))}/*142.21*/case (_, _) =>/*142.35*/ {_display_(Seq[Any](format.raw/*142.37*/("""
                        <a id="create-collection" class="btn btn-primary btn-sm pull-right" href=""""),_display_(Seq[Any](/*143.100*/routes/*143.106*/.Collections.newCollection(space))),format.raw/*143.139*/("""" title=""""),_display_(Seq[Any](/*143.149*/Messages("create.title", Messages("collection.title")))),format.raw/*143.203*/(""""><span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*143.251*/Messages("create.title", ""))),format.raw/*143.279*/("""</a>
                    """)))}})),format.raw/*145.18*/("""
            """)))}/*148.13*/case _ =>/*148.22*/ {}})),format.raw/*150.10*/("""

        <script>
            var removeIndicator = false;
	        var viewMode = '"""),_display_(Seq[Any](/*154.27*/mode/*154.31*/.getOrElse("tile"))),format.raw/*154.49*/("""';
	        $.cookie.raw = true;
	        $.cookie.json = true;
            $(function() """),format.raw/*157.26*/("""{"""),format.raw/*157.27*/("""            	                
                $('#tile-view-btn').click(function() """),format.raw/*158.54*/("""{"""),format.raw/*158.55*/("""
                  $('#tile-view').removeClass('hidden');
                  $('#list-view').addClass('hidden');
                  $('#tile-view-btn').addClass('active');
                  $('#list-view-btn').removeClass('active');
                  viewMode = "tile";
                  updatePage();                  
                  $.cookie('view-mode', 'tile', """),format.raw/*165.49*/("""{"""),format.raw/*165.50*/(""" path: '/' """),format.raw/*165.61*/("""}"""),format.raw/*165.62*/(""");
                  $('#masonry').masonry().masonry("""),format.raw/*166.51*/("""{"""),format.raw/*166.52*/("""
                      itemSelector: '.post-box',
                      columnWidth: '.post-box',
                      transitionDuration: 4
                  """),format.raw/*170.19*/("""}"""),format.raw/*170.20*/(""");
                """),format.raw/*171.17*/("""}"""),format.raw/*171.18*/(""");
                $('#list-view-btn').click(function() """),format.raw/*172.54*/("""{"""),format.raw/*172.55*/("""
                  $('#tile-view').addClass('hidden');
                  $('#list-view').removeClass('hidden');
                  $('#list-view-btn').addClass('active');
                  $('#tile-view-btn').removeClass('active');
                  viewMode = "list";
                  updatePage();
                  //Utilizing library from https://github.com/carhartl/jquery-cookie/tree/v1.4.1
                  $.cookie("view-mode", "list", """),format.raw/*180.49*/("""{"""),format.raw/*180.50*/(""" path: '/' """),format.raw/*180.61*/("""}"""),format.raw/*180.62*/(""");
                """),format.raw/*181.17*/("""}"""),format.raw/*181.18*/(""");                                
            """),format.raw/*182.13*/("""}"""),format.raw/*182.14*/(""");
            
            $(document).ready(function() """),format.raw/*184.42*/("""{"""),format.raw/*184.43*/("""  
            	//Set the cookie, for the case when it is passed in by the parameter
                $.cookie("view-mode", viewMode, """),format.raw/*186.49*/("""{"""),format.raw/*186.50*/(""" path: '/' """),format.raw/*186.61*/("""}"""),format.raw/*186.62*/(""");
                if (viewMode == "list") """),format.raw/*187.41*/("""{"""),format.raw/*187.42*/("""
                    $('#tile-view').addClass('hidden');
                    $('#list-view').removeClass('hidden');
                    $('#list-view-btn').addClass('active');
                    $('#tile-view-btn').removeClass('active');                      
                """),format.raw/*192.17*/("""}"""),format.raw/*192.18*/("""
                else """),format.raw/*193.22*/("""{"""),format.raw/*193.23*/("""
                    $('#tile-view').removeClass('hidden');
                    $('#list-view').addClass('hidden');
                    $('#tile-view-btn').addClass('active');
                    $('#list-view-btn').removeClass('active');                      
                """),format.raw/*198.17*/("""}"""),format.raw/*198.18*/("""
                updatePage();               
            """),format.raw/*200.13*/("""}"""),format.raw/*200.14*/(""");
            
            //Function to unify the changing of the href for the next/previous links. Called on button activation for
            //viewMode style, as well as on initial load of page.
            function updatePage() """),format.raw/*204.35*/("""{"""),format.raw/*204.36*/("""
"""),_display_(Seq[Any](/*205.2*/if(play.Play.application().configuration().getBoolean("sortInMemory")&&space.isDefined)/*205.89*/ {_display_(Seq[Any](format.raw/*205.91*/("""
            """),_display_(Seq[Any](/*206.14*/if(next != "")/*206.28*/ {_display_(Seq[Any](format.raw/*206.30*/("""
                $('#nextlink').attr('href', """"),_display_(Seq[Any](/*207.47*/(routes.Collections.sortedListInSpace(space.getOrElse(""), next.toInt, limit)))),format.raw/*207.125*/("""");
            """)))})),format.raw/*208.14*/("""
            """),_display_(Seq[Any](/*209.14*/if(prev != "")/*209.28*/ {_display_(Seq[Any](format.raw/*209.30*/("""
                $('#prevlink').attr('href', """"),_display_(Seq[Any](/*210.47*/(routes.Collections.sortedListInSpace(space.getOrElse(""), prev.toInt-limit, limit)))),format.raw/*210.131*/("""");
    """)))})),format.raw/*211.6*/("""
""")))}/*212.3*/else/*212.8*/{_display_(Seq[Any](format.raw/*212.9*/("""
                $('#nextlink').attr('href', """"),_display_(Seq[Any](/*213.47*/(routes.Collections.list("a", next, limit, space, "", owner)))),format.raw/*213.108*/("""");
                $('#prevlink').attr('href', """"),_display_(Seq[Any](/*214.47*/(routes.Collections.list("b", prev, limit, space, "", owner)))),format.raw/*214.108*/("""");
""")))})),format.raw/*215.2*/("""
			doSummarizeAbstracts();
            """),format.raw/*217.13*/("""}"""),format.raw/*217.14*/("""
        </script>
    </div>
</div>


  <div class="row hidden" id="tile-view">
    <div class="col-md-12">
        <div id="masonry">
            """),_display_(Seq[Any](/*226.14*/collectionsList/*226.29*/.map/*226.33*/ { collection =>_display_(Seq[Any](format.raw/*226.49*/("""
                """),_display_(Seq[Any](/*227.18*/collections/*227.29*/.tile(collection, routes.Collections.list(when, date, limit, space, "", owner), space, "col-lg-3 col-md-3 col-sm-3", false))),format.raw/*227.152*/("""
            """)))})),format.raw/*228.14*/("""
        </div>
    </div>
</div>


  <div class="row hidden" id="list-view">
	<div class="col-md-12">
        """),_display_(Seq[Any](/*236.10*/collectionsList/*236.25*/.map/*236.29*/ { collection =>_display_(Seq[Any](format.raw/*236.45*/("""
           """),_display_(Seq[Any](/*237.13*/collections/*237.24*/.listitem(collection, routes.Collections.list(when, date, limit, space, "", owner)))),format.raw/*237.107*/("""
        """)))})),format.raw/*238.10*/("""
	</div>
</div>

<div class="row">
    <div class="col-md-12">
        <ul class="pager">
            """),_display_(Seq[Any](/*245.14*/if(prev != "")/*245.28*/ {_display_(Seq[Any](format.raw/*245.30*/("""
                <li class="previous"><a class="btn btn-link" id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
  			""")))})),format.raw/*247.7*/("""
  			"""),_display_(Seq[Any](/*248.7*/if(next != "")/*248.21*/ {_display_(Seq[Any](format.raw/*248.23*/("""
                <li class ="next"><a class="btn btn-link" id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
  			""")))})),format.raw/*250.7*/("""
        </ul>
    </div>
</div>

<script src=""""),_display_(Seq[Any](/*255.15*/routes/*255.21*/.Assets.at("javascripts/descriptionSummary.js"))),format.raw/*255.68*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*256.15*/routes/*256.21*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*256.66*/("""" type="text/javascript"></script>

""")))})),format.raw/*258.2*/("""
"""))}
    }
    
    def render(collectionsList:List[models.Collection],prev:String,next:String,limit:Int,mode:Option[String],space:Option[String],spaceName:Option[String],title:Option[String],owner:Option[String],ownerName:Option[String],when:String,date:String,showTrash:Boolean,flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(collectionsList,prev,next,limit,mode,space,spaceName,title,owner,ownerName,when,date,showTrash)(flash,user)
    
    def f:((List[models.Collection],String,String,Int,Option[String],Option[String],Option[String],Option[String],Option[String],Option[String],String,String,Boolean) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (collectionsList,prev,next,limit,mode,space,spaceName,title,owner,ownerName,when,date,showTrash) => (flash,user) => apply(collectionsList,prev,next,limit,mode,space,spaceName,title,owner,ownerName,when,date,showTrash)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:24 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collectionList.scala.html
                    HASH: fd0b7f47d562f4e4564a83572d036393c9b75d85
                    MATRIX: 783->1|1247->348|1283->373|1327->409|1366->411|1403->414|1414->418|1445->428|1500->448|1514->454|1585->504|1669->553|1683->559|1744->599|1828->648|1842->654|1903->694|1988->743|2003->749|2067->791|2152->840|2167->846|2231->888|2316->937|2331->943|2395->985|2534->1088|2561->1106|2576->1112|2587->1131|2622->1157|2662->1159|2781->1242|2796->1248|2852->1281|2893->1285|2917->1286|2963->1330|2986->1344|3012->1361|3062->1375|3089->1393|3104->1399|3115->1418|3151->1445|3191->1447|3308->1528|3323->1534|3371->1559|3412->1563|3437->1565|3483->1609|3505->1622|3531->1639|3738->1810|3796->1846|3847->1861|3869->1874|3908->1875|3961->1892|4104->2012|4138->2028|4151->2033|4190->2034|4243->2051|4385->2170|4431->2184|5409->3127|5488->3197|5528->3199|5567->3203|5580->3208|5595->3214|5605->3221|5629->3236|5669->3238|6323->3864|6352->3865|6411->3896|6440->3897|6501->3930|6530->3931|6663->4036|6692->4037|6831->4149|6859->4150|7002->4257|7025->4258|7118->4323|7147->4324|7221->4371|7249->4372|7322->4417|7351->4418|7482->4521|7511->4522|7548->4531|7577->4532|7665->4585|7688->4599|7728->4601|7851->4688|7864->4692|7892->4698|7932->4701|7960->4706|7988->4717|8001->4722|8040->4723|8167->4813|8195->4818|8237->4828|8307->4870|8336->4871|8369->4876|8398->4877|8435->4900|8457->4912|8483->4921|8517->4923|8564->4933|8578->4937|8594->4943|8606->4958|8631->4973|8672->4975|8727->4993|8751->5007|8767->5013|8779->5036|8815->5062|8856->5064|8919->5090|9030->5190|9072->5192|9139->5222|9187->5260|9228->5262|9332->5329|9348->5335|9405->5368|9495->5420|9573->5474|9696->5560|9748->5588|9815->5622|9874->5648|9916->5691|9946->5711|9987->5713|10050->5739|10161->5839|10203->5841|10306->5907|10322->5913|10379->5946|10469->5998|10547->6052|10670->6138|10722->6166|10785->6196|10827->6239|10857->6259|10898->6261|10961->6287|11009->6325|11050->6327|11117->6357|11140->6370|11180->6371|11313->6467|11415->6545|11591->6683|11674->6742|11761->6810|11775->6815|11815->6816|11918->6882|11934->6888|11991->6921|12081->6973|12159->7027|12282->7113|12334->7141|12401->7175|12461->7202|12503->7246|12527->7260|12568->7262|12706->7362|12723->7368|12780->7401|12828->7411|12906->7465|12992->7513|13044->7541|13104->7585|13138->7613|13157->7622|13184->7636|13307->7722|13321->7726|13362->7744|13480->7833|13510->7834|13622->7917|13652->7918|14047->8284|14077->8285|14117->8296|14147->8297|14229->8350|14259->8351|14448->8511|14478->8512|14526->8531|14556->8532|14641->8588|14671->8589|15145->9034|15175->9035|15215->9046|15245->9047|15293->9066|15323->9067|15399->9114|15429->9115|15515->9172|15545->9173|15707->9306|15737->9307|15777->9318|15807->9319|15879->9362|15909->9363|16215->9640|16245->9641|16296->9663|16326->9664|16632->9941|16662->9942|16749->10000|16779->10001|17042->10235|17072->10236|17110->10238|17207->10325|17248->10327|17299->10341|17323->10355|17364->10357|17448->10404|17550->10482|17600->10499|17651->10513|17675->10527|17716->10529|17800->10576|17908->10660|17949->10669|17970->10672|17983->10677|18022->10678|18106->10725|18191->10786|18278->10836|18363->10897|18400->10902|18469->10942|18499->10943|18685->11092|18710->11107|18724->11111|18779->11127|18834->11145|18855->11156|19002->11279|19049->11293|19198->11405|19223->11420|19237->11424|19292->11440|19342->11453|19363->11464|19470->11547|19513->11557|19653->11660|19677->11674|19718->11676|19937->11863|19980->11870|20004->11884|20045->11886|20257->12066|20342->12114|20358->12120|20428->12167|20514->12216|20530->12222|20598->12267|20667->12304
                    LINES: 20->1|24->1|25->3|25->3|25->3|27->5|27->5|27->5|29->7|29->7|29->7|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|33->11|33->11|33->11|34->12|34->12|34->12|38->16|38->16|38->16|38->17|38->17|38->17|39->18|39->18|39->18|39->18|39->18|40->20|40->20|40->21|41->22|41->22|41->22|41->23|41->23|41->23|42->24|42->24|42->24|42->24|42->24|43->26|43->26|43->27|50->34|50->34|51->35|51->35|51->35|52->36|52->36|54->38|54->38|54->38|55->39|55->39|56->40|76->60|76->60|76->60|77->61|77->61|77->61|77->62|77->62|77->62|91->76|91->76|92->77|92->77|92->77|92->77|95->80|95->80|98->83|98->83|102->87|102->87|104->89|104->89|106->91|106->91|107->92|107->92|110->95|110->95|110->95|110->95|112->97|112->97|112->97|113->98|113->98|113->98|113->98|113->98|114->99|114->99|114->99|115->100|115->100|116->101|118->103|118->103|119->104|119->104|121->107|121->107|121->108|122->109|123->110|123->110|123->110|123->111|123->111|123->111|124->112|124->112|124->112|124->113|124->113|124->113|125->114|125->114|125->114|126->115|126->115|126->115|128->117|128->117|128->117|128->117|128->117|129->118|129->118|130->119|131->120|132->122|132->122|132->122|133->123|133->123|133->123|134->124|134->124|134->124|134->124|134->124|135->125|135->125|136->126|137->128|137->128|137->128|138->129|138->129|138->129|139->130|139->130|139->130|140->131|140->131|141->132|141->132|143->134|143->134|143->134|144->135|144->135|144->135|144->135|144->135|145->136|145->136|146->137|148->139|149->142|149->142|149->142|150->143|150->143|150->143|150->143|150->143|150->143|150->143|151->145|152->148|152->148|152->150|156->154|156->154|156->154|159->157|159->157|160->158|160->158|167->165|167->165|167->165|167->165|168->166|168->166|172->170|172->170|173->171|173->171|174->172|174->172|182->180|182->180|182->180|182->180|183->181|183->181|184->182|184->182|186->184|186->184|188->186|188->186|188->186|188->186|189->187|189->187|194->192|194->192|195->193|195->193|200->198|200->198|202->200|202->200|206->204|206->204|207->205|207->205|207->205|208->206|208->206|208->206|209->207|209->207|210->208|211->209|211->209|211->209|212->210|212->210|213->211|214->212|214->212|214->212|215->213|215->213|216->214|216->214|217->215|219->217|219->217|228->226|228->226|228->226|228->226|229->227|229->227|229->227|230->228|238->236|238->236|238->236|238->236|239->237|239->237|239->237|240->238|247->245|247->245|247->245|249->247|250->248|250->248|250->248|252->250|257->255|257->255|257->255|258->256|258->256|258->256|260->258
                    -- GENERATED --
                */
            