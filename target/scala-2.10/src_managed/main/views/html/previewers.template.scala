
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object previewers extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Array[models.Previewer],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(previewers: Array[models.Previewer]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.39*/("""

"""),_display_(Seq[Any](/*3.2*/main("Previewers")/*3.20*/ {_display_(Seq[Any](format.raw/*3.22*/("""
	<div class="page-header">
		<h1>Available Previewers</h1>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>id</th>
						<th>path</th>
						<th>main</th>
					</tr>
				</thead>
				<tbody>
					"""),_display_(Seq[Any](/*18.7*/previewers/*18.17*/.map/*18.21*/ { previewer =>_display_(Seq[Any](format.raw/*18.36*/("""
						<tr>
							<td><a href=""""),_display_(Seq[Any](/*20.22*/(routes.Assets.at(previewer.path + "/package.json")))),format.raw/*20.74*/("""">"""),_display_(Seq[Any](/*20.77*/previewer/*20.86*/.id)),format.raw/*20.89*/("""</a></td>
							<td>"""),_display_(Seq[Any](/*21.13*/previewer/*21.22*/.path)),format.raw/*21.27*/("""</td>
							<td>"""),_display_(Seq[Any](/*22.13*/previewer/*22.22*/.main)),format.raw/*22.27*/("""</td>
						</tr>
					""")))})),format.raw/*24.7*/("""
				</tbody>
			</table>
		</div>
	</div>
""")))})))}
    }
    
    def render(previewers:Array[models.Previewer]): play.api.templates.HtmlFormat.Appendable = apply(previewers)
    
    def f:((Array[models.Previewer]) => play.api.templates.HtmlFormat.Appendable) = (previewers) => apply(previewers)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/previewers.scala.html
                    HASH: 0ebc22fcd6c26e25d618b15e16f81eef8174ee6e
                    MATRIX: 609->1|740->38|777->41|803->59|842->61|1164->348|1183->358|1196->362|1249->377|1318->410|1392->462|1431->465|1449->474|1474->477|1532->499|1550->508|1577->513|1631->531|1649->540|1676->545|1731->569
                    LINES: 20->1|23->1|25->3|25->3|25->3|40->18|40->18|40->18|40->18|42->20|42->20|42->20|42->20|42->20|43->21|43->21|43->21|44->22|44->22|44->22|46->24
                    -- GENERATED --
                */
            