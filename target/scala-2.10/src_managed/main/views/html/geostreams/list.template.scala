
package views.html.geostreams

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object list extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[play.api.libs.json.JsValue],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(sensors: List[play.api.libs.json.JsValue])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services._

def /*3.2*/title/*3.7*/ = {{
    AppConfiguration.getSensorsTitle
}};
Seq[Any](format.raw/*1.81*/("""
"""),format.raw/*5.2*/("""
"""),_display_(Seq[Any](/*6.2*/main(title)/*6.13*/ {_display_(Seq[Any](format.raw/*6.15*/("""

    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*8.35*/routes/*8.41*/.Assets.at("stylesheets/leaflet.css"))),format.raw/*8.78*/("""">
    <!--[if lte IE 8]>
        <link rel="stylesheet" href=""""),_display_(Seq[Any](/*10.39*/routes/*10.45*/.Assets.at("stylesheets/leaflet.ie.css"))),format.raw/*10.85*/("""">
    <![endif]-->
    <script src=""""),_display_(Seq[Any](/*12.19*/routes/*12.25*/.Assets.at("javascripts/jquery.tablesorter.js"))),format.raw/*12.72*/("""" type="text/javascript"></script>

	<div class="page-header">
		<h1>"""),_display_(Seq[Any](/*15.8*/(AppConfiguration.getSensorsTitle))),format.raw/*15.42*/("""</h1>
	</div>
	<table class="table table-striped table-hover table-bordered tablesorter" id="sensorsListTable">
        <thead>
            <tr>
                <th>ID <span class="glyphicon glyphicon-sort pull-right" aria-hidden="true"></span></th>
                <th>"""),_display_(Seq[Any](/*21.22*/(AppConfiguration.getSensorTitle))),format.raw/*21.55*/(""" <span class="glyphicon glyphicon-sort pull-right" aria-hidden="true"></span></th>
                <th>Source <span class="glyphicon glyphicon-sort pull-right" aria-hidden="true"></span></th>
                <th>GeoJSON <span class="glyphicon glyphicon-sort pull-right" aria-hidden="true"></span></th>
                <th>Info<span class="glyphicon glyphicon-sort pull-right" aria-hidden="true"></span></th>

            </tr>
        </thead>
        <tbody>
            """),_display_(Seq[Any](/*29.14*/sensors/*29.21*/.map/*29.25*/ { item =>_display_(Seq[Any](format.raw/*29.35*/("""
                <tr>
                    <td>"""),_display_(Seq[Any](/*31.26*/(item \ "id"))),format.raw/*31.39*/("""</td>
                    <td>"""),_display_(Seq[Any](/*32.26*/((item \ "name").as[String]))),format.raw/*32.54*/("""</td>
                    <td>"""),_display_(Seq[Any](/*33.26*/((item \ "properties" \ "type" \ "id").asOpt[String].getOrElse("").toUpperCase()))),format.raw/*33.107*/("""</td>
                    <td>"""),_display_(Seq[Any](/*34.26*/(item \ "geometry"))),format.raw/*34.45*/("""</td>
                    <td>
                        <a class="btn btn-link disabled edit-sensor" href=""""),_display_(Seq[Any](/*36.77*/(routes.Geostreams.edit(((item \ "id").toString()))))),format.raw/*36.129*/(""""><span class="glyphicon glyphicon-edit"></span> Edit</a>
                        <button type="button" class="btn btn-link delete-sensor" id=""""),_display_(Seq[Any](/*37.87*/((item \ "id").toString()))),format.raw/*37.113*/("""" disabled><span class="glyphicon glyphicon-trash"></span> Delete</button>
                    </td>
                </tr>
            """)))})),format.raw/*40.14*/("""
        </tbody>
	</table>
    <script type="text/javascript">
        $(document).ready(function()"""),format.raw/*44.37*/("""{"""),format.raw/*44.38*/("""
            $("#sensorsListTable").tablesorter("""),format.raw/*45.48*/("""{"""),format.raw/*45.49*/("""
                sortList: [[1,0]]
            """),format.raw/*47.13*/("""}"""),format.raw/*47.14*/(""");
            """),_display_(Seq[Any](/*48.14*/if(user)/*48.22*/ {_display_(Seq[Any](format.raw/*48.24*/("""
                $('a.edit-sensor').removeClass('disabled');
                $('.delete-sensor').prop('disabled', false);
            """)))})),format.raw/*51.14*/("""
            $('.delete-sensor').on('click', function (event) """),format.raw/*52.62*/("""{"""),format.raw/*52.63*/("""
                if (confirm("Delete Sensor, all its Streams, and all its Datapoints?")) """),format.raw/*53.89*/("""{"""),format.raw/*53.90*/("""
                    var request = jsRoutes.api.Geostreams.deleteSensor(event.target.id).ajax("""),format.raw/*54.94*/("""{"""),format.raw/*54.95*/("""
                        type: 'DELETE',
                        contentType: "application/json",
                        dataType: 'json',
                        data: '"""),format.raw/*58.32*/("""{"""),format.raw/*58.33*/("""}"""),format.raw/*58.34*/("""'
                    """),format.raw/*59.21*/("""}"""),format.raw/*59.22*/(""");
                    request.done(function(response, textStatus, jqXHR) """),format.raw/*60.72*/("""{"""),format.raw/*60.73*/("""
                        window.location.href = window.location.href;
                    """),format.raw/*62.21*/("""}"""),format.raw/*62.22*/(""");
                    request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*63.75*/("""{"""),format.raw/*63.76*/("""
                        window.alert("Could not delete the sensor.");
                        console.error("Could not delete the sensor: " + textStatus + errorThrown);
                    """),format.raw/*66.21*/("""}"""),format.raw/*66.22*/(""");
                """),format.raw/*67.17*/("""}"""),format.raw/*67.18*/("""
            """),format.raw/*68.13*/("""}"""),format.raw/*68.14*/(""");
        """),format.raw/*69.9*/("""}"""),format.raw/*69.10*/(""");
    </script>
""")))})),format.raw/*71.2*/("""

"""))}
    }
    
    def render(sensors:List[play.api.libs.json.JsValue],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(sensors)(user)
    
    def f:((List[play.api.libs.json.JsValue]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (sensors) => (user) => apply(sensors)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/geostreams/list.scala.html
                    HASH: a843eba5b1a2cf54f2e8ed4ef774a9ce8900e3fc
                    MATRIX: 643->1|817->101|829->106|903->80|930->150|966->152|985->163|1024->165|1095->201|1109->207|1167->244|1267->308|1282->314|1344->354|1418->392|1433->398|1502->445|1607->515|1663->549|1970->820|2025->853|2534->1326|2550->1333|2563->1337|2611->1347|2694->1394|2729->1407|2796->1438|2846->1466|2913->1497|3017->1578|3084->1609|3125->1628|3268->1735|3343->1787|3523->1931|3572->1957|3740->2093|3868->2193|3897->2194|3973->2242|4002->2243|4077->2290|4106->2291|4158->2307|4175->2315|4215->2317|4382->2452|4472->2514|4501->2515|4618->2604|4647->2605|4769->2699|4798->2700|4997->2871|5026->2872|5055->2873|5105->2895|5134->2896|5236->2970|5265->2971|5383->3061|5412->3062|5517->3139|5546->3140|5764->3330|5793->3331|5840->3350|5869->3351|5910->3364|5939->3365|5977->3376|6006->3377|6055->3395
                    LINES: 20->1|23->3|23->3|26->1|27->5|28->6|28->6|28->6|30->8|30->8|30->8|32->10|32->10|32->10|34->12|34->12|34->12|37->15|37->15|43->21|43->21|51->29|51->29|51->29|51->29|53->31|53->31|54->32|54->32|55->33|55->33|56->34|56->34|58->36|58->36|59->37|59->37|62->40|66->44|66->44|67->45|67->45|69->47|69->47|70->48|70->48|70->48|73->51|74->52|74->52|75->53|75->53|76->54|76->54|80->58|80->58|80->58|81->59|81->59|82->60|82->60|84->62|84->62|85->63|85->63|88->66|88->66|89->67|89->67|90->68|90->68|91->69|91->69|93->71
                    -- GENERATED --
                */
            