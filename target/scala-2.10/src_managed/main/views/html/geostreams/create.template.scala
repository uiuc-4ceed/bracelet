
package views.html.geostreams

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object create extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(title: String = "Create")(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import services._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.64*/("""

"""),format.raw/*4.75*/("""
"""),format.raw/*6.1*/("""
"""),_display_(Seq[Any](/*7.2*/main(title)/*7.13*/ {_display_(Seq[Any](format.raw/*7.15*/("""
    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*8.35*/routes/*8.41*/.Assets.at("stylesheets/leaflet.css"))),format.raw/*8.78*/("""">
    <div class="container">
        <div class="page-header">
            <h1>"""),_display_(Seq[Any](/*11.18*/(AppConfiguration.getSensorTitle))),format.raw/*11.51*/(""" Information</h1>
        </div>

        <div id="sensor-form"></div>

        <div id="instruments">
                <!-- additional instruments will be added here as the "Add Instrument" button is clicked -->
        </div>

        <div class="btn btn-primary" id="addInstrument"><span class="glyphicon glyphicon-plus"></span> Add Instrument</div>

        <button type="submit" class="btn btn-primary submit" id="formSubmit"><span class="glyphicon glyphicon-send"></span> Create</button>

    </div>

""")))})),format.raw/*26.2*/("""

<script src=""""),_display_(Seq[Any](/*28.15*/routes/*28.21*/.Assets.at("javascripts/leaflet.js"))),format.raw/*28.57*/(""""></script>
<script src=""""),_display_(Seq[Any](/*29.15*/routes/*29.21*/.Assets.at("javascripts/jquery.validate.js"))),format.raw/*29.65*/(""""></script>
<script src=""""),_display_(Seq[Any](/*30.15*/routes/*30.21*/.Assets.at("javascripts/handlebars-v1.3.0.js"))),format.raw/*30.67*/(""""></script>
<script src=""""),_display_(Seq[Any](/*31.15*/routes/*31.21*/.Assets.at("javascripts/handlebars-loader.js"))),format.raw/*31.67*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*32.15*/routes/*32.21*/.Assets.at("javascripts/lib/jquery.serializejson.min.js"))),format.raw/*32.78*/(""""></script>
<script type="text/javascript">
    Handlebars.registerHelper('select', function( value, options )"""),format.raw/*34.67*/("""{"""),format.raw/*34.68*/("""
        var $el = $('<select />').html( options.fn(this) );
        $el.find('[value="' + value + '"]').attr("""),format.raw/*36.50*/("""{"""),format.raw/*36.51*/("""'selected':'selected'"""),format.raw/*36.72*/("""}"""),format.raw/*36.73*/(""");
        return $el.html();
    """),format.raw/*38.5*/("""}"""),format.raw/*38.6*/(""");

    Handlebars.registerHelper('slugify', function(string) """),format.raw/*40.59*/("""{"""),format.raw/*40.60*/("""
        return string.replace(/[\s]+/g, "-").replace(/[^\w|-]+/g, "").toLowerCase();
    """),format.raw/*42.5*/("""}"""),format.raw/*42.6*/(""");

    var insertSensorForm = function(data) """),format.raw/*44.43*/("""{"""),format.raw/*44.44*/("""
        var sensorTemplate = Handlebars.getTemplate(""""),_display_(Seq[Any](/*45.55*/routes/*45.61*/.Assets.at("templates/sensors/sensor-form"))),format.raw/*45.104*/("""");
        $("#sensor-form").append(sensorTemplate(data));

        $("#sensorLocation").on('keyup', function() """),format.raw/*48.53*/("""{"""),format.raw/*48.54*/("""
            updateMap()
        """),format.raw/*50.9*/("""}"""),format.raw/*50.10*/(""");
    """),format.raw/*51.5*/("""}"""),format.raw/*51.6*/(""";

    var insertInstrumentForm = function(data) """),format.raw/*53.47*/("""{"""),format.raw/*53.48*/("""
        var parametersTemplate = Handlebars.getTemplate(""""),_display_(Seq[Any](/*54.59*/routes/*54.65*/.Assets.at("templates/sensors/parameters-form"))),format.raw/*54.112*/("""");
        Handlebars.registerPartial('parameters', parametersTemplate);
        var instrumentTemplate = Handlebars.getTemplate(""""),_display_(Seq[Any](/*56.59*/routes/*56.65*/.Assets.at("templates/sensors/stream-form"))),format.raw/*56.108*/("""");
        $("#instruments").append(instrumentTemplate(data));
    """),format.raw/*58.5*/("""}"""),format.raw/*58.6*/(""";

    $(document).ready(function() """),format.raw/*60.34*/("""{"""),format.raw/*60.35*/("""
        var sensorsTitle = """"),_display_(Seq[Any](/*61.30*/(AppConfiguration.getSensorsTitle))),format.raw/*61.64*/("""";
        var sensorTitle = """"),_display_(Seq[Any](/*62.29*/(AppConfiguration.getSensorTitle))),format.raw/*62.62*/("""";

        var sensorJson = """),format.raw/*64.26*/("""{"""),format.raw/*64.27*/("""
            sensorsTitle: sensorsTitle,
            sensorTitle: sensorTitle,
            sensorTypes: """),format.raw/*67.26*/("""{"""),format.raw/*67.27*/("""
                1: "1 Instrument, 1 Measurement, No Depth, No Time-Series",
                2: "1 Instrument, 1 Measurement, No Depth, Yes Time-Series",
                3: "1 Instrument, Many Measurements, No Depth, No Time-Series",
                4: "1 Instrument, Many Measurements, No Depth, Yes Time-Series",
                5: "Many Instruments, 1 Measurement, Many Depths, Yes Time-Series",
                6: "Many Instruments, Many Measurements, Many Depths, Yes Time-Series",
                7: "1 Instrument, Many Measurements, One Depth, Yes Time-Series"
            """),format.raw/*75.13*/("""}"""),format.raw/*75.14*/("""
        """),format.raw/*76.9*/("""}"""),format.raw/*76.10*/(""";
        insertSensorForm(sensorJson);

    """),format.raw/*79.5*/("""}"""),format.raw/*79.6*/(""");
</script>
<script src=""""),_display_(Seq[Any](/*81.15*/routes/*81.21*/.Assets.at("javascripts/geostreams/sensorCreate.js"))),format.raw/*81.73*/(""""></script>
<script src=""""),_display_(Seq[Any](/*82.15*/routes/*82.21*/.Assets.at("javascripts/geostreams/updateMap.js"))),format.raw/*82.70*/(""""></script>
"""))}
    }
    
    def render(title:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(title)(user)
    
    def f:((String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (title) => (user) => apply(title)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/geostreams/create.scala.html
                    HASH: f61567b651727283ff090e056a6d558b2622be9e
                    MATRIX: 619->1|802->83|834->107|913->63|942->156|969->176|1005->178|1024->189|1063->191|1133->226|1147->232|1205->269|1323->351|1378->384|1916->891|1968->907|1983->913|2041->949|2103->975|2118->981|2184->1025|2246->1051|2261->1057|2329->1103|2391->1129|2406->1135|2474->1181|2559->1230|2574->1236|2653->1293|2791->1403|2820->1404|2958->1514|2987->1515|3036->1536|3065->1537|3126->1571|3154->1572|3244->1634|3273->1635|3390->1725|3418->1726|3492->1772|3521->1773|3612->1828|3627->1834|3693->1877|3834->1990|3863->1991|3923->2024|3952->2025|3986->2032|4014->2033|4091->2082|4120->2083|4215->2142|4230->2148|4300->2195|4468->2327|4483->2333|4549->2376|4644->2444|4672->2445|4736->2481|4765->2482|4831->2512|4887->2546|4954->2577|5009->2610|5066->2639|5095->2640|5227->2744|5256->2745|5864->3325|5893->3326|5929->3335|5958->3336|6030->3381|6058->3382|6121->3409|6136->3415|6210->3467|6272->3493|6287->3499|6358->3548
                    LINES: 20->1|25->4|25->4|26->1|28->4|29->6|30->7|30->7|30->7|31->8|31->8|31->8|34->11|34->11|49->26|51->28|51->28|51->28|52->29|52->29|52->29|53->30|53->30|53->30|54->31|54->31|54->31|55->32|55->32|55->32|57->34|57->34|59->36|59->36|59->36|59->36|61->38|61->38|63->40|63->40|65->42|65->42|67->44|67->44|68->45|68->45|68->45|71->48|71->48|73->50|73->50|74->51|74->51|76->53|76->53|77->54|77->54|77->54|79->56|79->56|79->56|81->58|81->58|83->60|83->60|84->61|84->61|85->62|85->62|87->64|87->64|90->67|90->67|98->75|98->75|99->76|99->76|102->79|102->79|104->81|104->81|104->81|105->82|105->82|105->82
                    -- GENERATED --
                */
            