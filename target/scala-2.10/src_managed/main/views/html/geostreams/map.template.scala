
package views.html.geostreams

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object map extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[play.api.libs.json.JsValue],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(sensors: List[play.api.libs.json.JsValue])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services._

import play.api.libs.json.Json


Seq[Any](format.raw/*1.81*/("""
"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Sensors Map")/*5.21*/ {_display_(Seq[Any](format.raw/*5.23*/("""

 <link rel="stylesheet" href=""""),_display_(Seq[Any](/*7.32*/routes/*7.38*/.Assets.at("stylesheets/leaflet.css"))),format.raw/*7.75*/("""">
 <!--[if lte IE 8]>
     <link rel="stylesheet" href=""""),_display_(Seq[Any](/*9.36*/routes/*9.42*/.Assets.at("stylesheets/leaflet.ie.css"))),format.raw/*9.82*/("""">
 <![endif]-->
 <script src=""""),_display_(Seq[Any](/*11.16*/routes/*11.22*/.Assets.at("javascripts/leaflet-src.js"))),format.raw/*11.62*/("""" type="text/javascript"></script>

	<div class="page-header">
		<h1>"""),_display_(Seq[Any](/*14.8*/(AppConfiguration.getSensorsTitle))),format.raw/*14.42*/("""</h1>
	</div>
	<div class="row">
		<div class="col-md-10">
			<div id="geostreams_map"></div>
		</div>
		<div class="col-md-2">
			<h3>Streams</h3>
			<div id="list_streams"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="padding-top:30px">
			<h3>Datapoints</h3>
			<pre id="datapoints" class="pre-scrollable"></pre>
		</div>
	</div>
""")))})),format.raw/*31.2*/("""
<script language="javascript">
   $(function() """),format.raw/*33.17*/("""{"""),format.raw/*33.18*/("""
	   var map = L.map('geostreams_map').setView([51.505, -0.09], 13);
	   
	   L.tileLayer('http://"""),format.raw/*36.25*/("""{"""),format.raw/*36.26*/("""s"""),format.raw/*36.27*/("""}"""),format.raw/*36.28*/(""".tile.openstreetmap.org/"""),format.raw/*36.52*/("""{"""),format.raw/*36.53*/("""z"""),format.raw/*36.54*/("""}"""),format.raw/*36.55*/("""/"""),format.raw/*36.56*/("""{"""),format.raw/*36.57*/("""x"""),format.raw/*36.58*/("""}"""),format.raw/*36.59*/("""/"""),format.raw/*36.60*/("""{"""),format.raw/*36.61*/("""y"""),format.raw/*36.62*/("""}"""),format.raw/*36.63*/(""".png', """),format.raw/*36.70*/("""{"""),format.raw/*36.71*/("""
	    attribution: 'Map data OpenStreetMap contributors',
	    maxZoom: 18
	   """),format.raw/*39.5*/("""}"""),format.raw/*39.6*/(""").addTo(map);
	   	   
	   var geojsonMarkerOptions = """),format.raw/*41.32*/("""{"""),format.raw/*41.33*/("""
		    radius: 3,
		    fillColor: "#ff7800",
		    color: "#000",
		    weight: 1,
		    opacity: 1,
		    fillOpacity: 0.8
		"""),format.raw/*48.3*/("""}"""),format.raw/*48.4*/(""";

	   
	        // prefacing with HTML prevents Play from escaping quotes and other JSON characters
            var geojsonLayer = L.geoJson("""),_display_(Seq[Any](/*52.43*/Html(Json.stringify(Json.toJson(sensors))))),format.raw/*52.85*/(""", """),format.raw/*52.87*/("""{"""),format.raw/*52.88*/("""

		        style: function (feature) """),format.raw/*54.37*/("""{"""),format.raw/*54.38*/("""
			        var color = """),format.raw/*55.24*/("""{"""),format.raw/*55.25*/("""fillColor: "#ff7800""""),format.raw/*55.45*/("""}"""),format.raw/*55.46*/(""";
			        var type = feature.properties.type;
		        	switch (type) """),format.raw/*57.26*/("""{"""),format.raw/*57.27*/("""
			            case 'epa':
			            	color = """),format.raw/*59.25*/("""{"""),format.raw/*59.26*/(""" fillColor: "#D53E4F""""),format.raw/*59.47*/("""}"""),format.raw/*59.48*/("""; 
				            break;
			            case 'gsfmp':
			            	color = """),format.raw/*62.25*/("""{"""),format.raw/*62.26*/(""" fillColor: "#FC8D59""""),format.raw/*62.47*/("""}"""),format.raw/*62.48*/("""; 
				            break;
			            case 'letg':
			            	color = """),format.raw/*65.25*/("""{"""),format.raw/*65.26*/(""" fillColor: "#99D594""""),format.raw/*65.47*/("""}"""),format.raw/*65.48*/("""; 
				            break;
			            case 'usgs':
			            	color = """),format.raw/*68.25*/("""{"""),format.raw/*68.26*/(""" fillColor: "#3288BD""""),format.raw/*68.47*/("""}"""),format.raw/*68.48*/("""; 
				            break;
				        default: color = """),format.raw/*70.30*/("""{"""),format.raw/*70.31*/("""fillColor: "#ff7800""""),format.raw/*70.51*/("""}"""),format.raw/*70.52*/(""";
	                """),format.raw/*71.18*/("""}"""),format.raw/*71.19*/("""
	                return color;
			    """),format.raw/*73.8*/("""}"""),format.raw/*73.9*/(""",
			    
			    pointToLayer: function (feature, latlng) """),format.raw/*75.49*/("""{"""),format.raw/*75.50*/("""
			        return L.circleMarker(latlng, geojsonMarkerOptions);
			    """),format.raw/*77.8*/("""}"""),format.raw/*77.9*/(""",
			    
			    onEachFeature: function (feature, layer) """),format.raw/*79.49*/("""{"""),format.raw/*79.50*/("""
			        layer.bindPopup(createPopup(feature, layer));
			    """),format.raw/*81.8*/("""}"""),format.raw/*81.9*/("""
			    
			"""),format.raw/*83.4*/("""}"""),format.raw/*83.5*/(""").addTo(map);
 			map.fitBounds(geojsonLayer.getBounds());

	     function createPopup(feature, layer) """),format.raw/*86.44*/("""{"""),format.raw/*86.45*/("""
			    var div = document.createElement('div');
		    	var link = document.createElement('a');
		    	link.innerHTML = "Streams";
		    	link.href = "#";
		    	link.onclick = function() """),format.raw/*91.34*/("""{"""),format.raw/*91.35*/("""
				    listStreams(feature.id);
				"""),format.raw/*93.5*/("""}"""),format.raw/*93.6*/(""";
				var info = document.createElement('div');
				info.innerHTML = 'Station: ' + feature.properties.popupContent +
                    '<br>Source: ' + feature.properties.type.id.toUpperCase();
				div.appendChild(info);
				div.appendChild(link);
				return div;
	     """),format.raw/*100.7*/("""}"""),format.raw/*100.8*/("""

	     function listStreams(sensor_id) """),format.raw/*102.39*/("""{"""),format.raw/*102.40*/("""
             var searchStreams = jsRoutes.api.Geostreams.getSensorStreams(sensor_id ).url;
	  	   var request = $.ajax("""),format.raw/*104.29*/("""{"""),format.raw/*104.30*/("""
               url: searchStreams,
		       type: 'GET',
		       contentType: "application/json",
		       dataType: 'json'
		     """),format.raw/*109.8*/("""}"""),format.raw/*109.9*/(""");

		  	 request.done(function (response, textStatus, jqXHR)"""),format.raw/*111.58*/("""{"""),format.raw/*111.59*/("""
			  	 var listStreamsElement = $('#list_streams');
			  	listStreamsElement.empty();
			  	for (var i = 0; i < response.length; i++) """),format.raw/*114.49*/("""{"""),format.raw/*114.50*/("""
					var stream_id = response[i]['stream_id']
			  		var stream_label = stream_id+" "+response[i]['name'];
			  		var link = document.createElement('a');
			    	link.href = "#";
			    	link.innerHTML = stream_label;
			    	link.onclick = function() """),format.raw/*120.35*/("""{"""),format.raw/*120.36*/("""
					   listDatapoints(stream_id);
					"""),format.raw/*122.6*/("""}"""),format.raw/*122.7*/(""";
			  	 listStreamsElement.append(link);
			  	 listStreamsElement.append(document.createElement('br'));
			  	"""),format.raw/*125.7*/("""}"""),format.raw/*125.8*/("""
		  	 """),format.raw/*126.7*/("""}"""),format.raw/*126.8*/(""");

	     	request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*128.62*/("""{"""),format.raw/*128.63*/("""
		        console.error("The following error occured: " + textStatus, errorThrown);
		     """),format.raw/*130.8*/("""}"""),format.raw/*130.9*/(""");
		     
	     """),format.raw/*132.7*/("""}"""),format.raw/*132.8*/("""

	     function listDatapoints(stream_id) """),format.raw/*134.42*/("""{"""),format.raw/*134.43*/("""
		       var searchDatapointsURL = jsRoutes.api.Geostreams.searchDatapoints("", null, null, null, stream_id, null, null, null, "json").url;
               var request = $.ajax("""),format.raw/*136.37*/("""{"""),format.raw/*136.38*/("""
                   url: searchDatapointsURL,
			  	   type: 'GET',
			       contentType: "application/json",
			       dataType: 'json',
			     """),format.raw/*141.9*/("""}"""),format.raw/*141.10*/(""");

		  	 request.done(function (response, textStatus, jqXHR)"""),format.raw/*143.58*/("""{"""),format.raw/*143.59*/("""
			  	var datapointsElement = $('#datapoints');
                datapointsElement.empty();
			  	datapointsElement.append(JSON.stringify(response, null, '   '));
		  	 """),format.raw/*147.7*/("""}"""),format.raw/*147.8*/(""");

	     	 request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*149.63*/("""{"""),format.raw/*149.64*/("""
		        console.error(
		            "The following error occured: "+
		            textStatus, errorThrown  
		        );
		     """),format.raw/*154.8*/("""}"""),format.raw/*154.9*/(""");
	     """),format.raw/*155.7*/("""}"""),format.raw/*155.8*/("""
   """),format.raw/*156.4*/("""}"""),format.raw/*156.5*/(""");
</script>"""))}
    }
    
    def render(sensors:List[play.api.libs.json.JsValue],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(sensors)(user)
    
    def f:((List[play.api.libs.json.JsValue]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (sensors) => (user) => apply(sensors)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/geostreams/map.scala.html
                    HASH: f7cac084df32c184a91923378035dcfb2cb4e0fb
                    MATRIX: 642->1|865->80|892->132|928->134|955->153|994->155|1062->188|1076->194|1134->231|1227->289|1241->295|1302->335|1370->367|1385->373|1447->413|1552->483|1608->517|2002->880|2078->928|2107->929|2233->1027|2262->1028|2291->1029|2320->1030|2372->1054|2401->1055|2430->1056|2459->1057|2488->1058|2517->1059|2546->1060|2575->1061|2604->1062|2633->1063|2662->1064|2691->1065|2726->1072|2755->1073|2861->1152|2889->1153|2971->1207|3000->1208|3154->1335|3182->1336|3361->1479|3425->1521|3455->1523|3484->1524|3550->1562|3579->1563|3631->1587|3660->1588|3708->1608|3737->1609|3839->1683|3868->1684|3948->1736|3977->1737|4026->1758|4055->1759|4162->1838|4191->1839|4240->1860|4269->1861|4375->1939|4404->1940|4453->1961|4482->1962|4588->2040|4617->2041|4666->2062|4695->2063|4778->2118|4807->2119|4855->2139|4884->2140|4931->2159|4960->2160|5026->2199|5054->2200|5140->2258|5169->2259|5268->2331|5296->2332|5382->2390|5411->2391|5503->2456|5531->2457|5570->2469|5598->2470|5729->2573|5758->2574|5974->2762|6003->2763|6068->2801|6096->2802|6396->3074|6425->3075|6494->3115|6524->3116|6673->3236|6703->3237|6864->3370|6893->3371|6983->3432|7013->3433|7177->3568|7207->3569|7489->3822|7519->3823|7588->3864|7617->3865|7757->3977|7786->3978|7821->3985|7850->3986|7944->4051|7974->4052|8094->4144|8123->4145|8168->4162|8197->4163|8269->4206|8299->4207|8505->4384|8535->4385|8710->4532|8740->4533|8830->4594|8860->4595|9057->4764|9086->4765|9181->4831|9211->4832|9372->4965|9401->4966|9438->4975|9467->4976|9499->4980|9528->4981
                    LINES: 20->1|26->1|27->4|28->5|28->5|28->5|30->7|30->7|30->7|32->9|32->9|32->9|34->11|34->11|34->11|37->14|37->14|54->31|56->33|56->33|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|59->36|62->39|62->39|64->41|64->41|71->48|71->48|75->52|75->52|75->52|75->52|77->54|77->54|78->55|78->55|78->55|78->55|80->57|80->57|82->59|82->59|82->59|82->59|85->62|85->62|85->62|85->62|88->65|88->65|88->65|88->65|91->68|91->68|91->68|91->68|93->70|93->70|93->70|93->70|94->71|94->71|96->73|96->73|98->75|98->75|100->77|100->77|102->79|102->79|104->81|104->81|106->83|106->83|109->86|109->86|114->91|114->91|116->93|116->93|123->100|123->100|125->102|125->102|127->104|127->104|132->109|132->109|134->111|134->111|137->114|137->114|143->120|143->120|145->122|145->122|148->125|148->125|149->126|149->126|151->128|151->128|153->130|153->130|155->132|155->132|157->134|157->134|159->136|159->136|164->141|164->141|166->143|166->143|170->147|170->147|172->149|172->149|177->154|177->154|178->155|178->155|179->156|179->156
                    -- GENERATED --
                */
            