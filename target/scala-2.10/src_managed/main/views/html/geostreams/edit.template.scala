
package views.html.geostreams

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object edit extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[play.api.libs.json.JsValue,List[play.api.libs.json.JsValue],List[models.MetadataDefinition],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(sensor: play.api.libs.json.JsValue, streams: List[play.api.libs.json.JsValue], definitions: List[models.MetadataDefinition])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.libs.json.Json

import helper._

import services._

implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.163*/("""
"""),format.raw/*3.1*/("""
"""),format.raw/*5.75*/("""
"""),format.raw/*7.1*/("""
"""),_display_(Seq[Any](/*8.2*/main("Edit")/*8.14*/ {_display_(Seq[Any](format.raw/*8.16*/("""
    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*9.35*/routes/*9.41*/.Assets.at("stylesheets/leaflet.css"))),format.raw/*9.78*/("""">
    <div class="container">
        <div class="page-header">
            <h1>"""),_display_(Seq[Any](/*12.18*/(AppConfiguration.getSensorTitle))),format.raw/*12.51*/(""" Information</h1>
        </div>

        <div id="sensor-form"></div>

        <br />
        <div id="instruments"></div>

        <div class="btn btn-primary" id="addInstrument" style="..."><span class="glyphicon glyphicon-plus"></span> Add Instrument</div>

        <button type="submit" class="btn btn-default submit" id="cancelSubmit"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
        <button type="submit" class="btn btn-primary submit" id="formSubmit"><span class="glyphicon glyphicon-send"></span> Save</button>



        </div>

""")))})),format.raw/*29.2*/("""

<link rel="stylesheet" href=""""),_display_(Seq[Any](/*31.31*/routes/*31.37*/.Assets.at("stylesheets/jquery-ui-timepicker-addon.css"))),format.raw/*31.93*/("""">
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*32.31*/routes/*32.37*/.Assets.at("stylesheets/chosen.css"))),format.raw/*32.73*/("""">
<script src=""""),_display_(Seq[Any](/*33.15*/routes/*33.21*/.Assets.at("javascripts/leaflet.js"))),format.raw/*33.57*/(""""></script>
<script src=""""),_display_(Seq[Any](/*34.15*/routes/*34.21*/.Assets.at("javascripts/jquery.validate.js"))),format.raw/*34.65*/(""""></script>
<script src=""""),_display_(Seq[Any](/*35.15*/routes/*35.21*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*35.63*/(""""></script>
<script src=""""),_display_(Seq[Any](/*36.15*/routes/*36.21*/.Assets.at("javascripts/lib/handlebars-v4.0.5.min.js"))),format.raw/*36.75*/(""""></script>
<script src=""""),_display_(Seq[Any](/*37.15*/routes/*37.21*/.Assets.at("javascripts/handlebars-loader.js"))),format.raw/*37.67*/(""""></script>
<script src=""""),_display_(Seq[Any](/*38.15*/routes/*38.21*/.Assets.at("javascripts/lib/jquery.serializejson.min.js"))),format.raw/*38.78*/(""""></script>
<script src=""""),_display_(Seq[Any](/*39.15*/routes/*39.21*/.Assets.at("javascripts/jquery-ui-timepicker-addon.js"))),format.raw/*39.76*/(""""></script>
<script src=""""),_display_(Seq[Any](/*40.15*/routes/*40.21*/.Assets.at("javascripts/metadata/definitionDropdowns.js"))),format.raw/*40.78*/(""""></script>
<script src=""""),_display_(Seq[Any](/*41.15*/routes/*41.21*/.Assets.at("javascripts/geostreams/sensorStreamEdit.js"))),format.raw/*41.77*/(""""></script>

<script type="text/javascript">
    Handlebars.registerHelper('select', function( value, options )"""),format.raw/*44.67*/("""{"""),format.raw/*44.68*/("""
        var $el = $('<select />').html( options.fn(this) );
        $el.find('[value="' + value + '"]').attr("""),format.raw/*46.50*/("""{"""),format.raw/*46.51*/("""'selected':'selected'"""),format.raw/*46.72*/("""}"""),format.raw/*46.73*/(""");
        return $el.html();
    """),format.raw/*48.5*/("""}"""),format.raw/*48.6*/(""");

    Handlebars.registerHelper('slugify', function(string) """),format.raw/*50.59*/("""{"""),format.raw/*50.60*/("""
        return string.replace(/[\s]+/g, "-").replace(/[^\w|-]+/g, "").toLowerCase();
    """),format.raw/*52.5*/("""}"""),format.raw/*52.6*/(""");

    var insertSensorForm = function(data) """),format.raw/*54.43*/("""{"""),format.raw/*54.44*/("""
        var sensorTemplate = Handlebars.getTemplate(""""),_display_(Seq[Any](/*55.55*/routes/*55.61*/.Assets.at("templates/sensors/sensor-form"))),format.raw/*55.104*/("""");
        // Convert GeoJSON object to json string, so Handlebars template doesn't render as [Object object]
        data["geometry"] = JSON.stringify(data["geometry"])
        $("#sensor-form").append(sensorTemplate(data));

        $("#sensorLocation").on('keyup', function() """),format.raw/*60.53*/("""{"""),format.raw/*60.54*/("""
            updateMap()
        """),format.raw/*62.9*/("""}"""),format.raw/*62.10*/(""");
    """),format.raw/*63.5*/("""}"""),format.raw/*63.6*/(""";

    var insertInstrumentForm = function(data) """),format.raw/*65.47*/("""{"""),format.raw/*65.48*/("""
        var parametersTemplate = Handlebars.getTemplate(""""),_display_(Seq[Any](/*66.59*/routes/*66.65*/.Assets.at("templates/sensors/parameters-form"))),format.raw/*66.112*/("""");
        Handlebars.registerPartial('parameters', parametersTemplate);
        var instrumentTemplate = Handlebars.getTemplate(""""),_display_(Seq[Any](/*68.59*/routes/*68.65*/.Assets.at("templates/sensors/stream-form"))),format.raw/*68.108*/("""");
        // Convert GeoJSON object to json string, so Handlebars template doesn't render as [Object object]
        data["geometry"] = JSON.stringify(data["geometry"])
        $("#instruments").append(instrumentTemplate(data));
    """),format.raw/*72.5*/("""}"""),format.raw/*72.6*/(""";

    $(document).ready(function() """),format.raw/*74.34*/("""{"""),format.raw/*74.35*/("""
        var sensorsTitle = """"),_display_(Seq[Any](/*75.30*/(AppConfiguration.getSensorsTitle))),format.raw/*75.64*/("""";
        var sensorTitle = """"),_display_(Seq[Any](/*76.29*/(AppConfiguration.getSensorTitle))),format.raw/*76.62*/("""";

        var sensorJson = """),_display_(Seq[Any](/*78.27*/Html(Json.stringify(Json.toJson(sensor))))),format.raw/*78.68*/(""";
        sensorJson.sensorsTitle = sensorsTitle;
        sensorJson.sensorTitle = sensorTitle;
        sensorJson.sensorTypes = """),format.raw/*81.34*/("""{"""),format.raw/*81.35*/("""
            1: "1 Instrument, 1 Measurement, No Depth, No Time-Series",
            2: "1 Instrument, 1 Measurement, No Depth, Yes Time-Series",
            3: "1 Instrument, Many Measurements, No Depth, No Time-Series",
            4: "1 Instrument, Many Measurements, No Depth, Yes Time-Series",
            5: "Many Instruments, 1 Measurement, Many Depths, Yes Time-Series",
            6: "Many Instruments, Many Measurements, Many Depths, Yes Time-Series",
            7: "1 Instrument, Many Measurements, One Depth, Yes Time-Series"
        """),format.raw/*89.9*/("""}"""),format.raw/*89.10*/(""";
        sensorJson.permalink = jsRoutes.api.Geostreams.getSensor(sensorJson.id).url;
        insertSensorForm(sensorJson);

        """),_display_(Seq[Any](/*93.10*/streams/*93.17*/.map/*93.21*/ { stream =>_display_(Seq[Any](format.raw/*93.33*/("""
            var streamJson = """),_display_(Seq[Any](/*94.31*/Html(Json.stringify(Json.toJson(stream))))),format.raw/*94.72*/(""";
            streamJson.sensorsTitle = sensorsTitle;
            streamJson.sensorTitle = sensorTitle;
            streamJson.formType = "edit";
            streamJson.permalink = jsRoutes.api.Geostreams.getStream(streamJson.id).url;
            streamJson.metadataDefinitions = """),_display_(Seq[Any](/*99.47*/(Html(Json.stringify(Json.toJson(definitions)))))),format.raw/*99.95*/(""";
            insertInstrumentForm(streamJson);
        """)))})),format.raw/*101.10*/("""

        $(".controlled-vocabulary-value").each(function(index, item) """),format.raw/*103.70*/("""{"""),format.raw/*103.71*/("""
            var targetID = $(item).data('id');
            var group = $(item).data('group');
            var definitionElement = $('select[data-id="' + targetID + '"][data-group="' + group + '"]');

            $(definitionElement).chosen("""),format.raw/*108.41*/("""{"""),format.raw/*108.42*/("""
                add_search_option: true,
                no_results_text: "Not found. Press enter to add ",
                search_contains: true,
                width: "100%",
                placeholder_text_single: "Select field"
            """),format.raw/*114.13*/("""}"""),format.raw/*114.14*/(""");

            var setupAutocomplete = function(selectElement, inputElement) """),format.raw/*116.75*/("""{"""),format.raw/*116.76*/("""
                var selectedOption = $(selectElement).find(':selected');
                var definition = """),format.raw/*118.34*/("""{"""),format.raw/*118.35*/("""
                    uri: selectedOption.val(),
                    type: selectedOption.data('type'),
                    definitions_url: selectedOption.data('definitions_url'),
                    query_parameter: selectedOption.data('query_parameter')
                """),format.raw/*123.17*/("""}"""),format.raw/*123.18*/(""";
                registerMDAutocomplete(inputElement, definition, selectElement);
            """),format.raw/*125.13*/("""}"""),format.raw/*125.14*/(""";

            $(definitionElement).on('change', function()"""),format.raw/*127.57*/("""{"""),format.raw/*127.58*/("""
                setupAutocomplete(this, item);
            """),format.raw/*129.13*/("""}"""),format.raw/*129.14*/(""");

            setupAutocomplete(definitionElement, item);
        """),format.raw/*132.9*/("""}"""),format.raw/*132.10*/(""");

    """),format.raw/*134.5*/("""}"""),format.raw/*134.6*/(""");
</script>
<script src=""""),_display_(Seq[Any](/*136.15*/routes/*136.21*/.Assets.at("javascripts/geostreams/updateMap.js"))),format.raw/*136.70*/(""""></script>
"""))}
    }
    
    def render(sensor:play.api.libs.json.JsValue,streams:List[play.api.libs.json.JsValue],definitions:List[models.MetadataDefinition],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(sensor,streams,definitions)(user)
    
    def f:((play.api.libs.json.JsValue,List[play.api.libs.json.JsValue],List[models.MetadataDefinition]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (sensor,streams,definitions) => (user) => apply(sensor,streams,definitions)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/geostreams/edit.scala.html
                    HASH: 5c2140218354d5da0276710a4bf678a633fb944a
                    MATRIX: 702->1|1016->214|1048->238|1128->162|1155->195|1183->287|1210->307|1246->309|1266->321|1305->323|1375->358|1389->364|1447->401|1565->483|1620->516|2215->1080|2283->1112|2298->1118|2376->1174|2445->1207|2460->1213|2518->1249|2571->1266|2586->1272|2644->1308|2706->1334|2721->1340|2787->1384|2849->1410|2864->1416|2928->1458|2990->1484|3005->1490|3081->1544|3143->1570|3158->1576|3226->1622|3288->1648|3303->1654|3382->1711|3444->1737|3459->1743|3536->1798|3598->1824|3613->1830|3692->1887|3754->1913|3769->1919|3847->1975|3986->2086|4015->2087|4153->2197|4182->2198|4231->2219|4260->2220|4321->2254|4349->2255|4439->2317|4468->2318|4585->2408|4613->2409|4687->2455|4716->2456|4807->2511|4822->2517|4888->2560|5196->2840|5225->2841|5285->2874|5314->2875|5348->2882|5376->2883|5453->2932|5482->2933|5577->2992|5592->2998|5662->3045|5830->3177|5845->3183|5911->3226|6173->3461|6201->3462|6265->3498|6294->3499|6360->3529|6416->3563|6483->3594|6538->3627|6604->3657|6667->3698|6824->3827|6853->3828|7428->4376|7457->4377|7628->4512|7644->4519|7657->4523|7707->4535|7774->4566|7837->4607|8154->4888|8224->4936|8314->4993|8414->5064|8444->5065|8714->5306|8744->5307|9020->5554|9050->5555|9157->5633|9187->5634|9323->5741|9353->5742|9654->6014|9684->6015|9808->6110|9838->6111|9926->6170|9956->6171|10045->6231|10075->6232|10171->6300|10201->6301|10237->6309|10266->6310|10330->6337|10346->6343|10418->6392
                    LINES: 20->1|27->5|27->5|28->1|29->3|30->5|31->7|32->8|32->8|32->8|33->9|33->9|33->9|36->12|36->12|53->29|55->31|55->31|55->31|56->32|56->32|56->32|57->33|57->33|57->33|58->34|58->34|58->34|59->35|59->35|59->35|60->36|60->36|60->36|61->37|61->37|61->37|62->38|62->38|62->38|63->39|63->39|63->39|64->40|64->40|64->40|65->41|65->41|65->41|68->44|68->44|70->46|70->46|70->46|70->46|72->48|72->48|74->50|74->50|76->52|76->52|78->54|78->54|79->55|79->55|79->55|84->60|84->60|86->62|86->62|87->63|87->63|89->65|89->65|90->66|90->66|90->66|92->68|92->68|92->68|96->72|96->72|98->74|98->74|99->75|99->75|100->76|100->76|102->78|102->78|105->81|105->81|113->89|113->89|117->93|117->93|117->93|117->93|118->94|118->94|123->99|123->99|125->101|127->103|127->103|132->108|132->108|138->114|138->114|140->116|140->116|142->118|142->118|147->123|147->123|149->125|149->125|151->127|151->127|153->129|153->129|156->132|156->132|158->134|158->134|160->136|160->136|160->136
                    -- GENERATED --
                */
            