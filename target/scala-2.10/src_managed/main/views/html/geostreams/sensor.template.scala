
package views.html.geostreams

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object sensor extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[play.api.libs.json.JsValue,String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(sensor: play.api.libs.json.JsValue, id: String):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services.AppConfiguration


Seq[Any](format.raw/*1.50*/("""
"""),_display_(Seq[Any](/*3.2*/main("Sensor")/*3.16*/ {_display_(Seq[Any](format.raw/*3.18*/("""
    <div class="page-header">
        <h1>"""),_display_(Seq[Any](/*5.14*/(AppConfiguration.getSensorTitle))),format.raw/*5.47*/("""</h1>
    </div>
    <div class="row"><div class="col-md-12">ID: <a href=""""),_display_(Seq[Any](/*7.59*/(api.routes.Geostreams.getSensor(id)))),format.raw/*7.96*/("""">"""),_display_(Seq[Any](/*7.99*/id)),format.raw/*7.101*/("""</a></div></div>
    <div class="row"><div class="col-md-12">Name: """),_display_(Seq[Any](/*8.52*/(sensor \ "name"))),format.raw/*8.69*/("""</div></div>
    <div class="row"><div class="col-md-12">Created: """),_display_(Seq[Any](/*9.55*/(sensor \ "created"))),format.raw/*9.75*/("""</div></div>
    """),format.raw/*10.104*/("""
    """),format.raw/*11.110*/("""
    <div class="row"><div class="col-md-12">Coordinates: """),_display_(Seq[Any](/*12.59*/(sensor \ "geometry" \ "coordinates"))),format.raw/*12.96*/("""</div></div>
    """),format.raw/*13.72*/("""
    """),format.raw/*14.63*/("""
        """),format.raw/*15.95*/("""
    """),format.raw/*16.10*/("""
""")))})))}
    }
    
    def render(sensor:play.api.libs.json.JsValue,id:String): play.api.templates.HtmlFormat.Appendable = apply(sensor,id)
    
    def f:((play.api.libs.json.JsValue,String) => play.api.templates.HtmlFormat.Appendable) = (sensor,id) => apply(sensor,id)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/geostreams/sensor.scala.html
                    HASH: 91efee3877a730a8fdd932417a12e6aa535ef042
                    MATRIX: 626->1|801->49|837->85|859->99|898->101|977->145|1031->178|1141->253|1199->290|1237->293|1261->295|1364->363|1402->380|1504->447|1545->467|1591->583|1625->693|1720->752|1779->789|1824->873|1857->936|1894->1031|1927->1041
                    LINES: 20->1|24->1|25->3|25->3|25->3|27->5|27->5|29->7|29->7|29->7|29->7|30->8|30->8|31->9|31->9|32->10|33->11|34->12|34->12|35->13|36->14|37->15|38->16
                    -- GENERATED --
                */
            