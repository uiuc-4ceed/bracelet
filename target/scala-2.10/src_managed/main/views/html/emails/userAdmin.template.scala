
package views.html.emails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object userAdmin extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[User,Boolean,Boolean,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(user: User, admin: Boolean, ssl: Boolean = true)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services.AppConfiguration


Seq[Any](format.raw/*1.84*/("""

"""),format.raw/*4.1*/("""
<html>
    <body>
        <p>"""),_display_(Seq[Any](/*7.13*/user/*7.17*/.fullName)),format.raw/*7.26*/(""",</p>
        <p>
            Your account on <a href=""""),_display_(Seq[Any](/*9.39*/routes/*9.45*/.Application.index().absoluteURL(ssl))),format.raw/*9.82*/("""" style="text-decoration: none">"""),_display_(Seq[Any](/*9.115*/AppConfiguration/*9.131*/.getDisplayName)),format.raw/*9.146*/("""</a>
            now """),_display_(Seq[Any](/*10.18*/if(!admin)/*10.28*/ {_display_(Seq[Any](format.raw/*10.30*/(""" no longer """)))})),format.raw/*10.42*/(""" has admin privileges.
        </p>
        """),_display_(Seq[Any](/*12.10*/footer())),format.raw/*12.18*/("""
    </body>
</html>
"""))}
    }
    
    def render(user:User,admin:Boolean,ssl:Boolean,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(user,admin,ssl)(request)
    
    def f:((User,Boolean,Boolean) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (user,admin,ssl) => (request) => apply(user,admin,ssl)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/emails/userAdmin.scala.html
                    HASH: a4fe9dfe19e46c55faa9d1357994f55650bbb6ec
                    MATRIX: 626->1|835->83|863->119|929->150|941->154|971->163|1062->219|1076->225|1134->262|1203->295|1228->311|1265->326|1323->348|1342->358|1382->360|1426->372|1507->417|1537->425
                    LINES: 20->1|24->1|26->4|29->7|29->7|29->7|31->9|31->9|31->9|31->9|31->9|31->9|32->10|32->10|32->10|32->10|34->12|34->12
                    -- GENERATED --
                */
            