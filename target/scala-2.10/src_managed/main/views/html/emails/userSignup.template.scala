
package views.html.emails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object userSignup extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[User,Boolean,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(user: User, ssl: Boolean = true)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services.AppConfiguration


Seq[Any](format.raw/*1.68*/("""

"""),format.raw/*4.1*/("""
<html>
    <body>
        <table style="border: 1px solid black; padding: 10px;">
            <tr>
                <td>
                    <img src=""""),_display_(Seq[Any](/*10.32*/user/*10.36*/.getAvatarUrl())),format.raw/*10.51*/("""" border="0" height="48" width="48">
                </td>
                <td>
                    A new user joined <a href=""""),_display_(Seq[Any](/*13.49*/routes/*13.55*/.Application.index().absoluteURL(ssl))),format.raw/*13.92*/("""" style="text-decoration: none">"""),_display_(Seq[Any](/*13.125*/AppConfiguration/*13.141*/.getDisplayName)),format.raw/*13.156*/("""</a><br/>
                    <a href=""""),_display_(Seq[Any](/*14.31*/routes/*14.37*/.Profile.viewProfileUUID(user.id).absoluteURL(ssl))),format.raw/*14.87*/("""" style="text-decoration: none">
                        <h2 style="margin: 0">"""),_display_(Seq[Any](/*15.48*/user/*15.52*/.fullName)),format.raw/*15.61*/("""</h2>
                    </a>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Login provider: <b>"""),_display_(Seq[Any](/*21.41*/user/*21.45*/.identityId.providerId)),format.raw/*21.67*/("""</b><br/>
                    Login userID: <b>"""),_display_(Seq[Any](/*22.39*/user/*22.43*/.identityId.userId)),format.raw/*22.61*/("""</b>
                </td>
            </tr>
        </table>
        """),_display_(Seq[Any](/*26.10*/footer())),format.raw/*26.18*/("""
    </body>
</html>
"""))}
    }
    
    def render(user:User,ssl:Boolean,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(user,ssl)(request)
    
    def f:((User,Boolean) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (user,ssl) => (request) => apply(user,ssl)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/emails/userSignup.scala.html
                    HASH: 1b87f085c8e7448c1928bc45a2cc4bd06664ea39
                    MATRIX: 619->1|812->67|840->103|1028->255|1041->259|1078->274|1242->402|1257->408|1316->445|1386->478|1412->494|1450->509|1526->549|1541->555|1613->605|1729->685|1742->689|1773->698|1970->859|1983->863|2027->885|2111->933|2124->937|2164->955|2271->1026|2301->1034
                    LINES: 20->1|24->1|26->4|32->10|32->10|32->10|35->13|35->13|35->13|35->13|35->13|35->13|36->14|36->14|36->14|37->15|37->15|37->15|43->21|43->21|43->21|44->22|44->22|44->22|48->26|48->26
                    -- GENERATED --
                */
            