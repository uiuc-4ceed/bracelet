
package views.html.emails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object userActivated extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[User,Boolean,Boolean,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(user: User, active: Boolean, ssl: Boolean = true)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services.AppConfiguration


Seq[Any](format.raw/*1.85*/("""

"""),format.raw/*4.1*/("""
<html>
    <body>
        """),_display_(Seq[Any](/*7.10*/user/*7.14*/.fullName)),format.raw/*7.23*/(""",<br/>
        <br>
        <p>
            Your account on <a href=""""),_display_(Seq[Any](/*10.39*/routes/*10.45*/.Application.index().absoluteURL(ssl))),format.raw/*10.82*/("""" style="text-decoration: none">"""),_display_(Seq[Any](/*10.115*/AppConfiguration/*10.131*/.getDisplayName)),format.raw/*10.146*/("""</a><br/>
            has been
            """),_display_(Seq[Any](/*12.14*/if(active)/*12.24*/ {_display_(Seq[Any](format.raw/*12.26*/("""
                enabled and you can now login.
            """)))}/*14.15*/else/*14.20*/{_display_(Seq[Any](format.raw/*14.21*/("""
                disabled.
            """)))})),format.raw/*16.14*/("""
        </p>
        """),_display_(Seq[Any](/*18.10*/footer())),format.raw/*18.18*/("""
    </body>
</html>
"""))}
    }
    
    def render(user:User,active:Boolean,ssl:Boolean,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(user,active,ssl)(request)
    
    def f:((User,Boolean,Boolean) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (user,active,ssl) => (request) => apply(user,active,ssl)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/emails/userActivated.scala.html
                    HASH: 6ed15ca25fc8e74c5034c7c5ca8409e14851b889
                    MATRIX: 630->1|840->84|868->120|931->148|943->152|973->161|1079->231|1094->237|1153->274|1223->307|1249->323|1287->338|1367->382|1386->392|1426->394|1506->456|1519->461|1558->462|1630->502|1689->525|1719->533
                    LINES: 20->1|24->1|26->4|29->7|29->7|29->7|32->10|32->10|32->10|32->10|32->10|32->10|34->12|34->12|34->12|36->14|36->14|36->14|38->16|40->18|40->18
                    -- GENERATED --
                */
            