
package views.html.emails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object footer extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services.AppConfiguration


Seq[Any](format.raw/*1.37*/("""

"""),format.raw/*4.1*/("""
<hr>
<p>
    <small>
        This message was sent by <a style="text-decoration:none;" href=""""),_display_(Seq[Any](/*8.74*/routes/*8.80*/.Application.index().absoluteURL(controllers.Utils.https(request)))),format.raw/*8.146*/("""" style="text-decoration: none">"""),_display_(Seq[Any](/*8.179*/AppConfiguration/*8.195*/.getDisplayName)),format.raw/*8.210*/("""</a> an
        instance of <a style="text-decoration:none;" href="http://clowder.ncsa.illinois.edu/">Clowder</a> running
        """),_display_(Seq[Any](/*10.10*/sys/*10.13*/.props.getOrElse("build.version", default = "0.0.0"))),format.raw/*10.65*/("""#"""),_display_(Seq[Any](/*10.67*/sys/*10.70*/.props.getOrElse("build.bamboo", default = "development"))),format.raw/*10.127*/("""<br/>
        If you have any questions about this email please contact <a style="text-decoration:none;" href=""""),_display_(Seq[Any](/*11.107*/routes/*11.113*/.Application.email().absoluteURL(controllers.Utils.https(request)))),format.raw/*11.179*/("""">the server admins.</a>
    </small>
</p>
"""))}
    }
    
    def render(request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply()(request)
    
    def f:(() => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = () => (request) => apply()(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/emails/footer.scala.html
                    HASH: 70cac1a6c8bf0fefe8aa9498f0c1f3b2d1068728
                    MATRIX: 602->1|764->36|792->72|922->167|936->173|1024->239|1093->272|1118->288|1155->303|1322->434|1334->437|1408->489|1446->491|1458->494|1538->551|1687->663|1703->669|1792->735
                    LINES: 20->1|24->1|26->4|30->8|30->8|30->8|30->8|30->8|30->8|32->10|32->10|32->10|32->10|32->10|32->10|33->11|33->11|33->11
                    -- GENERATED --
                */
            