
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tagListChar extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Map[Char, Map[String, Long]],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(tagMap: Map[Char, Map[String, Long]])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.76*/("""

"""),_display_(Seq[Any](/*3.2*/main("Tag list")/*3.18*/ {_display_(Seq[Any](format.raw/*3.20*/("""
    <div class="row">
        <div class="col-md-12">
            <h1>Tags</h1>
        </div>
    </div>
    """),_display_(Seq[Any](/*9.6*/if(tagMap.isEmpty)/*9.24*/ {_display_(Seq[Any](format.raw/*9.26*/("""
        <div class="row">
            <div id="tagList" class="col-md-12" style="text-align: justify">
                No tags found.
            </div>
        </div>
    """)))}/*15.7*/else/*15.12*/{_display_(Seq[Any](format.raw/*15.13*/("""
        <div class="row">
            <div id="tagList" class="col-md-12">
            """),_display_(Seq[Any](/*18.14*/for(firstChar <- tagMap.keys.toList.sorted) yield /*18.57*/ {_display_(Seq[Any](format.raw/*18.59*/("""
                <a href="#tag_"""),_display_(Seq[Any](/*19.32*/firstChar)),format.raw/*19.41*/("""">"""),_display_(Seq[Any](/*19.44*/firstChar)),format.raw/*19.53*/("""</a>&nbsp;
            """)))})),format.raw/*20.14*/("""
            </div>
        </div>
        """),_display_(Seq[Any](/*23.10*/for(firstChar <- tagMap.keys.toList.sorted) yield /*23.53*/ {_display_(Seq[Any](format.raw/*23.55*/("""
            <div id="tag_"""),_display_(Seq[Any](/*24.27*/firstChar)),format.raw/*24.36*/("""" class="row" style="height: 60px;"></div>
            """),_display_(Seq[Any](/*25.14*/defining(tagMap(firstChar).toList.sorted)/*25.55*/ { tagList =>_display_(Seq[Any](format.raw/*25.68*/("""
                """),_display_(Seq[Any](/*26.18*/for(idx <- List.range(0, tagList.size)) yield /*26.57*/ {_display_(Seq[Any](format.raw/*26.59*/("""
                    """),_display_(Seq[Any](/*27.22*/defining(tagList(idx))/*27.44*/ { tag =>_display_(Seq[Any](format.raw/*27.53*/("""
                        <div class="row">
                            """),_display_(Seq[Any](/*29.30*/if(idx == 0)/*29.42*/ {_display_(Seq[Any](format.raw/*29.44*/("""
                                <div class="col-md-1" style="height: 1em;">
                                    <span class="tag-first-char">"""),_display_(Seq[Any](/*31.67*/firstChar)),format.raw/*31.76*/("""</span>
                                </div>
                            """)))}/*33.31*/else/*33.36*/{_display_(Seq[Any](format.raw/*33.37*/("""
                                <div class="col-md-1" style="text-align: justify">&nbsp</div>
                            """)))})),format.raw/*35.30*/("""
                            <div class="col-md-11">
                                <a href=""""),_display_(Seq[Any](/*37.43*/routes/*37.49*/.Tags.search(tag._1))),format.raw/*37.69*/("""">"""),_display_(Seq[Any](/*37.72*/tag/*37.75*/._1)),format.raw/*37.78*/("""</a> ("""),_display_(Seq[Any](/*37.85*/tag/*37.88*/._2)),format.raw/*37.91*/(""")
                            </div>
                        </div>
                    """)))})),format.raw/*40.22*/("""
                """)))})),format.raw/*41.18*/("""
            """)))})),format.raw/*42.14*/("""
        """)))})),format.raw/*43.10*/("""
        <div id="tag_end" class="row" style="height: 60px;"></div>
    """)))})),format.raw/*45.6*/("""
""")))})),format.raw/*46.2*/("""
"""))}
    }
    
    def render(tagMap:Map[Char, Map[String, Long]],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(tagMap)(user)
    
    def f:((Map[Char, Map[String, Long]]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (tagMap) => (user) => apply(tagMap)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:23 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/tagListChar.scala.html
                    HASH: 48bca7349ed48112760720ea7f65c41cd138e102
                    MATRIX: 635->1|803->75|840->78|864->94|903->96|1049->208|1075->226|1114->228|1306->403|1319->408|1358->409|1483->498|1542->541|1582->543|1650->575|1681->584|1720->587|1751->596|1807->620|1887->664|1946->707|1986->709|2049->736|2080->745|2172->801|2222->842|2273->855|2327->873|2382->912|2422->914|2480->936|2511->958|2558->967|2666->1039|2687->1051|2727->1053|2906->1196|2937->1205|3032->1282|3045->1287|3084->1288|3240->1412|3371->1507|3386->1513|3428->1533|3467->1536|3479->1539|3504->1542|3547->1549|3559->1552|3584->1555|3705->1644|3755->1662|3801->1676|3843->1686|3947->1759|3980->1761
                    LINES: 20->1|23->1|25->3|25->3|25->3|31->9|31->9|31->9|37->15|37->15|37->15|40->18|40->18|40->18|41->19|41->19|41->19|41->19|42->20|45->23|45->23|45->23|46->24|46->24|47->25|47->25|47->25|48->26|48->26|48->26|49->27|49->27|49->27|51->29|51->29|51->29|53->31|53->31|55->33|55->33|55->33|57->35|59->37|59->37|59->37|59->37|59->37|59->37|59->37|59->37|59->37|62->40|63->41|64->42|65->43|67->45|68->46
                    -- GENERATED --
                */
            