
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object fileusermetadata extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[models.File,scala.collection.mutable.Map[String, Any],Boolean,Option[UUID],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(file: models.File, userMetadata: scala.collection.mutable.Map[String,Any], rdfExported: Boolean, curationId: Option[UUID])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import com.mongodb.casbah.Imports.DBObject

import collection.JavaConverters._

import api.Permission

def /*9.3*/printLevelUserMetadata/*9.25*/(metadata: scala.collection.mutable.Map[String,Any]):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*9.81*/("""
		<ul class="usr_md_">
		"""),_display_(Seq[Any](/*11.4*/for((key,value) <- metadata) yield /*11.32*/ {_display_(Seq[Any](format.raw/*11.34*/("""
			"""),_display_(Seq[Any](/*12.5*/if(value.isInstanceOf[com.mongodb.BasicDBList])/*12.52*/{_display_(Seq[Any](format.raw/*12.53*/("""
				"""),_display_(Seq[Any](/*13.6*/for(listValue <- value.asInstanceOf[com.mongodb.BasicDBList]) yield /*13.67*/ {_display_(Seq[Any](format.raw/*13.69*/("""
						<li class="usr_md_"><b class="usr_md_">"""),_display_(Seq[Any](/*14.47*/(key))),format.raw/*14.52*/(""":</b>
						"""),_display_(Seq[Any](/*15.8*/if(listValue.isInstanceOf[String])/*15.42*/ {_display_(Seq[Any](format.raw/*15.44*/("""			
							<span class="usr_md_">"""),_display_(Seq[Any](/*16.31*/listValue)),format.raw/*16.40*/("""</span><button class="usr_md_ btn btn-link btn-sm" type="button">Modify</button>
                            <button class="usr_md_ btn btn-link btn-sm" >Delete</button>
						""")))}/*18.9*/else/*18.14*/{_display_(Seq[Any](format.raw/*18.15*/("""
							<button class="usr_md_ btn btn-link btn-sm" type="button">Add property</button>
                            <button class="usr_md_ btn btn-link btn-sm" type="button">Delete</button>"""),_display_(Seq[Any](/*20.103*/printLevelUserMetadata(listValue.asInstanceOf[DBObject].toMap.asScala.asInstanceOf[scala.collection.mutable.Map[String, Any]]))),format.raw/*20.229*/("""
						""")))})),format.raw/*21.8*/("""</li>
				""")))})),format.raw/*22.6*/("""
			""")))}/*23.5*/else/*23.9*/{_display_(Seq[Any](format.raw/*23.10*/("""
				<li class="usr_md_"><b class="usr_md_">"""),_display_(Seq[Any](/*24.45*/(key))),format.raw/*24.50*/(""":</b>
				"""),_display_(Seq[Any](/*25.6*/if(value.isInstanceOf[String])/*25.36*/ {_display_(Seq[Any](format.raw/*25.38*/("""			
					<span class="usr_md_">"""),_display_(Seq[Any](/*26.29*/value)),format.raw/*26.34*/("""</span><button class="usr_md_ btn btn-link btn-sm" type="button">Modify</button>
                    <button class="usr_md_ btn btn-link btn-sm" >Delete</button>
				""")))}/*28.7*/else/*28.12*/{_display_(Seq[Any](format.raw/*28.13*/("""
					<button class="usr_md_ btn btn-link btn-sm" type="button">Add property</button>
                    <button class="usr_md_ btn btn-link btn-sm" type="button">Delete</button>"""),_display_(Seq[Any](/*30.95*/printLevelUserMetadata(value.asInstanceOf[DBObject].toMap.asScala.asInstanceOf[scala.collection.mutable.Map[String, Any]]))),format.raw/*30.217*/("""
				""")))})),format.raw/*31.6*/("""</li>
			""")))})),format.raw/*32.5*/("""	
		""")))})),format.raw/*33.4*/("""
		</ul>
	""")))};
Seq[Any](format.raw/*1.161*/("""

"""),format.raw/*6.1*/("""
<br/>

	"""),format.raw/*35.3*/("""
	
	<div id='generalUserMetadata_"""),_display_(Seq[Any](/*37.32*/(file.id.stringify))),format.raw/*37.51*/("""' class='usr_md_'>
		"""),_display_(Seq[Any](/*38.4*/if(user.isDefined && Permission.checkPermission(Permission.EditMetadata, ResourceRef(ResourceRef.file, file.id)))/*38.117*/ {_display_(Seq[Any](format.raw/*38.119*/("""
			<button class="usr_md_ btn btn-link btn-sm" type="button">Add property</button>
		""")))})),format.raw/*40.4*/("""

		"""),_display_(Seq[Any](/*42.4*/printLevelUserMetadata(userMetadata))),format.raw/*42.40*/("""
		<button class="usr_md_submit btn btn-primary btn-xs"><span class="glyphicon glyphicon-saved"></span> Submit</button>
		"""),_display_(Seq[Any](/*44.4*/if(rdfExported)/*44.19*/{_display_(Seq[Any](format.raw/*44.20*/("""
			<button class="usr_md_submit btn btn-default btn-sm rdfbtn"><span class="glyphicon glyphicon-pencil"></span> Get as RDF</button>
		""")))})),format.raw/*46.4*/("""
	</div>




<script language="javascript">
	var topId = "generalUserMetadata_"""),_display_(Seq[Any](/*53.36*/(file.id.stringify))),format.raw/*53.55*/("""";
	window["uploadIp"+topId] = """"),_display_(Seq[Any](/*54.31*/api/*54.34*/.routes.Files.addUserMetadata(file.id))),format.raw/*54.72*/("""";
    window["rdfIp"+topId] = """"),_display_(Seq[Any](/*55.31*/api/*55.34*/.routes.Files.getRDFUserMetadata(file.id))),format.raw/*55.75*/("""";
	window["modelIp"+topId] = """"),_display_(Seq[Any](/*56.30*/(routes.Assets.at("filesUserMetadataModel")))),format.raw/*56.74*/("""";
</script>
<script src=""""),_display_(Seq[Any](/*58.15*/routes/*58.21*/.Assets.at("javascripts/userMetadata.js"))),format.raw/*58.62*/("""" type="text/javascript"></script>


"""))}
    }
    
    def render(file:models.File,userMetadata:scala.collection.mutable.Map[String, Any],rdfExported:Boolean,curationId:Option[UUID],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(file,userMetadata,rdfExported,curationId)(user)
    
    def f:((models.File,scala.collection.mutable.Map[String, Any],Boolean,Option[UUID]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (file,userMetadata,rdfExported,curationId) => (user) => apply(file,userMetadata,rdfExported,curationId)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:30 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/fileusermetadata.scala.html
                    HASH: 8ff8a2d5b7b16deb896e63dfbdaca0c23da837f0
                    MATRIX: 686->1|1024->275|1054->297|1190->353|1252->380|1296->408|1336->410|1376->415|1432->462|1471->463|1512->469|1589->530|1629->532|1712->579|1739->584|1787->597|1830->631|1870->633|1940->667|1971->676|2166->854|2179->859|2218->860|2445->1050|2594->1176|2633->1184|2675->1195|2698->1200|2710->1204|2749->1205|2830->1250|2857->1255|2903->1266|2942->1296|2982->1298|3050->1330|3077->1335|3262->1503|3275->1508|3314->1509|3530->1689|3675->1811|3712->1817|3753->1827|3789->1832|3840->160|3868->265|3904->1843|3974->1877|4015->1896|4072->1918|4195->2031|4236->2033|4354->2120|4394->2125|4452->2161|4610->2284|4634->2299|4673->2300|4840->2436|4955->2515|4996->2534|5065->2567|5077->2570|5137->2608|5206->2641|5218->2644|5281->2685|5349->2717|5415->2761|5478->2788|5493->2794|5556->2835
                    LINES: 20->1|27->9|27->9|29->9|31->11|31->11|31->11|32->12|32->12|32->12|33->13|33->13|33->13|34->14|34->14|35->15|35->15|35->15|36->16|36->16|38->18|38->18|38->18|40->20|40->20|41->21|42->22|43->23|43->23|43->23|44->24|44->24|45->25|45->25|45->25|46->26|46->26|48->28|48->28|48->28|50->30|50->30|51->31|52->32|53->33|56->1|58->6|61->35|63->37|63->37|64->38|64->38|64->38|66->40|68->42|68->42|70->44|70->44|70->44|72->46|79->53|79->53|80->54|80->54|80->54|81->55|81->55|81->55|82->56|82->56|84->58|84->58|84->58
                    -- GENERATED --
                */
            