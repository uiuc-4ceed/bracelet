
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object newVocabulary extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[String,List[models.ProjectSpace],Boolean,Boolean,Option[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(errorString: String, spaces: List[models.ProjectSpace], isNameRequired: Boolean, isDescriptionRequired: Boolean, spaceId: Option[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.203*/("""

"""),format.raw/*4.75*/("""

"""),_display_(Seq[Any](/*6.2*/main("Create Vocabulary")/*6.27*/ {_display_(Seq[Any](format.raw/*6.29*/("""
  <!-- Custom items for the create collection workflow -->
  <script src=""""),_display_(Seq[Any](/*8.17*/routes/*8.23*/.Assets.at("javascripts/vocabulary-create.js"))),format.raw/*8.69*/("""" type="text/javascript"></script>
  <script src=""""),_display_(Seq[Any](/*9.17*/routes/*9.23*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*9.68*/("""" language="javascript"></script>
  <script type="text/javascript" language="javascript">
      //Global so that the javascript for the collection creation can reference this.
      var isNameRequired = """),_display_(Seq[Any](/*12.29*/isNameRequired)),format.raw/*12.43*/(""";
      var isDescRequired = """),_display_(Seq[Any](/*13.29*/isDescriptionRequired)),format.raw/*13.50*/(""";
      var isKeysRequired = true;
  </script>
    <script src=""""),_display_(Seq[Any](/*16.19*/routes/*16.25*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*16.67*/("""" type="text/javascript"></script>
    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*17.35*/routes/*17.41*/.Assets.at("stylesheets/chosen.css"))),format.raw/*17.77*/("""">
  <div class="page-header">
    <h1>Create New Vocabulary</h1>
  </div>
  <div class="row">
	<div class="col-md-12">
	
	<div>
        <span id="status" class="success hiddencomplete alert alert-success" role="alert">A Status Message</span>     
        """),_display_(Seq[Any](/*26.10*/if(errorString != null)/*26.33*/{_display_(Seq[Any](format.raw/*26.34*/("""
            <span class="error alert alert-danger" id="messageerror">"""),_display_(Seq[Any](/*27.71*/errorString)),format.raw/*27.82*/("""</span>
        """)))}/*28.10*/else/*28.14*/{_display_(Seq[Any](format.raw/*28.15*/("""           
            <span class="error hiddencomplete alert alert-danger" id="messageerror">An Error Message</span>
        """)))})),format.raw/*30.10*/("""
    </div>
    <!-- Basic required elements for creating a new collection. -->
    <table style="width: 100%; margin-bottom: 40px; margin-top: 40px;">
        <tr>
            <td style="width: 20%; vertical-align:top;" class="input-table-cell">Name:</td>
            <td style="width: 80%; vertical-align:top;" class="input-table-cell">
                <textarea class="form-control" cols=40 rows=4 type="text" id="name"></textarea>
                <span class="error hiddencomplete" id="nameerror"> The name is a required field</span><br>
            </td>
            <td style="vertical-align:top;" class="input-table-cell">

            </td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align:top;" class="input-table-cell">Keys:</td>
            <td style="width: 80%; vertical-align:top;" class="input-table-cell" id="desccell">
                <textarea class="form-control" cols=40 rows=4 type="text" id="keys"></textarea>
                <span class="error hiddencomplete" id="keyerror"> This keys is a required field</span><br>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align:top;" class="input-table-cell">Description:</td>
            <td style="width: 80%; vertical-align:top;" class="input-table-cell" id="desccell">
                <textarea class="form-control" cols=40 rows=4 type="text" id="description"></textarea>
                <span class="error hiddencomplete" id="descerror"> This description is a required field</span><br>
            </td>
        </tr>        
        <tr>
            <td style="width: 20%; vertical-align:top;" class="input-table-cell">Share with spaces (optional):</td>
            <td style="width: 35%; vertical-align:top;" class="input-table-cell" id="desccell">
                <select name="space" id="spaceid" class = "form-control chosen-select" multiple style="width: 342px;">
                    """),_display_(Seq[Any](/*62.22*/spaces/*62.28*/.map/*62.32*/ { space =>_display_(Seq[Any](format.raw/*62.43*/("""
                        <option id = """"),_display_(Seq[Any](/*63.40*/(space.id))),format.raw/*63.50*/("""" value=""""),_display_(Seq[Any](/*63.60*/(space.id))),format.raw/*63.70*/("""">"""),_display_(Seq[Any](/*63.73*/(space.name))),format.raw/*63.85*/("""</option>
                    """)))})),format.raw/*64.22*/("""
                </select>
            </td>
        </tr>
        <tr>
            <td>	                            
            </td>
            <td>
                <button style="margin-top: 10px; margin-right: 10px;" class="btn btn-primary" title="Create the collection" onclick="return createVocabulary();">
                  <span class="glyphicon glyphicon-ok"></span> Create
                </button>
                <button style="margin-top: 10px;" class="btn btn-default" title="Clear the input fields" onclick="return clearFields();">
                  <span class="glyphicon glyphicon-unchecked"></span> Reset
                </button>
            </td>
        </tr>
    </table>	
    <form id="collectioncreate" action='"""),_display_(Seq[Any](/*81.42*/routes/*81.48*/.Vocabularies.submit)),format.raw/*81.68*/("""' method="POST" enctype="multipart/form-data">
                    <!-- Redirect browsers with JavaScript disabled to the origin page -->
                    <noscript>Javascript is required in order to use the uploader to create a new collection.</noscript>
                    
                    <input type="hidden" name="name" id="hiddenname" value="not set">
                    <input type="hidden" name="keys" id="hiddenkeys" value="not set">
                    <input type="hidden" name="description" id="hiddendescription" value="not set">
                    <input type="hidden" name="space" id="hiddenspace" value="not set">
    </form>
    </div>
  </div>
<script language="javascript">
    $(".chosen-select").chosen("""),format.raw/*93.32*/("""{"""),format.raw/*93.33*/("""
        width: "100%",
        placeholder_text_multiple: "Select spaces for this collection."
    """),format.raw/*96.5*/("""}"""),format.raw/*96.6*/(""");
</script>
<script language = "javascript">
    var idNameObj = """),format.raw/*99.21*/("""{"""),format.raw/*99.22*/("""}"""),format.raw/*99.23*/(""";

    function isUndefined(value) """),format.raw/*101.33*/("""{"""),format.raw/*101.34*/("""
        return typeof value === 'undefined';
    """),format.raw/*103.5*/("""}"""),format.raw/*103.6*/("""

    function getName(id) """),format.raw/*105.26*/("""{"""),format.raw/*105.27*/("""
        return idNameObj[id];
    """),format.raw/*107.5*/("""}"""),format.raw/*107.6*/("""

    $(document).ready(function() """),format.raw/*109.34*/("""{"""),format.raw/*109.35*/("""
        """),_display_(Seq[Any](/*110.10*/for(space <- spaces) yield /*110.30*/ {_display_(Seq[Any](format.raw/*110.32*/("""
            idNameObj[""""),_display_(Seq[Any](/*111.25*/space/*111.30*/.name)),format.raw/*111.35*/(""""] = """"),_display_(Seq[Any](/*111.42*/space/*111.47*/.id)),format.raw/*111.50*/("""";

        """)))})),format.raw/*113.10*/("""
        $(".chosen-choices").addClass("form-control");
    """),format.raw/*115.5*/("""}"""),format.raw/*115.6*/(""");

    $(".chosen-select").chosen().change(function(event, params) """),format.raw/*117.65*/("""{"""),format.raw/*117.66*/("""
    var targetId = this.getAttribute("id");
    if(!isUndefined(params.selected)) """),format.raw/*119.39*/("""{"""),format.raw/*119.40*/("""
        var addedId = params.selected;
        """),_display_(Seq[Any](/*121.10*/for(space <- spaces) yield /*121.30*/ {_display_(Seq[Any](format.raw/*121.32*/("""
            var currentId = """"),_display_(Seq[Any](/*122.31*/space)),format.raw/*122.36*/("""";
            if(currentId != targetId) """),format.raw/*123.39*/("""{"""),format.raw/*123.40*/("""
                $("#"""),_display_(Seq[Any](/*124.22*/space/*124.27*/.id)),format.raw/*124.30*/(""" option [value="+addedId +']').remove();
                $('#"""),_display_(Seq[Any](/*125.22*/space/*125.27*/.id)),format.raw/*125.30*/("""').trigger("chosen:updated");
            """),format.raw/*126.13*/("""}"""),format.raw/*126.14*/("""
        """)))})),format.raw/*127.10*/("""
        $('ul.chosen-choices li.search-field').css("width", "0");

    """),format.raw/*130.5*/("""}"""),format.raw/*130.6*/("""
    else if (!isUndefined(params.deselected)) """),format.raw/*131.47*/("""{"""),format.raw/*131.48*/("""
        var removedId = params.deselected;
        """),_display_(Seq[Any](/*133.10*/for(space <- spaces) yield /*133.30*/ {_display_(Seq[Any](format.raw/*133.32*/("""
            var currentId = """"),_display_(Seq[Any](/*134.31*/space)),format.raw/*134.36*/("""";
            if(currentId != targetId) """),format.raw/*135.39*/("""{"""),format.raw/*135.40*/("""
                $('#"""),_display_(Seq[Any](/*136.22*/space/*136.27*/.id)),format.raw/*136.30*/("""').prepend($("<option></option>").attr("value", removedId).text(getName(removedId)));
                $('#"""),_display_(Seq[Any](/*137.22*/space/*137.27*/.id)),format.raw/*137.30*/("""').trigger("chosen:updated");
            """),format.raw/*138.13*/("""}"""),format.raw/*138.14*/("""
        """)))})),format.raw/*139.10*/("""
    """),format.raw/*140.5*/("""}"""),format.raw/*140.6*/("""
    """),format.raw/*141.5*/("""}"""),format.raw/*141.6*/(""")

    if("""),_display_(Seq[Any](/*143.9*/spaceId/*143.16*/.isDefined)),format.raw/*143.26*/(""") """),format.raw/*143.28*/("""{"""),format.raw/*143.29*/("""
        $('#"""),_display_(Seq[Any](/*144.14*/spaceId)),format.raw/*144.21*/("""').prop('selected','selected');
        $('#spaceid').trigger("chosen:updated");
    """),format.raw/*146.5*/("""}"""),format.raw/*146.6*/("""
  </script>
""")))})),format.raw/*148.2*/("""
"""))}
    }
    
    def render(errorString:String,spaces:List[models.ProjectSpace],isNameRequired:Boolean,isDescriptionRequired:Boolean,spaceId:Option[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(errorString,spaces,isNameRequired,isDescriptionRequired,spaceId)(flash,user)
    
    def f:((String,List[models.ProjectSpace],Boolean,Boolean,Option[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (errorString,spaces,isNameRequired,isDescriptionRequired,spaceId) => (flash,user) => apply(errorString,spaces,isNameRequired,isDescriptionRequired,spaceId)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/newVocabulary.scala.html
                    HASH: 8dc2f6b02c14d3998889c2f64f232081aa178d75
                    MATRIX: 691->1|994->222|1026->246|1106->202|1135->295|1172->298|1205->323|1244->325|1355->401|1369->407|1436->453|1522->504|1536->510|1602->555|1842->759|1878->773|1944->803|1987->824|2088->889|2103->895|2167->937|2272->1006|2287->1012|2345->1048|2638->1305|2670->1328|2709->1329|2816->1400|2849->1411|2885->1428|2898->1432|2937->1433|3098->1562|5064->3492|5079->3498|5092->3502|5141->3513|5217->3553|5249->3563|5295->3573|5327->3583|5366->3586|5400->3598|5463->3629|6237->4367|6252->4373|6294->4393|7056->5127|7085->5128|7212->5228|7240->5229|7334->5295|7363->5296|7392->5297|7456->5332|7486->5333|7564->5383|7593->5384|7649->5411|7679->5412|7742->5447|7771->5448|7835->5483|7865->5484|7912->5494|7949->5514|7990->5516|8052->5541|8067->5546|8095->5551|8139->5558|8154->5563|8180->5566|8226->5579|8314->5639|8343->5640|8440->5708|8470->5709|8582->5792|8612->5793|8698->5842|8735->5862|8776->5864|8844->5895|8872->5900|8942->5941|8972->5942|9031->5964|9046->5969|9072->5972|9171->6034|9186->6039|9212->6042|9283->6084|9313->6085|9356->6095|9456->6167|9485->6168|9561->6215|9591->6216|9681->6269|9718->6289|9759->6291|9827->6322|9855->6327|9925->6368|9955->6369|10014->6391|10029->6396|10055->6399|10199->6506|10214->6511|10240->6514|10311->6556|10341->6557|10384->6567|10417->6572|10446->6573|10479->6578|10508->6579|10555->6590|10572->6597|10605->6607|10636->6609|10666->6610|10717->6624|10747->6631|10860->6716|10889->6717|10935->6731
                    LINES: 20->1|23->4|23->4|24->1|26->4|28->6|28->6|28->6|30->8|30->8|30->8|31->9|31->9|31->9|34->12|34->12|35->13|35->13|38->16|38->16|38->16|39->17|39->17|39->17|48->26|48->26|48->26|49->27|49->27|50->28|50->28|50->28|52->30|84->62|84->62|84->62|84->62|85->63|85->63|85->63|85->63|85->63|85->63|86->64|103->81|103->81|103->81|115->93|115->93|118->96|118->96|121->99|121->99|121->99|123->101|123->101|125->103|125->103|127->105|127->105|129->107|129->107|131->109|131->109|132->110|132->110|132->110|133->111|133->111|133->111|133->111|133->111|133->111|135->113|137->115|137->115|139->117|139->117|141->119|141->119|143->121|143->121|143->121|144->122|144->122|145->123|145->123|146->124|146->124|146->124|147->125|147->125|147->125|148->126|148->126|149->127|152->130|152->130|153->131|153->131|155->133|155->133|155->133|156->134|156->134|157->135|157->135|158->136|158->136|158->136|159->137|159->137|159->137|160->138|160->138|161->139|162->140|162->140|163->141|163->141|165->143|165->143|165->143|165->143|165->143|166->144|166->144|168->146|168->146|170->148
                    -- GENERATED --
                */
            