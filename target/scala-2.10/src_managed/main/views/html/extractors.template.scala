
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object extractors extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[String],Integer,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dtsextractors:List[String], size:Integer):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import collection.JavaConverters._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.44*/("""
"""),format.raw/*4.75*/("""

"""),_display_(Seq[Any](/*6.2*/main("Extractors")/*6.20*/{_display_(Seq[Any](format.raw/*6.21*/("""
	<div class="page-header">
		<h1>Extractors</h1>
	</div>
	"""),_display_(Seq[Any](/*10.3*/if(size == 0)/*10.16*/ {_display_(Seq[Any](format.raw/*10.18*/("""
		<div class="row">
			<div class="col-md-12">
				No Extractors found. Sorry!
			</div>
		</div>
	""")))}/*16.3*/else/*16.7*/{_display_(Seq[Any](format.raw/*16.8*/("""
		<div class="container" > 
			<div class="row-fluid" >
				<div>
					<table id='tablec'  style="table-layout: fixed;width: 30%" cellpadding="10"  cellspacing="0" border="1">
					<thead>
						<tr>
						<th>Available Extractors</th>
						</tr>
					</thead>
					<tbody>
					"""),_display_(Seq[Any](/*27.7*/for(req <- dtsextractors) yield /*27.32*/{_display_(Seq[Any](format.raw/*27.33*/("""
						<tr>
						<td>"""),_display_(Seq[Any](/*29.12*/req)),format.raw/*29.15*/("""</td>
		 				</tr>	
				    """)))})),format.raw/*31.10*/("""
					</tbody>
					</table>
			  </div>
		   </div>
		</div>
	   """)))})),format.raw/*37.6*/("""
""")))})),format.raw/*38.2*/("""
"""))}
    }
    
    def render(dtsextractors:List[String],size:Integer): play.api.templates.HtmlFormat.Appendable = apply(dtsextractors,size)
    
    def f:((List[String],Integer) => play.api.templates.HtmlFormat.Appendable) = (dtsextractors,size) => apply(dtsextractors,size)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/extractors.scala.html
                    HASH: d02b169c3fa6a02525e77fd09f4a98e4f6f59677
                    MATRIX: 606->1|786->98|818->122|897->43|925->171|962->174|988->192|1026->193|1121->253|1143->266|1183->268|1302->369|1314->373|1352->374|1669->656|1710->681|1749->682|1808->705|1833->708|1894->737|1992->804|2025->806
                    LINES: 20->1|25->4|25->4|26->1|27->4|29->6|29->6|29->6|33->10|33->10|33->10|39->16|39->16|39->16|50->27|50->27|50->27|52->29|52->29|54->31|60->37|61->38
                    -- GENERATED --
                */
            