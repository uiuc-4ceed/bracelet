
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object datasetsContainingFile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[models.File,List[models.Dataset],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(file: models.File, datasets: List[models.Dataset])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.89*/("""

"""),format.raw/*4.1*/("""
<script src=""""),_display_(Seq[Any](/*5.15*/routes/*5.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*5.63*/("""" type="text/javascript"></script>

<div id="datasetsList" class="col-md-12">
"""),_display_(Seq[Any](/*8.2*/datasets/*8.10*/.map/*8.14*/ { dataset =>_display_(Seq[Any](format.raw/*8.27*/("""
    <div id="ds_"""),_display_(Seq[Any](/*9.18*/dataset/*9.25*/.id)),format.raw/*9.28*/("""" class="row bottom-padding">
        <div class="col-md-2">
        """),_display_(Seq[Any](/*11.10*/if(!dataset.thumbnail_id.isEmpty)/*11.43*/{_display_(Seq[Any](format.raw/*11.44*/("""
            <a href=""""),_display_(Seq[Any](/*12.23*/(routes.Datasets.dataset(dataset.id)))),format.raw/*12.60*/("""">
                <img class="img-responsive" src=""""),_display_(Seq[Any](/*13.51*/(routes.Files.thumbnail(UUID(dataset.thumbnail_id.get))))),format.raw/*13.107*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*13.128*/Html(dataset.name))),format.raw/*13.146*/("""">
            </a>
        """)))})),format.raw/*15.10*/("""
        </div>
        <div class="col-md-10">
            <div class="caption break-word">
                <a href=""""),_display_(Seq[Any](/*19.27*/(routes.Datasets.dataset(dataset.id)))),format.raw/*19.64*/("""" id='"""),_display_(Seq[Any](/*19.71*/dataset/*19.78*/.id)),format.raw/*19.81*/("""'>"""),_display_(Seq[Any](/*19.84*/Html(dataset.name))),format.raw/*19.102*/("""</a>
            </div>
            <div>
            <!-- If the user can edit the dataset, the elements are enabled, otherwise they are present but disabled to provide consistent UE. -->
                """),_display_(Seq[Any](/*23.18*/if(Permission.checkPermission(Permission.EditDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*23.118*/ {_display_(Seq[Any](format.raw/*23.120*/("""
                    <a href="#" class="btn btn-link btn-xs" onclick="removeFromDataset('"""),_display_(Seq[Any](/*24.90*/(dataset.id))),format.raw/*24.102*/("""','"""),_display_(Seq[Any](/*24.106*/(dataset.name))),format.raw/*24.120*/("""',event)" title="Remove from dataset"><span class="glyphicon glyphicon-remove"></span> Remove</a>
                """)))}/*25.19*/else/*25.24*/{_display_(Seq[Any](format.raw/*25.25*/("""
                    <a href="#" class="btn btn-link btn-xs disabled" onclick="return false;" title="Remove from dataset"><span class="glyphicon glyphicon-remove"></span> Remove</a>
                """)))})),format.raw/*27.18*/("""
            </div>
        </div>
    </div>
    """)))})),format.raw/*31.6*/("""
</div>

<script>
        function removeFromDataset(datasetId, datasetName,event)"""),format.raw/*35.65*/("""{"""),format.raw/*35.66*/("""
            var request = jsRoutes.api.Datasets.detachFile(datasetId, """"),_display_(Seq[Any](/*36.73*/file/*36.77*/.id)),format.raw/*36.80*/("""").ajax("""),format.raw/*36.88*/("""{"""),format.raw/*36.89*/("""
                   type: 'DELETE'
                 """),format.raw/*38.18*/("""}"""),format.raw/*38.19*/(""");
            request.done(function (response, textStatus, jqXHR)"""),format.raw/*39.64*/("""{"""),format.raw/*39.65*/("""
                console.log("Response " + response);
                $('#ds_'+datasetId).remove();
                $(event.target.parentNode.parentNode).remove();
            """),format.raw/*43.13*/("""}"""),format.raw/*43.14*/(""");
            request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*44.67*/("""{"""),format.raw/*44.68*/("""
            	console.error("The following error occured: "+textStatus, errorThrown);
                var errMsg = "You must be logged in to remove a file from a dataset.";                    
                if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*47.60*/("""{"""),format.raw/*47.61*/("""
                    notify("The file was not removed from the dataset due to : " + errorThrown, "error");
                """),format.raw/*49.17*/("""}"""),format.raw/*49.18*/("""
            """),format.raw/*50.13*/("""}"""),format.raw/*50.14*/(""");
        """),format.raw/*51.9*/("""}"""),format.raw/*51.10*/("""

</script>
"""))}
    }
    
    def render(file:models.File,datasets:List[models.Dataset],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(file,datasets)(user)
    
    def f:((models.File,List[models.Dataset]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (file,datasets) => (user) => apply(file,datasets)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasetsContainingFile.scala.html
                    HASH: dc8e497a286e8ed93928e395bbd69ec9956d3d94
                    MATRIX: 650->1|853->88|881->113|931->128|945->134|1008->176|1121->255|1137->263|1149->267|1199->280|1252->298|1267->305|1291->308|1397->378|1439->411|1478->412|1537->435|1596->472|1685->525|1764->581|1822->602|1863->620|1924->649|2079->768|2138->805|2181->812|2197->819|2222->822|2261->825|2302->843|2544->1049|2654->1149|2695->1151|2821->1241|2856->1253|2897->1257|2934->1271|3068->1387|3081->1392|3120->1393|3351->1592|3433->1643|3543->1725|3572->1726|3681->1799|3694->1803|3719->1806|3755->1814|3784->1815|3864->1867|3893->1868|3987->1934|4016->1935|4220->2111|4249->2112|4346->2181|4375->2182|4655->2434|4684->2435|4835->2558|4864->2559|4905->2572|4934->2573|4972->2584|5001->2585
                    LINES: 20->1|24->1|26->4|27->5|27->5|27->5|30->8|30->8|30->8|30->8|31->9|31->9|31->9|33->11|33->11|33->11|34->12|34->12|35->13|35->13|35->13|35->13|37->15|41->19|41->19|41->19|41->19|41->19|41->19|41->19|45->23|45->23|45->23|46->24|46->24|46->24|46->24|47->25|47->25|47->25|49->27|53->31|57->35|57->35|58->36|58->36|58->36|58->36|58->36|60->38|60->38|61->39|61->39|65->43|65->43|66->44|66->44|69->47|69->47|71->49|71->49|72->50|72->50|73->51|73->51
                    -- GENERATED --
                */
            