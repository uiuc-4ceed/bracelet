
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object file extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template19[models.File,String,List[Comment],Map[models.File, Array[scala.Tuple8[java.lang.String, String, String, String, java.lang.String, String, Long, String]]],List[models.Section],Boolean,List[models.Dataset],List[Folder],List[models.Metadata],Boolean,List[Extraction],Option[List[String]],Option[String],String,List[Folder],List[ProjectSpace],List[Dataset],Option[models.User],RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*2.2*/(file: models.File, id: String, comments: List[Comment],
		previews : Map[models.File, Array[(java.lang.String, String, String, String, java.lang.String, String, Long, String)]],
		sections: List[models.Section], beingProcessed: Boolean, datasets: List[models.Dataset], folders: List[Folder],
		mds: List[models.Metadata],
		rdfExported: Boolean,
		extractionsStatus: List[Extraction], outputFormats: Option[List[String]], spaceId: Option[String], access:String,
		folderHierarchy: List[Folder], spaces:List[ProjectSpace], allDatasets: List[Dataset])(implicit user: Option[models.User], request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import play.api.Play.current

import api.Permission

import _root_.util.FileUtils

import _root_.util.Formatters._

def /*20.2*/showPreview/*20.13*/(id: String, p: (java.lang.String, String, String, String, java.lang.String, String, Long, String), filename: String, contentType: String):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*20.155*/("""
	<script>
		Configuration.tab = "#previewer_"""),_display_(Seq[Any](/*22.36*/id)),format.raw/*22.38*/("""";
		Configuration.url = """"),_display_(Seq[Any](/*23.25*/p/*23.26*/._5)),format.raw/*23.29*/("""";
		Configuration.fileid = """"),_display_(Seq[Any](/*24.28*/p/*24.29*/._1)),format.raw/*24.32*/("""";
		Configuration.previewer = """"),_display_(Seq[Any](/*25.31*/routes/*25.37*/.Assets.at(p._3))),format.raw/*25.53*/("""";
		if(""""),_display_(Seq[Any](/*26.8*/p/*26.9*/._2)),format.raw/*26.12*/("""" === "Thumbnail")"""),format.raw/*26.30*/("""{"""),format.raw/*26.31*/("""
			Configuration.fileType = """"),_display_(Seq[Any](/*27.31*/p/*27.32*/._6)),format.raw/*27.35*/("""";
			Configuration.fileSize = """),_display_(Seq[Any](/*28.30*/p/*28.31*/._7)),format.raw/*28.34*/(""";
		"""),format.raw/*29.3*/("""}"""),format.raw/*29.4*/("""
		else if(""""),_display_(Seq[Any](/*30.13*/p/*30.14*/._2)),format.raw/*30.17*/("""" === "X3d")"""),format.raw/*30.29*/("""{"""),format.raw/*30.30*/("""
			Configuration.annotationsEditPath = """"),_display_(Seq[Any](/*31.42*/api/*31.45*/.routes.Previews.editAnnotation(UUID(p._1)))),format.raw/*31.88*/("""";
			Configuration.annotationsListPath = """"),_display_(Seq[Any](/*32.42*/api/*32.45*/.routes.Previews.listAnnotations(UUID(p._1)))),format.raw/*32.89*/("""";
			Configuration.annotationsAttachPath = """"),_display_(Seq[Any](/*33.44*/api/*33.47*/.routes.Previews.attachAnnotation(UUID(p._1)))),format.raw/*33.92*/("""";
			Configuration.wasPTM = """"),_display_(Seq[Any](/*34.29*/(filename.endsWith(".ptm") || contentType.contains("ptmimages")))),format.raw/*34.93*/("""";
			Configuration.calledFrom = "file";
		"""),format.raw/*36.3*/("""}"""),format.raw/*36.4*/("""
		else if(""""),_display_(Seq[Any](/*37.13*/p/*37.14*/._2)),format.raw/*37.17*/("""" === "Oni")"""),format.raw/*37.29*/("""{"""),format.raw/*37.30*/("""
		"""),format.raw/*38.3*/("""}"""),format.raw/*38.4*/("""
		else if(""""),_display_(Seq[Any](/*39.13*/p/*39.14*/._2)),format.raw/*39.17*/("""" === "Video")"""),format.raw/*39.31*/("""{"""),format.raw/*39.32*/("""
			Configuration.fileType = """"),_display_(Seq[Any](/*40.31*/p/*40.32*/._6)),format.raw/*40.35*/("""";
		"""),format.raw/*41.3*/("""}"""),format.raw/*41.4*/("""
		else if(""""),_display_(Seq[Any](/*42.13*/p/*42.14*/._2)),format.raw/*42.17*/("""" === "Video presentation")"""),format.raw/*42.44*/("""{"""),format.raw/*42.45*/("""
			Configuration.authenticatedFileModify = false;
			"""),_display_(Seq[Any](/*44.5*/if(user.isDefined)/*44.23*/ {_display_(Seq[Any](format.raw/*44.25*/("""
	     	 	"""),_display_(Seq[Any](/*45.11*/if(user.get.id.equals(file.author.id))/*45.49*/{_display_(Seq[Any](format.raw/*45.50*/("""
					Configuration.authenticatedFileModify = true;
	     	 	""")))})),format.raw/*47.11*/("""
			""")))})),format.raw/*48.5*/("""			
		"""),format.raw/*49.3*/("""}"""),format.raw/*49.4*/("""
		else if(""""),_display_(Seq[Any](/*50.13*/p/*50.14*/._2)),format.raw/*50.17*/("""" === "External resource")"""),format.raw/*50.43*/("""{"""),format.raw/*50.44*/("""
			Configuration.authenticatedFileModify = false;
			"""),_display_(Seq[Any](/*52.5*/if(user.isDefined)/*52.23*/ {_display_(Seq[Any](format.raw/*52.25*/("""
	     	 	"""),_display_(Seq[Any](/*53.11*/if(user.get.id.equals(file.author.id))/*53.49*/{_display_(Seq[Any](format.raw/*53.50*/("""
					Configuration.authenticatedFileModify = true;
	     	 	""")))})),format.raw/*55.11*/("""
			""")))})),format.raw/*56.5*/("""			
		"""),format.raw/*57.3*/("""}"""),format.raw/*57.4*/("""
	</script>
	<script type="text/javascript" src=""""),_display_(Seq[Any](/*59.39*/(routes.Assets.at(p._3) + "/" + p._4))),format.raw/*59.76*/(""""></script>
""")))};implicit def /*16.2*/implicitFieldConstructor/*16.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*8.148*/("""

"""),format.raw/*15.1*/("""
"""),format.raw/*16.75*/("""


	"""),format.raw/*19.119*/("""
"""),format.raw/*60.2*/("""

"""),_display_(Seq[Any](/*62.2*/main("File")/*62.14*/ {_display_(Seq[Any](format.raw/*62.16*/("""

	<style>

        #aboutdesc """),format.raw/*66.20*/("""{"""),format.raw/*66.21*/("""
            padding-top: 0px;
        """),format.raw/*68.9*/("""}"""),format.raw/*68.10*/("""

        .nav-tabs """),format.raw/*70.19*/("""{"""),format.raw/*70.20*/("""
        	display: block; 
        """),format.raw/*72.9*/("""}"""),format.raw/*72.10*/("""

        .nav>li>a """),format.raw/*74.19*/("""{"""),format.raw/*74.20*/("""
            display: block;
        """),format.raw/*76.9*/("""}"""),format.raw/*76.10*/("""        

		@media only screen and (max-width:1100px) """),format.raw/*78.46*/("""{"""),format.raw/*78.47*/("""
			.navbar """),format.raw/*79.12*/("""{"""),format.raw/*79.13*/("""
				display: none;
			"""),format.raw/*81.4*/("""}"""),format.raw/*81.5*/("""

			.breadcrumb """),format.raw/*83.16*/("""{"""),format.raw/*83.17*/("""
				display: none;
			"""),format.raw/*85.4*/("""}"""),format.raw/*85.5*/("""

			.container """),format.raw/*87.15*/("""{"""),format.raw/*87.16*/("""
				padding-left: 0px;
				margin-left: 30px;
			"""),format.raw/*90.4*/("""}"""),format.raw/*90.5*/("""

			body """),format.raw/*92.9*/("""{"""),format.raw/*92.10*/("""
				padding-top: 10px;
				background-color: #eeeeee;
			"""),format.raw/*95.4*/("""}"""),format.raw/*95.5*/("""

			h1 """),format.raw/*97.7*/("""{"""),format.raw/*97.8*/("""
    			font-size: 24px;
			"""),format.raw/*99.4*/("""}"""),format.raw/*99.5*/("""
		"""),format.raw/*100.3*/("""}"""),format.raw/*100.4*/("""

		@media only screen and (max-width: 550px) """),format.raw/*102.46*/("""{"""),format.raw/*102.47*/("""

			.button-list """),format.raw/*104.17*/("""{"""),format.raw/*104.18*/("""
				display: none;
			"""),format.raw/*106.4*/("""}"""),format.raw/*106.5*/("""

			#downloadAndDelete """),format.raw/*108.23*/("""{"""),format.raw/*108.24*/("""
                display: none;
            """),format.raw/*110.13*/("""}"""),format.raw/*110.14*/("""

            #extractionsRow """),format.raw/*112.29*/("""{"""),format.raw/*112.30*/("""
                display: none;
            """),format.raw/*114.13*/("""}"""),format.raw/*114.14*/("""

            #commentsSection """),format.raw/*116.30*/("""{"""),format.raw/*116.31*/("""
                display: none;
            """),format.raw/*118.13*/("""}"""),format.raw/*118.14*/("""

			#aboutdesc """),format.raw/*120.15*/("""{"""),format.raw/*120.16*/("""
				display: none;
			"""),format.raw/*122.4*/("""}"""),format.raw/*122.5*/("""

			.navbar """),format.raw/*124.12*/("""{"""),format.raw/*124.13*/("""
				display: none;
			"""),format.raw/*126.4*/("""}"""),format.raw/*126.5*/("""

			.breadcrumb """),format.raw/*128.16*/("""{"""),format.raw/*128.17*/("""
				display: none;
			"""),format.raw/*130.4*/("""}"""),format.raw/*130.5*/("""

			#file-name-div """),format.raw/*132.19*/("""{"""),format.raw/*132.20*/("""
				display: none;
			"""),format.raw/*134.4*/("""}"""),format.raw/*134.5*/("""

			body """),format.raw/*136.9*/("""{"""),format.raw/*136.10*/("""
				padding-top: 0px;
				background-color: #eeeeee;
			"""),format.raw/*139.4*/("""}"""),format.raw/*139.5*/("""

			#table-content """),format.raw/*141.19*/("""{"""),format.raw/*141.20*/("""
				display: none;
			"""),format.raw/*143.4*/("""}"""),format.raw/*143.5*/("""

			.license-sec """),format.raw/*145.17*/("""{"""),format.raw/*145.18*/("""
				display: none;
			"""),format.raw/*147.4*/("""}"""),format.raw/*147.5*/("""

			.datafile """),format.raw/*149.14*/("""{"""),format.raw/*149.15*/("""
				display: none;
			"""),format.raw/*151.4*/("""}"""),format.raw/*151.5*/("""

			.datatags """),format.raw/*153.14*/("""{"""),format.raw/*153.15*/("""
				display: none;
			"""),format.raw/*155.4*/("""}"""),format.raw/*155.5*/("""

            #moveToDatasetSec """),format.raw/*157.31*/("""{"""),format.raw/*157.32*/("""
                display: none;
            """),format.raw/*159.13*/("""}"""),format.raw/*159.14*/("""

		"""),format.raw/*161.3*/("""}"""),format.raw/*161.4*/("""
	</style>

	<script src=""""),_display_(Seq[Any](/*164.16*/routes/*164.22*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*164.64*/("""" type="text/javascript"></script>
	<script src=""""),_display_(Seq[Any](/*165.16*/routes/*165.22*/.Assets.at("javascripts/updateLicenseInfo.js"))),format.raw/*165.68*/("""" language="javascript"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
	<script src=""""),_display_(Seq[Any](/*167.16*/routes/*167.22*/.Assets.at("javascripts/previews.js"))),format.raw/*167.59*/("""" type="text/javascript"></script>
	<script src=""""),_display_(Seq[Any](/*168.16*/routes/*168.22*/.Assets.at("javascripts/popcorn-complete.min.js"))),format.raw/*168.71*/("""" type="text/javascript"></script>
	<script src=""""),_display_(Seq[Any](/*169.16*/routes/*169.22*/.Assets.at("javascripts/galleria-1.2.9.js"))),format.raw/*169.65*/("""" type="text/javascript"></script>
	<script src=""""),_display_(Seq[Any](/*170.16*/routes/*170.22*/.Assets.at("javascripts/fileListProcess.js"))),format.raw/*170.66*/("""" type="text/javascript"></script>
	<script src=""""),_display_(Seq[Any](/*171.16*/routes/*171.22*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*171.62*/("""" type="text/javascript"></script>
	<script src=""""),_display_(Seq[Any](/*172.16*/routes/*172.22*/.Assets.at("javascripts/sectionsListProcess.js"))),format.raw/*172.70*/("""" type="text/javascript"></script>
	<script src=""""),_display_(Seq[Any](/*173.16*/routes/*173.22*/.Assets.at("javascripts/files/datasets.js"))),format.raw/*173.65*/("""" type="text/javascript"></script>
	<script>
			var spaceId = """"),_display_(Seq[Any](/*175.20*/spaceId)),format.raw/*175.27*/("""";
			var isPageLoaded = false;
			var isx3dActive = false;
			var Configuration = """),format.raw/*178.24*/("""{"""),format.raw/*178.25*/("""}"""),format.raw/*178.26*/(""";
			Configuration.id  = """"),_display_(Seq[Any](/*179.26*/file/*179.30*/.id)),format.raw/*179.33*/("""";
			Configuration.authenticated = """),_display_(Seq[Any](/*180.35*/user/*180.39*/.isDefined)),format.raw/*180.49*/(""";
			Configuration.ptmAppletPath = """"),_display_(Seq[Any](/*181.36*/(routes.Assets.at("plugins") + "/" + "envlib.jar"))),format.raw/*181.86*/("""";
			Configuration.expressInstallPath = """"),_display_(Seq[Any](/*182.41*/(routes.Assets.at("plugins") + "/" + "expressInstall.swf"))),format.raw/*182.99*/("""";
			Configuration.iipZoomPath = """"),_display_(Seq[Any](/*183.34*/(routes.Assets.at("plugins") + "/" + "IIPZoom.swf"))),format.raw/*183.85*/("""";
			Configuration.jsPath = """"),_display_(Seq[Any](/*184.29*/(routes.Assets.at("javascripts")))),format.raw/*184.62*/("""";
			Configuration.imagesPath = """"),_display_(Seq[Any](/*185.33*/(routes.Assets.at("images")))),format.raw/*185.61*/("""";
			Configuration.appContext = """"),_display_(Seq[Any](/*186.33*/play/*186.37*/.api.Play.configuration.getString("application.context").getOrElse(""))),format.raw/*186.107*/("""";
			var cur_description;
			function updateFileDescription() """),format.raw/*188.37*/("""{"""),format.raw/*188.38*/("""
				cur_description = $('#ds_description').html().trim();
				$('<div class="edit_desc"> </div>').insertAfter($('#ds_description'));
				$('.edit_desc').append('<textarea id = "desc_input" class="form-control"/>');
				$('.edit_desc').append('<button id = "update_desc" class= "btn btn-sm btn-primary btn-margins" onclick = "saveDescription()"> <span class="glyphicon glyphicon-send"> </span> Save </button>');
				$('.edit_desc').append('<button id="cancel_desc" class="btn btn-sm edit-tab btn-default btn-margins" onclick="cancelDescription()"> <span class="glyphicon glyphicon-remove"></span> Cancel </button>');
				if(cur_description.indexOf("Add a description") != 0) """),format.raw/*194.59*/("""{"""),format.raw/*194.60*/("""
					$('#desc_input').val(htmlDecode(cur_description.replace(new RegExp("<br>", "g"), "\n")));
				"""),format.raw/*196.5*/("""}"""),format.raw/*196.6*/("""
				$('#ds_description').text("");
				$('#edit-description').css("display", "none");
				$('#edit-description').addClass("hiddencomplete");
			"""),format.raw/*200.4*/("""}"""),format.raw/*200.5*/("""

			function cancelDescription() """),format.raw/*202.33*/("""{"""),format.raw/*202.34*/("""
				$('#ds_description').html(cur_description);
				$('.edit_desc').remove();
				$('#ds_description').css("display", "inline");
				$('#edit-description').removeClass("inline");
				$('#edit-description').css("display", "");
				$('#aboutdesc').mouseleave();
			"""),format.raw/*209.4*/("""}"""),format.raw/*209.5*/("""

			function saveDescription() """),format.raw/*211.31*/("""{"""),format.raw/*211.32*/("""
				var description = $('#desc_input').val();
				var encDescription = htmlEncode(description);
				jsonData = JSON.stringify("""),format.raw/*214.31*/("""{"""),format.raw/*214.32*/(""""description": encDescription"""),format.raw/*214.61*/("""}"""),format.raw/*214.62*/(""");

				var request = jsRoutes.api.Files.updateDescription(""""),_display_(Seq[Any](/*216.58*/file/*216.62*/.id)),format.raw/*216.65*/("""").ajax("""),format.raw/*216.73*/("""{"""),format.raw/*216.74*/("""
					data: jsonData,
					type: 'PUT',
					contentType: "application/json"
				"""),format.raw/*220.5*/("""}"""),format.raw/*220.6*/(""");

				request.done(function(response, textStatus,jqXHR) """),format.raw/*222.55*/("""{"""),format.raw/*222.56*/("""
					$('#ds_description').html(urlify(htmlEncode(description).replace(/\n/g, "<br>")));
					$('.edit_desc').remove();
					$('#ds_description').css("display", "inline");
					$('#edit-description').removeClass("inline");
					$('#edit-description').css("display", "");
					$('#aboutdesc').mouseleave();
					notify("File description updated successfully", "success", false, 2000 );
				"""),format.raw/*230.5*/("""}"""),format.raw/*230.6*/(""");

				request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*232.59*/("""{"""),format.raw/*232.60*/("""
					console.error("The following error occurred: " + textStatus, errorThrown);
					var errMsg = " You must be logged in to update the information about a collection.";
					if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*235.48*/("""{"""),format.raw/*235.49*/("""
						notify("The collection information was not updated due to: " + errorThrown, "error");
					"""),format.raw/*237.6*/("""}"""),format.raw/*237.7*/("""
				"""),format.raw/*238.5*/("""}"""),format.raw/*238.6*/(""");

			"""),format.raw/*240.4*/("""}"""),format.raw/*240.5*/("""

			function urlify(text) """),format.raw/*242.26*/("""{"""),format.raw/*242.27*/("""
				var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
				return text.replace(urlRegex, function(url) """),format.raw/*244.49*/("""{"""),format.raw/*244.50*/("""
					return '<a href="' + url + '">' + url + '</a>';
				"""),format.raw/*246.5*/("""}"""),format.raw/*246.6*/(""")
			"""),format.raw/*247.4*/("""}"""),format.raw/*247.5*/("""
	</script>
	<script>
			window.onload = function() """),format.raw/*250.31*/("""{"""),format.raw/*250.32*/("""
				isPageLoaded = true;
			"""),format.raw/*252.4*/("""}"""),format.raw/*252.5*/("""
	</script>

	<div class="row">
		<div class="col-md-8 col-sm-8 col-lg-8">
			<div class="row">
				<ol class="breadcrumb">
					"""),_display_(Seq[Any](/*259.7*/if(spaces.length == 1 )/*259.30*/ {_display_(Seq[Any](format.raw/*259.32*/("""
							<li><span class="glyphicon glyphicon-hdd"></span> <a href=""""),_display_(Seq[Any](/*260.68*/routes/*260.74*/.Spaces.getSpace(spaces.head.id))),format.raw/*260.106*/("""" title=""""),_display_(Seq[Any](/*260.116*/spaces/*260.122*/.head.name)),format.raw/*260.132*/(""""> """),_display_(Seq[Any](/*260.136*/Html(ellipsize(spaces.head.name, 18)))),format.raw/*260.173*/("""</a></li>

					""")))}/*262.8*/else/*262.13*/{_display_(Seq[Any](format.raw/*262.14*/("""
						"""),_display_(Seq[Any](/*263.8*/if(spaces.length > 1)/*263.29*/ {_display_(Seq[Any](format.raw/*263.31*/("""
							<li>
							<span class="dropdown">
								<button class="btn-link dropdown-toggle" type="button" id="dropdown_space_list" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="true">
									<span class="glyphicon glyphicon-hdd"></span> <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" arialanelledby="dropdown_space_list">
									"""),_display_(Seq[Any](/*271.11*/spaces/*271.17*/.map/*271.21*/{ s =>_display_(Seq[Any](format.raw/*271.27*/("""
										<li><a href=""""),_display_(Seq[Any](/*272.25*/routes/*272.31*/.Spaces.getSpace(s.id))),format.raw/*272.53*/("""" title="s.name"><span class="glyphicon glyphicon-hdd"></span> """),_display_(Seq[Any](/*272.117*/Html(ellipsize(s.name, 18)))),format.raw/*272.144*/("""</a></li>
									""")))})),format.raw/*273.11*/("""
								</ul>

							</span>
							</li>
						""")))}/*278.9*/else/*278.14*/{_display_(Seq[Any](format.raw/*278.15*/("""
							<li><span class="glyphicon glyphicon-user"></span> <a href = """"),_display_(Seq[Any](/*279.71*/routes/*279.77*/.Profile.viewProfileUUID(file.author.id))),format.raw/*279.117*/(""""> """),_display_(Seq[Any](/*279.121*/file/*279.125*/.author.fullName)),format.raw/*279.141*/("""</a></li>
						""")))})),format.raw/*280.8*/("""
					""")))})),format.raw/*281.7*/("""
					"""),_display_(Seq[Any](/*282.7*/if(allDatasets.length == 1 )/*282.35*/ {_display_(Seq[Any](format.raw/*282.37*/("""
						"""),_display_(Seq[Any](/*283.8*/allDatasets/*283.19*/.map/*283.23*/ { ds =>_display_(Seq[Any](format.raw/*283.31*/("""
						<li> <span class="glyphicon glyphicon-briefcase"></span> <a href=""""),_display_(Seq[Any](/*284.74*/routes/*284.80*/.Datasets.dataset(ds.id))),format.raw/*284.104*/("""" title=""""),_display_(Seq[Any](/*284.114*/ds/*284.116*/.name)),format.raw/*284.121*/(""""> """),_display_(Seq[Any](/*284.125*/Html(ellipsize(ds.name, 18)))),format.raw/*284.153*/("""</a></li>
						""")))})),format.raw/*285.8*/("""
					""")))})),format.raw/*286.7*/("""
					"""),_display_(Seq[Any](/*287.7*/folderHierarchy/*287.22*/.map/*287.26*/ { fd =>_display_(Seq[Any](format.raw/*287.34*/("""
						<li><span class="glyphicon glyphicon-folder-close"></span> <a href=""""),_display_(Seq[Any](/*288.76*/routes/*288.82*/.Datasets.dataset(allDatasets(0).id))),format.raw/*288.118*/("""#folderId="""),_display_(Seq[Any](/*288.129*/fd/*288.131*/.id)),format.raw/*288.134*/("""" title=""""),_display_(Seq[Any](/*288.144*/fd/*288.146*/.displayName)),format.raw/*288.158*/("""">"""),_display_(Seq[Any](/*288.161*/Html(ellipsize(fd.displayName, 18)))),format.raw/*288.196*/("""</a></li>
					""")))})),format.raw/*289.7*/("""
					<li><span class="glyphicon glyphicon-file"></span> <span title=""""),_display_(Seq[Any](/*290.71*/file/*290.75*/.filename)),format.raw/*290.84*/("""">"""),_display_(Seq[Any](/*290.87*/Html(ellipsize(file.filename, 18)))),format.raw/*290.121*/("""</span></li>

				</ol>
				<div id="file-name-div" class="file-title-div col-xs-12">
					<div id="prf-file-name" class="text-left inline">
						<h1 id="file-name-title" class="inline"> <span class="glyphicon glyphicon-file"></span> """),_display_(Seq[Any](/*295.96*/Html(file.filename))),format.raw/*295.115*/("""</h1>
						"""),_display_(Seq[Any](/*296.8*/if(Permission.checkPermission(Permission.EditFile, ResourceRef(ResourceRef.file, file.id)))/*296.99*/ {_display_(Seq[Any](format.raw/*296.101*/("""
							<div id="h-edit-file" class="hiddencomplete">
								<a id ="edit-file" href="javascript:updateFileName()" title="Click to edit file name.">
									<span class ="glyphicon glyphicon-edit" aria-hidden ="true"></span>
								</a>
							</div>
						""")))})),format.raw/*302.8*/("""
					</div>
				</div>
			</div>
			<div class="row bottom-padding">
				<div class="col-md-12 box-white-space" id="aboutdesc">
					<p id ="ds_description" class="inline">"""),_display_(Seq[Any](/*308.46*/if(file.description.length > 0)/*308.77*/ {_display_(Seq[Any](format.raw/*308.79*/(""" """),_display_(Seq[Any](/*308.81*/Html(file.description))),format.raw/*308.103*/(""" """)))}/*308.106*/else/*308.111*/{_display_(Seq[Any](format.raw/*308.112*/("""
						"""),_display_(Seq[Any](/*309.8*/if(Permission.checkPermission(Permission.EditFile, ResourceRef(ResourceRef.file, file.id)))/*309.99*/{_display_(Seq[Any](format.raw/*309.100*/("""
							Add a description
						""")))})),format.raw/*311.8*/("""
					""")))})),format.raw/*312.7*/("""</p>
					"""),_display_(Seq[Any](/*313.7*/if(Permission.checkPermission(Permission.EditFile, ResourceRef(ResourceRef.file, file.id)))/*313.98*/ {_display_(Seq[Any](format.raw/*313.100*/("""
						<a id="edit-description" class="hiddencomplete" href="javascript:updateFileDescription()" title="Click to edit description">
							<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
						</a>
					""")))})),format.raw/*317.7*/("""
				</div>
			</div>
			"""),_display_(Seq[Any](/*320.5*/for((f, pvs) <- previews) yield /*320.30*/ {_display_(Seq[Any](format.raw/*320.32*/("""
				<div class="row" id="filePrevContainer_"""),_display_(Seq[Any](/*321.45*/(f.id.toString))),format.raw/*321.60*/("""" data-previewsCount=""""),_display_(Seq[Any](/*321.83*/(pvs.length))),format.raw/*321.95*/("""" data-filename=""""),_display_(Seq[Any](/*321.113*/f/*321.114*/.filename)),format.raw/*321.123*/("""">
					<div class="col-md-12">
							<!--
		    	First check if there is only one previewer, and if the value of the URL for it is "null". If so
		    	that means there was no preview generated and the file is not allowed to be used due to the license
		    	that is on the file.
		    	 -->
						"""),_display_(Seq[Any](/*328.8*/if(pvs.length == 1 && pvs(0)._5 == "null")/*328.50*/ {_display_(Seq[Any](format.raw/*328.52*/("""
							<h4>No previews currently available for this file</h4>
						""")))}/*330.9*/else/*330.14*/{_display_(Seq[Any](format.raw/*330.15*/("""
							<div class="tabbable">
								<ul class="nav nav-pills" id="myTab_"""),_display_(Seq[Any](/*332.46*/(f.id.toString))),format.raw/*332.61*/("""">
								"""),_display_(Seq[Any](/*333.10*/for(i <- pvs.indices) yield /*333.31*/ {_display_(Seq[Any](format.raw/*333.33*/("""
										<!-- Use preview.title as tab title if defined; otherwise use previewer name. -->
									"""),_display_(Seq[Any](/*335.11*/if(pvs(i)._8 != "")/*335.30*/ {_display_(Seq[Any](format.raw/*335.32*/("""
										<li class=""><a href="#previewer_"""),_display_(Seq[Any](/*336.45*/(f.id.toString))),format.raw/*336.60*/("""_"""),_display_(Seq[Any](/*336.62*/i)),format.raw/*336.63*/("""" data-toggle="tab">"""),_display_(Seq[Any](/*336.84*/pvs(i)/*336.90*/._8)),format.raw/*336.93*/("""</a></li>
									""")))}/*337.12*/else/*337.17*/{_display_(Seq[Any](format.raw/*337.18*/("""
										<li class=""><a href="#previewer_"""),_display_(Seq[Any](/*338.45*/(f.id.toString))),format.raw/*338.60*/("""_"""),_display_(Seq[Any](/*338.62*/i)),format.raw/*338.63*/("""" data-toggle="tab">"""),_display_(Seq[Any](/*338.84*/pvs(i)/*338.90*/._2)),format.raw/*338.93*/("""</a></li>
									""")))})),format.raw/*339.11*/("""
								""")))})),format.raw/*340.10*/("""
								</ul>
								<div class="tab-content"  id="previewsContent_"""),_display_(Seq[Any](/*342.56*/(f.id.toString))),format.raw/*342.71*/("""">
									"""),_display_(Seq[Any](/*343.11*/for(i <- pvs.indices) yield /*343.32*/ {_display_(Seq[Any](format.raw/*343.34*/("""
										<div class="tab-pane previewDiv" id="previewer_"""),_display_(Seq[Any](/*344.59*/(f.id.toString))),format.raw/*344.74*/("""_"""),_display_(Seq[Any](/*344.76*/i)),format.raw/*344.77*/("""" data-previewId=""""),_display_(Seq[Any](/*344.96*/(pvs(i)._1))),format.raw/*344.107*/("""" data-previewerId=""""),_display_(Seq[Any](/*344.128*/(pvs(i)._2))),format.raw/*344.139*/(""""></div>
										"""),_display_(Seq[Any](/*345.12*/showPreview(f.id.toString + "_" + i, pvs(i), f.filename, f.contentType))),format.raw/*345.83*/("""

									""")))})),format.raw/*347.11*/("""
									<script type="text/javascript">
											$(function () """),format.raw/*349.26*/("""{"""),format.raw/*349.27*/("""
												$('#myTab_"""),_display_(Seq[Any](/*350.24*/(f.id.toString))),format.raw/*350.39*/(""" a:first').tab('show');
											"""),format.raw/*351.12*/("""}"""),format.raw/*351.13*/(""")
									</script>
								</div>
							</div>
						""")))})),format.raw/*355.8*/("""
					</div>
				</div>
				"""),_display_(Seq[Any](/*358.6*/if(pvs.length <= 1)/*358.25*/ {_display_(Seq[Any](format.raw/*358.27*/("""
					<script>
							$('#myTab_"""),_display_(Seq[Any](/*360.19*/(f.id.toString))),format.raw/*360.34*/("""').attr('style', 'display:none;');
					</script>
				""")))})),format.raw/*362.6*/("""

				<div class="row bottom-padding" id="downloadAndDelete">
					<div class="col-md-12">
						<hr/>
						<div class="btn-group" role="group" aria-label="...">
							"""),_display_(Seq[Any](/*368.9*/if( Permission.checkPermission(Permission.DownloadFiles, ResourceRef(ResourceRef.file, file.id)))/*368.106*/ {_display_(Seq[Any](format.raw/*368.108*/("""
								<button id="downloadButton" type="button" onclick="return downloadFile();" class="btn btn-link" title="Download and enjoy this file."> <span class="glyphicon glyphicon-save"></span>
									Download </button>
							""")))}/*371.10*/else/*371.15*/{_display_(Seq[Any](format.raw/*371.16*/("""
									<!-- if 'disabled' title is not visible -->
								<button type="button" class="btn btn-link disabled" title="You don't have permission to download this file."> <span class="glyphicon glyphicon-save"></span>
									Download </button>
							""")))})),format.raw/*375.9*/("""
							<!--  If outputFormats list is not empty, display downloadAs drop-down button with the list of available formats -->
							"""),_display_(Seq[Any](/*377.9*/if(!outputFormats.isEmpty)/*377.35*/{_display_(Seq[Any](format.raw/*377.36*/("""
								<div class="btn-group" role="group">
									<button id="downloadAsButton" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <span class="glyphicon glyphicon-download-alt"></span> </button>
									<ul class="dropdown-menu" role="menu">
									"""),_display_(Seq[Any](/*381.11*/outputFormats/*381.24*/.get.map/*381.32*/ { outputFrmt =>_display_(Seq[Any](format.raw/*381.48*/("""
										<li id="doc" onclick="return downloadFileAs('"""),_display_(Seq[Any](/*382.57*/routes/*382.63*/.Files.downloadAsFormat(UUID(id), outputFrmt))),format.raw/*382.108*/("""'  );" > <a href="#">"""),_display_(Seq[Any](/*382.130*/outputFrmt)),format.raw/*382.140*/("""</a> </li>
									""")))})),format.raw/*383.11*/("""
									</ul>
								</div> <!-- end of class button-group -->
								""")))})),format.raw/*386.10*/(""" <!-- end of    if(!outputFormats.isEmpty)-->
						</div> <!-- end of class btn-group -->
					"""),_display_(Seq[Any](/*388.7*/if(user.isDefined)/*388.25*/ {_display_(Seq[Any](format.raw/*388.27*/("""
						<!-- If user can delete, the button is enabled, otherwise the button is present but disabled to provide consistent UE. -->
						"""),_display_(Seq[Any](/*390.8*/if(user.get.id.equals(file.author.id) || Permission.checkPermission(Permission.DeleteFile, ResourceRef(ResourceRef.file, file.id)))/*390.139*/{_display_(Seq[Any](format.raw/*390.140*/("""
							<span>
							"""),_display_(Seq[Any](/*392.9*/if(datasets.length > 0)/*392.32*/ {_display_(Seq[Any](format.raw/*392.34*/("""
								<button id="deleteButton" onclick="confirmDeleteResource('file','file','"""),_display_(Seq[Any](/*393.82*/(file.id))),format.raw/*393.91*/("""','"""),_display_(Seq[Any](/*393.95*/(file.filename.replace("'","&#39;")))),format.raw/*393.131*/("""', true, '"""),_display_(Seq[Any](/*393.142*/(routes.Datasets.dataset(datasets.head.id)))),format.raw/*393.185*/("""')" class="btn btn-link" title="Delete the file">
									<span class="glyphicon glyphicon-trash"></span> Delete</button>
							""")))}/*395.10*/else/*395.15*/{_display_(Seq[Any](format.raw/*395.16*/("""
								"""),_display_(Seq[Any](/*396.10*/if(folders.length > 0)/*396.32*/ {_display_(Seq[Any](format.raw/*396.34*/("""
									<button id="deleteButton" onclick="confirmDeleteResource('file','file','"""),_display_(Seq[Any](/*397.83*/(file.id))),format.raw/*397.92*/("""','"""),_display_(Seq[Any](/*397.96*/(file.filename.replace("'","&#39;")))),format.raw/*397.132*/("""', true, '"""),_display_(Seq[Any](/*397.143*/(routes.Datasets.dataset(folders.head.parentDatasetId)))),format.raw/*397.198*/("""#folderId="""),_display_(Seq[Any](/*397.209*/folders/*397.216*/.head.id)),format.raw/*397.224*/("""')" class="btn btn-link" title="Delete the file">
									<span class="glyphicon glyphicon-trash"></span> Delete</button>
								""")))})),format.raw/*399.10*/("""
							""")))})),format.raw/*400.9*/("""

							</span>
						""")))}/*403.9*/else/*403.14*/{_display_(Seq[Any](format.raw/*403.15*/("""
							<span class="align-right" title="No permission to delete the file">
								<button disabled class="btn btn-link"><span class="glyphicon glyphicon-trash"></span> Delete</button>
							</span>
						""")))})),format.raw/*407.8*/("""

						"""),_display_(Seq[Any](/*409.8*/if(!file.followers.contains(user.get.id))/*409.49*/ {_display_(Seq[Any](format.raw/*409.51*/("""
								<!-- 							<button id="followButton"
							class="btn btn-link followButton"
							objectId=""""),_display_(Seq[Any](/*412.19*/file/*412.23*/.id)),format.raw/*412.26*/(""""
								objectName=""""),_display_(Seq[Any](/*413.22*/file/*413.26*/.filename)),format.raw/*413.35*/(""""
								objectType="file"><span class="glyphicon glyphicon-star"></span> Follow
							</button>
							-->						""")))}/*416.19*/else/*416.24*/{_display_(Seq[Any](format.raw/*416.25*/("""
							<!-- 							<button id="followButton"
							class="btn btn-link followButton"
							objectId=""""),_display_(Seq[Any](/*419.19*/file/*419.23*/.id)),format.raw/*419.26*/(""""
							objectName=""""),_display_(Seq[Any](/*420.21*/file/*420.25*/.filename)),format.raw/*420.34*/(""""
							objectType="file"><span class="glyphicon glyphicon-star-empty"></span> Unfollow
							</button>
							-->						""")))})),format.raw/*423.18*/("""
						""")))})),format.raw/*424.8*/("""
						<hr/>
					</div>
				</div>
					<!--
				<div id="recommendPanel" class="panel panel-default" style="display : none;">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-parent="#accordion"
							href="#collapseThree"
							aria-expanded="true"
							style="float:left;">
                      Also follow these?
							</a>
							<a style="float:right;" href="javascript:$('#recommendPanel').slideToggle('slow');">x</a>
							<div style="clear : both;"></div>
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse in" aria-expanded="true">
						<div id="recommendDiv" class="panel-body">
                  </div>
					</div>
				</div> -->

					<!-- sections -->
				"""),_display_(Seq[Any](/*449.6*/if(sections.size > 0)/*449.27*/ {_display_(Seq[Any](format.raw/*449.29*/("""
					<div class="row">
						<div class="col-md-12">
							<h4>Sections</h4>
							<div class="galleriaHelp">Click on icon at bottom-right of gallery to toggle sections gallery fullscreen view</div>
							<div id="galleria" style="float:left;clear:none;width:100%;height:400px;">
							</div>
							<script type="text/javascript">
									$(document).ready(function() """),format.raw/*457.39*/("""{"""),format.raw/*457.40*/("""
										window["currGalleriaWidth"]= $("#galleria").css("width").replace("px","");
										$(window).resize(function()"""),format.raw/*459.38*/("""{"""),format.raw/*459.39*/("""
											newWidth = $("#galleria").css("width").replace("px","");
											if(newWidth != window["currGalleriaWidth"])"""),format.raw/*461.55*/("""{"""),format.raw/*461.56*/("""
												$("#galleria").css("height", Math.round(parseInt($("#galleria").css("height").replace("px",""))*parseInt(newWidth)/parseFloat(window["currGalleriaWidth"]))+"px" );
												window["currGalleriaWidth"]=newWidth;
											"""),format.raw/*464.12*/("""}"""),format.raw/*464.13*/("""
										"""),format.raw/*465.11*/("""}"""),format.raw/*465.12*/(""");
									"""),format.raw/*466.10*/("""}"""),format.raw/*466.11*/(""");
							</script>
						</div>
					</div>
					<div class="row" style="padding-top: 10px">
						<div class="col-md-12">
							<table class="table" id="scrollTableGalleria">
								<thead>
									<tr>
										<th class="galleriaOrder">#</th>
										<th class="galleriaTheThumbnail">Thumbnail</th>
										<th class="galleriaPosition">Position</th>
										<th class="galleriaDescr">Description</th>
										<th class="galleriaFindSimilar">Find</th>
									</tr>
								</thead>
								<tbody>
								"""),_display_(Seq[Any](/*483.10*/sections/*483.18*/.map/*483.22*/ { s =>_display_(Seq[Any](format.raw/*483.29*/("""
									<tr>
										<td class="galleriaOrder">"""),_display_(Seq[Any](/*485.38*/(if (s.order > 0) s.order else ""))),format.raw/*485.72*/("""</td>
										<td class="galleriaTheThumbnail">
										"""),_display_(Seq[Any](/*487.12*/if(s.preview.isDefined || s.thumbnail_id.isDefined)/*487.63*/ {_display_(Seq[Any](format.raw/*487.65*/("""
											"""),_display_(Seq[Any](/*488.13*/if(s.thumbnail_id.isDefined)/*488.41*/{_display_(Seq[Any](format.raw/*488.42*/("""
												<img src=""""),_display_(Seq[Any](/*489.24*/(routes.Files.thumbnail(UUID(s.thumbnail_id.get))))),format.raw/*489.74*/(""""
											""")))}/*490.14*/else/*490.19*/{_display_(Seq[Any](format.raw/*490.20*/("""
												<img src=""""),_display_(Seq[Any](/*491.24*/api/*491.27*/.routes.Previews.download(s.preview.get.id))),format.raw/*491.70*/(""""
												""")))})),format.raw/*492.14*/("""
											class="img-polaroid" style="max-width: 100%;"
											"""),_display_(Seq[Any](/*494.13*/if(s.startTime.isDefined)/*494.38*/ {_display_(Seq[Any](format.raw/*494.40*/("""
												data-title="Shot" data-description="Starts at """),_display_(Seq[Any](/*495.60*/s/*495.61*/.startTime)),format.raw/*495.71*/(""" seconds."
											""")))}/*496.14*/else/*496.19*/{_display_(Seq[Any](format.raw/*496.20*/("""
												"""),_display_(Seq[Any](/*497.14*/if(s.area.isDefined)/*497.34*/ {_display_(Seq[Any](format.raw/*497.36*/("""
													data-title="Area" data-description=""""),_display_(Seq[Any](/*498.51*/s/*498.52*/.area.toString)),format.raw/*498.66*/(""""
												""")))}/*499.15*/else/*499.20*/{_display_(Seq[Any](format.raw/*499.21*/("""
													data-title="Unknown" data-description="Unknown info"
													""")))})),format.raw/*501.15*/("""
											""")))})),format.raw/*502.13*/("""
											></img>
										""")))})),format.raw/*504.12*/("""
										</td>
										<td class="galleriaPosition">
										"""),_display_(Seq[Any](/*507.12*/if(s.startTime.isDefined)/*507.37*/ {_display_(Seq[Any](format.raw/*507.39*/("""
											"""),_display_(Seq[Any](/*508.13*/(s.startTime.get / 60))),format.raw/*508.35*/(""":"""),_display_(Seq[Any](/*508.37*/("%02d".format(s.startTime.get % 60)))),format.raw/*508.74*/("""
										""")))}/*509.13*/else/*509.18*/{_display_(Seq[Any](format.raw/*509.19*/("""
											"""),_display_(Seq[Any](/*510.13*/if(s.area.isDefined)/*510.33*/ {_display_(Seq[Any](format.raw/*510.35*/("""
												"""),_display_(Seq[Any](/*511.14*/s/*511.15*/.area.get.toString)),format.raw/*511.33*/("""
											""")))}/*512.14*/else/*512.19*/{_display_(Seq[Any](format.raw/*512.20*/("""
												Unknown info
											""")))})),format.raw/*514.13*/("""
										""")))})),format.raw/*515.12*/("""
										</td>
										<td class="galleriaDescr">
											<span id="sectiondescr_"""),_display_(Seq[Any](/*518.36*/(s.id))),format.raw/*518.42*/(""""  style="display:block;white-space: pre-line;">"""),_display_(Seq[Any](/*518.91*/s/*518.92*/.description.getOrElse(""))),format.raw/*518.118*/("""</span>
											"""),_display_(Seq[Any](/*519.13*/if(user.isDefined)/*519.31*/ {_display_(Seq[Any](format.raw/*519.33*/("""
												"""),_display_(Seq[Any](/*520.14*/if(user.get.id.equals(file.author.id))/*520.52*/{_display_(Seq[Any](format.raw/*520.53*/("""
													<div id="sectiondescrEditArea_"""),_display_(Seq[Any](/*521.45*/(s.id))),format.raw/*521.51*/("""" style="display:none;">
														<textarea name="sectiondescrText" class="sectionTextBox" id="sectiondescrText_"""),_display_(Seq[Any](/*522.94*/(s.id))),format.raw/*522.100*/("""" style="width:100%; height:100px;">"""),_display_(Seq[Any](/*522.137*/s/*522.138*/.description.getOrElse(""))),format.raw/*522.164*/("""</textarea>
													</div>
													<button class="btn btn-default btn-xs btn-margins" onclick="return openForEditSectionDescr('"""),_display_(Seq[Any](/*524.107*/s/*524.108*/.id)),format.raw/*524.111*/("""');" style="display:inline;" id="sectiondescrEdit_"""),_display_(Seq[Any](/*524.162*/(s.id))),format.raw/*524.168*/("""" title="Edit Description">
														<span class="glyphicon glyphicon-edit"></span> Edit</button>
													<button class="btn btn-primary" onclick="return postSectionDescr('"""),_display_(Seq[Any](/*526.81*/s/*526.82*/.id)),format.raw/*526.85*/("""', '"""),_display_(Seq[Any](/*526.90*/api/*526.93*/.routes.Sections.description(s.id).toString)),format.raw/*526.136*/("""');" style="display:none; margin-top:5px;" id="sectiondescrPost_"""),_display_(Seq[Any](/*526.201*/(s.id))),format.raw/*526.207*/("""" title="Submit">
														<span class="glyphicon glyphicon-ok"></span> Submit</button>
													<button class="btn btn-default" onclick="return cancelEditSectionDescr('"""),_display_(Seq[Any](/*528.87*/s/*528.88*/.id)),format.raw/*528.91*/("""');" style="display:none; margin-top:5px;" id="sectiondescrCancel_"""),_display_(Seq[Any](/*528.158*/(s.id))),format.raw/*528.164*/("""" title="Cancel">
														<span class="glyphicon glyphicon-remove"></span> Cancel</button>

													<script>
															window["previousSectiondescr_"""),_display_(Seq[Any](/*532.46*/(s.id))),format.raw/*532.52*/(""""] = '"""),_display_(Seq[Any](/*532.59*/s/*532.60*/.description.getOrElse(""))),format.raw/*532.86*/("""';
													</script>
												""")))})),format.raw/*534.14*/("""
											""")))})),format.raw/*535.13*/("""
										</td>
										<td class="galleriaFindSimilar"><a href=""""),_display_(Seq[Any](/*537.53*/routes/*537.59*/.Search.callSearchMultimediaIndexView(s.id))),format.raw/*537.102*/("""">Similar</a></td>
									</tr>
								""")))})),format.raw/*539.10*/("""
								</tbody>
								<script>
										function fixGalleriaTable()"""),format.raw/*542.38*/("""{"""),format.raw/*542.39*/("""
											galleriaScenesVisible = $("#scrollTableGalleria tbody tr").length;
											if(galleriaScenesVisible > 5)
												galleriaScenesVisible = 5;
											$("#scrollTableGalleria tbody").css("height", (parseInt($("#scrollTableGalleria tbody tr").css("height").replace("px",""))*galleriaScenesVisible)+"px");
										"""),format.raw/*547.11*/("""}"""),format.raw/*547.12*/("""
										fixGalleriaTable();
										$(window).resize(function()"""),format.raw/*549.38*/("""{"""),format.raw/*549.39*/("""
											fixGalleriaTable();
										"""),format.raw/*551.11*/("""}"""),format.raw/*551.12*/(""");
								</script>
							</table>
						</div>
					</div>


					<script>
							document.addEventListener("DOMContentLoaded", function () """),format.raw/*559.66*/("""{"""),format.raw/*559.67*/("""
								if ($("#ourvideo").length>0) """),format.raw/*560.38*/("""{"""),format.raw/*560.39*/("""
									var pop = Popcorn("#ourvideo");

									"""),_display_(Seq[Any](/*563.11*/sections/*563.19*/.map/*563.23*/ { s =>_display_(Seq[Any](format.raw/*563.30*/("""
									"""),_display_(Seq[Any](/*564.11*/if(s.startTime.isDefined)/*564.36*/ {_display_(Seq[Any](format.raw/*564.38*/("""
									pop.code("""),format.raw/*565.19*/("""{"""),format.raw/*565.20*/("""
										start: """),_display_(Seq[Any](/*566.19*/s/*566.20*/.startTime)),format.raw/*566.30*/(""",
										onStart: function() """),format.raw/*567.31*/("""{"""),format.raw/*567.32*/("""
											var frame = """),_display_(Seq[Any](/*568.25*/s/*568.26*/.order)),format.raw/*568.32*/(""" - 1;
											console.log("Jumping to " + frame + " " + """),_display_(Seq[Any](/*569.55*/s/*569.56*/.startTime)),format.raw/*569.66*/(""");
											$('#galleria').data('galleria').show(frame);
										"""),format.raw/*571.11*/("""}"""),format.raw/*571.12*/(""",
										onEnd: function() """),format.raw/*572.29*/("""{"""),format.raw/*572.30*/("""
											//console.log("end");
										"""),format.raw/*574.11*/("""}"""),format.raw/*574.12*/("""
									"""),format.raw/*575.10*/("""}"""),format.raw/*575.11*/(""");
									""")))})),format.raw/*576.11*/("""
									""")))})),format.raw/*577.11*/("""
								"""),format.raw/*578.9*/("""}"""),format.raw/*578.10*/("""
							"""),format.raw/*579.8*/("""}"""),format.raw/*579.9*/(""");
					</script>
					"""),_display_(Seq[Any](/*581.7*/defining(play.api.Play.configuration.getBoolean("navOnSectionClick").getOrElse(true))/*581.92*/{navOnSectionClick=>_display_(Seq[Any](format.raw/*581.112*/("""
						<script>
								$(function() """),format.raw/*583.22*/("""{"""),format.raw/*583.23*/("""
									// Load the classic theme
									Galleria.loadTheme('"""),_display_(Seq[Any](/*585.31*/routes/*585.37*/.Assets.at("javascripts/galleria-classic/galleria.classic.min.js"))),format.raw/*585.103*/("""');

									galleriaData = [];
									"""),_display_(Seq[Any](/*588.11*/sections/*588.19*/.map/*588.23*/ { s =>_display_(Seq[Any](format.raw/*588.30*/("""
									"""),_display_(Seq[Any](/*589.11*/if(s.preview.isDefined)/*589.34*/ {_display_(Seq[Any](format.raw/*589.36*/("""
									galleriaItem = """),format.raw/*590.25*/("""{"""),format.raw/*590.26*/("""}"""),format.raw/*590.27*/(""";
									galleriaItem["image"] = """"),_display_(Seq[Any](/*591.36*/api/*591.39*/.routes.Previews.download(s.preview.get.id))),format.raw/*591.82*/("""";
									"""),_display_(Seq[Any](/*592.11*/if(s.startTime.isDefined)/*592.36*/ {_display_(Seq[Any](format.raw/*592.38*/("""
									galleriaItem["title"] = "Shot";
									galleriaItem["description"] = "Starts at """),_display_(Seq[Any](/*594.52*/s/*594.53*/.startTime)),format.raw/*594.63*/(""" seconds.";
									""")))}/*595.12*/else/*595.17*/{_display_(Seq[Any](format.raw/*595.18*/("""
									"""),_display_(Seq[Any](/*596.11*/if(s.area.isDefined)/*596.31*/ {_display_(Seq[Any](format.raw/*596.33*/("""
									galleriaItem["title"] = "Area";
									galleriaItem["description"] = """"),_display_(Seq[Any](/*598.42*/s/*598.43*/.area.toString)),format.raw/*598.57*/("""";
									""")))}/*599.12*/else/*599.17*/{_display_(Seq[Any](format.raw/*599.18*/("""
									galleriaItem["title"] = "Unknown";
									galleriaItem["description"] = "Unknown info";
									""")))})),format.raw/*602.11*/("""
									""")))})),format.raw/*603.11*/("""
									galleriaData.push(galleriaItem);
									""")))})),format.raw/*605.11*/("""
									""")))})),format.raw/*606.11*/("""

									"""),_display_(Seq[Any](/*608.11*/if(navOnSectionClick)/*608.32*/{_display_(Seq[Any](format.raw/*608.33*/("""
									Galleria.configure("""),format.raw/*609.29*/("""{"""),format.raw/*609.30*/("""
										showInfo: false
									"""),format.raw/*611.10*/("""}"""),format.raw/*611.11*/(""");
									// Initialize Galleria and, if for a video, onclick events for going to time of selected image when clicking main image
									Galleria.run('#galleria', """),format.raw/*613.36*/("""{"""),format.raw/*613.37*/("""
										extend: function(options) """),format.raw/*614.37*/("""{"""),format.raw/*614.38*/("""
											if ($("#ourvideo").length>0) """),format.raw/*615.41*/("""{"""),format.raw/*615.42*/("""
												Galleria.log(this); // the gallery instance
												Galleria.log(options); // the gallery options

												// listen to when an image is shown
												this.bind('image', function(e) """),format.raw/*620.44*/("""{"""),format.raw/*620.45*/("""

													Galleria.log(e); // the event object may contain custom objects, in this case the main image
													Galleria.log(e.imageTarget); // the current image

													// lets make galleria make the video jump to the time of the selected image when clicking it:
													$(e.imageTarget).click(this.proxy(function()"""),format.raw/*626.58*/("""{"""),format.raw/*626.59*/("""
														var pop = Popcorn("#ourvideo");
														pop.currentTime(parseInt(this._data[this._active].description.match(/\d+/)[0]));
													"""),format.raw/*629.14*/("""}"""),format.raw/*629.15*/("""));
												"""),format.raw/*630.13*/("""}"""),format.raw/*630.14*/(""");
											"""),format.raw/*631.12*/("""}"""),format.raw/*631.13*/("""
										"""),format.raw/*632.11*/("""}"""),format.raw/*632.12*/(""",
										dataSource: galleriaData
									"""),format.raw/*634.10*/("""}"""),format.raw/*634.11*/(""");
									""")))})),format.raw/*635.11*/("""
									"""),_display_(Seq[Any](/*636.11*/if(!navOnSectionClick)/*636.33*/{_display_(Seq[Any](format.raw/*636.34*/("""
									// Initialize Galleria
									Galleria.run('#galleria', """),format.raw/*638.36*/("""{"""),format.raw/*638.37*/("""dataSource: galleriaData"""),format.raw/*638.61*/("""}"""),format.raw/*638.62*/(""");
									""")))})),format.raw/*639.11*/("""

									var gallery = $('#galleria').data('galleria');

									Galleria.ready(function(options) """),format.raw/*643.43*/("""{"""),format.raw/*643.44*/("""
										// 'this' is the gallery instance
										// 'options' is the gallery options
										this.bind('image', function(e) """),format.raw/*646.42*/("""{"""),format.raw/*646.43*/("""
											Galleria.log('Now viewing ' + e.imageTarget.src);
										"""),format.raw/*648.11*/("""}"""),format.raw/*648.12*/(""");
										var gallery = this; // galleria is ready and the gallery is assigned
										this.addElement('fscr');
										this.appendChild('stage','fscr');
										var fscr = this.$('fscr')
												.click(function() """),format.raw/*653.31*/("""{"""),format.raw/*653.32*/("""
													gallery.toggleFullscreen();
												"""),format.raw/*655.13*/("""}"""),format.raw/*655.14*/(""");
									"""),format.raw/*656.10*/("""}"""),format.raw/*656.11*/(""");
								"""),format.raw/*657.9*/("""}"""),format.raw/*657.10*/(""");
						</script>
					""")))})),format.raw/*659.7*/("""
				""")))})),format.raw/*660.6*/("""
			""")))})),format.raw/*661.5*/("""

			<div class="row bottom-padding" id="commentsSection">
				<div class="col-md-12">
					<h4>Comments</h4>
					"""),_display_(Seq[Any](/*666.7*/if(Permission.checkPermission(Permission.AddComment, ResourceRef(ResourceRef.file, file.id)))/*666.100*/ {_display_(Seq[Any](format.raw/*666.102*/("""
						"""),_display_(Seq[Any](/*667.8*/commentform(file.id.toString, "file", file.filename))),format.raw/*667.60*/("""
					""")))})),format.raw/*668.7*/("""
					<ol class="list-unstyled" id="reply_"""),_display_(Seq[Any](/*669.43*/file/*669.47*/.id.toString)),format.raw/*669.59*/("""">
					"""),_display_(Seq[Any](/*670.7*/comment(comments))),format.raw/*670.24*/("""
					</ol>
				</div>
			</div>


            <ul class="nav nav-tabs">
              <li class="active"><a href="#">System Metadata</a></li>
              <li><a href="#">User Metadata</a></li>
              <li><a href="#">Comments</a></li>
            </ul>
            

			<div class="row">
				<div class="col-md-12">
					<h4>System Generated File Metadata</h4>
				</div>
			</div>
			"""),_display_(Seq[Any](/*688.5*/if(user.isDefined)/*688.23*/ {_display_(Seq[Any](format.raw/*688.25*/("""
				<div class="row">
					"""),format.raw/*694.9*/("""
				</div>
			""")))})),format.raw/*696.5*/("""
			<div class="row bottom-padding">
				<div class="col-md-12" id="metadata-content">
				"""),_display_(Seq[Any](/*699.6*/metadatald/*699.16*/.viewForFile(mds, true))),format.raw/*699.39*/("""
				</div>
			</div>


			<div class="row bottom-padding" id="extractionsRow">
				<div class="col-md-12">
					<h4>Extractions</h4>
					<a class="btn btn-link" data-toggle="collapse" href="#extractions" aria-expanded="false"
					aria-controls="listExtractions">
						<span class="glyphicon glyphicon-list" aria-hidden="true"></span> List extraction events
					</a>
					"""),_display_(Seq[Any](/*711.7*/if(Permission.checkPermission(Permission.EditFile, ResourceRef(ResourceRef.file, file.id))
							&& play.api.Play.current.plugin[services.RabbitmqPlugin].isDefined)/*712.75*/ {_display_(Seq[Any](format.raw/*712.77*/("""
						<a href=""""),_display_(Seq[Any](/*713.17*/routes/*713.23*/.Extractors.submitFileExtraction(UUID(id)))),format.raw/*713.65*/("""" class="btn btn-link" >
							<span class="glyphicon glyphicon-send"></span>
							Submit file for extraction
						</a>
					""")))})),format.raw/*717.7*/("""
					<div class="collapse" id="extractions">
					"""),_display_(Seq[Any](/*719.7*/extractions/*719.18*/.extractionsByFile(extractionsStatus))),format.raw/*719.55*/("""
					</div>
				</div>
			</div>



			<script language="javascript">
					function updateFileName() """),format.raw/*727.32*/("""{"""),format.raw/*727.33*/("""
						fileName = $('#file-name-title').text().trim();
						$('<div class="inline file_name_div h3"> </div>').insertAfter($('#prf-file-name'));
						$('.file_name_div').append('<input type="text" id="file_name_input" class="form-control" />');
						$('.file_name_div').append('<button class="btn btn-sm btn-primary edit-tab btn-margins" onclick="saveFileName()"> <span class="glyphicon glyphicon-send"></span> Save</button>');
						$('.file_name_div').append('<button class="btn btn-sm btn-link btn-margins" onclick="cancelFileName()"> <span class="glyphicon glyphicon-remove"></span> Cancel</button>');
						$('.file_name_div').append('<div class="hiddencomplete" id="file-name-error"> <span class="small error">File name is required.</span></div>');
						$('#file-name-div').removeClass("file-title-div");
						$('#file_name_input').val(fileName);
						$('#file-name-title').text("");
						$('#h-edit-file').css("display", "none");
					"""),format.raw/*738.6*/("""}"""),format.raw/*738.7*/("""

					function saveFileName() """),format.raw/*740.30*/("""{"""),format.raw/*740.31*/("""
						if($('#file_name_input').val().length < 1) """),format.raw/*741.50*/("""{"""),format.raw/*741.51*/("""
							$('#file-name-error').show();
							return false;
						"""),format.raw/*744.7*/("""}"""),format.raw/*744.8*/(""" else """),format.raw/*744.14*/("""{"""),format.raw/*744.15*/("""
							$('#file-name-error').hide();
						"""),format.raw/*746.7*/("""}"""),format.raw/*746.8*/("""
						var name = $('#file_name_input').val();
						jsonData = JSON.stringify("""),format.raw/*748.33*/("""{"""),format.raw/*748.34*/(""""name": htmlEncode(name)"""),format.raw/*748.58*/("""}"""),format.raw/*748.59*/(""");

						var request = jsRoutes.api.Files.updateFileName('"""),_display_(Seq[Any](/*750.57*/file/*750.61*/.id)),format.raw/*750.64*/("""').ajax("""),format.raw/*750.72*/("""{"""),format.raw/*750.73*/("""
							data: jsonData,
							type: 'PUT',
							contentType: "application/json"
						"""),format.raw/*754.7*/("""}"""),format.raw/*754.8*/(""");
						request.done(function(response, textStatus, jqXHR) """),format.raw/*755.58*/("""{"""),format.raw/*755.59*/("""
							$('.file_name_div').remove();
							$('#file-name-title').text(htmlEncode(name).replace(/\n/g, "<br />"));
							$('#file-name-title').prepend('<span class="glyphicon glyphicon-file"></span> ');
							$('#prf-file-name').mouseleave();
							$('#file-name-div').addClass("file-title-div");
							$('#h-edit-file').css("display", "");
						"""),format.raw/*762.7*/("""}"""),format.raw/*762.8*/(""");
						request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*763.61*/("""{"""),format.raw/*763.62*/("""
							console.error("The following error occurred: " + textStatus, errorThrown);
							var errMsg = "Yoy must be logged in to update the file name.";
							if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*766.50*/("""{"""),format.raw/*766.51*/("""
								notify("The file name was not updated due to: "+ errorThrown, "error");
							"""),format.raw/*768.8*/("""}"""),format.raw/*768.9*/("""
						"""),format.raw/*769.7*/("""}"""),format.raw/*769.8*/(""");
					"""),format.raw/*770.6*/("""}"""),format.raw/*770.7*/("""

					function cancelFileName() """),format.raw/*772.32*/("""{"""),format.raw/*772.33*/("""
						$('#file-name-title').text(fileName);
						$('#file-name-title').prepend('<span class="glyphicon glyphicon-file"></span> ');
						$('.file_name_div').remove();
						$('#prf-file-name').css("display", "inline");
						$('#h-edit-file').css("display", "");
						$('#file-name-div').addClass("file-title-div");
						$('#prf-file-name').mouseleave();
					"""),format.raw/*780.6*/("""}"""),format.raw/*780.7*/("""

					$(document).ready(function()"""),format.raw/*782.34*/("""{"""),format.raw/*782.35*/("""
						$(document).on('mouseenter', '#prf-file-name', function() """),format.raw/*783.65*/("""{"""),format.raw/*783.66*/("""
							$('#h-edit-file').removeClass("hiddencomplete");
							$('#h-edit-file').addClass("inline");
						"""),format.raw/*786.7*/("""}"""),format.raw/*786.8*/(""").on('mouseleave', '#prf-file-name', function() """),format.raw/*786.56*/("""{"""),format.raw/*786.57*/("""
							$('#h-edit-file').removeClass("inline");
							$('#h-edit-file').addClass("hiddencomplete");
						"""),format.raw/*789.7*/("""}"""),format.raw/*789.8*/(""");
					"""),format.raw/*790.6*/("""}"""),format.raw/*790.7*/(""");
			</script>
		</div>

			<!-- right column -->
		<div class="col-md-4 col-sm-4 col-lg-4">
			<div class="row ds-section-sm file-info">
				<table class="table table-condensed" id="table-content">
					<tr><td><b>Type:</b></td><td>"""),_display_(Seq[Any](/*798.36*/file/*798.40*/.contentType)),format.raw/*798.52*/("""</td></tr>
					<tr><td><b>File&nbsp;size:</b></td><td title=""""),_display_(Seq[Any](/*799.53*/file/*799.57*/.length)),format.raw/*799.64*/(""" bytes">"""),_display_(Seq[Any](/*799.73*/humanReadableByteCount(file.length))),format.raw/*799.108*/("""</td></tr>
					"""),_display_(Seq[Any](/*800.7*/if(Permission.checkServerAdmin(user))/*800.44*/ {_display_(Seq[Any](format.raw/*800.46*/("""
						<tr><td><b>File&nbsp;location:</b></td>
							"""),_display_(Seq[Any](/*802.9*/if(file.loader == "services.mongodb.MongoDBByteStorage")/*802.65*/ {_display_(Seq[Any](format.raw/*802.67*/("""
								<td>mongo</td>
							""")))})),format.raw/*804.9*/("""
							"""),_display_(Seq[Any](/*805.9*/if(file.loader == "services.filesystem.DiskByteStorageService")/*805.72*/ {_display_(Seq[Any](format.raw/*805.74*/("""
								"""),_display_(Seq[Any](/*806.10*/if(file.loader_id.isEmpty)/*806.36*/ {_display_(Seq[Any](format.raw/*806.38*/("""
									<td>missing</td>
								""")))}/*808.11*/else/*808.16*/{_display_(Seq[Any](format.raw/*808.17*/("""
									<td>
										"""),_display_(Seq[Any](/*810.12*/if(file.loader_id.length>10)/*810.40*/ {_display_(Seq[Any](format.raw/*810.42*/("""
											<span title=""""),_display_(Seq[Any](/*811.26*/file/*811.30*/.loader_id)),format.raw/*811.40*/("""">..."""),_display_(Seq[Any](/*811.46*/file/*811.50*/.loader_id.substring(file.loader_id.length-10, file.loader_id.length))),format.raw/*811.119*/("""</span>
										""")))}/*812.13*/else/*812.18*/{_display_(Seq[Any](format.raw/*812.19*/("""
											"""),_display_(Seq[Any](/*813.13*/file/*813.17*/.loader_id)),format.raw/*813.27*/("""
										""")))})),format.raw/*814.12*/("""
										<a class="btn btn-link btn-sm clipboard" data-copy=""""),_display_(Seq[Any](/*815.64*/file/*815.68*/.loader_id)),format.raw/*815.78*/("""" title="Copy path to clipboard"><span class="glyphicon glyphicon-edit"></span> copy</a></td>
								""")))})),format.raw/*816.10*/("""
							""")))})),format.raw/*817.9*/("""
						</tr>
					""")))})),format.raw/*819.7*/("""
					<tr><td><b>Uploaded&nbsp;on:</b></td><td> """),_display_(Seq[Any](/*820.49*/file/*820.53*/.uploadDate.format("MMM dd, yyyy HH:mm:ss"))),format.raw/*820.96*/("""</td></tr>
					"""),_display_(Seq[Any](/*821.7*/if(file.originalname.length > 0)/*821.39*/ {_display_(Seq[Any](format.raw/*821.41*/("""
						<tr><td><b>Uploaded&nbsp;as:</b></td><td> """),_display_(Seq[Any](/*822.50*/file/*822.54*/.originalname)),format.raw/*822.67*/("""</td></tr>
					""")))})),format.raw/*823.7*/("""
					<tr><td><b>Uploaded&nbsp;by:</b></td><td> <a href = """"),_display_(Seq[Any](/*824.60*/routes/*824.66*/.Profile.viewProfileUUID(file.author.id))),format.raw/*824.106*/(""""> """),_display_(Seq[Any](/*824.110*/file/*824.114*/.author.fullName)),format.raw/*824.130*/("""</a></td></tr>
					"""),_display_(Seq[Any](/*825.7*/if( play.Play.application().configuration().getBoolean("enablePublic"))/*825.78*/ {_display_(Seq[Any](format.raw/*825.80*/("""
						<tr><td><b>Access:</b></td><td> """),_display_(Seq[Any](/*826.40*/access)),format.raw/*826.46*/("""</td></tr>
					""")))})),format.raw/*827.7*/("""
					<tr><td><b>Status:</b></td><td> """),_display_(Seq[Any](/*828.39*/file/*828.43*/.status)),format.raw/*828.50*/("""</td></tr>

				</table>
			</div>

				<!-- License elements-->
			<div class="row ds-section-sm license-sec">
				<div class="accordion" id="accordion6">
					<div class="accordion-group">
						<div class="accordian-heading">
							<h4>License</h4>

							<table>
								<tr>
									<td><b>Type: </b></td>
									<td id="licensetextdata" style="padding-left:10px;">blank</td>
								</tr>
								<tr>
									<td><b>Holder: </b></td>
									<td id="rightsholderdata" style="padding-left:10px;">blank</td>
								</tr>
							</table>
							"""),_display_(Seq[Any](/*850.9*/if(Permission.checkPermission(Permission.EditLicense, ResourceRef(ResourceRef.file, file.id)))/*850.103*/ {_display_(Seq[Any](format.raw/*850.105*/("""
								<a class="btn btn-link btn-sm accordion-toggle collapsed" data-toggle="collapse"
								data-parent="#accordion6" href="#collapseSix" id="editlicense"
								title="Edit License Information">
									<span class="glyphicon glyphicon-edit"></span> Edit
								</a>
							""")))})),format.raw/*856.9*/("""
						</div>
						<div id="collapseSix" class="accordion-body collapse">
							<div class="panel panel-primary">
								<div class="panel-body">
								"""),_display_(Seq[Any](/*861.10*/if(user.isDefined)/*861.28*/ {_display_(Seq[Any](format.raw/*861.30*/("""
									"""),_display_(Seq[Any](/*862.11*/licenseform(file.id.toString, file.licenseData, "file", file.author.fullName))),format.raw/*862.88*/("""
								""")))})),format.raw/*863.10*/("""
								</div>
							</div>
						</div>
					</div>
				</div>
				<script src=""""),_display_(Seq[Any](/*869.19*/routes/*869.25*/.Assets.at("javascripts/recommendation.js"))),format.raw/*869.68*/("""" type="text/javascript"></script>
				<script type="text/javascript" language="javascript">
						$(document).ready(function() """),format.raw/*871.36*/("""{"""),format.raw/*871.37*/("""
							//Will have to modify the if check to see if there is data that specifies what should be selected
							//Incoming data may specifiy the type of license, the name of the owner of the rights, the text
							//describing the license rights, the URL for the license, and whether or not downloading is
							//allowed.
							var fileLicenseType = """"),_display_(Seq[Any](/*876.32*/file/*876.36*/.licenseData.m_licenseType)),format.raw/*876.62*/("""";
							var fileRightsHolder = """"),_display_(Seq[Any](/*877.33*/file/*877.37*/.licenseData.m_rightsHolder)),format.raw/*877.64*/("""";
							var fileLicenseText = """"),_display_(Seq[Any](/*878.32*/file/*878.36*/.licenseData.m_licenseText)),format.raw/*878.62*/("""";
							var fileLicenseUrl = """"),_display_(Seq[Any](/*879.31*/file/*879.35*/.licenseData.m_licenseUrl)),format.raw/*879.60*/("""";
							var fileAllowDownload = """"),_display_(Seq[Any](/*880.34*/file/*880.38*/.licenseData.isDownloadAllowed(user))),format.raw/*880.74*/("""";
							var fileImageBase = '"""),_display_(Seq[Any](/*881.30*/routes/*881.36*/.Assets.at("images"))),format.raw/*881.56*/("""';
							var fileAuthorName = '"""),_display_(Seq[Any](/*882.31*/file/*882.35*/.author.fullName)),format.raw/*882.51*/("""';

							if (!"""),_display_(Seq[Any](/*884.14*/user/*884.18*/.isDefined)),format.raw/*884.28*/(""")"""),format.raw/*884.29*/("""{"""),format.raw/*884.30*/("""
								updateInterface(fileLicenseType, fileRightsHolder, fileLicenseText, fileLicenseUrl, fileAllowDownload, fileImageBase, fileAuthorName);
							"""),format.raw/*886.8*/("""}"""),format.raw/*886.9*/("""

							$(document).on('click', '.followButton', function() """),format.raw/*888.60*/("""{"""),format.raw/*888.61*/("""
								var id = $(this).attr('objectId');
								var name = $(this).attr('objectName');
								var type = $(this).attr('objectType');
								if ($(this).attr('id') === '') """),format.raw/*892.40*/("""{"""),format.raw/*892.41*/("""
									followHandler.call(this, jsRoutes, id, name, type, undefined, undefined);
								"""),format.raw/*894.9*/("""}"""),format.raw/*894.10*/(""" else """),format.raw/*894.16*/("""{"""),format.raw/*894.17*/("""
									followHandler.call(this, jsRoutes, id, name, type, function(data) """),format.raw/*895.76*/("""{"""),format.raw/*895.77*/("""
										recommendationHandler(jsRoutes, $('#recommendPanel'), $('#recommendDiv'),
												data['recommendations']);
									"""),format.raw/*898.10*/("""}"""),format.raw/*898.11*/(""", undefined);
								"""),format.raw/*899.9*/("""}"""),format.raw/*899.10*/("""
							"""),format.raw/*900.8*/("""}"""),format.raw/*900.9*/(""");
							$(document).on('mouseenter', '#aboutdesc', function() """),format.raw/*901.62*/("""{"""),format.raw/*901.63*/("""
								$('#edit-description').removeClass("hiddencomplete");
								$('#edit-description').addClass("inline");
							"""),format.raw/*904.8*/("""}"""),format.raw/*904.9*/(""").on('mouseleave', '#aboutdesc', function() """),format.raw/*904.53*/("""{"""),format.raw/*904.54*/("""
								$('#edit-description').removeClass("inline");
								$('#edit-description').addClass("hiddencomplete");
							"""),format.raw/*907.8*/("""}"""),format.raw/*907.9*/(""");
						"""),format.raw/*908.7*/("""}"""),format.raw/*908.8*/(""");

						function downloadFile() """),format.raw/*910.31*/("""{"""),format.raw/*910.32*/("""
							var downloadRoute = """"),_display_(Seq[Any](/*911.30*/routes/*911.36*/.Files.download(UUID(id)))),format.raw/*911.61*/("""";
							window.open(downloadRoute, '_blank');
						"""),format.raw/*913.7*/("""}"""),format.raw/*913.8*/("""

						function downloadFileAs(downloadRoute) """),format.raw/*915.46*/("""{"""),format.raw/*915.47*/("""
							console.log("downloadFileAs - route = " + downloadRoute);
							window.open(downloadRoute, '_blank');
						"""),format.raw/*918.7*/("""}"""),format.raw/*918.8*/("""

				</script>
			</div>
				<!-- End License elements -->

				<!-- Move the file to a dataset -->
			<div class="row ds-section-sm border-top" id="moveToDatasetSec">
				"""),_display_(Seq[Any](/*926.6*/if(datasets.length > 0)/*926.29*/ {_display_(Seq[Any](format.raw/*926.31*/("""
					<h4>"""),_display_(Seq[Any](/*927.11*/Messages("a.contains.b", Messages("dataset.title"), "file"))),format.raw/*927.70*/("""</h4>
						<!-- A file cannot be part of more datasets as of June 2016. The code, however supports datasets sharing  -->
					"""),_display_(Seq[Any](/*929.7*/datasets/*929.15*/.map/*929.19*/ { dataset =>_display_(Seq[Any](format.raw/*929.32*/("""
						<div id="dat_"""),_display_(Seq[Any](/*930.21*/dataset/*930.28*/.id)),format.raw/*930.31*/("""" class="row bottom-padding">
							<div class="col-md-2 col-sm-2 col-lg-2">
							"""),_display_(Seq[Any](/*932.9*/if(!dataset.thumbnail_id.isEmpty)/*932.42*/{_display_(Seq[Any](format.raw/*932.43*/("""
								<a href=""""),_display_(Seq[Any](/*933.19*/(routes.Datasets.dataset(dataset.id)))),format.raw/*933.56*/("""">
									<img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*934.57*/(routes.Files.thumbnail(UUID(dataset.thumbnail_id.get))))),format.raw/*934.113*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*934.134*/Html(dataset.name))),format.raw/*934.152*/("""">
								</a>
							""")))})),format.raw/*936.9*/("""
							</div>
							<div class="col-md-10 col-sm-10 col-lg-10">
								<div>
									<a href=""""),_display_(Seq[Any](/*940.20*/(routes.Datasets.dataset(dataset.id)))),format.raw/*940.57*/("""" id='"""),_display_(Seq[Any](/*940.64*/dataset/*940.71*/.id)),format.raw/*940.74*/("""' class ="dataset">"""),_display_(Seq[Any](/*940.94*/Html(dataset.name))),format.raw/*940.112*/("""</a>
								</div>
							</div>
						</div>
					"""),_display_(Seq[Any](/*944.7*/if(Permission.checkPermission(Permission.EditFile, ResourceRef(ResourceRef.file, file.id)))/*944.98*/ {_display_(Seq[Any](format.raw/*944.100*/("""
						<div class="form-inline">
							<div class="input-group input-group-sm col-md-8">
								<select id="datasetAddSelect" class="form-control add-resource"></select>
								<span class="input-group-btn">
									<a href="#" class="btn btn-default btn-large" id="moveFileBtn" title="Move file to a dataset" onclick="moveFromDatasetToDataset(null,'"""),_display_(Seq[Any](/*949.145*/dataset/*949.152*/.id)),format.raw/*949.155*/("""','"""),_display_(Seq[Any](/*949.159*/file/*949.163*/.id)),format.raw/*949.166*/("""')">
										<span class="glyphicon glyphicon-move"></span> Move
									</a>
								</span>
							</div>
						</div>
					""")))})),format.raw/*955.7*/("""
					""")))})),format.raw/*956.7*/("""
				""")))})),format.raw/*957.6*/("""
				"""),_display_(Seq[Any](/*958.6*/if(folders.length > 0)/*958.28*/ {_display_(Seq[Any](format.raw/*958.30*/("""
					<h4>Folder containing the file</h4>
					"""),_display_(Seq[Any](/*960.7*/folders/*960.14*/.map/*960.18*/ { folder =>_display_(Seq[Any](format.raw/*960.30*/("""
						<div id="dat_"""),_display_(Seq[Any](/*961.21*/folder/*961.27*/.parentDatasetId)),format.raw/*961.43*/("""" class="row bottom-padding">
							<div class="col-md-2 col-sm-2 col-lg-2">
							</div>
							<div class="col-md-10 col-sm-10 col-lg-10">
								<div>
									<a href=""""),_display_(Seq[Any](/*966.20*/routes/*966.26*/.Datasets.dataset(folder.parentDatasetId))),format.raw/*966.67*/("""#folderId="""),_display_(Seq[Any](/*966.78*/folder/*966.84*/.id)),format.raw/*966.87*/("""" id='"""),_display_(Seq[Any](/*966.94*/folder/*966.100*/.parentDatasetId)),format.raw/*966.116*/("""' class ="dataset">"""),_display_(Seq[Any](/*966.136*/Html(folder.displayName))),format.raw/*966.160*/("""</a>
								</div>
							</div>
						</div>
					"""),_display_(Seq[Any](/*970.7*/if(Permission.checkPermission(Permission.EditFile, ResourceRef(ResourceRef.file, file.id)))/*970.98*/ {_display_(Seq[Any](format.raw/*970.100*/("""
						<div class="form-inline">
							<div class="input-group input-group-sm col-md-8">
								<select id="datasetAddSelect" class="form-control add-resource"></select>
								<span class="input-group-btn">
									<a href="#" class="btn btn-success btn-large" id="moveFileBtn" title="Move file from folder to a dataset" onclick="moveFromDatasetToDataset('"""),_display_(Seq[Any](/*975.152*/folder/*975.158*/.id)),format.raw/*975.161*/("""','"""),_display_(Seq[Any](/*975.165*/folder/*975.171*/.parentDatasetId)),format.raw/*975.187*/("""','"""),_display_(Seq[Any](/*975.191*/file/*975.195*/.id)),format.raw/*975.198*/("""')">
										<span class="glyphicon glyphicon-move"></span> Move to """),_display_(Seq[Any](/*976.67*/Messages("dataset.title"))),format.raw/*976.92*/("""
									</a>
								</span>
							</div>
						</div>
					""")))})),format.raw/*981.7*/("""
					""")))})),format.raw/*982.7*/("""
				""")))})),format.raw/*983.6*/("""
				<script language="javascript">
						$("#datasetAddSelect").select2("""),format.raw/*985.38*/("""{"""),format.raw/*985.39*/("""
							theme: "bootstrap",
							placeholder: "Select a """),_display_(Seq[Any](/*987.32*/Messages("dataset.title"))),format.raw/*987.57*/("""",
							allowClear: true,
							ajax: """),format.raw/*989.14*/("""{"""),format.raw/*989.15*/("""
								url: function(params) """),format.raw/*990.31*/("""{"""),format.raw/*990.32*/("""
									return jsRoutes.api.Datasets.listMoveFileToDataset(""""),_display_(Seq[Any](/*991.63*/file/*991.67*/.id)),format.raw/*991.70*/("""", null, 15).url;
								"""),format.raw/*992.9*/("""}"""),format.raw/*992.10*/(""",
								data: function(params) """),format.raw/*993.32*/("""{"""),format.raw/*993.33*/("""
									return """),format.raw/*994.17*/("""{"""),format.raw/*994.18*/(""" title: params.term """),format.raw/*994.38*/("""}"""),format.raw/*994.39*/(""";
								"""),format.raw/*995.9*/("""}"""),format.raw/*995.10*/(""",
								processResults: function(data, page) """),format.raw/*996.46*/("""{"""),format.raw/*996.47*/("""
									return """),format.raw/*997.17*/("""{"""),format.raw/*997.18*/("""results: data.filter(function(x) """),format.raw/*997.51*/("""{"""),format.raw/*997.52*/("""
											var ids = $('.dataset').map(function() """),format.raw/*998.51*/("""{"""),format.raw/*998.52*/("""
												return $(this).attr('id');
											"""),format.raw/*1000.12*/("""}"""),format.raw/*1000.13*/(""");
											return $.inArray(x.id, ids) == -1;
										"""),format.raw/*1002.11*/("""}"""),format.raw/*1002.12*/(""").map(function(x) """),format.raw/*1002.30*/("""{"""),format.raw/*1002.31*/("""
											return """),format.raw/*1003.19*/("""{"""),format.raw/*1003.20*/("""
												text: x.name,
												id: x.id
											"""),format.raw/*1006.12*/("""}"""),format.raw/*1006.13*/("""
										"""),format.raw/*1007.11*/("""}"""),format.raw/*1007.12*/(""")"""),format.raw/*1007.13*/("""}"""),format.raw/*1007.14*/(""";
								"""),format.raw/*1008.9*/("""}"""),format.raw/*1008.10*/("""
							"""),format.raw/*1009.8*/("""}"""),format.raw/*1009.9*/("""
						"""),format.raw/*1010.7*/("""}"""),format.raw/*1010.8*/(""")
				</script>
			</div>

			<div class="row ds-section-sm border-top">
				<h4>Tags</h4>
				<ul id="tagList">
						<!-- Do the tags need to have [File] and [Section] appeneded to them? -->
					"""),_display_(Seq[Any](/*1018.7*/file/*1018.11*/.tags.map/*1018.20*/ { tag =>_display_(Seq[Any](format.raw/*1018.29*/("""
						<li class="tag"><a href=""""),_display_(Seq[Any](/*1019.33*/routes/*1019.39*/.Tags.search(tag.name))),format.raw/*1019.61*/("""" id=""""),_display_(Seq[Any](/*1019.68*/tag/*1019.71*/.name)),format.raw/*1019.76*/("""" data-id=""""),_display_(Seq[Any](/*1019.88*/tag/*1019.91*/.id)),format.raw/*1019.94*/("""">"""),_display_(Seq[Any](/*1019.97*/Html(tag.name))),format.raw/*1019.111*/(""" [File]</a>
							<a href='javascript:void(0);'>
								<span style="display:none;" id='"""),_display_(Seq[Any](/*1021.42*/tag/*1021.45*/.name)),format.raw/*1021.50*/("""' data-id='"""),_display_(Seq[Any](/*1021.62*/tag/*1021.65*/.id)),format.raw/*1021.68*/("""' class='glyphicon glyphicon-remove'></span></a></li>
					""")))})),format.raw/*1022.7*/("""
					"""),_display_(Seq[Any](/*1023.7*/file/*1023.11*/.sections.map/*1023.24*/ { section =>_display_(Seq[Any](format.raw/*1023.37*/("""
						"""),_display_(Seq[Any](/*1024.8*/section/*1024.15*/.tags.map/*1024.24*/ { tag =>_display_(Seq[Any](format.raw/*1024.33*/("""
							<li><a href=""""),_display_(Seq[Any](/*1025.22*/routes/*1025.28*/.Tags.search(tag.name))),format.raw/*1025.50*/("""">"""),_display_(Seq[Any](/*1025.53*/Html(tag.name))),format.raw/*1025.67*/(""" [Section]</a></li>
						""")))})),format.raw/*1026.8*/("""
					""")))})),format.raw/*1027.7*/("""
				</ul>
				"""),_display_(Seq[Any](/*1029.6*/if(Permission.checkPermission(Permission.AddTag, ResourceRef(ResourceRef.file, file.id)))/*1029.95*/ {_display_(Seq[Any](format.raw/*1029.97*/("""
					<form class="form-inline">
						<div class="input-group input-group-sm col-md-8">
							<input type="text" maxlength=""""),_display_(Seq[Any](/*1032.39*/play/*1032.43*/.api.Play.configuration.getInt("clowder.tagLength").getOrElse(100))),format.raw/*1032.109*/("""" id="tagField" class="form-control">
							<span class="input-group-btn">
								<button class="btn btn-success btn-large" id="tagB" title="Add Tag">
									<span class="glyphicon glyphicon-tag"></span>
								</button>
							</span>
						</div>
					</form>
				""")))}/*1040.7*/else/*1040.12*/{_display_(Seq[Any](format.raw/*1040.13*/("""
					<form class="form-inline">
						<div class="input-group input-group-sm col-md-8">
							<input disabled type="text" maxlength=""""),_display_(Seq[Any](/*1043.48*/play/*1043.52*/.api.Play.configuration.getInt("clowder.tagLength").getOrElse(100))),format.raw/*1043.118*/("""" id="tagField" class="form-control">
							<span class="input-group-btn">
								<button class="btn btn-success btn-large" id="tagB" title="Add Tag">
									<span class="glyphicon glyphicon-tag"></span>
								</button>
							</span>
						</div>
					</form>
				""")))})),format.raw/*1051.6*/("""
				<script language="javascript">
						window["userDefined"] = false;
				</script>
				"""),_display_(Seq[Any](/*1055.6*/if(user.isDefined)/*1055.24*/ {_display_(Seq[Any](format.raw/*1055.26*/("""
					<script language="javascript">
							window["userDefined"] = true;
					</script>
				""")))})),format.raw/*1059.6*/("""
				<script language="javascript">			
						//The removeTag code is almost identical to the code in tags.scala.html. Should be unified.
						function removeTag() """),format.raw/*1062.28*/("""{"""),format.raw/*1062.29*/("""
							var tagId = $(this).attr("id");
							console.log("Removing tag " + tagId);
							//The data-id attribute is needed since a) the removeTag for some reason is based off the tag name (text), which is what the id attribute is
							//and b) identifiying elements by the tag text can sometimes break, especially in the special cases where there are characters that need encoding.
							//
							//The real question is since that tags have UUIDs, why aren't they simply removed in that manner. In the add, the dom element isn't added until success
							//so the ID could be returned there and added into the elements as needed.
							var tagDataId = $(this).attr("data-id");
							console.log("data-id to remove is " + tagDataId);

							var request = jsRoutes.api.Files.removeTags('"""),_display_(Seq[Any](/*1073.54*/file/*1073.58*/.id)),format.raw/*1073.61*/("""').ajax("""),format.raw/*1073.69*/("""{"""),format.raw/*1073.70*/("""
								data: JSON.stringify("""),format.raw/*1074.30*/("""{"""),format.raw/*1074.31*/(""""tags":[tagId]"""),format.raw/*1074.45*/("""}"""),format.raw/*1074.46*/("""),
								type: 'POST',
								contentType: "application/json"
							"""),format.raw/*1077.8*/("""}"""),format.raw/*1077.9*/(""");

							request.done(function (response, textStatus, jqXHR) """),format.raw/*1079.60*/("""{"""),format.raw/*1079.61*/("""
								console.log("Response " + textStatus);
								$("[data-id=" + tagDataId + "]").parents("li").remove();
							"""),format.raw/*1082.8*/("""}"""),format.raw/*1082.9*/(""");

							request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*1084.62*/("""{"""),format.raw/*1084.63*/("""
								console.error("The following error occured: " + textStatus, errorThrown);
								var errMsg = "You must be logged in to remove a tag from a file.";
								if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*1087.52*/("""{"""),format.raw/*1087.53*/("""
									notify("The tag was not removed from the file due to : " + errorThrown, "error");
								"""),format.raw/*1089.9*/("""}"""),format.raw/*1089.10*/("""
							"""),format.raw/*1090.8*/("""}"""),format.raw/*1090.9*/(""");

							return false;
						"""),format.raw/*1093.7*/("""}"""),format.raw/*1093.8*/("""

				function removeIconMouseIn() """),format.raw/*1095.34*/("""{"""),format.raw/*1095.35*/("""
					$(this).find(".glyphicon-remove").fadeIn(100);
				"""),format.raw/*1097.5*/("""}"""),format.raw/*1097.6*/("""

				function removeIconMouseOut() """),format.raw/*1099.35*/("""{"""),format.raw/*1099.36*/("""
					$(this).find(".glyphicon-remove").fadeOut(100);
				"""),format.raw/*1101.5*/("""}"""),format.raw/*1101.6*/("""

				$(function() """),format.raw/*1103.18*/("""{"""),format.raw/*1103.19*/("""
					var tmpId = 1;
					if(window["userDefined"] == true)
						$("#tagList").children("li").each(function()"""),format.raw/*1106.51*/("""{"""),format.raw/*1106.52*/("""
							$(this).hover(
									removeIconMouseIn,removeIconMouseOut
							);
						"""),format.raw/*1110.7*/("""}"""),format.raw/*1110.8*/(""");
					$("#tagList").find(".glyphicon-remove").click(removeTag);

					$('#tagB').click(function() """),format.raw/*1113.34*/("""{"""),format.raw/*1113.35*/("""
						var tag = $('#tagField').val();
						tag = htmlEncode(tag);

						var isTagPresent = false;
						$("#tagList").children("li").each(function (index, tagLi) """),format.raw/*1118.65*/("""{"""),format.raw/*1118.66*/("""
							if($(tagLi).attr("name")===tag) """),format.raw/*1119.40*/("""{"""),format.raw/*1119.41*/("""
								isTagPresent = true;
							"""),format.raw/*1121.8*/("""}"""),format.raw/*1121.9*/("""
						"""),format.raw/*1122.7*/("""}"""),format.raw/*1122.8*/(""");

						if (tag !== "" && isTagPresent != true) """),format.raw/*1124.47*/("""{"""),format.raw/*1124.48*/("""
							console.log("submitting tag " + tag);
							var request = jsRoutes.api.Files.addTags('"""),_display_(Seq[Any](/*1126.51*/file/*1126.55*/.id)),format.raw/*1126.58*/("""').ajax("""),format.raw/*1126.66*/("""{"""),format.raw/*1126.67*/("""
								data: JSON.stringify("""),format.raw/*1127.30*/("""{"""),format.raw/*1127.31*/(""""tags":[tag]"""),format.raw/*1127.43*/("""}"""),format.raw/*1127.44*/("""),
								type: 'POST',
								contentType: "application/json",
							"""),format.raw/*1130.8*/("""}"""),format.raw/*1130.9*/(""");

							request.done(function (response, textStatus, jqXHR)"""),format.raw/*1132.59*/("""{"""),format.raw/*1132.60*/("""
								console.log("Response " + response);
								var url = jsRoutes.controllers.Tags.search(tag).url;
								$newTag = $("<li class='tag'><a href='" + url + "'id='" + htmlEncode(tag) +"' data-id='newId" + tmpId + "'>" + tag + " [File]</a><a href='javascript:void(0);'><i style='display:none;'id='" + htmlEncode(tag) +"' data-id='newId" + tmpId + "' class='glyphicon glyphicon-remove'></i></a></li>").appendTo('#tagList');
								$newTag.hover(removeIconMouseIn,removeIconMouseOut);
								$newTag.find(".glyphicon-remove").click(removeTag);
								$('#tagField').val("");
								tmpId = tmpId+1;
							"""),format.raw/*1140.8*/("""}"""),format.raw/*1140.9*/(""");

							request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*1142.62*/("""{"""),format.raw/*1142.63*/("""
								console.error("The following error occured: " + textStatus, errorThrown);
								var errMsg = "You must be logged in to add a tag to a file.";
								if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*1145.52*/("""{"""),format.raw/*1145.53*/("""
									notify("The tag was not added to the file due to : " + errorThrown, "error");
								"""),format.raw/*1147.9*/("""}"""),format.raw/*1147.10*/("""
							"""),format.raw/*1148.8*/("""}"""),format.raw/*1148.9*/(""");
							return false;
						"""),format.raw/*1150.7*/("""}"""),format.raw/*1150.8*/("""
					"""),format.raw/*1151.6*/("""}"""),format.raw/*1151.7*/(""");

					$('#tagField').keypress(function (e) """),format.raw/*1153.43*/("""{"""),format.raw/*1153.44*/("""
						if (e.which == 13) """),format.raw/*1154.26*/("""{"""),format.raw/*1154.27*/("""
							console.log("enter");
							$('#tagB').click();
							return false;
						"""),format.raw/*1158.7*/("""}"""),format.raw/*1158.8*/("""
					"""),format.raw/*1159.6*/("""}"""),format.raw/*1159.7*/(""");

				"""),format.raw/*1161.5*/("""}"""),format.raw/*1161.6*/(""");
				</script>
			</div>
		</div> <!-- end of right column -->
	</div>
""")))})),format.raw/*1166.2*/("""
"""))}
    }
    
    def render(file:models.File,id:String,comments:List[Comment],previews:Map[models.File, Array[scala.Tuple8[java.lang.String, String, String, String, java.lang.String, String, Long, String]]],sections:List[models.Section],beingProcessed:Boolean,datasets:List[models.Dataset],folders:List[Folder],mds:List[models.Metadata],rdfExported:Boolean,extractionsStatus:List[Extraction],outputFormats:Option[List[String]],spaceId:Option[String],access:String,folderHierarchy:List[Folder],spaces:List[ProjectSpace],allDatasets:List[Dataset],user:Option[models.User],request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(file,id,comments,previews,sections,beingProcessed,datasets,folders,mds,rdfExported,extractionsStatus,outputFormats,spaceId,access,folderHierarchy,spaces,allDatasets)(user,request)
    
    def f:((models.File,String,List[Comment],Map[models.File, Array[scala.Tuple8[java.lang.String, String, String, String, java.lang.String, String, Long, String]]],List[models.Section],Boolean,List[models.Dataset],List[Folder],List[models.Metadata],Boolean,List[Extraction],Option[List[String]],Option[String],String,List[Folder],List[ProjectSpace],List[Dataset]) => (Option[models.User],RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (file,id,comments,previews,sections,beingProcessed,datasets,folders,mds,rdfExported,extractionsStatus,outputFormats,spaceId,access,folderHierarchy,spaces,allDatasets) => (user,request) => apply(file,id,comments,previews,sections,beingProcessed,datasets,folders,mds,rdfExported,extractionsStatus,outputFormats,spaceId,access,folderHierarchy,spaces,allDatasets)(user,request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:46:59 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/file.scala.html
                    HASH: d600e45e28c95990aaa15555d324fc6f30eaf32c
                    MATRIX: 966->2|1786->945|1806->956|2030->1098|2112->1144|2136->1146|2199->1173|2209->1174|2234->1177|2300->1207|2310->1208|2335->1211|2404->1244|2419->1250|2457->1266|2502->1276|2511->1277|2536->1280|2582->1298|2611->1299|2678->1330|2688->1331|2713->1334|2781->1366|2791->1367|2816->1370|2847->1374|2875->1375|2924->1388|2934->1389|2959->1392|2999->1404|3028->1405|3106->1447|3118->1450|3183->1493|3263->1537|3275->1540|3341->1584|3423->1630|3435->1633|3502->1678|3569->1709|3655->1773|3725->1816|3753->1817|3802->1830|3812->1831|3837->1834|3877->1846|3906->1847|3936->1850|3964->1851|4013->1864|4023->1865|4048->1868|4090->1882|4119->1883|4186->1914|4196->1915|4221->1918|4253->1923|4281->1924|4330->1937|4340->1938|4365->1941|4420->1968|4449->1969|4539->2024|4566->2042|4606->2044|4653->2055|4700->2093|4739->2094|4833->2156|4869->2161|4902->2167|4930->2168|4979->2181|4989->2182|5014->2185|5068->2211|5097->2212|5187->2267|5214->2285|5254->2287|5301->2298|5348->2336|5387->2337|5481->2399|5517->2404|5550->2410|5578->2411|5664->2461|5723->2498|5768->749|5801->773|5881->612|5910->747|5939->822|5972->943|6000->2511|6038->2514|6059->2526|6099->2528|6158->2559|6187->2560|6253->2599|6282->2600|6330->2620|6359->2621|6421->2656|6450->2657|6498->2677|6527->2678|6591->2715|6620->2716|6702->2771|6731->2772|6771->2784|6800->2785|6850->2808|6878->2809|6923->2826|6952->2827|7002->2850|7030->2851|7074->2867|7103->2868|7180->2918|7208->2919|7245->2929|7274->2930|7359->2988|7387->2989|7422->2997|7450->2998|7505->3026|7533->3027|7564->3030|7593->3031|7668->3078|7698->3079|7745->3097|7775->3098|7826->3121|7855->3122|7908->3146|7938->3147|8011->3191|8041->3192|8100->3222|8130->3223|8203->3267|8233->3268|8293->3299|8323->3300|8396->3344|8426->3345|8471->3361|8501->3362|8552->3385|8581->3386|8623->3399|8653->3400|8704->3423|8733->3424|8779->3441|8809->3442|8860->3465|8889->3466|8938->3486|8968->3487|9019->3510|9048->3511|9086->3521|9116->3522|9201->3579|9230->3580|9279->3600|9309->3601|9360->3624|9389->3625|9436->3643|9466->3644|9517->3667|9546->3668|9590->3683|9620->3684|9671->3707|9700->3708|9744->3723|9774->3724|9825->3747|9854->3748|9915->3780|9945->3781|10018->3825|10048->3826|10080->3830|10109->3831|10173->3858|10189->3864|10254->3906|10341->3956|10357->3962|10426->4008|10619->4164|10635->4170|10695->4207|10782->4257|10798->4263|10870->4312|10957->4362|10973->4368|11039->4411|11126->4461|11142->4467|11209->4511|11296->4561|11312->4567|11375->4607|11462->4657|11478->4663|11549->4711|11636->4761|11652->4767|11718->4810|11819->4874|11849->4881|11961->4964|11991->4965|12021->4966|12085->4993|12099->4997|12125->5000|12199->5037|12213->5041|12246->5051|12320->5088|12393->5138|12473->5181|12554->5239|12627->5275|12701->5326|12769->5357|12825->5390|12897->5425|12948->5453|13020->5488|13034->5492|13128->5562|13220->5625|13250->5626|13954->6301|13984->6302|14112->6402|14141->6403|14314->6548|14343->6549|14406->6583|14436->6584|14728->6848|14757->6849|14818->6881|14848->6882|15004->7009|15034->7010|15092->7039|15122->7040|15220->7101|15234->7105|15260->7108|15297->7116|15327->7117|15436->7198|15465->7199|15552->7257|15582->7258|15999->7647|16028->7648|16119->7710|16149->7711|16396->7929|16426->7930|16552->8028|16581->8029|16614->8034|16643->8035|16678->8042|16707->8043|16763->8070|16793->8071|16968->8219|16998->8220|17084->8278|17113->8279|17146->8284|17175->8285|17256->8337|17286->8338|17343->8367|17372->8368|17538->8498|17571->8521|17612->8523|17717->8591|17733->8597|17789->8629|17837->8639|17854->8645|17888->8655|17930->8659|17991->8696|18027->8714|18041->8719|18081->8720|18125->8728|18156->8749|18197->8751|18623->9140|18639->9146|18653->9150|18698->9156|18760->9181|18776->9187|18821->9209|18923->9273|18974->9300|19027->9320|19097->9372|19111->9377|19151->9378|19259->9449|19275->9455|19339->9495|19381->9499|19396->9503|19436->9519|19485->9536|19524->9543|19567->9550|19605->9578|19646->9580|19690->9588|19711->9599|19725->9603|19772->9611|19883->9685|19899->9691|19947->9715|19995->9725|20008->9727|20037->9732|20079->9736|20131->9764|20180->9781|20219->9788|20262->9795|20287->9810|20301->9814|20348->9822|20461->9898|20477->9904|20537->9940|20586->9951|20599->9953|20626->9956|20674->9966|20687->9968|20723->9980|20764->9983|20823->10018|20871->10034|20979->10105|20993->10109|21025->10118|21065->10121|21123->10155|21396->10391|21439->10410|21488->10423|21589->10514|21631->10516|21924->10777|22135->10951|22176->10982|22217->10984|22256->10986|22302->11008|22325->11011|22340->11016|22381->11017|22425->11025|22526->11116|22567->11117|22632->11150|22671->11157|22718->11168|22819->11259|22861->11261|23115->11483|23177->11509|23219->11534|23260->11536|23342->11581|23380->11596|23440->11619|23475->11631|23531->11649|23543->11650|23576->11659|23913->11960|23965->12002|24006->12004|24095->12075|24109->12080|24149->12081|24262->12157|24300->12172|24349->12184|24387->12205|24428->12207|24568->12310|24597->12329|24638->12331|24720->12376|24758->12391|24797->12393|24821->12394|24879->12415|24895->12421|24921->12424|24961->12445|24975->12450|25015->12451|25097->12496|25135->12511|25174->12513|25198->12514|25256->12535|25272->12541|25298->12544|25351->12564|25394->12574|25501->12644|25539->12659|25589->12672|25627->12693|25668->12695|25764->12754|25802->12769|25841->12771|25865->12772|25921->12791|25956->12802|26015->12823|26050->12834|26107->12854|26201->12925|26246->12937|26342->13004|26372->13005|26433->13029|26471->13044|26535->13079|26565->13080|26654->13137|26719->13166|26748->13185|26789->13187|26859->13220|26897->13235|26984->13290|27191->13461|27299->13558|27341->13560|27588->13788|27602->13793|27642->13794|27929->14049|28098->14182|28134->14208|28174->14209|28519->14517|28542->14530|28560->14538|28615->14554|28709->14611|28725->14617|28794->14662|28854->14684|28888->14694|28942->14715|29050->14790|29183->14887|29211->14905|29252->14907|29425->15044|29567->15175|29608->15176|29667->15199|29700->15222|29741->15224|29860->15306|29892->15315|29933->15319|29993->15355|30042->15366|30109->15409|30260->15541|30274->15546|30314->15547|30361->15557|30393->15579|30434->15581|30554->15664|30586->15673|30627->15677|30687->15713|30736->15724|30815->15779|30864->15790|30882->15797|30914->15805|31079->15937|31120->15946|31163->15971|31177->15976|31217->15977|31457->16185|31502->16194|31553->16235|31594->16237|31737->16343|31751->16347|31777->16350|31837->16373|31851->16377|31883->16386|32019->16503|32033->16508|32073->16509|32215->16614|32229->16618|32255->16621|32314->16643|32328->16647|32360->16656|32516->16779|32556->16787|33335->17530|33366->17551|33407->17553|33811->17928|33841->17929|33993->18052|34023->18053|34175->18176|34205->18177|34472->18415|34502->18416|34542->18427|34572->18428|34613->18440|34643->18441|35204->18965|35222->18973|35236->18977|35282->18984|35371->19036|35428->19070|35526->19131|35587->19182|35628->19184|35678->19197|35716->19225|35756->19226|35817->19250|35890->19300|35924->19315|35938->19320|35978->19321|36039->19345|36052->19348|36118->19391|36166->19406|36273->19476|36308->19501|36349->19503|36446->19563|36457->19564|36490->19574|36533->19598|36547->19603|36587->19604|36638->19618|36668->19638|36709->19640|36797->19691|36808->19692|36845->19706|36880->19722|36894->19727|36934->19728|37048->19809|37094->19822|37158->19853|37263->19921|37298->19946|37339->19948|37389->19961|37434->19983|37473->19985|37533->20022|37565->20035|37579->20040|37619->20041|37669->20054|37699->20074|37740->20076|37791->20090|37802->20091|37843->20109|37876->20123|37890->20128|37930->20129|38001->20167|38046->20179|38172->20268|38201->20274|38287->20323|38298->20324|38348->20350|38405->20370|38433->20388|38474->20390|38525->20404|38573->20442|38613->20443|38695->20488|38724->20494|38879->20612|38909->20618|38984->20655|38996->20656|39046->20682|39222->20820|39234->20821|39261->20824|39350->20875|39380->20881|39600->21064|39611->21065|39637->21068|39679->21073|39692->21076|39759->21119|39862->21184|39892->21190|40108->21369|40119->21370|40145->21373|40250->21440|40280->21446|40482->21611|40511->21617|40555->21624|40566->21625|40615->21651|40687->21690|40733->21703|40839->21772|40855->21778|40922->21821|40998->21864|41099->21936|41129->21937|41491->22270|41521->22271|41618->22339|41648->22340|41719->22382|41749->22383|41921->22526|41951->22527|42018->22565|42048->22566|42138->22619|42156->22627|42170->22631|42216->22638|42264->22649|42299->22674|42340->22676|42388->22695|42418->22696|42474->22715|42485->22716|42518->22726|42579->22758|42609->22759|42671->22784|42682->22785|42711->22791|42808->22851|42819->22852|42852->22862|42950->22931|42980->22932|43039->22962|43069->22963|43142->23007|43172->23008|43211->23018|43241->23019|43287->23032|43331->23043|43368->23052|43398->23053|43434->23061|43463->23062|43523->23086|43618->23171|43678->23191|43744->23228|43774->23229|43877->23295|43893->23301|43983->23367|44063->23410|44081->23418|44095->23422|44141->23429|44189->23440|44222->23463|44263->23465|44317->23490|44347->23491|44377->23492|44451->23529|44464->23532|44530->23575|44580->23588|44615->23613|44656->23615|44786->23708|44797->23709|44830->23719|44872->23742|44886->23747|44926->23748|44974->23759|45004->23779|45045->23781|45165->23864|45176->23865|45213->23879|45246->23893|45260->23898|45300->23899|45443->24009|45487->24020|45573->24073|45617->24084|45666->24096|45697->24117|45737->24118|45795->24147|45825->24148|45890->24184|45920->24185|46116->24352|46146->24353|46212->24390|46242->24391|46312->24432|46342->24433|46578->24640|46608->24641|46973->24977|47003->24978|47187->25133|47217->25134|47262->25150|47292->25151|47335->25165|47365->25166|47405->25177|47435->25178|47510->25224|47540->25225|47586->25238|47634->25249|47666->25271|47706->25272|47803->25340|47833->25341|47886->25365|47916->25366|47962->25379|48092->25480|48122->25481|48283->25613|48313->25614|48414->25686|48444->25687|48700->25914|48730->25915|48813->25969|48843->25970|48884->25982|48914->25983|48953->25994|48983->25995|49040->26020|49078->26026|49115->26031|49267->26147|49371->26240|49413->26242|49457->26250|49532->26302|49571->26309|49651->26352|49665->26356|49700->26368|49745->26377|49785->26394|50216->26789|50244->26807|50285->26809|50341->26965|50389->26981|50517->27073|50537->27083|50583->27106|50996->27483|51171->27648|51212->27650|51266->27667|51282->27673|51347->27715|51509->27845|51597->27897|51618->27908|51678->27945|51809->28047|51839->28048|52815->28996|52844->28997|52904->29028|52934->29029|53013->29079|53043->29080|53136->29145|53165->29146|53200->29152|53230->29153|53302->29197|53331->29198|53439->29277|53469->29278|53522->29302|53552->29303|53649->29363|53663->29367|53689->29370|53726->29378|53756->29379|53873->29468|53902->29469|53991->29529|54021->29530|54401->29882|54430->29883|54522->29946|54552->29947|54783->30149|54813->30150|54929->30238|54958->30239|54993->30246|55022->30247|55058->30255|55087->30256|55149->30289|55179->30290|55571->30654|55600->30655|55664->30690|55694->30691|55788->30756|55818->30757|55954->30865|55983->30866|56060->30914|56090->30915|56226->31023|56255->31024|56291->31032|56320->31033|56592->31268|56606->31272|56641->31284|56741->31347|56755->31351|56785->31358|56831->31367|56890->31402|56943->31419|56990->31456|57031->31458|57122->31513|57188->31569|57229->31571|57293->31603|57338->31612|57411->31675|57452->31677|57499->31687|57535->31713|57576->31715|57632->31752|57646->31757|57686->31758|57749->31784|57787->31812|57828->31814|57891->31840|57905->31844|57938->31854|57981->31860|57995->31864|58088->31933|58127->31953|58141->31958|58181->31959|58231->31972|58245->31976|58278->31986|58323->31998|58424->32062|58438->32066|58471->32076|58607->32179|58648->32188|58699->32207|58785->32256|58799->32260|58865->32303|58918->32320|58960->32352|59001->32354|59088->32404|59102->32408|59138->32421|59187->32438|59284->32498|59300->32504|59364->32544|59406->32548|59421->32552|59461->32568|59518->32589|59599->32660|59640->32662|59717->32702|59746->32708|59795->32725|59871->32764|59885->32768|59915->32775|60509->33333|60614->33427|60656->33429|60973->33714|61168->33872|61196->33890|61237->33892|61285->33903|61385->33980|61428->33990|61549->34074|61565->34080|61631->34123|61788->34251|61818->34252|62212->34609|62226->34613|62275->34639|62347->34674|62361->34678|62411->34705|62482->34739|62496->34743|62545->34769|62615->34802|62629->34806|62677->34831|62750->34867|62764->34871|62823->34907|62892->34939|62908->34945|62951->34965|63021->34998|63035->35002|63074->35018|63128->35035|63142->35039|63175->35049|63205->35050|63235->35051|63414->35202|63443->35203|63533->35264|63563->35265|63769->35442|63799->35443|63919->35535|63949->35536|63984->35542|64014->35543|64119->35619|64149->35620|64310->35752|64340->35753|64390->35775|64420->35776|64456->35784|64485->35785|64578->35849|64608->35850|64757->35971|64786->35972|64859->36016|64889->36017|65038->36138|65067->36139|65104->36148|65133->36149|65196->36183|65226->36184|65293->36214|65309->36220|65357->36245|65439->36299|65468->36300|65544->36347|65574->36348|65719->36465|65748->36466|65958->36640|65991->36663|66032->36665|66080->36676|66162->36735|66326->36863|66344->36871|66358->36875|66410->36888|66468->36909|66485->36916|66511->36919|66633->37005|66676->37038|66716->37039|66772->37058|66832->37095|66928->37154|67008->37210|67067->37231|67109->37249|67165->37273|67301->37372|67361->37409|67405->37416|67422->37423|67448->37426|67505->37446|67547->37464|67636->37517|67737->37608|67779->37610|68172->37965|68190->37972|68217->37975|68259->37979|68274->37983|68301->37986|68463->38116|68502->38123|68540->38129|68582->38135|68614->38157|68655->38159|68739->38207|68756->38214|68770->38218|68821->38230|68879->38251|68895->38257|68934->38273|69147->38449|69163->38455|69227->38496|69275->38507|69291->38513|69317->38516|69361->38523|69378->38529|69418->38545|69476->38565|69524->38589|69613->38642|69714->38733|69756->38735|70156->39097|70173->39103|70200->39106|70242->39110|70259->39116|70299->39132|70341->39136|70356->39140|70383->39143|70491->39214|70539->39239|70635->39303|70674->39310|70712->39316|70814->39389|70844->39390|70940->39449|70988->39474|71058->39515|71088->39516|71148->39547|71178->39548|71278->39611|71292->39615|71318->39618|71372->39644|71402->39645|71464->39678|71494->39679|71540->39696|71570->39697|71619->39717|71649->39718|71687->39728|71717->39729|71793->39776|71823->39777|71869->39794|71899->39795|71961->39828|71991->39829|72071->39880|72101->39881|72182->39932|72213->39933|72302->39992|72333->39993|72381->40011|72412->40012|72461->40031|72492->40032|72581->40091|72612->40092|72653->40103|72684->40104|72715->40105|72746->40106|72785->40116|72816->40117|72853->40125|72883->40126|72919->40133|72949->40134|73185->40333|73200->40337|73220->40346|73269->40355|73340->40388|73357->40394|73403->40416|73448->40423|73462->40426|73491->40431|73541->40443|73555->40446|73582->40449|73623->40452|73662->40466|73791->40557|73805->40560|73834->40565|73884->40577|73898->40580|73925->40583|74018->40643|74062->40650|74077->40654|74101->40667|74154->40680|74199->40688|74217->40695|74237->40704|74286->40713|74346->40735|74363->40741|74409->40763|74450->40766|74488->40780|74548->40807|74588->40814|74641->40830|74741->40919|74783->40921|74948->41048|74963->41052|75054->41118|75346->41391|75361->41396|75402->41397|75576->41533|75591->41537|75682->41603|75987->41875|76116->41967|76145->41985|76187->41987|76314->42081|76508->42245|76539->42246|77374->43043|77389->43047|77416->43050|77454->43058|77485->43059|77545->43089|77576->43090|77620->43104|77651->43105|77752->43177|77782->43178|77875->43241|77906->43242|78055->43362|78085->43363|78180->43428|78211->43429|78450->43638|78481->43639|78610->43739|78641->43740|78678->43748|78708->43749|78768->43780|78798->43781|78863->43816|78894->43817|78980->43874|79010->43875|79076->43911|79107->43912|79194->43970|79224->43971|79273->43990|79304->43991|79444->44101|79475->44102|79589->44187|79619->44188|79749->44288|79780->44289|79975->44454|80006->44455|80076->44495|80107->44496|80173->44533|80203->44534|80239->44541|80269->44542|80349->44592|80380->44593|80514->44689|80529->44693|80556->44696|80594->44704|80625->44705|80685->44735|80716->44736|80758->44748|80789->44749|80891->44822|80921->44823|81013->44885|81044->44886|81686->45499|81716->45500|81811->45565|81842->45566|82076->45770|82107->45771|82232->45867|82263->45868|82300->45876|82330->45877|82389->45907|82419->45908|82454->45914|82484->45915|82560->45961|82591->45962|82647->45988|82678->45989|82791->46073|82821->46074|82856->46080|82886->46081|82923->46089|82953->46090|83060->46164
                    LINES: 20->2|37->20|37->20|39->20|41->22|41->22|42->23|42->23|42->23|43->24|43->24|43->24|44->25|44->25|44->25|45->26|45->26|45->26|45->26|45->26|46->27|46->27|46->27|47->28|47->28|47->28|48->29|48->29|49->30|49->30|49->30|49->30|49->30|50->31|50->31|50->31|51->32|51->32|51->32|52->33|52->33|52->33|53->34|53->34|55->36|55->36|56->37|56->37|56->37|56->37|56->37|57->38|57->38|58->39|58->39|58->39|58->39|58->39|59->40|59->40|59->40|60->41|60->41|61->42|61->42|61->42|61->42|61->42|63->44|63->44|63->44|64->45|64->45|64->45|66->47|67->48|68->49|68->49|69->50|69->50|69->50|69->50|69->50|71->52|71->52|71->52|72->53|72->53|72->53|74->55|75->56|76->57|76->57|78->59|78->59|79->16|79->16|80->8|82->15|83->16|86->19|87->60|89->62|89->62|89->62|93->66|93->66|95->68|95->68|97->70|97->70|99->72|99->72|101->74|101->74|103->76|103->76|105->78|105->78|106->79|106->79|108->81|108->81|110->83|110->83|112->85|112->85|114->87|114->87|117->90|117->90|119->92|119->92|122->95|122->95|124->97|124->97|126->99|126->99|127->100|127->100|129->102|129->102|131->104|131->104|133->106|133->106|135->108|135->108|137->110|137->110|139->112|139->112|141->114|141->114|143->116|143->116|145->118|145->118|147->120|147->120|149->122|149->122|151->124|151->124|153->126|153->126|155->128|155->128|157->130|157->130|159->132|159->132|161->134|161->134|163->136|163->136|166->139|166->139|168->141|168->141|170->143|170->143|172->145|172->145|174->147|174->147|176->149|176->149|178->151|178->151|180->153|180->153|182->155|182->155|184->157|184->157|186->159|186->159|188->161|188->161|191->164|191->164|191->164|192->165|192->165|192->165|194->167|194->167|194->167|195->168|195->168|195->168|196->169|196->169|196->169|197->170|197->170|197->170|198->171|198->171|198->171|199->172|199->172|199->172|200->173|200->173|200->173|202->175|202->175|205->178|205->178|205->178|206->179|206->179|206->179|207->180|207->180|207->180|208->181|208->181|209->182|209->182|210->183|210->183|211->184|211->184|212->185|212->185|213->186|213->186|213->186|215->188|215->188|221->194|221->194|223->196|223->196|227->200|227->200|229->202|229->202|236->209|236->209|238->211|238->211|241->214|241->214|241->214|241->214|243->216|243->216|243->216|243->216|243->216|247->220|247->220|249->222|249->222|257->230|257->230|259->232|259->232|262->235|262->235|264->237|264->237|265->238|265->238|267->240|267->240|269->242|269->242|271->244|271->244|273->246|273->246|274->247|274->247|277->250|277->250|279->252|279->252|286->259|286->259|286->259|287->260|287->260|287->260|287->260|287->260|287->260|287->260|287->260|289->262|289->262|289->262|290->263|290->263|290->263|298->271|298->271|298->271|298->271|299->272|299->272|299->272|299->272|299->272|300->273|305->278|305->278|305->278|306->279|306->279|306->279|306->279|306->279|306->279|307->280|308->281|309->282|309->282|309->282|310->283|310->283|310->283|310->283|311->284|311->284|311->284|311->284|311->284|311->284|311->284|311->284|312->285|313->286|314->287|314->287|314->287|314->287|315->288|315->288|315->288|315->288|315->288|315->288|315->288|315->288|315->288|315->288|315->288|316->289|317->290|317->290|317->290|317->290|317->290|322->295|322->295|323->296|323->296|323->296|329->302|335->308|335->308|335->308|335->308|335->308|335->308|335->308|335->308|336->309|336->309|336->309|338->311|339->312|340->313|340->313|340->313|344->317|347->320|347->320|347->320|348->321|348->321|348->321|348->321|348->321|348->321|348->321|355->328|355->328|355->328|357->330|357->330|357->330|359->332|359->332|360->333|360->333|360->333|362->335|362->335|362->335|363->336|363->336|363->336|363->336|363->336|363->336|363->336|364->337|364->337|364->337|365->338|365->338|365->338|365->338|365->338|365->338|365->338|366->339|367->340|369->342|369->342|370->343|370->343|370->343|371->344|371->344|371->344|371->344|371->344|371->344|371->344|371->344|372->345|372->345|374->347|376->349|376->349|377->350|377->350|378->351|378->351|382->355|385->358|385->358|385->358|387->360|387->360|389->362|395->368|395->368|395->368|398->371|398->371|398->371|402->375|404->377|404->377|404->377|408->381|408->381|408->381|408->381|409->382|409->382|409->382|409->382|409->382|410->383|413->386|415->388|415->388|415->388|417->390|417->390|417->390|419->392|419->392|419->392|420->393|420->393|420->393|420->393|420->393|420->393|422->395|422->395|422->395|423->396|423->396|423->396|424->397|424->397|424->397|424->397|424->397|424->397|424->397|424->397|424->397|426->399|427->400|430->403|430->403|430->403|434->407|436->409|436->409|436->409|439->412|439->412|439->412|440->413|440->413|440->413|443->416|443->416|443->416|446->419|446->419|446->419|447->420|447->420|447->420|450->423|451->424|476->449|476->449|476->449|484->457|484->457|486->459|486->459|488->461|488->461|491->464|491->464|492->465|492->465|493->466|493->466|510->483|510->483|510->483|510->483|512->485|512->485|514->487|514->487|514->487|515->488|515->488|515->488|516->489|516->489|517->490|517->490|517->490|518->491|518->491|518->491|519->492|521->494|521->494|521->494|522->495|522->495|522->495|523->496|523->496|523->496|524->497|524->497|524->497|525->498|525->498|525->498|526->499|526->499|526->499|528->501|529->502|531->504|534->507|534->507|534->507|535->508|535->508|535->508|535->508|536->509|536->509|536->509|537->510|537->510|537->510|538->511|538->511|538->511|539->512|539->512|539->512|541->514|542->515|545->518|545->518|545->518|545->518|545->518|546->519|546->519|546->519|547->520|547->520|547->520|548->521|548->521|549->522|549->522|549->522|549->522|549->522|551->524|551->524|551->524|551->524|551->524|553->526|553->526|553->526|553->526|553->526|553->526|553->526|553->526|555->528|555->528|555->528|555->528|555->528|559->532|559->532|559->532|559->532|559->532|561->534|562->535|564->537|564->537|564->537|566->539|569->542|569->542|574->547|574->547|576->549|576->549|578->551|578->551|586->559|586->559|587->560|587->560|590->563|590->563|590->563|590->563|591->564|591->564|591->564|592->565|592->565|593->566|593->566|593->566|594->567|594->567|595->568|595->568|595->568|596->569|596->569|596->569|598->571|598->571|599->572|599->572|601->574|601->574|602->575|602->575|603->576|604->577|605->578|605->578|606->579|606->579|608->581|608->581|608->581|610->583|610->583|612->585|612->585|612->585|615->588|615->588|615->588|615->588|616->589|616->589|616->589|617->590|617->590|617->590|618->591|618->591|618->591|619->592|619->592|619->592|621->594|621->594|621->594|622->595|622->595|622->595|623->596|623->596|623->596|625->598|625->598|625->598|626->599|626->599|626->599|629->602|630->603|632->605|633->606|635->608|635->608|635->608|636->609|636->609|638->611|638->611|640->613|640->613|641->614|641->614|642->615|642->615|647->620|647->620|653->626|653->626|656->629|656->629|657->630|657->630|658->631|658->631|659->632|659->632|661->634|661->634|662->635|663->636|663->636|663->636|665->638|665->638|665->638|665->638|666->639|670->643|670->643|673->646|673->646|675->648|675->648|680->653|680->653|682->655|682->655|683->656|683->656|684->657|684->657|686->659|687->660|688->661|693->666|693->666|693->666|694->667|694->667|695->668|696->669|696->669|696->669|697->670|697->670|715->688|715->688|715->688|717->694|719->696|722->699|722->699|722->699|734->711|735->712|735->712|736->713|736->713|736->713|740->717|742->719|742->719|742->719|750->727|750->727|761->738|761->738|763->740|763->740|764->741|764->741|767->744|767->744|767->744|767->744|769->746|769->746|771->748|771->748|771->748|771->748|773->750|773->750|773->750|773->750|773->750|777->754|777->754|778->755|778->755|785->762|785->762|786->763|786->763|789->766|789->766|791->768|791->768|792->769|792->769|793->770|793->770|795->772|795->772|803->780|803->780|805->782|805->782|806->783|806->783|809->786|809->786|809->786|809->786|812->789|812->789|813->790|813->790|821->798|821->798|821->798|822->799|822->799|822->799|822->799|822->799|823->800|823->800|823->800|825->802|825->802|825->802|827->804|828->805|828->805|828->805|829->806|829->806|829->806|831->808|831->808|831->808|833->810|833->810|833->810|834->811|834->811|834->811|834->811|834->811|834->811|835->812|835->812|835->812|836->813|836->813|836->813|837->814|838->815|838->815|838->815|839->816|840->817|842->819|843->820|843->820|843->820|844->821|844->821|844->821|845->822|845->822|845->822|846->823|847->824|847->824|847->824|847->824|847->824|847->824|848->825|848->825|848->825|849->826|849->826|850->827|851->828|851->828|851->828|873->850|873->850|873->850|879->856|884->861|884->861|884->861|885->862|885->862|886->863|892->869|892->869|892->869|894->871|894->871|899->876|899->876|899->876|900->877|900->877|900->877|901->878|901->878|901->878|902->879|902->879|902->879|903->880|903->880|903->880|904->881|904->881|904->881|905->882|905->882|905->882|907->884|907->884|907->884|907->884|907->884|909->886|909->886|911->888|911->888|915->892|915->892|917->894|917->894|917->894|917->894|918->895|918->895|921->898|921->898|922->899|922->899|923->900|923->900|924->901|924->901|927->904|927->904|927->904|927->904|930->907|930->907|931->908|931->908|933->910|933->910|934->911|934->911|934->911|936->913|936->913|938->915|938->915|941->918|941->918|949->926|949->926|949->926|950->927|950->927|952->929|952->929|952->929|952->929|953->930|953->930|953->930|955->932|955->932|955->932|956->933|956->933|957->934|957->934|957->934|957->934|959->936|963->940|963->940|963->940|963->940|963->940|963->940|963->940|967->944|967->944|967->944|972->949|972->949|972->949|972->949|972->949|972->949|978->955|979->956|980->957|981->958|981->958|981->958|983->960|983->960|983->960|983->960|984->961|984->961|984->961|989->966|989->966|989->966|989->966|989->966|989->966|989->966|989->966|989->966|989->966|989->966|993->970|993->970|993->970|998->975|998->975|998->975|998->975|998->975|998->975|998->975|998->975|998->975|999->976|999->976|1004->981|1005->982|1006->983|1008->985|1008->985|1010->987|1010->987|1012->989|1012->989|1013->990|1013->990|1014->991|1014->991|1014->991|1015->992|1015->992|1016->993|1016->993|1017->994|1017->994|1017->994|1017->994|1018->995|1018->995|1019->996|1019->996|1020->997|1020->997|1020->997|1020->997|1021->998|1021->998|1023->1000|1023->1000|1025->1002|1025->1002|1025->1002|1025->1002|1026->1003|1026->1003|1029->1006|1029->1006|1030->1007|1030->1007|1030->1007|1030->1007|1031->1008|1031->1008|1032->1009|1032->1009|1033->1010|1033->1010|1041->1018|1041->1018|1041->1018|1041->1018|1042->1019|1042->1019|1042->1019|1042->1019|1042->1019|1042->1019|1042->1019|1042->1019|1042->1019|1042->1019|1042->1019|1044->1021|1044->1021|1044->1021|1044->1021|1044->1021|1044->1021|1045->1022|1046->1023|1046->1023|1046->1023|1046->1023|1047->1024|1047->1024|1047->1024|1047->1024|1048->1025|1048->1025|1048->1025|1048->1025|1048->1025|1049->1026|1050->1027|1052->1029|1052->1029|1052->1029|1055->1032|1055->1032|1055->1032|1063->1040|1063->1040|1063->1040|1066->1043|1066->1043|1066->1043|1074->1051|1078->1055|1078->1055|1078->1055|1082->1059|1085->1062|1085->1062|1096->1073|1096->1073|1096->1073|1096->1073|1096->1073|1097->1074|1097->1074|1097->1074|1097->1074|1100->1077|1100->1077|1102->1079|1102->1079|1105->1082|1105->1082|1107->1084|1107->1084|1110->1087|1110->1087|1112->1089|1112->1089|1113->1090|1113->1090|1116->1093|1116->1093|1118->1095|1118->1095|1120->1097|1120->1097|1122->1099|1122->1099|1124->1101|1124->1101|1126->1103|1126->1103|1129->1106|1129->1106|1133->1110|1133->1110|1136->1113|1136->1113|1141->1118|1141->1118|1142->1119|1142->1119|1144->1121|1144->1121|1145->1122|1145->1122|1147->1124|1147->1124|1149->1126|1149->1126|1149->1126|1149->1126|1149->1126|1150->1127|1150->1127|1150->1127|1150->1127|1153->1130|1153->1130|1155->1132|1155->1132|1163->1140|1163->1140|1165->1142|1165->1142|1168->1145|1168->1145|1170->1147|1170->1147|1171->1148|1171->1148|1173->1150|1173->1150|1174->1151|1174->1151|1176->1153|1176->1153|1177->1154|1177->1154|1181->1158|1181->1158|1182->1159|1182->1159|1184->1161|1184->1161|1189->1166
                    -- GENERATED --
                */
            