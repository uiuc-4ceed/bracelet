
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object licenseform extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[String,models.LicenseData,String,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(id: String, licenseData: models.LicenseData, sourceObject: String, authorName: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.125*/("""

<!--
User interface panel that displays license options to the user and allows them to select one. Depending on the given license
type selected, the user will then have the option to make other choices regarding certain specifics for that license type. 
Finally, handle the user submission by pushing the data up to the correct API endpoint. In this case, it is either the 
File or Dataset API updateLicense endpoint, through the specified jsRoute.

In the future, may need to see if that endpoint can be unified.

Author: Mario Felarca

Used By: dataset.scala.html, file.scala.html 
 -->

"""),_display_(Seq[Any](/*16.2*/if(user.isDefined)/*16.20*/ {_display_(Seq[Any](format.raw/*16.22*/("""
	<form class="form-inline" id="form1">
	   <table class="license1 license2 hidden" style="width: 100%;">
	       <tr class="license1 license2 hidden">
		       <td colspan="2"><input type="checkbox" id="ownrights" value="yes">  I own the rights</td>
		   </tr>
		   <tr>
		       <td style="width: 30%">Rights Holder:</td>
		       <td><input style="width: 100%"; type="text" name="ownername"></td>
		   </tr>
		</table>
		<br>
		<br>
		License Selection:
		<br>
        <input type="radio" name="type" value="license1"> Limited<br>
        <input type="radio" name="type" value="license2"> Creative Commons<br>
		<input type="radio" name="type" value="license3"> Public Domain<br>
		<br>
		<table style="width: 100%;">
			<tr class="license1 hidden">
				<td style="width: 30%;">Description:</td>
				<td><input style="width: 100%;" type="text" name="licensedesc"></td>
			</tr>
			<tr class="license1 hidden">
				<td style="width: 30%;">License URL:</td>
				<td><input style="width: 100%;" type="text" name="licenseurl"></td>
			</tr>
			<tr class="license1 hidden">
			     <td colspan="2"><input type="checkbox" name="allowDownload" value="off">  Allow Downloading of the file</td>
			</tr>
			<tr class="license2 hidden">
				<td><input type="checkbox" id="commercial" value="yes">
					Allow commercial use</td>
			</tr>
			<tr class="license2 hidden">
				<td><input type="checkbox" id="remixing" value="yes">
					Allow remixing</td>
			</tr>
			<tr class="license2 hidden">
				<td><input type="checkbox" id="sharealike" value="yes" disabled>
					Require share-alike</td>
			</tr>
		</table>		
		<br>
		<button class="btn btn-primary" title="Update License Information" onclick='return updateData(""""),_display_(Seq[Any](/*61.99*/id)),format.raw/*61.101*/("""", """"),_display_(Seq[Any](/*61.106*/routes/*61.112*/.Assets.at("images"))),format.raw/*61.132*/("""", """"),_display_(Seq[Any](/*61.137*/sourceObject)),format.raw/*61.149*/("""", """"),_display_(Seq[Any](/*61.154*/authorName)),format.raw/*61.164*/("""")'>
		  <span class="glyphicon glyphicon-saved"></span> Submit
		</button>
		<!-- <button class="btn btn-default" onclick="return updateInterface();">Update</button> -->
		<button class="btn btn-default" title="Close Editor" onclick="return closeEdit();">
            <span class="glyphicon glyphicon-eject"></span> Close
		</button>
	</form>
	
	<!-- Need to also handle what to set as default radio. Whether it is default, because
	     there is no data, or if it is passed in from the model.
	-->
	
	<script type="text/javascript" language="javascript">
	
	var currentUserName = htmlDecode(""""),_display_(Seq[Any](/*76.37*/user/*76.41*/.get.fullName)),format.raw/*76.54*/(""""); 
    
	
    $(document).ready(function() """),format.raw/*79.34*/("""{"""),format.raw/*79.35*/("""        
        //Will have to modify the if check to see if there is data that specifies what should be selected
        //Incoming data may specifiy the type of license, the name of the owner of the rights, the text
        //describing the license rights, the URL for the license, and whether or not downloading is 
        //allowed.        
        var licenseType = """"),_display_(Seq[Any](/*84.29*/licenseData/*84.40*/.m_licenseType)),format.raw/*84.54*/("""";
        var rightsHolder = htmlDecode(""""),_display_(Seq[Any](/*85.41*/licenseData/*85.52*/.m_rightsHolder)),format.raw/*85.67*/("""");
        var licenseText = htmlDecode(""""),_display_(Seq[Any](/*86.40*/licenseData/*86.51*/.m_licenseText)),format.raw/*86.65*/("""");
        var licenseUrl = htmlDecode(""""),_display_(Seq[Any](/*87.39*/licenseData/*87.50*/.m_licenseUrl)),format.raw/*87.63*/("""");
        var allowDownload = """"),_display_(Seq[Any](/*88.31*/licenseData/*88.42*/.m_allowDownload)),format.raw/*88.58*/("""";
        var $radios = $('input[name=type]');       
        
        
        if (licenseType == null || licenseType.trim().length == 0) """),format.raw/*92.68*/("""{"""),format.raw/*92.69*/("""            
            //No license data is present so far, so go to defaults
            if($radios.is(':checked') === false) """),format.raw/*94.50*/("""{"""),format.raw/*94.51*/("""
                //None are selected, so select the default
                $radios.filter('[value=license1]').trigger('click');       
                //$('#ownrights').trigger('click');
                $('input[name=licensedesc]').val("All Rights Reserved");
                //updateLicenseForm('.' + $radios.filter('[value=license1]').val()); //This one works for sure!
                //updateLicenseForm('.' + $('input[name=type]:checked', '#form1').val()); //Works also, is better.
            """),format.raw/*101.13*/("""}"""),format.raw/*101.14*/("""
            else """),format.raw/*102.18*/("""{"""),format.raw/*102.19*/("""
                //Select the one that the data specifies.
                notify('Error setting defaults', "error");
            """),format.raw/*105.13*/("""}"""),format.raw/*105.14*/("""
        """),format.raw/*106.9*/("""}"""),format.raw/*106.10*/("""        
        else """),format.raw/*107.14*/("""{"""),format.raw/*107.15*/("""
            //license data is present 

            if (licenseType != "license3") """),format.raw/*110.44*/("""{"""),format.raw/*110.45*/("""
            	$radios.filter("[value=license3]").trigger('click');
            	$radios.filter("[value=" + licenseType + "]").trigger('click');	
            """),format.raw/*113.13*/("""}"""),format.raw/*113.14*/("""
            else """),format.raw/*114.18*/("""{"""),format.raw/*114.19*/("""
            	$radios.filter("[value=license1]").trigger('click');
                $radios.filter("[value=" + licenseType + "]").trigger('click');
            """),format.raw/*117.13*/("""}"""),format.raw/*117.14*/("""

            if (rightsHolder == currentUserName) """),format.raw/*119.50*/("""{"""),format.raw/*119.51*/("""
            	if (!$('#ownrights').prop('checked')) """),format.raw/*120.52*/("""{"""),format.raw/*120.53*/("""
            	    $('#ownrights').trigger('click');
            	"""),format.raw/*122.14*/("""}"""),format.raw/*122.15*/("""
            """),format.raw/*123.13*/("""}"""),format.raw/*123.14*/("""                        
            else """),format.raw/*124.18*/("""{"""),format.raw/*124.19*/("""            	
                if (licenseType == "license1" || licenseType == 'license2') """),format.raw/*125.77*/("""{"""),format.raw/*125.78*/("""
                    if (rightsHolder == null || rightsHolder.trim().length == 0)"""),format.raw/*126.81*/("""{"""),format.raw/*126.82*/("""                    	
                    	//No rights holder set, so default to "Author"
                    	$('input[name=ownername]').val(htmlDecode(""""),_display_(Seq[Any](/*128.66*/authorName)),format.raw/*128.76*/(""""));
                    	//If Author is same as current user, trigger the box.
                    	if (""""),_display_(Seq[Any](/*130.28*/authorName)),format.raw/*130.38*/("""" == currentUserName) """),format.raw/*130.60*/("""{"""),format.raw/*130.61*/("""
                    		if (!$('#ownrights').prop('checked')) """),format.raw/*131.61*/("""{"""),format.raw/*131.62*/("""
                                $('#ownrights').trigger('click');
                            """),format.raw/*133.29*/("""}"""),format.raw/*133.30*/("""
                    	"""),format.raw/*134.22*/("""}"""),format.raw/*134.23*/("""
                    """),format.raw/*135.21*/("""}"""),format.raw/*135.22*/("""
                    else """),format.raw/*136.26*/("""{"""),format.raw/*136.27*/("""
                        //In this case, the rightsHolder is valid free text
                        $('input[name=ownername]').val(rightsHolder);
                    """),format.raw/*139.21*/("""}"""),format.raw/*139.22*/("""
                """),format.raw/*140.17*/("""}"""),format.raw/*140.18*/("""                
            """),format.raw/*141.13*/("""}"""),format.raw/*141.14*/("""            
            
            //Set license text and license url based off license type
            if (licenseType == "license1") """),format.raw/*144.44*/("""{"""),format.raw/*144.45*/("""
                if (licenseText == null || licenseText.trim().length == 0) """),format.raw/*145.76*/("""{"""),format.raw/*145.77*/("""
                    $('input[name=licensedesc]').val("All Rights Reserved");
                """),format.raw/*147.17*/("""}"""),format.raw/*147.18*/("""
                else """),format.raw/*148.22*/("""{"""),format.raw/*148.23*/("""
                    $('input[name=licensedesc]').val(licenseText);
                """),format.raw/*150.17*/("""}"""),format.raw/*150.18*/("""
                
                $('input[name=licenseurl]').val(licenseUrl);                
                
                if (allowDownload == "true") """),format.raw/*154.46*/("""{"""),format.raw/*154.47*/("""
                    $('input[name=allowDownload]').attr('checked', true);
                """),format.raw/*156.17*/("""}"""),format.raw/*156.18*/("""
                else """),format.raw/*157.22*/("""{"""),format.raw/*157.23*/("""
                    $('input[name=allowDownload]').attr('checked', false);
                """),format.raw/*159.17*/("""}"""),format.raw/*159.18*/("""                
            """),format.raw/*160.13*/("""}"""),format.raw/*160.14*/("""            
            else if (licenseType == "license2") """),format.raw/*161.49*/("""{"""),format.raw/*161.50*/("""
                //Check the license text to determine what checkboxes to select
                //TODO make this so that the checkbox data simply comes down with the licensedata as well??
                var commBox = $('#commercial');
                var remixBox = $('#remixing');
                var shareBox = $('#sharealike');
                
                if (licenseText == "Attribution-NoDerivs") """),format.raw/*168.60*/("""{"""),format.raw/*168.61*/("""
                    //Only commercial checked
                    commBox.attr('checked', true);
                    localShareBox.attr("disabled", true);
                """),format.raw/*172.17*/("""}"""),format.raw/*172.18*/("""
                else if (licenseText == "Attribution-NonCommercial") """),format.raw/*173.70*/("""{"""),format.raw/*173.71*/("""
                    //Only re-mixing checked
                    remixBox.attr('checked', true);
                    localShareBox.attr("disabled", false);
                """),format.raw/*177.17*/("""}"""),format.raw/*177.18*/("""
                else if (licenseText == "Attribution-NonCommercial-ShareAlike") """),format.raw/*178.81*/("""{"""),format.raw/*178.82*/("""
                    //Only re-mixing and share-alike checked
                    remixBox.attr('checked', true);  
                    shareBox.attr('checked', true);
                    localShareBox.attr("disabled", false);
                """),format.raw/*183.17*/("""}"""),format.raw/*183.18*/(""" 
                else if (licenseText == "Attribution-ShareAlike") """),format.raw/*184.67*/("""{"""),format.raw/*184.68*/("""
                    //All checkboxes checked
                    remixBox.attr('checked', true);  
                    shareBox.attr('checked', true);
                    commBox.attr('checked', true);
                    localShareBox.attr("disabled", false);
                """),format.raw/*190.17*/("""}"""),format.raw/*190.18*/(""" 
                else if (licenseText == "Attribution") """),format.raw/*191.56*/("""{"""),format.raw/*191.57*/("""
                    //Only re-mixing and commercial checked
                    remixBox.attr('checked', true);  
                    commBox.attr('checked', true);
                    localShareBox.attr("disabled", false);
                """),format.raw/*196.17*/("""}"""),format.raw/*196.18*/(""" 
                
                $('input[name=licensedesc]').val(licenseText);
            """),format.raw/*199.13*/("""}"""),format.raw/*199.14*/("""                        
        """),format.raw/*200.9*/("""}"""),format.raw/*200.10*/("""
        
        //No matter what, must update the data on the UI. No submission though.
        //Reset the variables based on all the work that the ready function has done. (Primarily for the first license type)
        licenseType = $('input[name=type]:checked', '#form1').val();            
        rightsHolder = htmlEncode($('input[name=ownername]').val());
        licenseText = htmlEncode($('input[name=licensedesc]').val());
        licenseUrl = htmlEncode($('input[name=licenseurl]').val());
        allowDownload = $('input[name=allowDownload]').prop('checked');         
        var formImageBase = '"""),_display_(Seq[Any](/*209.31*/routes/*209.37*/.Assets.at("images"))),format.raw/*209.57*/("""';
        var authorName = """"),_display_(Seq[Any](/*210.28*/authorName)),format.raw/*210.38*/("""";
        updateInterface(licenseType, rightsHolder, licenseText, licenseUrl, allowDownload, formImageBase, authorName);
        
    """),format.raw/*213.5*/("""}"""),format.raw/*213.6*/(""");
	
	</script>
	
	<script type="text/javascript" language="javascript">			 
	  	  
	  var currentUserName = htmlDecode(""""),_display_(Seq[Any](/*219.39*/user/*219.43*/.get.fullName)),format.raw/*219.56*/(""""); 
	
	  //Handle the selection of the checkbox that makes the owner the current user
	  var cbox = $('#ownrights'); 
	  var rightsHolder = $('input[name=ownername]');
	  cbox.change( function() """),format.raw/*224.28*/("""{"""),format.raw/*224.29*/("""
		   
          if (cbox.is(':checked')) """),format.raw/*226.36*/("""{"""),format.raw/*226.37*/("""
        	  //disable the Rights Holder box and put the owner name into it
        	  rightsHolder.val(currentUserName);
        	  rightsHolder.attr("disabled", "disabled");
          """),format.raw/*230.11*/("""}"""),format.raw/*230.12*/("""
          else """),format.raw/*231.16*/("""{"""),format.raw/*231.17*/("""
        	  //enable the Rights Holder box and empty it
        	  rightsHolder.val('');
              rightsHolder.removeAttr("disabled");        	  
          """),format.raw/*235.11*/("""}"""),format.raw/*235.12*/("""
          
      """),format.raw/*237.7*/("""}"""),format.raw/*237.8*/(""");
	  
	  //Handle the de-select and select of remixing which needs to alter the status of share-alike
	  var localRemixBox = $('#remixing');
	  var localShareBox = $('#sharealike');
	  localRemixBox.change( function() """),format.raw/*242.37*/("""{"""),format.raw/*242.38*/("""
		 if (!localRemixBox.prop('checked')) """),format.raw/*243.40*/("""{"""),format.raw/*243.41*/("""
			 localShareBox.attr('checked', false);
			 localShareBox.attr("disabled", true);
		 """),format.raw/*246.4*/("""}"""),format.raw/*246.5*/(""" 
		 else """),format.raw/*247.9*/("""{"""),format.raw/*247.10*/("""
			 localShareBox.attr("disabled", false);
		 """),format.raw/*249.4*/("""}"""),format.raw/*249.5*/("""
	  """),format.raw/*250.4*/("""}"""),format.raw/*250.5*/(""");
	  
	  //Handle the change of selection for the license type 
	  $('input[name=type]').change( function() """),format.raw/*253.45*/("""{"""),format.raw/*253.46*/("""		  
		  var klass = '.' + $(this).val();
		  updateLicenseForm(klass);
	  """),format.raw/*256.4*/("""}"""),format.raw/*256.5*/(""");
	  
	  //Update the actual form elements to give the user the correct choices
	  function updateLicenseForm(klass) """),format.raw/*259.38*/("""{"""),format.raw/*259.39*/("""
          //var klass = '.' + $(this).val();
          $('.license1,.license2').addClass('hidden');
          $(klass).removeClass('hidden');
      """),format.raw/*263.7*/("""}"""),format.raw/*263.8*/("""
	  
	  function closeEdit() """),format.raw/*265.25*/("""{"""),format.raw/*265.26*/("""
		$("#editlicense").addClass('collapsed');
		$("#collapseSix").collapse('toggle');		
		return false;
	  """),format.raw/*269.4*/("""}"""),format.raw/*269.5*/("""
	  	  
	</script>		
	
""")))})),format.raw/*273.2*/("""
"""))}
    }
    
    def render(id:String,licenseData:models.LicenseData,sourceObject:String,authorName:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(id,licenseData,sourceObject,authorName)(user)
    
    def f:((String,models.LicenseData,String,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (id,licenseData,sourceObject,authorName) => (user) => apply(id,licenseData,sourceObject,authorName)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/licenseform.scala.html
                    HASH: ea5b70dd5238b6470903d4e4062a19b37204b93d
                    MATRIX: 646->1|864->124|1492->717|1519->735|1559->737|3307->2449|3332->2451|3374->2456|3390->2462|3433->2482|3475->2487|3510->2499|3552->2504|3585->2514|4216->3109|4229->3113|4264->3126|4337->3171|4366->3172|4777->3547|4797->3558|4833->3572|4912->3615|4932->3626|4969->3641|5048->3684|5068->3695|5104->3709|5182->3751|5202->3762|5237->3775|5307->3809|5327->3820|5365->3836|5533->3976|5562->3977|5719->4106|5748->4107|6277->4607|6307->4608|6354->4626|6384->4627|6543->4757|6573->4758|6610->4767|6640->4768|6691->4790|6721->4791|6834->4875|6864->4876|7050->5033|7080->5034|7127->5052|7157->5053|7345->5212|7375->5213|7455->5264|7485->5265|7566->5317|7596->5318|7690->5383|7720->5384|7762->5397|7792->5398|7863->5440|7893->5441|8012->5531|8042->5532|8152->5613|8182->5614|8374->5769|8407->5779|8551->5886|8584->5896|8635->5918|8665->5919|8755->5980|8785->5981|8909->6076|8939->6077|8990->6099|9020->6100|9070->6121|9100->6122|9155->6148|9185->6149|9381->6316|9411->6317|9457->6334|9487->6335|9545->6364|9575->6365|9743->6504|9773->6505|9878->6581|9908->6582|10031->6676|10061->6677|10112->6699|10142->6700|10255->6784|10285->6785|10471->6942|10501->6943|10621->7034|10651->7035|10702->7057|10732->7058|10853->7150|10883->7151|10941->7180|10971->7181|11061->7242|11091->7243|11529->7652|11559->7653|11760->7825|11790->7826|11889->7896|11919->7897|12121->8070|12151->8071|12261->8152|12291->8153|12563->8396|12593->8397|12690->8465|12720->8466|13027->8744|13057->8745|13143->8802|13173->8803|13443->9044|13473->9045|13596->9139|13626->9140|13687->9173|13717->9174|14368->9788|14384->9794|14427->9814|14494->9844|14527->9854|14690->9989|14719->9990|14878->10112|14892->10116|14928->10129|15153->10325|15183->10326|15254->10368|15284->10369|15498->10554|15528->10555|15573->10571|15603->10572|15793->10733|15823->10734|15869->10752|15898->10753|16146->10972|16176->10973|16245->11013|16275->11014|16391->11102|16420->11103|16458->11113|16488->11114|16563->11161|16592->11162|16624->11166|16653->11167|16791->11276|16821->11277|16924->11352|16953->11353|17100->11471|17130->11472|17307->11621|17336->11622|17394->11651|17424->11652|17557->11757|17586->11758|17642->11782
                    LINES: 20->1|23->1|38->16|38->16|38->16|83->61|83->61|83->61|83->61|83->61|83->61|83->61|83->61|83->61|98->76|98->76|98->76|101->79|101->79|106->84|106->84|106->84|107->85|107->85|107->85|108->86|108->86|108->86|109->87|109->87|109->87|110->88|110->88|110->88|114->92|114->92|116->94|116->94|123->101|123->101|124->102|124->102|127->105|127->105|128->106|128->106|129->107|129->107|132->110|132->110|135->113|135->113|136->114|136->114|139->117|139->117|141->119|141->119|142->120|142->120|144->122|144->122|145->123|145->123|146->124|146->124|147->125|147->125|148->126|148->126|150->128|150->128|152->130|152->130|152->130|152->130|153->131|153->131|155->133|155->133|156->134|156->134|157->135|157->135|158->136|158->136|161->139|161->139|162->140|162->140|163->141|163->141|166->144|166->144|167->145|167->145|169->147|169->147|170->148|170->148|172->150|172->150|176->154|176->154|178->156|178->156|179->157|179->157|181->159|181->159|182->160|182->160|183->161|183->161|190->168|190->168|194->172|194->172|195->173|195->173|199->177|199->177|200->178|200->178|205->183|205->183|206->184|206->184|212->190|212->190|213->191|213->191|218->196|218->196|221->199|221->199|222->200|222->200|231->209|231->209|231->209|232->210|232->210|235->213|235->213|241->219|241->219|241->219|246->224|246->224|248->226|248->226|252->230|252->230|253->231|253->231|257->235|257->235|259->237|259->237|264->242|264->242|265->243|265->243|268->246|268->246|269->247|269->247|271->249|271->249|272->250|272->250|275->253|275->253|278->256|278->256|281->259|281->259|285->263|285->263|287->265|287->265|291->269|291->269|295->273
                    -- GENERATED --
                */
            