
package views.html.collections

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[models.Collection,Call,Option[String],String,Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(collection: models.Collection, redirect: Call, space: Option[String], classes: String, showFollow: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters.ellipsize

import api.Permission

import play.api.i18n.Messages


Seq[Any](format.raw/*1.146*/("""
"""),format.raw/*5.1*/("""
<div class="post-box """),_display_(Seq[Any](/*6.23*/classes)),format.raw/*6.30*/("""" id=""""),_display_(Seq[Any](/*6.37*/collection/*6.47*/.id)),format.raw/*6.50*/("""-tile">
    <div class="panel panel-default collection-panel">
        <div class="pull-left">
            <span class="glyphicon glyphicon-th-large collection-background"></span>
        </div>
        <div class="panel-body">
            """),_display_(Seq[Any](/*12.14*/if(!collection.thumbnail_id.isEmpty)/*12.50*/{_display_(Seq[Any](format.raw/*12.51*/("""
                <a href=""""),_display_(Seq[Any](/*13.27*/(routes.Collections.collection(collection.id)))),format.raw/*13.73*/("""">
                    <img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*14.68*/(routes.Files.thumbnail(UUID(collection.thumbnail_id.toString().substring(5,collection.thumbnail_id.toString().length-1)))))),format.raw/*14.191*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*14.212*/Html(collection.name))),format.raw/*14.233*/("""">
                </a>
            """)))})),format.raw/*16.14*/("""
            <div class="caption break-word">
                <h4><a href=""""),_display_(Seq[Any](/*18.31*/(routes.Collections.collection(collection.id)))),format.raw/*18.77*/("""">"""),_display_(Seq[Any](/*18.80*/Html(collection.name))),format.raw/*18.101*/("""</a></h4>
                <p class="abstractsummary">"""),_display_(Seq[Any](/*19.45*/Html(collection.description.replace("\n","<br>")))),format.raw/*19.94*/("""</p>
            </div>
        </div>
            <!-- Collection Info -->
        <ul class="list-group">
            <li class="list-group-item collection-panel-footer">
                <span class="glyphicon glyphicon-briefcase hideTree" title=""""),_display_(Seq[Any](/*25.78*/collection/*25.88*/.datasetCount)),format.raw/*25.101*/(""" datasets"></span> """),_display_(Seq[Any](/*25.121*/collection/*25.131*/.datasetCount)),format.raw/*25.144*/("""
                <span class="glyphicon glyphicon-th-large" title=""""),_display_(Seq[Any](/*26.68*/collection/*26.78*/.childCollectionsCount)),format.raw/*26.100*/(""" child collections"></span> """),_display_(Seq[Any](/*26.129*/collection/*26.139*/.childCollectionsCount)),format.raw/*26.161*/("""
                """),_display_(Seq[Any](/*27.18*/if(user.isDefined)/*27.36*/ {_display_(Seq[Any](format.raw/*27.38*/("""
                    """),_display_(Seq[Any](/*28.22*/if(space.isDefined)/*28.41*/ {_display_(Seq[Any](format.raw/*28.43*/("""

                        """),_display_(Seq[Any](/*30.26*/if(user.get.id.equals(collection.author.id) || Permission.checkPermission(Permission.RemoveResourceFromSpace, ResourceRef(ResourceRef.space,UUID(space.get))))/*30.184*/ {_display_(Seq[Any](format.raw/*30.186*/("""
                            <button onclick="confirmRemoveResourceFromResource('space','"""),_display_(Seq[Any](/*31.90*/Messages("space.title"))),format.raw/*31.113*/("""','"""),_display_(Seq[Any](/*31.117*/(UUID(space.getOrElse(""))))),format.raw/*31.144*/("""','collection','"""),_display_(Seq[Any](/*31.161*/(collection.id))),format.raw/*31.176*/("""','"""),_display_(Seq[Any](/*31.180*/(collection.name.replace("'","&#39;")))),format.raw/*31.218*/("""',true,'"""),_display_(Seq[Any](/*31.227*/redirect/*31.235*/.url)),format.raw/*31.239*/("""')" class="btn btn-link" title="Remove the collection from the """),_display_(Seq[Any](/*31.303*/Messages("space.title"))),format.raw/*31.326*/("""">
                                <span class="glyphicon glyphicon-remove"></span></button>
                        """)))}/*33.27*/else/*33.32*/{_display_(Seq[Any](format.raw/*33.33*/("""
                            <div class="inline" title="No permission to remove the collection from the """),_display_(Seq[Any](/*34.105*/Messages("space.title"))),format.raw/*34.128*/(""".">
                                <button disabled class="btn btn-link"><span class="glyphicon glyphicon-remove"></span></button>
                            </div>
                        """)))})),format.raw/*37.26*/("""

                    """)))}/*39.23*/else/*39.28*/{_display_(Seq[Any](format.raw/*39.29*/("""
                        <!-- If user can delete, the button is enabled, otherwise the button is present but disabled to provide consistent UE. -->
                        """),_display_(Seq[Any](/*41.26*/if( user.get.id.equals(collection.author.id) || Permission.checkPermission(Permission.DeleteCollection, ResourceRef(ResourceRef.collection, collection.id)))/*41.182*/{_display_(Seq[Any](format.raw/*41.183*/("""
                            <button onclick="confirmDeleteResource('collection','collection','"""),_display_(Seq[Any](/*42.96*/(collection.id))),format.raw/*42.111*/("""','"""),_display_(Seq[Any](/*42.115*/(collection.name.replace("'","&#39;")))),format.raw/*42.153*/("""',false,'"""),_display_(Seq[Any](/*42.163*/redirect/*42.171*/.url)),format.raw/*42.175*/("""')" class="btn btn-link" title="Delete the collection but not its contents">
                                <span class="glyphicon glyphicon-trash"></span></button>
                        """)))}/*44.27*/else/*44.32*/{_display_(Seq[Any](format.raw/*44.33*/("""
                            <div class="inline" title="No permission to delete collection.">
                                <button disabled class="btn btn-link"><span class="glyphicon glyphicon-trash"></span></button>
                            </div>
                        """)))})),format.raw/*48.26*/("""
                    """)))})),format.raw/*49.22*/("""
                """)))})),format.raw/*50.18*/("""
            </li>
        </ul>
        """),_display_(Seq[Any](/*53.10*/if(showFollow)/*53.24*/ {_display_(Seq[Any](format.raw/*53.26*/("""
            <ul class="list-group center-margin">
            """),_display_(Seq[Any](/*55.14*/user/*55.18*/ match/*55.24*/ {/*56.17*/case Some(viewer) =>/*56.37*/ {_display_(Seq[Any](format.raw/*56.39*/("""
                    <a id="followButton" type="button" class="btn-link" data-toggle="button" autocomplete="off" objectType="collection" objectId=""""),_display_(Seq[Any](/*57.148*/collection/*57.158*/.id.stringify)),format.raw/*57.171*/("""">
                    """),_display_(Seq[Any](/*58.22*/if(viewer.followedEntities.filter(x => (x.id == collection.id)).nonEmpty)/*58.95*/ {_display_(Seq[Any](format.raw/*58.97*/("""
<!--                         <span class='glyphicon glyphicon-star-empty'></span>Unfollow
 -->                    """)))}/*60.27*/else/*60.32*/{_display_(Seq[Any](format.raw/*60.33*/("""
<!--                         <span class='glyphicon glyphicon-star'></span> Follow
 -->                    """)))})),format.raw/*62.26*/("""
                    </a>
                """)))}})),format.raw/*65.14*/("""
            </ul>
        """)))})),format.raw/*67.10*/("""
    </div>
</div>"""))}
    }
    
    def render(collection:models.Collection,redirect:Call,space:Option[String],classes:String,showFollow:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(collection,redirect,space,classes,showFollow)(user)
    
    def f:((models.Collection,Call,Option[String],String,Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (collection,redirect,space,classes,showFollow) => (user) => apply(collection,redirect,space,classes,showFollow)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collections/tile.scala.html
                    HASH: 2d2256c291c99a12059d88f375fe9334b734c2de
                    MATRIX: 664->1|997->145|1024->241|1082->264|1110->271|1152->278|1170->288|1194->291|1471->532|1516->568|1555->569|1618->596|1686->642|1792->712|1938->835|1996->856|2040->877|2109->914|2221->990|2289->1036|2328->1039|2372->1060|2462->1114|2533->1163|2819->1413|2838->1423|2874->1436|2931->1456|2951->1466|2987->1479|3091->1547|3110->1557|3155->1579|3221->1608|3241->1618|3286->1640|3340->1658|3367->1676|3407->1678|3465->1700|3493->1719|3533->1721|3596->1748|3764->1906|3805->1908|3931->1998|3977->2021|4018->2025|4068->2052|4122->2069|4160->2084|4201->2088|4262->2126|4308->2135|4326->2143|4353->2147|4454->2211|4500->2234|4637->2353|4650->2358|4689->2359|4831->2464|4877->2487|5101->2679|5143->2703|5156->2708|5195->2709|5404->2882|5570->3038|5610->3039|5742->3135|5780->3150|5821->3154|5882->3192|5929->3202|5947->3210|5974->3214|6184->3406|6197->3411|6236->3412|6549->3693|6603->3715|6653->3733|6731->3775|6754->3789|6794->3791|6894->3855|6907->3859|6922->3865|6933->3884|6962->3904|7002->3906|7187->4054|7207->4064|7243->4077|7303->4101|7385->4174|7425->4176|7560->4293|7573->4298|7612->4299|7753->4408|7829->4465|7889->4493
                    LINES: 20->1|28->1|29->5|30->6|30->6|30->6|30->6|30->6|36->12|36->12|36->12|37->13|37->13|38->14|38->14|38->14|38->14|40->16|42->18|42->18|42->18|42->18|43->19|43->19|49->25|49->25|49->25|49->25|49->25|49->25|50->26|50->26|50->26|50->26|50->26|50->26|51->27|51->27|51->27|52->28|52->28|52->28|54->30|54->30|54->30|55->31|55->31|55->31|55->31|55->31|55->31|55->31|55->31|55->31|55->31|55->31|55->31|55->31|57->33|57->33|57->33|58->34|58->34|61->37|63->39|63->39|63->39|65->41|65->41|65->41|66->42|66->42|66->42|66->42|66->42|66->42|66->42|68->44|68->44|68->44|72->48|73->49|74->50|77->53|77->53|77->53|79->55|79->55|79->55|79->56|79->56|79->56|80->57|80->57|80->57|81->58|81->58|81->58|83->60|83->60|83->60|85->62|87->65|89->67
                    -- GENERATED --
                */
            