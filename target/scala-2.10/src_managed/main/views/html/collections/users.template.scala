
package views.html.collections

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object users extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[models.Collection,Map[UUID, List[Tuple2[String, String]]],List[User],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(collection: models.Collection, userListSpaceRoleTupleMap: Map[UUID, List[Tuple2[String,String]]], users: List[User])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters._


Seq[Any](format.raw/*1.155*/("""
"""),format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*4.2*/main("Collection")/*4.20*/ {_display_(Seq[Any](format.raw/*4.22*/("""
    <div class="row">
        <ol class="breadcrumb">
            <li><span class="glyphicon glyphicon-th-large"></span><a href= """"),_display_(Seq[Any](/*7.78*/routes/*7.84*/.Collections.collection(collection.id))),format.raw/*7.122*/("""" title=""""),_display_(Seq[Any](/*7.132*/collection/*7.142*/.name)),format.raw/*7.147*/(""""> """),_display_(Seq[Any](/*7.151*/Html(ellipsize(collection.name, 18)))),format.raw/*7.187*/("""</a></li>
            <li><span class="glyphicon glyphicon-user"></span> Collaborators</li>
        </ol>
        <div class="row bottom-padding">
            <div class="col-md-12" id="ds-title">
                <h1 id="datasettitle">"""),_display_(Seq[Any](/*12.40*/collection/*12.50*/.name)),format.raw/*12.55*/("""</h1>
            </div>
        </div>

        <div class="col-md-12">
            """),_display_(Seq[Any](/*17.14*/if(users.isEmpty)/*17.31*/ {_display_(Seq[Any](format.raw/*17.33*/("""
                """),_display_(Seq[Any](/*18.18*/Html("No users with access to the collection"))),format.raw/*18.64*/("""
            """)))}/*19.15*/else/*19.20*/{_display_(Seq[Any](format.raw/*19.21*/("""
                <h3>Users with access to the collection</h3>

                <table id='user-space-role' class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="col-md-4">User</th>

                                <th>
                                    <table id='nested-space-role-headers' class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="col-md-6">"""),_display_(Seq[Any](/*31.71*/Messages("space.title"))),format.raw/*31.94*/("""</th>
                                                <th class="col-md-2">Role</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>

                        </tr>
                    </thead>
                    <tbody>
                        """),_display_(Seq[Any](/*41.26*/users/*41.31*/.filter(!_.fullName.isEmpty).map/*41.63*/ { u =>_display_(Seq[Any](format.raw/*41.70*/("""
                            <tr>
                                <td class="col-md-4">"""),_display_(Seq[Any](/*43.55*/u/*43.56*/.fullName)),format.raw/*43.65*/("""</td>
                                """),_display_(Seq[Any](/*44.34*/if(userListSpaceRoleTupleMap contains u.id)/*44.77*/ {_display_(Seq[Any](format.raw/*44.79*/("""
                                    <td>
                                        <table id='nested-space-role-pairs' class="table table-hover">
                                            <tbody>
                                                """),_display_(Seq[Any](/*48.50*/for(tupleSpaceRole <- userListSpaceRoleTupleMap(u.id)) yield /*48.104*/ {_display_(Seq[Any](format.raw/*48.106*/("""
                                                    <tr>
                                                        <td class="col-md-6">"""),_display_(Seq[Any](/*50.79*/tupleSpaceRole/*50.93*/._1)),format.raw/*50.96*/("""</td>
                                                        <td class="col-md-2">"""),_display_(Seq[Any](/*51.79*/tupleSpaceRole/*51.93*/._2)),format.raw/*51.96*/("""</td>
                                                    </tr>
                                                """)))})),format.raw/*53.50*/("""
                                            </tbody>
                                        </table>
                                    </td>
                                """)))})),format.raw/*57.34*/("""
                            </tr>
                        """)))})),format.raw/*59.26*/("""
                    </tbody>
                </table>
            """)))})),format.raw/*62.14*/("""
        </div>

    </div>
""")))})),format.raw/*66.2*/("""

"""))}
    }
    
    def render(collection:models.Collection,userListSpaceRoleTupleMap:Map[UUID, List[Tuple2[String, String]]],users:List[User],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(collection,userListSpaceRoleTupleMap,users)(user)
    
    def f:((models.Collection,Map[UUID, List[Tuple2[String, String]]],List[User]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (collection,userListSpaceRoleTupleMap,users) => (user) => apply(collection,userListSpaceRoleTupleMap,users)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:41 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collections/users.scala.html
                    HASH: 569dabd9514c96e9411df4fc3501ebccd615f87d
                    MATRIX: 681->1|961->154|988->188|1024->190|1050->208|1089->210|1256->342|1270->348|1330->386|1376->396|1395->406|1422->411|1462->415|1520->451|1792->687|1811->697|1838->702|1960->788|1986->805|2026->807|2080->825|2148->871|2181->886|2194->891|2233->892|2840->1463|2885->1486|3301->1866|3315->1871|3356->1903|3401->1910|3525->1998|3535->1999|3566->2008|3641->2047|3693->2090|3733->2092|4015->2338|4086->2392|4127->2394|4299->2530|4322->2544|4347->2547|4467->2631|4490->2645|4515->2648|4660->2761|4870->2939|4962->2999|5062->3067|5122->3096
                    LINES: 20->1|24->1|25->3|26->4|26->4|26->4|29->7|29->7|29->7|29->7|29->7|29->7|29->7|29->7|34->12|34->12|34->12|39->17|39->17|39->17|40->18|40->18|41->19|41->19|41->19|53->31|53->31|63->41|63->41|63->41|63->41|65->43|65->43|65->43|66->44|66->44|66->44|70->48|70->48|70->48|72->50|72->50|72->50|73->51|73->51|73->51|75->53|79->57|81->59|84->62|88->66
                    -- GENERATED --
                */
            