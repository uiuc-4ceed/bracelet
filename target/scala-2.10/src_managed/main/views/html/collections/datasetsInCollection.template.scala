
package views.html.collections

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object datasetsInCollection extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template8[List[Dataset],Map[UUID, Int],UUID,Int,Int,List[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(datasetsList: List[Dataset], commentMap:  Map[UUID, Int], collectionId: UUID, prev: Int, next: Int,
        userSelections: List[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*2.101*/("""

"""),format.raw/*5.1*/("""
"""),_display_(Seq[Any](/*6.2*/datasetsList/*6.14*/.map/*6.18*/ { dataset =>_display_(Seq[Any](format.raw/*6.31*/("""
    """),_display_(Seq[Any](/*7.6*/if(userSelections.indexOf(dataset.id.toString()) != -1)/*7.61*/ {_display_(Seq[Any](format.raw/*7.63*/("""
        """),_display_(Seq[Any](/*8.10*/datasets/*8.18*/.listitem(dataset,Some(collectionId.uuid),commentMap,routes.Collections.collection(collectionId), true))),format.raw/*8.121*/("""
    """)))}/*9.7*/else/*9.12*/{_display_(Seq[Any](format.raw/*9.13*/("""
        """),_display_(Seq[Any](/*10.10*/datasets/*10.18*/.listitem(dataset,Some(collectionId.uuid),commentMap,routes.Collections.collection(collectionId), false))),format.raw/*10.122*/("""
    """)))})),format.raw/*11.6*/("""
""")))})),format.raw/*12.2*/("""
"""),_display_(Seq[Any](/*13.2*/if(datasetsList.isEmpty)/*13.26*/{_display_(Seq[Any](format.raw/*13.27*/("""
    """),_display_(Seq[Any](/*14.6*/if(Permission.checkPermission(Permission.AddResourceToCollection, ResourceRef(ResourceRef.collection, collectionId)))/*14.123*/{_display_(Seq[Any](format.raw/*14.124*/("""
        There are no datasets in this collection.
    """)))}/*16.7*/else/*16.12*/{_display_(Seq[Any](format.raw/*16.13*/("""
        There are no datasets in this collection and not enough permission to edit this collection.
    """)))})),format.raw/*18.6*/("""

""")))})),format.raw/*20.2*/("""

<div class="row">
    <div class="col-md-12">
        <ul class="pager">
                <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
            """),_display_(Seq[Any](/*26.14*/if(prev >= 0)/*26.27*/ {_display_(Seq[Any](format.raw/*26.29*/("""
                <li class="previous"><a id="prevlink-ds" title="Page backwards" href="javascript:updateDatasets("""),_display_(Seq[Any](/*27.114*/prev)),format.raw/*27.118*/(""")"><span class="glyphicon glyphicon-chevron-left"></span>Previous</a></li>
            """)))})),format.raw/*28.14*/("""
            """),_display_(Seq[Any](/*29.14*/if(next >= 0)/*29.27*/ {_display_(Seq[Any](format.raw/*29.29*/("""
                <li class ="next"><a id="nextlink-ds" title="Page forwards" href="javascript:updateDatasets("""),_display_(Seq[Any](/*30.110*/next)),format.raw/*30.114*/(""")">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
            """)))})),format.raw/*31.14*/("""
        </ul>
    </div>
</div>
"""))}
    }
    
    def render(datasetsList:List[Dataset],commentMap:Map[UUID, Int],collectionId:UUID,prev:Int,next:Int,userSelections:List[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(datasetsList,commentMap,collectionId,prev,next,userSelections)(flash,user)
    
    def f:((List[Dataset],Map[UUID, Int],UUID,Int,Int,List[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (datasetsList,commentMap,collectionId,prev,next,userSelections) => (flash,user) => apply(datasetsList,commentMap,collectionId,prev,next,userSelections)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:41 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collections/datasetsInCollection.scala.html
                    HASH: 2add200ec0f0c4f7fcccee8535ffaeca145395a9
                    MATRIX: 701->1|1019->202|1047->227|1083->229|1103->241|1115->245|1165->258|1205->264|1268->319|1307->321|1352->331|1368->339|1493->442|1516->449|1528->454|1566->455|1612->465|1629->473|1756->577|1793->583|1826->585|1863->587|1896->611|1935->612|1976->618|2103->735|2143->736|2217->793|2230->798|2269->799|2406->905|2440->908|2702->1134|2724->1147|2764->1149|2915->1263|2942->1267|3062->1355|3112->1369|3134->1382|3174->1384|3321->1494|3348->1498|3466->1584
                    LINES: 20->1|25->2|27->5|28->6|28->6|28->6|28->6|29->7|29->7|29->7|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|33->11|34->12|35->13|35->13|35->13|36->14|36->14|36->14|38->16|38->16|38->16|40->18|42->20|48->26|48->26|48->26|49->27|49->27|50->28|51->29|51->29|51->29|52->30|52->30|53->31
                    -- GENERATED --
                */
            