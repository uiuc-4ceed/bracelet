
package views.html.collections

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listchildcollection extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[models.Collection,models.Collection,Call,play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(parent: models.Collection, collection: models.Collection, redirect: Call)(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.139*/("""
"""),format.raw/*3.1*/("""<div class="panel panel-default collection-panel" id=""""),_display_(Seq[Any](/*3.56*/collection/*3.66*/.id)),format.raw/*3.69*/("""-listitem">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-2">
                """),_display_(Seq[Any](/*7.18*/if(!collection.thumbnail_id.isEmpty)/*7.54*/{_display_(Seq[Any](format.raw/*7.55*/("""
                    <img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*8.68*/(routes.Files.thumbnail(UUID(collection.thumbnail_id.toString().substring(5,collection.thumbnail_id.toString().length-1)))))),format.raw/*8.191*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*8.212*/Html(collection.name))),format.raw/*8.233*/("""">
                """)))}/*9.19*/else/*9.24*/{_display_(Seq[Any](format.raw/*9.25*/("""
                    <a href=""""),_display_(Seq[Any](/*10.31*/routes/*10.37*/.Collections.collection(collection.id))),format.raw/*10.75*/("""">
                        <span class="bigicon glyphicon glyphicon-th-large"></span>
                    </a>
                """)))})),format.raw/*13.18*/("""
            </div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-xs-12">
                        <h3><a href=""""),_display_(Seq[Any](/*18.39*/(routes.Collections.collection(collection.id)))),format.raw/*18.85*/("""">"""),_display_(Seq[Any](/*18.88*/Html(collection.name))),format.raw/*18.109*/("""</a></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="row">
                            <div class="col-xs-12 abstractsummary">"""),_display_(Seq[Any](/*24.69*/Html(collection.description.replace("\n","<br>")))),format.raw/*24.118*/("""</div>
                        </div>
                        <div class="row top-padding">
                            <div class="col-xs-12">
                                """),_display_(Seq[Any](/*28.34*/Messages("owner.label"))),format.raw/*28.57*/(""": <a href= """"),_display_(Seq[Any](/*28.70*/routes/*28.76*/.Profile.viewProfileUUID(collection.author.id))),format.raw/*28.122*/(""""> """),_display_(Seq[Any](/*28.126*/collection/*28.136*/.author.fullName)),format.raw/*28.152*/(""" </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">Created on """),_display_(Seq[Any](/*32.64*/collection/*32.74*/.created.format("MMM dd, yyyy"))),format.raw/*32.105*/("""</div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        """),_display_(Seq[Any](/*36.26*/if(user.isDefined)/*36.44*/ {_display_(Seq[Any](format.raw/*36.46*/("""
                            """),_display_(Seq[Any](/*37.30*/if(!collection.followers.contains(user.get.id))/*37.77*/ {_display_(Seq[Any](format.raw/*37.79*/("""
                                <div>
<!--                                     <a id="followButton"
                                        class="btn btn-link"
                                        objectId=""""),_display_(Seq[Any](/*41.52*/collection/*41.62*/.id)),format.raw/*41.65*/(""""
                                        objectName=""""),_display_(Seq[Any](/*42.54*/collection/*42.64*/.name)),format.raw/*42.69*/(""""
                                        objectType="collection">
                                            <span class='glyphicon glyphicon-star'></span> Follow</a>
 -->                                </div>
                            """)))}/*46.31*/else/*46.36*/{_display_(Seq[Any](format.raw/*46.37*/("""
                                <div>
<!--                                     <a id="followButton"
                                        class="btn btn-link"
                                        objectId=""""),_display_(Seq[Any](/*50.52*/collection/*50.62*/.id)),format.raw/*50.65*/(""""
                                        objectName=""""),_display_(Seq[Any](/*51.54*/collection/*51.64*/.name)),format.raw/*51.69*/(""""
                                        objectType="collection">
                                            <span class='glyphicon glyphicon-star-empty'></span> Unfollow</a>
 -->                                </div>
                            """)))})),format.raw/*55.30*/("""
                            <!-- If user can delete, the button is enabled, otherwise the button is present but disabled to provide consistent UE. -->
                            """),_display_(Seq[Any](/*57.30*/if( user.get.id.equals(collection.author.id) || Permission.checkPermission(Permission.RemoveResourceFromCollection, ResourceRef(ResourceRef.collection, collection.id)))/*57.198*/{_display_(Seq[Any](format.raw/*57.199*/("""
                                <button onclick="confirmRemoveResourceFromResource('collection','collection','"""),_display_(Seq[Any](/*58.112*/(parent.id))),format.raw/*58.123*/("""','collection','"""),_display_(Seq[Any](/*58.140*/(collection.id))),format.raw/*58.155*/("""','"""),_display_(Seq[Any](/*58.159*/(collection.name))),format.raw/*58.176*/("""',true,'"""),_display_(Seq[Any](/*58.185*/(redirect))),format.raw/*58.195*/("""')"
                                class="btn btn-link" title="Remove the child collection from the collection """),_display_(Seq[Any](/*59.110*/parent/*59.116*/.name)),format.raw/*59.121*/("""">
                                    <span class="glyphicon glyphicon-remove"></span> Remove</button>
                            """)))}/*61.31*/else/*61.36*/{_display_(Seq[Any](format.raw/*61.37*/("""
                                <div class="inline" title="No permission to delete the child collection">
                                    <button class="btn btn-link btn-sm disabled"><span class="glyphicon glyphicon-remove"></span> Remove</button>
                                </div>
                            """)))})),format.raw/*65.30*/("""
                        """)))})),format.raw/*66.26*/("""
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>"""))}
    }
    
    def render(parent:models.Collection,collection:models.Collection,redirect:Call,flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(parent,collection,redirect)(flash,user)
    
    def f:((models.Collection,models.Collection,Call) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (parent,collection,redirect) => (flash,user) => apply(parent,collection,redirect)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collections/listchildcollection.scala.html
                    HASH: 53669d4ef35c831c4860f65cbcec09736f210c84
                    MATRIX: 686->1|940->138|967->162|1057->217|1075->227|1099->230|1253->349|1297->385|1335->386|1438->454|1583->577|1640->598|1683->619|1721->640|1733->645|1771->646|1838->677|1853->683|1913->721|2073->849|2281->1021|2349->1067|2388->1070|2432->1091|2715->1338|2787->1387|3000->1564|3045->1587|3094->1600|3109->1606|3178->1652|3219->1656|3239->1666|3278->1682|3491->1859|3510->1869|3564->1900|3733->2033|3760->2051|3800->2053|3866->2083|3922->2130|3962->2132|4211->2345|4230->2355|4255->2358|4346->2413|4365->2423|4392->2428|4652->2670|4665->2675|4704->2676|4953->2889|4972->2899|4997->2902|5088->2957|5107->2967|5134->2972|5415->3221|5632->3402|5810->3570|5850->3571|5999->3683|6033->3694|6087->3711|6125->3726|6166->3730|6206->3747|6252->3756|6285->3766|6435->3879|6451->3885|6479->3890|6631->4024|6644->4029|6683->4030|7036->4351|7094->4377
                    LINES: 20->1|24->1|25->3|25->3|25->3|25->3|29->7|29->7|29->7|30->8|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|35->13|40->18|40->18|40->18|40->18|46->24|46->24|50->28|50->28|50->28|50->28|50->28|50->28|50->28|50->28|54->32|54->32|54->32|58->36|58->36|58->36|59->37|59->37|59->37|63->41|63->41|63->41|64->42|64->42|64->42|68->46|68->46|68->46|72->50|72->50|72->50|73->51|73->51|73->51|77->55|79->57|79->57|79->57|80->58|80->58|80->58|80->58|80->58|80->58|80->58|80->58|81->59|81->59|81->59|83->61|83->61|83->61|87->65|88->66
                    -- GENERATED --
                */
            