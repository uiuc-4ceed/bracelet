
package views.html.collections

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object childCollections extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[List[Collection],Collection,Int,Int,play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(childCollections: List[Collection], collection: Collection, prev: Int, next: Int)(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.147*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/childCollections/*5.18*/.map/*5.22*/ { child_collection =>_display_(Seq[Any](format.raw/*5.44*/("""
    """),_display_(Seq[Any](/*6.6*/collections/*6.17*/.listchildcollection(collection, child_collection, routes.Collections.collection(collection.id)))),format.raw/*6.113*/("""
""")))})),format.raw/*7.2*/("""
"""),_display_(Seq[Any](/*8.2*/if(childCollections.isEmpty)/*8.30*/{_display_(Seq[Any](format.raw/*8.31*/("""
    """),_display_(Seq[Any](/*9.6*/if(Permission.checkPermission(Permission.AddResourceToCollection, ResourceRef(ResourceRef.collection, collection.id)))/*9.124*/{_display_(Seq[Any](format.raw/*9.125*/("""
        There are no child collections in this collection.
    """)))}/*11.7*/else/*11.12*/{_display_(Seq[Any](format.raw/*11.13*/("""
        There are no child collections in this collection and not enough permission to edit this collection.
    """)))})),format.raw/*13.6*/("""
""")))})),format.raw/*14.2*/("""

<div class="row">
    <div class="col-md-12">
        <ul class="pager">
                <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
            """),_display_(Seq[Any](/*20.14*/if(prev >= 0)/*20.27*/ {_display_(Seq[Any](format.raw/*20.29*/("""
                <li class="previous"><a id="prevlink-cc" title="Page backwards" href="javascript:updateChildCollections("""),_display_(Seq[Any](/*21.122*/prev)),format.raw/*21.126*/(""")"><span class="glyphicon glyphicon-chevron-left"></span>Previous</a></li>
            """)))})),format.raw/*22.14*/("""
            """),_display_(Seq[Any](/*23.14*/if(next >= 0)/*23.27*/ {_display_(Seq[Any](format.raw/*23.29*/("""
                <li class ="next"><a id="nextlink-cc" title="Page forwards" href="javascript:updateChildCollections("""),_display_(Seq[Any](/*24.118*/next)),format.raw/*24.122*/(""")">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
            """)))})),format.raw/*25.14*/("""
        </ul>
    </div>
</div>"""))}
    }
    
    def render(childCollections:List[Collection],collection:Collection,prev:Int,next:Int,flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(childCollections,collection,prev,next)(flash,user)
    
    def f:((List[Collection],Collection,Int,Int) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (childCollections,collection,prev,next) => (flash,user) => apply(childCollections,collection,prev,next)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collections/childCollections.scala.html
                    HASH: 424646a05ff7416d0a9808a829eede2a944177f0
                    MATRIX: 678->1|940->146|968->171|1004->173|1028->189|1040->193|1099->215|1139->221|1158->232|1276->328|1308->330|1344->332|1380->360|1418->361|1458->367|1585->485|1624->486|1707->552|1720->557|1759->558|1905->673|1938->675|2200->901|2222->914|2262->916|2421->1038|2448->1042|2568->1130|2618->1144|2640->1157|2680->1159|2835->1277|2862->1281|2980->1367
                    LINES: 20->1|24->1|26->4|27->5|27->5|27->5|27->5|28->6|28->6|28->6|29->7|30->8|30->8|30->8|31->9|31->9|31->9|33->11|33->11|33->11|35->13|36->14|42->20|42->20|42->20|43->21|43->21|44->22|45->23|45->23|45->23|46->24|46->24|47->25
                    -- GENERATED --
                */
            