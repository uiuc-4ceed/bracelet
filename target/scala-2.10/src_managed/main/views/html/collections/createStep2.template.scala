
package views.html.collections

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object createStep2 extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply():play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import _root_.util.Formatters._


Seq[Any](format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*4.2*/main("Collection Created")/*4.28*/ {_display_(Seq[Any](format.raw/*4.30*/("""

    <div class="page-header">
        <h1> Collection Successfully Created</h1>
    </div>

    <div class="row message-row">
        <div class="col-md-12">
            <p>You may go back to the home screen or close the window to refresh the tree and view your new collection!
            </p>
        </div>
    </div>
""")))})),format.raw/*16.2*/("""
"""))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:50 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collections/createStep2.scala.html
                    HASH: b973d7ce953fa7f3b1d6bfe9e47fc9114873f29a
                    MATRIX: 749->64|785->66|819->92|858->94|1213->418
                    LINES: 26->3|27->4|27->4|27->4|39->16
                    -- GENERATED --
                */
            