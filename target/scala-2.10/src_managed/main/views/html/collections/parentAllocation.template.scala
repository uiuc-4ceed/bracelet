
package views.html.collections

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object parentAllocation extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[models.Collection,UUID,Symbol,List[models.Collection],Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(currentCollection: models.Collection, resourceId: UUID, resourceType: Symbol, parentCollections: List[models.Collection], canAddToParent: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.libs.json.Json


Seq[Any](format.raw/*1.185*/("""
"""),format.raw/*4.1*/("""    <div class="row bottom-padding">
        <div class="col-md-12">
            <h4>Parent collections</h4>
        </div>
    </div>

    <div class="row">
        <div id="collectionsList" class="col-md-12">
        """),_display_(Seq[Any](/*12.10*/parentCollections/*12.27*/.map/*12.31*/ { p =>_display_(Seq[Any](format.raw/*12.38*/("""
            <div id="col_"""),_display_(Seq[Any](/*13.27*/p/*13.28*/.id)),format.raw/*13.31*/("""" class="row bottom-padding">
                <div class="col-xs-2">
                    <a href=""""),_display_(Seq[Any](/*15.31*/routes/*15.37*/.Collections.collection(p.id))),format.raw/*15.66*/("""">
                        <span class="smallicon glyphicon glyphicon-th-large"></span>
                    </a>
                </div>
                <div class="col-xs-10">
                    <div>
                        <a href=""""),_display_(Seq[Any](/*21.35*/routes/*21.41*/.Collections.collection(p.id))),format.raw/*21.70*/("""" id='"""),_display_(Seq[Any](/*21.77*/p/*21.78*/.id)),format.raw/*21.81*/("""' class ="collection">"""),_display_(Seq[Any](/*21.104*/Html(p.name))),format.raw/*21.116*/("""</a>
                    </div>
                    <div>
                    """),_display_(Seq[Any](/*24.22*/if(p.childCollectionsCount ==1)/*24.53*/{_display_(Seq[Any](format.raw/*24.54*/("""
                        1 collection
                    """)))}/*26.23*/else/*26.28*/{_display_(Seq[Any](format.raw/*26.29*/("""
                        """),_display_(Seq[Any](/*27.26*/p/*27.27*/.childCollectionsCount)),format.raw/*27.49*/(""" collections
                    """)))})),format.raw/*28.22*/("""
                    </div>
                </div>
            </div>
        """)))})),format.raw/*32.10*/("""
        </div>
    </div>



    <!--The owner of the collection and users with the AddResourceToCollection permission in at least one of the spaces the collection is part of can add a collection to a parent collection. -->
    """),_display_(Seq[Any](/*39.6*/if(canAddToParent)/*39.24*/ {_display_(Seq[Any](format.raw/*39.26*/("""
        <div class="form-inline">
            <div class="input-group input-group-sm col-md-8">
                <select id="collectionAddSelect" class="form-control add-resource">
                </select>
                    <span class="input-group-btn">
                    <a href="#" class="btn btn-default btn-large" id="addParentBtn" title="Add collection to parent" onclick="addCollectionToParentCollection('"""),_display_(Seq[Any](/*45.161*/currentCollection/*45.178*/.id)),format.raw/*45.181*/("""',event)">
                        <span class="glyphicon glyphicon-plus"></span> Add
                    </a>
                    </span>

            </div>
        </div>
    """)))})),format.raw/*52.6*/("""


<link rel="stylesheet" href=""""),_display_(Seq[Any](/*55.31*/routes/*55.37*/.Assets.at("stylesheets/chosen.css"))),format.raw/*55.73*/("""">
<script src=""""),_display_(Seq[Any](/*56.15*/routes/*56.21*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*56.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*57.15*/routes/*57.21*/.Assets.at("javascripts/collectionModify.js"))),format.raw/*57.66*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*58.15*/routes/*58.21*/.Assets.at("javascripts/datasets/collections.js"))),format.raw/*58.70*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*59.15*/routes/*59.21*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*59.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*60.15*/routes/*60.21*/.Assets.at("javascripts/collectionChildCollectionsList.js"))),format.raw/*60.80*/("""" type="text/javascript"></script>

<script language="javascript">
    $("#collectionAddSelect").select2("""),format.raw/*63.39*/("""{"""),format.raw/*63.40*/("""
        theme: "bootstrap",
        placeholder: "Select a Collection",
        allowClear: true,
        ajax: """),format.raw/*67.15*/("""{"""),format.raw/*67.16*/("""
            url: function(params) """),format.raw/*68.35*/("""{"""),format.raw/*68.36*/("""
                return jsRoutes.api.Collections.listPossibleParents( '"""),_display_(Seq[Any](/*69.72*/resourceId)),format.raw/*69.82*/("""', params.term,null, 5).url;
            """),format.raw/*70.13*/("""}"""),format.raw/*70.14*/(""",
            data: function(params) """),format.raw/*71.36*/("""{"""),format.raw/*71.37*/("""
                return """),format.raw/*72.24*/("""{"""),format.raw/*72.25*/(""" title: params.term """),format.raw/*72.45*/("""}"""),format.raw/*72.46*/(""";
            """),format.raw/*73.13*/("""}"""),format.raw/*73.14*/(""",
            processResults: function(data, page) """),format.raw/*74.50*/("""{"""),format.raw/*74.51*/("""
                return """),format.raw/*75.24*/("""{"""),format.raw/*75.25*/("""results: data.filter(function(x) """),format.raw/*75.58*/("""{"""),format.raw/*75.59*/("""
                    var ids = $('.collection').map(function() """),format.raw/*76.63*/("""{"""),format.raw/*76.64*/("""
                        return $(this).attr('id');
                    """),format.raw/*78.21*/("""}"""),format.raw/*78.22*/(""");
                    return $.inArray(x["id"], ids) == -1;
                """),format.raw/*80.17*/("""}"""),format.raw/*80.18*/(""").map(function(x) """),format.raw/*80.36*/("""{"""),format.raw/*80.37*/("""
                    return """),format.raw/*81.28*/("""{"""),format.raw/*81.29*/("""
                        text: x["collectionname"],
                        id: x["id"]
                    """),format.raw/*84.21*/("""}"""),format.raw/*84.22*/("""
                """),format.raw/*85.17*/("""}"""),format.raw/*85.18*/(""")"""),format.raw/*85.19*/("""}"""),format.raw/*85.20*/(""";
            """),format.raw/*86.13*/("""}"""),format.raw/*86.14*/("""
        """),format.raw/*87.9*/("""}"""),format.raw/*87.10*/("""
    """),format.raw/*88.5*/("""}"""),format.raw/*88.6*/(""");

</script>"""))}
    }
    
    def render(currentCollection:models.Collection,resourceId:UUID,resourceType:Symbol,parentCollections:List[models.Collection],canAddToParent:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(currentCollection,resourceId,resourceType,parentCollections,canAddToParent)(user)
    
    def f:((models.Collection,UUID,Symbol,List[models.Collection],Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (currentCollection,resourceId,resourceType,parentCollections,canAddToParent) => (user) => apply(currentCollection,resourceId,resourceType,parentCollections,canAddToParent)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collections/parentAllocation.scala.html
                    HASH: 2a535175865803a43e008ed84b66c3b528934861
                    MATRIX: 685->1|1017->184|1044->240|1300->460|1326->477|1339->481|1384->488|1447->515|1457->516|1482->519|1617->618|1632->624|1683->653|1955->889|1970->895|2021->924|2064->931|2074->932|2099->935|2159->958|2194->970|2309->1049|2349->1080|2388->1081|2466->1141|2479->1146|2518->1147|2580->1173|2590->1174|2634->1196|2700->1230|2811->1309|3076->1539|3103->1557|3143->1559|3598->1977|3625->1994|3651->1997|3861->2176|3930->2209|3945->2215|4003->2251|4056->2268|4071->2274|4135->2316|4220->2365|4235->2371|4302->2416|4387->2465|4402->2471|4473->2520|4558->2569|4573->2575|4635->2615|4720->2664|4735->2670|4816->2729|4949->2834|4978->2835|5119->2948|5148->2949|5211->2984|5240->2985|5348->3057|5380->3067|5449->3108|5478->3109|5543->3146|5572->3147|5624->3171|5653->3172|5701->3192|5730->3193|5772->3207|5801->3208|5880->3259|5909->3260|5961->3284|5990->3285|6051->3318|6080->3319|6171->3382|6200->3383|6300->3455|6329->3456|6434->3533|6463->3534|6509->3552|6538->3553|6594->3581|6623->3582|6759->3690|6788->3691|6833->3708|6862->3709|6891->3710|6920->3711|6962->3725|6991->3726|7027->3735|7056->3736|7088->3741|7116->3742
                    LINES: 20->1|26->1|27->4|35->12|35->12|35->12|35->12|36->13|36->13|36->13|38->15|38->15|38->15|44->21|44->21|44->21|44->21|44->21|44->21|44->21|44->21|47->24|47->24|47->24|49->26|49->26|49->26|50->27|50->27|50->27|51->28|55->32|62->39|62->39|62->39|68->45|68->45|68->45|75->52|78->55|78->55|78->55|79->56|79->56|79->56|80->57|80->57|80->57|81->58|81->58|81->58|82->59|82->59|82->59|83->60|83->60|83->60|86->63|86->63|90->67|90->67|91->68|91->68|92->69|92->69|93->70|93->70|94->71|94->71|95->72|95->72|95->72|95->72|96->73|96->73|97->74|97->74|98->75|98->75|98->75|98->75|99->76|99->76|101->78|101->78|103->80|103->80|103->80|103->80|104->81|104->81|107->84|107->84|108->85|108->85|108->85|108->85|109->86|109->86|110->87|110->87|111->88|111->88
                    -- GENERATED --
                */
            