
package views.html.collections

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listitem extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[models.Collection,Call,play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(collection: models.Collection, redirect: Call)(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.112*/("""
"""),format.raw/*3.1*/("""<div class="panel panel-default collection-panel" id=""""),_display_(Seq[Any](/*3.56*/collection/*3.66*/.id)),format.raw/*3.69*/("""-listitem">
    """),_display_(Seq[Any](/*4.6*/if(!collection.thumbnail_id.isEmpty)/*4.42*/ {_display_(Seq[Any](format.raw/*4.44*/("""
        <div class="row">
            <div class="pull-left col-xs-12">
                <span class="glyphicon glyphicon-th-large collection-background"></span>
            </div>
        </div>
    """)))})),format.raw/*10.6*/("""
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-lg-2">
            """),_display_(Seq[Any](/*14.14*/if(!collection.thumbnail_id.isEmpty)/*14.50*/{_display_(Seq[Any](format.raw/*14.51*/("""
                <a href=""""),_display_(Seq[Any](/*15.27*/(routes.Collections.collection(collection.id)))),format.raw/*15.73*/("""">
                    <img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*16.68*/(routes.Files.thumbnail(UUID(collection.thumbnail_id.toString().substring(5,collection.thumbnail_id.toString().length-1)))))),format.raw/*16.191*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*16.212*/Html(collection.name))),format.raw/*16.233*/("""">
                </a>
            """)))}/*18.15*/else/*18.20*/{_display_(Seq[Any](format.raw/*18.21*/("""
                <a href=""""),_display_(Seq[Any](/*19.27*/(routes.Collections.collection(collection.id)))),format.raw/*19.73*/("""">
                    <span class="bigicon glyphicon glyphicon-th-large collection-background"></span>
                </a>
            """)))})),format.raw/*22.14*/("""
            </div>
            <div class="col-md-10 col-sm-10 col-lg-10">
                <h3><a href=""""),_display_(Seq[Any](/*25.31*/(routes.Collections.collection(collection.id)))),format.raw/*25.77*/("""">"""),_display_(Seq[Any](/*25.80*/Html(collection.name))),format.raw/*25.101*/("""</a></h3>
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <ul class="list-unstyled">
                            <li class="abstractsummary">"""),_display_(Seq[Any](/*29.58*/Html(collection.description))),format.raw/*29.86*/("""</li>
                            <li>"""),_display_(Seq[Any](/*30.34*/collection/*30.44*/.created.format("MMM dd, yyyy"))),format.raw/*30.75*/("""</li>
                            <li>
                                <span class="glyphicon glyphicon-briefcase hideTree" title=""""),_display_(Seq[Any](/*32.94*/collection/*32.104*/.datasetCount)),format.raw/*32.117*/(""" datasets"></span> """),_display_(Seq[Any](/*32.137*/collection/*32.147*/.datasetCount)),format.raw/*32.160*/("""
                                <span class="glyphicon glyphicon-th-large" title=""""),_display_(Seq[Any](/*33.84*/collection/*33.94*/.childCollectionsCount)),format.raw/*33.116*/(""" child collections"></span> """),_display_(Seq[Any](/*33.145*/collection/*33.155*/.childCollectionsCount)),format.raw/*33.177*/("""
                                """),_display_(Seq[Any](/*34.34*/if(user.isDefined)/*34.52*/ {_display_(Seq[Any](format.raw/*34.54*/("""
                                        <!-- If user can delete, the button is enabled, otherwise the button is present but disabled to provide consistent UE. -->
                                    """),_display_(Seq[Any](/*36.38*/if( user.get.id.equals(collection.author.id) || Permission.checkPermission(Permission.DeleteCollection, ResourceRef(ResourceRef.collection, collection.id)))/*36.194*/{_display_(Seq[Any](format.raw/*36.195*/("""
                                        <button onclick="confirmDeleteResource('collection','collection','"""),_display_(Seq[Any](/*37.108*/(collection.id))),format.raw/*37.123*/("""','"""),_display_(Seq[Any](/*37.127*/(collection.name.replace("'","&#39;")))),format.raw/*37.165*/("""',false, '"""),_display_(Seq[Any](/*37.176*/redirect)),format.raw/*37.184*/("""')" class="btn btn-link" title="Delete the collection but not its contents">
                                            <span class="glyphicon glyphicon-trash"></span></button>
                                    """)))}/*39.39*/else/*39.44*/{_display_(Seq[Any](format.raw/*39.45*/("""
                                        <div class="inline" title="No permission to delete the collection">
                                            <button disabled class="btn btn-link"><span class="glyphicon glyphicon-trash"></span></button>
                                        </div>
                                    """)))})),format.raw/*43.38*/("""
                                """)))})),format.raw/*44.34*/("""
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4">
                        <ul class="list-unstyled">
                            <li>"""),_display_(Seq[Any](/*50.34*/if(user.isDefined && !collection.spaces.isEmpty)/*50.82*/{_display_(Seq[Any](format.raw/*50.83*/("""
                                <div id="collection-users">
                                    <a href=""""),_display_(Seq[Any](/*52.47*/routes/*52.53*/.Collections.users(collection.id))),format.raw/*52.86*/("""" class="btn btn-link ">
                                        <span class="glyphicon glyphicon-hand-right"></span> Users with access to the collection
                                    </a>
                                    <div class="alert-danger inner-item">
                                        """),_display_(Seq[Any](/*56.42*/flash/*56.47*/.get("error"))),format.raw/*56.60*/("""
                                    </div>
                                </div>
                            """)))})),format.raw/*59.30*/("""</li>
                            <li>
                            """),_display_(Seq[Any](/*61.30*/if(user.isDefined)/*61.48*/ {_display_(Seq[Any](format.raw/*61.50*/("""
                                """),_display_(Seq[Any](/*62.34*/if(!collection.followers.contains(user.get.id))/*62.81*/ {_display_(Seq[Any](format.raw/*62.83*/("""
<!--                                     <button id="followButton"
                                        class="btn btn-link"
                                        objectId=""""),_display_(Seq[Any](/*65.52*/collection/*65.62*/.id)),format.raw/*65.65*/(""""
                                        objectName=""""),_display_(Seq[Any](/*66.54*/collection/*66.64*/.name)),format.raw/*66.69*/(""""
                                        objectType="collection">
                                            <span class='glyphicon glyphicon-star'></span> Follow
                                    </button>
 -->                                """)))}/*70.39*/else/*70.44*/{_display_(Seq[Any](format.raw/*70.45*/("""
<!--                                     <button id="followButton"
                                        class="btn btn-link"
                                        objectId=""""),_display_(Seq[Any](/*73.52*/collection/*73.62*/.id)),format.raw/*73.65*/(""""
                                        objectName=""""),_display_(Seq[Any](/*74.54*/collection/*74.64*/.name)),format.raw/*74.69*/(""""
                                        objectType="collection">
                                            <span class='glyphicon glyphicon-star-empty'></span> Unfollow
                                    </button>
 -->                                """)))})),format.raw/*78.38*/("""
                            """)))})),format.raw/*79.30*/("""
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>"""))}
    }
    
    def render(collection:models.Collection,redirect:Call,flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(collection,redirect)(flash,user)
    
    def f:((models.Collection,Call) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (collection,redirect) => (flash,user) => apply(collection,redirect)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collections/listitem.scala.html
                    HASH: 56496edb51e9939059694b1080fcafbac160a023
                    MATRIX: 657->1|884->111|911->135|1001->190|1019->200|1043->203|1094->220|1138->256|1177->258|1409->459|1567->581|1612->617|1651->618|1714->645|1782->691|1888->761|2034->884|2092->905|2136->926|2192->964|2205->969|2244->970|2307->997|2375->1043|2545->1181|2687->1287|2755->1333|2794->1336|2838->1357|3087->1570|3137->1598|3212->1637|3231->1647|3284->1678|3452->1810|3472->1820|3508->1833|3565->1853|3585->1863|3621->1876|3741->1960|3760->1970|3805->1992|3871->2021|3891->2031|3936->2053|4006->2087|4033->2105|4073->2107|4310->2308|4476->2464|4516->2465|4661->2573|4699->2588|4740->2592|4801->2630|4849->2641|4880->2649|5114->2865|5127->2870|5166->2871|5530->3203|5596->3237|5869->3474|5926->3522|5965->3523|6108->3630|6123->3636|6178->3669|6524->3979|6538->3984|6573->3997|6717->4109|6821->4177|6848->4195|6888->4197|6958->4231|7014->4278|7054->4280|7270->4460|7289->4470|7314->4473|7405->4528|7424->4538|7451->4543|7718->4792|7731->4797|7770->4798|7986->4978|8005->4988|8030->4991|8121->5046|8140->5056|8167->5061|8455->5317|8517->5347
                    LINES: 20->1|24->1|25->3|25->3|25->3|25->3|26->4|26->4|26->4|32->10|36->14|36->14|36->14|37->15|37->15|38->16|38->16|38->16|38->16|40->18|40->18|40->18|41->19|41->19|44->22|47->25|47->25|47->25|47->25|51->29|51->29|52->30|52->30|52->30|54->32|54->32|54->32|54->32|54->32|54->32|55->33|55->33|55->33|55->33|55->33|55->33|56->34|56->34|56->34|58->36|58->36|58->36|59->37|59->37|59->37|59->37|59->37|59->37|61->39|61->39|61->39|65->43|66->44|72->50|72->50|72->50|74->52|74->52|74->52|78->56|78->56|78->56|81->59|83->61|83->61|83->61|84->62|84->62|84->62|87->65|87->65|87->65|88->66|88->66|88->66|92->70|92->70|92->70|95->73|95->73|95->73|96->74|96->74|96->74|100->78|101->79
                    -- GENERATED --
                */
            