
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object emailAdmin extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(subject: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.55*/("""
"""),_display_(Seq[Any](/*2.2*/main("Email Admins")/*2.22*/ {_display_(Seq[Any](format.raw/*2.24*/("""
    <div class="row">
        <div class="col-md-12">
            <h1>Email admins</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            This form will allow you to send an email to admins of this server.
        </div>
    </div>

    <br/>

    <form class="form-horizontal">
        <div class="form-group">
            <label for="subject" class="col-xs-12">Subject</label>
            <div class="col-xs-12">
                <input type="text" class="form-control" id="subject" value=""""),_display_(Seq[Any](/*20.78*/subject)),format.raw/*20.85*/("""" required paceholder="Subject of the email message">
            </div>
        </div>

        <div class="form-group">
            <label for="subject" class="col-xs-12">Body</label>
            <div class="col-xs-12">
                <textarea rows=4 id="body" style="resize: none;" class="form-control" placeholder="Body of the email message" required></textarea>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-default" onclick="return sendEmail();">Send</button>
            </div>
        </div>
    </form>

    <script>
            function sendEmail() """),format.raw/*39.34*/("""{"""),format.raw/*39.35*/("""
                var subject = $("#subject").val().trim();
                if(subject == "") """),format.raw/*41.35*/("""{"""),format.raw/*41.36*/("""
                    notify("Please fill out subjct of email.", "error", false, 3000);
                    return false;
                """),format.raw/*44.17*/("""}"""),format.raw/*44.18*/("""

                var body = $("#body").val().trim();
                if(body == "") """),format.raw/*47.32*/("""{"""),format.raw/*47.33*/("""
                    notify("Please fill out body of email.", "error", false, 3000);
                    return false;
                """),format.raw/*50.17*/("""}"""),format.raw/*50.18*/("""

                $.ajax("""),format.raw/*52.24*/("""{"""),format.raw/*52.25*/("""
                    url: """"),_display_(Seq[Any](/*53.28*/api/*53.31*/.routes.Admin.mail().url)),format.raw/*53.55*/("""",
                    data: JSON.stringify("""),format.raw/*54.42*/("""{"""),format.raw/*54.43*/(""" subject: subject, body: body"""),format.raw/*54.72*/("""}"""),format.raw/*54.73*/("""),
                    type: "POST",
                    contentType: "application/json"
                """),format.raw/*57.17*/("""}"""),format.raw/*57.18*/(""").done(function() """),format.raw/*57.36*/("""{"""),format.raw/*57.37*/("""
                    notify("Email send to admins.", "success", false, 5000);
                    $("#subject").val('')
                    $("#body").val('')
                """),format.raw/*61.17*/("""}"""),format.raw/*61.18*/(""").fail(function (jqXHR, textStatus, errorThrown) """),format.raw/*61.67*/("""{"""),format.raw/*61.68*/("""
                    console.error("The following error occured: " + textStatus, errorThrown);
                    notify("The application preferences was not updated due to : " + errorThrown, "error", false);
                """),format.raw/*64.17*/("""}"""),format.raw/*64.18*/(""");
                return false;
            """),format.raw/*66.13*/("""}"""),format.raw/*66.14*/("""
    </script>
""")))})),format.raw/*68.2*/("""
"""))}
    }
    
    def render(subject:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(subject)(user)
    
    def f:((String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (subject) => (user) => apply(subject)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:24 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/emailAdmin.scala.html
                    HASH: 8db60ae1277e2609aff0860134bf4f36514a3553
                    MATRIX: 612->1|759->54|795->56|823->76|862->78|1453->633|1482->640|2182->1312|2211->1313|2332->1406|2361->1407|2526->1544|2555->1545|2668->1630|2697->1631|2860->1766|2889->1767|2942->1792|2971->1793|3035->1821|3047->1824|3093->1848|3165->1892|3194->1893|3251->1922|3280->1923|3413->2028|3442->2029|3488->2047|3517->2048|3720->2223|3749->2224|3826->2273|3855->2274|4109->2500|4138->2501|4211->2546|4240->2547|4287->2563
                    LINES: 20->1|23->1|24->2|24->2|24->2|42->20|42->20|61->39|61->39|63->41|63->41|66->44|66->44|69->47|69->47|72->50|72->50|74->52|74->52|75->53|75->53|75->53|76->54|76->54|76->54|76->54|79->57|79->57|79->57|79->57|83->61|83->61|83->61|83->61|86->64|86->64|88->66|88->66|90->68
                    -- GENERATED --
                */
            