
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object datasetInfo extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Dataset,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(d: Dataset):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.14*/("""

<div id="dataset_"""),_display_(Seq[Any](/*3.19*/d/*3.20*/.id)),format.raw/*3.23*/("""" class="draggable ui-widget-content">
	<a href=""""),_display_(Seq[Any](/*4.12*/(routes.Datasets.dataset(d.id)))),format.raw/*4.43*/("""">"""),_display_(Seq[Any](/*4.46*/d/*4.47*/.name)),format.raw/*4.52*/("""</a>
	<div class="removeButtonWrapper">
		<button id="deselect_"""),_display_(Seq[Any](/*6.25*/d/*6.26*/.id)),format.raw/*6.29*/("""">x</button>
	</div>
</div>
<script>
$(function() """),format.raw/*10.14*/("""{"""),format.raw/*10.15*/("""
    $( "#deselect_"""),_display_(Seq[Any](/*11.20*/d/*11.21*/.id)),format.raw/*11.24*/("""" ).click(function() """),format.raw/*11.45*/("""{"""),format.raw/*11.46*/("""

     console.log("deselecting dataset " + """"),_display_(Seq[Any](/*13.45*/d/*13.46*/.id)),format.raw/*13.49*/("""");
     var request = jsRoutes.api.Selected.remove().ajax("""),format.raw/*14.56*/("""{"""),format.raw/*14.57*/("""
       data: JSON.stringify("""),format.raw/*15.29*/("""{"""),format.raw/*15.30*/(""""dataset":""""),_display_(Seq[Any](/*15.42*/d/*15.43*/.id)),format.raw/*15.46*/("""""""),format.raw/*15.47*/("""}"""),format.raw/*15.48*/("""),
       type: 'POST',
       contentType: "application/json",
     """),format.raw/*18.6*/("""}"""),format.raw/*18.7*/(""");
     
     request.done(function (response, textStatus, jqXHR)"""),format.raw/*20.57*/("""{"""),format.raw/*20.58*/("""
        $("#dataset_"""),_display_(Seq[Any](/*21.22*/d/*21.23*/.id)),format.raw/*21.26*/("""").remove();
     """),format.raw/*22.6*/("""}"""),format.raw/*22.7*/(""");
  
     request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*24.60*/("""{"""),format.raw/*24.61*/("""
        console.error(
            "The following error occured: "+
            textStatus, errorThrown
            
        );
        window.location = "../login"; // FIXME hardcoded
     """),format.raw/*31.6*/("""}"""),format.raw/*31.7*/(""");
     return false;
     
  """),format.raw/*34.3*/("""}"""),format.raw/*34.4*/(""");
 """),format.raw/*35.2*/("""}"""),format.raw/*35.3*/(""");
</script>
"""))}
    }
    
    def render(d:Dataset): play.api.templates.HtmlFormat.Appendable = apply(d)
    
    def f:((Dataset) => play.api.templates.HtmlFormat.Appendable) = (d) => apply(d)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasetInfo.scala.html
                    HASH: df131e1ed68908fabe963aad58b0359b4d165595
                    MATRIX: 594->1|700->13|755->33|764->34|788->37|873->87|925->118|963->121|972->122|998->127|1097->191|1106->192|1130->195|1208->245|1237->246|1293->266|1303->267|1328->270|1377->291|1406->292|1488->338|1498->339|1523->342|1610->401|1639->402|1696->431|1725->432|1773->444|1783->445|1808->448|1837->449|1866->450|1962->519|1990->520|2083->585|2112->586|2170->608|2180->609|2205->612|2250->630|2278->631|2371->696|2400->697|2618->888|2646->889|2703->919|2731->920|2762->924|2790->925
                    LINES: 20->1|23->1|25->3|25->3|25->3|26->4|26->4|26->4|26->4|26->4|28->6|28->6|28->6|32->10|32->10|33->11|33->11|33->11|33->11|33->11|35->13|35->13|35->13|36->14|36->14|37->15|37->15|37->15|37->15|37->15|37->15|37->15|40->18|40->18|42->20|42->20|43->21|43->21|43->21|44->22|44->22|46->24|46->24|53->31|53->31|56->34|56->34|57->35|57->35
                    -- GENERATED --
                */
            