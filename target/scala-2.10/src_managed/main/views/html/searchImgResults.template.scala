
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object searchImgResults extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[models.TempFile,String,Integer,Array[scala.Tuple4[String, String, Double, String]],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(file: models.TempFile, id: String,size:Integer, name:Array[(String,String,Double,String)] ):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.94*/("""
<!-- results: Seq[models.Result.Result]-->

"""),_display_(Seq[Any](/*4.2*/main("Search Results")/*4.24*/ {_display_(Seq[Any](format.raw/*4.26*/("""
	<div class="page-header">
		<h1>Search Results For <medium>"""),_display_(Seq[Any](/*6.35*/file/*6.39*/.filename)),format.raw/*6.48*/("""</medium></h1>
	</div>
	
	"""),_display_(Seq[Any](/*9.3*/if(size == 0)/*9.16*/ {_display_(Seq[Any](format.raw/*9.18*/("""
	<div class="row">
		<div class="col-md-12">
			No results found. Sorry!
		</div>
	</div>
	""")))})),format.raw/*15.3*/("""
	<div class="row">
		<div class="col-md-12">
		
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Proximity</th>
					
				</tr>
			</thead>
			<tbody>
			    """),_display_(Seq[Any](/*28.9*/name/*28.13*/.map/*28.17*/{   re=>_display_(Seq[Any](format.raw/*28.25*/(""" 
	               <tr>
						<td><a href="""),_display_(Seq[Any](/*30.20*/routes/*30.26*/.Files.file(UUID(re._1)))),format.raw/*30.50*/("""> """),_display_(Seq[Any](/*30.53*/re/*30.55*/._4)),format.raw/*30.58*/("""</a></td>
						<td>"""),_display_(Seq[Any](/*31.12*/re/*31.14*/._3)),format.raw/*31.17*/("""</td>
					</tr>
				""")))})),format.raw/*33.6*/("""
		</tbody>
		</table>
		</div>
	  </div>
			
    """)))})))}
    }
    
    def render(file:models.TempFile,id:String,size:Integer,name:Array[scala.Tuple4[String, String, Double, String]]): play.api.templates.HtmlFormat.Appendable = apply(file,id,size,name)
    
    def f:((models.TempFile,String,Integer,Array[scala.Tuple4[String, String, Double, String]]) => play.api.templates.HtmlFormat.Appendable) = (file,id,size,name) => apply(file,id,size,name)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/searchImgResults.scala.html
                    HASH: cc0d6c48a57c44a23cc4af7b1e77be58a9adb97e
                    MATRIX: 674->1|860->93|940->139|970->161|1009->163|1106->225|1118->229|1148->238|1209->265|1230->278|1269->280|1393->373|1636->581|1649->585|1662->589|1708->597|1786->639|1801->645|1847->669|1886->672|1897->674|1922->677|1979->698|1990->700|2015->703|2068->725
                    LINES: 20->1|23->1|26->4|26->4|26->4|28->6|28->6|28->6|31->9|31->9|31->9|37->15|50->28|50->28|50->28|50->28|52->30|52->30|52->30|52->30|52->30|52->30|53->31|53->31|53->31|55->33
                    -- GENERATED --
                */
            