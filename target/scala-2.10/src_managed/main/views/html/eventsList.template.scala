
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object eventsList extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[models.Event],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(newsfeed: List[models.Event])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters

import play.api.i18n.Messages


Seq[Any](format.raw/*1.68*/("""
"""),_display_(Seq[Any](/*4.2*/for(event <- newsfeed) yield /*4.24*/ {_display_(Seq[Any](format.raw/*4.26*/("""
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="row">
        <div class="col-xs-1">
          <a href=""""),_display_(Seq[Any](/*9.21*/routes/*9.27*/.Profile.viewProfileUUID(event.user.id))),format.raw/*9.66*/("""">
            <img class="img-responsive" src=""""),_display_(Seq[Any](/*10.47*/event/*10.52*/.user.avatarURL)),format.raw/*10.67*/("""">
          </a>
        </div>
        <div class="col-xs-11">
          <div class="row">
            <a href=""""),_display_(Seq[Any](/*15.23*/routes/*15.29*/.Profile.viewProfileUUID(event.user.id))),format.raw/*15.68*/("""">
              """),_display_(Seq[Any](/*16.16*/event/*16.21*/.user.fullName)),format.raw/*16.35*/("""
            </a>
            """),_display_(Seq[Any](/*18.14*/newsfeedCard(event))),format.raw/*18.33*/("""
          </div>
          <div class="row top-padding">
            """),_display_(Seq[Any](/*21.14*/Formatters/*21.24*/.humanReadableTimeSince(event.created))),format.raw/*21.62*/("""
          </div>
        </div>
      </div>
    </div>
  </div>
""")))})),format.raw/*27.2*/("""
<script>
  $(document).ready(function() """),format.raw/*29.32*/("""{"""),format.raw/*29.33*/("""
    // the default number of event index. means there is no event for this user.
    if(eventCount === 3)"""),format.raw/*31.25*/("""{"""),format.raw/*31.26*/("""
      """),_display_(Seq[Any](/*32.8*/if( newsfeed.size < 1)/*32.30*/ {_display_(Seq[Any](format.raw/*32.32*/("""
      $("#moreeventbutton").replaceWith("<p>You can follow <a href="""),_display_(Seq[Any](/*33.69*/routes/*33.75*/.Users.getUsers())),format.raw/*33.92*/(""">"""),_display_(Seq[Any](/*33.94*/Messages("users.title"))),format.raw/*33.117*/("""</a>, <a href="""),_display_(Seq[Any](/*33.132*/routes/*33.138*/.Spaces.list(""))),format.raw/*33.154*/(""">"""),_display_(Seq[Any](/*33.156*/Messages("spaces.title"))),format.raw/*33.180*/("""</a>,"
              + " <a href="""),_display_(Seq[Any](/*34.28*/routes/*34.34*/.Datasets.list(""))),format.raw/*34.52*/(""">"""),_display_(Seq[Any](/*34.54*/Messages("datasets.title"))),format.raw/*34.80*/("""</a> and <a href="""),_display_(Seq[Any](/*34.98*/routes/*34.104*/.Collections.list(""))),format.raw/*34.125*/(""">"""),_display_(Seq[Any](/*34.127*/Messages("collections.title"))),format.raw/*34.156*/("""</a>."
              + " Any updates on your followed instances will show here.</p>");
      """)))}/*36.9*/else/*36.15*/{_display_(Seq[Any](format.raw/*36.16*/("""
      """),_display_(Seq[Any](/*37.8*/if( newsfeed.size < 20)/*37.31*/ {_display_(Seq[Any](format.raw/*37.33*/(""" $("#moreeventbutton").replaceWith("<p>No more events.</p>");""")))})),format.raw/*37.95*/("""
      """)))})),format.raw/*38.8*/("""
    """),format.raw/*39.5*/("""}"""),format.raw/*39.6*/(""" else """),format.raw/*39.12*/("""{"""),format.raw/*39.13*/("""
      """),_display_(Seq[Any](/*40.8*/if( newsfeed.size < 10)/*40.31*/ {_display_(Seq[Any](format.raw/*40.33*/(""" $("#moreeventbutton").replaceWith("<p>No more events.</p>");""")))})),format.raw/*40.95*/("""
    """),format.raw/*41.5*/("""}"""),format.raw/*41.6*/("""

  """),format.raw/*43.3*/("""}"""),format.raw/*43.4*/(""");
</script>"""))}
    }
    
    def render(newsfeed:List[models.Event],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(newsfeed)(user)
    
    def f:((List[models.Event]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (newsfeed) => (user) => apply(newsfeed)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/eventsList.scala.html
                    HASH: 35eaca2c868b9d918d6893e04dabc1283dda2cb3
                    MATRIX: 624->1|845->67|881->131|918->153|957->155|1133->296|1147->302|1207->341|1292->390|1306->395|1343->410|1494->525|1509->531|1570->570|1624->588|1638->593|1674->607|1741->638|1782->657|1889->728|1908->738|1968->776|2066->843|2135->884|2164->885|2298->991|2327->992|2370->1000|2401->1022|2441->1024|2546->1093|2561->1099|2600->1116|2638->1118|2684->1141|2736->1156|2752->1162|2791->1178|2830->1180|2877->1204|2947->1238|2962->1244|3002->1262|3040->1264|3088->1290|3142->1308|3158->1314|3202->1335|3241->1337|3293->1366|3405->1461|3418->1467|3457->1468|3500->1476|3532->1499|3572->1501|3666->1563|3705->1571|3737->1576|3765->1577|3799->1583|3828->1584|3871->1592|3903->1615|3943->1617|4037->1679|4069->1684|4097->1685|4128->1689|4156->1690
                    LINES: 20->1|26->1|27->4|27->4|27->4|32->9|32->9|32->9|33->10|33->10|33->10|38->15|38->15|38->15|39->16|39->16|39->16|41->18|41->18|44->21|44->21|44->21|50->27|52->29|52->29|54->31|54->31|55->32|55->32|55->32|56->33|56->33|56->33|56->33|56->33|56->33|56->33|56->33|56->33|56->33|57->34|57->34|57->34|57->34|57->34|57->34|57->34|57->34|57->34|57->34|59->36|59->36|59->36|60->37|60->37|60->37|60->37|61->38|62->39|62->39|62->39|62->39|63->40|63->40|63->40|63->40|64->41|64->41|66->43|66->43
                    -- GENERATED --
                */
            