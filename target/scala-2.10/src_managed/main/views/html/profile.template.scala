
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object profile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[User,List[UserApiKey],Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(profile: User, keys: List[UserApiKey], ownProfile: Boolean)(implicit user: Option[models.User] = None):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.105*/("""
"""),_display_(Seq[Any](/*2.2*/main("Profile")/*2.17*/ {_display_(Seq[Any](format.raw/*2.19*/("""
    <script src=""""),_display_(Seq[Any](/*3.19*/routes/*3.25*/.Application.javascriptRoutes)),format.raw/*3.54*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*4.19*/routes/*4.25*/.Assets.at("javascripts/recommendation.js"))),format.raw/*4.68*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*5.19*/routes/*5.25*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*5.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*6.19*/routes/*6.25*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*6.70*/("""" type="text/javascript"></script>

    <div class="row">
        <div class="col-md-3 col-lg-3 col-sm-5">
            <img src="""),_display_(Seq[Any](/*10.23*/profile/*10.30*/.getAvatarUrl(256))),format.raw/*10.48*/(""" class="profilePicture">
            <div align="center">
                <h4>Followed by <span id="followerSize">"""),_display_(Seq[Any](/*12.58*/profile/*12.65*/.followers.size)),format.raw/*12.80*/("""</span> people</h4>
                """),_display_(Seq[Any](/*13.18*/user/*13.22*/ match/*13.28*/ {/*14.21*/case Some(viewer) =>/*14.41*/ {_display_(Seq[Any](format.raw/*14.43*/("""
                        """),_display_(Seq[Any](/*15.26*/if(ownProfile)/*15.40*/ {_display_(Seq[Any](format.raw/*15.42*/("""
                            <h4><a href=""""),_display_(Seq[Any](/*16.43*/routes/*16.49*/.Profile.editProfile)),format.raw/*16.69*/("""">Edit Profile</a></h4>
                        """)))}/*17.27*/else/*17.32*/{_display_(Seq[Any](format.raw/*17.33*/("""
                            <h4>
                                <button id="userFollowButton" type="button" class="followButton btn btn-link" data-toggle="button"
                                aria-pressed="
                                    """),_display_(Seq[Any](/*21.38*/if(profile.followers.contains(viewer.id))/*21.79*/ {_display_(Seq[Any](format.raw/*21.81*/("""
                                        true
                                    """)))}/*23.38*/else/*23.43*/{_display_(Seq[Any](format.raw/*23.44*/("""
                                        false
                                    """)))})),format.raw/*25.38*/(""""
                                autocomplete="off" objectId=""""),_display_(Seq[Any](/*26.63*/profile/*26.70*/.id.stringify)),format.raw/*26.83*/("""" objectName=""""),_display_(Seq[Any](/*26.98*/profile/*26.105*/.fullName)),format.raw/*26.114*/("""" objectType="user">
                                """),_display_(Seq[Any](/*27.34*/if(profile.followers.contains(viewer.id))/*27.75*/ {_display_(Seq[Any](format.raw/*27.77*/("""
<!--                                     <span class="glyphicon glyphicon-star-empty"></span> Unfollow
 -->                                """)))}/*29.39*/else/*29.44*/{_display_(Seq[Any](format.raw/*29.45*/("""
<!--                                     <span class="glyphicon glyphicon-star"></span> Follow
 -->                                """)))})),format.raw/*31.38*/("""
                                </button>
                            </h4>
                            <div id="recommendPanel" class="panel panel-default" style="display:none;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-parent="#accordion" href="#collapseThree" aria-expanded="true" style="float:left;">
                                            Also follow these?
                                        </a>
                                        <a style="float:right;" href="javascript:$('#recommendPanel').slideToggle('slow');">x</a>
                                        <div style="clear:both;"></div>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse in" aria-expanded="true">
                                    <div id="recommendDiv" class="panel-body"></div>
                                </div>
                            </div>
                                <!-- Is it the only way to store a scala variable as a global for re-use in Javascript? -->
                            <script>
                                    var viewerId = '"""),_display_(Seq[Any](/*50.54*/viewer/*50.60*/.id)),format.raw/*50.63*/("""';
                            </script>
                        """)))})),format.raw/*52.26*/("""
                    """)))}/*54.21*/case None =>/*54.33*/ {}})),format.raw/*55.18*/("""
            </div>
        </div>
        <div class="col-md-9 col-lg-9 col-sm-7">
            """),_display_(Seq[Any](/*59.14*/if(profile.identityId.providerId != "userpass")/*59.61*/ {_display_(Seq[Any](format.raw/*59.63*/("""
                <div>
                    <h1>"""),_display_(Seq[Any](/*61.26*/profile/*61.33*/.fullName)),format.raw/*61.42*/("""</h1>
                </div>
            """)))}/*63.15*/else/*63.20*/{_display_(Seq[Any](format.raw/*63.21*/("""
                """),_display_(Seq[Any](/*64.18*/if(ownProfile)/*64.32*/{_display_(Seq[Any](format.raw/*64.33*/("""
                    <div id="prf-first-name" class="text-left inline">
                        <h1 id="first-name-title" class="inline" style="cursor:pointer" title="Click to edit user's first name.">"""),_display_(Seq[Any](/*66.131*/Html(profile.firstName))),format.raw/*66.154*/("""</h1>
                        <div id="h-edit-first" class="hiddencomplete" title="Click to edit user's first name.">
                            <a href="javascript:updateFirstLastName()"></a>
                        </div>
                    </div>
                    <div id="prf-last-name" class="text-left inline edit-tab">
                        <h1 id="last-name-title" class="inline" style="cursor:pointer" title="Click to edit user's last name.">"""),_display_(Seq[Any](/*72.129*/Html(profile.lastName))),format.raw/*72.151*/("""</h1>
                        <div id="h-edit-last" class="hiddencomplete" title="Click to edit user's last name.">
                            <a href="javascript:updateFirstLastName()"><span class ="glyphicon glyphicon-edit"></span></a>
                        </div>
                    </div>
                """)))}/*77.19*/else/*77.24*/{_display_(Seq[Any](format.raw/*77.25*/("""
                    <div class="text-left inline">
                        <h1 class="inline">"""),_display_(Seq[Any](/*79.45*/Html(profile.firstName))),format.raw/*79.68*/("""</h1>
                    </div>
                    <div class="text-left inline edit-tab">
                        <h1 class="inline">"""),_display_(Seq[Any](/*82.45*/Html(profile.lastName))),format.raw/*82.67*/("""</h1>
                    </div>
                """)))})),format.raw/*84.18*/("""
            """)))})),format.raw/*85.14*/("""
            <div class="profile-section">
            """),_display_(Seq[Any](/*87.14*/profile/*87.21*/.identityId.providerId/*87.43*/ match/*87.49*/ {/*88.17*/case "userpass" =>/*88.35*/ {_display_(Seq[Any](format.raw/*88.37*/("""
                    Profile Source : Local Account
                """)))}/*91.17*/case "google" =>/*91.33*/ {_display_(Seq[Any](format.raw/*91.35*/("""
                    Profile Source : <img height="16px" src=""""),_display_(Seq[Any](/*92.63*/routes/*92.69*/.Assets.at("securesocial/images/providers/" + profile.identityId.providerId + ".png"))),format.raw/*92.154*/("""" alt=""""),_display_(Seq[Any](/*92.162*/profile/*92.169*/.identityId.providerId)),format.raw/*92.191*/("""" />
                    <a href="https://plus.google.com/"""),_display_(Seq[Any](/*93.55*/profile/*93.62*/.identityId.userId)),format.raw/*93.80*/("""">Google+</a>
                """)))}/*95.17*/case "twitter" =>/*95.34*/ {_display_(Seq[Any](format.raw/*95.36*/("""
                    Profile Source : <img height="16px" src=""""),_display_(Seq[Any](/*96.63*/routes/*96.69*/.Assets.at("securesocial/images/providers/" + profile.identityId.providerId + ".png"))),format.raw/*96.154*/("""" alt=""""),_display_(Seq[Any](/*96.162*/profile/*96.169*/.identityId.providerId)),format.raw/*96.191*/("""" />
                    <a href="https://twitter.com/intent/user?user_id="""),_display_(Seq[Any](/*97.71*/profile/*97.78*/.identityId.userId)),format.raw/*97.96*/("""">Twitter</a>
                """)))}/*99.17*/case "facebook" =>/*99.35*/ {_display_(Seq[Any](format.raw/*99.37*/("""
                    Profile Source : <img height="16px" src=""""),_display_(Seq[Any](/*100.63*/routes/*100.69*/.Assets.at("securesocial/images/providers/" + profile.identityId.providerId + ".png"))),format.raw/*100.154*/("""" alt=""""),_display_(Seq[Any](/*100.162*/profile/*100.169*/.identityId.providerId)),format.raw/*100.191*/("""" />
                    <a href="https://www.facebook.com/app_scoped_user_id/"""),_display_(Seq[Any](/*101.75*/profile/*101.82*/.identityId.userId)),format.raw/*101.100*/("""">Facebook</a>
                """)))}/*103.17*/case "orcid" =>/*103.32*/ {_display_(Seq[Any](format.raw/*103.34*/("""
                    Profile Source : <img height="16px" src=""""),_display_(Seq[Any](/*104.63*/routes/*104.69*/.Assets.at("securesocial/images/providers/" + profile.identityId.providerId + ".png"))),format.raw/*104.154*/("""" alt=""""),_display_(Seq[Any](/*104.162*/profile/*104.169*/.identityId.providerId)),format.raw/*104.191*/("""" />
                    <a href="https://orcid.org/"""),_display_(Seq[Any](/*105.49*/profile/*105.56*/.identityId.userId)),format.raw/*105.74*/("""">ORCID</a>
                """)))}/*107.17*/case provider =>/*107.33*/ {_display_(Seq[Any](format.raw/*107.35*/("""
                    Profile Source : <img height="16px" src=""""),_display_(Seq[Any](/*108.63*/routes/*108.69*/.Assets.at("securesocial/images/providers/" + profile.identityId.providerId + ".png"))),format.raw/*108.154*/("""" alt=""""),_display_(Seq[Any](/*108.162*/profile/*108.169*/.identityId.providerId)),format.raw/*108.191*/("""" />
                    """),_display_(Seq[Any](/*109.22*/provider/*109.30*/.capitalize)),format.raw/*109.41*/("""
                """)))}})),format.raw/*111.14*/("""
            </div>
            """),_display_(Seq[Any](/*113.14*/if(profile.email.isDefined)/*113.41*/ {_display_(Seq[Any](format.raw/*113.43*/("""
                <div>"""),_display_(Seq[Any](/*114.23*/profile/*114.30*/.email.get)),format.raw/*114.40*/("""</div>
            """)))})),format.raw/*115.14*/("""
            <div>&nbsp;</div>
            """),_display_(Seq[Any](/*117.14*/if(profile.profile.isDefined)/*117.43*/ {_display_(Seq[Any](format.raw/*117.45*/("""
                <div class="profile-section">
                    <h4>Institution</h4>
                    <p>"""),_display_(Seq[Any](/*120.25*/profile/*120.32*/.profile.get.getPositionAtInstitution)),format.raw/*120.69*/("""</p>

                    """),_display_(Seq[Any](/*122.22*/profile/*122.29*/.profile.get.orcidID/*122.49*/ match/*122.55*/{/*123.25*/case Some(info) =>/*123.43*/ {_display_(Seq[Any](format.raw/*123.45*/("""
                            <h4>Orcid ID</h4>
                            <a href="http://orcid.org/"""),_display_(Seq[Any](/*125.56*/info)),format.raw/*125.60*/("""" target="_blank"> <p>"""),_display_(Seq[Any](/*125.83*/info)),format.raw/*125.87*/("""</p> </a>
                        """)))}/*127.25*/case None =>/*127.37*/ {}})),format.raw/*128.22*/("""
                    """),_display_(Seq[Any](/*129.22*/if(profile.profile.get.biography.nonEmpty)/*129.64*/ {_display_(Seq[Any](format.raw/*129.66*/("""
                        <h4>Biography</h4>
                        <p>"""),_display_(Seq[Any](/*131.29*/profile/*131.36*/.profile.get.biography)),format.raw/*131.58*/("""</p>
                    """)))})),format.raw/*132.22*/("""
                    """),_display_(Seq[Any](/*133.22*/if(profile.profile.get.currentprojects.nonEmpty)/*133.70*/ {_display_(Seq[Any](format.raw/*133.72*/("""
                        <h4>Current Projects</h4>
                        <p>"""),_display_(Seq[Any](/*135.29*/profile/*135.36*/.profile.get.currentprojects.mkString(", "))),format.raw/*135.79*/("""</p>
                    """)))})),format.raw/*136.22*/("""
                    """),_display_(Seq[Any](/*137.22*/if(profile.profile.get.pastprojects.nonEmpty)/*137.67*/ {_display_(Seq[Any](format.raw/*137.69*/("""
                        <h4>Past Projects</h4>
                        <p>"""),_display_(Seq[Any](/*139.29*/profile/*139.36*/.profile.get.pastprojects.mkString(", "))),format.raw/*139.76*/("""</p>
                    """)))})),format.raw/*140.22*/("""
                </div>
            """)))})),format.raw/*142.14*/("""
            """),_display_(Seq[Any](/*143.14*/if(ownProfile && play.api.Play.current.plugin[services.StagingAreaPlugin].isDefined)/*143.98*/ {_display_(Seq[Any](format.raw/*143.100*/("""
                <div class="profile-disclaimer small">"""),_display_(Seq[Any](/*144.56*/play/*144.60*/.api.i18n.Messages("profile.disclaimer"))),format.raw/*144.100*/("""</div>
            """)))})),format.raw/*145.14*/("""
            """),_display_(Seq[Any](/*146.14*/if(ownProfile)/*146.28*/ {_display_(Seq[Any](format.raw/*146.30*/("""
                <h4 id="userkeys">User API Keys</h4>
                <span>Create your personal API keys by providing a name for the key and clicking the Add button. Key names have to be unique per user.</span>
                <div class="row wrapped" id="key-">
                    <div class="col-md-1"></div>
                    <div class="col-md-2">Name</div>
                    <div class="col-md-9">Key</div>
                </div>
                """),_display_(Seq[Any](/*154.18*/keys/*154.22*/.map/*154.26*/{key =>_display_(Seq[Any](format.raw/*154.33*/("""
                    <div class="row wrapped" id="key-"""),_display_(Seq[Any](/*155.55*/key/*155.58*/.name)),format.raw/*155.63*/("""">
                        <div class="col-md-1"><a href='javascript:deleteUserKey(""""),_display_(Seq[Any](/*156.83*/key/*156.86*/.name)),format.raw/*156.91*/("""");'><span class="glyphicon glyphicon-trash"></span></a></div>
                        <div class="col-md-2">"""),_display_(Seq[Any](/*157.48*/key/*157.51*/.name)),format.raw/*157.56*/("""</div>
                        <div class="col-md-9">"""),_display_(Seq[Any](/*158.48*/key/*158.51*/.key)),format.raw/*158.55*/("""</div>
                    </div>
                """)))})),format.raw/*160.18*/("""
                <div class="row wrapped">
                    <div class="col-md-1"><a href='javascript:addUserKey();'><span class="glyphicon glyphicon-plus"></span> Add</a></div>
                    <div class="col-md-2"><input type="text" id="userkeyname" class="form-control"></div>
                </div>
            """)))})),format.raw/*165.14*/("""
        </div>

    </div>

    <script language="javascript">
            var profileMail = """"),_display_(Seq[Any](/*171.33*/profile/*171.40*/.email.getOrElse(""))),format.raw/*171.60*/("""";
            var firstName = $('#first-name-title').text();
            var lastName = $('#last-name-title').text();

            $('#first-name-title').click(function()"""),format.raw/*175.52*/("""{"""),format.raw/*175.53*/("""
                updateFirstLastName();
            """),format.raw/*177.13*/("""}"""),format.raw/*177.14*/(""");
            $('#last-name-title').click(function()"""),format.raw/*178.51*/("""{"""),format.raw/*178.52*/("""
                updateFirstLastName();
            """),format.raw/*180.13*/("""}"""),format.raw/*180.14*/(""");
            function updateFirstLastName() """),format.raw/*181.44*/("""{"""),format.raw/*181.45*/("""
                cancelFirstLastName();
                firstName = $('#first-name-title').text();
                lastName = $('#last-name-title').text();
                $('<div class="inline first_name_div h3"> </div>').insertAfter($('#prf-first-name'));
                $('.first_name_div').append('<input type="text" id="first_name_input" style="width:300px"" />');
                $('.first_name_div').append('<div class="small hiddencomplete" id="first-name-error"> <span class="error">First name is required.</span></div>');

                $('<div class="inline last_name_div h3"></div>').insertAfter($('#prf-last-name'));
                $('.last_name_div').append('<input type="text" id="last_name_input" style="width:300px"" />');
                $('.last_name_div').append('<button class="btn btn-sm btn-primary edit-tab" onclick="saveFirstLastName()"> <span class="glyphicon glyphicon-send"></span> Save</button>');
                $('.last_name_div').append('<button class="btn btn-sm btn-link" onclick="cancelFirstLastName()"> <span class="glyphicon glyphicon-remove"></span> Cancel</button>');
                $('.last_name_div').append('<div class="small hiddencomplete" id="last-name-error"> <span class="error">Last name is required.</span></div>');

                $('#first_name_input').val(firstName);
                $('#first-name-title').text("");
                $('#h-edit-first').css("display", "none");

                $('#last_name_input').val(lastName);
                $('#last-name-title').text("");
                $('#h-edit-last').css("display", "none");
            """),format.raw/*202.13*/("""}"""),format.raw/*202.14*/("""

            function saveFirstLastName() """),format.raw/*204.42*/("""{"""),format.raw/*204.43*/("""
                if($('#first_name_input').val().length < 1) """),format.raw/*205.61*/("""{"""),format.raw/*205.62*/("""
                    $('#first-name-error').show();
                    return false;
                """),format.raw/*208.17*/("""}"""),format.raw/*208.18*/(""" else """),format.raw/*208.24*/("""{"""),format.raw/*208.25*/("""
                    $('#first-name-error').hide();
                """),format.raw/*210.17*/("""}"""),format.raw/*210.18*/("""
                if($('#last_name_input').val().length < 1) """),format.raw/*211.60*/("""{"""),format.raw/*211.61*/("""
                    $('#last-name-error').show();
                    return false;
                """),format.raw/*214.17*/("""}"""),format.raw/*214.18*/(""" else """),format.raw/*214.24*/("""{"""),format.raw/*214.25*/("""
                    $('#last-name-error').hide();
                """),format.raw/*216.17*/("""}"""),format.raw/*216.18*/("""
                var fName = htmlEncode($('#first_name_input').val());
                var lName = htmlEncode($('#last_name_input').val());
                var request = jsRoutes.api.Users.updateName(""""),_display_(Seq[Any](/*219.63*/profile/*219.70*/.id)),format.raw/*219.73*/("""", fName, lName ).ajax("""),format.raw/*219.96*/("""{"""),format.raw/*219.97*/("""
                    type: 'POST'
                """),format.raw/*221.17*/("""}"""),format.raw/*221.18*/(""");
                request.done(function(response, textStatus, jqXHR) """),format.raw/*222.68*/("""{"""),format.raw/*222.69*/("""
                    //console.log("Success");
                    $('.first_name_div').remove();
                    $('#first-name-title').html(fName);
                    $('#first-name-div').mouseleave();
                    $('#h-edit-first').css("display", "");
                    firstName = $('#first-name-title').text();

                    $('.last_name_div').remove();
                    $('#last-name-title').html(lName);
                    $('#last-name-div').mouseleave();
                    $('#h-edit-last').css("display", "");
                    lastName = $('#last-name-title').text();
                """),format.raw/*235.17*/("""}"""),format.raw/*235.18*/(""");
                request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*236.71*/("""{"""),format.raw/*236.72*/("""
                    console.error("The following error occurred: " + textStatus, errorThrown);
                    var errMsg = "Yoy must be logged in to update the name.";
                    if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*239.63*/("""{"""),format.raw/*239.64*/("""
                        notify("The name was not updated due to: "+ errorThrown, "error");
                    """),format.raw/*241.21*/("""}"""),format.raw/*241.22*/("""
                """),format.raw/*242.17*/("""}"""),format.raw/*242.18*/(""");
            """),format.raw/*243.13*/("""}"""),format.raw/*243.14*/("""

            function cancelFirstLastName() """),format.raw/*245.44*/("""{"""),format.raw/*245.45*/("""
                $('#first-name-title').html(firstName);
                $('.first_name_div').remove();
                $('#prf-first-name').css("display", "inline");
                $('#h-edit-first').css("display", "");
                $('#prf-first-name').mouseleave();

                $('#last-name-title').html(lastName);
                $('.last_name_div').remove();
                $('#last-name-title').css("display", "inline");
                $('#h-edit-last').css("display", "");
                $('#prf-last-name').mouseleave();
            """),format.raw/*257.13*/("""}"""),format.raw/*257.14*/("""

            $(document).ready(function()"""),format.raw/*259.41*/("""{"""),format.raw/*259.42*/("""
                $(document).on('mouseenter', '#prf-first-name', function() """),format.raw/*260.76*/("""{"""),format.raw/*260.77*/("""
                    $('#h-edit-first').removeClass("hiddencomplete");
                    $('#h-edit-first').addClass("inline");
                """),format.raw/*263.17*/("""}"""),format.raw/*263.18*/(""").on('mouseleave', '#prf-first-name', function() """),format.raw/*263.67*/("""{"""),format.raw/*263.68*/("""
                    $('#h-edit-first').removeClass("inline");
                    $('#h-edit-first').addClass("hiddencomplete");
                """),format.raw/*266.17*/("""}"""),format.raw/*266.18*/(""");

                $(document).on('mouseenter', '#prf-last-name', function() """),format.raw/*268.75*/("""{"""),format.raw/*268.76*/("""
                    $('#h-edit-last').removeClass("hiddencomplete");
                    $('#h-edit-last').addClass("inline");
                """),format.raw/*271.17*/("""}"""),format.raw/*271.18*/(""").on('mouseleave', '#prf-last-name', function() """),format.raw/*271.66*/("""{"""),format.raw/*271.67*/("""
                    $('#h-edit-last').removeClass("inline");
                    $('#h-edit-last').addClass("hiddencomplete");
                """),format.raw/*274.17*/("""}"""),format.raw/*274.18*/(""");

                function followCallback(data) """),format.raw/*276.47*/("""{"""),format.raw/*276.48*/("""
                    var $followerSize = $('#followerSize');
                    var followerSize = parseInt($followerSize.text());
                    $followerSize.text(followerSize + 1);

                    var newRowHtml = '<tr><td>viewerId</td></tr>';
                    $('#followersTable tr:last').after(newRowHtml);
                    recommendationHandler(jsRoutes, $('#recommendPanel'), $('#recommendDiv'),
                            data['recommendations']);
                """),format.raw/*285.17*/("""}"""),format.raw/*285.18*/("""

                function unfollowCallback() """),format.raw/*287.45*/("""{"""),format.raw/*287.46*/("""
                    var $followerSize = $('#followerSize');
                    var followerSize = parseInt($followerSize.text());
                    $followerSize.text(followerSize - 1);

                    $('#followersTable').find('td').filter(function () """),format.raw/*292.72*/("""{"""),format.raw/*292.73*/("""
                        return $(this).text().trim() == viewerId;
                    """),format.raw/*294.21*/("""}"""),format.raw/*294.22*/(""").first().remove();
                """),format.raw/*295.17*/("""}"""),format.raw/*295.18*/("""

                $(document).on('click', '.followButton', function() """),format.raw/*297.69*/("""{"""),format.raw/*297.70*/("""
                    var id = $(this).attr('objectId');
                    var name = $(this).attr('objectName');
                    var type = $(this).attr('objectType');
                    if ($(this).attr('id') == undefined || $(this).attr('id') === '') """),format.raw/*301.87*/("""{"""),format.raw/*301.88*/("""
                        followHandler.call(this, jsRoutes, id, name, type, undefined, undefined);
                    """),format.raw/*303.21*/("""}"""),format.raw/*303.22*/(""" else """),format.raw/*303.28*/("""{"""),format.raw/*303.29*/("""
                        followHandler.call(this, jsRoutes, id, name, type, followCallback, unfollowCallback);
                    """),format.raw/*305.21*/("""}"""),format.raw/*305.22*/("""
                """),format.raw/*306.17*/("""}"""),format.raw/*306.18*/(""");
            """),format.raw/*307.13*/("""}"""),format.raw/*307.14*/(""");

            function addUserKey() """),format.raw/*309.35*/("""{"""),format.raw/*309.36*/("""
                var userkeyname = $("#userkeyname").val();
                if (userkeyname == "") """),format.raw/*311.40*/("""{"""),format.raw/*311.41*/("""
                    return;
                """),format.raw/*313.17*/("""}"""),format.raw/*313.18*/("""
                $("#userkeyname").val("");
                var request = jsRoutes.api.Users.keysAdd(userkeyname).ajax("""),format.raw/*315.76*/("""{"""),format.raw/*315.77*/("""
                    type: 'POST'
                """),format.raw/*317.17*/("""}"""),format.raw/*317.18*/(""");
                request.done(function(response, textStatus, jqXHR) """),format.raw/*318.68*/("""{"""),format.raw/*318.69*/("""
                    var newkey = '<div class="row wrapped" id="key-' + userkeyname + '">';
                    newkey += '<div class="col-md-1"><a href="javascript:deleteUserKey(\'' + userkeyname + '\');"><span class="glyphicon glyphicon-trash"></span></a></div>';
                    newkey += '<div class="col-md-2">' + userkeyname + '</div>';
                    newkey += '<div class="col-md-9">' + response.key + '</div>';
                    newkey += '</div>';
                    // $("#userkeys").after(newkey);
                    $(newkey).insertAfter($('[id^="key-"]').last());
                """),format.raw/*326.17*/("""}"""),format.raw/*326.18*/(""");
                request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*327.71*/("""{"""),format.raw/*327.72*/("""
                    console.error("The following error occurred: " + textStatus, errorThrown);
                    var errMsg = "Yoy must be logged in to add the key.";
                    if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*330.63*/("""{"""),format.raw/*330.64*/("""
                        notify("The name was not updated due to: "+ errorThrown, "error");
                    """),format.raw/*332.21*/("""}"""),format.raw/*332.22*/("""
                """),format.raw/*333.17*/("""}"""),format.raw/*333.18*/(""");
            """),format.raw/*334.13*/("""}"""),format.raw/*334.14*/("""

            function deleteUserKey(name) """),format.raw/*336.42*/("""{"""),format.raw/*336.43*/("""
                var request = jsRoutes.api.Users.keysDelete(name).ajax("""),format.raw/*337.72*/("""{"""),format.raw/*337.73*/("""
                    type: 'DELETE'
                """),format.raw/*339.17*/("""}"""),format.raw/*339.18*/(""");
                request.done(function(response, textStatus, jqXHR) """),format.raw/*340.68*/("""{"""),format.raw/*340.69*/("""
                    $("#key-" + name).remove();
                    notify("Removed key '" + name + "'", "success", false, 2000 );
                """),format.raw/*343.17*/("""}"""),format.raw/*343.18*/(""");
                request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*344.71*/("""{"""),format.raw/*344.72*/("""
                    console.error("The following error occurred: " + textStatus, errorThrown);
                    var errMsg = "Yoy must be logged in to delete the key.";
                    if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*347.63*/("""{"""),format.raw/*347.64*/("""
                        notify("The name was not updated due to: "+ errorThrown, "error");
                    """),format.raw/*349.21*/("""}"""),format.raw/*349.22*/("""
                """),format.raw/*350.17*/("""}"""),format.raw/*350.18*/(""");
            """),format.raw/*351.13*/("""}"""),format.raw/*351.14*/("""
    </script>
""")))})))}
    }
    
    def render(profile:User,keys:List[UserApiKey],ownProfile:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(profile,keys,ownProfile)(user)
    
    def f:((User,List[UserApiKey],Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (profile,keys,ownProfile) => (user) => apply(profile,keys,ownProfile)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:46:56 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/profile.scala.html
                    HASH: a8e2a8ce22713297c033c2752d310d84fe7f03fa
                    MATRIX: 632->1|830->104|866->106|889->121|928->123|982->142|996->148|1046->177|1134->230|1148->236|1212->279|1300->332|1314->338|1377->380|1465->433|1479->439|1545->484|1710->613|1726->620|1766->638|1917->753|1933->760|1970->775|2043->812|2056->816|2071->822|2082->845|2111->865|2151->867|2213->893|2236->907|2276->909|2355->952|2370->958|2412->978|2480->1028|2493->1033|2532->1034|2817->1283|2867->1324|2907->1326|3009->1409|3022->1414|3061->1415|3177->1499|3277->1563|3293->1570|3328->1583|3379->1598|3396->1605|3428->1614|3518->1668|3568->1709|3608->1711|3768->1853|3781->1858|3820->1859|3985->1992|5332->3303|5347->3309|5372->3312|5470->3378|5511->3421|5532->3433|5558->3454|5691->3551|5747->3598|5787->3600|5871->3648|5887->3655|5918->3664|5979->3707|5992->3712|6031->3713|6085->3731|6108->3745|6147->3746|6386->3948|6432->3971|6928->4430|6973->4452|7306->4767|7319->4772|7358->4773|7490->4869|7535->4892|7708->5029|7752->5051|7834->5101|7880->5115|7972->5171|7988->5178|8019->5200|8034->5206|8045->5225|8072->5243|8112->5245|8200->5331|8225->5347|8265->5349|8364->5412|8379->5418|8487->5503|8532->5511|8549->5518|8594->5540|8689->5599|8705->5606|8745->5624|8795->5672|8821->5689|8861->5691|8960->5754|8975->5760|9083->5845|9128->5853|9145->5860|9190->5882|9301->5957|9317->5964|9357->5982|9407->6030|9434->6048|9474->6050|9574->6113|9590->6119|9699->6204|9745->6212|9763->6219|9809->6241|9925->6320|9942->6327|9984->6345|10036->6394|10061->6409|10102->6411|10202->6474|10218->6480|10327->6565|10373->6573|10391->6580|10437->6602|10527->6655|10544->6662|10585->6680|10634->6726|10660->6742|10701->6744|10801->6807|10817->6813|10926->6898|10972->6906|10990->6913|11036->6935|11099->6961|11117->6969|11151->6980|11203->7012|11273->7045|11310->7072|11351->7074|11411->7097|11428->7104|11461->7114|11514->7134|11595->7178|11634->7207|11675->7209|11824->7321|11841->7328|11901->7365|11965->7392|11982->7399|12012->7419|12028->7425|12039->7451|12067->7469|12108->7471|12247->7573|12274->7577|12334->7600|12361->7604|12416->7664|12438->7676|12465->7701|12524->7723|12576->7765|12617->7767|12726->7839|12743->7846|12788->7868|12847->7894|12906->7916|12964->7964|13005->7966|13121->8045|13138->8052|13204->8095|13263->8121|13322->8143|13377->8188|13418->8190|13531->8266|13548->8273|13611->8313|13670->8339|13740->8376|13791->8390|13885->8474|13927->8476|14020->8532|14034->8536|14098->8576|14151->8596|14202->8610|14226->8624|14267->8626|14762->9084|14776->9088|14790->9092|14836->9099|14928->9154|14941->9157|14969->9162|15091->9247|15104->9250|15132->9255|15279->9365|15292->9368|15320->9373|15411->9427|15424->9430|15451->9434|15535->9485|15891->9808|16024->9904|16041->9911|16084->9931|16284->10102|16314->10103|16395->10155|16425->10156|16507->10209|16537->10210|16618->10262|16648->10263|16723->10309|16753->10310|18389->11917|18419->11918|18491->11961|18521->11962|18611->12023|18641->12024|18772->12126|18802->12127|18837->12133|18867->12134|18964->12202|18994->12203|19083->12263|19113->12264|19243->12365|19273->12366|19308->12372|19338->12373|19434->12440|19464->12441|19703->12643|19720->12650|19746->12653|19798->12676|19828->12677|19907->12727|19937->12728|20036->12798|20066->12799|20721->13425|20751->13426|20853->13499|20883->13500|21148->13736|21178->13737|21319->13849|21349->13850|21395->13867|21425->13868|21469->13883|21499->13884|21573->13929|21603->13930|22186->14484|22216->14485|22287->14527|22317->14528|22422->14604|22452->14605|22627->14751|22657->14752|22735->14801|22765->14802|22940->14948|22970->14949|23077->15027|23107->15028|23280->15172|23310->15173|23387->15221|23417->15222|23590->15366|23620->15367|23699->15417|23729->15418|24248->15908|24278->15909|24353->15955|24383->15956|24674->16218|24704->16219|24820->16306|24850->16307|24915->16343|24945->16344|25044->16414|25074->16415|25363->16675|25393->16676|25541->16795|25571->16796|25606->16802|25636->16803|25796->16934|25826->16935|25872->16952|25902->16953|25946->16968|25976->16969|26043->17007|26073->17008|26201->17107|26231->17108|26305->17153|26335->17154|26483->17273|26513->17274|26592->17324|26622->17325|26721->17395|26751->17396|27387->18003|27417->18004|27519->18077|27549->18078|27810->18310|27840->18311|27981->18423|28011->18424|28057->18441|28087->18442|28131->18457|28161->18458|28233->18501|28263->18502|28364->18574|28394->18575|28475->18627|28505->18628|28604->18698|28634->18699|28811->18847|28841->18848|28943->18921|28973->18922|29237->19157|29267->19158|29408->19270|29438->19271|29484->19288|29514->19289|29558->19304|29588->19305
                    LINES: 20->1|23->1|24->2|24->2|24->2|25->3|25->3|25->3|26->4|26->4|26->4|27->5|27->5|27->5|28->6|28->6|28->6|32->10|32->10|32->10|34->12|34->12|34->12|35->13|35->13|35->13|35->14|35->14|35->14|36->15|36->15|36->15|37->16|37->16|37->16|38->17|38->17|38->17|42->21|42->21|42->21|44->23|44->23|44->23|46->25|47->26|47->26|47->26|47->26|47->26|47->26|48->27|48->27|48->27|50->29|50->29|50->29|52->31|71->50|71->50|71->50|73->52|74->54|74->54|74->55|78->59|78->59|78->59|80->61|80->61|80->61|82->63|82->63|82->63|83->64|83->64|83->64|85->66|85->66|91->72|91->72|96->77|96->77|96->77|98->79|98->79|101->82|101->82|103->84|104->85|106->87|106->87|106->87|106->87|106->88|106->88|106->88|108->91|108->91|108->91|109->92|109->92|109->92|109->92|109->92|109->92|110->93|110->93|110->93|111->95|111->95|111->95|112->96|112->96|112->96|112->96|112->96|112->96|113->97|113->97|113->97|114->99|114->99|114->99|115->100|115->100|115->100|115->100|115->100|115->100|116->101|116->101|116->101|117->103|117->103|117->103|118->104|118->104|118->104|118->104|118->104|118->104|119->105|119->105|119->105|120->107|120->107|120->107|121->108|121->108|121->108|121->108|121->108|121->108|122->109|122->109|122->109|123->111|125->113|125->113|125->113|126->114|126->114|126->114|127->115|129->117|129->117|129->117|132->120|132->120|132->120|134->122|134->122|134->122|134->122|134->123|134->123|134->123|136->125|136->125|136->125|136->125|137->127|137->127|137->128|138->129|138->129|138->129|140->131|140->131|140->131|141->132|142->133|142->133|142->133|144->135|144->135|144->135|145->136|146->137|146->137|146->137|148->139|148->139|148->139|149->140|151->142|152->143|152->143|152->143|153->144|153->144|153->144|154->145|155->146|155->146|155->146|163->154|163->154|163->154|163->154|164->155|164->155|164->155|165->156|165->156|165->156|166->157|166->157|166->157|167->158|167->158|167->158|169->160|174->165|180->171|180->171|180->171|184->175|184->175|186->177|186->177|187->178|187->178|189->180|189->180|190->181|190->181|211->202|211->202|213->204|213->204|214->205|214->205|217->208|217->208|217->208|217->208|219->210|219->210|220->211|220->211|223->214|223->214|223->214|223->214|225->216|225->216|228->219|228->219|228->219|228->219|228->219|230->221|230->221|231->222|231->222|244->235|244->235|245->236|245->236|248->239|248->239|250->241|250->241|251->242|251->242|252->243|252->243|254->245|254->245|266->257|266->257|268->259|268->259|269->260|269->260|272->263|272->263|272->263|272->263|275->266|275->266|277->268|277->268|280->271|280->271|280->271|280->271|283->274|283->274|285->276|285->276|294->285|294->285|296->287|296->287|301->292|301->292|303->294|303->294|304->295|304->295|306->297|306->297|310->301|310->301|312->303|312->303|312->303|312->303|314->305|314->305|315->306|315->306|316->307|316->307|318->309|318->309|320->311|320->311|322->313|322->313|324->315|324->315|326->317|326->317|327->318|327->318|335->326|335->326|336->327|336->327|339->330|339->330|341->332|341->332|342->333|342->333|343->334|343->334|345->336|345->336|346->337|346->337|348->339|348->339|349->340|349->340|352->343|352->343|353->344|353->344|356->347|356->347|358->349|358->349|359->350|359->350|360->351|360->351
                    -- GENERATED --
                */
            