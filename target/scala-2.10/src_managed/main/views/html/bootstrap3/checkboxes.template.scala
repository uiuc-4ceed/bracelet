
package views.html.bootstrap3

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object checkboxes extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[Field,String,Map[String, Boolean],String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(field: Field, label: String = "Checkbox field", checkboxMap: Map[String, Boolean], help:String = ""):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.103*/("""
  <div class="form-group """),_display_(Seq[Any](/*2.27*/if(field.hasErrors)/*2.46*/ {_display_(Seq[Any](format.raw/*2.48*/("""has-error""")))})),format.raw/*2.58*/("""">
    <label class="col-sm-1 control-label" for=""""),_display_(Seq[Any](/*3.49*/field/*3.54*/.id)),format.raw/*3.57*/("""">"""),_display_(Seq[Any](/*3.60*/label)),format.raw/*3.65*/("""</label>
    <div class="col-sm-10" id=""""),_display_(Seq[Any](/*4.33*/field/*4.38*/.id)),format.raw/*4.41*/("""">
      """),_display_(Seq[Any](/*5.8*/for((checkboxName, isChecked) <- checkboxMap) yield /*5.53*/ {_display_(Seq[Any](format.raw/*5.55*/("""
        <label class="checkbox">
          <input
            type="checkbox"
            name=""""),_display_(Seq[Any](/*9.20*/(field.name + "[]"))),format.raw/*9.39*/(""""
            id=""""),_display_(Seq[Any](/*10.18*/checkboxName)),format.raw/*10.30*/(""""
            value=""""),_display_(Seq[Any](/*11.21*/checkboxName)),format.raw/*11.33*/(""""
            """),_display_(Seq[Any](/*12.14*/if(isChecked)/*12.27*/ {_display_(Seq[Any](format.raw/*12.29*/("""checked""")))})),format.raw/*12.37*/(""">
          """),_display_(Seq[Any](/*13.12*/checkboxName)),format.raw/*13.24*/("""
        </label>
      """)))})),format.raw/*15.8*/("""
      <span class="help-block">"""),_display_(Seq[Any](/*16.33*/help)),format.raw/*16.37*/("""</span>
      <span class="help-block">"""),_display_(Seq[Any](/*17.33*/{field.error.map { error => error.message }})),format.raw/*17.77*/("""</span>
    </div>
  </div>
"""))}
    }
    
    def render(field:Field,label:String,checkboxMap:Map[String, Boolean],help:String): play.api.templates.HtmlFormat.Appendable = apply(field,label,checkboxMap,help)
    
    def f:((Field,String,Map[String, Boolean],String) => play.api.templates.HtmlFormat.Appendable) = (field,label,checkboxMap,help) => apply(field,label,checkboxMap,help)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/bootstrap3/checkboxes.scala.html
                    HASH: 8498b2f38f1af666b9c16640bdb0c6c21fd1cf74
                    MATRIX: 637->1|833->102|895->129|922->148|961->150|1002->160|1088->211|1101->216|1125->219|1163->222|1189->227|1265->268|1278->273|1302->276|1346->286|1406->331|1445->333|1578->431|1618->450|1673->469|1707->481|1765->503|1799->515|1850->530|1872->543|1912->545|1952->553|2001->566|2035->578|2091->603|2160->636|2186->640|2262->680|2328->724
                    LINES: 20->1|23->1|24->2|24->2|24->2|24->2|25->3|25->3|25->3|25->3|25->3|26->4|26->4|26->4|27->5|27->5|27->5|31->9|31->9|32->10|32->10|33->11|33->11|34->12|34->12|34->12|34->12|35->13|35->13|37->15|38->16|38->16|39->17|39->17
                    -- GENERATED --
                */
            