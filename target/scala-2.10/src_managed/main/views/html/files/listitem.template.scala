
package views.html.files

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listitem extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template8[File,Map[UUID, Int],Call,Option[String],Option[String],Option[String],Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(file: File, comments: Map[UUID, Int], redirect: Call, dataset: Option[String], space: Option[String], folder: Option[String], showMove: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import _root_.util.Formatters.humanReadableByteCount


Seq[Any](format.raw/*1.183*/("""
"""),format.raw/*4.1*/("""<div class="panel panel-default file-panel" id=""""),_display_(Seq[Any](/*4.50*/file/*4.54*/.id)),format.raw/*4.57*/("""-listitem">
    """),_display_(Seq[Any](/*5.6*/if(!file.thumbnail_id.isEmpty)/*5.36*/{_display_(Seq[Any](format.raw/*5.37*/("""
        <div class="row">
            <div class="pull-left col-xs-12">
                <span class="glyphicon glyphicon-file"></span>
            </div>
        </div>
    """)))})),format.raw/*11.6*/("""
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-lg-2">
                """),_display_(Seq[Any](/*15.18*/if(!file.thumbnail_id.isEmpty)/*15.48*/{_display_(Seq[Any](format.raw/*15.49*/("""
                    <a href=""""),_display_(Seq[Any](/*16.31*/(routes.Files.file(file.id, dataset, space, folder)))),format.raw/*16.83*/("""">
                        <img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*17.72*/(routes.Files.thumbnail(UUID(file.thumbnail_id.toString().substring(5,file.thumbnail_id.toString().length-1)))))),format.raw/*17.183*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*17.204*/Html(file.filename))),format.raw/*17.223*/("""">
                    </a>
                """)))}/*19.19*/else/*19.24*/{_display_(Seq[Any](format.raw/*19.25*/("""
                    <a href=""""),_display_(Seq[Any](/*20.31*/(routes.Files.file(file.id, dataset, space, folder)))),format.raw/*20.83*/("""">
                        <span class="bigicon glyphicon glyphicon-file hideTree"></span>
                    </a>
                """)))})),format.raw/*23.18*/("""
            </div>

            <div class="col-md-10 col-sm-10 col-lg-10">
                <h3><a href=""""),_display_(Seq[Any](/*27.31*/(routes.Files.file(file.id, dataset, space, folder)))),format.raw/*27.83*/("""">"""),_display_(Seq[Any](/*27.86*/file/*27.90*/.filename)),format.raw/*27.99*/("""</a></h3>
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <ul class="list-unstyled">
                            <li>"""),_display_(Seq[Any](/*31.34*/(if(file.contentType != null) file.contentType else "N/A"))),format.raw/*31.92*/("""</li>
                            <li>"""),_display_(Seq[Any](/*32.34*/file/*32.38*/.uploadDate.format("MMM dd, yyyy"))),format.raw/*32.72*/("""</li>
                            <li>"""),_display_(Seq[Any](/*33.34*/humanReadableByteCount(file.length))),format.raw/*33.69*/("""</li>
                            <li class="">
                                <span class="glyphicon glyphicon-tags" title=""""),_display_(Seq[Any](/*35.80*/file/*35.84*/.tags.size)),format.raw/*35.94*/(""" tags"></span> """),_display_(Seq[Any](/*35.110*/file/*35.114*/.tags.size)),format.raw/*35.124*/("""
                                <span class="glyphicon glyphicon-list" title=""""),_display_(Seq[Any](/*36.80*/(file.metadataCount))),format.raw/*36.100*/(""" metadata fields"></span> """),_display_(Seq[Any](/*36.127*/(file.metadataCount))),format.raw/*36.147*/("""
                                <span class="glyphicon glyphicon-comment" title=""""),_display_(Seq[Any](/*37.83*/(comments.get(file.id)))),format.raw/*37.106*/(""" comments"></span> """),_display_(Seq[Any](/*37.126*/(comments.get(file.id)))),format.raw/*37.149*/("""
                                """),_display_(Seq[Any](/*38.34*/if(user.isDefined)/*38.52*/ {_display_(Seq[Any](format.raw/*38.54*/("""
                                        <!-- If user can delete, the button is enabled, otherwise the button is present but disabled to provide consistent UE. -->
                                    """),_display_(Seq[Any](/*40.38*/if(user.get.id.equals(file.author.id) || Permission.checkPermission(Permission.DeleteFile, ResourceRef(ResourceRef.file, file.id)))/*40.169*/{_display_(Seq[Any](format.raw/*40.170*/("""
                                        <button onclick="
                                                """),_display_(Seq[Any](/*42.50*/folder/*42.56*/ match/*42.62*/ {/*43.53*/case None =>/*43.65*/ {_display_(Seq[Any](format.raw/*43.67*/("""confirmDeleteResource('file','file','"""),_display_(Seq[Any](/*43.105*/(file.id))),format.raw/*43.114*/("""','"""),_display_(Seq[Any](/*43.118*/(file.filename.replace("'","&#39;")))),format.raw/*43.154*/("""',false, '"""),_display_(Seq[Any](/*43.165*/redirect)),format.raw/*43.173*/("""')""")))}/*44.53*/case Some(s) =>/*44.68*/ {_display_(Seq[Any](format.raw/*44.70*/("""confirmDeleteResource('file','file','"""),_display_(Seq[Any](/*44.108*/(file.id))),format.raw/*44.117*/("""','"""),_display_(Seq[Any](/*44.121*/(file.filename.replace("'","&#39;")))),format.raw/*44.157*/("""',false, '"""),_display_(Seq[Any](/*44.168*/redirect)),format.raw/*44.176*/("""#"""),_display_(Seq[Any](/*44.178*/s)),format.raw/*44.179*/("""')""")))}})),format.raw/*45.50*/("""
                                                " class="btn btn-link" title="Delete the file">
                                            <span class="glyphicon glyphicon-trash"></span></button>
                                    """)))}/*48.39*/else/*48.44*/{_display_(Seq[Any](format.raw/*48.45*/("""
                                        <div class="inline" title="No permission to delete the file">
                                            <button disabled class="btn btn-link"><span class="glyphicon glyphicon-trash"></span></button>
                                        </div>
                                    """)))})),format.raw/*52.38*/("""
                                """)))})),format.raw/*53.34*/("""
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4">
                        <ul class="list-unstyled">
                            """),_display_(Seq[Any](/*59.30*/if( Permission.checkPermission(Permission.DownloadFiles, ResourceRef(ResourceRef.file, file.id)))/*59.127*/ {_display_(Seq[Any](format.raw/*59.129*/("""
                                <li>
                                    <button id="downloadButton" onclick="window.open(jsRoutes.api.Files.download('"""),_display_(Seq[Any](/*61.116*/file/*61.120*/.id)),format.raw/*61.123*/("""').url, '_blank');" class="btn btn-link" title="Download and enjoy this file.">
                                        <span class="glyphicon glyphicon-save"></span> Download
                                    </button>
                                </li>
                            """)))}/*65.31*/else/*65.36*/{_display_(Seq[Any](format.raw/*65.37*/("""
                                <li>
                                    <div class="inline" title="No permission to download the file">
                                        <button disabled class="btn btn-link"><span class="glyphicon glyphicon-save"></span> Download</button>
                                    </div>
                                </li>
                            """)))})),format.raw/*71.30*/("""

                            """),_display_(Seq[Any](/*73.30*/user/*73.34*/ match/*73.40*/ {/*74.33*/case Some(viewer) =>/*74.53*/ {_display_(Seq[Any](format.raw/*74.55*/("""
                                    <li>
                                        <a
                                        id="followButton"
                                        type="button"
                                        class="btn btn-link hideTree"
                                        autocomplete="off"
                                        objectType="file"
                                        objectId=""""),_display_(Seq[Any](/*82.52*/file/*82.56*/.id.stringify)),format.raw/*82.69*/(""""
                                        >
                                        """),_display_(Seq[Any](/*84.42*/if(viewer.followedEntities.filter(x => (x.id == file.id)).nonEmpty)/*84.109*/ {_display_(Seq[Any](format.raw/*84.111*/("""
<!--                                             <span class='glyphicon glyphicon-star-empty'></span>Unfollow
 -->                                        """)))}/*86.47*/else/*86.52*/{_display_(Seq[Any](format.raw/*86.53*/("""
<!--                                             <span class='glyphicon glyphicon-star'></span>Follow
 -->                                        """)))})),format.raw/*88.46*/("""
                                        </a>
                                    </li>
                                    """),_display_(Seq[Any](/*91.38*/if(showMove && (user.get.id.equals(file.author.id) || Permission.checkPermission(Permission.EditFile, ResourceRef(ResourceRef.file, file.id))))/*91.181*/ {_display_(Seq[Any](format.raw/*91.183*/("""
                                        <a class="btn btn-link hideTree" onclick="showMoveModal('"""),_display_(Seq[Any](/*92.99*/file/*92.103*/.id)),format.raw/*92.106*/("""')"> <span class='glyphicon glyphicon-move'></span> Move </a>
                                    """)))})),format.raw/*93.38*/("""
                                """)))}/*95.33*/case None =>/*95.45*/ {}})),format.raw/*96.30*/("""


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
"""))}
    }
    
    def render(file:File,comments:Map[UUID, Int],redirect:Call,dataset:Option[String],space:Option[String],folder:Option[String],showMove:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(file,comments,redirect,dataset,space,folder,showMove)(user)
    
    def f:((File,Map[UUID, Int],Call,Option[String],Option[String],Option[String],Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (file,comments,redirect,dataset,space,folder,showMove) => (user) => apply(file,comments,redirect,dataset,space,folder,showMove)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/files/listitem.scala.html
                    HASH: b774208964eba7784bdb8422f3035bfb35c23179
                    MATRIX: 687->1|1039->182|1066->260|1150->309|1162->313|1186->316|1237->333|1275->363|1313->364|1519->539|1681->665|1720->695|1759->696|1826->727|1900->779|2010->853|2144->964|2202->985|2244->1004|2308->1050|2321->1055|2360->1056|2427->1087|2501->1139|2666->1272|2809->1379|2883->1431|2922->1434|2935->1438|2966->1447|3191->1636|3271->1694|3346->1733|3359->1737|3415->1771|3490->1810|3547->1845|3710->1972|3723->1976|3755->1986|3808->2002|3822->2006|3855->2016|3971->2096|4014->2116|4078->2143|4121->2163|4240->2246|4286->2269|4343->2289|4389->2312|4459->2346|4486->2364|4526->2366|4763->2567|4904->2698|4944->2699|5088->2807|5103->2813|5118->2819|5129->2874|5150->2886|5190->2888|5265->2926|5297->2935|5338->2939|5397->2975|5445->2986|5476->2994|5498->3050|5522->3065|5562->3067|5637->3105|5669->3114|5710->3118|5769->3154|5817->3165|5848->3173|5887->3175|5911->3176|5947->3229|6201->3465|6214->3470|6253->3471|6611->3797|6677->3831|6946->4064|7053->4161|7094->4163|7284->4316|7298->4320|7324->4323|7632->4613|7645->4618|7684->4619|8107->5010|8174->5041|8187->5045|8202->5051|8213->5086|8242->5106|8282->5108|8753->5543|8766->5547|8801->5560|8922->5645|8999->5712|9040->5714|9215->5871|9228->5876|9267->5877|9447->6025|9608->6150|9761->6293|9802->6295|9937->6394|9951->6398|9977->6401|10108->6500|10161->6567|10182->6579|10208->6612
                    LINES: 20->1|26->1|27->4|27->4|27->4|27->4|28->5|28->5|28->5|34->11|38->15|38->15|38->15|39->16|39->16|40->17|40->17|40->17|40->17|42->19|42->19|42->19|43->20|43->20|46->23|50->27|50->27|50->27|50->27|50->27|54->31|54->31|55->32|55->32|55->32|56->33|56->33|58->35|58->35|58->35|58->35|58->35|58->35|59->36|59->36|59->36|59->36|60->37|60->37|60->37|60->37|61->38|61->38|61->38|63->40|63->40|63->40|65->42|65->42|65->42|65->43|65->43|65->43|65->43|65->43|65->43|65->43|65->43|65->43|65->44|65->44|65->44|65->44|65->44|65->44|65->44|65->44|65->44|65->44|65->44|65->45|68->48|68->48|68->48|72->52|73->53|79->59|79->59|79->59|81->61|81->61|81->61|85->65|85->65|85->65|91->71|93->73|93->73|93->73|93->74|93->74|93->74|101->82|101->82|101->82|103->84|103->84|103->84|105->86|105->86|105->86|107->88|110->91|110->91|110->91|111->92|111->92|111->92|112->93|113->95|113->95|113->96
                    -- GENERATED --
                */
            