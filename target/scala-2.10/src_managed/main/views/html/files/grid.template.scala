
package views.html.files

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object grid extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[List[File],Map[UUID, Int],UUID,Option[String],ResourceRef,Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(filesList: List[File], fileComments: Map[UUID, Int], dsId: UUID, space: Option[String], parent: ResourceRef, showMove: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.166*/("""

"""),_display_(Seq[Any](/*3.2*/util/*3.6*/.masonry())),format.raw/*3.16*/("""

<div class="row" id="tile-view">
    <div class="col-md-12">

        """),_display_(Seq[Any](/*8.10*/filesList/*8.19*/.map/*8.23*/ { file =>_display_(Seq[Any](format.raw/*8.33*/("""
            """),_display_(Seq[Any](/*9.14*/if(parent.resourceType == ResourceRef.folder)/*9.59*/ {_display_(Seq[Any](format.raw/*9.61*/("""
                """),_display_(Seq[Any](/*10.18*/files/*10.23*/.listitem(file, fileComments, (routes.Datasets.dataset(dsId)), Some(dsId.stringify), space, Some(parent.id.stringify), showMove))),format.raw/*10.151*/("""
            """)))}/*11.15*/else/*11.20*/{_display_(Seq[Any](format.raw/*11.21*/("""
                """),_display_(Seq[Any](/*12.18*/files/*12.23*/.listitem(file, fileComments, (routes.Datasets.dataset(dsId)), Some(dsId.stringify), space, None,showMove))),format.raw/*12.129*/("""
            """)))})),format.raw/*13.14*/("""

        """)))})),format.raw/*15.10*/("""

    </div>
</div>
"""))}
    }
    
    def render(filesList:List[File],fileComments:Map[UUID, Int],dsId:UUID,space:Option[String],parent:ResourceRef,showMove:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(filesList,fileComments,dsId,space,parent,showMove)(user)
    
    def f:((List[File],Map[UUID, Int],UUID,Option[String],ResourceRef,Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (filesList,fileComments,dsId,space,parent,showMove) => (user) => apply(filesList,fileComments,dsId,space,parent,showMove)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/files/grid.scala.html
                    HASH: 61797d95d179383709e2b7e60c485e517ec6ad4e
                    MATRIX: 671->1|930->165|967->168|978->172|1009->182|1117->255|1134->264|1146->268|1193->278|1242->292|1295->337|1334->339|1388->357|1402->362|1553->490|1586->505|1599->510|1638->511|1692->529|1706->534|1835->640|1881->654|1924->665
                    LINES: 20->1|23->1|25->3|25->3|25->3|30->8|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|33->11|33->11|33->11|34->12|34->12|34->12|35->13|37->15
                    -- GENERATED --
                */
            