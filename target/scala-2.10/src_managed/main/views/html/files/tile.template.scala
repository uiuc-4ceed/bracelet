
package views.html.files

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[models.File,String,Call,Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(file: models.File, classes: String, redirect: Call, showFollow: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters.humanReadableByteCount

import api.Permission


Seq[Any](format.raw/*1.111*/("""

"""),format.raw/*5.1*/("""
<div class="post-box """),_display_(Seq[Any](/*6.23*/classes)),format.raw/*6.30*/("""" id=""""),_display_(Seq[Any](/*6.37*/file/*6.41*/.id)),format.raw/*6.44*/("""-tile">
    <div class="panel panel-default file-panel">
        <div class="pull-left">
            <span class="glyphicon glyphicon-file"></span>
        </div>
        <div class="panel-body">
            """),_display_(Seq[Any](/*12.14*/if(!file.thumbnail_id.isEmpty)/*12.44*/{_display_(Seq[Any](format.raw/*12.45*/("""
                <a href=""""),_display_(Seq[Any](/*13.27*/(routes.Files.file(file.id)))),format.raw/*13.55*/("""">
                    <img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*14.68*/(routes.Files.thumbnail(UUID(file.thumbnail_id.toString().substring(5,file.thumbnail_id.toString().length-1)))))),format.raw/*14.179*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*14.200*/(file.filename))),format.raw/*14.215*/("""">
                </a>
            """)))})),format.raw/*16.14*/("""
            <div class="caption break-word">
                <h4><a href=""""),_display_(Seq[Any](/*18.31*/(routes.Files.file(file.id)))),format.raw/*18.59*/("""">"""),_display_(Seq[Any](/*18.62*/file/*18.66*/.filename)),format.raw/*18.75*/("""</a></h4>
                <ul class="list-unstyled">
                    <li>Author: <strong>"""),_display_(Seq[Any](/*20.42*/file/*20.46*/.author.fullName)),format.raw/*20.62*/("""</strong></li>
                    <li>Uploaded: <strong>"""),_display_(Seq[Any](/*21.44*/file/*21.48*/.uploadDate.format("dd MMM, yyyy"))),format.raw/*21.82*/("""</strong></li>
                    <li>Size: <strong>"""),_display_(Seq[Any](/*22.40*/humanReadableByteCount(file.length))),format.raw/*22.75*/("""</strong></li>
                </ul>
            </div>
        </div>
        <ul class="list-group">
            <li class="list-group-item file-panel-footer">
                <span class="glyphicon glyphicon glyphicon-tags" title=""""),_display_(Seq[Any](/*28.74*/file/*28.78*/.tags.size)),format.raw/*28.88*/(""" tags"></span> """),_display_(Seq[Any](/*28.104*/file/*28.108*/.tags.size)),format.raw/*28.118*/("""
                <span class="glyphicon glyphicon glyphicon-list" title=""""),_display_(Seq[Any](/*29.74*/(file.metadataCount))),format.raw/*29.94*/(""" metadata fields"></span> """),_display_(Seq[Any](/*29.121*/(file.metadataCount))),format.raw/*29.141*/("""
                """),_display_(Seq[Any](/*30.18*/if(user.isDefined)/*30.36*/ {_display_(Seq[Any](format.raw/*30.38*/("""
                    """),_display_(Seq[Any](/*31.22*/if(user.get.id.equals(file.author.id) || Permission.checkPermission(Permission.DeleteFile, ResourceRef(ResourceRef.file, file.id)))/*31.153*/{_display_(Seq[Any](format.raw/*31.154*/("""
                        <button onclick="confirmDeleteResource('file','file','"""),_display_(Seq[Any](/*32.80*/(file.id))),format.raw/*32.89*/("""','"""),_display_(Seq[Any](/*32.93*/(file.filename.replace("'","&#39;")))),format.raw/*32.129*/("""', false, '"""),_display_(Seq[Any](/*32.141*/redirect)),format.raw/*32.149*/("""')" class="btn btn-link" title="Delete the file">
                        <span class="glyphicon glyphicon-trash"></span></button>
                    """)))}/*34.23*/else/*34.28*/{_display_(Seq[Any](format.raw/*34.29*/("""
                        <div class="inline" title="No permission to delete the file">
                            <button disabled class="btn btn-link"><span class="glyphicon glyphicon-trash"></span></button>
                        </div>
                    """)))})),format.raw/*38.22*/("""
                """)))})),format.raw/*39.18*/("""
            </li>
        </ul>
        """),_display_(Seq[Any](/*42.10*/if(showFollow)/*42.24*/ {_display_(Seq[Any](format.raw/*42.26*/("""
            """),_display_(Seq[Any](/*43.14*/user/*43.18*/ match/*43.24*/ {/*44.17*/case Some(viewer) =>/*44.37*/ {_display_(Seq[Any](format.raw/*44.39*/("""
                    <ul class="list-group center-margin">
                        <a
                        id="followButton"
                        type="button"
                        class="btn-link"
                        data-toggle="button"
                        aria-pressed="
                            """),_display_(Seq[Any](/*52.30*/if(viewer.followedEntities.filter(x => (x.id == file.id)).nonEmpty)/*52.97*/ {_display_(Seq[Any](format.raw/*52.99*/("""
                                true
                            """)))}/*54.31*/else/*54.36*/{_display_(Seq[Any](format.raw/*54.37*/("""
                                false
                            """)))})),format.raw/*56.30*/("""
                        "
                        autocomplete="off"
                        objectType="file"
                        objectId=""""),_display_(Seq[Any](/*60.36*/file/*60.40*/.id.stringify)),format.raw/*60.53*/(""""
                        >
                        """),_display_(Seq[Any](/*62.26*/if(viewer.followedEntities.filter(x => (x.id == file.id)).nonEmpty)/*62.93*/ {_display_(Seq[Any](format.raw/*62.95*/("""
                            <span class='glyphicon glyphicon-star-empty'></span>Unfollow
                        """)))}/*64.27*/else/*64.32*/{_display_(Seq[Any](format.raw/*64.33*/("""
                            <span class='glyphicon glyphicon-star'></span>Follow
                        """)))})),format.raw/*66.26*/("""
                        </a>
                    </ul>
                """)))}})),format.raw/*70.14*/("""
        """)))})),format.raw/*71.10*/("""

    </div>
</div>"""))}
    }
    
    def render(file:models.File,classes:String,redirect:Call,showFollow:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(file,classes,redirect,showFollow)(user)
    
    def f:((models.File,String,Call,Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (file,classes,redirect,showFollow) => (user) => apply(file,classes,redirect,showFollow)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/files/tile.scala.html
                    HASH: affd676bfa3cfa529c0f57d17fb0e5b9048d5c30
                    MATRIX: 637->1|917->110|945->189|1003->212|1031->219|1073->226|1085->230|1109->233|1354->442|1393->472|1432->473|1495->500|1545->528|1651->598|1785->709|1843->730|1881->745|1950->782|2062->858|2112->886|2151->889|2164->893|2195->902|2325->996|2338->1000|2376->1016|2470->1074|2483->1078|2539->1112|2629->1166|2686->1201|2957->1436|2970->1440|3002->1450|3055->1466|3069->1470|3102->1480|3212->1554|3254->1574|3318->1601|3361->1621|3415->1639|3442->1657|3482->1659|3540->1681|3681->1812|3721->1813|3837->1893|3868->1902|3908->1906|3967->1942|4016->1954|4047->1962|4218->2115|4231->2120|4270->2121|4564->2383|4614->2401|4692->2443|4715->2457|4755->2459|4805->2473|4818->2477|4833->2483|4844->2502|4873->2522|4913->2524|5269->2844|5345->2911|5385->2913|5471->2981|5484->2986|5523->2987|5623->3055|5806->3202|5819->3206|5854->3219|5943->3272|6019->3339|6059->3341|6193->3457|6206->3462|6245->3463|6384->3570|6490->3657|6532->3667
                    LINES: 20->1|26->1|28->5|29->6|29->6|29->6|29->6|29->6|35->12|35->12|35->12|36->13|36->13|37->14|37->14|37->14|37->14|39->16|41->18|41->18|41->18|41->18|41->18|43->20|43->20|43->20|44->21|44->21|44->21|45->22|45->22|51->28|51->28|51->28|51->28|51->28|51->28|52->29|52->29|52->29|52->29|53->30|53->30|53->30|54->31|54->31|54->31|55->32|55->32|55->32|55->32|55->32|55->32|57->34|57->34|57->34|61->38|62->39|65->42|65->42|65->42|66->43|66->43|66->43|66->44|66->44|66->44|74->52|74->52|74->52|76->54|76->54|76->54|78->56|82->60|82->60|82->60|84->62|84->62|84->62|86->64|86->64|86->64|88->66|91->70|92->71
                    -- GENERATED --
                */
            