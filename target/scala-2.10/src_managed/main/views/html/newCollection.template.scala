
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object newCollection extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template8[String,List[models.ProjectSpace],Boolean,Boolean,Option[String],Option[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(errorString: String, spaceList: List[models.ProjectSpace], isNameRequired: Boolean, isDescriptionRequired: Boolean, spaceId: Option[String], spaceName: Option[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import helper._

import _root_.util.Formatters._

implicit def /*6.2*/implicitFieldConstructor/*6.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.233*/("""
"""),format.raw/*5.1*/("""
"""),format.raw/*6.75*/("""

"""),_display_(Seq[Any](/*8.2*/main(Messages("create.title", Messages("collection.title")))/*8.62*/ {_display_(Seq[Any](format.raw/*8.64*/("""

    <style>

    </style>

  <!-- Custom items for the create collection workflow -->
  <script src=""""),_display_(Seq[Any](/*15.17*/routes/*15.23*/.Assets.at("javascripts/collection-create.js"))),format.raw/*15.69*/("""" type="text/javascript"></script>
  <script src=""""),_display_(Seq[Any](/*16.17*/routes/*16.23*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*16.68*/("""" language="javascript"></script>
  <script type="text/javascript" language="javascript">
      //Global so that the javascript for the collection creation can reference this.
      var isNameRequired = """),_display_(Seq[Any](/*19.29*/isNameRequired)),format.raw/*19.43*/(""";
      var isDescRequired = """),_display_(Seq[Any](/*20.29*/isDescriptionRequired)),format.raw/*20.50*/(""";           
  </script>
    <script src=""""),_display_(Seq[Any](/*22.19*/routes/*22.25*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*22.67*/("""" type="text/javascript"></script>
    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*23.35*/routes/*23.41*/.Assets.at("stylesheets/chosen.css"))),format.raw/*23.77*/("""">
    <ol class="breadcrumb navigate-bread">

        """),_display_(Seq[Any](/*26.10*/(spaceId, spaceName)/*26.30*/ match/*26.36*/ {/*27.13*/case (Some(id), Some(name)) =>/*27.43*/ {_display_(Seq[Any](format.raw/*27.45*/("""
                <li><span class="glyphicon glyphicon-hdd"></span> <a href=""""),_display_(Seq[Any](/*28.77*/routes/*28.83*/.Spaces.getSpace(UUID(id)))),format.raw/*28.109*/("""" title=""""),_display_(Seq[Any](/*28.119*/name)),format.raw/*28.123*/(""""> """),_display_(Seq[Any](/*28.127*/Html(ellipsize(name, 18)))),format.raw/*28.152*/("""</a></li>

            """)))}/*31.13*/case (_, _) =>/*31.27*/ {_display_(Seq[Any](format.raw/*31.29*/("""
                """),_display_(Seq[Any](/*32.18*/user/*32.22*/ match/*32.28*/ {/*33.21*/case Some(u) =>/*33.36*/ {_display_(Seq[Any](format.raw/*33.38*/(""" <li><span class="glyphicon glyphicon-user"></span> <a href= """"),_display_(Seq[Any](/*33.101*/routes/*33.107*/.Profile.viewProfileUUID(u.id))),format.raw/*33.137*/(""""> """),_display_(Seq[Any](/*33.141*/u/*33.142*/.fullName)),format.raw/*33.151*/(""" </a> </li>""")))}/*34.21*/case None =>/*34.33*/ {}})),format.raw/*35.18*/("""
            """)))}})),format.raw/*37.10*/("""
        <li> <span class="glyphicon glyphicon-th-large"></span> """),_display_(Seq[Any](/*38.66*/Messages("create.header", Messages("collection.title")))),format.raw/*38.121*/("""</li>

    </ol>
  <div class="page-header">
    <h1>"""),_display_(Seq[Any](/*42.10*/Messages("create.header", Messages("collection.title")))),format.raw/*42.65*/("""</h1>
      <p>"""),_display_(Seq[Any](/*43.11*/Messages("collection.create.message", Messages("collections.title"), Messages("datasets.title").toLowerCase, Messages("collection.title").toLowerCase, Messages("space.title")))),format.raw/*43.186*/("""</p>

  </div>
  <div class="row">
    <div class="col-md-12">
      <span id="status" class="success hiddencomplete alert alert-success" role="alert">A Status Message</span>
      """),_display_(Seq[Any](/*49.8*/if(errorString != null)/*49.31*/{_display_(Seq[Any](format.raw/*49.32*/("""
        <span class="error alert alert-danger" id="messageerror">"""),_display_(Seq[Any](/*50.67*/errorString)),format.raw/*50.78*/("""</span>
      """)))}/*51.8*/else/*51.12*/{_display_(Seq[Any](format.raw/*51.13*/("""
        <span class="error hiddencomplete alert alert-danger" id="messageerror">An Error Message</span>
      """)))})),format.raw/*53.8*/("""
    </div>
    <!-- Basic required elements for creating a new collection. -->
    <div class="form-group">
            <label id="namelabel" for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="A short name">
            <span class="error hiddencomplete" id="nameerror">The name is a required field</span>
        </div>
        <div class="form-group">
            <label id="desclabel" for="description">Description</label>
            <textarea cols=40 rows=4 type="text" id="description" class="form-control"
                placeholder="A longer description"></textarea>
            <span class="error hiddencomplete" id="descerror">This description is a required field</span>
        </div>
        <div class="form-group">
                    <label id="spacelabel" for="space">Share with
                        """),_display_(Seq[Any](/*69.26*/if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*69.97*/ {_display_(Seq[Any](format.raw/*69.99*/("""
                            """),_display_(Seq[Any](/*70.30*/Messages("spaces.title"))),format.raw/*70.54*/("""
                         """)))}/*71.28*/else/*71.33*/{_display_(Seq[Any](format.raw/*71.34*/("""
                            a """),_display_(Seq[Any](/*72.32*/Messages("space.title"))),format.raw/*72.55*/("""
                        """)))})),format.raw/*73.26*/("""
                    </label>
                    <select name="space" id="spaceid" class = "form-control chosen-select" """),_display_(Seq[Any](/*75.93*/if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*75.164*/{_display_(Seq[Any](format.raw/*75.165*/("""multiple""")))})),format.raw/*75.174*/(""" style="width: 342px;">
                        """),_display_(Seq[Any](/*76.26*/if(!play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*76.98*/ {_display_(Seq[Any](format.raw/*76.100*/("""
                            <option value="">Select a """),_display_(Seq[Any](/*77.56*/Messages("space.title"))),format.raw/*77.79*/(""" to share the collection with (Optional)</option>
                        """)))})),format.raw/*78.26*/("""
                        """),_display_(Seq[Any](/*79.26*/spaceList/*79.35*/.map/*79.39*/ { space =>_display_(Seq[Any](format.raw/*79.50*/("""
                            <option id = """"),_display_(Seq[Any](/*80.44*/(space.id))),format.raw/*80.54*/("""" value=""""),_display_(Seq[Any](/*80.64*/(space.id))),format.raw/*80.74*/("""">"""),_display_(Seq[Any](/*80.77*/(space.name))),format.raw/*80.89*/("""</option>
                        """)))})),format.raw/*81.26*/("""
                    </select>
                    """),_display_(Seq[Any](/*83.22*/if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*83.93*/ {_display_(Seq[Any](format.raw/*83.95*/("""
                        <p class="help-block">"""),_display_(Seq[Any](/*84.48*/Messages("create.share.multiple.message", Messages("collection.title").toLowerCase, Messages("spaces.title")))),format.raw/*84.157*/("""</p>
                    """)))}/*85.23*/else/*85.28*/{_display_(Seq[Any](format.raw/*85.29*/("""
                        <p class="help-block">"""),_display_(Seq[Any](/*86.48*/Messages("create.share.one.message", Messages("collection.title").toLowerCase, Messages("space.title")))),format.raw/*86.151*/("""</p>
                    """)))})),format.raw/*87.22*/("""
                </div>
    <!-- <table style="width: 100%; margin-bottom: 40px; margin-top: 40px;">
        <tr>
            <td style="width: 20%; vertical-align:top;" class="input-table-cell">Name:</td>
            <td style="width: 80%; vertical-align:top;" class="input-table-cell">
                <textarea class="form-control" cols=40 rows=4 type="text" id="name"></textarea>
                <span class="error hiddencomplete" id="nameerror"> The name is a required field</span><br>
            </td>
            <td style="vertical-align:top;" class="input-table-cell">

            </td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align:top;" class="input-table-cell">Description:</td>
            <td style="width: 80%; vertical-align:top;" class="input-table-cell" id="desccell">
                <textarea class="form-control" cols=40 rows=4 type="text" id="description"></textarea>
                <span class="error hiddencomplete" id="descerror"> This description is a required field</span><br>
            </td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align:top;" class="input-table-cell">Share with """),_display_(Seq[Any](/*108.94*/Messages("spaces.title"))),format.raw/*108.118*/(""":</td>
            <td style="width: 35%; vertical-align:top;" class="input-table-cell" id="desccell">
                <select name="space" id="spaceid" class = "form-control chosen-select" """),_display_(Seq[Any](/*110.89*/if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*110.160*/{_display_(Seq[Any](format.raw/*110.161*/("""multiple""")))})),format.raw/*110.170*/(""" style="width: 342px;">
                    """),_display_(Seq[Any](/*111.22*/if(!play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*111.94*/ {_display_(Seq[Any](format.raw/*111.96*/("""
                        <option value="">Select a """),_display_(Seq[Any](/*112.52*/Messages("space.title"))),format.raw/*112.75*/(""" to share the collection with (Optional)</option>
                    """)))})),format.raw/*113.22*/("""
                    """),_display_(Seq[Any](/*114.22*/spaceList/*114.31*/.map/*114.35*/ { space =>_display_(Seq[Any](format.raw/*114.46*/("""
                        <option id = """"),_display_(Seq[Any](/*115.40*/(space.id))),format.raw/*115.50*/("""" value=""""),_display_(Seq[Any](/*115.60*/(space.id))),format.raw/*115.70*/("""">"""),_display_(Seq[Any](/*115.73*/(space.name))),format.raw/*115.85*/("""</option>
                    """)))})),format.raw/*116.22*/("""
                </select>
                """),_display_(Seq[Any](/*118.18*/if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*118.89*/ {_display_(Seq[Any](format.raw/*118.91*/("""
                    <p class="help-block">"""),_display_(Seq[Any](/*119.44*/Messages("create.share.multiple.message", Messages("collection.title").toLowerCase, Messages("spaces.title")))),format.raw/*119.153*/("""</p>
                """)))}/*120.19*/else/*120.24*/{_display_(Seq[Any](format.raw/*120.25*/("""
                    <p class="help-block">"""),_display_(Seq[Any](/*121.44*/Messages("create.share.one.message", Messages("collection.title").toLowerCase, Messages("space.title")))),format.raw/*121.147*/("""</p>
                """)))})),format.raw/*122.18*/("""
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <button style="margin-top: 10px; margin-right: 10px;" class="btn btn-primary" title="Create the collection" onclick="return createCollection();">
                  <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*130.65*/Messages("create.title", ""))),format.raw/*130.93*/("""
                </button>
                <button style="margin-top: 10px;" class="btn btn-default" title="Clear the input fields" onclick="return clearFields();">
                  <span class="glyphicon glyphicon-unchecked"></span> Reset
                </button>
            </td>
        </tr>
    </table> -->
    <div class="row create-row visible-lg">
            <div class="col-md-12 create-col">
                <button style="margin-left: 10px;" type="submit" class="btn btn-primary start" title="Create the collection" onclick="return createCollection();">
                    <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*141.67*/Messages("create.title", ""))),format.raw/*141.95*/("""
                </button>
                <button class="btn btn-default" title="Clear the input fields" onclick="return clearFields();">
                    <span class="glyphicon glyphicon-unchecked"></span> Reset
                </button>
            </div>
        </div>

      <div class="row create-row visible-xs visible sm">
          <div class="col-md-12 create-col">
              <button style="margin-left: 10px;" type="submit" class="btn btn-primary start" title="Create the collection" onclick="return createCollectionDiffRedirect();">
                  <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*152.65*/Messages("create.title", ""))),format.raw/*152.93*/("""
              </button>
              <button class="btn btn-default" title="Clear the input fields" onclick="return clearFields();">
                  <span class="glyphicon glyphicon-unchecked"></span> Reset
              </button>
          </div>
      </div>

    <form id="collectioncreate" action='"""),_display_(Seq[Any](/*160.42*/routes/*160.48*/.Collections.submit)),format.raw/*160.67*/("""' method="POST" enctype="multipart/form-data">
            <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript>Javascript is required in order to use the uploader to create a new collection.</noscript>

        <input type="hidden" name="name" id="hiddenname" value="not set">
        <input type="hidden" name="description" id="hiddendescription" value="not set">
        <input type="hidden" name="space" id="hiddenspace" value="not set">
        <input type="hidden" name="redirect" id="hiddenredirect" value="not set">
    </form>
    </div>


  </div>
<script language="javascript">
    $(".chosen-select").chosen("""),format.raw/*174.32*/("""{"""),format.raw/*174.33*/("""
        width: "100%",
        placeholder_text_multiple: "(optional)"
    """),format.raw/*177.5*/("""}"""),format.raw/*177.6*/(""");
</script>
<script language = "javascript">
    var idNameObj = """),format.raw/*180.21*/("""{"""),format.raw/*180.22*/("""}"""),format.raw/*180.23*/(""";

    function isUndefined(value) """),format.raw/*182.33*/("""{"""),format.raw/*182.34*/("""
        return typeof value === 'undefined';
    """),format.raw/*184.5*/("""}"""),format.raw/*184.6*/("""

    function getName(id) """),format.raw/*186.26*/("""{"""),format.raw/*186.27*/("""
        return idNameObj[id];
    """),format.raw/*188.5*/("""}"""),format.raw/*188.6*/("""

    $(document).ready(function() """),format.raw/*190.34*/("""{"""),format.raw/*190.35*/("""
        """),_display_(Seq[Any](/*191.10*/for(space <- spaceList) yield /*191.33*/ {_display_(Seq[Any](format.raw/*191.35*/("""
            idNameObj[""""),_display_(Seq[Any](/*192.25*/space/*192.30*/.name)),format.raw/*192.35*/(""""] = """"),_display_(Seq[Any](/*192.42*/space/*192.47*/.id)),format.raw/*192.50*/("""";

        """)))})),format.raw/*194.10*/("""
        $(".chosen-choices").addClass("form-control");
    """),format.raw/*196.5*/("""}"""),format.raw/*196.6*/(""");

    $(".chosen-select").chosen().change(function(event, params) """),format.raw/*198.65*/("""{"""),format.raw/*198.66*/("""
    var targetId = this.getAttribute("id");
    if(!isUndefined(params.selected)) """),format.raw/*200.39*/("""{"""),format.raw/*200.40*/("""
        var addedId = params.selected;
        """),_display_(Seq[Any](/*202.10*/for(space <- spaceList) yield /*202.33*/ {_display_(Seq[Any](format.raw/*202.35*/("""
            var currentId = """"),_display_(Seq[Any](/*203.31*/space/*203.36*/.id)),format.raw/*203.39*/("""";
            if(currentId != targetId) """),format.raw/*204.39*/("""{"""),format.raw/*204.40*/("""
                $("#"""),_display_(Seq[Any](/*205.22*/space/*205.27*/.id)),format.raw/*205.30*/(""" option [value="+addedId +']').remove();
                $('#"""),_display_(Seq[Any](/*206.22*/space/*206.27*/.id)),format.raw/*206.30*/("""').trigger("chosen:updated");
            """),format.raw/*207.13*/("""}"""),format.raw/*207.14*/("""
        """)))})),format.raw/*208.10*/("""
        $('ul.chosen-choices li.search-field').css("width", "0");

    """),format.raw/*211.5*/("""}"""),format.raw/*211.6*/("""
    else if (!isUndefined(params.deselected)) """),format.raw/*212.47*/("""{"""),format.raw/*212.48*/("""
        var removedId = params.deselected;
        """),_display_(Seq[Any](/*214.10*/for(space <- spaceList) yield /*214.33*/ {_display_(Seq[Any](format.raw/*214.35*/("""
            var currentId = """"),_display_(Seq[Any](/*215.31*/space/*215.36*/.id)),format.raw/*215.39*/("""";
            if(currentId != targetId) """),format.raw/*216.39*/("""{"""),format.raw/*216.40*/("""
                $('#"""),_display_(Seq[Any](/*217.22*/space/*217.27*/.id)),format.raw/*217.30*/("""').prepend($("<option></option>").attr("value", removedId).text(getName(removedId)));
                $('#"""),_display_(Seq[Any](/*218.22*/space/*218.27*/.id)),format.raw/*218.30*/("""').trigger("chosen:updated");
            """),format.raw/*219.13*/("""}"""),format.raw/*219.14*/("""
        """)))})),format.raw/*220.10*/("""
    """),format.raw/*221.5*/("""}"""),format.raw/*221.6*/("""
    """),format.raw/*222.5*/("""}"""),format.raw/*222.6*/(""")

    if("""),_display_(Seq[Any](/*224.9*/spaceId/*224.16*/.isDefined)),format.raw/*224.26*/(""") """),format.raw/*224.28*/("""{"""),format.raw/*224.29*/("""
        $('#"""),_display_(Seq[Any](/*225.14*/spaceId)),format.raw/*225.21*/("""').prop('selected','selected');
        $('#spaceid').trigger("chosen:updated");
    """),format.raw/*227.5*/("""}"""),format.raw/*227.6*/("""
  </script>
""")))})),format.raw/*229.2*/("""
"""))}
    }
    
    def render(errorString:String,spaceList:List[models.ProjectSpace],isNameRequired:Boolean,isDescriptionRequired:Boolean,spaceId:Option[String],spaceName:Option[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(errorString,spaceList,isNameRequired,isDescriptionRequired,spaceId,spaceName)(flash,user)
    
    def f:((String,List[models.ProjectSpace],Boolean,Boolean,Option[String],Option[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (errorString,spaceList,isNameRequired,isDescriptionRequired,spaceId,spaceName) => (flash,user) => apply(errorString,spaceList,isNameRequired,isDescriptionRequired,spaceId,spaceName)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:46 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/newCollection.scala.html
                    HASH: 90ede731f60691e525d87ab7313297ef49babf50
                    MATRIX: 706->1|1103->316|1135->340|1215->232|1242->314|1270->389|1307->392|1375->452|1414->454|1554->558|1569->564|1637->610|1724->661|1739->667|1806->712|2046->916|2082->930|2148->960|2191->981|2270->1024|2285->1030|2349->1072|2454->1141|2469->1147|2527->1183|2619->1239|2648->1259|2663->1265|2674->1280|2713->1310|2753->1312|2866->1389|2881->1395|2930->1421|2977->1431|3004->1435|3045->1439|3093->1464|3136->1501|3159->1515|3199->1517|3253->1535|3266->1539|3281->1545|3292->1568|3316->1583|3356->1585|3456->1648|3472->1654|3525->1684|3566->1688|3577->1689|3609->1698|3640->1731|3661->1743|3687->1764|3734->1788|3836->1854|3914->1909|4004->1963|4081->2018|4133->2034|4331->2209|4548->2391|4580->2414|4619->2415|4722->2482|4755->2493|4788->2508|4801->2512|4840->2513|4983->2625|5889->3495|5969->3566|6009->3568|6075->3598|6121->3622|6167->3650|6180->3655|6219->3656|6287->3688|6332->3711|6390->3737|6548->3859|6629->3930|6669->3931|6711->3940|6796->3989|6877->4061|6918->4063|7010->4119|7055->4142|7162->4217|7224->4243|7242->4252|7255->4256|7304->4267|7384->4311|7416->4321|7462->4331|7494->4341|7533->4344|7567->4356|7634->4391|7722->4443|7802->4514|7842->4516|7926->4564|8058->4673|8103->4700|8116->4705|8155->4706|8239->4754|8365->4857|8423->4883|9636->6059|9684->6083|9912->6274|9994->6345|10035->6346|10078->6355|10160->6400|10242->6472|10283->6474|10372->6526|10418->6549|10522->6620|10581->6642|10600->6651|10614->6655|10664->6666|10741->6706|10774->6716|10821->6726|10854->6736|10894->6739|10929->6751|10993->6782|11074->6826|11155->6897|11196->6899|11277->6943|11410->7052|11452->7075|11466->7080|11506->7081|11587->7125|11714->7228|11769->7250|12130->7574|12181->7602|12854->8238|12905->8266|13559->8883|13610->8911|13954->9218|13970->9224|14012->9243|14697->9899|14727->9900|14831->9976|14860->9977|14955->10043|14985->10044|15015->10045|15079->10080|15109->10081|15187->10131|15216->10132|15272->10159|15302->10160|15365->10195|15394->10196|15458->10231|15488->10232|15535->10242|15575->10265|15616->10267|15678->10292|15693->10297|15721->10302|15765->10309|15780->10314|15806->10317|15852->10330|15940->10390|15969->10391|16066->10459|16096->10460|16208->10543|16238->10544|16324->10593|16364->10616|16405->10618|16473->10649|16488->10654|16514->10657|16584->10698|16614->10699|16673->10721|16688->10726|16714->10729|16813->10791|16828->10796|16854->10799|16925->10841|16955->10842|16998->10852|17098->10924|17127->10925|17203->10972|17233->10973|17323->11026|17363->11049|17404->11051|17472->11082|17487->11087|17513->11090|17583->11131|17613->11132|17672->11154|17687->11159|17713->11162|17857->11269|17872->11274|17898->11277|17969->11319|17999->11320|18042->11330|18075->11335|18104->11336|18137->11341|18166->11342|18213->11353|18230->11360|18263->11370|18294->11372|18324->11373|18375->11387|18405->11394|18518->11479|18547->11480|18593->11494
                    LINES: 20->1|27->6|27->6|28->1|29->5|30->6|32->8|32->8|32->8|39->15|39->15|39->15|40->16|40->16|40->16|43->19|43->19|44->20|44->20|46->22|46->22|46->22|47->23|47->23|47->23|50->26|50->26|50->26|50->27|50->27|50->27|51->28|51->28|51->28|51->28|51->28|51->28|51->28|53->31|53->31|53->31|54->32|54->32|54->32|54->33|54->33|54->33|54->33|54->33|54->33|54->33|54->33|54->33|54->34|54->34|54->35|55->37|56->38|56->38|60->42|60->42|61->43|61->43|67->49|67->49|67->49|68->50|68->50|69->51|69->51|69->51|71->53|87->69|87->69|87->69|88->70|88->70|89->71|89->71|89->71|90->72|90->72|91->73|93->75|93->75|93->75|93->75|94->76|94->76|94->76|95->77|95->77|96->78|97->79|97->79|97->79|97->79|98->80|98->80|98->80|98->80|98->80|98->80|99->81|101->83|101->83|101->83|102->84|102->84|103->85|103->85|103->85|104->86|104->86|105->87|126->108|126->108|128->110|128->110|128->110|128->110|129->111|129->111|129->111|130->112|130->112|131->113|132->114|132->114|132->114|132->114|133->115|133->115|133->115|133->115|133->115|133->115|134->116|136->118|136->118|136->118|137->119|137->119|138->120|138->120|138->120|139->121|139->121|140->122|148->130|148->130|159->141|159->141|170->152|170->152|178->160|178->160|178->160|192->174|192->174|195->177|195->177|198->180|198->180|198->180|200->182|200->182|202->184|202->184|204->186|204->186|206->188|206->188|208->190|208->190|209->191|209->191|209->191|210->192|210->192|210->192|210->192|210->192|210->192|212->194|214->196|214->196|216->198|216->198|218->200|218->200|220->202|220->202|220->202|221->203|221->203|221->203|222->204|222->204|223->205|223->205|223->205|224->206|224->206|224->206|225->207|225->207|226->208|229->211|229->211|230->212|230->212|232->214|232->214|232->214|233->215|233->215|233->215|234->216|234->216|235->217|235->217|235->217|236->218|236->218|236->218|237->219|237->219|238->220|239->221|239->221|240->222|240->222|242->224|242->224|242->224|242->224|242->224|243->225|243->225|245->227|245->227|247->229
                    -- GENERATED --
                */
            