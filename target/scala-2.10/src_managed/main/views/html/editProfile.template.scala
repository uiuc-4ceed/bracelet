
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object editProfile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[Form[models.Profile],List[String],List[String],Map[String, String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[models.Profile], allInstitutionOptions: List[String], allProjectOptions: List[String], emailtimes: Map[String,String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.197*/("""

"""),format.raw/*4.75*/("""
  <script src=""""),_display_(Seq[Any](/*5.17*/routes/*5.23*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*5.68*/("""" language="javascript"></script>
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*6.31*/routes/*6.37*/.Assets.at("stylesheets/chosen.css"))),format.raw/*6.73*/("""">

"""),_display_(Seq[Any](/*8.2*/main("Edit Profile")/*8.22*/ {_display_(Seq[Any](format.raw/*8.24*/("""
  <div class="page-header">
    <h1>Edit Profile</h1>
  </div>
  <div class="row">
    <div class="col-md-12">
      """),_display_(Seq[Any](/*14.8*/if(myForm.hasErrors)/*14.28*/ {_display_(Seq[Any](format.raw/*14.30*/("""
      <div class="alert alert-error">
        <p>Please fix all errors</p>
      </div>
      """)))})),format.raw/*18.8*/("""
      """),_display_(Seq[Any](/*19.8*/flash/*19.13*/.get("error").map/*19.30*/ { message =>_display_(Seq[Any](format.raw/*19.43*/("""
        <div class="alert alert-error">
          <p>"""),_display_(Seq[Any](/*21.15*/message)),format.raw/*21.22*/("""</p>
        </div>
      """)))})),format.raw/*23.8*/("""
      """),_display_(Seq[Any](/*24.8*/form(action = routes.Profile.submitChanges, 'enctype -> "multipart/form-data", 'class -> "form-horizontal")/*24.115*/ {_display_(Seq[Any](format.raw/*24.117*/("""
        <fieldset  class="editProfileInput">
          """),_display_(Seq[Any](/*26.12*/select(myForm("institution"), options(allInstitutionOptions), '_label -> "Institution", 'class -> "chosen-select", 'placeholder -> "Select or enter an institution..."))),format.raw/*26.179*/("""
          """),_display_(Seq[Any](/*27.12*/inputText(myForm("position"), 'class -> "form-control", '_label -> "Position"))),format.raw/*27.90*/("""

          <div class="control-group">
            <label class="control-label" for="avatarUrl">Avatar URL</label><br/>
            <input type="radio" name="avatarUrl" id="grav_url_radio" value="" onclick="selectGravatar()"> Use Gravatar</input><br/>
            <input type="radio" name="avatarUrl" id="custom_url_radio" value="" onclick="updateCustomValue();"> Custom </input>
            <input type="text" id="custom_url_value" class="form-control" onkeyup="updateCustomValue();"></input>
          </div>

          """),_display_(Seq[Any](/*36.12*/inputText(myForm("orcidID"), 'class -> "form-control", '_label -> "Orcid ID", 'title -> "ORCID provides a persistent digital identifier that distinguishes you from every other researcher and, through integration in key research workflows such as manuscript and grant submission, supports automated linkages between you and your professional activities ensuring that your work is recognized."))),format.raw/*36.404*/("""
          """),_display_(Seq[Any](/*37.12*/textarea(myForm("biography"),'class -> "form-control",  '_label -> "Biography"))),format.raw/*37.91*/("""

          """),_display_(Seq[Any](/*39.12*/select(myForm("currentprojects"), options(allProjectOptions), '_label -> "Current Projects", 'multiple -> "true", 'class -> "chosen-select form-control", 'placeholder -> "Add your projects"))),format.raw/*39.202*/("""

          """),_display_(Seq[Any](/*41.12*/select(myForm("pastprojects"), options(allProjectOptions), '_label -> "Past Projects", 'multiple -> "true", 'class -> "chosen-select form-control", 'placeholder -> "Add your projects"))),format.raw/*41.196*/("""

          """),_display_(Seq[Any](/*43.12*/select(myForm("emailsettings"), options(emailtimes), '_label -> "Email Settings"))),format.raw/*43.93*/("""
          <p>An e-mail digest of activities that involve your followed objects.</p>

        </fieldset>
        """),_display_(Seq[Any](/*47.10*/if(play.api.Play.current.plugin[services.StagingAreaPlugin].isDefined)/*47.80*/ {_display_(Seq[Any](format.raw/*47.82*/("""
          <div class="profile-disclaimer small">"""),_display_(Seq[Any](/*48.50*/play/*48.54*/.api.i18n.Messages("profile.disclaimer"))),format.raw/*48.94*/("""</div>
        """)))})),format.raw/*49.10*/("""
        <div class="form-actions" style="margin-top: 16px;">
          <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-saved"></span> Submit Changes</button>
        </div>
      """)))})),format.raw/*53.8*/("""
    </div>
  </div>
<script src=""""),_display_(Seq[Any](/*56.15*/routes/*56.21*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*56.63*/("""" type="text/javascript"></script>
<script language="javascript">
  $(".chosen-select").chosen("""),format.raw/*58.30*/("""{"""),format.raw/*58.31*/("""
    add_search_option: true,
    no_results_text: "Not found. Press enter to add",
    allow_single_deselect: true
  """),format.raw/*62.3*/("""}"""),format.raw/*62.4*/(""");
  $(document).ready(function() """),format.raw/*63.32*/("""{"""),format.raw/*63.33*/("""
    $(".chosen-choices").addClass("form-control");
   """),format.raw/*65.4*/("""}"""),format.raw/*65.5*/(""");

  // Populate URL initially on startup
  """),_display_(Seq[Any](/*68.4*/user/*68.8*/.map/*68.12*/ { currUser =>_display_(Seq[Any](format.raw/*68.26*/("""
    var avUrl = '"""),_display_(Seq[Any](/*69.19*/{currUser.profile match {
      case Some(pro) => pro.avatarUrl.getOrElse("")
      case None => ""
  }})),format.raw/*72.5*/("""';

  $("custom_url_radio").val(avUrl);
  if (avUrl == '') """),format.raw/*75.20*/("""{"""),format.raw/*75.21*/("""
      $("#grav_url_radio").prop( "checked", true );
      $("#custom_url_radio").prop( "checked", false );
      selectGravatar();
  """),format.raw/*79.3*/("""}"""),format.raw/*79.4*/(""" else """),format.raw/*79.10*/("""{"""),format.raw/*79.11*/("""
      $("#grav_url_radio").prop( "checked", false );
      $("#custom_url_radio").prop( "checked", true );
      $("#custom_url_value").val(avUrl);
    """),format.raw/*83.5*/("""}"""),format.raw/*83.6*/("""
  """)))})),format.raw/*84.4*/("""

  function selectGravatar() """),format.raw/*86.29*/("""{"""),format.raw/*86.30*/("""
    $('custom_url_value').css("display","none")
  """),format.raw/*88.3*/("""}"""),format.raw/*88.4*/("""

  function updateCustomValue() """),format.raw/*90.32*/("""{"""),format.raw/*90.33*/("""
    $('custom_url_value').css("display","")
    if ($("#custom_url_radio").prop("checked")) """),format.raw/*92.49*/("""{"""),format.raw/*92.50*/("""
      // If Custom URL checked, store the value
        $('#custom_url_radio').val($('#custom_url_value').val())
    """),format.raw/*95.5*/("""}"""),format.raw/*95.6*/(""" else """),format.raw/*95.12*/("""{"""),format.raw/*95.13*/("""
      // Gravatar URL stores blank string
      $('#custom_url_radio').val('')
    """),format.raw/*98.5*/("""}"""),format.raw/*98.6*/("""

  """),format.raw/*100.3*/("""}"""),format.raw/*100.4*/("""
</script>
""")))})),format.raw/*102.2*/("""
"""))}
    }
    
    def render(myForm:Form[models.Profile],allInstitutionOptions:List[String],allProjectOptions:List[String],emailtimes:Map[String, String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm,allInstitutionOptions,allProjectOptions,emailtimes)(flash,user)
    
    def f:((Form[models.Profile],List[String],List[String],Map[String, String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm,allInstitutionOptions,allProjectOptions,emailtimes) => (flash,user) => apply(myForm,allInstitutionOptions,allProjectOptions,emailtimes)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/editProfile.scala.html
                    HASH: 2ad7765be569e3cbdf5e67479633434d8b2f61c3
                    MATRIX: 692->1|989->216|1021->240|1101->196|1130->289|1182->306|1196->312|1262->357|1361->421|1375->427|1432->463|1471->468|1499->488|1538->490|1692->609|1721->629|1761->631|1888->727|1931->735|1945->740|1971->757|2022->770|2113->825|2142->832|2200->859|2243->867|2360->974|2401->976|2494->1033|2684->1200|2732->1212|2832->1290|3392->1814|3807->2206|3855->2218|3956->2297|4005->2310|4218->2500|4267->2513|4474->2697|4523->2710|4626->2791|4777->2906|4856->2976|4896->2978|4982->3028|4995->3032|5057->3072|5105->3088|5348->3300|5419->3335|5434->3341|5498->3383|5621->3478|5650->3479|5795->3597|5823->3598|5885->3632|5914->3633|5996->3688|6024->3689|6105->3735|6117->3739|6130->3743|6182->3757|6237->3776|6362->3880|6449->3939|6478->3940|6639->4074|6667->4075|6701->4081|6730->4082|6910->4235|6938->4236|6973->4240|7031->4270|7060->4271|7138->4322|7166->4323|7227->4356|7256->4357|7377->4450|7406->4451|7551->4569|7579->4570|7613->4576|7642->4577|7753->4661|7781->4662|7813->4666|7842->4667|7886->4679
                    LINES: 20->1|23->4|23->4|24->1|26->4|27->5|27->5|27->5|28->6|28->6|28->6|30->8|30->8|30->8|36->14|36->14|36->14|40->18|41->19|41->19|41->19|41->19|43->21|43->21|45->23|46->24|46->24|46->24|48->26|48->26|49->27|49->27|58->36|58->36|59->37|59->37|61->39|61->39|63->41|63->41|65->43|65->43|69->47|69->47|69->47|70->48|70->48|70->48|71->49|75->53|78->56|78->56|78->56|80->58|80->58|84->62|84->62|85->63|85->63|87->65|87->65|90->68|90->68|90->68|90->68|91->69|94->72|97->75|97->75|101->79|101->79|101->79|101->79|105->83|105->83|106->84|108->86|108->86|110->88|110->88|112->90|112->90|114->92|114->92|117->95|117->95|117->95|117->95|120->98|120->98|122->100|122->100|124->102
                    -- GENERATED --
                */
            