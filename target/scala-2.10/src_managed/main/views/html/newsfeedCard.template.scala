
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object newsfeedCard extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[models.Event,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(event: models.Event)(implicit user: Option[models.User] = None):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services.AppConfiguration

import play.api.i18n.Messages

import play.Logger

def /*5.2*/linkForObject/*5.15*/(obj_type: String, obj_id: Option[UUID], obj_name: Option[String]):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*5.85*/("""
	"""),_display_(Seq[Any](/*6.3*/obj_id/*6.9*/ match/*6.15*/ {/*7.3*/case Some(id) =>/*7.19*/ {_display_(Seq[Any](format.raw/*7.21*/("""
  """),_display_(Seq[Any](/*8.4*/obj_type/*8.12*/ match/*8.18*/ {/*9.5*/case "file" =>/*9.19*/ {_display_(Seq[Any](format.raw/*9.21*/("""
      file: <a href=""""),_display_(Seq[Any](/*10.23*/routes/*10.29*/.Files.file(obj_id.get))),format.raw/*10.52*/("""">"""),_display_(Seq[Any](/*10.55*/obj_name/*10.63*/.get)),format.raw/*10.67*/("""</a>
    """)))}/*12.5*/case "dataset" =>/*12.22*/ {_display_(Seq[Any](format.raw/*12.24*/("""
      """),_display_(Seq[Any](/*13.8*/Messages("dataset.title")/*13.33*/.toLowerCase)),format.raw/*13.45*/(""" : <a href=""""),_display_(Seq[Any](/*13.58*/routes/*13.64*/.Datasets.dataset(obj_id.get))),format.raw/*13.93*/("""">"""),_display_(Seq[Any](/*13.96*/obj_name/*13.104*/.get)),format.raw/*13.108*/("""</a>
    """)))}/*15.5*/case "collection" =>/*15.25*/ {_display_(Seq[Any](format.raw/*15.27*/("""
      """),_display_(Seq[Any](/*16.8*/Messages("collection.title")/*16.36*/.toLowerCase)),format.raw/*16.48*/(""" : <a href=""""),_display_(Seq[Any](/*16.61*/routes/*16.67*/.Collections.collection(obj_id.get))),format.raw/*16.102*/("""">"""),_display_(Seq[Any](/*16.105*/obj_name/*16.113*/.get)),format.raw/*16.117*/("""</a>
    """)))}/*18.5*/case "user" =>/*18.19*/ {_display_(Seq[Any](format.raw/*18.21*/("""
      user: <a href=""""),_display_(Seq[Any](/*19.23*/routes/*19.29*/.Profile.viewProfileUUID(obj_id.get))),format.raw/*19.65*/("""">"""),_display_(Seq[Any](/*19.68*/obj_name/*19.76*/.get)),format.raw/*19.80*/("""</a>
    """)))}/*21.5*/case "space" =>/*21.20*/ {_display_(Seq[Any](format.raw/*21.22*/("""
      """),_display_(Seq[Any](/*22.8*/Messages("space.title"))),format.raw/*22.31*/(""": <a href=""""),_display_(Seq[Any](/*22.43*/routes/*22.49*/.Spaces.getSpace(obj_id.get))),format.raw/*22.77*/("""">"""),_display_(Seq[Any](/*22.80*/obj_name/*22.88*/.get)),format.raw/*22.92*/("""</a>
    """)))}/*24.5*/case "curation" =>/*24.23*/ {_display_(Seq[Any](format.raw/*24.25*/("""
      curation object: <a href=""""),_display_(Seq[Any](/*25.34*/routes/*25.40*/.CurationObjects.getCurationObject(obj_id.get))),format.raw/*25.86*/("""">"""),_display_(Seq[Any](/*25.89*/obj_name/*25.97*/.get)),format.raw/*25.101*/("""</a>
    """)))}/*27.5*/case "subcollection" =>/*27.28*/ {_display_(Seq[Any](format.raw/*27.30*/("""
      """),_display_(Seq[Any](/*28.8*/Messages("collection.title")/*28.36*/.toLowerCase)),format.raw/*28.48*/(""" : <a href=""""),_display_(Seq[Any](/*28.61*/routes/*28.67*/.Collections.collection(obj_id.get))),format.raw/*28.102*/("""">"""),_display_(Seq[Any](/*28.105*/obj_name/*28.113*/.get)),format.raw/*28.117*/("""</a> from collection:
      <a href=""""),_display_(Seq[Any](/*29.17*/routes/*29.23*/.Collections.collection(event.source_id.get))),format.raw/*29.67*/("""">"""),_display_(Seq[Any](/*29.70*/event/*29.75*/.source_name.get)),format.raw/*29.91*/("""</a>
    """)))}/*31.5*/case _ =>/*31.14*/ {_display_(Seq[Any](format.raw/*31.16*/("""
      """),_display_(Seq[Any](/*32.8*/obj_name/*32.16*/ match/*32.22*/ {/*33.9*/case None =>/*33.21*/ {_display_(Seq[Any](format.raw/*33.23*/("""
          """),_display_(Seq[Any](/*34.12*/obj_type)),format.raw/*34.20*/("""
        """)))}/*36.9*/case Some(name) =>/*36.27*/ {_display_(Seq[Any](format.raw/*36.29*/("""
          """),_display_(Seq[Any](/*37.12*/obj_type)),format.raw/*37.20*/(""": """),_display_(Seq[Any](/*37.23*/name)),format.raw/*37.27*/("""
        """)))}})),format.raw/*39.8*/("""
    """)))}})),format.raw/*41.4*/("""
  """)))}/*43.3*/case None =>/*43.15*/ {_display_(Seq[Any](format.raw/*43.17*/("""
  	<a>"""),_display_(Seq[Any](/*44.8*/obj_name/*44.16*/ match/*44.22*/ {/*45.9*/case None =>/*45.21*/ {_display_(Seq[Any](format.raw/*45.23*/("""
          """),_display_(Seq[Any](/*46.12*/obj_type)),format.raw/*46.20*/("""
        """)))}/*48.9*/case Some(name) =>/*48.27*/ {_display_(Seq[Any](format.raw/*48.29*/("""
          """),_display_(Seq[Any](/*49.12*/obj_type)),format.raw/*49.20*/(""": """),_display_(Seq[Any](/*49.23*/name)),format.raw/*49.27*/("""
        """)))}})),format.raw/*51.8*/("""</a>
      """)))}})),format.raw/*53.8*/("""
  """)))};def /*56.2*/actionTextForEvent/*56.20*/(event_type_prefix: String) = {{
  event_type_prefix match {
    case "follow" => "is now following "
    case "unfollow" => "stopped following "
    case "upload" => "uploaded "
    case "create" => "created "
    case "delete" => "deleted "
    case "edit" => "edited their comment to "
    case "download" => "downloaded "
    case "postrequest" => "requested access to"
    case _ => {
      Logger.warn("unknown event type prefix in actionTextForEvent: "+event_type_prefix)
      event_type_prefix
    }
  }
}};def /*73.2*/actionTextsForEvent/*73.21*/(event_type_prefix: String) = {{
  event_type_prefix match {
    case "add" => List("added ", " to ")
    case "attach" => List("attached ", " to ")
    case "remove" => List("removed ", " from ")
    case "detach" => List("detached ", " from ")
    case _ => {
      Logger.warn("unknown event type prefix in actionTextsForEvent: "+event_type_prefix)
      List(event_type_prefix+" ", " ")
    }
  }
}};
Seq[Any](format.raw/*1.66*/("""
"""),format.raw/*54.4*/("""

"""),format.raw/*71.2*/("""

"""),format.raw/*84.2*/("""

"""),_display_(Seq[Any](/*86.2*/event/*86.7*/.event_type/*86.18*/ match/*86.24*/ {/*87.3*/case "move_dataset_trash" =>/*87.31*/ {_display_(Seq[Any](format.raw/*87.33*/("""
    moved to trash """),_display_(Seq[Any](/*88.21*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*88.81*/("""
  """)))}/*90.3*/case "move_collection_trash" =>/*90.34*/ {_display_(Seq[Any](format.raw/*90.36*/("""
    moved to trash """),_display_(Seq[Any](/*91.21*/linkForObject("collection", event.object_id, event.object_name))),format.raw/*91.84*/("""
  """)))}/*93.3*/case "restore_dataset_trash" =>/*93.34*/ {_display_(Seq[Any](format.raw/*93.36*/("""
    restored from trash """),_display_(Seq[Any](/*94.26*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*94.86*/("""
  """)))}/*96.3*/case "restore_collection_trash" =>/*96.37*/ {_display_(Seq[Any](format.raw/*96.39*/("""
    retored from trash """),_display_(Seq[Any](/*97.25*/linkForObject("collection", event.object_id, event.object_name))),format.raw/*97.88*/("""
  """)))}/*99.3*/case "edit_profile" =>/*99.25*/ {_display_(Seq[Any](format.raw/*99.27*/("""
    edited their <a href=""""),_display_(Seq[Any](/*100.28*/routes/*100.34*/.Profile.viewProfileUUID(event.user.id))),format.raw/*100.73*/("""">profile</a>
  """)))}/*102.3*/case "update_dataset_information" =>/*102.39*/ {_display_(Seq[Any](format.raw/*102.41*/("""
    updated information for """),_display_(Seq[Any](/*103.30*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*103.90*/("""
  """)))}/*105.3*/case "update_space_information" =>/*105.37*/ {_display_(Seq[Any](format.raw/*105.39*/("""
    updated information for """),_display_(Seq[Any](/*106.30*/linkForObject("space", event.object_id, event.object_name))),format.raw/*106.88*/("""
  """)))}/*108.3*/case "update_file_information" =>/*108.36*/ {_display_(Seq[Any](format.raw/*108.38*/("""
    updated information for """),_display_(Seq[Any](/*109.30*/linkForObject("file", event.object_id, event.object_name))),format.raw/*109.87*/("""
  """)))}/*111.3*/case "update_collection_information" =>/*111.42*/ {_display_(Seq[Any](format.raw/*111.44*/("""
    updated information for """),_display_(Seq[Any](/*112.30*/linkForObject("collection", event.object_id, event.object_name))),format.raw/*112.93*/("""
  """)))}/*114.3*/case "update_curation_information" =>/*114.40*/ {_display_(Seq[Any](format.raw/*114.42*/("""
    updated information for """),_display_(Seq[Any](/*115.30*/linkForObject("curation", event.object_id, event.object_name))),format.raw/*115.91*/("""
  """)))}/*117.3*/case "set_note_file" =>/*117.26*/ {_display_(Seq[Any](format.raw/*117.28*/("""
    set note for """),_display_(Seq[Any](/*118.19*/linkForObject("file", event.object_id, event.object_name))),format.raw/*118.76*/("""
  """)))}/*120.3*/case "add_user_to_space" =>/*120.30*/ {_display_(Seq[Any](format.raw/*120.32*/("""
    added """),_display_(Seq[Any](/*121.12*/linkForObject("user", event.targetuser.map(_.id), event.targetuser.map(_.fullName)))),format.raw/*121.95*/(""" as a user to """),_display_(Seq[Any](/*121.110*/linkForObject("space", event.object_id, event.object_name))),format.raw/*121.168*/("""
  """)))}/*123.3*/case "remove_user_from_space" =>/*123.35*/ {_display_(Seq[Any](format.raw/*123.37*/("""
    removed """),_display_(Seq[Any](/*124.14*/linkForObject("user", event.targetuser.map(_.id), event.targetuser.map(_.fullName)))),format.raw/*124.97*/(""" as a user from """),_display_(Seq[Any](/*124.114*/linkForObject("space", event.object_id, event.object_name))),format.raw/*124.172*/("""
  """)))}/*126.3*/case "added_folder" =>/*126.25*/ {_display_(Seq[Any](format.raw/*126.27*/("""
    created a folder in """),_display_(Seq[Any](/*127.26*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*127.86*/("""
  """)))}/*129.3*/case "deleted_folder" =>/*129.27*/ {_display_(Seq[Any](format.raw/*129.29*/("""
    deleted a folder from """),_display_(Seq[Any](/*130.28*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*130.88*/("""
  """)))}/*132.3*/case "updated_folder" =>/*132.27*/ {_display_(Seq[Any](format.raw/*132.29*/("""
    updated a folder in """),_display_(Seq[Any](/*133.26*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*133.86*/("""
  """)))}/*135.3*/case "add_file" | "add_file_1" | "add_file_2" | "add_file_3" =>/*135.66*/ {_display_(Seq[Any](format.raw/*135.68*/("""
    added a file to """),_display_(Seq[Any](/*136.22*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*136.82*/("""
  """)))}/*138.3*/case "add_file_folder" | "add_file_folder_1" | "add_file_folder_2" | "add_file_folder_3" =>/*138.94*/ {_display_(Seq[Any](format.raw/*138.96*/("""
    added a file to a folder within """),_display_(Seq[Any](/*139.38*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*139.98*/("""
  """)))}/*141.3*/case "add_sub_collection" =>/*141.31*/ {_display_(Seq[Any](format.raw/*141.33*/("""
    added """),_display_(Seq[Any](/*142.12*/linkForObject("collection", event.object_id, event.object_name))),format.raw/*142.75*/(""" to
    """),_display_(Seq[Any](/*143.6*/linkForObject("collection", event.source_id, event.source_name))),format.raw/*143.69*/("""
  """)))}/*145.3*/case "addMetadata_file" | "addMetadata_dataset" =>/*145.53*/ {_display_(Seq[Any](format.raw/*145.55*/("""
    added metadata to """),_display_(Seq[Any](/*146.24*/linkForObject(
      event.event_type.substring(event.event_type.indexOf('_')+1),
      event.object_id,
      event.object_name
    ))),format.raw/*150.6*/("""
  """)))}/*152.3*/case "acceptrequest_space" =>/*152.32*/ {_display_(Seq[Any](format.raw/*152.34*/("""
    accepted """),_display_(Seq[Any](/*153.15*/linkForObject("user", event.targetuser.map(_.id), event.targetuser.map(_.fullName)))),format.raw/*153.98*/("""'s request to
    """),_display_(Seq[Any](/*154.6*/linkForObject("space", event.object_id, event.object_name))),format.raw/*154.64*/("""
  """)))}/*156.3*/case "rejectrequest_space" =>/*156.32*/ {_display_(Seq[Any](format.raw/*156.34*/("""
    rejected  """),_display_(Seq[Any](/*157.16*/linkForObject("user", event.targetuser.map(_.id), event.targetuser.map(_.fullName)))),format.raw/*157.99*/("""'s  request to
    """),_display_(Seq[Any](/*158.6*/linkForObject("space", event.object_id, event.object_name))),format.raw/*158.64*/("""
  """)))}/*160.3*/case "tos_update" =>/*160.23*/ {_display_(Seq[Any](format.raw/*160.25*/("""
    updated the <a href=""""),_display_(Seq[Any](/*161.27*/routes/*161.33*/.Application.tos())),format.raw/*161.51*/("""">Terms of Service</a>
  """)))}/*163.3*/case "added_metadata_space" =>/*163.33*/ {_display_(Seq[Any](format.raw/*163.35*/("""
    added a new metadata definition to """),_display_(Seq[Any](/*164.41*/linkForObject("space", event.object_id, event.object_name))),format.raw/*164.99*/("""
  """)))}/*166.3*/case "added_metadata_instance" =>/*166.36*/ {_display_(Seq[Any](format.raw/*166.38*/("""
    added a new metadata definition to """),_display_(Seq[Any](/*167.41*/(AppConfiguration.getDisplayName))),format.raw/*167.74*/("""
  """)))}/*169.3*/case "edit_metadata_space" =>/*169.32*/ {_display_(Seq[Any](format.raw/*169.34*/("""
    edited a metadata definition for """),_display_(Seq[Any](/*170.39*/linkForObject("space", event.object_id, event.object_name))),format.raw/*170.97*/("""
  """)))}/*172.3*/case "edit_metadata_instance" =>/*172.35*/ {_display_(Seq[Any](format.raw/*172.37*/("""
    edited a metadata definition for """),_display_(Seq[Any](/*173.39*/(AppConfiguration.getDisplayName))),format.raw/*173.72*/("""
  """)))}/*175.3*/case "delete_metadata_space" =>/*175.34*/ {_display_(Seq[Any](format.raw/*175.36*/("""
    delete a metadata definition from """),_display_(Seq[Any](/*176.40*/linkForObject("space", event.object_id, event.object_name))),format.raw/*176.98*/("""
  """)))}/*178.3*/case "delete_metadata_instance" =>/*178.37*/ {_display_(Seq[Any](format.raw/*178.39*/("""
    deleted a metadata definition from """),_display_(Seq[Any](/*179.41*/(AppConfiguration.getDisplayName))),format.raw/*179.74*/("""
  """)))}/*181.3*/case "mention_file_comment" =>/*181.33*/ {_display_(Seq[Any](format.raw/*181.35*/("""
    """),_display_(Seq[Any](/*182.6*/event/*182.11*/.targetuser/*182.22*/ match/*182.28*/ {/*183.7*/case Some(u) =>/*183.22*/ {_display_(Seq[Any](format.raw/*183.24*/("""
        was mentioned by """),_display_(Seq[Any](/*184.27*/linkForObject("user", Some(u.id), Some(u.fullName)))),format.raw/*184.78*/(""" in a comment on """),_display_(Seq[Any](/*184.96*/linkForObject("file", event.object_id, event.object_name))),format.raw/*184.153*/("""
      """)))}/*186.7*/case None =>/*186.19*/ {_display_(Seq[Any](format.raw/*186.21*/("""
        was mentioned in a comment on """),_display_(Seq[Any](/*187.40*/linkForObject("file", event.object_id, event.object_name))),format.raw/*187.97*/("""
      """)))}})),format.raw/*189.6*/("""
  """)))}/*191.3*/case "mention_dataset_comment" =>/*191.36*/ {_display_(Seq[Any](format.raw/*191.38*/("""
    """),_display_(Seq[Any](/*192.6*/event/*192.11*/.targetuser/*192.22*/ match/*192.28*/ {/*193.7*/case Some(u) =>/*193.22*/ {_display_(Seq[Any](format.raw/*193.24*/("""
        was mentioned by """),_display_(Seq[Any](/*194.27*/linkForObject("user", Some(u.id), Some(u.fullName)))),format.raw/*194.78*/(""" in a comment on """),_display_(Seq[Any](/*194.96*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*194.156*/("""
      """)))}/*196.7*/case None =>/*196.19*/ {_display_(Seq[Any](format.raw/*196.21*/("""
        was mentioned in a comment on """),_display_(Seq[Any](/*197.40*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*197.100*/("""
      """)))}})),format.raw/*199.6*/("""
  """)))}/*201.3*/case _ =>/*201.12*/ {_display_(Seq[Any](format.raw/*201.14*/("""
    """),_display_(Seq[Any](/*202.6*/defining(event.event_type.split("_"))/*202.43*/ { event_type_split =>_display_(Seq[Any](format.raw/*202.65*/("""
      """),_display_(Seq[Any](/*203.8*/event_type_split/*203.24*/.length/*203.31*/ match/*203.37*/ {/*204.9*/case 2 =>/*204.18*/ {_display_(Seq[Any](format.raw/*204.20*/("""
          """),_display_(Seq[Any](/*205.12*/if(event_type_split(0) == "comment")/*205.48*/{_display_(Seq[Any](format.raw/*205.49*/("""
            commented <font color="black"><em>"""),_display_(Seq[Any](/*206.48*/event/*206.53*/.object_name)),format.raw/*206.65*/("""</em></font> on """),_display_(Seq[Any](/*206.82*/linkForObject("file", event.source_id, event.source_name))),format.raw/*206.139*/("""
          """)))}/*207.12*/else/*207.16*/{_display_(Seq[Any](format.raw/*207.17*/("""
            """),_display_(Seq[Any](/*208.14*/actionTextForEvent(event_type_split(0)))),format.raw/*208.53*/(""" """),_display_(Seq[Any](/*208.55*/linkForObject(event_type_split(1), event.object_id, event.object_name))),format.raw/*208.125*/("""
          """)))})),format.raw/*209.12*/("""
        """)))}/*211.9*/case 3 =>/*211.18*/ {_display_(Seq[Any](format.raw/*211.20*/("""
          """),_display_(Seq[Any](/*212.12*/defining(actionTextsForEvent(event_type_split(0)))/*212.62*/ { texts =>_display_(Seq[Any](format.raw/*212.73*/("""
            """),_display_(Seq[Any](/*213.14*/if(event.event_type.indexOf("tag")>=0)/*213.52*/{_display_(Seq[Any](format.raw/*213.53*/("""
              """),_display_(Seq[Any](/*214.16*/texts(0))),format.raw/*214.24*/(""" """),_display_(Seq[Any](/*214.26*/linkForObject(event_type_split(1), None, None))),format.raw/*214.72*/(""" """),_display_(Seq[Any](/*214.74*/texts(1))),format.raw/*214.82*/(""" """),_display_(Seq[Any](/*214.84*/linkForObject(event_type_split(2), event.object_id, event.object_name))),format.raw/*214.154*/("""
            """)))}/*215.15*/else/*215.20*/{_display_(Seq[Any](format.raw/*215.21*/("""
              """),format.raw/*216.50*/("""
              """),_display_(Seq[Any](/*217.16*/if(event.event_type.indexOf("file")>=0 && event.event_type.indexOf("dataset") < 0 && event.event_type.indexOf("folder") < 0)/*217.140*/ {_display_(Seq[Any](format.raw/*217.142*/("""
                added """),_display_(Seq[Any](/*218.24*/event_type_split(2))),format.raw/*218.43*/(""" files to """),_display_(Seq[Any](/*218.54*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*218.114*/("""
              """)))}/*219.17*/else/*219.22*/{_display_(Seq[Any](format.raw/*219.23*/("""
                """),_display_(Seq[Any](/*220.18*/texts(0))),format.raw/*220.26*/(""" """),_display_(Seq[Any](/*220.28*/linkForObject(event_type_split(1), event.object_id, event.object_name))),format.raw/*220.98*/(""" """),_display_(Seq[Any](/*220.100*/texts(1))),format.raw/*220.108*/(""" """),_display_(Seq[Any](/*220.110*/linkForObject(event_type_split(2), event.source_id, event.source_name))),format.raw/*220.180*/("""
              """)))})),format.raw/*221.16*/("""
            """)))})),format.raw/*222.14*/("""
          """)))})),format.raw/*223.12*/("""
        """)))}/*225.9*/case 4 =>/*225.18*/{_display_(Seq[Any](format.raw/*225.19*/("""
          """),format.raw/*226.58*/("""
          """),_display_(Seq[Any](/*227.12*/if(event.event_type.indexOf("file")>=0 && event.event_type.indexOf("dataset") < 0)/*227.94*/ {_display_(Seq[Any](format.raw/*227.96*/("""
            added """),_display_(Seq[Any](/*228.20*/event_type_split(3))),format.raw/*228.39*/(""" files to a folder within """),_display_(Seq[Any](/*228.66*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*228.126*/("""
          """)))}/*229.13*/else/*229.18*/{_display_(Seq[Any](format.raw/*229.19*/("""
            """),_display_(Seq[Any](/*230.14*/event_type_split(0))),format.raw/*230.33*/(""" """),_display_(Seq[Any](/*230.35*/linkForObject(event_type_split(1), event.object_id, event.object_name))),format.raw/*230.105*/(""" """),_display_(Seq[Any](/*230.107*/event_type_split(1))),format.raw/*230.126*/(""" """),_display_(Seq[Any](/*230.128*/linkForObject(event_type_split(2), event.source_id, event.source_name))),format.raw/*230.198*/("""
          """)))})),format.raw/*231.12*/("""
        """)))}/*233.9*/case _ =>/*233.18*/ {_display_(Seq[Any](format.raw/*233.20*/("""
          """),_display_(Seq[Any](/*234.12*/Logger/*234.18*/.warn("unknown event type encountered: "+event.event_type))),format.raw/*234.76*/("""
          """),_display_(Seq[Any](/*235.12*/event/*235.17*/.event_type)),format.raw/*235.28*/("""
        """)))}})),format.raw/*237.8*/("""
    """)))})),format.raw/*238.6*/("""
  """)))}})),format.raw/*240.2*/("""
"""))}
    }
    
    def render(event:models.Event,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(event)(user)
    
    def f:((models.Event) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (event) => (user) => apply(event)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/newsfeedCard.scala.html
                    HASH: 693bb2d54c41955710c2e5ade2f072e816a816a7
                    MATRIX: 620->1|845->152|866->165|1016->235|1053->238|1066->244|1080->250|1089->255|1113->271|1152->273|1190->277|1206->285|1220->291|1229->298|1251->312|1290->314|1349->337|1364->343|1409->366|1448->369|1465->377|1491->381|1519->396|1545->413|1585->415|1628->423|1662->448|1696->460|1745->473|1760->479|1811->508|1850->511|1868->519|1895->523|1923->538|1952->558|1992->560|2035->568|2072->596|2106->608|2155->621|2170->627|2228->662|2268->665|2286->673|2313->677|2341->692|2364->706|2404->708|2463->731|2478->737|2536->773|2575->776|2592->784|2618->788|2646->803|2670->818|2710->820|2753->828|2798->851|2846->863|2861->869|2911->897|2950->900|2967->908|2993->912|3021->927|3048->945|3088->947|3158->981|3173->987|3241->1033|3280->1036|3297->1044|3324->1048|3352->1063|3384->1086|3424->1088|3467->1096|3504->1124|3538->1136|3587->1149|3602->1155|3660->1190|3700->1193|3718->1201|3745->1205|3819->1243|3834->1249|3900->1293|3939->1296|3953->1301|3991->1317|4019->1332|4037->1341|4077->1343|4120->1351|4137->1359|4152->1365|4162->1376|4183->1388|4223->1390|4271->1402|4301->1410|4329->1429|4356->1447|4396->1449|4444->1461|4474->1469|4513->1472|4539->1476|4581->1494|4619->1504|4641->1511|4662->1523|4702->1525|4745->1533|4762->1541|4777->1547|4787->1558|4808->1570|4848->1572|4896->1584|4926->1592|4954->1611|4981->1629|5021->1631|5069->1643|5099->1651|5138->1654|5164->1658|5206->1676|5250->1696|5277->1703|5304->1721|5832->2238|5860->2257|6292->65|6320->1700|6349->2235|6378->2659|6416->2662|6429->2667|6449->2678|6464->2684|6474->2689|6511->2717|6551->2719|6608->2740|6690->2800|6712->2807|6752->2838|6792->2840|6849->2861|6934->2924|6956->2931|6996->2962|7036->2964|7098->2990|7180->3050|7202->3057|7245->3091|7285->3093|7346->3118|7431->3181|7453->3188|7484->3210|7524->3212|7589->3240|7605->3246|7667->3285|7703->3305|7749->3341|7790->3343|7857->3373|7940->3433|7963->3440|8007->3474|8048->3476|8115->3506|8196->3564|8219->3571|8262->3604|8303->3606|8370->3636|8450->3693|8473->3700|8522->3739|8563->3741|8630->3771|8716->3834|8739->3841|8786->3878|8827->3880|8894->3910|8978->3971|9001->3978|9034->4001|9075->4003|9131->4022|9211->4079|9234->4086|9271->4113|9312->4115|9361->4127|9467->4210|9520->4225|9602->4283|9625->4290|9667->4322|9708->4324|9759->4338|9865->4421|9920->4438|10002->4496|10025->4503|10057->4525|10098->4527|10161->4553|10244->4613|10267->4620|10301->4644|10342->4646|10407->4674|10490->4734|10513->4741|10547->4765|10588->4767|10651->4793|10734->4853|10757->4860|10830->4923|10871->4925|10930->4947|11013->5007|11036->5014|11137->5105|11178->5107|11253->5145|11336->5205|11359->5212|11397->5240|11438->5242|11487->5254|11573->5317|11618->5326|11704->5389|11727->5396|11787->5446|11828->5448|11889->5472|12045->5606|12068->5613|12107->5642|12148->5644|12200->5659|12306->5742|12361->5761|12442->5819|12465->5826|12504->5855|12545->5857|12598->5873|12704->5956|12760->5976|12841->6034|12864->6041|12894->6061|12935->6063|12999->6090|13015->6096|13056->6114|13101->6143|13141->6173|13182->6175|13260->6216|13341->6274|13364->6281|13407->6314|13448->6316|13526->6357|13582->6390|13605->6397|13644->6426|13685->6428|13761->6467|13842->6525|13865->6532|13907->6564|13948->6566|14024->6605|14080->6638|14103->6645|14144->6676|14185->6678|14262->6718|14343->6776|14366->6783|14410->6817|14451->6819|14529->6860|14585->6893|14608->6900|14648->6930|14689->6932|14731->6938|14746->6943|14767->6954|14783->6960|14794->6969|14819->6984|14860->6986|14924->7013|14998->7064|15053->7082|15134->7139|15161->7154|15183->7166|15224->7168|15301->7208|15381->7265|15422->7279|15445->7286|15488->7319|15529->7321|15571->7327|15586->7332|15607->7343|15623->7349|15634->7358|15659->7373|15700->7375|15764->7402|15838->7453|15893->7471|15977->7531|16004->7546|16026->7558|16067->7560|16144->7600|16228->7660|16269->7674|16292->7681|16311->7690|16352->7692|16394->7698|16441->7735|16502->7757|16546->7765|16572->7781|16589->7788|16605->7794|16616->7805|16635->7814|16676->7816|16725->7828|16771->7864|16811->7865|16896->7913|16911->7918|16946->7930|17000->7947|17081->8004|17113->8016|17127->8020|17167->8021|17218->8035|17280->8074|17319->8076|17413->8146|17458->8158|17487->8177|17506->8186|17547->8188|17596->8200|17656->8250|17706->8261|17757->8275|17805->8313|17845->8314|17898->8330|17929->8338|17968->8340|18037->8386|18076->8388|18107->8396|18146->8398|18240->8468|18274->8483|18288->8488|18328->8489|18372->8539|18425->8555|18560->8679|18602->8681|18663->8705|18705->8724|18753->8735|18837->8795|18873->8812|18887->8817|18927->8818|18982->8836|19013->8844|19052->8846|19145->8916|19185->8918|19217->8926|19257->8928|19351->8998|19400->9014|19447->9028|19492->9040|19521->9059|19540->9068|19580->9069|19620->9127|19669->9139|19761->9221|19802->9223|19859->9243|19901->9262|19965->9289|20049->9349|20081->9362|20095->9367|20135->9368|20186->9382|20228->9401|20267->9403|20361->9473|20401->9475|20444->9494|20484->9496|20578->9566|20623->9578|20652->9597|20671->9606|20712->9608|20761->9620|20777->9626|20858->9684|20907->9696|20922->9701|20956->9712|20999->9730|21037->9736|21074->9742
                    LINES: 20->1|27->5|27->5|29->5|30->6|30->6|30->6|30->7|30->7|30->7|31->8|31->8|31->8|31->9|31->9|31->9|32->10|32->10|32->10|32->10|32->10|32->10|33->12|33->12|33->12|34->13|34->13|34->13|34->13|34->13|34->13|34->13|34->13|34->13|35->15|35->15|35->15|36->16|36->16|36->16|36->16|36->16|36->16|36->16|36->16|36->16|37->18|37->18|37->18|38->19|38->19|38->19|38->19|38->19|38->19|39->21|39->21|39->21|40->22|40->22|40->22|40->22|40->22|40->22|40->22|40->22|41->24|41->24|41->24|42->25|42->25|42->25|42->25|42->25|42->25|43->27|43->27|43->27|44->28|44->28|44->28|44->28|44->28|44->28|44->28|44->28|44->28|45->29|45->29|45->29|45->29|45->29|45->29|46->31|46->31|46->31|47->32|47->32|47->32|47->33|47->33|47->33|48->34|48->34|49->36|49->36|49->36|50->37|50->37|50->37|50->37|51->39|52->41|53->43|53->43|53->43|54->44|54->44|54->44|54->45|54->45|54->45|55->46|55->46|56->48|56->48|56->48|57->49|57->49|57->49|57->49|58->51|59->53|60->56|60->56|75->73|75->73|87->1|88->54|90->71|92->84|94->86|94->86|94->86|94->86|94->87|94->87|94->87|95->88|95->88|96->90|96->90|96->90|97->91|97->91|98->93|98->93|98->93|99->94|99->94|100->96|100->96|100->96|101->97|101->97|102->99|102->99|102->99|103->100|103->100|103->100|104->102|104->102|104->102|105->103|105->103|106->105|106->105|106->105|107->106|107->106|108->108|108->108|108->108|109->109|109->109|110->111|110->111|110->111|111->112|111->112|112->114|112->114|112->114|113->115|113->115|114->117|114->117|114->117|115->118|115->118|116->120|116->120|116->120|117->121|117->121|117->121|117->121|118->123|118->123|118->123|119->124|119->124|119->124|119->124|120->126|120->126|120->126|121->127|121->127|122->129|122->129|122->129|123->130|123->130|124->132|124->132|124->132|125->133|125->133|126->135|126->135|126->135|127->136|127->136|128->138|128->138|128->138|129->139|129->139|130->141|130->141|130->141|131->142|131->142|132->143|132->143|133->145|133->145|133->145|134->146|138->150|139->152|139->152|139->152|140->153|140->153|141->154|141->154|142->156|142->156|142->156|143->157|143->157|144->158|144->158|145->160|145->160|145->160|146->161|146->161|146->161|147->163|147->163|147->163|148->164|148->164|149->166|149->166|149->166|150->167|150->167|151->169|151->169|151->169|152->170|152->170|153->172|153->172|153->172|154->173|154->173|155->175|155->175|155->175|156->176|156->176|157->178|157->178|157->178|158->179|158->179|159->181|159->181|159->181|160->182|160->182|160->182|160->182|160->183|160->183|160->183|161->184|161->184|161->184|161->184|162->186|162->186|162->186|163->187|163->187|164->189|165->191|165->191|165->191|166->192|166->192|166->192|166->192|166->193|166->193|166->193|167->194|167->194|167->194|167->194|168->196|168->196|168->196|169->197|169->197|170->199|171->201|171->201|171->201|172->202|172->202|172->202|173->203|173->203|173->203|173->203|173->204|173->204|173->204|174->205|174->205|174->205|175->206|175->206|175->206|175->206|175->206|176->207|176->207|176->207|177->208|177->208|177->208|177->208|178->209|179->211|179->211|179->211|180->212|180->212|180->212|181->213|181->213|181->213|182->214|182->214|182->214|182->214|182->214|182->214|182->214|182->214|183->215|183->215|183->215|184->216|185->217|185->217|185->217|186->218|186->218|186->218|186->218|187->219|187->219|187->219|188->220|188->220|188->220|188->220|188->220|188->220|188->220|188->220|189->221|190->222|191->223|192->225|192->225|192->225|193->226|194->227|194->227|194->227|195->228|195->228|195->228|195->228|196->229|196->229|196->229|197->230|197->230|197->230|197->230|197->230|197->230|197->230|197->230|198->231|199->233|199->233|199->233|200->234|200->234|200->234|201->235|201->235|201->235|202->237|203->238|204->240
                    -- GENERATED --
                */
            