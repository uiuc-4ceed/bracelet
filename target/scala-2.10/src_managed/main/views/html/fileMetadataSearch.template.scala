
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object fileMetadataSearch extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.40*/("""

"""),_display_(Seq[Any](/*3.2*/main("File user metadata search")/*3.35*/ {_display_(Seq[Any](format.raw/*3.37*/("""
<div class="page-header">
  <h1>File user metadata search</h1>
</div>
<div class="row-fluid">
	<div class="span12">
		<form class="form-inline">
			<h5>Select properties to search for (in strings, input * alone to search for any string value):</h5>
			<div id='queryUserMetadata' class='usr_md_'>
				&nbsp;&nbsp;&nbsp;<button class="usr_md_" type="button">Add property</button>
				<ul class="usr_md_ usr_md_search_list"></ul>
				<button class="usr_md_" type="button">Add disjunction</button>
				<br />
				<button class="usr_md_submit btn" type="button">Submit</button></div>
		</form>	
		<table id='resultTable' class="table table-bordered table-hover" style="display:none;">
			<thead>
				<tr>					
						<th>Name</th>
						<th>Type</th>
						<th>Uploaded</th>
						<th></th>
						"""),_display_(Seq[Any](/*25.8*/if(user.isDefined)/*25.26*/ {_display_(Seq[Any](format.raw/*25.28*/("""
							<th></th>
						""")))})),format.raw/*27.8*/("""
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<ul class="pager">
			<li class="previous" style="visibility:hidden;"><a class="btn btn-link" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
			<li class ="next" style="visibility:hidden;"><a class="btn btn-link" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
		</ul>
	</div>
</div>
<script language="javascript">
	window["userDefined"] = false;
	window["userId"] = "";
</script>
"""),_display_(Seq[Any](/*43.2*/if(user.isDefined)/*43.20*/ {_display_(Seq[Any](format.raw/*43.22*/("""
	<script language="javascript">
		window["userDefined"] = true;
		window["userId"] = """"),_display_(Seq[Any](/*46.24*/user/*46.28*/.get.identityId.userId)),format.raw/*46.50*/("""";
	</script>
""")))})),format.raw/*48.2*/("""
<script language="javascript">	
	var queryIp = """"),_display_(Seq[Any](/*50.18*/api/*50.21*/.routes.Files.searchFilesUserMetadata)),format.raw/*50.58*/("""";
	var modelIp = """"),_display_(Seq[Any](/*51.18*/(routes.Assets.at("filesUserMetadataModel")))),format.raw/*51.62*/("""";
	var searchOn = "files";
	var searchFor = "userMetadata";
</script>
<script src=""""),_display_(Seq[Any](/*55.15*/routes/*55.21*/.Assets.at("javascripts/searchUserMetadata.js"))),format.raw/*55.68*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*56.15*/routes/*56.21*/.Assets.at("javascripts/fileListProcess.js"))),format.raw/*56.65*/("""" type="text/javascript"></script>
""")))})),format.raw/*57.2*/("""
"""))}
    }
    
    def render(user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply()(user)
    
    def f:(() => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = () => (user) => apply()(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/fileMetadataSearch.scala.html
                    HASH: dd787ea21573df9e2ba4153d4caad1f1ef41fdb1
                    MATRIX: 613->1|745->39|782->42|823->75|862->77|1691->871|1718->889|1758->891|1814->916|2356->1423|2383->1441|2423->1443|2547->1531|2560->1535|2604->1557|2650->1572|2736->1622|2748->1625|2807->1662|2863->1682|2929->1726|3050->1811|3065->1817|3134->1864|3219->1913|3234->1919|3300->1963|3367->1999
                    LINES: 20->1|23->1|25->3|25->3|25->3|47->25|47->25|47->25|49->27|65->43|65->43|65->43|68->46|68->46|68->46|70->48|72->50|72->50|72->50|73->51|73->51|77->55|77->55|77->55|78->56|78->56|78->56|79->57
                    -- GENERATED --
                */
            