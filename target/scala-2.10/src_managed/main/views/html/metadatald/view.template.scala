
package views.html.metadatald

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object view extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[List[models.Metadata],Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(metadata: List[models.Metadata], toDelete: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import java.net.URL

import play.api.libs.json.JsString

import play.api.libs.json.JsObject

import play.api.libs.json.JsValue

import play.api.libs.json.JsArray

import api.Permission

def /*14.2*/printContent/*14.14*/(agent: Agent, content: JsValue, contextId: Option[UUID], contextURL: Option[URL], resourceId: UUID):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*14.118*/("""
"""),_display_(Seq[Any](/*15.2*/content/*15.9*/ match/*15.15*/ {/*16.5*/case o: JsObject =>/*16.24*/ {_display_(Seq[Any](format.raw/*16.26*/("""
        <ul>
        """),_display_(Seq[Any](/*18.10*/for((key, value) <- o.fields) yield /*18.39*/ {_display_(Seq[Any](format.raw/*18.41*/("""
            """),_display_(Seq[Any](/*19.14*/value/*19.19*/ match/*19.25*/ {/*20.17*/case o: JsObject =>/*20.36*/ {_display_(Seq[Any](format.raw/*20.38*/("""
                    <li class="md-block">
                        <a class="collapse-icon">
                            <span class="glyphicon glyphicon-minus"></span>
                        </a>
                            <!-- only with context and user (not extractor) generated -->
                        """),_display_(Seq[Any](/*26.26*/if(contextId.isDefined && agent.typeOfAgent.toLowerCase().indexOf("user") >= 0)/*26.105*/ {_display_(Seq[Any](format.raw/*26.107*/("""
                            <!-- ids don't like spaces, you can use regex for removing different characters in metadata names (key) as well -->
                        """),_display_(Seq[Any](/*28.26*/defining(contextId.get + key.replaceAll("[() ]","") + resourceId.toString())/*28.102*/ { mid =>_display_(Seq[Any](format.raw/*28.111*/("""
                            """),_display_(Seq[Any](/*29.30*/if(user.isDefined)/*29.48*/ {_display_(Seq[Any](format.raw/*29.50*/("""
                                <a id='"""),_display_(Seq[Any](/*30.41*/mid)),format.raw/*30.44*/("""' href="javascript:void(0)"
                                onmouseover="getMetadataContext('"""),_display_(Seq[Any](/*31.67*/mid)),format.raw/*31.70*/("""','"""),_display_(Seq[Any](/*31.74*/contextId/*31.83*/.get)),format.raw/*31.87*/("""','"""),_display_(Seq[Any](/*31.91*/key)),format.raw/*31.94*/("""');"
                                onmouseout="leaveMetadataContext('"""),_display_(Seq[Any](/*32.68*/mid)),format.raw/*32.71*/("""');">
                                    <strong>"""),_display_(Seq[Any](/*33.46*/key)),format.raw/*33.49*/(""":</strong></a>
                            """)))}/*34.31*/else/*34.36*/{_display_(Seq[Any](format.raw/*34.37*/("""
                                <strong>"""),_display_(Seq[Any](/*35.42*/key)),format.raw/*35.45*/(""":</strong>
                            """)))})),format.raw/*36.30*/("""
                            """),_display_(Seq[Any](/*37.30*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*37.87*/("""
                        """)))})),format.raw/*38.26*/("""
                        """)))}/*39.27*/else/*39.32*/{_display_(Seq[Any](format.raw/*39.33*/("""
                    """),_display_(Seq[Any](/*40.22*/contextURL/*40.32*/ match/*40.38*/ {/*41.25*/case Some(u) =>/*41.40*/ {_display_(Seq[Any](format.raw/*41.42*/("""
                            <a href=""""),_display_(Seq[Any](/*42.39*/u/*42.40*/.toString)),format.raw/*42.49*/("""" target="_blank" title="Show context reference"><strong>"""),_display_(Seq[Any](/*42.107*/key)),format.raw/*42.110*/(""":</strong></a>
                            """),_display_(Seq[Any](/*43.30*/printContent(agent, value,contextId,contextURL,resourceId))),format.raw/*43.88*/("""
                        """)))}/*45.25*/case None =>/*45.37*/ {_display_(Seq[Any](format.raw/*45.39*/("""
                            <strong>"""),_display_(Seq[Any](/*46.38*/key)),format.raw/*46.41*/(""":</strong> """),_display_(Seq[Any](/*46.53*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*46.110*/("""
                        """)))}})),format.raw/*48.22*/("""
                    """)))})),format.raw/*49.22*/("""
                    </li>
                """)))}/*52.17*/case o: JsArray =>/*52.35*/ {_display_(Seq[Any](format.raw/*52.37*/("""
                    <li class="md-block">
                        <a class="collapse-icon">
                            <span class="glyphicon glyphicon-minus"></span>
                        </a>
                        """),_display_(Seq[Any](/*57.26*/if(contextId.isDefined && agent.typeOfAgent.toLowerCase().indexOf("user") >= 0)/*57.105*/ {_display_(Seq[Any](format.raw/*57.107*/("""
                            """),_display_(Seq[Any](/*58.30*/defining(contextId.get + key.replaceAll("[() ]","") + resourceId.toString())/*58.106*/ { mid =>_display_(Seq[Any](format.raw/*58.115*/("""
                                """),_display_(Seq[Any](/*59.34*/if(user.isDefined)/*59.52*/ {_display_(Seq[Any](format.raw/*59.54*/("""
                                    <a id='"""),_display_(Seq[Any](/*60.45*/mid)),format.raw/*60.48*/("""' href="javascript:void(0)"
                                    onmouseover="getMetadataContext('"""),_display_(Seq[Any](/*61.71*/mid)),format.raw/*61.74*/("""','"""),_display_(Seq[Any](/*61.78*/contextId/*61.87*/.get)),format.raw/*61.91*/("""','"""),_display_(Seq[Any](/*61.95*/key)),format.raw/*61.98*/("""');"
                                    onmouseout="leaveMetadataContext('"""),_display_(Seq[Any](/*62.72*/mid)),format.raw/*62.75*/("""');">
                                        <strong>"""),_display_(Seq[Any](/*63.50*/key)),format.raw/*63.53*/(""":</strong></a>
                                """)))}/*64.35*/else/*64.40*/{_display_(Seq[Any](format.raw/*64.41*/("""
                                    <strong>"""),_display_(Seq[Any](/*65.46*/key)),format.raw/*65.49*/(""":</strong>
                                """)))})),format.raw/*66.34*/("""
                                """),_display_(Seq[Any](/*67.34*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*67.91*/("""
                            """)))})),format.raw/*68.30*/("""
                        """)))}/*69.27*/else/*69.32*/{_display_(Seq[Any](format.raw/*69.33*/("""
                            """),_display_(Seq[Any](/*70.30*/contextURL/*70.40*/ match/*70.46*/ {/*71.33*/case Some(u) =>/*71.48*/ {_display_(Seq[Any](format.raw/*71.50*/("""
                                    <a href=""""),_display_(Seq[Any](/*72.47*/u/*72.48*/.toString)),format.raw/*72.57*/("""" target="_blank" title="Show context reference"><strong>"""),_display_(Seq[Any](/*72.115*/key)),format.raw/*72.118*/(""":</strong></a>
                                    """),_display_(Seq[Any](/*73.38*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*73.95*/("""
                                """)))}/*75.33*/case None =>/*75.45*/ {_display_(Seq[Any](format.raw/*75.47*/("""
                                    <strong>"""),_display_(Seq[Any](/*76.46*/key)),format.raw/*76.49*/(""":</strong> """),_display_(Seq[Any](/*76.61*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*76.118*/("""
                                """)))}})),format.raw/*78.30*/("""
                        """)))})),format.raw/*79.26*/("""
                    </li>
                """)))}/*82.17*/case _ =>/*82.26*/ {_display_(Seq[Any](format.raw/*82.28*/("""
                        <!--                                 <li class="md-block">
 -->                                """),_display_(Seq[Any](/*84.38*/if(contextId.isDefined && agent.typeOfAgent.toLowerCase().indexOf("user") >= 0)/*84.117*/ {_display_(Seq[Any](format.raw/*84.119*/("""
                        """),_display_(Seq[Any](/*85.26*/defining(contextId.get + key.replaceAll("[() ]","") + resourceId.toString())/*85.102*/ { mid =>_display_(Seq[Any](format.raw/*85.111*/("""
                            """),_display_(Seq[Any](/*86.30*/if(user.isDefined)/*86.48*/ {_display_(Seq[Any](format.raw/*86.50*/("""
                                <a id='"""),_display_(Seq[Any](/*87.41*/mid)),format.raw/*87.44*/("""' href="javascript:void(0)"
                                onmouseover="getMetadataContext('"""),_display_(Seq[Any](/*88.67*/mid)),format.raw/*88.70*/("""','"""),_display_(Seq[Any](/*88.74*/contextId/*88.83*/.get)),format.raw/*88.87*/("""','"""),_display_(Seq[Any](/*88.91*/key)),format.raw/*88.94*/("""');"
                                onmouseout="leaveMetadataContext('"""),_display_(Seq[Any](/*89.68*/mid)),format.raw/*89.71*/("""');">
                                    <strong>"""),_display_(Seq[Any](/*90.46*/key)),format.raw/*90.49*/(""":</strong></a>
                            """)))}/*91.31*/else/*91.36*/{_display_(Seq[Any](format.raw/*91.37*/("""
                                <strong>"""),_display_(Seq[Any](/*92.42*/key)),format.raw/*92.45*/(""":</strong>
                            """)))})),format.raw/*93.30*/("""
                            """),_display_(Seq[Any](/*94.30*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*94.87*/("""
                        """)))})),format.raw/*95.26*/("""
                    """)))}/*96.23*/else/*96.28*/{_display_(Seq[Any](format.raw/*96.29*/("""
                        """),_display_(Seq[Any](/*97.26*/contextURL/*97.36*/ match/*97.42*/ {/*98.29*/case Some(u) =>/*98.44*/ {_display_(Seq[Any](format.raw/*98.46*/("""
                                <a href=""""),_display_(Seq[Any](/*99.43*/u/*99.44*/.toString)),format.raw/*99.53*/("""" target="_blank" title="Show context reference"><strong>"""),_display_(Seq[Any](/*99.111*/key)),format.raw/*99.114*/(""":</strong></a>
                                """),_display_(Seq[Any](/*100.34*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*100.91*/("""
                            """)))}/*102.29*/case None =>/*102.41*/ {_display_(Seq[Any](format.raw/*102.43*/(""" """),_display_(Seq[Any](/*102.45*/printContent(agent,value,contextId,contextURL,resourceId)))))}})),format.raw/*103.26*/("""
                    """)))})),format.raw/*104.22*/("""
                        <!--                                 </li>
 -->                            """)))}})),format.raw/*107.14*/("""

        """)))})),format.raw/*109.10*/("""
        </ul>
    """)))}/*112.5*/case a: JsArray =>/*112.23*/ {_display_(Seq[Any](format.raw/*112.25*/("""
        <ul>
        """),_display_(Seq[Any](/*114.10*/for((value, i) <- a.value.zipWithIndex) yield /*114.49*/ {_display_(Seq[Any](format.raw/*114.51*/("""
            <li class="md-block">"""),_display_(Seq[Any](/*115.35*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*115.92*/("""</li>
        """)))})),format.raw/*116.10*/("""
        </ul>
    """)))}/*119.5*/case s: JsString =>/*119.24*/ {_display_(Seq[Any](format.raw/*119.26*/("""
        """),_display_(Seq[Any](/*120.10*/if(s.value.startsWith("http"))/*120.40*/ {_display_(Seq[Any](format.raw/*120.42*/("""
            <a target="_blank" href=""""),_display_(Seq[Any](/*121.39*/s/*121.40*/.value)),format.raw/*121.46*/("""">"""),_display_(Seq[Any](/*121.49*/s/*121.50*/.value)),format.raw/*121.56*/("""</a>
        """)))}/*122.11*/else/*122.16*/{_display_(Seq[Any](format.raw/*122.17*/("""
            """),_display_(Seq[Any](/*123.14*/s/*123.15*/.value)),format.raw/*123.21*/("""
        """)))})),format.raw/*124.10*/("""
    """)))}/*126.5*/case _ =>/*126.14*/ {_display_(Seq[Any](_display_(Seq[Any](/*126.17*/content))))}})),format.raw/*127.2*/("""
""")))};def /*130.2*/printHeader/*130.13*/(agent: Agent, date: java.util.Date, resourceId: UUID, mid: UUID):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*130.82*/("""
"""),_display_(Seq[Any](/*131.2*/agent/*131.7*/.operation)),format.raw/*131.17*/(""" by
"""),_display_(Seq[Any](/*132.2*/if(agent.displayName.length > 0)/*132.34*/ {_display_(Seq[Any](format.raw/*132.36*/("""
    """),_display_(Seq[Any](/*133.6*/if(agent.typeOfAgent.toLowerCase().indexOf("user") >= 0)/*133.62*/ {_display_(Seq[Any](format.raw/*133.64*/("""
        """),_display_(Seq[Any](/*134.10*/defining(resourceId.toString() + mid.toString())/*134.58*/ { aid =>_display_(Seq[Any](format.raw/*134.67*/("""
            """),_display_(Seq[Any](/*135.14*/if(user.isDefined)/*135.32*/ {_display_(Seq[Any](format.raw/*135.34*/("""

                <a id='"""),_display_(Seq[Any](/*137.25*/aid)),format.raw/*137.28*/("""' href=""""),_display_(Seq[Any](/*137.37*/routes/*137.43*/.Profile.viewProfileUUID(agent.id))),format.raw/*137.77*/(""""
                onmouseover="getAgentContext('"""),_display_(Seq[Any](/*138.48*/aid)),format.raw/*138.51*/("""','"""),_display_(Seq[Any](/*138.55*/agent/*138.60*/.id)),format.raw/*138.63*/("""');"
                onmouseout="leaveAgentContext('"""),_display_(Seq[Any](/*139.49*/aid)),format.raw/*139.52*/("""');">"""),_display_(Seq[Any](/*139.58*/agent/*139.63*/.displayName)),format.raw/*139.75*/("""</a>
            """)))}/*140.15*/else/*140.20*/{_display_(Seq[Any](format.raw/*140.21*/("""
                """),_display_(Seq[Any](/*141.18*/agent/*141.23*/.displayName)),format.raw/*141.35*/("""
            """)))})),format.raw/*142.14*/("""

        """)))})),format.raw/*144.10*/("""
    """)))}/*145.7*/else/*145.12*/{_display_(Seq[Any](format.raw/*145.13*/("""
        """),_display_(Seq[Any](/*146.10*/if(!agent.url.isDefined)/*146.34*/ {_display_(Seq[Any](format.raw/*146.36*/("""
            <a href=""""),_display_(Seq[Any](/*147.23*/agent/*147.28*/.url)),format.raw/*147.32*/("""" target="_blank">"""),_display_(Seq[Any](/*147.51*/agent/*147.56*/.displayName)),format.raw/*147.68*/("""</a>
        """)))}/*148.11*/else/*148.16*/{_display_(Seq[Any](format.raw/*148.17*/("""
            """),_display_(Seq[Any](/*149.14*/agent/*149.19*/.displayName)),format.raw/*149.31*/("""
        """)))})),format.raw/*150.10*/("""
    """)))})),format.raw/*151.6*/("""
""")))}/*152.3*/else/*152.8*/{_display_(Seq[Any](format.raw/*152.9*/("""
    """),_display_(Seq[Any](/*153.6*/if(agent.url.isDefined)/*153.29*/ {_display_(Seq[Any](format.raw/*153.31*/("""
        <a href=""""),_display_(Seq[Any](/*154.19*/agent/*154.24*/.url)),format.raw/*154.28*/("""" target="_blank">"""),_display_(Seq[Any](/*154.47*/agent/*154.52*/.url)),format.raw/*154.56*/("""</a>
    """)))}/*155.7*/else/*155.12*/{_display_(Seq[Any](format.raw/*155.13*/("""
        unknown
    """)))})),format.raw/*157.6*/("""
""")))})),format.raw/*158.2*/("""
    on """),_display_(Seq[Any](/*159.9*/dateFormatter(date))),format.raw/*159.28*/("""
""")))};def /*9.2*/dateFormatter/*9.15*/(date: java.util.Date) = {{
    val formatter = new java.text.SimpleDateFormat("MMM d, yyyy")
    formatter.format(date)
}};
Seq[Any](format.raw/*1.90*/("""
"""),format.raw/*8.1*/("""
"""),format.raw/*12.2*/("""

"""),format.raw/*128.2*/("""

"""),format.raw/*160.2*/("""

"""),_display_(Seq[Any](/*162.2*/if(metadata.size == 0)/*162.24*/ {_display_(Seq[Any](format.raw/*162.26*/("""
        <!--         <p>No metadata available for this resource</p>
 -->    """)))})),format.raw/*164.10*/("""
"""),_display_(Seq[Any](/*165.2*/for((m, i) <- metadata.zipWithIndex) yield /*165.38*/ {_display_(Seq[Any](format.raw/*165.40*/("""
    <div class="panel panel-default metaDataPanel">
        <div class="panel-heading" role="tab" id="heading_"""),_display_(Seq[Any](/*167.60*/m/*167.61*/.id)),format.raw/*167.64*/("""">
            <a data-toggle="collapse" href="#collapse_"""),_display_(Seq[Any](/*168.56*/m/*168.57*/.id)),format.raw/*168.60*/("""" class="collapse-icon">
                <span class="panel-icon glyphicon glyphicon-minus"></span>
            </a>
            <span>
            """),_display_(Seq[Any](/*172.14*/printHeader(m.creator, m.createdAt, m.attachedTo.id, m.id))),format.raw/*172.72*/("""
            </span>
            """),_display_(Seq[Any](/*174.14*/if(toDelete && Permission.checkPermission(Permission.DeleteMetadata, ResourceRef(ResourceRef.metadata, m.id)))/*174.124*/ {_display_(Seq[Any](format.raw/*174.126*/("""
                <a id=""""),_display_(Seq[Any](/*175.25*/m/*175.26*/.id)),format.raw/*175.29*/("""" title="Delete this metadata" class="btn btn-link delete-icon">
                    <span class="panel-icon glyphicon glyphicon-trash"></span>
                </a>
            """)))})),format.raw/*178.14*/("""
        </div>
        <div id="collapse_"""),_display_(Seq[Any](/*180.28*/m/*180.29*/.id)),format.raw/*180.32*/("""" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading_"""),_display_(Seq[Any](/*180.111*/m/*180.112*/.id)),format.raw/*180.115*/("""">
            <div class="panel-body">
                <div class="tree">

                    <a  href="#" class="btn btn-success btn-large" data-toggle="modal" data-target="#metadataModal" id="editMetaData" title="Dataset metadata">
                        <span class="glyphicon glyphicon-plus"></span> View | Edit
                    </a>

                </div>
            </div>
        </div>
    </div>
""")))})),format.raw/*192.2*/("""

    <!-- Modal -->
<div id="metadataModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

            <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Dataset Metadata</h4>
            </div>
            <div id="custMenu">
                <div class="form-group padTop">
                    <div class="showTemplates">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>Select a template:</label><br />
                                    <select class="templates form-control"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="metaDataSettings">
            </div>

                <!--             <div class="modal-body2" style="padding:20px;">
            </div>
 -->            <div class="templateData modal-body">
                <!--Template boxes will be added here -->
            <br />
        </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success pull-left" id="addMetaData">Add New</button>

                <button type="button" class="btn btn-success" id="submitMetaData" data-dismiss="modal">Submit</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script src=""""),_display_(Seq[Any](/*239.15*/routes/*239.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*239.63*/("""" type="text/javascript"></script>
<script>

        $(document).ready(function() """),format.raw/*242.38*/("""{"""),format.raw/*242.39*/("""

            $(".nav-tabs > li.active").removeClass("active")
            var url = window.location.href;
            let params = new URLSearchParams(location.search.slice(1));

            if (url.indexOf("?") >= 0)"""),format.raw/*248.39*/("""{"""),format.raw/*248.40*/("""
                for (let p of params) """),format.raw/*249.39*/("""{"""),format.raw/*249.40*/("""
                    if (p[0] === "ds")"""),format.raw/*250.39*/("""{"""),format.raw/*250.40*/("""
                        $("#tab-metadata1").addClass("active");
                        $("#tab-metadata").addClass("active");
                        console.log("MD");
                    """),format.raw/*254.21*/("""}"""),format.raw/*254.22*/("""else"""),format.raw/*254.26*/("""{"""),format.raw/*254.27*/("""
                        $("#tab-files1").addClass("active");
                        $("#tab-files").addClass("active");
                        console.log("NOT MD");

                    """),format.raw/*259.21*/("""}"""),format.raw/*259.22*/("""
                """),format.raw/*260.17*/("""}"""),format.raw/*260.18*/("""
            """),format.raw/*261.13*/("""}"""),format.raw/*261.14*/("""else"""),format.raw/*261.18*/("""{"""),format.raw/*261.19*/("""
                $("#tab-files1").addClass("active");
                $("#tab-files").addClass("active");
            """),format.raw/*264.13*/("""}"""),format.raw/*264.14*/("""

        """),format.raw/*266.9*/("""}"""),format.raw/*266.10*/(""");

        $(function () """),format.raw/*268.23*/("""{"""),format.raw/*268.24*/("""
            $('[data-toggle="tooltip"]').tooltip();

            $('.collapse')
                    .on('shown.bs.collapse', function()"""),format.raw/*272.56*/("""{"""),format.raw/*272.57*/("""
                        $(this).parent().find(".panel-icon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
                    """),format.raw/*274.21*/("""}"""),format.raw/*274.22*/(""")
                    .on('hidden.bs.collapse', function()"""),format.raw/*275.57*/("""{"""),format.raw/*275.58*/("""
                        $(this).parent().find(".panel-icon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                    """),format.raw/*277.21*/("""}"""),format.raw/*277.22*/(""");

            $('.tree li').on('click', function (e) """),format.raw/*279.52*/("""{"""),format.raw/*279.53*/("""
                //console.log("clicked");
                var children = $(this).find('> ul > li > ul > li');
                if (children.is(":visible")) """),format.raw/*282.46*/("""{"""),format.raw/*282.47*/("""
                    children.hide('fast');
                    $(this).find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                """),format.raw/*285.17*/("""}"""),format.raw/*285.18*/(""" else """),format.raw/*285.24*/("""{"""),format.raw/*285.25*/("""
                    children.show('fast');
                    $(this).find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
                """),format.raw/*288.17*/("""}"""),format.raw/*288.18*/("""
                e.stopPropagation();
            """),format.raw/*290.13*/("""}"""),format.raw/*290.14*/(""");
        """),format.raw/*291.9*/("""}"""),format.raw/*291.10*/(""")

        $(document).on('click', '#editMetaData', function(e)"""),format.raw/*293.61*/("""{"""),format.raw/*293.62*/("""
            $(".modal-body").empty();
            get4ceedMetaData();
        """),format.raw/*296.9*/("""}"""),format.raw/*296.10*/(""");

        $(document).on('click', '#addMetaData', function(e)"""),format.raw/*298.60*/("""{"""),format.raw/*298.61*/("""
            var div = $("<div />");
            div.html(createDiv());
            $(".modal-body").append(div);
        """),format.raw/*302.9*/("""}"""),format.raw/*302.10*/(""");

        $('#metadataModal').on('hidden.bs.modal', function () """),format.raw/*304.63*/("""{"""),format.raw/*304.64*/("""
            // window.location.search = 'ds';
        """),format.raw/*306.9*/("""}"""),format.raw/*306.10*/(""")

        $(document).on('click', '#submitMetaData', function(e)"""),format.raw/*308.63*/("""{"""),format.raw/*308.64*/("""
            var url = window.location.pathname;
            var id = url.substring(url.lastIndexOf('/') + 1);

            checkIfDatasetAttachedTemplate();
            location.reload();            
        """),format.raw/*314.9*/("""}"""),format.raw/*314.10*/(""");

        //Should we show a template for this dataset?
        function get4ceedMetaData() """),format.raw/*317.37*/("""{"""),format.raw/*317.38*/("""
            var url = window.location.pathname;
            var id = url.substring(url.lastIndexOf('/') + 1);
            $.ajax("""),format.raw/*320.20*/("""{"""),format.raw/*320.21*/("""
                url: jsRoutes.api.T2C2.getDatasetWithAttachedVocab(id).url,
                type:"GET",
                dataType: "json",
                beforeSend: function(xhr)"""),format.raw/*324.42*/("""{"""),format.raw/*324.43*/("""
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.setRequestHeader("Accept", "application/json");
                """),format.raw/*327.17*/("""}"""),format.raw/*327.18*/(""",
                success: function(data)"""),format.raw/*328.40*/("""{"""),format.raw/*328.41*/("""
                    if (data.template.id === "none")"""),format.raw/*329.53*/("""{"""),format.raw/*329.54*/("""
                        // console.log(data.template.id);
                    """),format.raw/*331.21*/("""}"""),format.raw/*331.22*/("""else"""),format.raw/*331.26*/("""{"""),format.raw/*331.27*/("""
                        createBoxesForPreviousDataset(data);
                    """),format.raw/*333.21*/("""}"""),format.raw/*333.22*/("""
                """),format.raw/*334.17*/("""}"""),format.raw/*334.18*/("""
            """),format.raw/*335.13*/("""}"""),format.raw/*335.14*/(""")

        """),format.raw/*337.9*/("""}"""),format.raw/*337.10*/("""

        //Does this ds have a template?
        function checkIfDatasetAttachedTemplate() """),format.raw/*340.51*/("""{"""),format.raw/*340.52*/("""

            var url = window.location.pathname;
            var id = url.substring(url.lastIndexOf('/') + 1);

            $.ajax("""),format.raw/*345.20*/("""{"""),format.raw/*345.21*/("""
                url: jsRoutes.api.Datasets.hasAttachedVocabulary(id).url,
                type:"GET",
                dataType: "json",
                beforeSend: function(xhr)"""),format.raw/*349.42*/("""{"""),format.raw/*349.43*/("""
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.setRequestHeader("Accept", "application/json");
                """),format.raw/*352.17*/("""}"""),format.raw/*352.18*/(""",
                success: function(data)"""),format.raw/*353.40*/("""{"""),format.raw/*353.41*/("""
                    if (data.length > 0)"""),format.raw/*354.41*/("""{"""),format.raw/*354.42*/("""
                        edit4CeedMetaData(data[0].id);
                    """),format.raw/*356.21*/("""}"""),format.raw/*356.22*/("""else"""),format.raw/*356.26*/("""{"""),format.raw/*356.27*/("""
                        postTemplate(false, id);
                    """),format.raw/*358.21*/("""}"""),format.raw/*358.22*/("""

                """),format.raw/*360.17*/("""}"""),format.raw/*360.18*/("""
            """),format.raw/*361.13*/("""}"""),format.raw/*361.14*/(""")

        """),format.raw/*363.9*/("""}"""),format.raw/*363.10*/("""

        function postTemplate(templateType, datasetID)"""),format.raw/*365.55*/("""{"""),format.raw/*365.56*/("""

            var templateTerms = buildTemplate();
            var tagName = $('.tagName').val();//.toUpperCase();
            var shareTemplate = $('#checkShareTemplate').is(":checked");
            var datasetName = $(" .datasetName").val();
            var templateType = "false"; //templateType.toString();
            $.ajax("""),format.raw/*372.20*/("""{"""),format.raw/*372.21*/("""
                url: jsRoutes.api.Vocabularies.createVocabularyFromJson(shareTemplate).url,
                type:"POST",
                data: JSON.stringify("""),format.raw/*375.38*/("""{"""),format.raw/*375.39*/(""" name: datasetName, terms: templateTerms, tags: tagName, master: templateType"""),format.raw/*375.116*/("""}"""),format.raw/*375.117*/("""),
                beforeSend: function(xhr)"""),format.raw/*376.42*/("""{"""),format.raw/*376.43*/("""
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.setRequestHeader("Accept", "application/json");
                """),format.raw/*379.17*/("""}"""),format.raw/*379.18*/(""",
                success: function(data)"""),format.raw/*380.40*/("""{"""),format.raw/*380.41*/("""
                    addTemplateToDataset(datasetID, data.id);
                """),format.raw/*382.17*/("""}"""),format.raw/*382.18*/(""",
                error: function(xhr, status, error) """),format.raw/*383.53*/("""{"""),format.raw/*383.54*/("""
                    swal("""),format.raw/*384.26*/("""{"""),format.raw/*384.27*/("""
                        title: "Error",
                        text: "There was a problem creating this template",
                        type: "error",
                        timer: 1500,
                        showConfirmButton: false
                    """),format.raw/*390.21*/("""}"""),format.raw/*390.22*/(""");
                """),format.raw/*391.17*/("""}"""),format.raw/*391.18*/("""
            """),format.raw/*392.13*/("""}"""),format.raw/*392.14*/(""")
        """),format.raw/*393.9*/("""}"""),format.raw/*393.10*/("""

        function buildTemplate() """),format.raw/*395.34*/("""{"""),format.raw/*395.35*/("""
            var metaDataKeys = $.map($(' .metaDataKey'), function (el) """),format.raw/*396.72*/("""{"""),format.raw/*396.73*/(""" return el.value; """),format.raw/*396.91*/("""}"""),format.raw/*396.92*/(""");
            var metaDataVals = $.map($(' .metaDataVal'), function (el) """),format.raw/*397.72*/("""{"""),format.raw/*397.73*/("""return el.value;"""),format.raw/*397.89*/("""}"""),format.raw/*397.90*/(""");
            var metaDataUnits = $.map($(' .metaDataUnit'), function (el) """),format.raw/*398.74*/("""{"""),format.raw/*398.75*/("""return el.value;"""),format.raw/*398.91*/("""}"""),format.raw/*398.92*/(""");
            var metaDataTypes = $.map($(' .metaDataType'), function (el) """),format.raw/*399.74*/("""{"""),format.raw/*399.75*/("""return el.value;"""),format.raw/*399.91*/("""}"""),format.raw/*399.92*/(""");
            var requireField = $.map($(' .requireField'), function (el) """),format.raw/*400.73*/("""{"""),format.raw/*400.74*/("""return el.value;"""),format.raw/*400.90*/("""}"""),format.raw/*400.91*/(""");

            var arr = [];

            $.each(metaDataKeys, function (idx, keyName) """),format.raw/*404.58*/("""{"""),format.raw/*404.59*/("""
                if (keyName != '')"""),format.raw/*405.35*/("""{"""),format.raw/*405.36*/("""
                    var objCombined = """),format.raw/*406.39*/("""{"""),format.raw/*406.40*/("""}"""),format.raw/*406.41*/(""";
                    objCombined['key'] = keyName;
                    objCombined['units'] = metaDataUnits[idx];;
                    objCombined['data_type'] = metaDataTypes[idx];;
                    objCombined['default_value'] = metaDataVals[idx];
                    objCombined['required'] = requireField[idx];

                    arr.push(objCombined);
                """),format.raw/*414.17*/("""}"""),format.raw/*414.18*/("""
            """),format.raw/*415.13*/("""}"""),format.raw/*415.14*/(""");
            return(arr);

        """),format.raw/*418.9*/("""}"""),format.raw/*418.10*/("""

        function addTemplateToDataset(datasetid, templateID)"""),format.raw/*420.61*/("""{"""),format.raw/*420.62*/("""
            var url = "/t2c2/templates/" + templateID + "/attachToDataset/" + datasetid + "";
            $.ajax("""),format.raw/*422.20*/("""{"""),format.raw/*422.21*/("""
                url: url,
                // url: jsRoutes.T2C2.attachVocabToDataset(templateID,datasetid).url,
                type:"PUT",
                beforeSend: function(xhr)"""),format.raw/*426.42*/("""{"""),format.raw/*426.43*/("""
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.setRequestHeader("Accept", "application/json");
                """),format.raw/*429.17*/("""}"""),format.raw/*429.18*/(""",
                data: JSON.stringify("""),format.raw/*430.38*/("""{"""),format.raw/*430.39*/(""" template_id: templateID, dataset_id: datasetid"""),format.raw/*430.86*/("""}"""),format.raw/*430.87*/("""),
                success: function(data)"""),format.raw/*431.40*/("""{"""),format.raw/*431.41*/("""
                    //console.log("attaching");
                """),format.raw/*433.17*/("""}"""),format.raw/*433.18*/(""",
                error: function(xhr, status, error) """),format.raw/*434.53*/("""{"""),format.raw/*434.54*/("""
                """),format.raw/*435.17*/("""}"""),format.raw/*435.18*/("""

            """),format.raw/*437.13*/("""}"""),format.raw/*437.14*/(""")

        """),format.raw/*439.9*/("""}"""),format.raw/*439.10*/("""

        function edit4CeedMetaData(templateId) """),format.raw/*441.48*/("""{"""),format.raw/*441.49*/("""
            var url = window.location.pathname;
            var id = url.substring(url.lastIndexOf('/') + 1);

            var templateTerms = buildTemplate();
            var tagName = ""; //$('.metaDataTags').val().toUpperCase();
            var datasetName = ""; //$(".metaDataName").val();
            var datasetDescription = ""; //$(".metaDataDesc").val();
            var templateId = templateId;

            // console.log(templateTerms);
            // console.log(templateId);
            $.ajax("""),format.raw/*453.20*/("""{"""),format.raw/*453.21*/("""
                url: jsRoutes.api.Vocabularies.editVocabulary(templateId).url,
                type:"PUT",
                dataType: "json",
                data: JSON.stringify("""),format.raw/*457.38*/("""{"""),format.raw/*457.39*/("""name: datasetName, description: datasetDescription, tags: tagName, isPublic: false, terms: templateTerms, attached_dataset: templateId"""),format.raw/*457.173*/("""}"""),format.raw/*457.174*/("""),
                beforeSend: function(xhr)"""),format.raw/*458.42*/("""{"""),format.raw/*458.43*/("""
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.setRequestHeader("Accept", "application/json");
                """),format.raw/*461.17*/("""}"""),format.raw/*461.18*/(""",
                success: function(data)"""),format.raw/*462.40*/("""{"""),format.raw/*462.41*/("""
                """),format.raw/*463.17*/("""}"""),format.raw/*463.18*/(""",
                error: function(xhr, status, error) """),format.raw/*464.53*/("""{"""),format.raw/*464.54*/("""
                """),format.raw/*465.17*/("""}"""),format.raw/*465.18*/("""

            """),format.raw/*467.13*/("""}"""),format.raw/*467.14*/(""")
        """),format.raw/*468.9*/("""}"""),format.raw/*468.10*/("""

        function buildTemplate() """),format.raw/*470.34*/("""{"""),format.raw/*470.35*/("""
            var metaDataKeys = $.map($('.metaDataKey'), function (el) """),format.raw/*471.71*/("""{"""),format.raw/*471.72*/(""" return el.value; """),format.raw/*471.90*/("""}"""),format.raw/*471.91*/(""");
            var metaDataVals = $.map($('.metaDataVal'), function (el) """),format.raw/*472.71*/("""{"""),format.raw/*472.72*/("""return el.value;"""),format.raw/*472.88*/("""}"""),format.raw/*472.89*/(""");
            var metaDataUnits = $.map($('.metaDataUnit'), function (el) """),format.raw/*473.73*/("""{"""),format.raw/*473.74*/("""return el.value;"""),format.raw/*473.90*/("""}"""),format.raw/*473.91*/(""");

            var arr = [];

            $.each(metaDataKeys, function (idx, keyName) """),format.raw/*477.58*/("""{"""),format.raw/*477.59*/("""
                if (keyName != '')"""),format.raw/*478.35*/("""{"""),format.raw/*478.36*/("""
                    var objCombined = """),format.raw/*479.39*/("""{"""),format.raw/*479.40*/("""}"""),format.raw/*479.41*/(""";
                    objCombined['key'] = keyName;
                    objCombined['units'] = metaDataUnits[idx];;
                    objCombined['default_value'] = metaDataVals[idx];
                    arr.push(objCombined);
                """),format.raw/*484.17*/("""}"""),format.raw/*484.18*/("""
            """),format.raw/*485.13*/("""}"""),format.raw/*485.14*/(""");
            return(arr);

        """),format.raw/*488.9*/("""}"""),format.raw/*488.10*/("""

        $(function () """),format.raw/*490.23*/("""{"""),format.raw/*490.24*/("""
            $('.delete-icon').unbind().on('click', function()"""),format.raw/*491.62*/("""{"""),format.raw/*491.63*/("""
                var delete_icon = $(this);

                var request = jsRoutes.api.Metadata.removeMetadata(this.id).ajax("""),format.raw/*494.82*/("""{"""),format.raw/*494.83*/("""
                    type: 'DELETE'
                """),format.raw/*496.17*/("""}"""),format.raw/*496.18*/(""");

                request.done(function (response, textStatus, jqXHR) """),format.raw/*498.69*/("""{"""),format.raw/*498.70*/("""
                    //console.log("success");
                    delete_icon.closest(".panel").remove();
                """),format.raw/*501.17*/("""}"""),format.raw/*501.18*/(""");

                request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*503.71*/("""{"""),format.raw/*503.72*/("""
                    console.error("The following error occured: " + textStatus, errorThrown);
                    var errMsg = "You must be logged in to add metadata";
                    if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*506.64*/("""{"""),format.raw/*506.65*/("""
                        notify("Metadata was not removed due to : " + errorThrown, "error");
                    """),format.raw/*508.21*/("""}"""),format.raw/*508.22*/("""
                """),format.raw/*509.17*/("""}"""),format.raw/*509.18*/(""");
            """),format.raw/*510.13*/("""}"""),format.raw/*510.14*/(""");
        """),format.raw/*511.9*/("""}"""),format.raw/*511.10*/(""")

        // get metadata definitions
        function getMetadataContext(mid, uuid, key) """),format.raw/*514.53*/("""{"""),format.raw/*514.54*/("""
            var request = jsRoutes.api.ContextLD.getContextById(uuid).ajax("""),format.raw/*515.76*/("""{"""),format.raw/*515.77*/("""
                type: 'GET',
                contentType: "application/json"
            """),format.raw/*518.13*/("""}"""),format.raw/*518.14*/(""");

            request.done(function (response, textStatus, jqXHR) """),format.raw/*520.65*/("""{"""),format.raw/*520.66*/("""
                var fields = response;
                var context = "Context is not defined.";
                if (fields['@context']) """),format.raw/*523.42*/("""{"""),format.raw/*523.43*/("""
                    context = JSON.stringify(fields['@context'][key]);
                """),format.raw/*525.17*/("""}"""),format.raw/*525.18*/("""
                $("#"+mid).popover("""),format.raw/*526.36*/("""{"""),format.raw/*526.37*/("""
                    content:context,
                    trigger:'hover',
                    placement:'top',
                    template: '<div class="popover" role="tooltip" style="max-width:600px;word-break:break-all"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
                """),format.raw/*531.17*/("""}"""),format.raw/*531.18*/(""");
                $("#"+mid).popover('show');
            """),format.raw/*533.13*/("""}"""),format.raw/*533.14*/(""");

            request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*535.67*/("""{"""),format.raw/*535.68*/("""
                console.error("The following error occured: " + textStatus, errorThrown);
                var errMsg = "You must be logged in to retrieve metadata definitions";
                if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*538.60*/("""{"""),format.raw/*538.61*/("""
                    notify("Metadata context was not shown due to : " + errorThrown, "error");
                """),format.raw/*540.17*/("""}"""),format.raw/*540.18*/("""
            """),format.raw/*541.13*/("""}"""),format.raw/*541.14*/(""");
        """),format.raw/*542.9*/("""}"""),format.raw/*542.10*/("""
        function leaveMetadataContext(mid) """),format.raw/*543.44*/("""{"""),format.raw/*543.45*/("""
            $("#"+mid).popover('hide');
        """),format.raw/*545.9*/("""}"""),format.raw/*545.10*/("""

        // get agent author definitions
        function getAgentContext(aid, uuid) """),format.raw/*548.45*/("""{"""),format.raw/*548.46*/("""
            var request = jsRoutes.api.Users.findById(uuid).ajax("""),format.raw/*549.66*/("""{"""),format.raw/*549.67*/("""
                type: 'GET',
                contentType: "application/json"
            """),format.raw/*552.13*/("""}"""),format.raw/*552.14*/(""");

            request.done(function (response, textStatus, jqXHR) """),format.raw/*554.65*/("""{"""),format.raw/*554.66*/("""
                var fields = response;
                //console.log(fields['fullName']);
                $("#"+aid).popover("""),format.raw/*557.36*/("""{"""),format.raw/*557.37*/("""
                    content:fields['email'],
                    trigger:'hover',
                    placement:'top',
                    template: '<div class="popover" role="tooltip" style="max-width:600px;word-break:break-all"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
                """),format.raw/*562.17*/("""}"""),format.raw/*562.18*/(""");
                $("#"+aid).popover('show');
            """),format.raw/*564.13*/("""}"""),format.raw/*564.14*/(""");

            request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*566.67*/("""{"""),format.raw/*566.68*/("""
                console.error("The following error occured: " + textStatus, errorThrown);
                var errMsg = "You must be logged in to retrieve metadata definitions";
                if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*569.60*/("""{"""),format.raw/*569.61*/("""
                    notify("Agent metadata context was not shown due to : " + errorThrown, "error");
                """),format.raw/*571.17*/("""}"""),format.raw/*571.18*/("""
            """),format.raw/*572.13*/("""}"""),format.raw/*572.14*/(""");
        """),format.raw/*573.9*/("""}"""),format.raw/*573.10*/("""

        function leaveAgentContext(aid) """),format.raw/*575.41*/("""{"""),format.raw/*575.42*/("""
            $("#"+aid).popover('hide');
        """),format.raw/*577.9*/("""}"""),format.raw/*577.10*/("""

        function createBoxesForPreviousDataset(data)"""),format.raw/*579.53*/("""{"""),format.raw/*579.54*/("""
            var div1 = "<div><input type='hidden' id='templateId' value="+data.template.id+" /></div>";
            $(".modal-body").append(div1);

            // var div2 = $("<div />");
            // div2.html(createOtherDiv(data.template.name, data.template.description, data.template.tags));
            // $(".modal-body2").append(div2);

            $.each(data.template.terms, function(i, val) """),format.raw/*587.58*/("""{"""),format.raw/*587.59*/("""
                var div3 = $("<div />");
                div3.html(createDiv(val.key, val.default_value, val.units));
                $(".modal-body").append(div3);
            """),format.raw/*591.13*/("""}"""),format.raw/*591.14*/(""");
        """),format.raw/*592.9*/("""}"""),format.raw/*592.10*/("""

        function counter() """),format.raw/*594.28*/("""{"""),format.raw/*594.29*/("""
            var count = 0;

            this.reset = function() """),format.raw/*597.37*/("""{"""),format.raw/*597.38*/("""
                count = 0;
                return count;
            """),format.raw/*600.13*/("""}"""),format.raw/*600.14*/(""";

            this.add = function() """),format.raw/*602.35*/("""{"""),format.raw/*602.36*/("""
                return ++count;
            """),format.raw/*604.13*/("""}"""),format.raw/*604.14*/(""";
        """),format.raw/*605.9*/("""}"""),format.raw/*605.10*/("""

        $("body").on("click", ".remove", function () """),format.raw/*607.54*/("""{"""),format.raw/*607.55*/("""
            $(this).closest(".top-buffer").remove();
        """),format.raw/*609.9*/("""}"""),format.raw/*609.10*/(""");

        var counter1 = new counter();


        //Create dynamic textbox
        function createOtherDiv(name, desc, tags) """),format.raw/*615.51*/("""{"""),format.raw/*615.52*/("""
            var valKeyName = $.trim(name);
            var valStr = $.trim(desc);
            var valUnits = $.trim(tags);

            return '<div class="row top-buffer"><div class="col-xs-4"><b>' + "<label for='metaDataName'>Name: " + '</b></label>' +
                    '<input class="metaDataName form-control" name="metaDataName" id="metaDataName" type="text" value=' + valKeyName.replace(/ /g,"&nbsp;") +'></div>' +

                    '<div class="col-xs-4"><b>' + "<label for='metaDataDesc'>Description:</b></label>" +
                    '<input class="metaDataDesc form-control" name="metaDataDesc" id="metaDataDesc" type="text" value=' + valStr.replace(/ /g,"&nbsp;") +'></div>' +

                    '<div class="col-xs-4"><b>' + "<label for='metaDataTags'>Tags: " + '</label></b>' +
                    '<input class="metaDataTags form-control" name="metaDataTags" id="metaDataTags" type="text" value=' + valUnits.replace(/ /g,"&nbsp;") +'></div></div><br />'

        """),format.raw/*629.9*/("""}"""),format.raw/*629.10*/("""

        //Create dynamic textbox
        function createDiv(keyName, val, units) """),format.raw/*632.49*/("""{"""),format.raw/*632.50*/("""
            //console.log(keyName, val, units);
            var valKeyName = $.trim(keyName);
            var valStr = $.trim(val);
            var valUnits = $.trim(units);

            //format text
            var txtToWrite = "Value";

            var i = counter1.add();
            return '<div class="row top-buffer"><div class="col-xs-6"><b>' + "<label for='name'>Key: " + '</b></label>' +
                    '<input class="metaDataKey form-control" name="metaDataKey' + i + '" id="metaDataKey' + i + '" type="text" value=' + valKeyName.replace(/ /g,"&nbsp;") +'></div>' +

                    '<div class="col-xs-2" style="margin-left:-15px;"><b>' + "<label for='val'>"+ txtToWrite + '</b></label>' +
                    '<input class="metaDataVal form-control" name="metaDataVal' + i + '" id="metaDataVal' + i + '" type="text" value=' + valStr.replace(/ /g,"&nbsp;") +'></div>' +


                    '<div class="col-xs-2"><b>' + "<label for='name'>Units: " + '</label></b>' +
                    '<input class="metaDataUnit form-control" name="metaDataUnit' + i + '" id="metaDataUnit' + i + '" type="text" value=' + valUnits.replace(/ /g,"&nbsp;") +'></div>' +

                    '<div class="col-xs-1" style="margin-left:-15px;"><b>' + "<label for='val'>&nbsp;" + '</label></b>' +
                    '<input type="button" style="background-color:#d9534f;" value="Remove" class="remove btn btn-danger btnRemove" name="btnRemove' + i + '" id="btnRemove' + i + '"></div>' +
                    '<span class="up"></span><span class="down"></span></div>'

        """),format.raw/*656.9*/("""}"""),format.raw/*656.10*/("""

        function moveUp(element) """),format.raw/*658.34*/("""{"""),format.raw/*658.35*/("""
            if(element.previousElementSibling)
                element.parentNode.insertBefore(element, element.previousElementSibling);
        """),format.raw/*661.9*/("""}"""),format.raw/*661.10*/("""
        function moveDown(element) """),format.raw/*662.36*/("""{"""),format.raw/*662.37*/("""
            if(element.nextElementSibling)
                element.parentNode.insertBefore(element.nextElementSibling, element);
        """),format.raw/*665.9*/("""}"""),format.raw/*665.10*/("""
        document.querySelector('ul').addEventListener('click', function(e) """),format.raw/*666.76*/("""{"""),format.raw/*666.77*/("""
            if(e.target.className === 'down') moveDown(e.target.parentNode);
            else if(e.target.className === 'up') moveUp(e.target.parentNode);
        """),format.raw/*669.9*/("""}"""),format.raw/*669.10*/(""");

        //TEMPLATES
        function getTemplates() """),format.raw/*672.33*/("""{"""),format.raw/*672.34*/("""
            $.ajax("""),format.raw/*673.20*/("""{"""),format.raw/*673.21*/("""
                url: jsRoutes.api.Vocabularies.list().url,
                type:"GET",
                dataType: "json",
                beforeSend: function(xhr)"""),format.raw/*677.42*/("""{"""),format.raw/*677.43*/("""
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.setRequestHeader("Accept", "application/json");
                """),format.raw/*680.17*/("""}"""),format.raw/*680.18*/(""",
                success: function(data)"""),format.raw/*681.40*/("""{"""),format.raw/*681.41*/("""
                    //console.log("yes");
                    $('<option>').val('').text('--Select One--').appendTo('.templates');
                    showTemplates(data);
                """),format.raw/*685.17*/("""}"""),format.raw/*685.18*/(""",
                error: function(xhr, status, error) """),format.raw/*686.53*/("""{"""),format.raw/*686.54*/("""
                    //console.log("no");
                    swal("""),format.raw/*688.26*/("""{"""),format.raw/*688.27*/("""
                        title: "Error",
                        text: "There was a problem returning custom templates",
                        type: "error",
                        timer: 1500,
                        showConfirmButton: false
                    """),format.raw/*694.21*/("""}"""),format.raw/*694.22*/(""");
                """),format.raw/*695.17*/("""}"""),format.raw/*695.18*/("""
            """),format.raw/*696.13*/("""}"""),format.raw/*696.14*/(""")

        """),format.raw/*698.9*/("""}"""),format.raw/*698.10*/("""

        //Load user templates
        function showTemplates(data) """),format.raw/*701.38*/("""{"""),format.raw/*701.39*/("""
            console.log(data);

            $.each(data, function(key, val) """),format.raw/*704.45*/("""{"""),format.raw/*704.46*/("""

                $(".templates").append($("<option class='placeholder'></option>").val(val.id).html(val.name));
            """),format.raw/*707.13*/("""}"""),format.raw/*707.14*/(""");

            $(".templates").focus();
        """),format.raw/*710.9*/("""}"""),format.raw/*710.10*/("""

        function getTemplate(id)"""),format.raw/*712.33*/("""{"""),format.raw/*712.34*/("""
            $.ajax("""),format.raw/*713.20*/("""{"""),format.raw/*713.21*/("""
                url: jsRoutes.api.T2C2.getVocabulary(id).url,
                type:"GET",
                dataType: "json",
                beforeSend: function(xhr)"""),format.raw/*717.42*/("""{"""),format.raw/*717.43*/("""
                    xhr.setRequestHeader("Content-Type", "application/json");
                    xhr.setRequestHeader("Accept", "application/json");
                """),format.raw/*720.17*/("""}"""),format.raw/*720.18*/(""",
                success: function(data)"""),format.raw/*721.40*/("""{"""),format.raw/*721.41*/("""
                    createBoxes(data);
                """),format.raw/*723.17*/("""}"""),format.raw/*723.18*/(""",
                error: function(xhr, status, error) """),format.raw/*724.53*/("""{"""),format.raw/*724.54*/("""
                    swal("""),format.raw/*725.26*/("""{"""),format.raw/*725.27*/("""
                        title: "Error",
                        text: "There was a problem returning global templates",
                        type: "error",
                        timer: 1500,
                        showConfirmButton: false
                    """),format.raw/*731.21*/("""}"""),format.raw/*731.22*/(""");
                """),format.raw/*732.17*/("""}"""),format.raw/*732.18*/("""
            """),format.raw/*733.13*/("""}"""),format.raw/*733.14*/(""")
        """),format.raw/*734.9*/("""}"""),format.raw/*734.10*/("""

        function createBoxes(data)"""),format.raw/*736.35*/("""{"""),format.raw/*736.36*/("""
            $(".templateData").empty();
            $(".btnDataset").show();
            $(".btnAdd").show();
            $("#btnTemplate").show();
            $(".otherOptions").show();
            var menuName = $('.nav-tabs .active > a').attr("href");
            //Get current tab and use name to determine what the label will say based on it's tab
            console.log(menuName);
            $.each(data.terms, function(key, val) """),format.raw/*745.51*/("""{"""),format.raw/*745.52*/("""
                var div = $("<div />");
                div.html(createDiv(val.key, val.default_value, val.units, val.data_type, val.required));
                $(menuName + ' ' + ".templateData").append(div);
            """),format.raw/*749.13*/("""}"""),format.raw/*749.14*/(""");

            // disableRequiredInput();

        """),format.raw/*753.9*/("""}"""),format.raw/*753.10*/("""

        //Handle template load when new menu item is selected
        $(document).on('change', '.templates', function()"""),format.raw/*756.58*/("""{"""),format.raw/*756.59*/("""

            var id = $(this).val();

            // $(".tagTemplates").val([]);
            // $(".tagData").hide();
            // $(".globalTemplates").val([]);
            // $(".templateSearch").val('');

            if (id != '')"""),format.raw/*765.26*/("""{"""),format.raw/*765.27*/("""
                getTemplate(id);
            """),format.raw/*767.13*/("""}"""),format.raw/*767.14*/("""
        """),format.raw/*768.9*/("""}"""),format.raw/*768.10*/(""");
        getTemplates();

</script>"""))}
    }
    
    def render(metadata:List[models.Metadata],toDelete:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(metadata,toDelete)(user)
    
    def f:((List[models.Metadata],Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (metadata,toDelete) => (user) => apply(metadata,toDelete)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Aug 29 17:10:56 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/metadatald/view.scala.html
                    HASH: 0dfdf645e4ab28a0d68f41ab21b2f5d80ed5f1d4
                    MATRIX: 640->1|991->416|1012->428|1198->532|1235->534|1250->541|1265->547|1275->554|1303->573|1343->575|1402->598|1447->627|1487->629|1537->643|1551->648|1566->654|1577->673|1605->692|1645->694|1994->1007|2083->1086|2124->1088|2330->1258|2416->1334|2464->1343|2530->1373|2557->1391|2597->1393|2674->1434|2699->1437|2829->1531|2854->1534|2894->1538|2912->1547|2938->1551|2978->1555|3003->1558|3111->1630|3136->1633|3223->1684|3248->1687|3311->1732|3324->1737|3363->1738|3441->1780|3466->1783|3538->1823|3604->1853|3683->1910|3741->1936|3786->1963|3799->1968|3838->1969|3896->1991|3915->2001|3930->2007|3941->2034|3965->2049|4005->2051|4080->2090|4090->2091|4121->2100|4216->2158|4242->2161|4322->2205|4402->2263|4447->2314|4468->2326|4508->2328|4582->2366|4607->2369|4655->2381|4735->2438|4794->2486|4848->2508|4911->2569|4938->2587|4978->2589|5237->2812|5326->2891|5367->2893|5433->2923|5519->2999|5567->3008|5637->3042|5664->3060|5704->3062|5785->3107|5810->3110|5944->3208|5969->3211|6009->3215|6027->3224|6053->3228|6093->3232|6118->3235|6230->3311|6255->3314|6346->3369|6371->3372|6438->3421|6451->3426|6490->3427|6572->3473|6597->3476|6673->3520|6743->3554|6822->3611|6884->3641|6929->3668|6942->3673|6981->3674|7047->3704|7066->3714|7081->3720|7092->3755|7116->3770|7156->3772|7239->3819|7249->3820|7280->3829|7375->3887|7401->3890|7489->3942|7568->3999|7621->4066|7642->4078|7682->4080|7764->4126|7789->4129|7837->4141|7917->4198|7984->4262|8042->4288|8105->4349|8123->4358|8163->4360|8320->4481|8409->4560|8450->4562|8512->4588|8598->4664|8646->4673|8712->4703|8739->4721|8779->4723|8856->4764|8881->4767|9011->4861|9036->4864|9076->4868|9094->4877|9120->4881|9160->4885|9185->4888|9293->4960|9318->4963|9405->5014|9430->5017|9493->5062|9506->5067|9545->5068|9623->5110|9648->5113|9720->5153|9786->5183|9865->5240|9923->5266|9964->5289|9977->5294|10016->5295|10078->5321|10097->5331|10112->5337|10123->5368|10147->5383|10187->5385|10266->5428|10276->5429|10307->5438|10402->5496|10428->5499|10513->5547|10593->5604|10643->5663|10665->5675|10706->5677|10745->5679|10831->5763|10886->5785|11021->5900|11065->5911|11104->5936|11132->5954|11173->5956|11233->5979|11289->6018|11330->6020|11402->6055|11482->6112|11530->6127|11569->6152|11598->6171|11639->6173|11686->6183|11726->6213|11767->6215|11843->6254|11854->6255|11883->6261|11923->6264|11934->6265|11963->6271|11997->6286|12011->6291|12051->6292|12102->6306|12113->6307|12142->6313|12185->6323|12210->6334|12229->6343|12279->6346|12314->6356|12340->6361|12361->6372|12512->6441|12550->6443|12564->6448|12597->6458|12638->6463|12680->6495|12721->6497|12763->6503|12829->6559|12870->6561|12917->6571|12975->6619|13023->6628|13074->6642|13102->6660|13143->6662|13206->6688|13232->6691|13278->6700|13294->6706|13351->6740|13437->6789|13463->6792|13504->6796|13519->6801|13545->6804|13635->6857|13661->6860|13704->6866|13719->6871|13754->6883|13792->6902|13806->6907|13846->6908|13901->6926|13916->6931|13951->6943|13998->6957|14042->6968|14067->6975|14081->6980|14121->6981|14168->6991|14202->7015|14243->7017|14303->7040|14318->7045|14345->7049|14401->7068|14416->7073|14451->7085|14485->7100|14499->7105|14539->7106|14590->7120|14605->7125|14640->7137|14683->7147|14721->7153|14742->7156|14755->7161|14794->7162|14836->7168|14869->7191|14910->7193|14966->7212|14981->7217|15008->7221|15064->7240|15079->7245|15106->7249|15135->7260|15149->7265|15189->7266|15243->7288|15277->7290|15322->7299|15364->7318|15388->278|15409->291|15561->89|15588->276|15616->413|15646->6358|15676->7320|15715->7323|15747->7345|15788->7347|15899->7425|15937->7427|15990->7463|16031->7465|16180->7577|16191->7578|16217->7581|16312->7639|16323->7640|16349->7643|16535->7792|16616->7850|16687->7884|16808->7994|16850->7996|16912->8021|16923->8022|16949->8025|17160->8203|17240->8246|17251->8247|17277->8250|17394->8329|17406->8330|17433->8333|17879->8747|19660->10491|19676->10497|19741->10539|19852->10621|19882->10622|20129->10840|20159->10841|20227->10880|20257->10881|20325->10920|20355->10921|20575->11112|20605->11113|20638->11117|20668->11118|20887->11308|20917->11309|20963->11326|20993->11327|21035->11340|21065->11341|21098->11345|21128->11346|21275->11464|21305->11465|21343->11475|21373->11476|21428->11502|21458->11503|21623->11639|21653->11640|21823->11781|21853->11782|21940->11840|21970->11841|22140->11982|22170->11983|22254->12038|22284->12039|22469->12195|22499->12196|22700->12368|22730->12369|22765->12375|22795->12376|22995->12547|23025->12548|23104->12598|23134->12599|23173->12610|23203->12611|23295->12674|23325->12675|23432->12754|23462->12755|23554->12818|23584->12819|23734->12941|23764->12942|23859->13008|23889->13009|23972->13064|24002->13065|24096->13130|24126->13131|24363->13340|24393->13341|24516->13435|24546->13436|24705->13566|24735->13567|24944->13747|24974->13748|25170->13915|25200->13916|25270->13957|25300->13958|25382->14011|25412->14012|25520->14091|25550->14092|25583->14096|25613->14097|25724->14179|25754->14180|25800->14197|25830->14198|25872->14211|25902->14212|25941->14223|25971->14224|26092->14316|26122->14317|26283->14449|26313->14450|26520->14628|26550->14629|26746->14796|26776->14797|26846->14838|26876->14839|26946->14880|26976->14881|27081->14957|27111->14958|27144->14962|27174->14963|27273->15033|27303->15034|27350->15052|27380->15053|27422->15066|27452->15067|27491->15078|27521->15079|27606->15135|27636->15136|27995->15466|28025->15467|28213->15626|28243->15627|28350->15704|28381->15705|28454->15749|28484->15750|28680->15917|28710->15918|28780->15959|28810->15960|28918->16039|28948->16040|29031->16094|29061->16095|29116->16121|29146->16122|29437->16384|29467->16385|29515->16404|29545->16405|29587->16418|29617->16419|29655->16429|29685->16430|29749->16465|29779->16466|29880->16538|29910->16539|29957->16557|29987->16558|30090->16632|30120->16633|30165->16649|30195->16650|30300->16726|30330->16727|30375->16743|30405->16744|30510->16820|30540->16821|30585->16837|30615->16838|30719->16913|30749->16914|30794->16930|30824->16931|30941->17019|30971->17020|31035->17055|31065->17056|31133->17095|31163->17096|31193->17097|31601->17476|31631->17477|31673->17490|31703->17491|31768->17528|31798->17529|31889->17591|31919->17592|32062->17706|32092->17707|32303->17889|32333->17890|32529->18057|32559->18058|32627->18097|32657->18098|32733->18145|32763->18146|32834->18188|32864->18189|32958->18254|32988->18255|33071->18309|33101->18310|33147->18327|33177->18328|33220->18342|33250->18343|33289->18354|33319->18355|33397->18404|33427->18405|33964->18913|33994->18914|34202->19093|34232->19094|34396->19228|34427->19229|34500->19273|34530->19274|34726->19441|34756->19442|34826->19483|34856->19484|34902->19501|34932->19502|35015->19556|35045->19557|35091->19574|35121->19575|35164->19589|35194->19590|35232->19600|35262->19601|35326->19636|35356->19637|35456->19708|35486->19709|35533->19727|35563->19728|35665->19801|35695->19802|35740->19818|35770->19819|35874->19894|35904->19895|35949->19911|35979->19912|36096->20000|36126->20001|36190->20036|36220->20037|36288->20076|36318->20077|36348->20078|36622->20323|36652->20324|36694->20337|36724->20338|36789->20375|36819->20376|36872->20400|36902->20401|36993->20463|37023->20464|37178->20590|37208->20591|37289->20643|37319->20644|37420->20716|37450->20717|37602->20840|37632->20841|37735->20915|37765->20916|38026->21148|38056->21149|38199->21263|38229->21264|38275->21281|38305->21282|38349->21297|38379->21298|38418->21309|38448->21310|38568->21401|38598->21402|38703->21478|38733->21479|38852->21569|38882->21570|38979->21638|39009->21639|39175->21777|39205->21778|39322->21867|39352->21868|39417->21904|39447->21905|39815->22244|39845->22245|39933->22304|39963->22305|40062->22375|40092->22376|40358->22613|40388->22614|40529->22726|40559->22727|40601->22740|40631->22741|40670->22752|40700->22753|40773->22797|40803->22798|40880->22847|40910->22848|41025->22934|41055->22935|41150->23001|41180->23002|41299->23092|41329->23093|41426->23161|41456->23162|41611->23288|41641->23289|42017->23636|42047->23637|42135->23696|42165->23697|42264->23767|42294->23768|42560->24005|42590->24006|42737->24124|42767->24125|42809->24138|42839->24139|42878->24150|42908->24151|42979->24193|43009->24194|43086->24243|43116->24244|43199->24298|43229->24299|43661->24702|43691->24703|43898->24881|43928->24882|43967->24893|43997->24894|44055->24923|44085->24924|44179->24989|44209->24990|44308->25060|44338->25061|44404->25098|44434->25099|44508->25144|44538->25145|44576->25155|44606->25156|44690->25211|44720->25212|44810->25274|44840->25275|44996->25402|45026->25403|46041->26390|46071->26391|46183->26474|46213->26475|47819->28053|47849->28054|47913->28089|47943->28090|48117->28236|48147->28237|48212->28273|48242->28274|48408->28412|48438->28413|48543->28489|48573->28490|48765->28654|48795->28655|48880->28711|48910->28712|48959->28732|48989->28733|49181->28896|49211->28897|49407->29064|49437->29065|49507->29106|49537->29107|49755->29296|49785->29297|49868->29351|49898->29352|49994->29419|50024->29420|50319->29686|50349->29687|50397->29706|50427->29707|50469->29720|50499->29721|50538->29732|50568->29733|50666->29802|50696->29803|50802->29880|50832->29881|50986->30006|51016->30007|51093->30056|51123->30057|51186->30091|51216->30092|51265->30112|51295->30113|51490->30279|51520->30280|51716->30447|51746->30448|51816->30489|51846->30490|51931->30546|51961->30547|52044->30601|52074->30602|52129->30628|52159->30629|52454->30895|52484->30896|52532->30915|52562->30916|52604->30929|52634->30930|52672->30940|52702->30941|52767->30977|52797->30978|53265->31417|53295->31418|53547->31641|53577->31642|53657->31694|53687->31695|53837->31816|53867->31817|54132->32053|54162->32054|54237->32100|54267->32101|54304->32110|54334->32111
                    LINES: 20->1|33->14|33->14|35->14|36->15|36->15|36->15|36->16|36->16|36->16|38->18|38->18|38->18|39->19|39->19|39->19|39->20|39->20|39->20|45->26|45->26|45->26|47->28|47->28|47->28|48->29|48->29|48->29|49->30|49->30|50->31|50->31|50->31|50->31|50->31|50->31|50->31|51->32|51->32|52->33|52->33|53->34|53->34|53->34|54->35|54->35|55->36|56->37|56->37|57->38|58->39|58->39|58->39|59->40|59->40|59->40|59->41|59->41|59->41|60->42|60->42|60->42|60->42|60->42|61->43|61->43|62->45|62->45|62->45|63->46|63->46|63->46|63->46|64->48|65->49|67->52|67->52|67->52|72->57|72->57|72->57|73->58|73->58|73->58|74->59|74->59|74->59|75->60|75->60|76->61|76->61|76->61|76->61|76->61|76->61|76->61|77->62|77->62|78->63|78->63|79->64|79->64|79->64|80->65|80->65|81->66|82->67|82->67|83->68|84->69|84->69|84->69|85->70|85->70|85->70|85->71|85->71|85->71|86->72|86->72|86->72|86->72|86->72|87->73|87->73|88->75|88->75|88->75|89->76|89->76|89->76|89->76|90->78|91->79|93->82|93->82|93->82|95->84|95->84|95->84|96->85|96->85|96->85|97->86|97->86|97->86|98->87|98->87|99->88|99->88|99->88|99->88|99->88|99->88|99->88|100->89|100->89|101->90|101->90|102->91|102->91|102->91|103->92|103->92|104->93|105->94|105->94|106->95|107->96|107->96|107->96|108->97|108->97|108->97|108->98|108->98|108->98|109->99|109->99|109->99|109->99|109->99|110->100|110->100|111->102|111->102|111->102|111->102|111->103|112->104|114->107|116->109|118->112|118->112|118->112|120->114|120->114|120->114|121->115|121->115|122->116|124->119|124->119|124->119|125->120|125->120|125->120|126->121|126->121|126->121|126->121|126->121|126->121|127->122|127->122|127->122|128->123|128->123|128->123|129->124|130->126|130->126|130->126|130->127|131->130|131->130|133->130|134->131|134->131|134->131|135->132|135->132|135->132|136->133|136->133|136->133|137->134|137->134|137->134|138->135|138->135|138->135|140->137|140->137|140->137|140->137|140->137|141->138|141->138|141->138|141->138|141->138|142->139|142->139|142->139|142->139|142->139|143->140|143->140|143->140|144->141|144->141|144->141|145->142|147->144|148->145|148->145|148->145|149->146|149->146|149->146|150->147|150->147|150->147|150->147|150->147|150->147|151->148|151->148|151->148|152->149|152->149|152->149|153->150|154->151|155->152|155->152|155->152|156->153|156->153|156->153|157->154|157->154|157->154|157->154|157->154|157->154|158->155|158->155|158->155|160->157|161->158|162->159|162->159|163->9|163->9|167->1|168->8|169->12|171->128|173->160|175->162|175->162|175->162|177->164|178->165|178->165|178->165|180->167|180->167|180->167|181->168|181->168|181->168|185->172|185->172|187->174|187->174|187->174|188->175|188->175|188->175|191->178|193->180|193->180|193->180|193->180|193->180|193->180|205->192|252->239|252->239|252->239|255->242|255->242|261->248|261->248|262->249|262->249|263->250|263->250|267->254|267->254|267->254|267->254|272->259|272->259|273->260|273->260|274->261|274->261|274->261|274->261|277->264|277->264|279->266|279->266|281->268|281->268|285->272|285->272|287->274|287->274|288->275|288->275|290->277|290->277|292->279|292->279|295->282|295->282|298->285|298->285|298->285|298->285|301->288|301->288|303->290|303->290|304->291|304->291|306->293|306->293|309->296|309->296|311->298|311->298|315->302|315->302|317->304|317->304|319->306|319->306|321->308|321->308|327->314|327->314|330->317|330->317|333->320|333->320|337->324|337->324|340->327|340->327|341->328|341->328|342->329|342->329|344->331|344->331|344->331|344->331|346->333|346->333|347->334|347->334|348->335|348->335|350->337|350->337|353->340|353->340|358->345|358->345|362->349|362->349|365->352|365->352|366->353|366->353|367->354|367->354|369->356|369->356|369->356|369->356|371->358|371->358|373->360|373->360|374->361|374->361|376->363|376->363|378->365|378->365|385->372|385->372|388->375|388->375|388->375|388->375|389->376|389->376|392->379|392->379|393->380|393->380|395->382|395->382|396->383|396->383|397->384|397->384|403->390|403->390|404->391|404->391|405->392|405->392|406->393|406->393|408->395|408->395|409->396|409->396|409->396|409->396|410->397|410->397|410->397|410->397|411->398|411->398|411->398|411->398|412->399|412->399|412->399|412->399|413->400|413->400|413->400|413->400|417->404|417->404|418->405|418->405|419->406|419->406|419->406|427->414|427->414|428->415|428->415|431->418|431->418|433->420|433->420|435->422|435->422|439->426|439->426|442->429|442->429|443->430|443->430|443->430|443->430|444->431|444->431|446->433|446->433|447->434|447->434|448->435|448->435|450->437|450->437|452->439|452->439|454->441|454->441|466->453|466->453|470->457|470->457|470->457|470->457|471->458|471->458|474->461|474->461|475->462|475->462|476->463|476->463|477->464|477->464|478->465|478->465|480->467|480->467|481->468|481->468|483->470|483->470|484->471|484->471|484->471|484->471|485->472|485->472|485->472|485->472|486->473|486->473|486->473|486->473|490->477|490->477|491->478|491->478|492->479|492->479|492->479|497->484|497->484|498->485|498->485|501->488|501->488|503->490|503->490|504->491|504->491|507->494|507->494|509->496|509->496|511->498|511->498|514->501|514->501|516->503|516->503|519->506|519->506|521->508|521->508|522->509|522->509|523->510|523->510|524->511|524->511|527->514|527->514|528->515|528->515|531->518|531->518|533->520|533->520|536->523|536->523|538->525|538->525|539->526|539->526|544->531|544->531|546->533|546->533|548->535|548->535|551->538|551->538|553->540|553->540|554->541|554->541|555->542|555->542|556->543|556->543|558->545|558->545|561->548|561->548|562->549|562->549|565->552|565->552|567->554|567->554|570->557|570->557|575->562|575->562|577->564|577->564|579->566|579->566|582->569|582->569|584->571|584->571|585->572|585->572|586->573|586->573|588->575|588->575|590->577|590->577|592->579|592->579|600->587|600->587|604->591|604->591|605->592|605->592|607->594|607->594|610->597|610->597|613->600|613->600|615->602|615->602|617->604|617->604|618->605|618->605|620->607|620->607|622->609|622->609|628->615|628->615|642->629|642->629|645->632|645->632|669->656|669->656|671->658|671->658|674->661|674->661|675->662|675->662|678->665|678->665|679->666|679->666|682->669|682->669|685->672|685->672|686->673|686->673|690->677|690->677|693->680|693->680|694->681|694->681|698->685|698->685|699->686|699->686|701->688|701->688|707->694|707->694|708->695|708->695|709->696|709->696|711->698|711->698|714->701|714->701|717->704|717->704|720->707|720->707|723->710|723->710|725->712|725->712|726->713|726->713|730->717|730->717|733->720|733->720|734->721|734->721|736->723|736->723|737->724|737->724|738->725|738->725|744->731|744->731|745->732|745->732|746->733|746->733|747->734|747->734|749->736|749->736|758->745|758->745|762->749|762->749|766->753|766->753|769->756|769->756|778->765|778->765|780->767|780->767|781->768|781->768
                    -- GENERATED --
                */
            