
package views.html.metadatald

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object viewDataset extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Dataset,List[models.Metadata],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset, metadata: List[models.Metadata])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.89*/("""

"""),_display_(Seq[Any](/*3.2*/main("Metadata")/*3.18*/ {_display_(Seq[Any](format.raw/*3.20*/("""

    <div class="row">
        <div class="col-md-10">
            <h1>Metadata for dataset <a href=""""),_display_(Seq[Any](/*7.48*/routes/*7.54*/.Datasets.dataset(dataset.id))),format.raw/*7.83*/("""">"""),_display_(Seq[Any](/*7.86*/Html(dataset.name))),format.raw/*7.104*/("""</a></h1>
        </div>
        <div class="col-md-2">
            <a href=""""),_display_(Seq[Any](/*10.23*/api/*10.26*/.routes.Datasets.getMetadataJsonLD(dataset.id))),format.raw/*10.72*/("""" class="pull-right" title="JSON-LD" target="_blank">
                <img src=""""),_display_(Seq[Any](/*11.28*/routes/*11.34*/.Assets.at("images/json-ld.png"))),format.raw/*11.66*/(""""/>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*17.14*/addMetadata("dataset", dataset.id.toString, "metadata-content"))),format.raw/*17.77*/("""
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" id="metadata-content">
            """),_display_(Seq[Any](/*22.14*/view(metadata, true))),format.raw/*22.34*/("""
        </div>
    </div>
""")))})))}
    }
    
    def render(dataset:Dataset,metadata:List[models.Metadata],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,metadata)(user)
    
    def f:((Dataset,List[models.Metadata]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,metadata) => (user) => apply(dataset,metadata)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/metadatald/viewDataset.scala.html
                    HASH: 199dc5f08ede277fa6bb03a97867d86b8d583c3f
                    MATRIX: 647->1|828->88|865->91|889->107|928->109|1066->212|1080->218|1130->247|1168->250|1208->268|1322->346|1334->349|1402->395|1519->476|1534->482|1588->514|1738->628|1823->691|1975->807|2017->827
                    LINES: 20->1|23->1|25->3|25->3|25->3|29->7|29->7|29->7|29->7|29->7|32->10|32->10|32->10|33->11|33->11|33->11|39->17|39->17|44->22|44->22
                    -- GENERATED --
                */
            