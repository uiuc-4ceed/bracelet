
package views.html.metadatald

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object viewFile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[File,List[models.Metadata],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(file: File, metadata: List[models.Metadata])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.83*/("""

"""),_display_(Seq[Any](/*3.2*/main("Metadata")/*3.18*/ {_display_(Seq[Any](format.raw/*3.20*/("""
    <div class="row">
        <div class="col-md-10">
            <h1>Metadata for file <a href=""""),_display_(Seq[Any](/*6.45*/routes/*6.51*/.Files.file(file.id))),format.raw/*6.71*/("""">"""),_display_(Seq[Any](/*6.74*/file/*6.78*/.filename)),format.raw/*6.87*/("""</a></h1>
        </div>
        <div class="col-md-2">
            <a href=""""),_display_(Seq[Any](/*9.23*/api/*9.26*/.routes.Files.getMetadataJsonLD(file.id))),format.raw/*9.66*/("""" class="pull-right" title="JSON-LD" target="_blank">
                <img src=""""),_display_(Seq[Any](/*10.28*/routes/*10.34*/.Assets.at("images/json-ld.png"))),format.raw/*10.66*/(""""/>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*16.14*/addMetadata("file", file.id.toString, "metadata-content"))),format.raw/*16.71*/("""
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" id="metadata-content">
            """),_display_(Seq[Any](/*21.14*/view(metadata, true))),format.raw/*21.34*/("""
        </div>
    </div>
""")))})))}
    }
    
    def render(file:File,metadata:List[models.Metadata],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(file,metadata)(user)
    
    def f:((File,List[models.Metadata]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (file,metadata) => (user) => apply(file,metadata)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/metadatald/viewFile.scala.html
                    HASH: fd271a6c8d52956e3761e17045e82e784b74bc06
                    MATRIX: 641->1|816->82|853->85|877->101|916->103|1050->202|1064->208|1105->228|1143->231|1155->235|1185->244|1298->322|1309->325|1370->365|1487->446|1502->452|1556->484|1706->598|1785->655|1937->771|1979->791
                    LINES: 20->1|23->1|25->3|25->3|25->3|28->6|28->6|28->6|28->6|28->6|28->6|31->9|31->9|31->9|32->10|32->10|32->10|38->16|38->16|43->21|43->21
                    -- GENERATED --
                */
            