
package views.html.metadatald

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object addMetadata extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[String,String,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(resType: String, id: String, contentDiv: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.87*/("""
"""),format.raw/*3.82*/("""
"""),_display_(Seq[Any](/*4.2*/if(resType == "dataset" && Permission.checkPermission(Permission.AddMetadata, ResourceRef(ResourceRef.dataset, UUID(id)))
        || resType == "file" && Permission.checkPermission(Permission.AddMetadata, ResourceRef(ResourceRef.file, UUID(id)))
        || resType.startsWith("curation"))/*6.43*/ {_display_(Seq[Any](format.raw/*6.45*/("""
    <div id=""""),_display_(Seq[Any](/*7.15*/id)),format.raw/*7.17*/("""" class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <h5>Add metadata</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal form-md">
                        <select id="add-metadata-select">
                            <option value=""></option>
                        </select>
                    </form>
                </div>
            </div>

            <div id="selected_field" class="row"></div>
        </div>
    </div>

<link rel="stylesheet" href=""""),_display_(Seq[Any](/*28.31*/routes/*28.37*/.Assets.at("stylesheets/chosen.css"))),format.raw/*28.73*/("""">
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*29.31*/routes/*29.37*/.Assets.at("stylesheets/jquery-ui-timepicker-addon.css"))),format.raw/*29.93*/("""">
<script src=""""),_display_(Seq[Any](/*30.15*/routes/*30.21*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*30.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*31.15*/routes/*31.21*/.Assets.at("javascripts/handlebars-v1.3.0.js"))),format.raw/*31.67*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*32.15*/routes/*32.21*/.Assets.at("javascripts/handlebars-loader.js"))),format.raw/*32.67*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*33.15*/routes/*33.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*33.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*34.15*/routes/*34.21*/.Assets.at("javascripts/jquery-ui-timepicker-addon.js"))),format.raw/*34.76*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*35.15*/routes/*35.21*/.Assets.at("javascripts/terraformer/terraformer-1.0.5.min.js"))),format.raw/*35.83*/(""""></script>
<script src=""""),_display_(Seq[Any](/*36.15*/routes/*36.21*/.Assets.at("javascripts/terraformer/terraformer-wkt-parser-1.1.0.min.js"))),format.raw/*36.94*/(""""></script>

<style>
.ui-autocomplete-category """),format.raw/*39.27*/("""{"""),format.raw/*39.28*/("""
font-weight: bold;
padding: .2em .4em;
margin: .8em 0 .2em;
line-height: 1.5;
"""),format.raw/*44.1*/("""}"""),format.raw/*44.2*/("""
</style>

<script>
    // Assumption: the input list contains: <cat_short_name>;<cat_display_name> or <cat_short_name>:<var_name>, where <cat_display_name> contains no ":", and <var_name> contains no ";".
    $.widget( "custom.catcomplete", $.ui.autocomplete, """),format.raw/*49.56*/("""{"""),format.raw/*49.57*/("""
        _create: function() """),format.raw/*50.29*/("""{"""),format.raw/*50.30*/("""
            this._super();
            this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
        """),format.raw/*53.9*/("""}"""),format.raw/*53.10*/(""",
        _renderMenu: function( ul, items ) """),format.raw/*54.44*/("""{"""),format.raw/*54.45*/("""
            var that = this;
            var currentCategory = "";
            var category = "";
            var dispCatMap = new Map();
            $.each( items, function( index, item ) """),format.raw/*59.52*/("""{"""),format.raw/*59.53*/("""
                //console.log("in renderMenu: item: ", item);
                var label = item.label;
                var char_index = label.indexOf(";");
                if (char_index > -1) """),format.raw/*63.38*/("""{"""),format.raw/*63.39*/("""
                    var shortName = label.slice(0, char_index);
                    var displName = label.slice(char_index + 1);
                    //console.log("Setting category name '" + shortName + "' to '" + displName + "'");
                    dispCatMap.set(shortName, displName);
                """),format.raw/*68.17*/("""}"""),format.raw/*68.18*/("""
            """),format.raw/*69.13*/("""}"""),format.raw/*69.14*/(""");
            $.each( items, function( index, item ) """),format.raw/*70.52*/("""{"""),format.raw/*70.53*/("""
                var li;
                // Each item from an original array of strings was converted to an object """),format.raw/*72.91*/("""{"""),format.raw/*72.92*/(""" label: "val1", value: "val1" """),format.raw/*72.122*/("""}"""),format.raw/*72.123*/(""".
                //console.log("item: ", item);
                var label = item.label;
                var char_index = label.indexOf(":");
                if (char_index > -1) """),format.raw/*76.38*/("""{"""),format.raw/*76.39*/("""
                    category = label.slice(0, char_index);
                    item.label = label.slice(char_index + 1);
                """),format.raw/*79.17*/("""}"""),format.raw/*79.18*/("""
                if ( category != currentCategory ) """),format.raw/*80.52*/("""{"""),format.raw/*80.53*/("""
                    //console.log("category changed from " + currentCategory + " to: " + category);
                    var displayed_cat_name = category;
                    if (dispCatMap.has(category)) """),format.raw/*83.51*/("""{"""),format.raw/*83.52*/("""
                        displayed_cat_name = dispCatMap.get(category);
                    """),format.raw/*85.21*/("""}"""),format.raw/*85.22*/("""
                    ul.append( "<li class='ui-autocomplete-category'>" + displayed_cat_name + "</li>" );
                    currentCategory = category;
                """),format.raw/*88.17*/("""}"""),format.raw/*88.18*/("""
                // Used to be: Display only the items containing ":".
                // Don't display the items containing ";".
                if (label.indexOf(";") <= 0) """),format.raw/*91.46*/("""{"""),format.raw/*91.47*/("""
                    li = that._renderItemData( ul, item );
                """),format.raw/*93.17*/("""}"""),format.raw/*93.18*/("""
            """),format.raw/*94.13*/("""}"""),format.raw/*94.14*/(""");
        """),format.raw/*95.9*/("""}"""),format.raw/*95.10*/("""
    """),format.raw/*96.5*/("""}"""),format.raw/*96.6*/(""");
</script>

<script language="javascript">
    // submit metadata entry to server
    function submit_"""),_display_(Seq[Any](/*101.22*/id)),format.raw/*101.24*/("""
    (event)"""),format.raw/*102.12*/("""{"""),format.raw/*102.13*/("""
        event.preventDefault();
        var field_label = $("#"""),_display_(Seq[Any](/*104.32*/id)),format.raw/*104.34*/("""  #field-value").data("label");
        var field_id = $("#"""),_display_(Seq[Any](/*105.29*/id)),format.raw/*105.31*/("""  #field-value").data("id");
        var field_type = $("#"""),_display_(Seq[Any](/*106.31*/id)),format.raw/*106.33*/("""  #add-metadata-select option:selected").data("type");
        var field_value = "";
        var field_types_with_simple_values = """),format.raw/*108.46*/("""{"""),format.raw/*108.47*/(""""string": 1, "listjquery": 1, "listgeocode": 1, "scientific_variable": 1,
            "datetime": 1, "wkt": 1"""),format.raw/*109.36*/("""}"""),format.raw/*109.37*/(""";
        if (field_type in field_types_with_simple_values) """),format.raw/*110.59*/("""{"""),format.raw/*110.60*/("""
            field_value = $("#"""),_display_(Seq[Any](/*111.32*/id)),format.raw/*111.34*/("""  #field-value").val();
        """),format.raw/*112.9*/("""}"""),format.raw/*112.10*/(""" else if (field_type === "list") """),format.raw/*112.43*/("""{"""),format.raw/*112.44*/("""
            field_value = $("#"""),_display_(Seq[Any](/*113.32*/id)),format.raw/*113.34*/("""  #field-value option:selected").val();
        """),format.raw/*114.9*/("""}"""),format.raw/*114.10*/("""  else """),format.raw/*114.17*/("""{"""),format.raw/*114.18*/("""
            console.log("Wrong field type: " + field_type);
        """),format.raw/*116.9*/("""}"""),format.raw/*116.10*/("""

        var error = false;
        if (field_value != "") """),format.raw/*119.32*/("""{"""),format.raw/*119.33*/("""
            // define contexts
            var contexts = [];
            contexts.push("https://clowder.ncsa.illinois.edu/contexts/metadata.jsonld");
            var context = """),format.raw/*123.27*/("""{"""),format.raw/*123.28*/("""}"""),format.raw/*123.29*/(""";
            context[field_label] = field_id;
            contexts.push(context);

            var content = """),format.raw/*127.27*/("""{"""),format.raw/*127.28*/("""}"""),format.raw/*127.29*/(""";
            if (field_type === "listgeocode") """),format.raw/*128.47*/("""{"""),format.raw/*128.48*/("""
                // geocode example: "Champaign, IL, USA: 40.12, -88.24"
                var parts = field_value.split(":");
                content[field_label] = parts[0].trim();
                var lat_lng = parts[1].trim().split(",");
                content["Latitude"] = lat_lng[0].trim();
                content["Longitude"] = lat_lng[1].trim();
            """),format.raw/*135.13*/("""}"""),format.raw/*135.14*/(""" else if (field_type === "scientific_variable") """),format.raw/*135.62*/("""{"""),format.raw/*135.63*/("""
                content[field_label] = field_value;
                var unit_val = $("#"""),_display_(Seq[Any](/*137.37*/id)),format.raw/*137.39*/(""" #unit-value").val();
                if (unit_val) """),format.raw/*138.31*/("""{"""),format.raw/*138.32*/("""
                    content["Unit"] = unit_val;
                """),format.raw/*140.17*/("""}"""),format.raw/*140.18*/("""
            """),format.raw/*141.13*/("""}"""),format.raw/*141.14*/(""" else if (field_type === "wkt") """),format.raw/*141.46*/("""{"""),format.raw/*141.47*/("""
                try """),format.raw/*142.21*/("""{"""),format.raw/*142.22*/("""
                    var primitive = Terraformer.WKT.parse(field_value);
                    content["GeoJSON"] = primitive;
                """),format.raw/*145.17*/("""}"""),format.raw/*145.18*/(""" catch(err) """),format.raw/*145.30*/("""{"""),format.raw/*145.31*/("""
                    notify("There is an error in your WKT. " + err, "error", false, 2000);
                    error = true;
                """),format.raw/*148.17*/("""}"""),format.raw/*148.18*/("""
            """),format.raw/*149.13*/("""}"""),format.raw/*149.14*/(""" else """),format.raw/*149.20*/("""{"""),format.raw/*149.21*/("""
                content[field_label] = field_value;
            """),format.raw/*151.13*/("""}"""),format.raw/*151.14*/("""

            var body = """),format.raw/*153.24*/("""{"""),format.raw/*153.25*/("""
                    "@context": contexts,
                    """"),_display_(Seq[Any](/*155.23*/(resType))),format.raw/*155.32*/("""_id": """"),_display_(Seq[Any](/*155.40*/id)),format.raw/*155.42*/("""",
                    "content": content
            """),format.raw/*157.13*/("""}"""),format.raw/*157.14*/(""";

            if (!error) """),format.raw/*159.25*/("""{"""),format.raw/*159.26*/("""
                var request = jsRoutes.api.Metadata.addUserMetadata().ajax("""),format.raw/*160.76*/("""{"""),format.raw/*160.77*/("""
                    data: JSON.stringify(body),
                    type: 'POST',
                    contentType: "application/json"
                """),format.raw/*164.17*/("""}"""),format.raw/*164.18*/(""");

                request.done(function (response, textStatus, jqXHR) """),format.raw/*166.69*/("""{"""),format.raw/*166.70*/("""
                    // reset submission value
                    $("#"""),_display_(Seq[Any](/*168.26*/id)),format.raw/*168.28*/("""  #selected_field").empty();
                    $("#"""),_display_(Seq[Any](/*169.26*/id)),format.raw/*169.28*/(""" #add-metadata-select").val('').trigger("chosen:updated");
                    if($('#'+'"""),_display_(Seq[Any](/*170.32*/contentDiv)),format.raw/*170.42*/("""'+' .panel').length == 0) """),format.raw/*170.68*/("""{"""),format.raw/*170.69*/("""
                        $('#' + """"),_display_(Seq[Any](/*171.35*/contentDiv)),format.raw/*171.45*/("""").empty();
                    """),format.raw/*172.21*/("""}"""),format.raw/*172.22*/("""
                    $('#'+""""),_display_(Seq[Any](/*173.29*/contentDiv)),format.raw/*173.39*/("""").prepend(response);
                    notify("Metadata successfully added.", "success", false, 2000);
                """),format.raw/*175.17*/("""}"""),format.raw/*175.18*/(""");

                request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*177.71*/("""{"""),format.raw/*177.72*/("""
                    console.error("The following error occured: " + textStatus, errorThrown);
                    var errMsg = "You must be logged in to add metadata";
                    if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*180.64*/("""{"""),format.raw/*180.65*/("""
                      notify("Metadata was not added due to : " + errorThrown, "error");
                    """),format.raw/*182.21*/("""}"""),format.raw/*182.22*/("""
                """),format.raw/*183.17*/("""}"""),format.raw/*183.18*/(""");
            """),format.raw/*184.13*/("""}"""),format.raw/*184.14*/("""

        """),format.raw/*186.9*/("""}"""),format.raw/*186.10*/("""
    """),format.raw/*187.5*/("""}"""),format.raw/*187.6*/("""
    var url = jsRoutes.api.Metadata.getDefinitions();
    var showSpaceInfo = false;
    if(""""),_display_(Seq[Any](/*190.10*/resType)),format.raw/*190.17*/("""" == "dataset") """),format.raw/*190.33*/("""{"""),format.raw/*190.34*/("""
        if(spaceId == "") """),format.raw/*191.27*/("""{"""),format.raw/*191.28*/("""
            url = jsRoutes.api.Datasets.getMetadataDefinitions(""""),_display_(Seq[Any](/*192.66*/id)),format.raw/*192.68*/("""");
            showSpaceInfo = true;
        """),format.raw/*194.9*/("""}"""),format.raw/*194.10*/(""" else """),format.raw/*194.16*/("""{"""),format.raw/*194.17*/("""
            url = jsRoutes.api.Datasets.getMetadataDefinitions(""""),_display_(Seq[Any](/*195.66*/id)),format.raw/*195.68*/("""", spaceId);
        """),format.raw/*196.9*/("""}"""),format.raw/*196.10*/("""
    """),format.raw/*197.5*/("""}"""),format.raw/*197.6*/(""" else if (""""),_display_(Seq[Any](/*197.18*/resType)),format.raw/*197.25*/("""" == "file") """),format.raw/*197.38*/("""{"""),format.raw/*197.39*/("""
        if(spaceId == "") """),format.raw/*198.27*/("""{"""),format.raw/*198.28*/("""
            showSpaceInfo = true;
            url = jsRoutes.api.Files.getMetadataDefinitions(""""),_display_(Seq[Any](/*200.63*/id)),format.raw/*200.65*/("""");
        """),format.raw/*201.9*/("""}"""),format.raw/*201.10*/(""" else """),format.raw/*201.16*/("""{"""),format.raw/*201.17*/("""
            url = jsRoutes.api.Files.getMetadataDefinitions(""""),_display_(Seq[Any](/*202.63*/id)),format.raw/*202.65*/("""", spaceId);
        """),format.raw/*203.9*/("""}"""),format.raw/*203.10*/("""

    """),format.raw/*205.5*/("""}"""),format.raw/*205.6*/(""" else if (""""),_display_(Seq[Any](/*205.18*/resType)),format.raw/*205.25*/("""" == "curationObject") """),format.raw/*205.48*/("""{"""),format.raw/*205.49*/("""
        url = jsRoutes.api.CurationObjects.getMetadataDefinitions(""""),_display_(Seq[Any](/*206.69*/id)),format.raw/*206.71*/("""");
    """),format.raw/*207.5*/("""}"""),format.raw/*207.6*/(""" else if (""""),_display_(Seq[Any](/*207.18*/resType)),format.raw/*207.25*/("""" == "curationFile") """),format.raw/*207.46*/("""{"""),format.raw/*207.47*/("""
        url = jsRoutes.api.CurationObjects.getMetadataDefinitionsByFile(""""),_display_(Seq[Any](/*208.75*/id)),format.raw/*208.77*/("""");
    """),format.raw/*209.5*/("""}"""),format.raw/*209.6*/("""
    // get metadata definitions
    var request = url.ajax("""),format.raw/*211.28*/("""{"""),format.raw/*211.29*/("""
        type: 'GET',
        contentType: "application/json"
    """),format.raw/*214.5*/("""}"""),format.raw/*214.6*/(""");

    request.done(function (response, textStatus, jqXHR) """),format.raw/*216.57*/("""{"""),format.raw/*216.58*/("""
        var fields = response;
        for (var i = 0; i < fields.length; i++) """),format.raw/*218.49*/("""{"""),format.raw/*218.50*/("""
            var elem = $("<option></option>");
            elem.attr("data-description", fields[i].json.description);
            elem.attr("data-type", fields[i].json.type);
            elem.attr("data-id", fields[i].id);
            elem.attr("value", fields[i].json.uri);
            elem.attr("space_id", fields[i].spaceId);
            elem.text(fields[i].json.label);
            $("#"""),_display_(Seq[Any](/*226.18*/id)),format.raw/*226.20*/("""  #add-metadata-select").append(elem);
        """),format.raw/*227.9*/("""}"""),format.raw/*227.10*/("""
        // chosen pulldown configuration
        $("#"""),_display_(Seq[Any](/*229.14*/id)),format.raw/*229.16*/("""  #add-metadata-select").chosen("""),format.raw/*229.48*/("""{"""),format.raw/*229.49*/("""
            search_contains: true,
            width: "100%",
            placeholder_text_single: "Select field""""),format.raw/*232.52*/("""}"""),format.raw/*232.53*/(""");
        // register selection listener
        $("#"""),_display_(Seq[Any](/*234.14*/id)),format.raw/*234.16*/("""  #add-metadata-select").change(function () """),format.raw/*234.60*/("""{"""),format.raw/*234.61*/("""
            $("#"""),_display_(Seq[Any](/*235.18*/id)),format.raw/*235.20*/("""  #add-metadata-select option:selected").each(function() """),format.raw/*235.77*/("""{"""),format.raw/*235.78*/("""
                // create html form
                var field_label = $(this).text();
                var field_description = $(this).data("description");
                var field_id = $(this).val();
                var field_type = $(this).data("type");
                var space_id = $(this).attr("space_id");
                // Create the template and HTML content.
                var template_map =
                    """),format.raw/*244.21*/("""{"""),format.raw/*244.22*/(""""string": "add_metadata_string",
                     "list": "add_metadata_list",
                     "listjquery": "add_metadata_listjquery",
                     "listgeocode": "add_metadata_string",
                     "scientific_variable": "add_metadata_scientific_variable",
                     "datetime": "add_metadata_datetime",
                     "wkt": "add_metadata_wktlocation"
                    """),format.raw/*251.21*/("""}"""),format.raw/*251.22*/(""";
                var modalTemplate = Handlebars.getTemplate(""""),_display_(Seq[Any](/*252.62*/routes/*252.68*/.Assets.at("templates/metadata/"))),format.raw/*252.101*/("""" + template_map[field_type]);
                var html = modalTemplate("""),format.raw/*253.42*/("""{"""),format.raw/*253.43*/("""'field_label': field_label, 'field_description': field_description,
                    'field_id': field_id, 'field_type': field_type"""),format.raw/*254.67*/("""}"""),format.raw/*254.68*/(""");
                var footerHtml = "";
                if(typeof space_id !== 'undefined' && showSpaceInfo) """),format.raw/*256.70*/("""{"""),format.raw/*256.71*/("""
                    var request2 = jsRoutes.api.Spaces.get(space_id).ajax("""),format.raw/*257.75*/("""{"""),format.raw/*257.76*/("""
                        type: 'GET',
                        contentType: "application/json"
                    """),format.raw/*260.21*/("""}"""),format.raw/*260.22*/(""");
                    request2.done(function (response, textStatus, jqXHR) """),format.raw/*261.74*/("""{"""),format.raw/*261.75*/("""
                        var footerTemplate = Handlebars.getTemplate(""""),_display_(Seq[Any](/*262.71*/routes/*262.77*/.Assets.at("templates/metadata/footer"))),format.raw/*262.116*/("""");
                        var spaceName= response.name;
                        footerHtml = footerTemplate("""),format.raw/*264.53*/("""{"""),format.raw/*264.54*/("""'space_name': spaceName, 'space_url': jsRoutes.controllers.Spaces.getSpace(space_id).url, 'uri':field_id """),format.raw/*264.159*/("""}"""),format.raw/*264.160*/(""");
                        $('#"""),_display_(Seq[Any](/*265.30*/id)),format.raw/*265.32*/("""  #selected_field').html(html + footerHtml);
                        // register submit listener
                        $("#"""),_display_(Seq[Any](/*267.30*/id)),format.raw/*267.32*/("""  #add-metadata-button").click(submit_"""),_display_(Seq[Any](/*267.71*/id)),format.raw/*267.73*/(""");
                    """),format.raw/*268.21*/("""}"""),format.raw/*268.22*/(""");
                    request2.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*269.76*/("""{"""),format.raw/*269.77*/("""
                        $('#"""),_display_(Seq[Any](/*270.30*/id)),format.raw/*270.32*/("""  #selected_field').html(html);
                        // register submit listener
                        $("#"""),_display_(Seq[Any](/*272.30*/id)),format.raw/*272.32*/("""  #add-metadata-button").click(submit_"""),_display_(Seq[Any](/*272.71*/id)),format.raw/*272.73*/(""");
                    """),format.raw/*273.21*/("""}"""),format.raw/*273.22*/(""");
                """),format.raw/*274.17*/("""}"""),format.raw/*274.18*/(""" else """),format.raw/*274.24*/("""{"""),format.raw/*274.25*/("""
                    $('#"""),_display_(Seq[Any](/*275.26*/id)),format.raw/*275.28*/("""  #selected_field').html(html);
                    // register submit listener
                    $("#"""),_display_(Seq[Any](/*277.26*/id)),format.raw/*277.28*/("""  #add-metadata-button").click(submit_"""),_display_(Seq[Any](/*277.67*/id)),format.raw/*277.69*/(""");
                """),format.raw/*278.17*/("""}"""),format.raw/*278.18*/("""


                if (field_type === "scientific_variable") """),format.raw/*281.59*/("""{"""),format.raw/*281.60*/("""
                    // Find the field with the specific uri.
                    var field = $.grep(fields, function(e)"""),format.raw/*283.59*/("""{"""),format.raw/*283.60*/(""" return e.json.uri == field_id; """),format.raw/*283.92*/("""}"""),format.raw/*283.93*/(""");
                    $("#"""),_display_(Seq[Any](/*284.26*/id)),format.raw/*284.28*/("""  #field-value").catcomplete("""),format.raw/*284.57*/("""{"""),format.raw/*284.58*/("""
                        minLength: 3,
                        source: function( request, response ) """),format.raw/*286.63*/("""{"""),format.raw/*286.64*/("""

                            var useSyn = $("#"""),_display_(Seq[Any](/*288.47*/id)),format.raw/*288.49*/("""  #useSynonyms").prop("checked");
                            var query_param = field[0].json.query_parameter;
                            var url = encodeURIComponent(field[0].json.definitions_url + "?" + query_param + "=" + request.term + "&useSynonyms=" + useSyn);
                            $.ajax("""),format.raw/*291.36*/("""{"""),format.raw/*291.37*/("""
                                url: jsRoutes.api.Metadata.getUrl(url).url,
                                //dataType: "jsonp",
                                dataType: "json",
                                //data: """),format.raw/*295.41*/("""{"""),format.raw/*295.42*/(""" term: request.term, useSynonyms: useSyn """),format.raw/*295.83*/("""}"""),format.raw/*295.84*/(""",
                                success: function( data ) """),format.raw/*296.59*/("""{"""),format.raw/*296.60*/("""
                                    // The vars list is in data.vars_data, and the categories in data.cat_data. Assuming that "listjquery" will use a URL that returns filtered data, we don't filter again. Returns cat_data with the vars listif present, otherwise returns the original data.
                                    if ('cat_data' in data) """),format.raw/*298.61*/("""{"""),format.raw/*298.62*/("""
                                        var res = data.cat_data.concat(data.vars_data);
                                        response(res);
                                    """),format.raw/*301.37*/("""}"""),format.raw/*301.38*/(""" else """),format.raw/*301.44*/("""{"""),format.raw/*301.45*/("""
                                        response(data);
                                    """),format.raw/*303.37*/("""}"""),format.raw/*303.38*/("""
                                """),format.raw/*304.33*/("""}"""),format.raw/*304.34*/("""
                            """),format.raw/*305.29*/("""}"""),format.raw/*305.30*/(""");
                        """),format.raw/*306.25*/("""}"""),format.raw/*306.26*/("""
                    """),format.raw/*307.21*/("""}"""),format.raw/*307.22*/(""");
                    $("#"""),_display_(Seq[Any](/*308.26*/id)),format.raw/*308.28*/(""" #unit-value").autocomplete("""),format.raw/*308.56*/("""{"""),format.raw/*308.57*/("""
                        minLength: 1,
                        source: function( request, response ) """),format.raw/*310.63*/("""{"""),format.raw/*310.64*/("""
                            var url = encodeURIComponent(field[0].json.definitions_url);
                            $.ajax("""),format.raw/*312.36*/("""{"""),format.raw/*312.37*/("""
                                url: jsRoutes.api.Metadata.getUrl(url).url,
                                dataType: "json",
                                success: function( data ) """),format.raw/*315.59*/("""{"""),format.raw/*315.60*/("""
                                    if (! ('unit_data' in data)) """),format.raw/*316.66*/("""{"""),format.raw/*316.67*/("""
                                        response(Array("Error: no unit_data field in the returned result."));
                                    """),format.raw/*318.37*/("""}"""),format.raw/*318.38*/(""" else """),format.raw/*318.44*/("""{"""),format.raw/*318.45*/("""
                                        var searchspace = data.unit_data;
                                        var searchwords = request.term.split(" ");
                                        $.each(searchwords, function() """),format.raw/*321.72*/("""{"""),format.raw/*321.73*/("""
                                            searchspace = $.ui.autocomplete.filter(searchspace, this);
                                        """),format.raw/*323.41*/("""}"""),format.raw/*323.42*/(""");
                                        response(searchspace);
                                    """),format.raw/*325.37*/("""}"""),format.raw/*325.38*/("""
                                """),format.raw/*326.33*/("""}"""),format.raw/*326.34*/("""
                            """),format.raw/*327.29*/("""}"""),format.raw/*327.30*/(""");
                        """),format.raw/*328.25*/("""}"""),format.raw/*328.26*/("""
                    """),format.raw/*329.21*/("""}"""),format.raw/*329.22*/(""");
                """),format.raw/*330.17*/("""}"""),format.raw/*330.18*/(""" else if (field_type === "datetime") """),format.raw/*330.55*/("""{"""),format.raw/*330.56*/("""
                    // This widget uses the ISO 8601 format, such as 2016-01-01T10:00:00-06:00 or 2016-01-01T10:00:00Z.
                    // This uses Trent Richardson's jQuery UI Timepicker add-on.
                    // jQuery UI Datepicker options: http://api.jqueryui.com/datepicker/
                    // jQuery UI Timepicker addon options: http://trentrichardson.com/examples/timepicker/
                    $("#field-value").datetimepicker(
                        """),format.raw/*336.25*/("""{"""),format.raw/*336.26*/(""" controlType: 'select',
                          // Uses "select" instead of the default slider.
                          // If we put "T" in the "separator" option instead of "timeFormat", the widget changes 2-digit year values xx we put directly into the field to 20xx; if we put "T" in timeFormat, then the year values are kept. So we used the latter.
                          dateFormat: $.datepicker.ISO_8601,
                          timeFormat: "'T'HH:mm:ssZ",
                          separator: '',
                          // Allows direct input.
                          timeInput: true
                        """),format.raw/*344.25*/("""}"""),format.raw/*344.26*/(""");
                """),format.raw/*345.17*/("""}"""),format.raw/*345.18*/(""" else if (field_type === "listjquery") """),format.raw/*345.57*/("""{"""),format.raw/*345.58*/("""
                    // Find the field with the specific uri.
                    var field = $.grep(fields, function(e)"""),format.raw/*347.59*/("""{"""),format.raw/*347.60*/(""" return e.json.uri == field_id; """),format.raw/*347.92*/("""}"""),format.raw/*347.93*/(""");
                    $("#"""),_display_(Seq[Any](/*348.26*/id)),format.raw/*348.28*/(""" #field-value").autocomplete("""),format.raw/*348.57*/("""{"""),format.raw/*348.58*/("""
                        minLength: 3,
                        source: function( request, response ) """),format.raw/*350.63*/("""{"""),format.raw/*350.64*/("""
                            // Get the query parameter from the saved json, not hardcoded.
                            var query_param = field[0].json.query_parameter;
                            var url = encodeURIComponent(field[0].json.definitions_url + "?" + query_param + "=" + request.term);
                            $.ajax("""),format.raw/*354.36*/("""{"""),format.raw/*354.37*/("""
                                url: jsRoutes.api.Metadata.getUrl(url).url,
                                dataType: "json",
                                success: function( data ) """),format.raw/*357.59*/("""{"""),format.raw/*357.60*/("""
                                    var searchspace = data;
                                    var searchwords = request.term.split(" ");
                                    $.each(searchwords, function() """),format.raw/*360.68*/("""{"""),format.raw/*360.69*/("""
                                        searchspace = $.ui.autocomplete.filter(searchspace, this);
                                    """),format.raw/*362.37*/("""}"""),format.raw/*362.38*/(""");
                                    response(searchspace);
                                """),format.raw/*364.33*/("""}"""),format.raw/*364.34*/("""
                            """),format.raw/*365.29*/("""}"""),format.raw/*365.30*/(""");
                        """),format.raw/*366.25*/("""}"""),format.raw/*366.26*/("""
                    """),format.raw/*367.21*/("""}"""),format.raw/*367.22*/(""");
                """),format.raw/*368.17*/("""}"""),format.raw/*368.18*/(""" else if (field_type === "listgeocode") """),format.raw/*368.58*/("""{"""),format.raw/*368.59*/("""
                    // Find the field with the specific uri.
                    var field = $.grep(fields, function(e)"""),format.raw/*370.59*/("""{"""),format.raw/*370.60*/(""" return e.json.uri == field_id; """),format.raw/*370.92*/("""}"""),format.raw/*370.93*/(""");
                    $("#"""),_display_(Seq[Any](/*371.26*/id)),format.raw/*371.28*/("""  #field-value").autocomplete("""),format.raw/*371.58*/("""{"""),format.raw/*371.59*/("""
                        minLength: 3,
                        source: function( request, response ) """),format.raw/*373.63*/("""{"""),format.raw/*373.64*/("""
                            // Sets a variable query parameter in $.ajax.data below.
                            var query_param = field[0].json.query_parameter;
                            var url = encodeURIComponent(field[0].json.definitions_url + "?" + query_param + "=" + request.term);
                            $.ajax("""),format.raw/*377.36*/("""{"""),format.raw/*377.37*/("""
                                url: jsRoutes.api.Metadata.getUrl(url).url,
                                //dataType: "jsonp",
                                dataType: "json",
                                //data: query_data,
                                success: function( data ) """),format.raw/*382.59*/("""{"""),format.raw/*382.60*/("""
                                    // Assuming that the remote service returns filtered data, no need to filter again.
                                    response(data);
                                """),format.raw/*385.33*/("""}"""),format.raw/*385.34*/("""
                            """),format.raw/*386.29*/("""}"""),format.raw/*386.30*/(""");
                        """),format.raw/*387.25*/("""}"""),format.raw/*387.26*/("""
                    """),format.raw/*388.21*/("""}"""),format.raw/*388.22*/(""");
                """),format.raw/*389.17*/("""}"""),format.raw/*389.18*/(""" else if (field_type === "list") """),format.raw/*389.51*/("""{"""),format.raw/*389.52*/("""
                    // find field with the specific uri
                    var field = $.grep(fields, function(e)"""),format.raw/*391.59*/("""{"""),format.raw/*391.60*/(""" return e.json.uri == field_id; """),format.raw/*391.92*/("""}"""),format.raw/*391.93*/(""");

                    // make call to external service
                    var request = jsRoutes.api.Metadata.getDefinition(field[0].id).ajax("""),format.raw/*394.89*/("""{"""),format.raw/*394.90*/("""
                        type: 'GET',
                        contentType: "application/json"
                    """),format.raw/*397.21*/("""}"""),format.raw/*397.22*/(""");

                    request.done(function (response, textStatus, jqXHR) """),format.raw/*399.73*/("""{"""),format.raw/*399.74*/("""
                        var vocabulary = JSON.parse(response);
                        // modalTemplate was assigned above using the map.
                        var html = modalTemplate("""),format.raw/*402.50*/("""{"""),format.raw/*402.51*/("""'field_label': field_label, 'field_description': field_description,
                            'field_id': field_id, 'field_type': field_type, 'options': vocabulary"""),format.raw/*403.98*/("""}"""),format.raw/*403.99*/(""");
                        $("#"""),_display_(Seq[Any](/*404.30*/id)),format.raw/*404.32*/("""  #selected_field").html(html + footerHtml);
                        // chosen pulldown configuration
                        $("#"""),_display_(Seq[Any](/*406.30*/id)),format.raw/*406.32*/("""  #field-value").chosen("""),format.raw/*406.56*/("""{"""),format.raw/*406.57*/("""
                            no_results_text: "Not found. Press enter to add ",
                            add_search_option: true,
                            search_contains: true,
                            width: "100%",
                            placeholder_text_single: "Select field""""),format.raw/*411.68*/("""}"""),format.raw/*411.69*/(""");
                        // register submit listener
                        $("#"""),_display_(Seq[Any](/*413.30*/id)),format.raw/*413.32*/("""  #add-metadata-button").click(submit_"""),_display_(Seq[Any](/*413.71*/id)),format.raw/*413.73*/(""");
                      """),format.raw/*414.23*/("""}"""),format.raw/*414.24*/(""");

                    request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*416.75*/("""{"""),format.raw/*416.76*/("""
                        console.error("The following error occured: " + textStatus, errorThrown);
                        notify("Could not retrieve external vocabulary: " + errorThrown, "error");
                    """),format.raw/*419.21*/("""}"""),format.raw/*419.22*/(""");
                """),format.raw/*420.17*/("""}"""),format.raw/*420.18*/("""
            """),format.raw/*421.13*/("""}"""),format.raw/*421.14*/(""");
        """),format.raw/*422.9*/("""}"""),format.raw/*422.10*/(""");

        request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*424.63*/("""{"""),format.raw/*424.64*/("""
            console.error("The following error occured: " + textStatus, errorThrown);
            var errMsg = "You must be logged in to retrieve metadata definitions";
            if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*427.56*/("""{"""),format.raw/*427.57*/("""
              notify("Metadata was not added due to : " + errorThrown, "error");
            """),format.raw/*429.13*/("""}"""),format.raw/*429.14*/("""
        """),format.raw/*430.9*/("""}"""),format.raw/*430.10*/(""");
    """),format.raw/*431.5*/("""}"""),format.raw/*431.6*/(""");
</script>
""")))})),format.raw/*433.2*/("""
"""))}
    }
    
    def render(resType:String,id:String,contentDiv:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(resType,id,contentDiv)(user)
    
    def f:((String,String,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (resType,id,contentDiv) => (user) => apply(resType,id,contentDiv)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/metadatald/addMetadata.scala.html
                    HASH: 46d02a5bcd6d442052fe1a44ac91909c27f80264
                    MATRIX: 638->1|839->86|867->191|903->193|1199->481|1238->483|1288->498|1311->500|2025->1178|2040->1184|2098->1220|2167->1253|2182->1259|2260->1315|2313->1332|2328->1338|2392->1380|2477->1429|2492->1435|2560->1481|2645->1530|2660->1536|2728->1582|2813->1631|2828->1637|2892->1679|2977->1728|2992->1734|3069->1789|3154->1838|3169->1844|3253->1906|3315->1932|3330->1938|3425->2011|3500->2058|3529->2059|3635->2138|3663->2139|3952->2400|3981->2401|4038->2430|4067->2431|4220->2557|4249->2558|4322->2603|4351->2604|4569->2794|4598->2795|4819->2988|4848->2989|5183->3296|5212->3297|5253->3310|5282->3311|5364->3365|5393->3366|5536->3481|5565->3482|5624->3512|5654->3513|5861->3692|5890->3693|6056->3831|6085->3832|6165->3884|6194->3885|6428->4091|6457->4092|6577->4184|6606->4185|6804->4355|6833->4356|7036->4531|7065->4532|7169->4608|7198->4609|7239->4622|7268->4623|7306->4634|7335->4635|7367->4640|7395->4641|7537->4746|7562->4748|7603->4760|7633->4761|7734->4825|7759->4827|7856->4887|7881->4889|7977->4948|8002->4950|8161->5080|8191->5081|8329->5190|8359->5191|8448->5251|8478->5252|8547->5284|8572->5286|8632->5318|8662->5319|8724->5352|8754->5353|8823->5385|8848->5387|8924->5435|8954->5436|8990->5443|9020->5444|9117->5513|9147->5514|9236->5574|9266->5575|9473->5753|9503->5754|9533->5755|9672->5865|9702->5866|9732->5867|9809->5915|9839->5916|10234->6282|10264->6283|10341->6331|10371->6332|10497->6421|10522->6423|10603->6475|10633->6476|10727->6541|10757->6542|10799->6555|10829->6556|10890->6588|10920->6589|10970->6610|11000->6611|11170->6752|11200->6753|11241->6765|11271->6766|11442->6908|11472->6909|11514->6922|11544->6923|11579->6929|11609->6930|11703->6995|11733->6996|11787->7021|11817->7022|11919->7088|11951->7097|11996->7105|12021->7107|12104->7161|12134->7162|12190->7189|12220->7190|12325->7266|12355->7267|12535->7418|12565->7419|12666->7491|12696->7492|12805->7564|12830->7566|12921->7620|12946->7622|13073->7712|13106->7722|13161->7748|13191->7749|13263->7784|13296->7794|13357->7826|13387->7827|13453->7856|13486->7866|13637->7988|13667->7989|13770->8063|13800->8064|14061->8296|14091->8297|14230->8407|14260->8408|14306->8425|14336->8426|14380->8441|14410->8442|14448->8452|14478->8453|14511->8458|14540->8459|14672->8554|14702->8561|14747->8577|14777->8578|14833->8605|14863->8606|14966->8672|14991->8674|15065->8720|15095->8721|15130->8727|15160->8728|15263->8794|15288->8796|15337->8817|15367->8818|15400->8823|15429->8824|15478->8836|15508->8843|15550->8856|15580->8857|15636->8884|15666->8885|15800->8982|15825->8984|15865->8996|15895->8997|15930->9003|15960->9004|16060->9067|16085->9069|16134->9090|16164->9091|16198->9097|16227->9098|16276->9110|16306->9117|16358->9140|16388->9141|16494->9210|16519->9212|16555->9220|16584->9221|16633->9233|16663->9240|16713->9261|16743->9262|16855->9337|16880->9339|16916->9347|16945->9348|17034->9408|17064->9409|17158->9475|17187->9476|17276->9536|17306->9537|17415->9617|17445->9618|17874->10010|17899->10012|17974->10059|18004->10060|18096->10115|18121->10117|18182->10149|18212->10150|18355->10264|18385->10265|18477->10320|18502->10322|18575->10366|18605->10367|18660->10385|18685->10387|18771->10444|18801->10445|19256->10871|19286->10872|19732->11289|19762->11290|19862->11353|19878->11359|19935->11392|20036->11464|20066->11465|20229->11599|20259->11600|20397->11709|20427->11710|20531->11785|20561->11786|20704->11900|20734->11901|20839->11977|20869->11978|20977->12049|20993->12055|21056->12094|21195->12204|21225->12205|21360->12310|21391->12311|21460->12343|21485->12345|21648->12471|21673->12473|21749->12512|21774->12514|21826->12537|21856->12538|21963->12616|21993->12617|22060->12647|22085->12649|22235->12762|22260->12764|22336->12803|22361->12805|22413->12828|22443->12829|22491->12848|22521->12849|22556->12855|22586->12856|22649->12882|22674->12884|22816->12989|22841->12991|22917->13030|22942->13032|22990->13051|23020->13052|23110->13113|23140->13114|23289->13234|23319->13235|23380->13267|23410->13268|23475->13296|23500->13298|23558->13327|23588->13328|23718->13429|23748->13430|23833->13478|23858->13480|24190->13783|24220->13784|24469->14004|24499->14005|24569->14046|24599->14047|24688->14107|24718->14108|25097->14458|25127->14459|25336->14639|25366->14640|25401->14646|25431->14647|25553->14740|25583->14741|25645->14774|25675->14775|25733->14804|25763->14805|25819->14832|25849->14833|25899->14854|25929->14855|25994->14883|26019->14885|26076->14913|26106->14914|26236->15015|26266->15016|26420->15141|26450->15142|26664->15327|26694->15328|26789->15394|26819->15395|26995->15542|27025->15543|27060->15549|27090->15550|27348->15779|27378->15780|27551->15924|27581->15925|27712->16027|27742->16028|27804->16061|27834->16062|27892->16091|27922->16092|27978->16119|28008->16120|28058->16141|28088->16142|28136->16161|28166->16162|28232->16199|28262->16200|28767->16676|28797->16677|29455->17306|29485->17307|29533->17326|29563->17327|29631->17366|29661->17367|29810->17487|29840->17488|29901->17520|29931->17521|29996->17549|30021->17551|30079->17580|30109->17581|30239->17682|30269->17683|30632->18017|30662->18018|30876->18203|30906->18204|31142->18411|31172->18412|31337->18548|31367->18549|31490->18643|31520->18644|31578->18673|31608->18674|31664->18701|31694->18702|31744->18723|31774->18724|31822->18743|31852->18744|31921->18784|31951->18785|32100->18905|32130->18906|32191->18938|32221->18939|32286->18967|32311->18969|32370->18999|32400->19000|32530->19101|32560->19102|32917->19430|32947->19431|33266->19721|33296->19722|33530->19927|33560->19928|33618->19957|33648->19958|33704->19985|33734->19986|33784->20007|33814->20008|33862->20027|33892->20028|33954->20061|33984->20062|34128->20177|34158->20178|34219->20210|34249->20211|34423->20356|34453->20357|34596->20471|34626->20472|34731->20548|34761->20549|34978->20737|35008->20738|35202->20903|35232->20904|35301->20936|35326->20938|35494->21069|35519->21071|35572->21095|35602->21096|35925->21390|35955->21391|36076->21475|36101->21477|36177->21516|36202->21518|36256->21543|36286->21544|36393->21622|36423->21623|36670->21841|36700->21842|36748->21861|36778->21862|36820->21875|36850->21876|36889->21887|36919->21888|37014->21954|37044->21955|37298->22180|37328->22181|37451->22275|37481->22276|37518->22285|37548->22286|37583->22293|37612->22294|37658->22308
                    LINES: 20->1|24->1|25->3|26->4|28->6|28->6|29->7|29->7|50->28|50->28|50->28|51->29|51->29|51->29|52->30|52->30|52->30|53->31|53->31|53->31|54->32|54->32|54->32|55->33|55->33|55->33|56->34|56->34|56->34|57->35|57->35|57->35|58->36|58->36|58->36|61->39|61->39|66->44|66->44|71->49|71->49|72->50|72->50|75->53|75->53|76->54|76->54|81->59|81->59|85->63|85->63|90->68|90->68|91->69|91->69|92->70|92->70|94->72|94->72|94->72|94->72|98->76|98->76|101->79|101->79|102->80|102->80|105->83|105->83|107->85|107->85|110->88|110->88|113->91|113->91|115->93|115->93|116->94|116->94|117->95|117->95|118->96|118->96|123->101|123->101|124->102|124->102|126->104|126->104|127->105|127->105|128->106|128->106|130->108|130->108|131->109|131->109|132->110|132->110|133->111|133->111|134->112|134->112|134->112|134->112|135->113|135->113|136->114|136->114|136->114|136->114|138->116|138->116|141->119|141->119|145->123|145->123|145->123|149->127|149->127|149->127|150->128|150->128|157->135|157->135|157->135|157->135|159->137|159->137|160->138|160->138|162->140|162->140|163->141|163->141|163->141|163->141|164->142|164->142|167->145|167->145|167->145|167->145|170->148|170->148|171->149|171->149|171->149|171->149|173->151|173->151|175->153|175->153|177->155|177->155|177->155|177->155|179->157|179->157|181->159|181->159|182->160|182->160|186->164|186->164|188->166|188->166|190->168|190->168|191->169|191->169|192->170|192->170|192->170|192->170|193->171|193->171|194->172|194->172|195->173|195->173|197->175|197->175|199->177|199->177|202->180|202->180|204->182|204->182|205->183|205->183|206->184|206->184|208->186|208->186|209->187|209->187|212->190|212->190|212->190|212->190|213->191|213->191|214->192|214->192|216->194|216->194|216->194|216->194|217->195|217->195|218->196|218->196|219->197|219->197|219->197|219->197|219->197|219->197|220->198|220->198|222->200|222->200|223->201|223->201|223->201|223->201|224->202|224->202|225->203|225->203|227->205|227->205|227->205|227->205|227->205|227->205|228->206|228->206|229->207|229->207|229->207|229->207|229->207|229->207|230->208|230->208|231->209|231->209|233->211|233->211|236->214|236->214|238->216|238->216|240->218|240->218|248->226|248->226|249->227|249->227|251->229|251->229|251->229|251->229|254->232|254->232|256->234|256->234|256->234|256->234|257->235|257->235|257->235|257->235|266->244|266->244|273->251|273->251|274->252|274->252|274->252|275->253|275->253|276->254|276->254|278->256|278->256|279->257|279->257|282->260|282->260|283->261|283->261|284->262|284->262|284->262|286->264|286->264|286->264|286->264|287->265|287->265|289->267|289->267|289->267|289->267|290->268|290->268|291->269|291->269|292->270|292->270|294->272|294->272|294->272|294->272|295->273|295->273|296->274|296->274|296->274|296->274|297->275|297->275|299->277|299->277|299->277|299->277|300->278|300->278|303->281|303->281|305->283|305->283|305->283|305->283|306->284|306->284|306->284|306->284|308->286|308->286|310->288|310->288|313->291|313->291|317->295|317->295|317->295|317->295|318->296|318->296|320->298|320->298|323->301|323->301|323->301|323->301|325->303|325->303|326->304|326->304|327->305|327->305|328->306|328->306|329->307|329->307|330->308|330->308|330->308|330->308|332->310|332->310|334->312|334->312|337->315|337->315|338->316|338->316|340->318|340->318|340->318|340->318|343->321|343->321|345->323|345->323|347->325|347->325|348->326|348->326|349->327|349->327|350->328|350->328|351->329|351->329|352->330|352->330|352->330|352->330|358->336|358->336|366->344|366->344|367->345|367->345|367->345|367->345|369->347|369->347|369->347|369->347|370->348|370->348|370->348|370->348|372->350|372->350|376->354|376->354|379->357|379->357|382->360|382->360|384->362|384->362|386->364|386->364|387->365|387->365|388->366|388->366|389->367|389->367|390->368|390->368|390->368|390->368|392->370|392->370|392->370|392->370|393->371|393->371|393->371|393->371|395->373|395->373|399->377|399->377|404->382|404->382|407->385|407->385|408->386|408->386|409->387|409->387|410->388|410->388|411->389|411->389|411->389|411->389|413->391|413->391|413->391|413->391|416->394|416->394|419->397|419->397|421->399|421->399|424->402|424->402|425->403|425->403|426->404|426->404|428->406|428->406|428->406|428->406|433->411|433->411|435->413|435->413|435->413|435->413|436->414|436->414|438->416|438->416|441->419|441->419|442->420|442->420|443->421|443->421|444->422|444->422|446->424|446->424|449->427|449->427|451->429|451->429|452->430|452->430|453->431|453->431|455->433
                    -- GENERATED --
                */
            