
package views.html.metadatald

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object search extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.40*/("""

"""),_display_(Seq[Any](/*3.2*/main("Search metadata")/*3.25*/ {_display_(Seq[Any](format.raw/*3.27*/("""
    <h2>Advanced Search</h2>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form top-padding" id="metadata-search">

                    <div id="basic-search-container">
                        <!-- ANY/ALL DROPDOWN -->
                        <div class='form-group col-lg-4 col-md-4'>
                            <select id='add-metadata-grouping'>
                                <option value='AND'>Match ALL of the selected terms</option>
                                <option value='OR'>Match ANY of the selected terms</option>
                            </select>
                        </div>

                        <!-- ADD TERM BTN -->
                        <div class="form-group col-lg-8 col-md-8">
                            <a class='btn btn-default' id='add-clause' onclick='getBasicDefinitions(2)'>
                                <span class='glyphicon glyphicon-plus'></span> Add term</a>
                        </div>

                        <!-- SET OF TERM ROWS (populated in script below) -->
                        <div id="metadata-search-rows"></div>
                    </div>

                    <!-- SUBMIT BTN -->
                    <div class="form-group col-lg-4 col-md-4">
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Search</button>
                        <span id="mt-search-feedback"></span>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="row top-padding">
        <div class="col-lg-6 col-md-6"><h2>Datasets</h2><div id="datasets-results"></div></div>
        <div class="col-lg-6 col-md-6"><h2>Files</h2><div id="files-results"></div></div>
    </div>
    <div class="row top-padding">
        <div id="getmore" class="col-lg-12 col-md-12 text-center"></div>
    </div>
    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*46.35*/routes/*46.41*/.Assets.at("stylesheets/chosen.css"))),format.raw/*46.77*/("""">
    <script src=""""),_display_(Seq[Any](/*47.19*/routes/*47.25*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*47.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*48.19*/routes/*48.25*/.Assets.at("javascripts/handlebars-v1.3.0.js"))),format.raw/*48.71*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*49.19*/routes/*49.25*/.Assets.at("javascripts/handlebars-loader.js"))),format.raw/*49.71*/("""" type="text/javascript"></script>
    <script>
        var row_list = [];
        $("#add-metadata-grouping").select2("""),format.raw/*52.45*/("""{"""),format.raw/*52.46*/("""
            theme: "bootstrap",
            allowClear: false,
            width: "100%"
        """),format.raw/*56.9*/("""}"""),format.raw/*56.10*/(""")

        getBasicDefinitions(1);
        // Count is the batch size to get results; e.g. 60 would be 1-60, 61-120, 121-180, etc.
        count_stepsize = 60;
        count = 0;

        // Add a new row to the set of criteria
        function getBasicDefinitions(rowId) """),format.raw/*64.45*/("""{"""),format.raw/*64.46*/("""
            var rowStr = String(rowId);

            // Basic row definition
            $("#metadata-search-rows").append(
                $("<p><div id='metadata-clause-"+rowStr+"'>" +
                    <!-- FIELD SELECTOR DROPDOWN -->
                    "<div class='form-group col-lg-4 col-md-4'>" +
                        "<select id='add-metadata-select-"+rowStr+"'><option value=''></option></select></div>" +
                    <!-- OPERATOR DROPDOWN -->
                    "<div class='form-group col-lg-2 col-md-2'> <select id='add-metadata-operator-"+rowStr+"'>" +
                        // "<option value=':'>contains</option>" +
                        "<option value='=='>equals</option>" +
                        "<option value='!='>does not equal</option>" +
                        "<option value='>'>greater than</option>" +
                        "<option value='<'>less than</option>" +
                    "</select></div>" +
                    <!-- VALUE FIELD -->
                    "<div class='form-group col-lg-4 col-md-4'>" +
                        "<input type='text' class='form-control' id='v-"+rowStr+"' placeholder='Type value here'>" +
                    "</div>" +
                    <!-- REMOVE ROW BUTTON -->
                    "<div class='form-group col-lg-2 col-md-2'>" +
                    "<button class='btn' id='remove-"+rowStr+"' onclick='removeRow("+rowStr+")'>" +
                    "<span class='glyphicon glyphicon-minus'></span></button></div>" +
                "</div>"
                )
            );
            document.getElementById("add-clause").onclick = function () """),format.raw/*92.73*/("""{"""),format.raw/*92.74*/(""" getBasicDefinitions(rowId+1); """),format.raw/*92.105*/("""}"""),format.raw/*92.106*/(""";
            document.getElementById("v-"+rowStr).onkeydown = function(evt) """),format.raw/*93.76*/("""{"""),format.raw/*93.77*/("""
                // Allow Enter key to perform search on any value box
                if (evt.keyCode == 13) """),format.raw/*95.40*/("""{"""),format.raw/*95.41*/("""
                    count = 0;
                    search()
                    return false
                """),format.raw/*99.17*/("""}"""),format.raw/*99.18*/(""" else return true
            """),format.raw/*100.13*/("""}"""),format.raw/*100.14*/(""";
            row_list.push(rowStr);

            // Add theme to operator/grouping dropdowns
            $("#add-metadata-operator-"+rowStr).select2("""),format.raw/*104.57*/("""{"""),format.raw/*104.58*/("""
                theme: "bootstrap",
                allowClear: false,
                width: "100%"
            """),format.raw/*108.13*/("""}"""),format.raw/*108.14*/(""");
            $("#add-metadata-grouping-"+rowStr).select2("""),format.raw/*109.57*/("""{"""),format.raw/*109.58*/("""
                theme: "bootstrap",
                allowClear: false,
                width: "100%"
            """),format.raw/*113.13*/("""}"""),format.raw/*113.14*/(""");

            // fetch metadata definitions
            var request = jsRoutes.api.Metadata.getDefinitionsDistinctName().ajax("""),format.raw/*116.83*/("""{"""),format.raw/*116.84*/("""
                type: 'GET',
                contentType: "application/json"
            """),format.raw/*119.13*/("""}"""),format.raw/*119.14*/(""");
            request.done(function (response, textStatus, jqXHR) """),format.raw/*120.65*/("""{"""),format.raw/*120.66*/("""
                var fields = response;

                $("#add-metadata-select-"+rowStr).empty()
                for (var i = 0; i < fields.length; i++) """),format.raw/*124.57*/("""{"""),format.raw/*124.58*/("""
                    var elem = $("<option></option>");
                    elem.attr("data-type", fields[i].json.type);
                    elem.attr("data-id", fields[i].json.label);
                    elem.attr("value", "metadata."+fields[i].json.label);
                    elem.text(fields[i].json.label);
                    $("#add-metadata-select-"+String(rowId)).append(elem);
                """),format.raw/*131.17*/("""}"""),format.raw/*131.18*/("""

                // Select box will populate as user types, with metadata field autocomplete suggestions
                $("#add-metadata-select-"+rowStr).select2("""),format.raw/*134.59*/("""{"""),format.raw/*134.60*/("""
                    theme: "bootstrap",
                    placeholder: "Select a type or field key",
                    allowClear: true,
                    width: "100%",
                    ajax: """),format.raw/*139.27*/("""{"""),format.raw/*139.28*/("""
                        url: function(filter) """),format.raw/*140.47*/("""{"""),format.raw/*140.48*/("""
                            // Get autocomplete results if typing; otherwise return standard set of definitions
                            if (filter.term == null || filter.term == "") """),format.raw/*142.75*/("""{"""),format.raw/*142.76*/("""
                                return jsRoutes.api.Metadata.getDefinitionsDistinctName().url
                            """),format.raw/*144.29*/("""}"""),format.raw/*144.30*/("""
                            else """),format.raw/*145.34*/("""{"""),format.raw/*145.35*/("""
                                return jsRoutes.api.Metadata.getAutocompleteName(filter.term).url
                            """),format.raw/*147.29*/("""}"""),format.raw/*147.30*/("""
                        """),format.raw/*148.25*/("""}"""),format.raw/*148.26*/(""",
                        // Populate autocomplete as user types
                        processResults: function(data, page) """),format.raw/*150.62*/("""{"""),format.raw/*150.63*/("""
                            var outMap = """),format.raw/*151.42*/("""{"""),format.raw/*151.43*/("""}"""),format.raw/*151.44*/(""";

                            for (var rez=0; rez<data.length; rez++) """),format.raw/*153.69*/("""{"""),format.raw/*153.70*/("""
                                var entry = data[rez];

                                // Metadata Definitions
                                if (typeof(entry) == 'object') """),format.raw/*157.64*/("""{"""),format.raw/*157.65*/("""
                                    var entryGroup = "Metadata Definitions";
                                    var entryData = """),format.raw/*159.53*/("""{"""),format.raw/*159.54*/("""text: entry.json.label, id: "metadata."+entry.json.label"""),format.raw/*159.110*/("""}"""),format.raw/*159.111*/(""";
                                // suggestions from elasticsearch
                                """),format.raw/*161.33*/("""}"""),format.raw/*161.34*/(""" else """),format.raw/*161.40*/("""{"""),format.raw/*161.41*/("""
                                    if (entry.indexOf('.') > -1) """),format.raw/*162.66*/("""{"""),format.raw/*162.67*/("""
                                        if (entry.indexOf('/extractors/') > -1) """),format.raw/*163.81*/("""{"""),format.raw/*163.82*/("""
                                            // Group extractor-specific fields together under extractor
                                            var entryGroup = entry.substring(entry.indexOf('/extractors/')+12, entry.lastIndexOf('.'))+" (Extractor)";
                                        """),format.raw/*166.41*/("""}"""),format.raw/*166.42*/("""
                                        else if (entry.split('.').length > 2 )"""),format.raw/*167.79*/("""{"""),format.raw/*167.80*/("""
                                            // Group user-submitted metadata under user's name
                                            var entryGroup = entry.substring(entry.indexOf('.')+1, entry.lastIndexOf('.'))+" (User)";
                                        """),format.raw/*170.41*/("""}"""),format.raw/*170.42*/("""
                                        else """),format.raw/*171.46*/("""{"""),format.raw/*171.47*/("""
                                            // This should be metadata definitions otherwise
                                            var entryGroup = "Metadata Definitions";
                                        """),format.raw/*174.41*/("""}"""),format.raw/*174.42*/("""
                                        var entryData = """),format.raw/*175.57*/("""{"""),format.raw/*175.58*/("""id: entry, text: entry.substring(entry.lastIndexOf('.')+1, entry.length)"""),format.raw/*175.130*/("""}"""),format.raw/*175.131*/("""
                                    """),format.raw/*176.37*/("""}"""),format.raw/*176.38*/(""" else """),format.raw/*176.44*/("""{"""),format.raw/*176.45*/("""
                                        // Simple entry
                                        var entryGroup = ""
                                        var entryData = """),format.raw/*179.57*/("""{"""),format.raw/*179.58*/("""text: entry, id: entry"""),format.raw/*179.80*/("""}"""),format.raw/*179.81*/("""
                                    """),format.raw/*180.37*/("""}"""),format.raw/*180.38*/("""
                                """),format.raw/*181.33*/("""}"""),format.raw/*181.34*/("""
                                if (!outMap.hasOwnProperty(entryGroup))
                                    outMap[entryGroup] = [];
                                outMap[entryGroup].push(entryData)
                            """),format.raw/*185.29*/("""}"""),format.raw/*185.30*/("""

                            var outList = [];
                            for (var group in outMap) """),format.raw/*188.55*/("""{"""),format.raw/*188.56*/("""
                                if (group == "") """),format.raw/*189.50*/("""{"""),format.raw/*189.51*/("""
                                    for (var ungrouped=0; ungrouped<outMap[group].length; ungrouped++)
                                        outList.push(outMap[group][ungrouped])
                                """),format.raw/*192.33*/("""}"""),format.raw/*192.34*/(""" else
                                    outList.push("""),format.raw/*193.50*/("""{"""),format.raw/*193.51*/(""""text":group, "children": outMap[group]"""),format.raw/*193.90*/("""}"""),format.raw/*193.91*/(""")
                            """),format.raw/*194.29*/("""}"""),format.raw/*194.30*/("""

                            return """),format.raw/*196.36*/("""{"""),format.raw/*196.37*/("""
                                results: outList
                            """),format.raw/*198.29*/("""}"""),format.raw/*198.30*/(""";
                        """),format.raw/*199.25*/("""}"""),format.raw/*199.26*/("""
                    """),format.raw/*200.21*/("""}"""),format.raw/*200.22*/("""
                """),format.raw/*201.17*/("""}"""),format.raw/*201.18*/(""")
            """),format.raw/*202.13*/("""}"""),format.raw/*202.14*/(""");

            request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*204.67*/("""{"""),format.raw/*204.68*/("""
                console.error("The following error occured: " + textStatus, errorThrown);
                var errMsg = "You must be logged in to retrieve metadata definitions";
                if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*207.60*/("""{"""),format.raw/*207.61*/("""
                    notify("Metadata was not added due to : " + errorThrown, "error");
                """),format.raw/*209.17*/("""}"""),format.raw/*209.18*/("""
            """),format.raw/*210.13*/("""}"""),format.raw/*210.14*/(""");
        """),format.raw/*211.9*/("""}"""),format.raw/*211.10*/("""

        // Construct a JSON object with search information
        function generateSearchObject() """),format.raw/*214.41*/("""{"""),format.raw/*214.42*/("""
            var searchObj = [];
            for (var row=0; row<row_list.length; row++) """),format.raw/*216.57*/("""{"""),format.raw/*216.58*/("""
                var rowkey = $("#add-metadata-select-"+row_list[row]+" :selected").val();
                var rowop = $("#add-metadata-operator-"+row_list[row]).val();
                var rowval = $("#v-"+row_list[row]).val();

                // Ignore any terms without a value
                if (rowval != "") """),format.raw/*222.35*/("""{"""),format.raw/*222.36*/("""
                    if (rowkey.indexOf('.') > -1) """),format.raw/*223.51*/("""{"""),format.raw/*223.52*/("""
                        var keyvals = rowkey.split('.')
                        var extractorKey = keyvals[0]
                        var leafKey = keyvals[keyvals.length-1]
                    """),format.raw/*227.21*/("""}"""),format.raw/*227.22*/(""" else """),format.raw/*227.28*/("""{"""),format.raw/*227.29*/("""
                        var extractorKey = null
                        var leafKey = rowkey
                    """),format.raw/*230.21*/("""}"""),format.raw/*230.22*/("""

                    searchObj.push("""),format.raw/*232.36*/("""{"""),format.raw/*232.37*/("""
                        "field_key": rowkey,
                        "operator": rowop,
                        "field_value": rowval,
                        "extractor_key": extractorKey,
                        "field_leaf_key": leafKey
                    """),format.raw/*238.21*/("""}"""),format.raw/*238.22*/(""");
                """),format.raw/*239.17*/("""}"""),format.raw/*239.18*/("""
            """),format.raw/*240.13*/("""}"""),format.raw/*240.14*/("""

            return searchObj;
        """),format.raw/*243.9*/("""}"""),format.raw/*243.10*/("""

        // Submit contents of basic search rows
        function search(from_count) """),format.raw/*246.37*/("""{"""),format.raw/*246.38*/("""
            from_count = from_count || 0;
            console.log("Querying result set from "+String(from_count)+" with size "+String(count_stepsize));
            var query = generateSearchObject();
            var grouping = $("#add-metadata-grouping").val()
            if (query != []) """),format.raw/*251.30*/("""{"""),format.raw/*251.31*/("""
                var request = jsRoutes.api.Search.searchJson(JSON.stringify(query), grouping, from_count, count_stepsize).ajax("""),format.raw/*252.128*/("""{"""),format.raw/*252.129*/("""
                    type: 'GET',
                    contentType: "application/json"
                """),format.raw/*255.17*/("""}"""),format.raw/*255.18*/(""");

                request.done(function(resp, status, err)"""),format.raw/*257.57*/("""{"""),format.raw/*257.58*/("""
                    if (from_count > 0) """),format.raw/*258.41*/("""{"""),format.raw/*258.42*/("""
                        parseSearchResults(resp, status, err, false)
                    """),format.raw/*260.21*/("""}"""),format.raw/*260.22*/(""" else """),format.raw/*260.28*/("""{"""),format.raw/*260.29*/("""
                        parseSearchResults(resp, status, err)
                    """),format.raw/*262.21*/("""}"""),format.raw/*262.22*/("""
                """),format.raw/*263.17*/("""}"""),format.raw/*263.18*/(""");

                request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*265.71*/("""{"""),format.raw/*265.72*/("""
                    console.error("The following error occured: " + textStatus, errorThrown);
                """),format.raw/*267.17*/("""}"""),format.raw/*267.18*/(""");

                return;
            """),format.raw/*270.13*/("""}"""),format.raw/*270.14*/("""

            $( "#mt-search-feedback" ).text( " Not valid!" ).show().fadeOut( 2000 );
        """),format.raw/*273.9*/("""}"""),format.raw/*273.10*/("""

        function removeRow(rowId) """),format.raw/*275.35*/("""{"""),format.raw/*275.36*/("""
            var rowStr = String(rowId);
            $("#metadata-clause-"+rowStr).remove();
            row_list.splice(row_list.indexOf(rowStr), 1);
        """),format.raw/*279.9*/("""}"""),format.raw/*279.10*/("""

        function parseSearchResults(response, textStatus, jqXHR, empty=true) """),format.raw/*281.78*/("""{"""),format.raw/*281.79*/("""
            if (empty) """),format.raw/*282.24*/("""{"""),format.raw/*282.25*/("""
                $( "#datasets-results" ).empty();
                $( "#files-results" ).empty();
            """),format.raw/*285.13*/("""}"""),format.raw/*285.14*/("""

            var datasets = response.datasets;
            if (datasets.length == 0) $('#datasets-results').append("No datasets found</br>");
            for (var i=0; i<datasets.length; i++) """),format.raw/*289.51*/("""{"""),format.raw/*289.52*/("""
                var modalTemplate = Handlebars.getTemplate(""""),_display_(Seq[Any](/*290.62*/routes/*290.68*/.Assets.at("templates/metadata/search_dataset_result"))),format.raw/*290.122*/("""");
                var html = modalTemplate("""),format.raw/*291.42*/("""{"""),format.raw/*291.43*/("""'url': jsRoutes.controllers.Datasets.dataset(datasets[i].id).url, 'name': datasets[i].name"""),format.raw/*291.133*/("""}"""),format.raw/*291.134*/(""");
                $('#datasets-results').append(html);
            """),format.raw/*293.13*/("""}"""),format.raw/*293.14*/("""

            var files = response.files;
            if (files.length == 0) $('#files-results').append("No files found</br>");
            for (var i=0; i<files.length; i++) """),format.raw/*297.48*/("""{"""),format.raw/*297.49*/("""
                var modalTemplate = Handlebars.getTemplate(""""),_display_(Seq[Any](/*298.62*/routes/*298.68*/.Assets.at("templates/metadata/search_file_result"))),format.raw/*298.119*/("""");
                var html = modalTemplate("""),format.raw/*299.42*/("""{"""),format.raw/*299.43*/("""'url': jsRoutes.controllers.Files.file(files[i].id).url, 'name': files[i].name"""),format.raw/*299.121*/("""}"""),format.raw/*299.122*/(""");
                $('#files-results').append(html);
                // TODO: example search strings
                // TODO: Support API direct calls
            """),format.raw/*303.13*/("""}"""),format.raw/*303.14*/("""

            if (response.count >= count_stepsize) """),format.raw/*305.51*/("""{"""),format.raw/*305.52*/("""
                $('#getmore').html('<a id="showmore" class="btn btn-link"><span class="glyphicon glyphicon-hand-down"></span> Show more results</a>');
                $('#showmore').click( function() """),format.raw/*307.50*/("""{"""),format.raw/*307.51*/("""
                    if (response.count < count_stepsize)
                        count += response.count;
                    else
                        count += count_stepsize;
                    search(count);
                """),format.raw/*313.17*/("""}"""),format.raw/*313.18*/(""" );
            """),format.raw/*314.13*/("""}"""),format.raw/*314.14*/(""" else """),format.raw/*314.20*/("""{"""),format.raw/*314.21*/("""
                $('#getmore').html('')
            """),format.raw/*316.13*/("""}"""),format.raw/*316.14*/("""

        """),format.raw/*318.9*/("""}"""),format.raw/*318.10*/("""

        // form submission
        $( "form[id='metadata-search']").submit(function( event ) """),format.raw/*321.67*/("""{"""),format.raw/*321.68*/("""
            event.preventDefault();
            count = 0;
            search();

        """),format.raw/*326.9*/("""}"""),format.raw/*326.10*/(""");
    </script>
""")))})))}
    }
    
    def render(user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply()(user)
    
    def f:(() => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = () => (user) => apply()(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/metadatald/search.scala.html
                    HASH: fb15f7f60f6400970424edd5e92c285fdddfedb0
                    MATRIX: 612->1|744->39|781->42|812->65|851->67|2868->2048|2883->2054|2941->2090|2998->2111|3013->2117|3077->2159|3166->2212|3181->2218|3249->2264|3338->2317|3353->2323|3421->2369|3568->2488|3597->2489|3722->2587|3751->2588|4051->2860|4080->2861|5752->4505|5781->4506|5841->4537|5871->4538|5976->4615|6005->4616|6143->4726|6172->4727|6310->4837|6339->4838|6398->4868|6428->4869|6607->5019|6637->5020|6780->5134|6810->5135|6898->5194|6928->5195|7071->5309|7101->5310|7258->5438|7288->5439|7407->5529|7437->5530|7533->5597|7563->5598|7747->5753|7777->5754|8209->6157|8239->6158|8432->6322|8462->6323|8694->6526|8724->6527|8800->6574|8830->6575|9046->6762|9076->6763|9228->6886|9258->6887|9321->6921|9351->6922|9507->7049|9537->7050|9591->7075|9621->7076|9776->7202|9806->7203|9877->7245|9907->7246|9937->7247|10037->7318|10067->7319|10272->7495|10302->7496|10461->7626|10491->7627|10577->7683|10608->7684|10737->7784|10767->7785|10802->7791|10832->7792|10927->7858|10957->7859|11067->7940|11097->7941|11422->8237|11452->8238|11560->8317|11590->8318|11889->8588|11919->8589|11994->8635|12024->8636|12272->8855|12302->8856|12388->8913|12418->8914|12520->8986|12551->8987|12617->9024|12647->9025|12682->9031|12712->9032|12914->9205|12944->9206|12995->9228|13025->9229|13091->9266|13121->9267|13183->9300|13213->9301|13471->9530|13501->9531|13632->9633|13662->9634|13741->9684|13771->9685|14015->9900|14045->9901|14129->9956|14159->9957|14227->9996|14257->9997|14316->10027|14346->10028|14412->10065|14442->10066|14549->10144|14579->10145|14634->10171|14664->10172|14714->10193|14744->10194|14790->10211|14820->10212|14863->10226|14893->10227|14992->10297|15022->10298|15288->10535|15318->10536|15451->10640|15481->10641|15523->10654|15553->10655|15592->10666|15622->10667|15752->10768|15782->10769|15900->10858|15930->10859|16274->11174|16304->11175|16384->11226|16414->11227|16638->11422|16668->11423|16703->11429|16733->11430|16876->11544|16906->11545|16972->11582|17002->11583|17292->11844|17322->11845|17370->11864|17400->11865|17442->11878|17472->11879|17540->11919|17570->11920|17685->12006|17715->12007|18035->12298|18065->12299|18223->12427|18254->12428|18385->12530|18415->12531|18504->12591|18534->12592|18604->12633|18634->12634|18753->12724|18783->12725|18818->12731|18848->12732|18960->12815|18990->12816|19036->12833|19066->12834|19169->12908|19199->12909|19339->13020|19369->13021|19438->13061|19468->13062|19591->13157|19621->13158|19686->13194|19716->13195|19903->13354|19933->13355|20041->13434|20071->13435|20124->13459|20154->13460|20293->13570|20323->13571|20545->13764|20575->13765|20674->13827|20690->13833|20768->13887|20842->13932|20872->13933|20992->14023|21023->14024|21120->14092|21150->14093|21354->14268|21384->14269|21483->14331|21499->14337|21574->14388|21648->14433|21678->14434|21786->14512|21817->14513|22009->14676|22039->14677|22120->14729|22150->14730|22380->14931|22410->14932|22671->15164|22701->15165|22746->15181|22776->15182|22811->15188|22841->15189|22922->15241|22952->15242|22990->15252|23020->15253|23144->15348|23174->15349|23293->15440|23323->15441
                    LINES: 20->1|23->1|25->3|25->3|25->3|68->46|68->46|68->46|69->47|69->47|69->47|70->48|70->48|70->48|71->49|71->49|71->49|74->52|74->52|78->56|78->56|86->64|86->64|114->92|114->92|114->92|114->92|115->93|115->93|117->95|117->95|121->99|121->99|122->100|122->100|126->104|126->104|130->108|130->108|131->109|131->109|135->113|135->113|138->116|138->116|141->119|141->119|142->120|142->120|146->124|146->124|153->131|153->131|156->134|156->134|161->139|161->139|162->140|162->140|164->142|164->142|166->144|166->144|167->145|167->145|169->147|169->147|170->148|170->148|172->150|172->150|173->151|173->151|173->151|175->153|175->153|179->157|179->157|181->159|181->159|181->159|181->159|183->161|183->161|183->161|183->161|184->162|184->162|185->163|185->163|188->166|188->166|189->167|189->167|192->170|192->170|193->171|193->171|196->174|196->174|197->175|197->175|197->175|197->175|198->176|198->176|198->176|198->176|201->179|201->179|201->179|201->179|202->180|202->180|203->181|203->181|207->185|207->185|210->188|210->188|211->189|211->189|214->192|214->192|215->193|215->193|215->193|215->193|216->194|216->194|218->196|218->196|220->198|220->198|221->199|221->199|222->200|222->200|223->201|223->201|224->202|224->202|226->204|226->204|229->207|229->207|231->209|231->209|232->210|232->210|233->211|233->211|236->214|236->214|238->216|238->216|244->222|244->222|245->223|245->223|249->227|249->227|249->227|249->227|252->230|252->230|254->232|254->232|260->238|260->238|261->239|261->239|262->240|262->240|265->243|265->243|268->246|268->246|273->251|273->251|274->252|274->252|277->255|277->255|279->257|279->257|280->258|280->258|282->260|282->260|282->260|282->260|284->262|284->262|285->263|285->263|287->265|287->265|289->267|289->267|292->270|292->270|295->273|295->273|297->275|297->275|301->279|301->279|303->281|303->281|304->282|304->282|307->285|307->285|311->289|311->289|312->290|312->290|312->290|313->291|313->291|313->291|313->291|315->293|315->293|319->297|319->297|320->298|320->298|320->298|321->299|321->299|321->299|321->299|325->303|325->303|327->305|327->305|329->307|329->307|335->313|335->313|336->314|336->314|336->314|336->314|338->316|338->316|340->318|340->318|343->321|343->321|348->326|348->326
                    -- GENERATED --
                */
            