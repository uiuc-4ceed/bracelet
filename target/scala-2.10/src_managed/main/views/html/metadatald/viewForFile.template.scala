
package views.html.metadatald

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object viewForFile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[List[models.Metadata],Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(metadata: List[models.Metadata], toDelete: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import java.net.URL

import play.api.libs.json.JsString

import play.api.libs.json.JsObject

import play.api.libs.json.JsValue

import play.api.libs.json.JsArray

import api.Permission

def /*14.2*/printContent/*14.14*/(agent: Agent, content: JsValue, contextId: Option[UUID], contextURL: Option[URL], resourceId: UUID):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*14.118*/("""
"""),_display_(Seq[Any](/*15.2*/content/*15.9*/ match/*15.15*/ {/*16.1*/case o: JsObject =>/*16.20*/ {_display_(Seq[Any](format.raw/*16.22*/("""
<ul>
    """),_display_(Seq[Any](/*18.6*/for((key, value) <- o.fields) yield /*18.35*/ {_display_(Seq[Any](format.raw/*18.37*/("""
    """),_display_(Seq[Any](/*19.6*/value/*19.11*/ match/*19.17*/ {/*20.5*/case o: JsObject =>/*20.24*/ {_display_(Seq[Any](format.raw/*20.26*/("""
    <li class="md-block">
        <a class="collapse-icon">
            <span class="glyphicon glyphicon-minus"></span>
        </a>
        <!-- only with context and user (not extractor) generated -->
        """),_display_(Seq[Any](/*26.10*/if(contextId.isDefined && agent.typeOfAgent.toLowerCase().indexOf("user") >= 0)/*26.89*/ {_display_(Seq[Any](format.raw/*26.91*/("""
        <!-- ids don't like spaces, you can use regex for removing different characters in metadata names (key) as well -->
        """),_display_(Seq[Any](/*28.10*/defining(contextId.get + key.replaceAll("[() ]","") + resourceId.toString())/*28.86*/ { mid =>_display_(Seq[Any](format.raw/*28.95*/("""
        """),_display_(Seq[Any](/*29.10*/if(user.isDefined)/*29.28*/ {_display_(Seq[Any](format.raw/*29.30*/("""
        <a id='"""),_display_(Seq[Any](/*30.17*/mid)),format.raw/*30.20*/("""' href="javascript:void(0)"
           onmouseover="getMetadataContext('"""),_display_(Seq[Any](/*31.46*/mid)),format.raw/*31.49*/("""','"""),_display_(Seq[Any](/*31.53*/contextId/*31.62*/.get)),format.raw/*31.66*/("""','"""),_display_(Seq[Any](/*31.70*/key)),format.raw/*31.73*/("""');"
           onmouseout="leaveMetadataContext('"""),_display_(Seq[Any](/*32.47*/mid)),format.raw/*32.50*/("""');">
            <strong>"""),_display_(Seq[Any](/*33.22*/key)),format.raw/*33.25*/(""":</strong></a>
        """)))}/*34.11*/else/*34.16*/{_display_(Seq[Any](format.raw/*34.17*/("""
        <strong>"""),_display_(Seq[Any](/*35.18*/key)),format.raw/*35.21*/(""":</strong>
        """)))})),format.raw/*36.10*/("""
        """),_display_(Seq[Any](/*37.10*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*37.67*/("""
        """)))})),format.raw/*38.10*/("""
        """)))}/*39.11*/else/*39.16*/{_display_(Seq[Any](format.raw/*39.17*/("""
        """),_display_(Seq[Any](/*40.10*/contextURL/*40.20*/ match/*40.26*/ {/*41.9*/case Some(u) =>/*41.24*/ {_display_(Seq[Any](format.raw/*41.26*/("""
        <a href=""""),_display_(Seq[Any](/*42.19*/u/*42.20*/.toString)),format.raw/*42.29*/("""" target="_blank" title="Show context reference"><strong>"""),_display_(Seq[Any](/*42.87*/key)),format.raw/*42.90*/(""":</strong></a>
        """),_display_(Seq[Any](/*43.10*/printContent(agent, value,contextId,contextURL,resourceId))),format.raw/*43.68*/("""
        """)))}/*45.9*/case None =>/*45.21*/ {_display_(Seq[Any](format.raw/*45.23*/("""
        <strong>"""),_display_(Seq[Any](/*46.18*/key)),format.raw/*46.21*/(""":</strong> """),_display_(Seq[Any](/*46.33*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*46.90*/("""
        """)))}})),format.raw/*48.10*/("""
        """)))})),format.raw/*49.10*/("""
    </li>
    """)))}/*52.5*/case o: JsArray =>/*52.23*/ {_display_(Seq[Any](format.raw/*52.25*/("""
    <li class="md-block">
        <a class="collapse-icon">
            <span class="glyphicon glyphicon-minus"></span>
        </a>
        """),_display_(Seq[Any](/*57.10*/if(contextId.isDefined && agent.typeOfAgent.toLowerCase().indexOf("user") >= 0)/*57.89*/ {_display_(Seq[Any](format.raw/*57.91*/("""
        """),_display_(Seq[Any](/*58.10*/defining(contextId.get + key.replaceAll("[() ]","") + resourceId.toString())/*58.86*/ { mid =>_display_(Seq[Any](format.raw/*58.95*/("""
        """),_display_(Seq[Any](/*59.10*/if(user.isDefined)/*59.28*/ {_display_(Seq[Any](format.raw/*59.30*/("""
        <a id='"""),_display_(Seq[Any](/*60.17*/mid)),format.raw/*60.20*/("""' href="javascript:void(0)"
           onmouseover="getMetadataContext('"""),_display_(Seq[Any](/*61.46*/mid)),format.raw/*61.49*/("""','"""),_display_(Seq[Any](/*61.53*/contextId/*61.62*/.get)),format.raw/*61.66*/("""','"""),_display_(Seq[Any](/*61.70*/key)),format.raw/*61.73*/("""');"
           onmouseout="leaveMetadataContext('"""),_display_(Seq[Any](/*62.47*/mid)),format.raw/*62.50*/("""');">
            <strong>"""),_display_(Seq[Any](/*63.22*/key)),format.raw/*63.25*/(""":</strong></a>
        """)))}/*64.11*/else/*64.16*/{_display_(Seq[Any](format.raw/*64.17*/("""
        <strong>"""),_display_(Seq[Any](/*65.18*/key)),format.raw/*65.21*/(""":</strong>
        """)))})),format.raw/*66.10*/("""
        """),_display_(Seq[Any](/*67.10*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*67.67*/("""
        """)))})),format.raw/*68.10*/("""
        """)))}/*69.11*/else/*69.16*/{_display_(Seq[Any](format.raw/*69.17*/("""
        """),_display_(Seq[Any](/*70.10*/contextURL/*70.20*/ match/*70.26*/ {/*71.9*/case Some(u) =>/*71.24*/ {_display_(Seq[Any](format.raw/*71.26*/("""
        <a href=""""),_display_(Seq[Any](/*72.19*/u/*72.20*/.toString)),format.raw/*72.29*/("""" target="_blank" title="Show context reference"><strong>"""),_display_(Seq[Any](/*72.87*/key)),format.raw/*72.90*/(""":</strong></a>
        """),_display_(Seq[Any](/*73.10*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*73.67*/("""
        """)))}/*75.9*/case None =>/*75.21*/ {_display_(Seq[Any](format.raw/*75.23*/("""
        <strong>"""),_display_(Seq[Any](/*76.18*/key)),format.raw/*76.21*/(""":</strong> """),_display_(Seq[Any](/*76.33*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*76.90*/("""
        """)))}})),format.raw/*78.10*/("""
        """)))})),format.raw/*79.10*/("""
    </li>
    """)))}/*82.5*/case _ =>/*82.14*/ {_display_(Seq[Any](format.raw/*82.16*/("""
    <li class="md-block">
        """),_display_(Seq[Any](/*84.10*/if(contextId.isDefined && agent.typeOfAgent.toLowerCase().indexOf("user") >= 0)/*84.89*/ {_display_(Seq[Any](format.raw/*84.91*/("""
        """),_display_(Seq[Any](/*85.10*/defining(contextId.get + key.replaceAll("[() ]","") + resourceId.toString())/*85.86*/ { mid =>_display_(Seq[Any](format.raw/*85.95*/("""
        """),_display_(Seq[Any](/*86.10*/if(user.isDefined)/*86.28*/ {_display_(Seq[Any](format.raw/*86.30*/("""
        <a id='"""),_display_(Seq[Any](/*87.17*/mid)),format.raw/*87.20*/("""' href="javascript:void(0)"
           onmouseover="getMetadataContext('"""),_display_(Seq[Any](/*88.46*/mid)),format.raw/*88.49*/("""','"""),_display_(Seq[Any](/*88.53*/contextId/*88.62*/.get)),format.raw/*88.66*/("""','"""),_display_(Seq[Any](/*88.70*/key)),format.raw/*88.73*/("""');"
           onmouseout="leaveMetadataContext('"""),_display_(Seq[Any](/*89.47*/mid)),format.raw/*89.50*/("""');">
            <strong>"""),_display_(Seq[Any](/*90.22*/key)),format.raw/*90.25*/(""":</strong></a>
        """)))}/*91.11*/else/*91.16*/{_display_(Seq[Any](format.raw/*91.17*/("""
        <strong>"""),_display_(Seq[Any](/*92.18*/key)),format.raw/*92.21*/(""":</strong>
        """)))})),format.raw/*93.10*/("""
        """),_display_(Seq[Any](/*94.10*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*94.67*/("""
        """)))})),format.raw/*95.10*/("""
        """)))}/*96.11*/else/*96.16*/{_display_(Seq[Any](format.raw/*96.17*/("""
        """),_display_(Seq[Any](/*97.10*/contextURL/*97.20*/ match/*97.26*/ {/*98.9*/case Some(u) =>/*98.24*/ {_display_(Seq[Any](format.raw/*98.26*/("""
        <a href=""""),_display_(Seq[Any](/*99.19*/u/*99.20*/.toString)),format.raw/*99.29*/("""" target="_blank" title="Show context reference"><strong>"""),_display_(Seq[Any](/*99.87*/key)),format.raw/*99.90*/(""":</strong></a>
        """),_display_(Seq[Any](/*100.10*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*100.67*/("""
        """)))}/*102.9*/case None =>/*102.21*/ {_display_(Seq[Any](format.raw/*102.23*/("""<strong>"""),_display_(Seq[Any](/*102.32*/key)),format.raw/*102.35*/(""":</strong> """),_display_(Seq[Any](/*102.47*/printContent(agent,value,contextId,contextURL,resourceId)))))}})),format.raw/*103.10*/("""
        """)))})),format.raw/*104.10*/("""
    </li>
    """)))}})),format.raw/*107.6*/("""

    """)))})),format.raw/*109.6*/("""
</ul>
""")))}/*112.1*/case a: JsArray =>/*112.19*/ {_display_(Seq[Any](format.raw/*112.21*/("""
<ul>
    """),_display_(Seq[Any](/*114.6*/for((value, i) <- a.value.zipWithIndex) yield /*114.45*/ {_display_(Seq[Any](format.raw/*114.47*/("""
    <li class="md-block">"""),_display_(Seq[Any](/*115.27*/printContent(agent,value,contextId,contextURL,resourceId))),format.raw/*115.84*/("""</li>
    """)))})),format.raw/*116.6*/("""
</ul>
""")))}/*119.1*/case s: JsString =>/*119.20*/ {_display_(Seq[Any](format.raw/*119.22*/("""
"""),_display_(Seq[Any](/*120.2*/if(s.value.startsWith("http"))/*120.32*/ {_display_(Seq[Any](format.raw/*120.34*/("""
<a target="_blank" href=""""),_display_(Seq[Any](/*121.27*/s/*121.28*/.value)),format.raw/*121.34*/("""">"""),_display_(Seq[Any](/*121.37*/s/*121.38*/.value)),format.raw/*121.44*/("""</a>
""")))}/*122.3*/else/*122.8*/{_display_(Seq[Any](format.raw/*122.9*/("""
"""),_display_(Seq[Any](/*123.2*/s/*123.3*/.value)),format.raw/*123.9*/("""
""")))})),format.raw/*124.2*/("""
""")))}/*126.1*/case _ =>/*126.10*/ {_display_(Seq[Any](_display_(Seq[Any](/*126.13*/content))))}})),format.raw/*127.2*/("""
""")))};def /*130.2*/printHeader/*130.13*/(agent: Agent, date: java.util.Date, resourceId: UUID, mid: UUID):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*130.82*/("""
"""),_display_(Seq[Any](/*131.2*/agent/*131.7*/.operation)),format.raw/*131.17*/(""" by
"""),_display_(Seq[Any](/*132.2*/if(agent.displayName.length > 0)/*132.34*/ {_display_(Seq[Any](format.raw/*132.36*/("""
"""),_display_(Seq[Any](/*133.2*/if(agent.typeOfAgent.toLowerCase().indexOf("user") >= 0)/*133.58*/ {_display_(Seq[Any](format.raw/*133.60*/("""
"""),_display_(Seq[Any](/*134.2*/defining(resourceId.toString() + mid.toString())/*134.50*/ { aid =>_display_(Seq[Any](format.raw/*134.59*/("""
"""),_display_(Seq[Any](/*135.2*/if(user.isDefined)/*135.20*/ {_display_(Seq[Any](format.raw/*135.22*/("""

<a id='"""),_display_(Seq[Any](/*137.9*/aid)),format.raw/*137.12*/("""' href=""""),_display_(Seq[Any](/*137.21*/routes/*137.27*/.Profile.viewProfileUUID(agent.id))),format.raw/*137.61*/(""""
   onmouseover="getAgentContext('"""),_display_(Seq[Any](/*138.35*/aid)),format.raw/*138.38*/("""','"""),_display_(Seq[Any](/*138.42*/agent/*138.47*/.id)),format.raw/*138.50*/("""');"
   onmouseout="leaveAgentContext('"""),_display_(Seq[Any](/*139.36*/aid)),format.raw/*139.39*/("""');">"""),_display_(Seq[Any](/*139.45*/agent/*139.50*/.displayName)),format.raw/*139.62*/("""</a>
""")))}/*140.3*/else/*140.8*/{_display_(Seq[Any](format.raw/*140.9*/("""
"""),_display_(Seq[Any](/*141.2*/agent/*141.7*/.displayName)),format.raw/*141.19*/("""
""")))})),format.raw/*142.2*/("""

""")))})),format.raw/*144.2*/("""
""")))}/*145.3*/else/*145.8*/{_display_(Seq[Any](format.raw/*145.9*/("""
"""),_display_(Seq[Any](/*146.2*/if(!agent.url.isDefined)/*146.26*/ {_display_(Seq[Any](format.raw/*146.28*/("""
<a href=""""),_display_(Seq[Any](/*147.11*/agent/*147.16*/.url)),format.raw/*147.20*/("""" target="_blank">"""),_display_(Seq[Any](/*147.39*/agent/*147.44*/.displayName)),format.raw/*147.56*/("""</a>
""")))}/*148.3*/else/*148.8*/{_display_(Seq[Any](format.raw/*148.9*/("""
"""),_display_(Seq[Any](/*149.2*/agent/*149.7*/.displayName)),format.raw/*149.19*/("""
""")))})),format.raw/*150.2*/("""
""")))})),format.raw/*151.2*/("""
""")))}/*152.3*/else/*152.8*/{_display_(Seq[Any](format.raw/*152.9*/("""
"""),_display_(Seq[Any](/*153.2*/if(agent.url.isDefined)/*153.25*/ {_display_(Seq[Any](format.raw/*153.27*/("""
<a href=""""),_display_(Seq[Any](/*154.11*/agent/*154.16*/.url)),format.raw/*154.20*/("""" target="_blank">"""),_display_(Seq[Any](/*154.39*/agent/*154.44*/.url)),format.raw/*154.48*/("""</a>
""")))}/*155.3*/else/*155.8*/{_display_(Seq[Any](format.raw/*155.9*/("""
unknown
""")))})),format.raw/*157.2*/("""
""")))})),format.raw/*158.2*/("""
on """),_display_(Seq[Any](/*159.5*/dateFormatter(date))),format.raw/*159.24*/("""
""")))};def /*9.2*/dateFormatter/*9.15*/(date: java.util.Date) = {{
val formatter = new java.text.SimpleDateFormat("MMM d, yyyy")
formatter.format(date)
}};
Seq[Any](format.raw/*1.90*/("""
"""),format.raw/*8.1*/("""
"""),format.raw/*12.2*/("""

"""),format.raw/*128.2*/("""

"""),format.raw/*160.2*/("""

"""),_display_(Seq[Any](/*162.2*/if(metadata.size == 0)/*162.24*/ {_display_(Seq[Any](format.raw/*162.26*/("""
<p class="text-center">No metadata available for this resource</p>
""")))})),format.raw/*164.2*/("""
"""),_display_(Seq[Any](/*165.2*/for((m, i) <- metadata.zipWithIndex) yield /*165.38*/ {_display_(Seq[Any](format.raw/*165.40*/("""
<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading_"""),_display_(Seq[Any](/*167.56*/m/*167.57*/.id)),format.raw/*167.60*/("""">
        <a data-toggle="collapse" href="#collapse_"""),_display_(Seq[Any](/*168.52*/m/*168.53*/.id)),format.raw/*168.56*/("""" class="collapse-icon">
            <span class="panel-icon glyphicon glyphicon-minus"></span>
        </a>
                <span>
                    """),_display_(Seq[Any](/*172.22*/printHeader(m.creator, m.createdAt, m.attachedTo.id, m.id))),format.raw/*172.80*/("""
                </span>
        """),_display_(Seq[Any](/*174.10*/if(toDelete && Permission.checkPermission(Permission.DeleteMetadata, ResourceRef(ResourceRef.metadata, m.id)))/*174.120*/ {_display_(Seq[Any](format.raw/*174.122*/("""
        <a id=""""),_display_(Seq[Any](/*175.17*/m/*175.18*/.id)),format.raw/*175.21*/("""" title="Delete this metadata" class="btn btn-link delete-icon">
            <span class="panel-icon glyphicon glyphicon-trash"></span>
        </a>
        """)))})),format.raw/*178.10*/("""
    </div>
    <div id="collapse_"""),_display_(Seq[Any](/*180.24*/m/*180.25*/.id)),format.raw/*180.28*/("""" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading_"""),_display_(Seq[Any](/*180.107*/m/*180.108*/.id)),format.raw/*180.111*/("""">
        <div class="panel-body">
            <div class="tree">
                """),_display_(Seq[Any](/*183.18*/printContent(m.creator, m.content, m.contextId, m.contextURL, m.attachedTo.id))),format.raw/*183.96*/("""
            </div>
        </div>
    </div>
</div>
""")))})),format.raw/*188.2*/("""

<script src=""""),_display_(Seq[Any](/*190.15*/routes/*190.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*190.63*/("""" type="text/javascript"></script>
<script>
$(function () """),format.raw/*192.15*/("""{"""),format.raw/*192.16*/("""
    $('[data-toggle="tooltip"]').tooltip();

    $('.collapse')
            .on('shown.bs.collapse', function()"""),format.raw/*196.48*/("""{"""),format.raw/*196.49*/("""
                $(this).parent().find(".panel-icon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
            """),format.raw/*198.13*/("""}"""),format.raw/*198.14*/(""")
            .on('hidden.bs.collapse', function()"""),format.raw/*199.49*/("""{"""),format.raw/*199.50*/("""
                $(this).parent().find(".panel-icon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
            """),format.raw/*201.13*/("""}"""),format.raw/*201.14*/(""");

    $('.tree li').on('click', function (e) """),format.raw/*203.44*/("""{"""),format.raw/*203.45*/("""
        //console.log("clicked");
        var children = $(this).find('> ul > li');
        if (children.is(":visible")) """),format.raw/*206.38*/("""{"""),format.raw/*206.39*/("""
            children.hide('fast');
            $(this).find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        """),format.raw/*209.9*/("""}"""),format.raw/*209.10*/(""" else """),format.raw/*209.16*/("""{"""),format.raw/*209.17*/("""
            children.show('fast');
            $(this).find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        """),format.raw/*212.9*/("""}"""),format.raw/*212.10*/("""
        e.stopPropagation();
    """),format.raw/*214.5*/("""}"""),format.raw/*214.6*/(""");
"""),format.raw/*215.1*/("""}"""),format.raw/*215.2*/(""")

$(function () """),format.raw/*217.15*/("""{"""),format.raw/*217.16*/("""
    $('.delete-icon').unbind().on('click', function()"""),format.raw/*218.54*/("""{"""),format.raw/*218.55*/("""
        var delete_icon = $(this);

        var request = jsRoutes.api.Metadata.removeMetadata(this.id).ajax("""),format.raw/*221.74*/("""{"""),format.raw/*221.75*/("""
            type: 'DELETE'
        """),format.raw/*223.9*/("""}"""),format.raw/*223.10*/(""");

        request.done(function (response, textStatus, jqXHR) """),format.raw/*225.61*/("""{"""),format.raw/*225.62*/("""
            //console.log("success");
            delete_icon.closest(".panel").remove();
        """),format.raw/*228.9*/("""}"""),format.raw/*228.10*/(""");

        request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*230.63*/("""{"""),format.raw/*230.64*/("""
            console.error("The following error occured: " + textStatus, errorThrown);
            var errMsg = "You must be logged in to add metadata";
            if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*233.56*/("""{"""),format.raw/*233.57*/("""
                notify("Metadata was not removed due to : " + errorThrown, "error");
            """),format.raw/*235.13*/("""}"""),format.raw/*235.14*/("""
        """),format.raw/*236.9*/("""}"""),format.raw/*236.10*/(""");
    """),format.raw/*237.5*/("""}"""),format.raw/*237.6*/(""");
"""),format.raw/*238.1*/("""}"""),format.raw/*238.2*/(""")

// get metadata definitions
function getMetadataContext(mid, uuid, key) """),format.raw/*241.45*/("""{"""),format.raw/*241.46*/("""
    var request = jsRoutes.api.ContextLD.getContextById(uuid).ajax("""),format.raw/*242.68*/("""{"""),format.raw/*242.69*/("""
        type: 'GET',
        contentType: "application/json"
    """),format.raw/*245.5*/("""}"""),format.raw/*245.6*/(""");

    request.done(function (response, textStatus, jqXHR) """),format.raw/*247.57*/("""{"""),format.raw/*247.58*/("""
        var fields = response;
        var context = "Context is not defined.";
        if (fields['@context']) """),format.raw/*250.34*/("""{"""),format.raw/*250.35*/("""
            context = JSON.stringify(fields['@context'][key]);
        """),format.raw/*252.9*/("""}"""),format.raw/*252.10*/("""
        //console.log(context);
        $("#"+mid).popover("""),format.raw/*254.28*/("""{"""),format.raw/*254.29*/("""
            content:context,
            trigger:'hover',
            placement:'top',
            template: '<div class="popover" role="tooltip" style="max-width:600px;word-break:break-all"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
        """),format.raw/*259.9*/("""}"""),format.raw/*259.10*/(""");
        $("#"+mid).popover('show');
    """),format.raw/*261.5*/("""}"""),format.raw/*261.6*/(""");

    request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*263.59*/("""{"""),format.raw/*263.60*/("""
        console.error("The following error occured: " + textStatus, errorThrown);
        var errMsg = "You must be logged in to retrieve metadata definitions";
        if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*266.52*/("""{"""),format.raw/*266.53*/("""
            notify("Metadata context was not shown due to : " + errorThrown, "error");
        """),format.raw/*268.9*/("""}"""),format.raw/*268.10*/("""
    """),format.raw/*269.5*/("""}"""),format.raw/*269.6*/(""");
"""),format.raw/*270.1*/("""}"""),format.raw/*270.2*/("""
function leaveMetadataContext(mid) """),format.raw/*271.36*/("""{"""),format.raw/*271.37*/("""
    $("#"+mid).popover('hide');
"""),format.raw/*273.1*/("""}"""),format.raw/*273.2*/("""

// get agent author definitions
function getAgentContext(aid, uuid) """),format.raw/*276.37*/("""{"""),format.raw/*276.38*/("""
    var request = jsRoutes.api.Users.findById(uuid).ajax("""),format.raw/*277.58*/("""{"""),format.raw/*277.59*/("""
        type: 'GET',
        contentType: "application/json"
    """),format.raw/*280.5*/("""}"""),format.raw/*280.6*/(""");

    request.done(function (response, textStatus, jqXHR) """),format.raw/*282.57*/("""{"""),format.raw/*282.58*/("""
        var fields = response;
        //console.log(fields['fullName']);
        $("#"+aid).popover("""),format.raw/*285.28*/("""{"""),format.raw/*285.29*/("""
            content:fields['email'],
            trigger:'hover',
            placement:'top',
            template: '<div class="popover" role="tooltip" style="max-width:600px;word-break:break-all"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
        """),format.raw/*290.9*/("""}"""),format.raw/*290.10*/(""");
        $("#"+aid).popover('show');
    """),format.raw/*292.5*/("""}"""),format.raw/*292.6*/(""");

    request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*294.59*/("""{"""),format.raw/*294.60*/("""
        console.error("The following error occured: " + textStatus, errorThrown);
        var errMsg = "You must be logged in to retrieve metadata definitions";
        if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*297.52*/("""{"""),format.raw/*297.53*/("""
            notify("Agent metadata context was not shown due to : " + errorThrown, "error");
        """),format.raw/*299.9*/("""}"""),format.raw/*299.10*/("""
    """),format.raw/*300.5*/("""}"""),format.raw/*300.6*/(""");
"""),format.raw/*301.1*/("""}"""),format.raw/*301.2*/("""
function leaveAgentContext(aid) """),format.raw/*302.33*/("""{"""),format.raw/*302.34*/("""
    $("#"+aid).popover('hide');
"""),format.raw/*304.1*/("""}"""),format.raw/*304.2*/("""
</script>"""))}
    }
    
    def render(metadata:List[models.Metadata],toDelete:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(metadata,toDelete)(user)
    
    def f:((List[models.Metadata],Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (metadata,toDelete) => (user) => apply(metadata,toDelete)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/metadatald/viewForFile.scala.html
                    HASH: d2826e1c5fcd99a14dc6b51af97b8ac62cfa218e
                    MATRIX: 647->1|998->408|1019->420|1205->524|1242->526|1257->533|1272->539|1282->542|1310->561|1350->563|1396->574|1441->603|1481->605|1522->611|1536->616|1551->622|1561->629|1589->648|1629->650|1878->863|1966->942|2006->944|2176->1078|2261->1154|2308->1163|2354->1173|2381->1191|2421->1193|2474->1210|2499->1213|2608->1286|2633->1289|2673->1293|2691->1302|2717->1306|2757->1310|2782->1313|2869->1364|2894->1367|2957->1394|2982->1397|3025->1422|3038->1427|3077->1428|3131->1446|3156->1449|3208->1469|3254->1479|3333->1536|3375->1546|3404->1557|3417->1562|3456->1563|3502->1573|3521->1583|3536->1589|3546->1600|3570->1615|3610->1617|3665->1636|3675->1637|3706->1646|3800->1704|3825->1707|3885->1731|3965->1789|3993->1808|4014->1820|4054->1822|4108->1840|4133->1843|4181->1855|4260->1912|4303->1932|4345->1942|4379->1963|4406->1981|4446->1983|4625->2126|4713->2205|4753->2207|4799->2217|4884->2293|4931->2302|4977->2312|5004->2330|5044->2332|5097->2349|5122->2352|5231->2425|5256->2428|5296->2432|5314->2441|5340->2445|5380->2449|5405->2452|5492->2503|5517->2506|5580->2533|5605->2536|5648->2561|5661->2566|5700->2567|5754->2585|5779->2588|5831->2608|5877->2618|5956->2675|5998->2685|6027->2696|6040->2701|6079->2702|6125->2712|6144->2722|6159->2728|6169->2739|6193->2754|6233->2756|6288->2775|6298->2776|6329->2785|6423->2843|6448->2846|6508->2870|6587->2927|6615->2946|6636->2958|6676->2960|6730->2978|6755->2981|6803->2993|6882->3050|6925->3070|6967->3080|7001->3101|7019->3110|7059->3112|7131->3148|7219->3227|7259->3229|7305->3239|7390->3315|7437->3324|7483->3334|7510->3352|7550->3354|7603->3371|7628->3374|7737->3447|7762->3450|7802->3454|7820->3463|7846->3467|7886->3471|7911->3474|7998->3525|8023->3528|8086->3555|8111->3558|8154->3583|8167->3588|8206->3589|8260->3607|8285->3610|8337->3630|8383->3640|8462->3697|8504->3707|8533->3718|8546->3723|8585->3724|8631->3734|8650->3744|8665->3750|8675->3761|8699->3776|8739->3778|8794->3797|8804->3798|8835->3807|8929->3865|8954->3868|9015->3892|9095->3949|9124->3968|9146->3980|9187->3982|9233->3991|9259->3994|9308->4006|9394->4074|9437->4084|9486->4106|9525->4113|9552->4122|9580->4140|9621->4142|9668->4153|9724->4192|9765->4194|9829->4221|9909->4278|9952->4289|9979->4298|10008->4317|10049->4319|10087->4321|10127->4351|10168->4353|10232->4380|10243->4381|10272->4387|10312->4390|10323->4391|10352->4397|10377->4404|10390->4409|10429->4410|10467->4412|10477->4413|10505->4419|10539->4421|10560->4424|10579->4433|10629->4436|10664->4446|10690->4451|10711->4462|10862->4531|10900->4533|10914->4538|10947->4548|10988->4553|11030->4585|11071->4587|11109->4589|11175->4645|11216->4647|11254->4649|11312->4697|11360->4706|11398->4708|11426->4726|11467->4728|11513->4738|11539->4741|11585->4750|11601->4756|11658->4790|11731->4826|11757->4829|11798->4833|11813->4838|11839->4841|11916->4881|11942->4884|11985->4890|12000->4895|12035->4907|12060->4914|12073->4919|12112->4920|12150->4922|12164->4927|12199->4939|12233->4941|12268->4944|12289->4947|12302->4952|12341->4953|12379->4955|12413->4979|12454->4981|12502->4992|12517->4997|12544->5001|12600->5020|12615->5025|12650->5037|12675->5044|12688->5049|12727->5050|12765->5052|12779->5057|12814->5069|12848->5071|12882->5073|12903->5076|12916->5081|12955->5082|12993->5084|13026->5107|13067->5109|13115->5120|13130->5125|13157->5129|13213->5148|13228->5153|13255->5157|13280->5164|13293->5169|13332->5170|13374->5180|13408->5182|13449->5187|13491->5206|13515->278|13536->291|13680->89|13707->276|13735->405|13765->4448|13795->5208|13834->5211|13866->5233|13907->5235|14008->5304|14046->5306|14099->5342|14140->5344|14267->5434|14278->5435|14304->5438|14395->5492|14406->5493|14432->5496|14622->5649|14703->5707|14774->5741|14895->5851|14937->5853|14991->5870|15002->5871|15028->5874|15219->6032|15291->6067|15302->6068|15328->6071|15445->6150|15457->6151|15484->6154|15605->6238|15706->6316|15792->6370|15845->6386|15861->6392|15926->6434|16013->6492|16043->6493|16184->6605|16214->6606|16368->6731|16398->6732|16477->6782|16507->6783|16661->6908|16691->6909|16767->6956|16797->6957|16948->7079|16978->7080|17154->7228|17184->7229|17219->7235|17249->7236|17424->7383|17454->7384|17516->7418|17545->7419|17576->7422|17605->7423|17651->7440|17681->7441|17764->7495|17794->7496|17933->7606|17963->7607|18027->7643|18057->7644|18150->7708|18180->7709|18307->7808|18337->7809|18432->7875|18462->7876|18699->8084|18729->8085|18856->8183|18886->8184|18923->8193|18953->8194|18988->8201|19017->8202|19048->8205|19077->8206|19181->8281|19211->8282|19308->8350|19338->8351|19432->8417|19461->8418|19550->8478|19580->8479|19722->8593|19752->8594|19852->8667|19882->8668|19971->8728|20001->8729|20328->9028|20358->9029|20429->9072|20458->9073|20549->9135|20579->9136|20821->9349|20851->9350|20975->9446|21005->9447|21038->9452|21067->9453|21098->9456|21127->9457|21192->9493|21222->9494|21283->9527|21312->9528|21411->9598|21441->9599|21528->9657|21558->9658|21652->9724|21681->9725|21770->9785|21800->9786|21931->9888|21961->9889|22296->10196|22326->10197|22397->10240|22426->10241|22517->10303|22547->10304|22789->10517|22819->10518|22949->10620|22979->10621|23012->10626|23041->10627|23072->10630|23101->10631|23163->10664|23193->10665|23254->10698|23283->10699
                    LINES: 20->1|33->14|33->14|35->14|36->15|36->15|36->15|36->16|36->16|36->16|38->18|38->18|38->18|39->19|39->19|39->19|39->20|39->20|39->20|45->26|45->26|45->26|47->28|47->28|47->28|48->29|48->29|48->29|49->30|49->30|50->31|50->31|50->31|50->31|50->31|50->31|50->31|51->32|51->32|52->33|52->33|53->34|53->34|53->34|54->35|54->35|55->36|56->37|56->37|57->38|58->39|58->39|58->39|59->40|59->40|59->40|59->41|59->41|59->41|60->42|60->42|60->42|60->42|60->42|61->43|61->43|62->45|62->45|62->45|63->46|63->46|63->46|63->46|64->48|65->49|67->52|67->52|67->52|72->57|72->57|72->57|73->58|73->58|73->58|74->59|74->59|74->59|75->60|75->60|76->61|76->61|76->61|76->61|76->61|76->61|76->61|77->62|77->62|78->63|78->63|79->64|79->64|79->64|80->65|80->65|81->66|82->67|82->67|83->68|84->69|84->69|84->69|85->70|85->70|85->70|85->71|85->71|85->71|86->72|86->72|86->72|86->72|86->72|87->73|87->73|88->75|88->75|88->75|89->76|89->76|89->76|89->76|90->78|91->79|93->82|93->82|93->82|95->84|95->84|95->84|96->85|96->85|96->85|97->86|97->86|97->86|98->87|98->87|99->88|99->88|99->88|99->88|99->88|99->88|99->88|100->89|100->89|101->90|101->90|102->91|102->91|102->91|103->92|103->92|104->93|105->94|105->94|106->95|107->96|107->96|107->96|108->97|108->97|108->97|108->98|108->98|108->98|109->99|109->99|109->99|109->99|109->99|110->100|110->100|111->102|111->102|111->102|111->102|111->102|111->102|111->103|112->104|114->107|116->109|118->112|118->112|118->112|120->114|120->114|120->114|121->115|121->115|122->116|124->119|124->119|124->119|125->120|125->120|125->120|126->121|126->121|126->121|126->121|126->121|126->121|127->122|127->122|127->122|128->123|128->123|128->123|129->124|130->126|130->126|130->126|130->127|131->130|131->130|133->130|134->131|134->131|134->131|135->132|135->132|135->132|136->133|136->133|136->133|137->134|137->134|137->134|138->135|138->135|138->135|140->137|140->137|140->137|140->137|140->137|141->138|141->138|141->138|141->138|141->138|142->139|142->139|142->139|142->139|142->139|143->140|143->140|143->140|144->141|144->141|144->141|145->142|147->144|148->145|148->145|148->145|149->146|149->146|149->146|150->147|150->147|150->147|150->147|150->147|150->147|151->148|151->148|151->148|152->149|152->149|152->149|153->150|154->151|155->152|155->152|155->152|156->153|156->153|156->153|157->154|157->154|157->154|157->154|157->154|157->154|158->155|158->155|158->155|160->157|161->158|162->159|162->159|163->9|163->9|167->1|168->8|169->12|171->128|173->160|175->162|175->162|175->162|177->164|178->165|178->165|178->165|180->167|180->167|180->167|181->168|181->168|181->168|185->172|185->172|187->174|187->174|187->174|188->175|188->175|188->175|191->178|193->180|193->180|193->180|193->180|193->180|193->180|196->183|196->183|201->188|203->190|203->190|203->190|205->192|205->192|209->196|209->196|211->198|211->198|212->199|212->199|214->201|214->201|216->203|216->203|219->206|219->206|222->209|222->209|222->209|222->209|225->212|225->212|227->214|227->214|228->215|228->215|230->217|230->217|231->218|231->218|234->221|234->221|236->223|236->223|238->225|238->225|241->228|241->228|243->230|243->230|246->233|246->233|248->235|248->235|249->236|249->236|250->237|250->237|251->238|251->238|254->241|254->241|255->242|255->242|258->245|258->245|260->247|260->247|263->250|263->250|265->252|265->252|267->254|267->254|272->259|272->259|274->261|274->261|276->263|276->263|279->266|279->266|281->268|281->268|282->269|282->269|283->270|283->270|284->271|284->271|286->273|286->273|289->276|289->276|290->277|290->277|293->280|293->280|295->282|295->282|298->285|298->285|303->290|303->290|305->292|305->292|307->294|307->294|310->297|310->297|312->299|312->299|313->300|313->300|314->301|314->301|315->302|315->302|317->304|317->304
                    -- GENERATED --
                */
            