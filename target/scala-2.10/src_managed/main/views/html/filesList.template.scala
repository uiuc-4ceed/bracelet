
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object filesList extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template8[List[models.File],Map[UUID, Int],String,String,Int,Option[String],Option[Dataset],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*2.2*/(filesList: List[models.File], comments: Map[UUID, Int], prev: String, next: String, limit: Int, mode: Option[String], d:Option[Dataset])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*2.175*/("""

"""),_display_(Seq[Any](/*4.2*/main("Files")/*4.15*/ {_display_(Seq[Any](format.raw/*4.17*/("""

"""),_display_(Seq[Any](/*6.2*/util/*6.6*/.masonry())),format.raw/*6.16*/("""
<script src=""""),_display_(Seq[Any](/*7.15*/routes/*7.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*7.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*8.15*/routes/*8.21*/.Assets.at("javascripts/fileListProcess.js"))),format.raw/*8.65*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*9.15*/routes/*9.21*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*9.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*10.15*/routes/*10.21*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*10.63*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*11.19*/routes/*11.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*11.67*/("""" type="text/javascript"></script>

    <div class="row">
        <div class="col-md-12">
        """),_display_(Seq[Any](/*15.10*/if(d.isDefined)/*15.25*/ {_display_(Seq[Any](format.raw/*15.27*/("""
            <h1>Files in Dataset <a href=""""),_display_(Seq[Any](/*16.44*/routes/*16.50*/.Datasets.dataset(d.get.id))),format.raw/*16.77*/("""">"""),_display_(Seq[Any](/*16.80*/d/*16.81*/.get.name)),format.raw/*16.90*/("""</a></h1>
        """)))}/*17.11*/else/*17.16*/{_display_(Seq[Any](format.raw/*17.17*/("""
            <h1>Files</h1>
        """)))})),format.raw/*19.10*/("""
        </div>
    </div>

<div class="row">
    <div class="col-md-11">
    </div>
    <div class="col-md-1">
        <div class="btn-group btn-group-sm pull-right">                                        
            <button type="button" class="btn btn-default active" href="#tile-view" id="tile-view-btn"><span class="glyphicon glyphicon-th-large"></span></button>
            <button type="button" class="btn btn-default" href="#list-view" id="list-view-btn"><span class="glyphicon glyphicon-th-list"></span></button>            
        </div>
        <script type="text/javascript" language="javascript">
	        var viewMode = '"""),_display_(Seq[Any](/*32.27*/mode/*32.31*/.getOrElse("tile"))),format.raw/*32.49*/("""';
	        $.cookie.raw = true;
	        $.cookie.json = true;
            $(function() """),format.raw/*35.26*/("""{"""),format.raw/*35.27*/("""                      	            
                $('#tile-view-btn').click(function() """),format.raw/*36.54*/("""{"""),format.raw/*36.55*/("""
                  $('#tile-view').removeClass('hidden');
                  $('#list-view').addClass('hidden');
                  $('#tile-view-btn').addClass('active');
                  $('#list-view-btn').removeClass('active');
                  viewMode = "tile";
                  updatePage();
                  $.cookie('view-mode', 'tile', """),format.raw/*43.49*/("""{"""),format.raw/*43.50*/(""" path: '/' """),format.raw/*43.61*/("""}"""),format.raw/*43.62*/(""");
                  console.log("setting tile " + $.cookie('view-mode'));
                  $('#masonry').masonry().masonry("""),format.raw/*45.51*/("""{"""),format.raw/*45.52*/("""
                      itemSelector: '.post-box',
                      columnWidth: '.post-box',
                      transitionDuration: 4
                  """),format.raw/*49.19*/("""}"""),format.raw/*49.20*/(""");
                """),format.raw/*50.17*/("""}"""),format.raw/*50.18*/(""");
                $('#list-view-btn').click(function() """),format.raw/*51.54*/("""{"""),format.raw/*51.55*/("""
                  $('#tile-view').addClass('hidden');
                  $('#list-view').removeClass('hidden');
                  $('#list-view-btn').addClass('active');
                  $('#tile-view-btn').removeClass('active');
                  viewMode = "list";
                  updatePage();
                  //Utilizing library from https://github.com/carhartl/jquery-cookie/tree/v1.4.1
                  $.cookie("view-mode", "list", """),format.raw/*59.49*/("""{"""),format.raw/*59.50*/(""" path: '/' """),format.raw/*59.61*/("""}"""),format.raw/*59.62*/(""");
                  console.log("setting list " + $.cookie('view-mode'));
                """),format.raw/*61.17*/("""}"""),format.raw/*61.18*/(""");                                 
            """),format.raw/*62.13*/("""}"""),format.raw/*62.14*/(""");
            
            $(document).ready(function() """),format.raw/*64.42*/("""{"""),format.raw/*64.43*/("""            	
            	console.log("on load, cookies are " + $.cookie('view-mode'));
                console.log("on load, viewMode is " + viewMode); 
                //Set the cookie, for the case when it is passed in by the parameter
                $.cookie("view-mode", viewMode, """),format.raw/*68.49*/("""{"""),format.raw/*68.50*/(""" path: '/' """),format.raw/*68.61*/("""}"""),format.raw/*68.62*/(""");
                if (viewMode == "list") """),format.raw/*69.41*/("""{"""),format.raw/*69.42*/("""
                    $('#tile-view').addClass('hidden');
                    $('#list-view').removeClass('hidden');
                    $('#list-view-btn').addClass('active');
                    $('#tile-view-btn').removeClass('active');                      
                """),format.raw/*74.17*/("""}"""),format.raw/*74.18*/("""
                else """),format.raw/*75.22*/("""{"""),format.raw/*75.23*/("""
                    $('#tile-view').removeClass('hidden');
                    $('#list-view').addClass('hidden');
                    $('#tile-view-btn').addClass('active');
                    $('#list-view-btn').removeClass('active');                      
                """),format.raw/*80.17*/("""}"""),format.raw/*80.18*/("""      
            	updatePage();            	
            """),format.raw/*82.13*/("""}"""),format.raw/*82.14*/(""");
            
            //Function to unify the changing of the href for the next/previous links. Called on button activation for
            //viewMode style, as well as on initial load of page.
	        function updatePage() """),format.raw/*86.32*/("""{"""),format.raw/*86.33*/("""
                    $('#nextlink').attr('href', """"),_display_(Seq[Any](/*87.51*/(routes.Files.list("a", next, limit)))),format.raw/*87.88*/("""");
                    $('#prevlink').attr('href', """"),_display_(Seq[Any](/*88.51*/(routes.Files.list("b", prev, limit)))),format.raw/*88.88*/("""");
	        """),format.raw/*89.10*/("""}"""),format.raw/*89.11*/("""
        </script>
    </div>
</div>

  <div class="row hidden" id="tile-view">
    <div class="col-md-12">
        <div id="masonry">
            """),_display_(Seq[Any](/*97.14*/filesList/*97.23*/.map/*97.27*/ { file =>_display_(Seq[Any](format.raw/*97.37*/("""
                    """),_display_(Seq[Any](/*98.22*/files/*98.27*/.tile(file, "col-lg-3 col-md-3 col-sm-3", routes.Application.index(), false))),format.raw/*98.103*/("""
            """)))})),format.raw/*99.14*/("""
        </div>
    </div>
</div>
    <div class="row hidden" id="list-view">
        <div class="col-md-12">
				"""),_display_(Seq[Any](/*105.6*/filesList/*105.15*/.map/*105.19*/ { file =>_display_(Seq[Any](format.raw/*105.29*/("""
                    """),_display_(Seq[Any](/*106.22*/files/*106.27*/.listitem(file, comments, routes.Application.index(), None, None, None, false))),format.raw/*106.105*/("""
				""")))})),format.raw/*107.6*/("""
        </div>
    </div>


<div class="row">
    <div class="col-md-12">
        <ul class="pager">
            <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
            """),_display_(Seq[Any](/*116.14*/if(d.isDefined && prev.toInt >0 ||  d.isEmpty && prev != "")/*116.74*/ {_display_(Seq[Any](format.raw/*116.76*/("""
                <li class="previous"><a class="btn btn-link" id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
            """)))})),format.raw/*118.14*/("""
            """),_display_(Seq[Any](/*119.14*/if(d.isDefined && next.contains("next") ||  d.isEmpty && next != "")/*119.82*/ {_display_(Seq[Any](format.raw/*119.84*/("""
                <li class ="next"><a class="btn btn-link" id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
            """)))})),format.raw/*121.14*/("""
        </ul>
    </div>
</div>

""")))})))}
    }
    
    def render(filesList:List[models.File],comments:Map[UUID, Int],prev:String,next:String,limit:Int,mode:Option[String],d:Option[Dataset],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(filesList,comments,prev,next,limit,mode,d)(user)
    
    def f:((List[models.File],Map[UUID, Int],String,String,Int,Option[String],Option[Dataset]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (filesList,comments,prev,next,limit,mode,d) => (user) => apply(filesList,comments,prev,next,limit,mode,d)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:30 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/filesList.scala.html
                    HASH: bb2648b0f6b9d845dafe52cf1d57076ab5fd2a09
                    MATRIX: 686->2|954->175|991->178|1012->191|1051->193|1088->196|1099->200|1130->210|1180->225|1194->231|1257->273|1341->322|1355->328|1420->372|1504->421|1518->427|1579->467|1664->516|1679->522|1743->564|1832->617|1847->623|1911->665|2046->764|2070->779|2110->781|2190->825|2205->831|2254->858|2293->861|2303->862|2334->871|2372->891|2385->896|2424->897|2493->934|3168->1573|3181->1577|3221->1595|3338->1684|3367->1685|3484->1774|3513->1775|3889->2123|3918->2124|3957->2135|3986->2136|4139->2261|4168->2262|4356->2422|4385->2423|4432->2442|4461->2443|4545->2499|4574->2500|5047->2945|5076->2946|5115->2957|5144->2958|5263->3049|5292->3050|5368->3098|5397->3099|5482->3156|5511->3157|5827->3445|5856->3446|5895->3457|5924->3458|5995->3501|6024->3502|6329->3779|6358->3780|6408->3802|6437->3803|6742->4080|6771->4081|6858->4140|6887->4141|7146->4372|7175->4373|7262->4424|7321->4461|7411->4515|7470->4552|7511->4565|7540->4566|7724->4714|7742->4723|7755->4727|7803->4737|7861->4759|7875->4764|7974->4840|8020->4854|8171->4969|8190->4978|8204->4982|8253->4992|8312->5014|8327->5019|8429->5097|8467->5103|8753->5352|8823->5412|8864->5414|9091->5608|9142->5622|9220->5690|9261->5692|9481->5879
                    LINES: 20->2|23->2|25->4|25->4|25->4|27->6|27->6|27->6|28->7|28->7|28->7|29->8|29->8|29->8|30->9|30->9|30->9|31->10|31->10|31->10|32->11|32->11|32->11|36->15|36->15|36->15|37->16|37->16|37->16|37->16|37->16|37->16|38->17|38->17|38->17|40->19|53->32|53->32|53->32|56->35|56->35|57->36|57->36|64->43|64->43|64->43|64->43|66->45|66->45|70->49|70->49|71->50|71->50|72->51|72->51|80->59|80->59|80->59|80->59|82->61|82->61|83->62|83->62|85->64|85->64|89->68|89->68|89->68|89->68|90->69|90->69|95->74|95->74|96->75|96->75|101->80|101->80|103->82|103->82|107->86|107->86|108->87|108->87|109->88|109->88|110->89|110->89|118->97|118->97|118->97|118->97|119->98|119->98|119->98|120->99|126->105|126->105|126->105|126->105|127->106|127->106|127->106|128->107|137->116|137->116|137->116|139->118|140->119|140->119|140->119|142->121
                    -- GENERATED --
                */
            