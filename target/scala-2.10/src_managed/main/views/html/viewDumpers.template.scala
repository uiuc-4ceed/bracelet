
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object viewDumpers extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit user: Option[models.User] ):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.41*/("""

"""),_display_(Seq[Any](/*3.2*/main("Data dumps")/*3.20*/ {_display_(Seq[Any](format.raw/*3.22*/("""
	<div class="page-header">
		<h1>Data dumps</h1>
	</div>
	
	<div class="row">
		<div class="col-md-2">
			<form role="form-horizontal">
			  <div class="form-group">
				<button class="btn btn-default dumpBtn btn-margins" onclick="return fileMdDump();"><span class="glyphicon glyphicon-download-alt"></span> Metadata of files</button>
				<button class="btn btn-default dumpBtn btn-margins" onclick="return dsMdDump();"><span class="glyphicon glyphicon-download-alt"></span> Metadata of datasets</button>
				<button class="btn btn-default dumpBtn btn-margins" onclick="return datasetDump();"><span class="glyphicon glyphicon-download-alt"></span> Datasets files groupings</button>
			  </div>
			</form>			
		</div>
	</div>	
	<script>
		function fileMdDump() """),format.raw/*20.25*/("""{"""),format.raw/*20.26*/("""		        
			var request = $.ajax("""),format.raw/*21.25*/("""{"""),format.raw/*21.26*/("""
				url:  """"),_display_(Seq[Any](/*22.13*/api/*22.16*/.routes.Files.dumpFilesMetadata)),format.raw/*22.47*/("""",
				type: "POST"
	     	"""),format.raw/*24.8*/("""}"""),format.raw/*24.9*/(""");
			request.done(function (response) """),format.raw/*25.37*/("""{"""),format.raw/*25.38*/(""" 
				console.log("Response: " + response);
				alert(response);
			"""),format.raw/*28.4*/("""}"""),format.raw/*28.5*/(""");
			request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*29.58*/("""{"""),format.raw/*29.59*/("""
				console.error("The following error occured: " + textStatus, errorThrown);
				alert("The metadata of the files (some or all of them) were not dumped due to : " + errorThrown);
			"""),format.raw/*32.4*/("""}"""),format.raw/*32.5*/(""");	
			return false;
		"""),format.raw/*34.3*/("""}"""),format.raw/*34.4*/("""
		
		function dsMdDump() """),format.raw/*36.23*/("""{"""),format.raw/*36.24*/("""		        
			var request = $.ajax("""),format.raw/*37.25*/("""{"""),format.raw/*37.26*/("""
				url:  """"),_display_(Seq[Any](/*38.13*/api/*38.16*/.routes.Datasets.dumpDatasetsMetadata)),format.raw/*38.53*/("""",
				type: "POST"
	     	"""),format.raw/*40.8*/("""}"""),format.raw/*40.9*/(""");
			request.done(function (response) """),format.raw/*41.37*/("""{"""),format.raw/*41.38*/(""" 
				console.log("Response: " + response);
				alert(response);
			"""),format.raw/*44.4*/("""}"""),format.raw/*44.5*/(""");
			request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*45.58*/("""{"""),format.raw/*45.59*/("""
				console.error("The following error occured: " + textStatus, errorThrown);
				alert("The metadata of the datasets (some or all of them) were not dumped due to : " + errorThrown);
			"""),format.raw/*48.4*/("""}"""),format.raw/*48.5*/(""");	
			return false;
		"""),format.raw/*50.3*/("""}"""),format.raw/*50.4*/("""
		
		function datasetDump() """),format.raw/*52.26*/("""{"""),format.raw/*52.27*/("""		        
			var request = $.ajax("""),format.raw/*53.25*/("""{"""),format.raw/*53.26*/("""
				url:  """"),_display_(Seq[Any](/*54.13*/api/*54.16*/.routes.Datasets.dumpDatasetGroupings)),format.raw/*54.53*/("""",
				type: "POST"
	     	"""),format.raw/*56.8*/("""}"""),format.raw/*56.9*/(""");
			request.done(function (response) """),format.raw/*57.37*/("""{"""),format.raw/*57.38*/(""" 
				console.log("Response: " + response);
				alert(response);
			"""),format.raw/*60.4*/("""}"""),format.raw/*60.5*/(""");
			request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*61.58*/("""{"""),format.raw/*61.59*/("""
				console.error("The following error occured: " + textStatus, errorThrown);
				alert("The file groupings of the datasets (some or all of them) were not dumped due to : " + errorThrown);
			"""),format.raw/*64.4*/("""}"""),format.raw/*64.5*/(""");	
			return false;
		"""),format.raw/*66.3*/("""}"""),format.raw/*66.4*/("""
	</script>

""")))})))}
    }
    
    def render(user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply()(user)
    
    def f:(() => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = () => (user) => apply()(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/viewDumpers.scala.html
                    HASH: 61024b12d4c55de2a6e9fe700f27850af4d5f408
                    MATRIX: 606->1|739->40|776->43|802->61|841->63|1630->824|1659->825|1722->860|1751->861|1800->874|1812->877|1865->908|1919->935|1947->936|2014->975|2043->976|2138->1044|2166->1045|2254->1105|2283->1106|2494->1290|2522->1291|2572->1314|2600->1315|2654->1341|2683->1342|2746->1377|2775->1378|2824->1391|2836->1394|2895->1431|2949->1458|2977->1459|3044->1498|3073->1499|3168->1567|3196->1568|3284->1628|3313->1629|3527->1816|3555->1817|3605->1840|3633->1841|3690->1870|3719->1871|3782->1906|3811->1907|3860->1920|3872->1923|3931->1960|3985->1987|4013->1988|4080->2027|4109->2028|4204->2096|4232->2097|4320->2157|4349->2158|4569->2351|4597->2352|4647->2375|4675->2376
                    LINES: 20->1|23->1|25->3|25->3|25->3|42->20|42->20|43->21|43->21|44->22|44->22|44->22|46->24|46->24|47->25|47->25|50->28|50->28|51->29|51->29|54->32|54->32|56->34|56->34|58->36|58->36|59->37|59->37|60->38|60->38|60->38|62->40|62->40|63->41|63->41|66->44|66->44|67->45|67->45|70->48|70->48|72->50|72->50|74->52|74->52|75->53|75->53|76->54|76->54|76->54|78->56|78->56|79->57|79->57|82->60|82->60|83->61|83->61|86->64|86->64|88->66|88->66
                    -- GENERATED --
                */
            