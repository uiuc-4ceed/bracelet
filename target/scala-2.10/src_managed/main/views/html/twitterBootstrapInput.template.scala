
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object twitterBootstrapInput extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[helper.FieldElements,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(elements: helper.FieldElements):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.34*/("""

<div class="control-group """),_display_(Seq[Any](/*3.28*/if(elements.hasErrors)/*3.50*/ {_display_(Seq[Any](format.raw/*3.52*/("""error""")))})),format.raw/*3.58*/("""">
  <label class="control-label" for=""""),_display_(Seq[Any](/*4.38*/elements/*4.46*/.id)),format.raw/*4.49*/("""">"""),_display_(Seq[Any](/*4.52*/elements/*4.60*/.label)),format.raw/*4.66*/("""</label>
  <div class="controls">
    """),_display_(Seq[Any](/*6.6*/elements/*6.14*/.input)),format.raw/*6.20*/("""
    """),_display_(Seq[Any](/*7.6*/if(elements.hasErrors)/*7.28*/ {_display_(Seq[Any](format.raw/*7.30*/("""
      <span class="help-inline">"""),_display_(Seq[Any](/*8.34*/elements/*8.42*/.errors.mkString(", "))),format.raw/*8.64*/("""</span>
    """)))})),format.raw/*9.6*/("""
  </div>
</div>"""))}
    }
    
    def render(elements:helper.FieldElements): play.api.templates.HtmlFormat.Appendable = apply(elements)
    
    def f:((helper.FieldElements) => play.api.templates.HtmlFormat.Appendable) = (elements) => apply(elements)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/twitterBootstrapInput.scala.html
                    HASH: 52cda938a8169faf3891715b0cba72eb63946d1f
                    MATRIX: 617->1|743->33|807->62|837->84|876->86|913->92|988->132|1004->140|1028->143|1066->146|1082->154|1109->160|1182->199|1198->207|1225->213|1265->219|1295->241|1334->243|1403->277|1419->285|1462->307|1505->320
                    LINES: 20->1|23->1|25->3|25->3|25->3|25->3|26->4|26->4|26->4|26->4|26->4|26->4|28->6|28->6|28->6|29->7|29->7|29->7|30->8|30->8|30->8|31->9
                    -- GENERATED --
                */
            