
package views.html.curations

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listitem extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[models.CurationObject,Option[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObject: models.CurationObject, space: Option[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.100*/("""

<div class = "panel panel-default dataset-panel" id=""""),_display_(Seq[Any](/*3.55*/curationObject/*3.69*/.id)),format.raw/*3.72*/("""-listitem">
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-2">
                <span class="bigicon glyphicon glyphicon-briefcase"></span>
            </div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <div class="row">
                            <div class="col-xs-12">
                                <span class="h2"><a href=""""),_display_(Seq[Any](/*14.60*/(routes.CurationObjects.getCurationObject(curationObject.id)))),format.raw/*14.121*/("""">"""),_display_(Seq[Any](/*14.124*/Html(curationObject.name))),format.raw/*14.149*/("""</a></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">"""),_display_(Seq[Any](/*18.53*/Html(curationObject.description))),format.raw/*18.85*/("""</div>
                        </div>
                        <div class="row top-padding">
                            <div class="col-xs-12">
                                """),_display_(Seq[Any](/*22.34*/Messages("owner.label"))),format.raw/*22.57*/(""": <a href= """"),_display_(Seq[Any](/*22.70*/routes/*22.76*/.Profile.viewProfileUUID(curationObject.author.id))),format.raw/*22.126*/(""""> """),_display_(Seq[Any](/*22.130*/curationObject/*22.144*/.author.fullName)),format.raw/*22.160*/(""" </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">Created on: """),_display_(Seq[Any](/*26.65*/curationObject/*26.79*/.created.date.format("MMM dd, yyyy"))),format.raw/*26.115*/("""</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">Repository: """),_display_(Seq[Any](/*29.65*/curationObject/*29.79*/.repository.getOrElse(""))),format.raw/*29.104*/("""</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">Published on:
                                """),_display_(Seq[Any](/*33.34*/{curationObject.publishedDate match {
                                    case Some(d) => d.format("MMM dd, yyyy")
                                    case None =>
                                    }
                                })),format.raw/*37.34*/("""
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">External identifier:
                                """),_display_(Seq[Any](/*42.34*/curationObject/*42.48*/.externalIdentifier/*42.67*/ match/*42.73*/ {/*43.37*/case Some(e) =>/*43.52*/ {_display_(Seq[Any](format.raw/*43.54*/("""
                                        """),_display_(Seq[Any](/*44.42*/if(e.toString.startsWith("http://") || e.toString.startsWith("https://"))/*44.115*/ {_display_(Seq[Any](format.raw/*44.117*/("""
                                            <a href="""),_display_(Seq[Any](/*45.54*/e)),format.raw/*45.55*/(""">"""),_display_(Seq[Any](/*45.57*/e)),format.raw/*45.58*/("""</a>
                                        """)))}/*46.43*/else/*46.48*/{_display_(Seq[Any](format.raw/*46.49*/("""
                                            """),_display_(Seq[Any](/*47.46*/e)),format.raw/*47.47*/("""
                                        """)))})),format.raw/*48.42*/("""
                                    """)))}/*50.37*/case None =>/*50.49*/ {}})),format.raw/*51.34*/("""
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <h5>"""),_display_(Seq[Any](/*56.38*/Messages("dataset.title"))),format.raw/*56.63*/(""": <a href =""""),_display_(Seq[Any](/*56.76*/routes/*56.82*/.Datasets.dataset(curationObject.datasets(0).id))),format.raw/*56.130*/("""">"""),_display_(Seq[Any](/*56.133*/curationObject/*56.147*/.datasets(0).name)),format.raw/*56.164*/("""</a></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4"></div>
                </div>
            </div>
        </div>
    </div>
</div>"""))}
    }
    
    def render(curationObject:models.CurationObject,space:Option[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObject,space)(user)
    
    def f:((models.CurationObject,Option[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObject,space) => (user) => apply(curationObject,space)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:41 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/curations/listitem.scala.html
                    HASH: 1656fe991ede5f23d58ff8c80807fa05646854a1
                    MATRIX: 650->1|843->99|934->155|956->169|980->172|1497->653|1581->714|1621->717|1669->742|1877->914|1931->946|2144->1123|2189->1146|2238->1159|2253->1165|2326->1215|2367->1219|2391->1233|2430->1249|2644->1427|2667->1441|2726->1477|2906->1621|2929->1635|2977->1660|3191->1838|3448->2073|3698->2287|3721->2301|3749->2320|3764->2326|3775->2365|3799->2380|3839->2382|3917->2424|4000->2497|4041->2499|4131->2553|4154->2554|4192->2556|4215->2557|4280->2604|4293->2609|4332->2610|4414->2656|4437->2657|4511->2699|4568->2774|4589->2786|4615->2823|4849->3021|4896->3046|4945->3059|4960->3065|5031->3113|5071->3116|5095->3130|5135->3147
                    LINES: 20->1|23->1|25->3|25->3|25->3|36->14|36->14|36->14|36->14|40->18|40->18|44->22|44->22|44->22|44->22|44->22|44->22|44->22|44->22|48->26|48->26|48->26|51->29|51->29|51->29|55->33|59->37|64->42|64->42|64->42|64->42|64->43|64->43|64->43|65->44|65->44|65->44|66->45|66->45|66->45|66->45|67->46|67->46|67->46|68->47|68->47|69->48|70->50|70->50|70->51|75->56|75->56|75->56|75->56|75->56|75->56|75->56|75->56
                    -- GENERATED --
                */
            