
package views.html.curations

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object newCuration extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template10[UUID,String,String,Option[ProjectSpace],List[models.ProjectSpace],Boolean,Boolean,Boolean,List[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(id: UUID, name:String, desc:String, defaultspace:Option[ProjectSpace], spaceList: List[models.ProjectSpace], isNameRequired: Boolean, isDescriptionRequired: Boolean, isNew:Boolean, creators: List[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import _root_.util.Formatters._

import api.Permission


Seq[Any](format.raw/*1.243*/("""
"""),format.raw/*5.1*/("""
"""),_display_(Seq[Any](/*6.2*/main("Curation Object")/*6.25*/ {_display_(Seq[Any](format.raw/*6.27*/("""
    <script src=""""),_display_(Seq[Any](/*7.19*/routes/*7.25*/.Assets.at("javascripts/curation-create.js"))),format.raw/*7.69*/("""" language="javascript"></script>
    <script src=""""),_display_(Seq[Any](/*8.19*/routes/*8.25*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*8.70*/("""" language="javascript"></script>
    <script type="text/javascript" language="javascript">
      //Global so that the javascript for the collection creation can reference this.
      var isNameRequired = """),_display_(Seq[Any](/*11.29*/isNameRequired)),format.raw/*11.43*/(""";
      var isDescRequired = """),_display_(Seq[Any](/*12.29*/isDescriptionRequired)),format.raw/*12.50*/(""";
      var origName = """"),_display_(Seq[Any](/*13.24*/name)),format.raw/*13.28*/("""";
      
      var origCreators = [
	  """),_display_(Seq[Any](/*16.5*/if(creators.length !=0)/*16.28*/ {_display_(Seq[Any](format.raw/*16.30*/("""
      	"""),_display_(Seq[Any](/*17.9*/for(i <- 0 to (creators.length  - 2) ) yield /*17.47*/ {_display_(Seq[Any](format.raw/*17.49*/("""
      		""""),_display_(Seq[Any](/*18.11*/creators(i)/*18.22*/.trim)),format.raw/*18.27*/("""",
  		""")))})),format.raw/*19.6*/("""
   		""""),_display_(Seq[Any](/*20.8*/creators(creators.length-1)/*20.35*/.trim)),format.raw/*20.40*/(""""
   		""")))})),format.raw/*21.7*/(""" ];
    </script>
    """),_display_(Seq[Any](/*23.6*/if(isNew)/*23.15*/ {_display_(Seq[Any](format.raw/*23.17*/("""
        <ol class="breadcrumb">
            """),_display_(Seq[Any](/*25.14*/defaultspace/*25.26*/ match/*25.32*/ {/*26.17*/case Some(sp) =>/*26.33*/ {_display_(Seq[Any](format.raw/*26.35*/("""
                   <li><span class="glyphicon glyphicon-hdd"></span> <a href=""""),_display_(Seq[Any](/*27.80*/routes/*27.86*/.Spaces.getSpace(sp.id))),format.raw/*27.109*/("""" title=""""),_display_(Seq[Any](/*27.119*/sp/*27.121*/.name)),format.raw/*27.126*/(""""> """),_display_(Seq[Any](/*27.130*/Html(ellipsize(sp.name,18)))),format.raw/*27.157*/("""</a></li>
                """)))}/*29.17*/case None =>/*29.29*/ {_display_(Seq[Any](format.raw/*29.31*/("""
                    """),_display_(Seq[Any](/*30.22*/user/*30.26*/ match/*30.32*/ {/*31.25*/case Some(u) =>/*31.40*/ {_display_(Seq[Any](format.raw/*31.42*/("""
                           <li> <span class="glyphicon glyphicon-user"></span> <a href= """"),_display_(Seq[Any](/*32.91*/routes/*32.97*/.Profile.viewProfileUUID(u.id))),format.raw/*32.127*/("""" title=""""),_display_(Seq[Any](/*32.137*/u/*32.138*/.fullName)),format.raw/*32.147*/(""""> """),_display_(Seq[Any](/*32.151*/Html(ellipsize(u.fullName, 18)))),format.raw/*32.182*/(""" </a></li>
                        """)))}/*34.25*/case None =>/*34.37*/ {}})),format.raw/*35.22*/("""
                """)))}})),format.raw/*37.14*/("""

            <li><span class="glyphicon glyphicon-globe"></span> Create New """),_display_(Seq[Any](/*39.77*/Messages("curationobject.label"))),format.raw/*39.109*/("""</li>
        </ol>
        <div class="page-header">

            <h1>Create New """),_display_(Seq[Any](/*43.29*/Messages("curationobject.label"))),format.raw/*43.61*/("""</h1>
        </div>
    """)))}/*45.7*/else/*45.12*/{_display_(Seq[Any](format.raw/*45.13*/("""
        <ol class="breadcrumb">
            """),_display_(Seq[Any](/*47.14*/defaultspace/*47.26*/ match/*47.32*/ {/*48.17*/case Some(sp) =>/*48.33*/ {_display_(Seq[Any](format.raw/*48.35*/("""
                    <li><span class="glyphicon glyphicon-hdd"></span> <a href=""""),_display_(Seq[Any](/*49.81*/routes/*49.87*/.Spaces.getSpace(sp.id))),format.raw/*49.110*/("""" title=""""),_display_(Seq[Any](/*49.120*/sp/*49.122*/.name)),format.raw/*49.127*/(""""> """),_display_(Seq[Any](/*49.131*/Html(ellipsize(sp.name, 18)))),format.raw/*49.159*/("""</a></li>
                """)))}/*51.17*/case None =>/*51.29*/ {_display_(Seq[Any](format.raw/*51.31*/("""
                    //Right now this is not used. It is a placeholder for when we have a personal space/ private space/ user space where you can publish stuff into.
                    """),_display_(Seq[Any](/*53.22*/user/*53.26*/ match/*53.32*/ {/*54.25*/case Some(u) =>/*54.40*/ {_display_(Seq[Any](format.raw/*54.42*/("""
                            <li><span class="glyphicon glyphicon-user"></span> <a href= """"),_display_(Seq[Any](/*55.91*/routes/*55.97*/.Profile.viewProfileUUID(u.id))),format.raw/*55.127*/("""" title=""""),_display_(Seq[Any](/*55.137*/u/*55.138*/.fullName)),format.raw/*55.147*/(""""> """),_display_(Seq[Any](/*55.151*/Html(ellipsize(u.fullName, 18)))),format.raw/*55.182*/(""" </a></li>
                        """)))}/*57.25*/case None =>/*57.37*/ {}})),format.raw/*58.22*/("""
                """)))}})),format.raw/*60.14*/("""
            <li><span class="glyphicon glyphicon-edit"></span> Edit """),_display_(Seq[Any](/*61.70*/Messages("curationobject.label"))),format.raw/*61.102*/("""</li>
        </ol>
        <div class="page-header">
            <h1>Edit """),_display_(Seq[Any](/*64.23*/Messages("curationobject.label"))),format.raw/*64.55*/("""</h1>
        </div>
    """)))})),format.raw/*66.6*/("""

    <div class="row">
        <div class="col-md-12">
            <fieldset id="curationObjectFieldSet">
            <div class="form-group">
                <label id="namelabel" for="name"> Name</label>
                <input type="text" class="form-control" id="name" placeholder="A short name" value=""""),_display_(Seq[Any](/*73.102*/name)),format.raw/*73.106*/("""">
                <span class="error hiddencomplete" id="nameerror">The name is a required field.</span>
            </div>
                        <div class="form-group">
			  <div id='aboutcreators'>
                <label id="creators-label" title="Ordered list of those responsible for this publication"> Creator(s): </label>
                        
            	<div id="ds_creators" class="inline">

                	
            	</div>
            				    
			  <a id="add-creator" class="hiddencomplete" href="javascript:addCreator()" title="Click to add a Creator">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </a>
				<div><span class="error 
				  """),_display_(Seq[Any](/*89.8*/if(creators.length >0 )/*89.31*/ {_display_(Seq[Any](format.raw/*89.33*/("""hiddencomplete""")))})),format.raw/*89.48*/("""
					" id="creatorerror">One or more creators are required </span></div>
			  </div>  
			</div>	                
            
            <div class="form-group">
 				<label id="desclabel" for="description"> 
                """),_display_(Seq[Any](/*96.18*/if(Messages("dataset.description").toLowerCase == Messages("publicationrequest.description").toLowerCase)/*96.123*/ {_display_(Seq[Any](format.raw/*96.125*/("""
                	"""),_display_(Seq[Any](/*97.19*/Messages("publicationrequest.description"))),format.raw/*97.61*/("""
                """)))}/*98.19*/else/*98.24*/{_display_(Seq[Any](format.raw/*98.25*/("""
                		"""),_display_(Seq[Any](/*99.20*/Messages("publicationrequest.description"))),format.raw/*99.62*/(""" ("""),_display_(Seq[Any](/*99.65*/Messages("dataset.description"))),format.raw/*99.96*/(""")
				""")))})),format.raw/*100.6*/("""                		
                </label>
                <textarea rows="4" type="text" id="description" class="form-control" >"""),_display_(Seq[Any](/*102.88*/Html(desc))),format.raw/*102.98*/("""</textarea>
                               <span class="error 
                """),_display_(Seq[Any](/*104.18*/if(desc.length >0 )/*104.37*/ {_display_(Seq[Any](format.raw/*104.39*/("""hiddencomplete""")))})),format.raw/*104.54*/("""
                " id="descerror">The abstract is a required field.</span>
            </div>

            """),_display_(Seq[Any](/*108.14*/if(!spaceList.isEmpty)/*108.36*/ {_display_(Seq[Any](format.raw/*108.38*/("""
                <div class="form-group">
                    <label id="spacelabel" for="space"> Create in """),_display_(Seq[Any](/*110.68*/Messages("space.title"))),format.raw/*110.91*/(""" </label>
                    <select  name="space" id="spaceid" class = "chosen-select">
                    """),_display_(Seq[Any](/*112.22*/defaultspace/*112.34*/ match/*112.40*/ {/*113.25*/case Some(defaultspace) =>/*113.51*/ {_display_(Seq[Any](format.raw/*113.53*/("""
                            <option value=""""),_display_(Seq[Any](/*114.45*/(defaultspace.id))),format.raw/*114.62*/("""">"""),_display_(Seq[Any](/*114.65*/(defaultspace.name))),format.raw/*114.84*/("""</option>
                            """),_display_(Seq[Any](/*115.30*/spaceList/*115.39*/.map/*115.43*/ { space =>_display_(Seq[Any](format.raw/*115.54*/("""
                                """),_display_(Seq[Any](/*116.34*/if(space.id != defaultspace.id)/*116.65*/ {_display_(Seq[Any](format.raw/*116.67*/("""
                                    <option value=""""),_display_(Seq[Any](/*117.53*/(space.id))),format.raw/*117.63*/("""">"""),_display_(Seq[Any](/*117.66*/(space.name))),format.raw/*117.78*/("""</option>
                                """)))})),format.raw/*118.34*/("""
                            """)))})),format.raw/*119.30*/("""
                        """)))}/*121.25*/case None =>/*121.37*/ {_display_(Seq[Any](format.raw/*121.39*/("""
                            <option selected disabled>Select the """),_display_(Seq[Any](/*122.67*/Messages("space.title"))),format.raw/*122.90*/("""</option>
                            """),_display_(Seq[Any](/*123.30*/spaceList/*123.39*/.map/*123.43*/ { space =>_display_(Seq[Any](format.raw/*123.54*/("""

                                <option value=""""),_display_(Seq[Any](/*125.49*/(space.id))),format.raw/*125.59*/("""">"""),_display_(Seq[Any](/*125.62*/(space.name))),format.raw/*125.74*/("""</option>

                            """)))})),format.raw/*127.30*/("""
                        """)))}})),format.raw/*129.22*/("""
                    </select>
                    <span class="error hiddencomplete" id="spaceerror"> Please select a """),_display_(Seq[Any](/*131.90*/Messages("space.title"))),format.raw/*131.113*/(""".</span><br>
                </div>

            """)))}/*134.15*/else/*134.20*/{_display_(Seq[Any](format.raw/*134.21*/("""
                <div class="form-group">
                    <label id="spacelabel" for="space"> Create in """),_display_(Seq[Any](/*136.68*/Messages("space.title"))),format.raw/*136.91*/(""" </label>
                    <select  name="space" id="spaceid" class = "chosen-select">
                        No """),_display_(Seq[Any](/*138.29*/Messages("spaces.title"))),format.raw/*138.53*/(""" available for creating a """),_display_(Seq[Any](/*138.80*/Messages("curationobject.label"))),format.raw/*138.112*/(""". Please add this """),_display_(Seq[Any](/*138.131*/Messages("dataset.title")/*138.156*/.toLowerCase)),format.raw/*138.168*/(""" to a """),_display_(Seq[Any](/*138.175*/Messages("space.title"))),format.raw/*138.198*/(""" first
                    </select>
                    <span class="error hiddencomplete" id="spaceerror"> Please select a """),_display_(Seq[Any](/*140.90*/Messages("space.title"))),format.raw/*140.113*/(""".</span><br>
                </div>
            """)))})),format.raw/*142.14*/("""
            </fieldset>
            <form id="curationcreate"  method="POST" enctype="multipart/form-data">
                    <!-- Redirect browsers with JavaScript disabled to the origin page -->
                <noscript>Javascript is required in order to create a new """),_display_(Seq[Any](/*146.76*/Messages("curationobject.label"))),format.raw/*146.108*/(""".</noscript>

                <input type="hidden" name="name" id="hiddenname" value="not set">
                <input type="hidden" name="description" id="hiddendescription" value="not set">
                <input type="hidden" name="creators" id="hiddencreators" value="not set">

            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*157.14*/if(!spaceList.isEmpty)/*157.36*/ {_display_(Seq[Any](format.raw/*157.38*/("""
                """),_display_(Seq[Any](/*158.18*/if(isNew)/*158.27*/ {_display_(Seq[Any](format.raw/*158.29*/("""
                    <button class="btn btn-primary btn-margins" title="Create the """),_display_(Seq[Any](/*159.84*/Messages("curationobject.label"))),format.raw/*159.116*/("""" onclick="return createCuration();">
                        <span class="glyphicon glyphicon-upload"></span> Create
                    </button>
                    <button class="btn btn-default btn-margins" title="Reset the input fields" onclick="return resetFields();">
                        <span class="glyphicon glyphicon-unchecked"></span> Reset
                    </button>
                """)))}/*165.19*/else/*165.24*/{_display_(Seq[Any](format.raw/*165.25*/("""
                    <button class="btn btn-default btn-margins" title="Edit """),_display_(Seq[Any](/*166.78*/Messages("curationobject.label"))),format.raw/*166.110*/("""" onclick="return updateCuration();">
                        <span class="glyphicon glyphicon-upload"></span> Update
                    </button>
                    <button class="btn btn-default btn-margins" title="cancel edit" onclick="return cancelEdit('"""),_display_(Seq[Any](/*169.114*/id)),format.raw/*169.116*/("""');">
                        <span class="glyphicon glyphicon-remove-sign"></span> Cancel
                    </button>
                """)))})),format.raw/*172.18*/("""
            """)))}/*173.15*/else/*173.20*/{_display_(Seq[Any](format.raw/*173.21*/("""
                """),_display_(Seq[Any](/*174.18*/if(isNew)/*174.27*/ {_display_(Seq[Any](format.raw/*174.29*/("""
                    <button class="btn btn-primary btn-margins disabled" title="Create the """),_display_(Seq[Any](/*175.93*/Messages("curationobject.label"))),format.raw/*175.125*/("""">
                        <span class="glyphicon glyphicon-upload"></span> Create
                    </button>
                    <button class="btn btn-default btn-margins disabled" title="Reset the input fields">
                        <span class="glyphicon glyphicon-unchecked"></span> Reset
                    </button>
                """)))}/*181.19*/else/*181.24*/{_display_(Seq[Any](format.raw/*181.25*/("""
                    <button class="btn btn-default btn-margins disabled" title="Edit """),_display_(Seq[Any](/*182.87*/Messages("curationobject.label"))),format.raw/*182.119*/("""">
                        <span class="glyphicon glyphicon-upload"></span> Update
                    </button>
                    <button class="btn btn-default btn-margins disabled" title="cancel edit">
                        <span class="glyphicon glyphicon-remove-sign"></span> Cancel
                    </button>
                """)))})),format.raw/*188.18*/("""
            """)))})),format.raw/*189.14*/("""
        </div>
    </div>

    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*193.35*/routes/*193.41*/.Assets.at("stylesheets/chosen.css"))),format.raw/*193.77*/("""">
    <script src=""""),_display_(Seq[Any](/*194.19*/routes/*194.25*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*194.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*195.19*/routes/*195.25*/.Assets.at("javascripts/creators.js"))),format.raw/*195.62*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*196.19*/routes/*196.25*/.Assets.at("javascripts/people.js"))),format.raw/*196.60*/("""" type="text/javascript"></script>
    <script language="javascript">
        $(".chosen-select").chosen("""),format.raw/*198.36*/("""{"""),format.raw/*198.37*/("""
            width: "100%"
        """),format.raw/*200.9*/("""}"""),format.raw/*200.10*/(""");
        var origDesc = $('#description').val();
        displayCreators(origCreators);
        $("#name").change( function() """),format.raw/*203.39*/("""{"""),format.raw/*203.40*/("""
            if($("#name").val().length == 0) """),format.raw/*204.46*/("""{"""),format.raw/*204.47*/("""
              $("#nameerror").show();
            """),format.raw/*206.13*/("""}"""),format.raw/*206.14*/(""" else """),format.raw/*206.20*/("""{"""),format.raw/*206.21*/("""
              $("#nameerror").hide();
            """),format.raw/*208.13*/("""}"""),format.raw/*208.14*/("""
        """),format.raw/*209.9*/("""}"""),format.raw/*209.10*/(""");
        $("#description").change( function() """),format.raw/*210.46*/("""{"""),format.raw/*210.47*/("""
            if($("#description").val().length == 0) """),format.raw/*211.53*/("""{"""),format.raw/*211.54*/("""
              $("#descerror").show();
            """),format.raw/*213.13*/("""}"""),format.raw/*213.14*/(""" else """),format.raw/*213.20*/("""{"""),format.raw/*213.21*/("""
              $("#descerror").hide();
            """),format.raw/*215.13*/("""}"""),format.raw/*215.14*/("""
        """),format.raw/*216.9*/("""}"""),format.raw/*216.10*/(""");
        setCreatorChangeCallback( function() """),format.raw/*217.46*/("""{"""),format.raw/*217.47*/("""
            if($("#ds_creators .creator").length == 0) """),format.raw/*218.56*/("""{"""),format.raw/*218.57*/("""
              $("#creatorerror").show();
            """),format.raw/*220.13*/("""}"""),format.raw/*220.14*/(""" else """),format.raw/*220.20*/("""{"""),format.raw/*220.21*/("""
              $("#creatorerror").hide();
            """),format.raw/*222.13*/("""}"""),format.raw/*222.14*/("""
        """),format.raw/*223.9*/("""}"""),format.raw/*223.10*/(""");
</script>

""")))})))}
    }
    
    def render(id:UUID,name:String,desc:String,defaultspace:Option[ProjectSpace],spaceList:List[models.ProjectSpace],isNameRequired:Boolean,isDescriptionRequired:Boolean,isNew:Boolean,creators:List[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(id,name,desc,defaultspace,spaceList,isNameRequired,isDescriptionRequired,isNew,creators)(user)
    
    def f:((UUID,String,String,Option[ProjectSpace],List[models.ProjectSpace],Boolean,Boolean,Boolean,List[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (id,name,desc,defaultspace,spaceList,isNameRequired,isDescriptionRequired,isNew,creators) => (user) => apply(id,name,desc,defaultspace,spaceList,isNameRequired,isDescriptionRequired,isNew,creators)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:41 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/curations/newCuration.scala.html
                    HASH: b88f2e7d939af5307fd3247758c322495009c20a
                    MATRIX: 720->1|1142->242|1169->330|1205->332|1236->355|1275->357|1329->376|1343->382|1408->426|1495->478|1509->484|1575->529|1817->735|1853->749|1919->779|1962->800|2023->825|2049->829|2125->870|2157->893|2197->895|2241->904|2295->942|2335->944|2382->955|2402->966|2429->971|2468->979|2511->987|2547->1014|2574->1019|2613->1027|2671->1050|2689->1059|2729->1061|2811->1107|2832->1119|2847->1125|2858->1144|2883->1160|2923->1162|3039->1242|3054->1248|3100->1271|3147->1281|3159->1283|3187->1288|3228->1292|3278->1319|3324->1363|3345->1375|3385->1377|3443->1399|3456->1403|3471->1409|3482->1436|3506->1451|3546->1453|3673->1544|3688->1550|3741->1580|3788->1590|3799->1591|3831->1600|3872->1604|3926->1635|3981->1696|4002->1708|4028->1733|4079->1765|4193->1843|4248->1875|4367->1958|4421->1990|4465->2017|4478->2022|4517->2023|4599->2069|4620->2081|4635->2087|4646->2106|4671->2122|4711->2124|4828->2205|4843->2211|4889->2234|4936->2244|4948->2246|4976->2251|5017->2255|5068->2283|5114->2327|5135->2339|5175->2341|5398->2528|5411->2532|5426->2538|5437->2565|5461->2580|5501->2582|5628->2673|5643->2679|5696->2709|5743->2719|5754->2720|5786->2729|5827->2733|5881->2764|5936->2825|5957->2837|5983->2862|6034->2894|6140->2964|6195->2996|6307->3072|6361->3104|6418->3130|6763->3438|6790->3442|7563->4180|7595->4203|7635->4205|7682->4220|7947->4449|8062->4554|8103->4556|8158->4575|8222->4617|8259->4636|8272->4641|8311->4642|8367->4662|8431->4704|8470->4707|8523->4738|8562->4745|8730->4876|8763->4886|8880->4966|8909->4985|8950->4987|8998->5002|9143->5110|9175->5132|9216->5134|9362->5243|9408->5266|9556->5377|9578->5389|9594->5395|9606->5422|9642->5448|9683->5450|9765->5495|9805->5512|9845->5515|9887->5534|9963->5573|9982->5582|9996->5586|10046->5597|10117->5631|10158->5662|10199->5664|10289->5717|10322->5727|10362->5730|10397->5742|10473->5785|10536->5815|10582->5866|10604->5878|10645->5880|10749->5947|10795->5970|10871->6009|10890->6018|10904->6022|10954->6033|11041->6083|11074->6093|11114->6096|11149->6108|11222->6148|11282->6196|11439->6316|11486->6339|11556->6390|11570->6395|11610->6396|11756->6505|11802->6528|11957->6646|12004->6670|12068->6697|12124->6729|12181->6748|12217->6773|12253->6785|12298->6792|12345->6815|12508->6941|12555->6964|12637->7013|12949->7288|13005->7320|13438->7716|13470->7738|13511->7740|13566->7758|13585->7767|13626->7769|13747->7853|13803->7885|14228->8291|14242->8296|14282->8297|14397->8375|14453->8407|14752->8668|14778->8670|14949->8808|14983->8823|14997->8828|15037->8829|15092->8847|15111->8856|15152->8858|15282->8951|15338->8983|15705->9331|15719->9336|15759->9337|15883->9424|15939->9456|16311->9795|16358->9809|16457->9871|16473->9877|16532->9913|16590->9934|16606->9940|16671->9982|16761->10035|16777->10041|16837->10078|16927->10131|16943->10137|17001->10172|17135->10277|17165->10278|17228->10313|17258->10314|17415->10442|17445->10443|17520->10489|17550->10490|17630->10541|17660->10542|17695->10548|17725->10549|17805->10600|17835->10601|17872->10610|17902->10611|17979->10659|18009->10660|18091->10713|18121->10714|18201->10765|18231->10766|18266->10772|18296->10773|18376->10824|18406->10825|18443->10834|18473->10835|18550->10883|18580->10884|18665->10940|18695->10941|18778->10995|18808->10996|18843->11002|18873->11003|18956->11057|18986->11058|19023->11067|19053->11068
                    LINES: 20->1|28->1|29->5|30->6|30->6|30->6|31->7|31->7|31->7|32->8|32->8|32->8|35->11|35->11|36->12|36->12|37->13|37->13|40->16|40->16|40->16|41->17|41->17|41->17|42->18|42->18|42->18|43->19|44->20|44->20|44->20|45->21|47->23|47->23|47->23|49->25|49->25|49->25|49->26|49->26|49->26|50->27|50->27|50->27|50->27|50->27|50->27|50->27|50->27|51->29|51->29|51->29|52->30|52->30|52->30|52->31|52->31|52->31|53->32|53->32|53->32|53->32|53->32|53->32|53->32|53->32|54->34|54->34|54->35|55->37|57->39|57->39|61->43|61->43|63->45|63->45|63->45|65->47|65->47|65->47|65->48|65->48|65->48|66->49|66->49|66->49|66->49|66->49|66->49|66->49|66->49|67->51|67->51|67->51|69->53|69->53|69->53|69->54|69->54|69->54|70->55|70->55|70->55|70->55|70->55|70->55|70->55|70->55|71->57|71->57|71->58|72->60|73->61|73->61|76->64|76->64|78->66|85->73|85->73|101->89|101->89|101->89|101->89|108->96|108->96|108->96|109->97|109->97|110->98|110->98|110->98|111->99|111->99|111->99|111->99|112->100|114->102|114->102|116->104|116->104|116->104|116->104|120->108|120->108|120->108|122->110|122->110|124->112|124->112|124->112|124->113|124->113|124->113|125->114|125->114|125->114|125->114|126->115|126->115|126->115|126->115|127->116|127->116|127->116|128->117|128->117|128->117|128->117|129->118|130->119|131->121|131->121|131->121|132->122|132->122|133->123|133->123|133->123|133->123|135->125|135->125|135->125|135->125|137->127|138->129|140->131|140->131|143->134|143->134|143->134|145->136|145->136|147->138|147->138|147->138|147->138|147->138|147->138|147->138|147->138|147->138|149->140|149->140|151->142|155->146|155->146|166->157|166->157|166->157|167->158|167->158|167->158|168->159|168->159|174->165|174->165|174->165|175->166|175->166|178->169|178->169|181->172|182->173|182->173|182->173|183->174|183->174|183->174|184->175|184->175|190->181|190->181|190->181|191->182|191->182|197->188|198->189|202->193|202->193|202->193|203->194|203->194|203->194|204->195|204->195|204->195|205->196|205->196|205->196|207->198|207->198|209->200|209->200|212->203|212->203|213->204|213->204|215->206|215->206|215->206|215->206|217->208|217->208|218->209|218->209|219->210|219->210|220->211|220->211|222->213|222->213|222->213|222->213|224->215|224->215|225->216|225->216|226->217|226->217|227->218|227->218|229->220|229->220|229->220|229->220|231->222|231->222|232->223|232->223
                    -- GENERATED --
                */
            