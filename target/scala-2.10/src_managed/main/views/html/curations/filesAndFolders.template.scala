
package views.html.curations

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object filesAndFolders extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template9[CurationObject,Option[String],List[CurationFolder],List[CurationFolder],Int,Boolean,List[CurationFile],List[models.Metadata],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curation: CurationObject, currentFolder: Option[String], foldersList: List[CurationFolder], folderHierarchy: List[CurationFolder], filepage: Int, next: Boolean, files:List[CurationFile], m: List[models.Metadata])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.251*/("""
"""),format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*4.2*/if(files.size > 0 ||folderHierarchy.length > 0 || foldersList.size>0)/*4.71*/{_display_(Seq[Any](format.raw/*4.72*/("""
    <h2> Files</h2>
""")))})),format.raw/*6.2*/("""

"""),_display_(Seq[Any](/*8.2*/if(folderHierarchy.length > 0 )/*8.33*/ {_display_(Seq[Any](format.raw/*8.35*/("""
  <h2> <a href="javascript:updatePageAndFolder(0, '')"><span class="glyphicon glyphicon-home"></span></a>
    """),_display_(Seq[Any](/*10.6*/folderHierarchy/*10.21*/.map/*10.25*/ { cFolder =>_display_(Seq[Any](format.raw/*10.38*/("""
    > <a href="javascript:updatePageAndFolder(0, '"""),_display_(Seq[Any](/*11.52*/cFolder/*11.59*/.id.stringify)),format.raw/*11.72*/("""')"> """),_display_(Seq[Any](/*11.78*/cFolder/*11.85*/.displayName)),format.raw/*11.97*/("""</a>
    """)))})),format.raw/*12.6*/("""
  </h2>
""")))})),format.raw/*14.2*/("""

<div id="folderListDiv">
"""),_display_(Seq[Any](/*17.2*/foldersList/*17.13*/.map/*17.17*/ { folder =>_display_(Seq[Any](format.raw/*17.29*/("""
        """),_display_(Seq[Any](/*18.10*/if(currentFolder.isDefined)/*18.37*/ {_display_(Seq[Any](format.raw/*18.39*/("""
            """),_display_(Seq[Any](/*19.14*/curations/*19.23*/.listFolder(folder, UUID(currentFolder.getOrElse("")), curation.id, curation.status =="In Preparation"))),format.raw/*19.126*/("""
        """)))}/*20.11*/else/*20.16*/{_display_(Seq[Any](format.raw/*20.17*/("""
            """),_display_(Seq[Any](/*21.14*/curations/*21.23*/.listFolder(folder, curation.id, curation.id, curation.status =="In Preparation"))),format.raw/*21.104*/("""
        """)))})),format.raw/*22.10*/("""
""")))})),format.raw/*23.2*/("""
</div>


<div>
<ul style = "list-style-type: none">
  """),_display_(Seq[Any](/*29.4*/for(file<- files) yield /*29.21*/ {_display_(Seq[Any](format.raw/*29.23*/("""

    <li id=""""),_display_(Seq[Any](/*31.14*/file/*31.18*/.id)),format.raw/*31.21*/("""-listitem">
      <h4><a href="""),_display_(Seq[Any](/*32.20*/routes/*32.26*/.Files.file(file.fileId))),format.raw/*32.50*/(""">"""),_display_(Seq[Any](/*32.52*/file/*32.56*/.filename)),format.raw/*32.65*/("""</a>
        """),_display_(Seq[Any](/*33.10*/if(curation.status =="In Preparation" && Permission.checkPermission(Permission.EditStagingArea, ResourceRef(ResourceRef.curationObject, curation.id)))/*33.160*/ {_display_(Seq[Any](format.raw/*33.162*/("""
            """),_display_(Seq[Any](/*34.14*/if(currentFolder.isDefined)/*34.41*/{_display_(Seq[Any](format.raw/*34.42*/("""
              <a id="file-"""),_display_(Seq[Any](/*35.28*/file/*35.32*/.id)),format.raw/*35.35*/("""" title="Remove file from """),_display_(Seq[Any](/*35.62*/Messages("curationobject.label"))),format.raw/*35.94*/("""" class="btn btn-link delete-file"
              onclick="confirmDeleteResource('curation file','following file from this """),_display_(Seq[Any](/*36.89*/Messages("curationobject.label"))),format.raw/*36.121*/("""','"""),_display_(Seq[Any](/*36.125*/(file.id))),format.raw/*36.134*/("""','"""),_display_(Seq[Any](/*36.138*/(file.filename.replace("'","&#39;")))),format.raw/*36.174*/("""', '"""),_display_(Seq[Any](/*36.179*/currentFolder/*36.192*/.get)),format.raw/*36.196*/("""', '"""),_display_(Seq[Any](/*36.201*/curation/*36.209*/.id)),format.raw/*36.212*/("""')">
                <span class="glyphicon glyphicon-trash"></span>
              </a>
            """)))}/*39.15*/else/*39.20*/{_display_(Seq[Any](format.raw/*39.21*/("""
                <a id="file-"""),_display_(Seq[Any](/*40.30*/file/*40.34*/.id)),format.raw/*40.37*/("""" title="Remove file from """),_display_(Seq[Any](/*40.64*/Messages("curationobject.label"))),format.raw/*40.96*/("""" class="btn btn-link delete-file"
                onclick="confirmDeleteResource('curation file','following file from this """),_display_(Seq[Any](/*41.91*/Messages("curationobject.label"))),format.raw/*41.123*/("""','"""),_display_(Seq[Any](/*41.127*/(file.id))),format.raw/*41.136*/("""','"""),_display_(Seq[Any](/*41.140*/(file.filename.replace("'","&#39;")))),format.raw/*41.176*/("""', '"""),_display_(Seq[Any](/*41.181*/curation/*41.189*/.id)),format.raw/*41.192*/("""', '"""),_display_(Seq[Any](/*41.197*/curation/*41.205*/.id)),format.raw/*41.208*/("""')">
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
            """)))})),format.raw/*44.14*/("""
        """)))})),format.raw/*45.10*/("""
      </h4>
      <div id ='allMetadata_"""),_display_(Seq[Any](/*47.30*/(file.id))),format.raw/*47.39*/("""'>
        <button class="btn btn-link" type="button">Show file metadata in request</button>
        <div style="display:none;">
          <div class="row border-top">
            <div class="col-md-12">
              """),_display_(Seq[Any](/*52.16*/if(curation.status =="In Preparation" && Permission.checkPermission(Permission.AddMetadata, ResourceRef(ResourceRef.curationObject, curation.id)))/*52.162*/ {_display_(Seq[Any](format.raw/*52.164*/("""
                <div class="row">
                  <div class="col-md-12">
                  """),_display_(Seq[Any](/*55.20*/metadatald/*55.30*/.addMetadata("curationFile", file.id.toString, "metadata-content-"+file.id.stringify))),format.raw/*55.115*/("""
                  </div>
                </div>
              """)))})),format.raw/*58.16*/("""
            <div class="row">
              <div class="col-md-12" id="metadata-content-"""),_display_(Seq[Any](/*60.60*/file/*60.64*/.id.stringify)),format.raw/*60.77*/("""">
                """),_display_(Seq[Any](/*61.18*/if(curation.status =="In Preparation")/*61.56*/ {_display_(Seq[Any](format.raw/*61.58*/("""
                    """),_display_(Seq[Any](/*62.22*/metadatald/*62.32*/.view(m.filter(_.attachedTo.id == file.id), true))),format.raw/*62.81*/("""
                """)))}/*63.19*/else/*63.24*/{_display_(Seq[Any](format.raw/*63.25*/("""
                    """),_display_(Seq[Any](/*64.22*/metadatald/*64.32*/.view(m.filter(_.attachedTo.id == file.id), false))),format.raw/*64.82*/("""
                """)))})),format.raw/*65.18*/("""
              </div>
            </div>
            </div>
          </div>

        </div>
      </div>
    </li>

    <script>
                    $("#allMetadata_"""),_display_(Seq[Any](/*76.38*/(file.id.toString))),format.raw/*76.56*/(""" > button").on('click', function()"""),format.raw/*76.90*/("""{"""),format.raw/*76.91*/("""
                        if($("#allMetadata_"""),_display_(Seq[Any](/*77.45*/(file.id.toString))),format.raw/*77.63*/(""" > div").attr('style') == 'display:none;')"""),format.raw/*77.105*/("""{"""),format.raw/*77.106*/("""
                            $("#allMetadata_"""),_display_(Seq[Any](/*78.46*/(file.id.toString))),format.raw/*78.64*/(""" > div").attr('style', 'display:block;');
                            $("#allMetadata_"""),_display_(Seq[Any](/*79.46*/(file.id.toString))),format.raw/*79.64*/(""" > button").get(0).innerHTML = "Hide file metadata";
                        """),format.raw/*80.25*/("""}"""),format.raw/*80.26*/("""
                        else"""),format.raw/*81.29*/("""{"""),format.raw/*81.30*/("""
                            $("#allMetadata_"""),_display_(Seq[Any](/*82.46*/(file.id.toString))),format.raw/*82.64*/(""" > div").attr('style', 'display:none;');
                            $("#allMetadata_"""),_display_(Seq[Any](/*83.46*/(file.id.toString))),format.raw/*83.64*/(""" > button").get(0).innerHTML = "Show file metadata in request";
                        """),format.raw/*84.25*/("""}"""),format.raw/*84.26*/("""
                    """),format.raw/*85.21*/("""}"""),format.raw/*85.22*/(""");

    </script>
  """)))})),format.raw/*88.4*/("""
</ul>
  </div>


<div class="row">
  <div class="col-md-12">
    <ul class="pager">
        <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
      """),_display_(Seq[Any](/*97.8*/if(filepage > 0)/*97.24*/ {_display_(Seq[Any](format.raw/*97.26*/("""
        <li class="previous"><a id="prevlink" title="Page backwards" href="javascript:updatePageAndFolder("""),_display_(Seq[Any](/*98.108*/(filepage-1))),format.raw/*98.120*/(""", '"""),_display_(Seq[Any](/*98.124*/currentFolder)),format.raw/*98.137*/("""')"><span class="glyphicon glyphicon-chevron-left"></span>Previous</a></li>
      """)))})),format.raw/*99.8*/("""
      """),_display_(Seq[Any](/*100.8*/if(next)/*100.16*/ {_display_(Seq[Any](format.raw/*100.18*/("""
        <li class ="next"><a id="nextlink" title="Page forwards"  href="javascript:updatePageAndFolder("""),_display_(Seq[Any](/*101.105*/(filepage+1))),format.raw/*101.117*/(""",'"""),_display_(Seq[Any](/*101.120*/currentFolder)),format.raw/*101.133*/("""')">Next<span class="glyphicon glyphicon-chevron-right"></span></a></li>
      """)))})),format.raw/*102.8*/("""
    </ul>
  </div>
</div>
"""))}
    }
    
    def render(curation:CurationObject,currentFolder:Option[String],foldersList:List[CurationFolder],folderHierarchy:List[CurationFolder],filepage:Int,next:Boolean,files:List[CurationFile],m:List[models.Metadata],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curation,currentFolder,foldersList,folderHierarchy,filepage,next,files,m)(user)
    
    def f:((CurationObject,Option[String],List[CurationFolder],List[CurationFolder],Int,Boolean,List[CurationFile],List[models.Metadata]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curation,currentFolder,foldersList,folderHierarchy,filepage,next,files,m) => (user) => apply(curation,currentFolder,foldersList,folderHierarchy,filepage,next,files,m)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:41 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/curations/filesAndFolders.scala.html
                    HASH: 26f184d6d45c747399a95b7b054a26c6db48784b
                    MATRIX: 745->1|1111->250|1138->274|1174->276|1251->345|1289->346|1341->368|1378->371|1417->402|1456->404|1603->516|1627->531|1640->535|1691->548|1779->600|1795->607|1830->620|1872->626|1888->633|1922->645|1963->655|2004->665|2067->693|2087->704|2100->708|2150->720|2196->730|2232->757|2272->759|2322->773|2340->782|2466->885|2495->896|2508->901|2547->902|2597->916|2615->925|2719->1006|2761->1016|2794->1018|2885->1074|2918->1091|2958->1093|3009->1108|3022->1112|3047->1115|3114->1146|3129->1152|3175->1176|3213->1178|3226->1182|3257->1191|3307->1205|3467->1355|3508->1357|3558->1371|3594->1398|3633->1399|3697->1427|3710->1431|3735->1434|3798->1461|3852->1493|4011->1616|4066->1648|4107->1652|4139->1661|4180->1665|4239->1701|4281->1706|4304->1719|4331->1723|4373->1728|4391->1736|4417->1739|4537->1841|4550->1846|4589->1847|4655->1877|4668->1881|4693->1884|4756->1911|4810->1943|4971->2068|5026->2100|5067->2104|5099->2113|5140->2117|5199->2153|5241->2158|5259->2166|5285->2169|5327->2174|5345->2182|5371->2185|5510->2292|5552->2302|5630->2344|5661->2353|5916->2572|6072->2718|6113->2720|6245->2816|6264->2826|6372->2911|6468->2975|6594->3065|6607->3069|6642->3082|6698->3102|6745->3140|6785->3142|6843->3164|6862->3174|6933->3223|6970->3242|6983->3247|7022->3248|7080->3270|7099->3280|7171->3330|7221->3348|7424->3515|7464->3533|7526->3567|7555->3568|7636->3613|7676->3631|7747->3673|7777->3674|7859->3720|7899->3738|8022->3825|8062->3843|8167->3920|8196->3921|8253->3950|8282->3951|8364->3997|8404->4015|8526->4101|8566->4119|8682->4207|8711->4208|8760->4229|8789->4230|8841->4251|9098->4473|9123->4489|9163->4491|9308->4599|9343->4611|9384->4615|9420->4628|9534->4711|9578->4719|9596->4727|9637->4729|9780->4834|9816->4846|9857->4849|9894->4862|10006->4942
                    LINES: 20->1|24->1|25->3|26->4|26->4|26->4|28->6|30->8|30->8|30->8|32->10|32->10|32->10|32->10|33->11|33->11|33->11|33->11|33->11|33->11|34->12|36->14|39->17|39->17|39->17|39->17|40->18|40->18|40->18|41->19|41->19|41->19|42->20|42->20|42->20|43->21|43->21|43->21|44->22|45->23|51->29|51->29|51->29|53->31|53->31|53->31|54->32|54->32|54->32|54->32|54->32|54->32|55->33|55->33|55->33|56->34|56->34|56->34|57->35|57->35|57->35|57->35|57->35|58->36|58->36|58->36|58->36|58->36|58->36|58->36|58->36|58->36|58->36|58->36|58->36|61->39|61->39|61->39|62->40|62->40|62->40|62->40|62->40|63->41|63->41|63->41|63->41|63->41|63->41|63->41|63->41|63->41|63->41|63->41|63->41|66->44|67->45|69->47|69->47|74->52|74->52|74->52|77->55|77->55|77->55|80->58|82->60|82->60|82->60|83->61|83->61|83->61|84->62|84->62|84->62|85->63|85->63|85->63|86->64|86->64|86->64|87->65|98->76|98->76|98->76|98->76|99->77|99->77|99->77|99->77|100->78|100->78|101->79|101->79|102->80|102->80|103->81|103->81|104->82|104->82|105->83|105->83|106->84|106->84|107->85|107->85|110->88|119->97|119->97|119->97|120->98|120->98|120->98|120->98|121->99|122->100|122->100|122->100|123->101|123->101|123->101|123->101|124->102
                    -- GENERATED --
                */
            