
package views.html.curations

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[models.CurationObject,Option[String],String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObject: models.CurationObject, space: Option[String], classes:String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.116*/("""

<div class="post-box """),_display_(Seq[Any](/*3.23*/classes)),format.raw/*3.30*/("""" id=""""),_display_(Seq[Any](/*3.37*/curationObject/*3.51*/.id)),format.raw/*3.54*/("""-tile">
    <div class="panel panel-default dataset-panel">
        <div class="pull-left">
            <span class="glyphicon glyphicon-briefcase"></span>
        </div>
        <div class="panel-body">
            <div class="caption break-word">
                <h4><a href=""""),_display_(Seq[Any](/*10.31*/(routes.CurationObjects.getCurationObject(curationObject.id)))),format.raw/*10.92*/("""">"""),_display_(Seq[Any](/*10.95*/Html(curationObject.name))),format.raw/*10.120*/("""</a></h4>
                <p class='abstractsummary'>"""),_display_(Seq[Any](/*11.45*/Html(curationObject.description))),format.raw/*11.77*/("""<br/><br/>
                    Creator: """),_display_(Seq[Any](/*12.31*/curationObject/*12.45*/.author.fullName)),format.raw/*12.61*/(""" <br/>
                    Created on: """),_display_(Seq[Any](/*13.34*/curationObject/*13.48*/.created.date.format("MMM dd, yyyy"))),format.raw/*13.84*/(""" <br/>
                    Repository: """),_display_(Seq[Any](/*14.34*/curationObject/*14.48*/.repository.getOrElse(""))),format.raw/*14.73*/(""" <br/>
                    Published on: """),_display_(Seq[Any](/*15.36*/{curationObject.publishedDate match {
                            case Some(d) => d.format("MMM dd, yyyy")
                            case None =>
                        }
                    })),format.raw/*19.22*/("""<br/>
                    External identifier: """),_display_(Seq[Any](/*20.43*/curationObject/*20.57*/.externalIdentifier/*20.76*/ match/*20.82*/ {/*21.25*/case Some(e) =>/*21.40*/ {_display_(Seq[Any](format.raw/*21.42*/("""
                            """),_display_(Seq[Any](/*22.30*/if(e.toString.startsWith("http://") || e.toString.startsWith("https://"))/*22.103*/ {_display_(Seq[Any](format.raw/*22.105*/("""
                                <a href="""),_display_(Seq[Any](/*23.42*/e)),format.raw/*23.43*/(""">"""),_display_(Seq[Any](/*23.45*/e)),format.raw/*23.46*/("""</a>
                            """)))}/*24.31*/else/*24.36*/{_display_(Seq[Any](format.raw/*24.37*/("""
                                """),_display_(Seq[Any](/*25.34*/e)),format.raw/*25.35*/("""
                            """)))})),format.raw/*26.30*/("""
                        """)))}/*28.25*/case None =>/*28.37*/ {}})),format.raw/*29.22*/("""<br/>
                    <h5>"""),_display_(Seq[Any](/*30.26*/Messages("dataset.title"))),format.raw/*30.51*/(""" <a href =""""),_display_(Seq[Any](/*30.63*/routes/*30.69*/.Datasets.dataset(curationObject.datasets(0).id))),format.raw/*30.117*/("""">"""),_display_(Seq[Any](/*30.120*/curationObject/*30.134*/.datasets(0).name)),format.raw/*30.151*/("""</a></h5>
                </p>
            </div>
        </div>
        <ul class="list-group">
            <li class="list-group-item dataset-panel-footer">
                <span class="glyphicon glyphicon-folder-close" title=""""),_display_(Seq[Any](/*36.72*/curationObject/*36.86*/.folders.size)),format.raw/*36.99*/(""" folders"></span> """),_display_(Seq[Any](/*36.118*/curationObject/*36.132*/.folders.size)),format.raw/*36.145*/("""
                <span class="glyphicon glyphicon-file" title=""""),_display_(Seq[Any](/*37.64*/curationObject/*37.78*/.files.size)),format.raw/*37.89*/(""" files"></span> """),_display_(Seq[Any](/*37.106*/curationObject/*37.120*/.files.size)),format.raw/*37.131*/("""
                <span class="glyphicon glyphicon-list" title=""""),_display_(Seq[Any](/*38.64*/(curationObject.metadataCount))),format.raw/*38.94*/(""" metadata fields"></span> """),_display_(Seq[Any](/*38.121*/(curationObject.metadataCount))),format.raw/*38.151*/("""
            </li>
        </ul>
    </div>
</div>"""))}
    }
    
    def render(curationObject:models.CurationObject,space:Option[String],classes:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObject,space,classes)(user)
    
    def f:((models.CurationObject,Option[String],String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObject,space,classes) => (user) => apply(curationObject,space,classes)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:41 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/curations/tile.scala.html
                    HASH: c70fdbe424f418c12fec196a87a5e97f17551839
                    MATRIX: 653->1|862->115|921->139|949->146|991->153|1013->167|1037->170|1352->449|1435->510|1474->513|1522->538|1612->592|1666->624|1743->665|1766->679|1804->695|1880->735|1903->749|1961->785|2037->825|2060->839|2107->864|2185->906|2402->1101|2486->1149|2509->1163|2537->1182|2552->1188|2563->1215|2587->1230|2627->1232|2693->1262|2776->1335|2817->1337|2895->1379|2918->1380|2956->1382|2979->1383|3032->1418|3045->1423|3084->1424|3154->1458|3177->1459|3239->1489|3284->1540|3305->1552|3331->1577|3398->1608|3445->1633|3493->1645|3508->1651|3579->1699|3619->1702|3643->1716|3683->1733|3949->1963|3972->1977|4007->1990|4063->2009|4087->2023|4123->2036|4223->2100|4246->2114|4279->2125|4333->2142|4357->2156|4391->2167|4491->2231|4543->2261|4607->2288|4660->2318
                    LINES: 20->1|23->1|25->3|25->3|25->3|25->3|25->3|32->10|32->10|32->10|32->10|33->11|33->11|34->12|34->12|34->12|35->13|35->13|35->13|36->14|36->14|36->14|37->15|41->19|42->20|42->20|42->20|42->20|42->21|42->21|42->21|43->22|43->22|43->22|44->23|44->23|44->23|44->23|45->24|45->24|45->24|46->25|46->25|47->26|48->28|48->28|48->29|49->30|49->30|49->30|49->30|49->30|49->30|49->30|49->30|55->36|55->36|55->36|55->36|55->36|55->36|56->37|56->37|56->37|56->37|56->37|56->37|57->38|57->38|57->38|57->38
                    -- GENERATED --
                */
            