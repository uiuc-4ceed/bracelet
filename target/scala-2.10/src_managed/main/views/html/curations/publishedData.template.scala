
package views.html.curations

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object publishedData extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[List[play.api.libs.json.JsValue],String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(publishedData: List[play.api.libs.json.JsValue], servicesUrl: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.libs.json._


Seq[Any](format.raw/*1.108*/("""
"""),format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*4.2*/main("Published Data")/*4.24*/ {_display_(Seq[Any](format.raw/*4.26*/("""
  <div class="row">
    <div class="col-md-12">
      <h1>"""),_display_(Seq[Any](/*7.12*/Html("Published Data"))),format.raw/*7.34*/("""</h1>
    </div>
  </div>
  <div>
  """),_display_(Seq[Any](/*11.4*/curations/*11.13*/.publishedGrid(publishedData, servicesUrl, None))),format.raw/*11.61*/("""
  </div>

""")))})))}
    }
    
    def render(publishedData:List[play.api.libs.json.JsValue],servicesUrl:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(publishedData,servicesUrl)(user)
    
    def f:((List[play.api.libs.json.JsValue],String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (publishedData,servicesUrl) => (user) => apply(publishedData,servicesUrl)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:41 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/curations/publishedData.scala.html
                    HASH: 827cdeb3d4866a97697fdf3294eb40e62eae151c
                    MATRIX: 658->1|887->107|914->137|950->139|980->161|1019->163|1114->223|1157->245|1229->282|1247->291|1317->339
                    LINES: 20->1|24->1|25->3|26->4|26->4|26->4|29->7|29->7|33->11|33->11|33->11
                    -- GENERATED --
                */
            