
package views.html.curations

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object publishedGrid extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[List[play.api.libs.json.JsValue],String,Option[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(publishedData: List[play.api.libs.json.JsValue], servicesUrl: String, query: Option[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.libs.json._


Seq[Any](format.raw/*1.131*/("""
"""),format.raw/*3.1*/("""  
<div  class="pubquery col-md-12"><div class="col-md-1">Filter by:</div><div class="col-md-5" id ="filters"></div><div class="col-md-6" id="legend"></div></div>

  <div id="pubs" class="grid col-md-12">
    """),_display_(Seq[Any](/*7.6*/for(pd <- publishedData) yield /*7.30*/ {_display_(Seq[Any](format.raw/*7.32*/("""
          """),_display_(Seq[Any](/*8.12*/pd/*8.14*/ match/*8.20*/ {/*9.7*/case jo:JsObject =>/*9.26*/ {_display_(Seq[Any](format.raw/*9.28*/("""
	    		<div class="grid-item col-lg-5 col-md-12 col-sm-12" data-Creator=""""),_display_(Seq[Any](/*10.75*/(jo \ "Creator")/*10.91*/ match/*10.97*/ {/*11.11*/case creator:JsString =>/*11.35*/ {_display_(Seq[Any](_display_(Seq[Any](/*11.38*/((jo \ "CreatorName").as[String].trim)))))}/*12.11*/case creators:JsArray =>/*12.35*/ {_display_(Seq[Any](_display_(Seq[Any](/*12.38*/(((jo \ "CreatorName"))(0).as[String].trim))),_display_(Seq[Any](/*12.82*/for(i <- 1 to (creators.value.size-1)) yield /*12.120*/{_display_(Seq[Any](format.raw/*12.121*/(""", """),_display_(Seq[Any](/*12.124*/(((jo \ "CreatorName"))(i).as[String].trim)))))}))))}})),format.raw/*12.170*/(""""
          data-space=""""),_display_(Seq[Any](/*13.24*/((jo \ "Publishing Project Name").asOpt[String] match {
           case Some(name) => name
           case None => (jo \ "Publishing Project").as[String]
           }))),format.raw/*16.14*/("""" 
           data-year=""""),_display_(Seq[Any](/*17.24*/defining(".*[\\s]20([0-9][0-9])[\\s].*".r)/*17.66*/ {findyear =>_display_(Seq[Any](format.raw/*17.79*/(""" """),_display_(Seq[Any](/*17.81*/((jo \ "Publication Date").as[String] match {
           	case findyear(year) => { "20" + year}
           	case _ => "unknown"
           	})))))})),format.raw/*20.16*/(""""
           	
           	data-type=""""),_display_(Seq[Any](/*22.25*/if((jo \ "DOI").as[String].contains("10.5072"))/*22.72*/ {_display_(Seq[Any](format.raw/*22.74*/("""Testing Only""")))}/*22.88*/else/*22.93*/{_display_(Seq[Any](format.raw/*22.94*/("""Production""")))})),format.raw/*22.105*/(""""
           		>
    	  	<div class="panel panel-default pub-panel">
        	<div class="panel-body">
          	
            <p><b>Title:</b> """),_display_(Seq[Any](/*27.31*/((jo \ "Title").as[String]))),format.raw/*27.58*/("""</p>
            <p><b>Persistent ID:</b> <a style="word-break:break-all" href=""""),_display_(Seq[Any](/*28.77*/((jo \ "DOI").as[String]))),format.raw/*28.102*/("""" target="_blank">"""),_display_(Seq[Any](/*28.121*/((jo \ "DOI").as[String]))),format.raw/*28.146*/("""</a></p>
            
          <p><b>Creator(s)</b> """),_display_(Seq[Any](/*30.33*/(jo \ "Creator")/*30.49*/ match/*30.55*/ {/*31.11*/case creator:JsString =>/*31.35*/ {_display_(Seq[Any](format.raw/*31.37*/(""" 
            <a href=""""),_display_(Seq[Any](/*32.23*/creator/*32.30*/.as[String])),format.raw/*32.41*/("""" target="_blank">"""),_display_(Seq[Any](/*32.60*/((jo \ "CreatorName").as[String]))),format.raw/*32.93*/("""</a>
          """)))}/*34.11*/case creators:JsArray =>/*34.35*/ {_display_(Seq[Any](format.raw/*34.37*/("""
          	
          	<a href=""""),_display_(Seq[Any](/*36.22*/creators(0)/*36.33*/.as[String])),format.raw/*36.44*/("""" target="_blank">"""),_display_(Seq[Any](/*36.63*/(((jo \ "CreatorName"))(0).as[String]))),format.raw/*36.101*/("""</a>
          	  """),_display_(Seq[Any](/*37.15*/for(i <- 1 to (creators.value.size-1)) yield /*37.53*/ {_display_(Seq[Any](format.raw/*37.55*/("""
          	  	,<a href=""""),_display_(Seq[Any](/*38.26*/creators(i)/*38.37*/.as[String])),format.raw/*38.48*/("""" target="_blank">"""),_display_(Seq[Any](/*38.67*/(((jo \ "CreatorName"))(i).as[String]))),format.raw/*38.105*/("""</a>
          	  	""")))})),format.raw/*39.16*/("""
          	""")))}})),format.raw/*41.12*/("""</p>
           
                 
           <p><b>Abstract:</b> """),_display_(Seq[Any](/*44.33*/((jo \ "Abstract").as[String]))),format.raw/*44.63*/("""</p>   
           <div><b>Published:</b> from <a href=""""),_display_(Seq[Any](/*45.50*/((jo \ "Publishing Project").as[String]))),format.raw/*45.90*/("""" target="_blank">
           """),_display_(Seq[Any](/*46.13*/((jo \ "Publishing Project Name").asOpt[String] match {
           case Some(name) => name
           case None => (jo \ "Publishing Project").as[String]
           }))),format.raw/*49.14*/("""
           	</a> to <div class="repository">"""),_display_(Seq[Any](/*50.46*/((jo \ "Repository").as[String]))),format.raw/*50.78*/("""</div> on """),_display_(Seq[Any](/*50.89*/((jo \ "Publication Date").as[String]))),format.raw/*50.127*/("""
           	</div>
           	
        </div>
      </div>
      </div>
    """)))}})),format.raw/*57.6*/("""
    """)))})),format.raw/*58.6*/("""

      
      </div>
      <script src=""""),_display_(Seq[Any](/*62.21*/routes/*62.27*/.Assets.at("javascripts/curationProcess.js"))),format.raw/*62.71*/("""" type="text/javascript"></script>
      <script src=""""),_display_(Seq[Any](/*63.21*/routes/*63.27*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*63.67*/("""" type="text/javascript"></script>
      <script src=""""),_display_(Seq[Any](/*64.21*/routes/*64.27*/.Assets.at("javascripts/repositories.js"))),format.raw/*64.68*/("""" type="text/javascript"></script>
      <script src=""""),_display_(Seq[Any](/*65.21*/routes/*65.27*/.Assets.at("javascripts/people.js"))),format.raw/*65.62*/("""" type="text/javascript"></script>
      
      <script src=""""),_display_(Seq[Any](/*67.21*/routes/*67.27*/.Assets.at("javascripts/filtrify.js"))),format.raw/*67.64*/(""""  type="text/javascript"></script>
	  <link rel="stylesheet" href=""""),_display_(Seq[Any](/*68.34*/routes/*68.40*/.Assets.at("stylesheets/filtrify.css"))),format.raw/*68.78*/("""">
      <script src="/assets/javascripts/lib/masonry.pkgd.min.js" type="text/javascript"></script>
      <script>

		$(document).ready(function()"""),format.raw/*72.31*/("""{"""),format.raw/*72.32*/("""
		
			//people.js and repositories.js can change the size of the grid elements - run the masonry layout once they are done
        	$(document).one('ajaxStop', function() """),format.raw/*75.49*/("""{"""),format.raw/*75.50*/(""" 
            	$.filtrify("pubs", "filters", """),format.raw/*76.44*/("""{"""),format.raw/*76.45*/("""
            	  	"""),_display_(Seq[Any](/*77.18*/query/*77.23*/ match/*77.29*/ {/*78.18*/case Some(q) =>/*78.33*/ {_display_(Seq[Any](format.raw/*78.35*/("""
	            			query : """),format.raw/*79.25*/("""{"""),format.raw/*79.26*/(""""Space" : """),_display_(Seq[Any](/*79.37*/Html(q))),format.raw/*79.44*/("""}"""),format.raw/*79.45*/(""",
    	        			""")))}/*81.18*/case None =>/*81.30*/ {}})),format.raw/*82.16*/("""	
                	callback: function(query, match, mismatch) """),format.raw/*83.61*/("""{"""),format.raw/*83.62*/("""
                		
                    	writeLegend(query, match, mismatch);
                    	$('.grid').masonry();
                	"""),format.raw/*87.18*/("""}"""),format.raw/*87.19*/("""
                """),format.raw/*88.17*/("""}"""),format.raw/*88.18*/(""");

                $('.grid').masonry("""),format.raw/*90.36*/("""{"""),format.raw/*90.37*/("""
              		// options
  	              itemSelector: '.grid-item',
      	          columnWidth: '.grid-item',
          	      gutter: 10
              	"""),format.raw/*95.16*/("""}"""),format.raw/*95.17*/(""");
        	"""),format.raw/*96.10*/("""}"""),format.raw/*96.11*/("""); 
        	 	  
            	  
        """),format.raw/*99.9*/("""}"""),format.raw/*99.10*/(""");

	    function writeLegend ( query, match, mismatch ) """),format.raw/*101.54*/("""{"""),format.raw/*101.55*/("""
	        if ( !mismatch.length ) """),format.raw/*102.34*/("""{"""),format.raw/*102.35*/("""
	            $("#legend").html("<i>Viewing all publications.</i>");
	        """),format.raw/*104.10*/("""}"""),format.raw/*104.11*/(""" else """),format.raw/*104.17*/("""{"""),format.raw/*104.18*/("""
	            var category, tags, i, tag, legend = "<p>Viewing:</p>";
	            for ( category in query ) """),format.raw/*106.40*/("""{"""),format.raw/*106.41*/("""
	                tags = query[category];
	                if ( tags.length ) """),format.raw/*108.37*/("""{"""),format.raw/*108.38*/("""
	                    legend += "<p><span>" + category + "(s):</span>";
	                    for ( i = 0; i < tags.length; i++ ) """),format.raw/*110.58*/("""{"""),format.raw/*110.59*/("""
	                        tag = tags[i];
							if(i>0) legend += ", ";
	                        legend += "<em>" + tag + "</em>";
	                        
	                    """),format.raw/*115.22*/("""}"""),format.raw/*115.23*/(""";
	                    legend += "</p>";
	                """),format.raw/*117.18*/("""}"""),format.raw/*117.19*/(""";
	            """),format.raw/*118.14*/("""}"""),format.raw/*118.15*/(""";
	            legend += "<p><i>" + match.length + " publication" + (match.length !== 1 ? "s" : "") + " found.</i></p>";
	            $("#legend").html( legend );
	        """),format.raw/*121.10*/("""}"""),format.raw/*121.11*/(""";
	    """),format.raw/*122.6*/("""}"""),format.raw/*122.7*/(""";

	
                   
 
      </script>
"""))}
    }
    
    def render(publishedData:List[play.api.libs.json.JsValue],servicesUrl:String,query:Option[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(publishedData,servicesUrl,query)(user)
    
    def f:((List[play.api.libs.json.JsValue],String,Option[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (publishedData,servicesUrl,query) => (user) => apply(publishedData,servicesUrl,query)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:41 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/curations/publishedGrid.scala.html
                    HASH: 5e2d96c9b42e0437fc7fcd489f31263df0055208
                    MATRIX: 673->1|925->130|952->160|1196->370|1235->394|1274->396|1321->408|1331->410|1345->416|1354->425|1381->444|1420->446|1531->521|1556->537|1571->543|1582->556|1615->580|1664->583|1716->633|1749->657|1798->660|1872->704|1927->742|1967->743|2007->746|2084->792|2145->817|2334->984|2396->1010|2447->1052|2498->1065|2536->1067|2705->1210|2780->1249|2836->1296|2876->1298|2908->1312|2921->1317|2960->1318|3004->1329|3185->1474|3234->1501|3351->1582|3399->1607|3455->1626|3503->1651|3593->1705|3618->1721|3633->1727|3644->1740|3677->1764|3717->1766|3777->1790|3793->1797|3826->1808|3881->1827|3936->1860|3971->1887|4004->1911|4044->1913|4114->1947|4134->1958|4167->1969|4222->1988|4283->2026|4338->2045|4392->2083|4432->2085|4494->2111|4514->2122|4547->2133|4602->2152|4663->2190|4715->2210|4761->2235|4864->2302|4916->2332|5009->2389|5071->2429|5138->2460|5327->2627|5409->2673|5463->2705|5510->2716|5571->2754|5682->2839|5719->2845|5797->2887|5812->2893|5878->2937|5969->2992|5984->2998|6046->3038|6137->3093|6152->3099|6215->3140|6306->3195|6321->3201|6378->3236|6476->3298|6491->3304|6550->3341|6655->3410|6670->3416|6730->3454|6904->3600|6933->3601|7133->3773|7162->3774|7235->3819|7264->3820|7318->3838|7332->3843|7347->3849|7358->3869|7382->3884|7422->3886|7475->3911|7504->3912|7551->3923|7580->3930|7609->3931|7647->3968|7668->3980|7694->3999|7784->4061|7813->4062|7979->4200|8008->4201|8053->4218|8082->4219|8149->4258|8178->4259|8366->4419|8395->4420|8435->4432|8464->4433|8533->4475|8562->4476|8648->4533|8678->4534|8741->4568|8771->4569|8878->4647|8908->4648|8943->4654|8973->4655|9111->4764|9141->4765|9248->4843|9278->4844|9436->4973|9466->4974|9673->5152|9703->5153|9790->5211|9820->5212|9864->5227|9894->5228|10095->5400|10125->5401|10160->5408|10189->5409
                    LINES: 20->1|24->1|25->3|29->7|29->7|29->7|30->8|30->8|30->8|30->9|30->9|30->9|31->10|31->10|31->10|31->11|31->11|31->11|31->12|31->12|31->12|31->12|31->12|31->12|31->12|31->12|32->13|35->16|36->17|36->17|36->17|36->17|39->20|41->22|41->22|41->22|41->22|41->22|41->22|41->22|46->27|46->27|47->28|47->28|47->28|47->28|49->30|49->30|49->30|49->31|49->31|49->31|50->32|50->32|50->32|50->32|50->32|51->34|51->34|51->34|53->36|53->36|53->36|53->36|53->36|54->37|54->37|54->37|55->38|55->38|55->38|55->38|55->38|56->39|57->41|60->44|60->44|61->45|61->45|62->46|65->49|66->50|66->50|66->50|66->50|72->57|73->58|77->62|77->62|77->62|78->63|78->63|78->63|79->64|79->64|79->64|80->65|80->65|80->65|82->67|82->67|82->67|83->68|83->68|83->68|87->72|87->72|90->75|90->75|91->76|91->76|92->77|92->77|92->77|92->78|92->78|92->78|93->79|93->79|93->79|93->79|93->79|94->81|94->81|94->82|95->83|95->83|99->87|99->87|100->88|100->88|102->90|102->90|107->95|107->95|108->96|108->96|111->99|111->99|113->101|113->101|114->102|114->102|116->104|116->104|116->104|116->104|118->106|118->106|120->108|120->108|122->110|122->110|127->115|127->115|129->117|129->117|130->118|130->118|133->121|133->121|134->122|134->122
                    -- GENERATED --
                */
            