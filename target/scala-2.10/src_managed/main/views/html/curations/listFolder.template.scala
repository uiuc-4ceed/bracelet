
package views.html.curations

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listFolder extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[CurationFolder,UUID,UUID,Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(folder: CurationFolder, parentId:UUID, parentCurationObject: UUID, inPreparation: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.129*/("""

"""),format.raw/*4.1*/("""<div class="panel panel-default folder-panel" id=""""),_display_(Seq[Any](/*4.52*/folder/*4.58*/.id)),format.raw/*4.61*/("""-listitem">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-2 col-sm-2 col-lg-2">
        <h1 class="text-center"><span class="glyphicon glyphicon-folder-close"></span></h1>
      </div>
      <div class="col-md-10 col-sm-10 col-lg-10">
        <div id="folder-title-"""),_display_(Seq[Any](/*11.32*/folder/*11.38*/.id)),format.raw/*11.41*/("""">
          <h3 id =""""),_display_(Seq[Any](/*12.21*/folder/*12.27*/.id)),format.raw/*12.30*/("""-name" class="inline"><a href="javascript:updatePageAndFolder(0, '"""),_display_(Seq[Any](/*12.97*/folder/*12.103*/.id.stringify)),format.raw/*12.116*/("""')">"""),_display_(Seq[Any](/*12.121*/folder/*12.127*/.displayName)),format.raw/*12.139*/("""</a></h3>
        </div>

        <div class="row">
          <div class="col-md-12 col-lg-12 col-sm-12">
            <ul class="list-unstyled">
              <li>
                <span class="glyphicon glyphicon-folder-close"></span> """),_display_(Seq[Any](/*19.73*/folder/*19.79*/.folders.length)),format.raw/*19.94*/("""
                <span class="glyphicon glyphicon-file"></span> """),_display_(Seq[Any](/*20.65*/folder/*20.71*/.files.length)),format.raw/*20.84*/("""
                """),_display_(Seq[Any](/*21.18*/if(user.isDefined && inPreparation)/*21.53*/ {_display_(Seq[Any](format.raw/*21.55*/("""
                  """),_display_(Seq[Any](/*22.20*/if(Permission.checkPermission(Permission.EditStagingArea, ResourceRef(ResourceRef.curationObject, parentCurationObject)))/*22.141*/{_display_(Seq[Any](format.raw/*22.142*/("""
                    <button id="folder-"""),_display_(Seq[Any](/*23.41*/folder/*23.47*/.id)),format.raw/*23.50*/("""" class="btn btn-link delete-folder" style="text-align:right" title="Delete"
                    onclick="confirmDeleteResource('curation folder','following folder from this request','"""),_display_(Seq[Any](/*24.109*/(folder.id))),format.raw/*24.120*/("""','"""),_display_(Seq[Any](/*24.124*/(folder.displayName.replace("'","&#39;")))),format.raw/*24.165*/("""', '"""),_display_(Seq[Any](/*24.170*/parentCurationObject)),format.raw/*24.190*/("""', '"""),_display_(Seq[Any](/*24.195*/parentId)),format.raw/*24.203*/("""')">
                      <span class="glyphicon glyphicon-trash"></span></button>
                  """)))}/*26.21*/else/*26.26*/{_display_(Seq[Any](format.raw/*26.27*/("""
                    <button disabled class="btn btn-link" style="text-align:right" title="Delete">
                      <span class="glyphicon glyphicon-trash"></span></button>
                  """)))})),format.raw/*29.20*/("""
                """)))})),format.raw/*30.18*/("""
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>"""))}
    }
    
    def render(folder:CurationFolder,parentId:UUID,parentCurationObject:UUID,inPreparation:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(folder,parentId,parentCurationObject,inPreparation)(user)
    
    def f:((CurationFolder,UUID,UUID,Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (folder,parentId,parentCurationObject,inPreparation) => (user) => apply(folder,parentId,parentCurationObject,inPreparation)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:41 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/curations/listFolder.scala.html
                    HASH: 97caf2404577417682a283f5892f769c54e0eb49
                    MATRIX: 648->1|892->128|920->153|1006->204|1020->210|1044->213|1374->507|1389->513|1414->516|1473->539|1488->545|1513->548|1616->615|1632->621|1668->634|1710->639|1726->645|1761->657|2033->893|2048->899|2085->914|2186->979|2201->985|2236->998|2290->1016|2334->1051|2374->1053|2430->1073|2561->1194|2601->1195|2678->1236|2693->1242|2718->1245|2940->1430|2974->1441|3015->1445|3079->1486|3121->1491|3164->1511|3206->1516|3237->1524|3359->1628|3372->1633|3411->1634|3641->1832|3691->1850
                    LINES: 20->1|24->1|26->4|26->4|26->4|26->4|33->11|33->11|33->11|34->12|34->12|34->12|34->12|34->12|34->12|34->12|34->12|34->12|41->19|41->19|41->19|42->20|42->20|42->20|43->21|43->21|43->21|44->22|44->22|44->22|45->23|45->23|45->23|46->24|46->24|46->24|46->24|46->24|46->24|46->24|46->24|48->26|48->26|48->26|51->29|52->30
                    -- GENERATED --
                */
            