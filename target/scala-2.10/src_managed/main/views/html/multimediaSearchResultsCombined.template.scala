
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object multimediaSearchResultsCombined extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[String,Option[UUID],List[scala.Tuple4[models.UUID, String, String, Double]],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(fileName:String="Query file", thumbnailId:Option[UUID], resultsList:List[(models.UUID, String, String, Double)])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.151*/("""

"""),format.raw/*3.71*/("""
"""),format.raw/*4.69*/("""
"""),format.raw/*5.71*/("""
				
"""),format.raw/*7.73*/("""

"""),_display_(Seq[Any](/*9.2*/main("Search Results")/*9.24*/ {_display_(Seq[Any](format.raw/*9.26*/("""		
	<div class="page-header">
		<h2>Combined Weighted Search Results For <medium>"""),_display_(Seq[Any](/*11.53*/fileName)),format.raw/*11.61*/("""</medium></h2>

        """),_display_(Seq[Any](/*13.10*/if(thumbnailId.isDefined)/*13.35*/ {_display_(Seq[Any](format.raw/*13.37*/("""
       <img class='mmd-img media-object' 
          src='"""),_display_(Seq[Any](/*15.17*/(routes.Files.thumbnail(thumbnailId.get)))),format.raw/*15.58*/("""'
          title='Thumbnail of query file'>
        </img>
    """)))})),format.raw/*18.6*/("""
  </div>
  
	"""),_display_(Seq[Any](/*21.3*/if(resultsList.size == 0)/*21.28*/ {_display_(Seq[Any](format.raw/*21.30*/("""
	<div class="row">
		<div class="col-md-12">
			No results found
		</div>
	</div>
	""")))}/*27.4*/else/*27.9*/{_display_(Seq[Any](format.raw/*27.10*/("""
	<table>
	<tr>  
  <td>		
		<table>			
			
  """),_display_(Seq[Any](/*33.4*/for((file_id, fileName, thumbnailId, proximity) <- resultsList) yield /*33.67*/ {_display_(Seq[Any](format.raw/*33.69*/("""
		<tr>		
				<td style="vertical-align:middle; height:150px;">
					<div class="media">				  
													
							"""),_display_(Seq[Any](/*38.9*/if(!thumbnailId.isEmpty)/*38.33*/{_display_(Seq[Any](format.raw/*38.34*/("""
								<img class='mmd-img media-object' 
									src='"""),_display_(Seq[Any](/*40.16*/(routes.Files.thumbnail(UUID(thumbnailId))))),format.raw/*40.59*/("""'
									alt='Thumbnail of """),_display_(Seq[Any](/*41.29*/fileName)),format.raw/*41.37*/("""' 
									title='Thumbnail of """),_display_(Seq[Any](/*42.31*/fileName)),format.raw/*42.39*/("""'>
								</img>
							""")))}/*44.10*/else/*44.15*/{_display_(Seq[Any](format.raw/*44.16*/("""No thumbnail available""")))})),format.raw/*44.39*/("""						
							</a>
					  <div class="media-body">
					    <h4 class="media-heading">"""),_display_(Seq[Any](/*47.37*/fileName)),format.raw/*47.45*/("""</h4>
					    Combined weighted proximity (normalized): """),_display_(Seq[Any](/*48.53*/proximity)),format.raw/*48.62*/("""			    
					    <br> 	<a href=""""),_display_(Seq[Any](/*49.26*/(routes.Files.file( file_id  )))),format.raw/*49.57*/("""">View File """),_display_(Seq[Any](/*49.70*/fileName)),format.raw/*49.78*/("""</a>    
					  </div>
					</div>
					</td></tr>
	""")))})),format.raw/*53.3*/(""" """),format.raw/*53.86*/("""
			
		</table></td>
		
	</tr>
	</table>
	""")))})),format.raw/*59.3*/("""
""")))})),format.raw/*60.2*/("""
"""))}
    }
    
    def render(fileName:String,thumbnailId:Option[UUID],resultsList:List[scala.Tuple4[models.UUID, String, String, Double]],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(fileName,thumbnailId,resultsList)(user)
    
    def f:((String,Option[UUID],List[scala.Tuple4[models.UUID, String, String, Double]]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (fileName,thumbnailId,resultsList) => (user) => apply(fileName,thumbnailId,resultsList)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:24 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/multimediaSearchResultsCombined.scala.html
                    HASH: f054a1ea1c3a84705e8f4c479571c084e44f7886
                    MATRIX: 702->1|946->150|975->222|1003->291|1031->362|1064->440|1101->443|1131->465|1170->467|1288->549|1318->557|1379->582|1413->607|1453->609|1548->668|1611->709|1707->774|1757->789|1791->814|1831->816|1934->902|1946->907|1985->908|2067->955|2146->1018|2186->1020|2338->1137|2371->1161|2410->1162|2505->1221|2570->1264|2636->1294|2666->1302|2735->1335|2765->1343|2810->1370|2823->1375|2862->1376|2917->1399|3040->1486|3070->1494|3164->1552|3195->1561|3264->1594|3317->1625|3366->1638|3396->1646|3480->1699|3509->1782|3583->1825|3616->1827
                    LINES: 20->1|23->1|25->3|26->4|27->5|29->7|31->9|31->9|31->9|33->11|33->11|35->13|35->13|35->13|37->15|37->15|40->18|43->21|43->21|43->21|49->27|49->27|49->27|55->33|55->33|55->33|60->38|60->38|60->38|62->40|62->40|63->41|63->41|64->42|64->42|66->44|66->44|66->44|66->44|69->47|69->47|70->48|70->48|71->49|71->49|71->49|71->49|75->53|75->53|81->59|82->60
                    -- GENERATED --
                */
            