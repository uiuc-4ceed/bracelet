
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object dashboard extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template17[String,List[models.Event],User,List[Dataset],Map[UUID, Int],List[Collection],List[ProjectSpace],Boolean,List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],Option[Boolean],List[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(displayedName: String,  newsfeed: List[models.Event], profile:User, datasetsList: List[Dataset],  dscomments: Map[UUID, Int], collectionsList:List[Collection], spacesList: List[ProjectSpace],
        deletePermission: Boolean,  followers: List[(UUID, String, String, String)], followedUsers: List[(UUID, String, String, String)], followedFiles: List[(UUID, String, String)],
        followedDatasets: List[(UUID, String, String)], followedCollections: List[(UUID, String, String)],
        followedSpaces: List[(UUID, String, String)], ownProfile: Option[Boolean], userSelections: List[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters

import api.Permission

import models.DatasetStatus

import play.api.i18n.Messages


Seq[Any](format.raw/*4.149*/("""

"""),format.raw/*10.1*/("""
"""),_display_(Seq[Any](/*11.2*/main(displayedName)/*11.21*/ {_display_(Seq[Any](format.raw/*11.23*/("""
<script src=""""),_display_(Seq[Any](/*12.15*/routes/*12.21*/.Assets.at("javascripts/collectionListProcess.js"))),format.raw/*12.71*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*13.15*/routes/*13.21*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*13.68*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*14.15*/routes/*14.21*/.Assets.at("javascripts/spaceListProcess.js"))),format.raw/*14.66*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*15.15*/routes/*15.21*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*15.61*/("""" type="text/javascript"></script>
<div class="row featurette" style="display:margin:-150px;">
    <div class="col-md-6">
        <h1 class="featurette-heading">"""),_display_(Seq[Any](/*18.41*/profile/*18.48*/.fullName)),format.raw/*18.57*/("""</h1>
        <a href=""""),_display_(Seq[Any](/*19.19*/routes/*19.25*/.Profile.viewProfileUUID(profile.id))),format.raw/*19.61*/("""" class="btn btn-link" title="Show profile"><span class="glyphicon glyphicon-user"></span> Profile</a>
        <a href=""""),_display_(Seq[Any](/*20.19*/routes/*20.25*/.Spaces.newSpace())),format.raw/*20.43*/("""" class="btn btn-link" title="Create a new space"><span class="glyphicon glyphicon-ok"></span> Create Space</a>
        <a href=""""),_display_(Seq[Any](/*21.19*/routes/*21.25*/.Collections.newCollection(None))),format.raw/*21.57*/("""" class="btn btn-link" title="Create a new collection"><span class="glyphicon glyphicon-ok"></span> Create Collection</a>
        <a href=""""),_display_(Seq[Any](/*22.19*/routes/*22.25*/.Datasets.newDataset(None, None))),format.raw/*22.57*/("""" class="btn btn-link" title="Create a new dataset"><span class="glyphicon glyphicon-ok"></span> Create Dataset</a>
        
        """),format.raw/*26.11*/("""
        <h2> My Collections and Datasets</h2>
        <!-jstree goes here-->
        <div class="showSearch">
            <input class="search-collection form-control" placeholder="Search your collections"></input><br />
        </div>
        <div class="panel panel-default">
            <div id="collections">
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <h2>My Events</h2>
            """),_display_(Seq[Any](/*40.14*/for(event <- newsfeed) yield /*40.36*/ {_display_(Seq[Any](format.raw/*40.38*/("""
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <div class="media-left">
                                <a href=""""),_display_(Seq[Any](/*45.43*/routes/*45.49*/.Profile.viewProfileUUID(event.user.id))),format.raw/*45.88*/("""">
                                    <img class="media-object" src=""""),_display_(Seq[Any](/*46.69*/event/*46.74*/.user.avatarURL)),format.raw/*46.89*/("""">
                                </a>
                            </div>
                            <div class="media-body">
                                <a href=""""),_display_(Seq[Any](/*50.43*/routes/*50.49*/.Profile.viewProfileUUID(event.user.id))),format.raw/*50.88*/("""">
                                    <h4 class="media-heading">"""),_display_(Seq[Any](/*51.64*/event/*51.69*/.user.fullName)),format.raw/*51.83*/("""</h4>
                                </a>
                                """),_display_(Seq[Any](/*53.34*/newsfeedCard(event))),format.raw/*53.53*/("""
                            </div>
                            <div class="media-right">
                                """),_display_(Seq[Any](/*56.34*/Formatters/*56.44*/.humanReadableTimeSince(event.created))),format.raw/*56.82*/("""
                            </div>
                        </div>
                    </div>
                </div>
            """)))})),format.raw/*61.14*/("""
            """),_display_(Seq[Any](/*62.14*/if(newsfeed.size < 1)/*62.35*/ {_display_(Seq[Any](format.raw/*62.37*/("""
                <p>You can follow <a href=""""),_display_(Seq[Any](/*63.45*/routes/*63.51*/.Users.getUsers())),format.raw/*63.68*/("""">users</a>, <a href=""""),_display_(Seq[Any](/*63.91*/routes/*63.97*/.Spaces.list(""))),format.raw/*63.113*/("""">spaces</a>, <a href=""""),_display_(Seq[Any](/*63.137*/routes/*63.143*/.Datasets.list(""))),format.raw/*63.161*/("""">datasets</a> and <a href=""""),_display_(Seq[Any](/*63.190*/routes/*63.196*/.Collections.list(""))),format.raw/*63.217*/("""">collections</a>.
                    Any updates on your followed instances will show here.</p>
            """)))})),format.raw/*65.14*/("""

    </div>
</div>

<div class="row top-padding">
                                        </div>
        </div>
    </div>
    <script src=""""),_display_(Seq[Any](/*74.19*/routes/*74.25*/.Assets.at("javascripts/lib/masonry.pkgd.min.js"))),format.raw/*74.74*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*75.19*/routes/*75.25*/.Assets.at("javascripts/lib/imagesloaded.pkgd.min.js"))),format.raw/*75.79*/("""" type="text/javascript"></script>
    <script>
    //Get collections
    function getCollections() """),format.raw/*78.31*/("""{"""),format.raw/*78.32*/("""
        $.ajax("""),format.raw/*79.16*/("""{"""),format.raw/*79.17*/("""
            url: "api/t2c2/allCollectionsWithDatasetIds",
            dataType: "json",
            beforeSend: function(xhr)"""),format.raw/*82.38*/("""{"""),format.raw/*82.39*/("""
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader("Accept", "application/json");
            """),format.raw/*85.13*/("""}"""),format.raw/*85.14*/(""",
            success: function(data) """),format.raw/*86.37*/("""{"""),format.raw/*86.38*/("""
                    var col = "#collections";
                    createJSTrees(data, col);
            """),format.raw/*89.13*/("""}"""),format.raw/*89.14*/(""",
            error: function(xhr, status, error) """),format.raw/*90.49*/("""{"""),format.raw/*90.50*/("""

            """),format.raw/*92.13*/("""}"""),format.raw/*92.14*/("""
        """),format.raw/*93.9*/("""}"""),format.raw/*93.10*/(""");
    """),format.raw/*94.5*/("""}"""),format.raw/*94.6*/("""

    getCollections();

    function createJSTrees(jsonData, type) """),format.raw/*98.44*/("""{"""),format.raw/*98.45*/("""
        console.log(jsonData);
        //build key/value pairs for datasets that exist
        for (var i = 0; i < jsonData.length; i++)"""),format.raw/*101.50*/("""{"""),format.raw/*101.51*/("""

            //if there are multiple datasets then split the string
            if (jsonData[i]['dataset_ids'])"""),format.raw/*104.44*/("""{"""),format.raw/*104.45*/("""

                var arr = jsonData[i]['dataset_ids'].split(",");

                var parent_collection_ids = jsonData[i]['id'];
                var root_flag = "false"
                $.each(arr, function(key, val)"""),format.raw/*110.47*/("""{"""),format.raw/*110.48*/("""

                    var dataset_name = val.split(':')[0].replace(/[^a-zA-Z 0-9]+/g,'');
                    var dataset_id = val.substring(val.indexOf(":") + 1);
                    //differentiate between collections and datasets
                    var ds = "#dataset_id" + dataset_id;
                    jsonData.push("""),format.raw/*116.35*/("""{"""),format.raw/*116.36*/("""parent_collection_ids : parent_collection_ids, name : dataset_name, root_flag : root_flag, id : ds """),format.raw/*116.135*/("""}"""),format.raw/*116.136*/(""");
                """),format.raw/*117.17*/("""}"""),format.raw/*117.18*/(""");

            """),format.raw/*119.13*/("""}"""),format.raw/*119.14*/("""

            //create library supported key names (text,parent)
            jsonData[i].text = jsonData[i]['name'];
            jsonData[i].parent = jsonData[i]['parent_collection_ids'];

            //remove old keys
            delete jsonData[i].name;
            delete jsonData[i].parent_collection_ids;

            // Is this a root collection?
            if (jsonData[i].root_flag == "true")"""),format.raw/*130.49*/("""{"""),format.raw/*130.50*/("""
                jsonData[i].parent = "#";

            """),format.raw/*133.13*/("""}"""),format.raw/*133.14*/("""else"""),format.raw/*133.18*/("""{"""),format.raw/*133.19*/("""
                var str = "";
                // Remove special characters from the values
                str = jsonData[i].parent.replace(/[List()]+/gi, '');
                jsonData[i].parent = str;
            """),format.raw/*138.13*/("""}"""),format.raw/*138.14*/("""

            //For new root collections where the json requires a root parent to have a value of #
            if (jsonData[i].parent == "")"""),format.raw/*141.42*/("""{"""),format.raw/*141.43*/("""
                jsonData[i].parent = "#";
            """),format.raw/*143.13*/("""}"""),format.raw/*143.14*/("""

        """),format.raw/*145.9*/("""}"""),format.raw/*145.10*/("""

        //Create tree from js object
        //jstree initialization and settings
        $(type).jstree("""),format.raw/*149.24*/("""{"""),format.raw/*149.25*/("""

            "core" : """),format.raw/*151.22*/("""{"""),format.raw/*151.23*/("""
                "check_callback" : true,
                "data" : jsonData,
                "multiple" : false,
                "themes" : """),format.raw/*155.28*/("""{"""),format.raw/*155.29*/("""
                  "variant" : "large",
                  "stripes" : true
                """),format.raw/*158.17*/("""}"""),format.raw/*158.18*/(""",
                "search": """),format.raw/*159.27*/("""{"""),format.raw/*159.28*/("""
                  "case_insensitive": true,
                  "show_only_matches" : true
                """),format.raw/*162.17*/("""}"""),format.raw/*162.18*/(""",
            """),format.raw/*163.13*/("""}"""),format.raw/*163.14*/(""",
            "checkbox" : """),format.raw/*164.26*/("""{"""),format.raw/*164.27*/("""
                "keep_selected_style" : false,
                "three_state" : false
             """),format.raw/*167.14*/("""}"""),format.raw/*167.15*/(""",
            "contextmenu":"""),format.raw/*168.27*/("""{"""),format.raw/*168.28*/("""
                "items": function($node) """),format.raw/*169.42*/("""{"""),format.raw/*169.43*/("""
                    var tree = $(type).jstree(true);
                    return """),format.raw/*171.28*/("""{"""),format.raw/*171.29*/("""
                        "Create": """),format.raw/*172.35*/("""{"""),format.raw/*172.36*/("""
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Create New ",
                            "icon" : "glyphicon glyphicon-plus",
                            "action": function (obj) """),format.raw/*177.54*/("""{"""),format.raw/*177.55*/("""
                                $node = tree.create_node($node);
                                tree.edit($node);
                            """),format.raw/*180.29*/("""}"""),format.raw/*180.30*/("""
                        """),format.raw/*181.25*/("""}"""),format.raw/*181.26*/("""
                    """),format.raw/*182.21*/("""}"""),format.raw/*182.22*/(""";
                """),format.raw/*183.17*/("""}"""),format.raw/*183.18*/("""
            """),format.raw/*184.13*/("""}"""),format.raw/*184.14*/(""",
            "plugins" : [
                "checkbox",
                // "contextmenu",
                "massload",
                "search",
                "sort",
                // "state",
                "types",
                "unique",
                "wholerow",
                "changed",
                "conditionalselect"
            ],

        """),format.raw/*199.9*/("""}"""),format.raw/*199.10*/(""");

    """),format.raw/*201.5*/("""}"""),format.raw/*201.6*/("""

    //Search Plugin
    $(".search-collection").keyup(function() """),format.raw/*204.46*/("""{"""),format.raw/*204.47*/("""
        var searchString = $(this).val();
        $("#collections").jstree('search', searchString);
    """),format.raw/*207.5*/("""}"""),format.raw/*207.6*/(""");

    //Prevent collection search from submitting the form
    $('.search-collection').keydown(function (e) """),format.raw/*210.50*/("""{"""),format.raw/*210.51*/("""
        if (e.keyCode == 13) """),format.raw/*211.30*/("""{"""),format.raw/*211.31*/("""
            e.preventDefault();
        """),format.raw/*213.9*/("""}"""),format.raw/*213.10*/("""
    """),format.raw/*214.5*/("""}"""),format.raw/*214.6*/(""");

    $("#collections").on("select_node.jstree", function(e, data)"""),format.raw/*216.65*/("""{"""),format.raw/*216.66*/("""
            var str = data.node.id;
            var res = str.match(/J/gi);

            //how to determine if dataset
            if (res != "j")"""),format.raw/*221.28*/("""{"""),format.raw/*221.29*/("""
                if (str.match("^#dataset_id"))"""),format.raw/*222.47*/("""{"""),format.raw/*222.48*/("""
                    var newStr = str.replace("#dataset_id", "");
                    window.location.href = "/datasets/" + newStr;

                """),format.raw/*226.17*/("""}"""),format.raw/*226.18*/("""else"""),format.raw/*226.22*/("""{"""),format.raw/*226.23*/("""
                    window.location.href = "/collection/" + str;
                """),format.raw/*228.17*/("""}"""),format.raw/*228.18*/("""
            """),format.raw/*229.13*/("""}"""),format.raw/*229.14*/("""

    """),format.raw/*231.5*/("""}"""),format.raw/*231.6*/(""");

    </script>

    <script>
        var removeIndicator=true;
        function activateOne(id) """),format.raw/*237.34*/("""{"""),format.raw/*237.35*/("""
            // initialize Masonry
            var $container = $('#'+id).masonry();
            // layout Masonry again after all images have loaded
            imagesLoaded( '#masonry', function() """),format.raw/*241.50*/("""{"""),format.raw/*241.51*/("""
                $container.masonry("""),format.raw/*242.36*/("""{"""),format.raw/*242.37*/("""
                itemSelector: '.post-box',
                columnWidth: '.post-box',
                transitionDuration: 4
                """),format.raw/*246.17*/("""}"""),format.raw/*246.18*/(""");
            """),format.raw/*247.13*/("""}"""),format.raw/*247.14*/(""");
        """),format.raw/*248.9*/("""}"""),format.raw/*248.10*/("""

        function activate()"""),format.raw/*250.28*/("""{"""),format.raw/*250.29*/("""
            activateOne("masonry-datasets");
            activateOne("masonry-collections");
            activateOne("masonry-spaces");
        """),format.raw/*254.9*/("""}"""),format.raw/*254.10*/("""
        $(document).ready(function() """),format.raw/*255.38*/("""{"""),format.raw/*255.39*/("""

            /*
            * Clamped-width.
            * Usage:
            *  <div data-clampedwidth=".myParent">This long content will force clamped width</div>
            *
            * Author: LV
            */
            $('[data-clampedwidth]').each(function () """),format.raw/*264.55*/("""{"""),format.raw/*264.56*/("""
                var elem = $(this);
                var parentPanel = elem.data('clampedwidth');
                var resizeFn = function () """),format.raw/*267.44*/("""{"""),format.raw/*267.45*/("""
                    var sideBarNavWidth = $(parentPanel).width() - parseInt(elem.css('paddingLeft')) - parseInt(elem.css('paddingRight')) - parseInt(elem.css('marginLeft')) - parseInt(elem.css('marginRight')) - parseInt(elem.css('borderLeftWidth')) - parseInt(elem.css('borderRightWidth'));
                    elem.css('width', sideBarNavWidth);
                """),format.raw/*270.17*/("""}"""),format.raw/*270.18*/(""";

                resizeFn();
                $(window).resize(resizeFn);
            """),format.raw/*274.13*/("""}"""),format.raw/*274.14*/(""");

            var panel = $('#resources-panel');
            panel.affix("""),format.raw/*277.25*/("""{"""),format.raw/*277.26*/("""
                offset: """),format.raw/*278.25*/("""{"""),format.raw/*278.26*/("""
                    top: panel.offset() - panel.height()
                """),format.raw/*280.17*/("""}"""),format.raw/*280.18*/("""
            """),format.raw/*281.13*/("""}"""),format.raw/*281.14*/(""");

            var width = $('.col-md-7').width() * 0.05;
            $('.media-object' ).each(function() """),format.raw/*284.49*/("""{"""),format.raw/*284.50*/("""
                $(this ).css("width", width);
                $(this ).css("height", width);
            """),format.raw/*287.13*/("""}"""),format.raw/*287.14*/(""");
            activate();
        """),format.raw/*289.9*/("""}"""),format.raw/*289.10*/(""");



        // fire when showing from tab
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) """),format.raw/*294.67*/("""{"""),format.raw/*294.68*/("""
            activate();
        """),format.raw/*296.9*/("""}"""),format.raw/*296.10*/(""")
    </script>
    <script src=""""),_display_(Seq[Any](/*298.19*/routes/*298.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*298.67*/("""" type="text/javascript"></script>
    """),format.raw/*303.14*/("""
""")))})),format.raw/*304.2*/("""
"""))}
    }
    
    def render(displayedName:String,newsfeed:List[models.Event],profile:User,datasetsList:List[Dataset],dscomments:Map[UUID, Int],collectionsList:List[Collection],spacesList:List[ProjectSpace],deletePermission:Boolean,followers:List[scala.Tuple4[UUID, String, String, String]],followedUsers:List[scala.Tuple4[UUID, String, String, String]],followedFiles:List[scala.Tuple3[UUID, String, String]],followedDatasets:List[scala.Tuple3[UUID, String, String]],followedCollections:List[scala.Tuple3[UUID, String, String]],followedSpaces:List[scala.Tuple3[UUID, String, String]],ownProfile:Option[Boolean],userSelections:List[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(displayedName,newsfeed,profile,datasetsList,dscomments,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections)(user)
    
    def f:((String,List[models.Event],User,List[Dataset],Map[UUID, Int],List[Collection],List[ProjectSpace],Boolean,List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],Option[Boolean],List[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (displayedName,newsfeed,profile,datasetsList,dscomments,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections) => (user) => apply(displayedName,newsfeed,profile,datasetsList,dscomments,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:46:59 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/dashboard.scala.html
                    HASH: dfdde6b57a9e173d844026068b24f91b0596f27f
                    MATRIX: 1000->1|1839->632|1868->748|1905->750|1933->769|1973->771|2024->786|2039->792|2111->842|2196->891|2211->897|2280->944|2365->993|2380->999|2447->1044|2532->1093|2547->1099|2609->1139|2807->1301|2823->1308|2854->1317|2914->1341|2929->1347|2987->1383|3144->1504|3159->1510|3199->1528|3365->1658|3380->1664|3434->1696|3610->1836|3625->1842|3679->1874|3840->2185|4303->2612|4341->2634|4381->2636|4652->2871|4667->2877|4728->2916|4835->2987|4849->2992|4886->3007|5092->3177|5107->3183|5168->3222|5270->3288|5284->3293|5320->3307|5432->3383|5473->3402|5632->3525|5651->3535|5711->3573|5873->3703|5923->3717|5953->3738|5993->3740|6074->3785|6089->3791|6128->3808|6187->3831|6202->3837|6241->3853|6302->3877|6318->3883|6359->3901|6425->3930|6441->3936|6485->3957|6628->4068|6806->4210|6821->4216|6892->4265|6981->4318|6996->4324|7072->4378|7200->4478|7229->4479|7273->4495|7302->4496|7456->4622|7485->4623|7668->4778|7697->4779|7763->4817|7792->4818|7925->4923|7954->4924|8032->4974|8061->4975|8103->4989|8132->4990|8168->4999|8197->5000|8231->5007|8259->5008|8355->5076|8384->5077|8550->5214|8580->5215|8721->5327|8751->5328|8997->5545|9027->5546|9380->5870|9410->5871|9539->5970|9570->5971|9618->5990|9648->5991|9693->6007|9723->6008|10153->6409|10183->6410|10268->6466|10298->6467|10331->6471|10361->6472|10605->6687|10635->6688|10805->6829|10835->6830|10919->6885|10949->6886|10987->6896|11017->6897|11153->7004|11183->7005|11235->7028|11265->7029|11434->7169|11464->7170|11584->7261|11614->7262|11671->7290|11701->7291|11836->7397|11866->7398|11909->7412|11939->7413|11995->7440|12025->7441|12153->7540|12183->7541|12240->7569|12270->7570|12341->7612|12371->7613|12481->7694|12511->7695|12575->7730|12605->7731|12914->8011|12944->8012|13117->8156|13147->8157|13201->8182|13231->8183|13281->8204|13311->8205|13358->8223|13388->8224|13430->8237|13460->8238|13850->8600|13880->8601|13916->8609|13945->8610|14041->8677|14071->8678|14204->8783|14233->8784|14372->8894|14402->8895|14461->8925|14491->8926|14560->8967|14590->8968|14623->8973|14652->8974|14749->9042|14779->9043|14955->9190|14985->9191|15061->9238|15091->9239|15269->9388|15299->9389|15332->9393|15362->9394|15473->9476|15503->9477|15545->9490|15575->9491|15609->9497|15638->9498|15766->9597|15796->9598|16024->9797|16054->9798|16119->9834|16149->9835|16318->9975|16348->9976|16392->9991|16422->9992|16461->10003|16491->10004|16549->10033|16579->10034|16752->10179|16782->10180|16849->10218|16879->10219|17182->10493|17212->10494|17382->10635|17412->10636|17805->11000|17835->11001|17951->11088|17981->11089|18085->11164|18115->11165|18169->11190|18199->11191|18302->11265|18332->11266|18374->11279|18404->11280|18540->11387|18570->11388|18705->11494|18735->11495|18798->11530|18828->11531|18967->11641|18997->11642|19058->11675|19088->11676|19159->11710|19175->11716|19240->11758|19308->11878|19342->11880
                    LINES: 20->1|33->4|35->10|36->11|36->11|36->11|37->12|37->12|37->12|38->13|38->13|38->13|39->14|39->14|39->14|40->15|40->15|40->15|43->18|43->18|43->18|44->19|44->19|44->19|45->20|45->20|45->20|46->21|46->21|46->21|47->22|47->22|47->22|49->26|63->40|63->40|63->40|68->45|68->45|68->45|69->46|69->46|69->46|73->50|73->50|73->50|74->51|74->51|74->51|76->53|76->53|79->56|79->56|79->56|84->61|85->62|85->62|85->62|86->63|86->63|86->63|86->63|86->63|86->63|86->63|86->63|86->63|86->63|86->63|86->63|88->65|97->74|97->74|97->74|98->75|98->75|98->75|101->78|101->78|102->79|102->79|105->82|105->82|108->85|108->85|109->86|109->86|112->89|112->89|113->90|113->90|115->92|115->92|116->93|116->93|117->94|117->94|121->98|121->98|124->101|124->101|127->104|127->104|133->110|133->110|139->116|139->116|139->116|139->116|140->117|140->117|142->119|142->119|153->130|153->130|156->133|156->133|156->133|156->133|161->138|161->138|164->141|164->141|166->143|166->143|168->145|168->145|172->149|172->149|174->151|174->151|178->155|178->155|181->158|181->158|182->159|182->159|185->162|185->162|186->163|186->163|187->164|187->164|190->167|190->167|191->168|191->168|192->169|192->169|194->171|194->171|195->172|195->172|200->177|200->177|203->180|203->180|204->181|204->181|205->182|205->182|206->183|206->183|207->184|207->184|222->199|222->199|224->201|224->201|227->204|227->204|230->207|230->207|233->210|233->210|234->211|234->211|236->213|236->213|237->214|237->214|239->216|239->216|244->221|244->221|245->222|245->222|249->226|249->226|249->226|249->226|251->228|251->228|252->229|252->229|254->231|254->231|260->237|260->237|264->241|264->241|265->242|265->242|269->246|269->246|270->247|270->247|271->248|271->248|273->250|273->250|277->254|277->254|278->255|278->255|287->264|287->264|290->267|290->267|293->270|293->270|297->274|297->274|300->277|300->277|301->278|301->278|303->280|303->280|304->281|304->281|307->284|307->284|310->287|310->287|312->289|312->289|317->294|317->294|319->296|319->296|321->298|321->298|321->298|322->303|323->304
                    -- GENERATED --
                */
            