
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object home extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template17[String,List[models.Event],User,List[Dataset],Map[UUID, Int],List[Collection],List[ProjectSpace],Boolean,List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],Option[Boolean],List[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*2.2*/(displayedName: String,  newsfeed: List[models.Event], profile:User, datasetsList: List[Dataset],  dscomments: Map[UUID, Int], collectionsList:List[Collection], spacesList: List[ProjectSpace],
        deletePermission: Boolean,  followers: List[(UUID, String, String, String)], followedUsers: List[(UUID, String, String, String)], followedFiles: List[(UUID, String, String)],
        followedDatasets: List[(UUID, String, String)], followedCollections: List[(UUID, String, String)],
        followedSpaces: List[(UUID, String, String)], ownProfile: Option[Boolean], userSelections: List[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters

import api.Permission

import models.DatasetStatus

import play.api.i18n.Messages


Seq[Any](format.raw/*5.149*/("""

"""),format.raw/*11.1*/("""

"""),_display_(Seq[Any](/*13.2*/main(displayedName)/*13.21*/ {_display_(Seq[Any](format.raw/*13.23*/("""
    <script src=""""),_display_(Seq[Any](/*14.19*/routes/*14.25*/.Assets.at("javascripts/collectionListProcess.js"))),format.raw/*14.75*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*15.19*/routes/*15.25*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*15.72*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*16.19*/routes/*16.25*/.Assets.at("javascripts/spaceListProcess.js"))),format.raw/*16.70*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*17.19*/routes/*17.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*17.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*18.19*/routes/*18.25*/.Assets.at("javascripts/stickytabs/jquery.stickytabs.js"))),format.raw/*18.82*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*19.19*/routes/*19.25*/.Assets.at("javascripts/lib/masonry.pkgd.min.js"))),format.raw/*19.74*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*20.19*/routes/*20.25*/.Assets.at("javascripts/lib/imagesloaded.pkgd.min.js"))),format.raw/*20.79*/("""" type="text/javascript"></script>
    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*21.35*/routes/*21.41*/.Assets.at("stylesheets/MaterialDesign-Webfont-master/css/materialdesignicons.css"))),format.raw/*21.124*/("""">


    <div class="jumbotron"  style="padding-top:-80px">

        <div class="row">
            <div class="row-offcanvas row-offcanvas-left">
                <!-- sidebar -->
                <!-- <h2>My Dashboard</h2> -->
                <div class="column col-sm-6 col-xs-1 sidebar-offcanvas" id="sidebar">
                    <ul class="nav" id="menu">
                        <div class="panel" id="tree">
                        </div>
                    </ul>
                    <div class="leg row">
                            <fieldset>
                                <h6><span class="label"><img src=""""),_display_(Seq[Any](/*37.68*/routes/*37.74*/.Assets.at("images/home.png"))),format.raw/*37.103*/("""" class="img"></span>Space
                                <span class="label"><img src=""""),_display_(Seq[Any](/*38.64*/routes/*38.70*/.Assets.at("images/folder_new.png"))),format.raw/*38.105*/(""""  class="img"></span>Collection
                                <span class="label"><img src=""""),_display_(Seq[Any](/*39.64*/routes/*39.70*/.Assets.at("images/folder.gif"))),format.raw/*39.101*/("""" class="img"></span>Dataset
                                <span class="label"><img src=""""),_display_(Seq[Any](/*40.64*/routes/*40.70*/.Assets.at("images/file.png"))),format.raw/*40.99*/("""" class="img"></span>File</h6>
                            </fieldset>
                    </div>
                </div>
                <!-- /sidebar -->

                <!-- main right col -->
                <div class="column-main col-sm-6 col-xs-11" id="main">
                    <p><a href="#" data-toggle="offcanvas"><i class="glyphicon glyphicon-chevron-left glyphicon-2x"></i></a></p>
                    <!-- <p>
                        Main content...
                    </p> -->
                    <iframe seamless frameBorder="0" id="preview" allowtransparency="true" style="background-color: transparent;">
                    </iframe>



                </div>
                <!-- /main -->
            </div>
        </div>
        </div>

        <a href="#" id="infoPop" data-toggle="popover" title="Popover Header" data-content="Some content inside the popover"></a>


            </div>

        </div>
    </div>

        <div class="modal fade" id="collectionModal" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Create Collection</h4>
                </div>
                <div>
                    <iframe seamless class="modal-body" style="height:calc(100vh - 30vh); width: 100%;" frameBorder="0" id="previewCollection">
                    </iframe>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" id="collbtn" data-dismiss="modal">Close</button>
                </div>
              </div>

            </div>
          </div>

          <div class="modal fade" id="datasetModal" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Create Dataset</h4>
                </div>

                <div class="modal-body" style="padding-top: 0px; padding-bottom: 0px">
                    <iframe seamless class="panel panel-default" style="height:calc(100vh - 30vh); width: 100%;" frameBorder="0" id="previewDataset">
                    </iframe>
                  </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" id="databtn" data-dismiss="modal">Close</button>
                </div>
              </div>

            </div>
          </div>

          <div class="modal fade" id="addFilesModal" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Add Files</h4>
                </div>
                <iframe seamless class="modal-body" style="height:calc(100vh - 30vh); width: 100%;" frameBorder="0" id="previewFile">
                </iframe>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" id="filebtn" data-dismiss="modal">Close</button>
                </div>
              </div>

            </div>
        </div>

    <div id="metadataModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

                <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dataset Metadata</h4>
                </div>
                <div id="custMenu">
                    <div class="form-group padTop">
                        <div class="showTemplates">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label>Select a template: (optional)</label><br />
                                        <select class="form-control" id="treeTemplates"></select><br />
<!--                                         <label>Choose a name for your template:</label>
 --><!--                                         <input type="text" class="form-control" id="templateName" placeholder="<Example> Sample Name, PECVD Oxide, Diffusion" required><br />
 -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="metaDataSettings">
                </div>

                    <!--             <div class="modal-body2" style="padding:20px;">
            </div>
 -->            <div class="templateData modal-body">
                    <!--Template boxes will be added here -->
                <br />
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success pull-left" id="addMetaData">Add New</button>

                    <button type="button" class="btn btn-success" id="submitMetaData" data-dismiss="modal">Submit</button>

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <style>

        /** CSS Styles for good adjustment */
        .col-xs-11, .col-sm-6 """),format.raw/*184.31*/("""{"""),format.raw/*184.32*/("""
            padding-left: 7.5px;
        """),format.raw/*186.9*/("""}"""),format.raw/*186.10*/("""

        .jumbotron """),format.raw/*188.20*/("""{"""),format.raw/*188.21*/("""
            padding-top: 10px;
        """),format.raw/*190.9*/("""}"""),format.raw/*190.10*/("""

        body """),format.raw/*192.14*/("""{"""),format.raw/*192.15*/("""
            overflow-x:hidden;
        """),format.raw/*194.9*/("""}"""),format.raw/*194.10*/("""

        .panel """),format.raw/*196.16*/("""{"""),format.raw/*196.17*/("""
            background-color: transparent;
            border : none;
        """),format.raw/*199.9*/("""}"""),format.raw/*199.10*/("""

        .wrapper, .row """),format.raw/*201.24*/("""{"""),format.raw/*201.25*/("""
           height: 100%;
        """),format.raw/*203.9*/("""}"""),format.raw/*203.10*/("""

        .wrapper:before, .wrapper:after,
        .column:before, .column:after """),format.raw/*206.39*/("""{"""),format.raw/*206.40*/("""
            content: "";
            display: table;
        """),format.raw/*209.9*/("""}"""),format.raw/*209.10*/("""

        .wrapper:after,
        .column:after """),format.raw/*212.23*/("""{"""),format.raw/*212.24*/("""
            clear: both;
        """),format.raw/*214.9*/("""}"""),format.raw/*214.10*/("""

        #sidebar """),format.raw/*216.18*/("""{"""),format.raw/*216.19*/("""
            padding-left: 0;
            float: left;
            min-height: 100%;
            padding-right: 0;
        """),format.raw/*221.9*/("""}"""),format.raw/*221.10*/("""

        #sidebar .collapse.in """),format.raw/*223.31*/("""{"""),format.raw/*223.32*/("""
            display: inline;
        """),format.raw/*225.9*/("""}"""),format.raw/*225.10*/("""

        #sidebar > .nav>li>a """),format.raw/*227.30*/("""{"""),format.raw/*227.31*/("""
            white-space: nowrap;
            overflow: hidden;
        """),format.raw/*230.9*/("""}"""),format.raw/*230.10*/("""

        #main """),format.raw/*232.15*/("""{"""),format.raw/*232.16*/("""
            /* padding: 15px; */
            left: 0;
        """),format.raw/*235.9*/("""}"""),format.raw/*235.10*/("""

        #previewCollection """),format.raw/*237.28*/("""{"""),format.raw/*237.29*/("""
            padding-top: 0px;
        """),format.raw/*239.9*/("""}"""),format.raw/*239.10*/("""

        legend """),format.raw/*241.16*/("""{"""),format.raw/*241.17*/("""
            display: block;
            padding-left: 2px;
            padding-right: 2px;
            border: none;
        """),format.raw/*246.9*/("""}"""),format.raw/*246.10*/("""

        #tree """),format.raw/*248.15*/("""{"""),format.raw/*248.16*/("""
            margin-bottom: 0px;
        """),format.raw/*250.9*/("""}"""),format.raw/*250.10*/("""

        .jumbotron p """),format.raw/*252.22*/("""{"""),format.raw/*252.23*/("""
            padding-bottom: 0px;
            margin-bottom: -5px;
        """),format.raw/*255.9*/("""}"""),format.raw/*255.10*/("""

        /** Smaller Devices */

        @media screen and (max-width: 768px) """),format.raw/*259.47*/("""{"""),format.raw/*259.48*/("""
            .fa-2x """),format.raw/*260.20*/("""{"""),format.raw/*260.21*/("""
                font-size: 1.5em;
            """),format.raw/*262.13*/("""}"""),format.raw/*262.14*/("""
            .jumbotron """),format.raw/*263.24*/("""{"""),format.raw/*263.25*/("""
                padding-top: 5px !important;
            """),format.raw/*265.13*/("""}"""),format.raw/*265.14*/("""
            .col-xs-1 """),format.raw/*266.23*/("""{"""),format.raw/*266.24*/("""
                width: auto !important;
            """),format.raw/*268.13*/("""}"""),format.raw/*268.14*/("""

            #sidebar """),format.raw/*270.22*/("""{"""),format.raw/*270.23*/("""
                min-width: 90%;
            """),format.raw/*272.13*/("""}"""),format.raw/*272.14*/("""
            .row-offcanvas.active #main"""),format.raw/*273.40*/("""{"""),format.raw/*273.41*/("""
                width: auto !important;
            """),format.raw/*275.13*/("""}"""),format.raw/*275.14*/("""

            #main """),format.raw/*277.19*/("""{"""),format.raw/*277.20*/("""
                width: 1%;
                left: 0;
                padding-right: 0px;
            """),format.raw/*281.13*/("""}"""),format.raw/*281.14*/("""

            .wrapper, .row """),format.raw/*283.28*/("""{"""),format.raw/*283.29*/("""
                height: 100%;
                padding-left: 5%;

                /* margin-left:0;
                margin-right:-5px; */
            """),format.raw/*289.13*/("""}"""),format.raw/*289.14*/("""

            /* .active, .row """),format.raw/*291.30*/("""{"""),format.raw/*291.31*/("""
                padding-right: 5% !important;
            """),format.raw/*293.13*/("""}"""),format.raw/*293.14*/(""" */

            #tree """),format.raw/*295.19*/("""{"""),format.raw/*295.20*/("""
                height: calc(100vh - 20vh);
                min-width: 90%;
            """),format.raw/*298.13*/("""}"""),format.raw/*298.14*/("""

            #preview """),format.raw/*300.22*/("""{"""),format.raw/*300.23*/("""
                height: calc(100vh - 25vh);;
                width: 100%;
                float:right;
            """),format.raw/*304.13*/("""}"""),format.raw/*304.14*/("""

            #sidebar .visible-xs """),format.raw/*306.34*/("""{"""),format.raw/*306.35*/("""
               display:inline !important;
            """),format.raw/*308.13*/("""}"""),format.raw/*308.14*/("""

            .row-offcanvas """),format.raw/*310.28*/("""{"""),format.raw/*310.29*/("""
               -webkit-transition: all 0.4s ease-in-out;
               -moz-transition: all 0.4s ease-in-out;
               transition: all 0.4s ease-in-out;
            """),format.raw/*314.13*/("""}"""),format.raw/*314.14*/("""

            .row-offcanvas-left.active """),format.raw/*316.40*/("""{"""),format.raw/*316.41*/("""
               left: 90%;
            """),format.raw/*318.13*/("""}"""),format.raw/*318.14*/("""

            .row-offcanvas-left.active .sidebar-offcanvas """),format.raw/*320.59*/("""{"""),format.raw/*320.60*/("""
               left: -90%;
               position: absolute;
               top: 0;
               width: 45%;
            """),format.raw/*325.13*/("""}"""),format.raw/*325.14*/("""
        """),format.raw/*326.9*/("""}"""),format.raw/*326.10*/("""

        /** Larger Devices */

        @media screen and (min-width: 768px) """),format.raw/*330.47*/("""{"""),format.raw/*330.48*/("""
            .row-offcanvas """),format.raw/*331.28*/("""{"""),format.raw/*331.29*/("""
                -webkit-transition: all 0.25s ease-out;
                -moz-transition: all 0.25s ease-out;
                transition: all 0.25s ease-out;
            """),format.raw/*335.13*/("""}"""),format.raw/*335.14*/("""

            /* .active """),format.raw/*337.24*/("""{"""),format.raw/*337.25*/("""
            
            """),format.raw/*339.13*/("""}"""),format.raw/*339.14*/(""" */

            .row-offcanvas-left.active """),format.raw/*341.40*/("""{"""),format.raw/*341.41*/("""
                left: 90%;
            """),format.raw/*343.13*/("""}"""),format.raw/*343.14*/("""

            .leg.row """),format.raw/*345.22*/("""{"""),format.raw/*345.23*/("""
                margin-left: 0px;
            """),format.raw/*347.13*/("""}"""),format.raw/*347.14*/("""

            .row-offcanvas-left.active .sidebar-offcanvas """),format.raw/*349.59*/("""{"""),format.raw/*349.60*/("""
                left: -90%;
                position: absolute;
                top: 0;
                width: 3%;
                text-align: center;
                min-width:42px;
            """),format.raw/*356.13*/("""}"""),format.raw/*356.14*/("""

            #main """),format.raw/*358.19*/("""{"""),format.raw/*358.20*/("""
                left: 0;
            """),format.raw/*360.13*/("""}"""),format.raw/*360.14*/("""
        """),format.raw/*361.9*/("""}"""),format.raw/*361.10*/("""


        /* Webix Styles */

        .my_style  """),format.raw/*366.20*/("""{"""),format.raw/*366.21*/("""
            font-family: "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Geneva, Verdana, sans-serif;
        """),format.raw/*368.9*/("""}"""),format.raw/*368.10*/("""
        .webix_tree_home """),format.raw/*369.26*/("""{"""),format.raw/*369.27*/("""
            background-image: url('./../../assets/images/home.png');
        """),format.raw/*371.9*/("""}"""),format.raw/*371.10*/("""
        .webix_tree_puzzle """),format.raw/*372.28*/("""{"""),format.raw/*372.29*/("""
            background-image: url("./../../assets/images/folder_new.png");
        """),format.raw/*374.9*/("""}"""),format.raw/*374.10*/("""
        .webix_tree_xyz """),format.raw/*375.25*/("""{"""),format.raw/*375.26*/("""
                background-image: url("./../../assets/images/file.png");
        """),format.raw/*377.9*/("""}"""),format.raw/*377.10*/("""
        .webix_tree_folder """),format.raw/*378.28*/("""{"""),format.raw/*378.29*/("""
            background-image: url('./../../assets/images/folder.gif');
        """),format.raw/*380.9*/("""}"""),format.raw/*380.10*/("""


        #tree """),format.raw/*383.15*/("""{"""),format.raw/*383.16*/("""
            height: calc(100vh - 20vh);
            width: 100%;
            float:left;
            overflow-y:auto;
        """),format.raw/*388.9*/("""}"""),format.raw/*388.10*/("""

        #preview """),format.raw/*390.18*/("""{"""),format.raw/*390.19*/("""
            height: calc(100vh - 25vh);;
            width: 100%;
            float:right;
        """),format.raw/*394.9*/("""}"""),format.raw/*394.10*/("""

        /*.webix_tree_item .check, */
        /*.webix_tree_item.webix_selected .check """),format.raw/*397.50*/("""{"""),format.raw/*397.51*/("""*/
            /*float: left; */
            /*line-height:inherit;*/
            /*width: 20px;*/
            /*padding: 0px;*/
            /*margin:0px 0px 0px 0px;*/
            /*background-color: transparent;*/
            /*color: #666;*/
        /*"""),format.raw/*405.11*/("""}"""),format.raw/*405.12*/("""*/

    </style>
    <script src=""""),_display_(Seq[Any](/*408.19*/routes/*408.25*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*408.67*/("""" type="text/javascript"></script>
    <script>
            // SIDEBAR
            $('[data-toggle=offcanvas]').click(function() """),format.raw/*411.59*/("""{"""),format.raw/*411.60*/("""
                $('.row').toggleClass('active');
                $('.row-offcanvas').toggleClass('active');
                $('.collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');
                $('.column').toggleClass('col-sm-6');
                $('.column-main').toggleClass('col-xs-11').toggleClass('panel').toggleClass('col-sm-6');
                $('.glyphicon').toggleClass('glyphicon-chevron-left').toggleClass('glyphicon-chevron-right');
            """),format.raw/*418.13*/("""}"""),format.raw/*418.14*/(""");

            var idsOfSelected = []
            // Functionality for editing node names - using prototype EditAbility
            webix.protoUI("""),format.raw/*422.27*/("""{"""),format.raw/*422.28*/("""
                name:"edittree"
            """),format.raw/*424.13*/("""}"""),format.raw/*424.14*/(""", webix.EditAbility, webix.ui.tree);


            var menu = [
                    "Delete",
                    "Download"];

            // Implementation of WEBIX TREE below
            tree = webix.ui("""),format.raw/*432.29*/("""{"""),format.raw/*432.30*/("""
                // Using tree container
                container: "tree",
                // Dividing into rows - splitting search, sort, and tree
                rows:[
                """),format.raw/*437.17*/("""{"""),format.raw/*437.18*/(""" view:"toolbar", height:95, rows:[
                    """),format.raw/*438.21*/("""{"""),format.raw/*438.22*/(""" cols:[
                        """),format.raw/*439.25*/("""{"""),format.raw/*439.26*/("""view:"text", placeholder:"Search...""""),format.raw/*439.62*/("""}"""),format.raw/*439.63*/(""",
                        """),format.raw/*440.25*/("""{"""),format.raw/*440.26*/("""view:"icon", icon:"mdi mdi-help-circle", width:50,
                        click:function() """),format.raw/*441.42*/("""{"""),format.raw/*441.43*/("""
                            webix.ui("""),format.raw/*442.38*/("""{"""),format.raw/*442.39*/("""
                                view:"window",
                                id:"my_win",
                                head:"Info",
                                width: 300,
                                height: 150,
                                position:"center",
                                move:true,
                                resize: true,
                                head:"""),format.raw/*451.38*/("""{"""),format.raw/*451.39*/("""
                                    view:"button", label:"Close", width:70, click:("$$('my_win').close();")

                                """),format.raw/*454.33*/("""}"""),format.raw/*454.34*/(""",
                                body:"""),format.raw/*455.38*/("""{"""),format.raw/*455.39*/("""
                                    template:"You may search for whatever is already loaded in the tree-view. Owners may right click object to rename/delete, drag and drop to move."
                                """),format.raw/*457.33*/("""}"""),format.raw/*457.34*/("""
                            """),format.raw/*458.29*/("""}"""),format.raw/*458.30*/(""").show();
                        """),format.raw/*459.25*/("""}"""),format.raw/*459.26*/("""}"""),format.raw/*459.27*/("""
                    ]
                    """),format.raw/*461.21*/("""}"""),format.raw/*461.22*/(""",
                    """),format.raw/*462.21*/("""{"""),format.raw/*462.22*/("""cols:[
                    """),format.raw/*463.21*/("""{"""),format.raw/*463.22*/("""
                        view:"button",
                             value:"Sort",
                             //type:"image",
                             //image:"sort_a_z.png",
                             //label:"Sort",
                             click:function()"""),format.raw/*469.46*/("""{"""),format.raw/*469.47*/("""
                                $$("tree").sort("#value#","asc")
                            """),format.raw/*471.29*/("""}"""),format.raw/*471.30*/("""
                    """),format.raw/*472.21*/("""}"""),format.raw/*472.22*/(""",
                    """),format.raw/*473.21*/("""{"""),format.raw/*473.22*/("""
                        view:"button",
                        value:"Delete",
                        click:function()"""),format.raw/*476.41*/("""{"""),format.raw/*476.42*/("""
                            var len = idsOfSelected.length;
                            for(var i = 0; i < len; i++) """),format.raw/*478.58*/("""{"""),format.raw/*478.59*/("""
                                var nodeId = idsOfSelected[0];

                                var dataType = $$("tree").getItem(nodeId).type;
                                if (dataType == "collection") """),format.raw/*482.63*/("""{"""),format.raw/*482.64*/("""
                                    $$("tree").remove(nodeId);
                                    url = jsRoutes.api.Collections.removeCollectionAndContents(nodeId).url;
                                    idsOfSelected.splice(idsOfSelected.indexOf(nodeId), 1);
                                """),format.raw/*486.33*/("""}"""),format.raw/*486.34*/("""else if(dataType == "dataset") """),format.raw/*486.65*/("""{"""),format.raw/*486.66*/("""
                                    $$("tree").remove(nodeId);
                                    url = jsRoutes.api.Datasets.deleteDataset(nodeId).url;
                                    idsOfSelected.splice(idsOfSelected.indexOf(nodeId), 1);
                                """),format.raw/*490.33*/("""}"""),format.raw/*490.34*/("""else if(dataType == "file")"""),format.raw/*490.61*/("""{"""),format.raw/*490.62*/("""
                                    $$("tree").remove(nodeId);
                                    url = jsRoutes.api.Files.removeFile(nodeId).url;
                                    idsOfSelected.splice(idsOfSelected.indexOf(nodeId), 1);
                                """),format.raw/*494.33*/("""}"""),format.raw/*494.34*/(""" else """),format.raw/*494.40*/("""{"""),format.raw/*494.41*/("""
                                    idsOfSelected.splice(idsOfSelected.indexOf(nodeId), 1);
                                """),format.raw/*496.33*/("""}"""),format.raw/*496.34*/("""
                                if(dataType == "collection" || dataType == "dataset") """),format.raw/*497.87*/("""{"""),format.raw/*497.88*/("""
                                    $.ajax("""),format.raw/*498.44*/("""{"""),format.raw/*498.45*/("""
                                        url : url,
                                        type : "DELETE"
                                    """),format.raw/*501.37*/("""}"""),format.raw/*501.38*/(""")
                                """),format.raw/*502.33*/("""}"""),format.raw/*502.34*/(""" else if(dataType == "file")"""),format.raw/*502.62*/("""{"""),format.raw/*502.63*/("""
                                    $.ajax("""),format.raw/*503.44*/("""{"""),format.raw/*503.45*/("""
                                        url : url,
                                        type : "POST"
                                    """),format.raw/*506.37*/("""}"""),format.raw/*506.38*/(""")
                                """),format.raw/*507.33*/("""}"""),format.raw/*507.34*/(""" else if(dataType == "space") """),format.raw/*507.64*/("""{"""),format.raw/*507.65*/("""
                                    webix.message("You cannot delete a space!");
                                """),format.raw/*509.33*/("""}"""),format.raw/*509.34*/("""
                                """),format.raw/*510.33*/("""}"""),format.raw/*510.34*/("""
                            """),format.raw/*511.29*/("""}"""),format.raw/*511.30*/("""
                    """),format.raw/*512.21*/("""}"""),format.raw/*512.22*/(""",
                    ]"""),format.raw/*513.22*/("""}"""),format.raw/*513.23*/("""
                ]"""),format.raw/*514.18*/("""}"""),format.raw/*514.19*/(""",

                    // Tree view
                    """),format.raw/*517.21*/("""{"""),format.raw/*517.22*/("""
                        view:"edittree",
                        type:"lineTree",
                        drag: true,
                        multiselect: "level",
                        select:"row",
                        css: "my_style",
                        editable:true,
                        editValue:"value",
                        editor:"text",
                        editaction:"dblclick",
                        id:"tree",
                        // URL for parent
                        url:'api/fulltree/getChildrenOfNode?nodeType=root',
                        on: """),format.raw/*531.29*/("""{"""),format.raw/*531.30*/("""
                            onBeforeContextMenu: function(id) """),format.raw/*532.63*/("""{"""),format.raw/*532.64*/("""
                                console.log(window.innerWidth)
                            if(window.innerWidth<800) """),format.raw/*534.55*/("""{"""),format.raw/*534.56*/("""
                                return false;
                            """),format.raw/*536.29*/("""}"""),format.raw/*536.30*/("""
                            webix.delay(function()"""),format.raw/*537.51*/("""{"""),format.raw/*537.52*/("""
                            this.select(id);"""),format.raw/*538.45*/("""}"""),format.raw/*538.46*/(""",this)
                            """),format.raw/*539.29*/("""}"""),format.raw/*539.30*/("""
                        """),format.raw/*540.25*/("""}"""),format.raw/*540.26*/(""",
                        onContext:"""),format.raw/*541.35*/("""{"""),format.raw/*541.36*/("""

                        """),format.raw/*543.25*/("""}"""),format.raw/*543.26*/(""",
                        ready:function() """),format.raw/*544.42*/("""{"""),format.raw/*544.43*/("""
                            var state = webix.storage.local.get("state");
                            if (state) """),format.raw/*546.40*/("""{"""),format.raw/*546.41*/("""
                                this.setState(state);
                            """),format.raw/*548.29*/("""}"""),format.raw/*548.30*/("""
                            this.closeAll();
                        """),format.raw/*550.25*/("""}"""),format.raw/*550.26*/(""",
                        type:"""),format.raw/*551.30*/("""{"""),format.raw/*551.31*/("""
                            myCustomCheckbox:function(obj, common)"""),format.raw/*552.67*/("""{"""),format.raw/*552.68*/("""
                                if (obj.indeterminate && !obj.checked)
                                    return "<span class='webix_icon check mdi mdi-checkbox-intermediate'></span>";
                                return "<span class='webix_icon check mdi mdi-checkbox-"+(obj.checked?"marked":"blank")+"-outline'></span>";

                            """),format.raw/*557.29*/("""}"""),format.raw/*557.30*/("""
                        """),format.raw/*558.25*/("""}"""),format.raw/*558.26*/(""",
                        onClick:"""),format.raw/*559.33*/("""{"""),format.raw/*559.34*/("""
                            check:function(e, id)"""),format.raw/*560.50*/("""{"""),format.raw/*560.51*/("""
                                var checkMark = this.getItem(id).checked;
                                console.log(checkMark)
                                if (checkMark) """),format.raw/*563.48*/("""{"""),format.raw/*563.49*/("""
                                    this.uncheckItem(id);
                                    idsOfSelected.splice(idsOfSelected.indexOf(id), 1);
                                """),format.raw/*566.33*/("""}"""),format.raw/*566.34*/(""" else """),format.raw/*566.40*/("""{"""),format.raw/*566.41*/("""
                                    this.checkItem(id);
                                    idsOfSelected.push(id);
                                """),format.raw/*569.33*/("""}"""),format.raw/*569.34*/("""
                            """),format.raw/*570.29*/("""}"""),format.raw/*570.30*/("""
                        """),format.raw/*571.25*/("""}"""),format.raw/*571.26*/(""",
                        // span for providing the correct selection
                        template: """"),format.raw/*573.36*/("""{"""),format.raw/*573.37*/("""common.icon()"""),format.raw/*573.50*/("""}"""),format.raw/*573.51*/("""{"""),format.raw/*573.52*/("""common.myCustomCheckbox()"""),format.raw/*573.77*/("""}"""),format.raw/*573.78*/("""{"""),format.raw/*573.79*/("""common.folder()"""),format.raw/*573.94*/("""}"""),format.raw/*573.95*/(""" <span> #value# </span>",
                    """),format.raw/*574.21*/("""}"""),format.raw/*574.22*/("""
                ]
            """),format.raw/*576.13*/("""}"""),format.raw/*576.14*/(""");

            $('#collbtn').on('click', function(event) """),format.raw/*578.55*/("""{"""),format.raw/*578.56*/("""
                location.reload();
            """),format.raw/*580.13*/("""}"""),format.raw/*580.14*/(""");

            $('#databtn').on('click', function(event) """),format.raw/*582.55*/("""{"""),format.raw/*582.56*/("""
                location.reload();
            """),format.raw/*584.13*/("""}"""),format.raw/*584.14*/(""");

            $('#filebtn').on('click', function(event) """),format.raw/*586.55*/("""{"""),format.raw/*586.56*/("""
                location.reload();
            """),format.raw/*588.13*/("""}"""),format.raw/*588.14*/(""");


            // Webix context menu options
            contextMenu = webix.ui("""),format.raw/*592.36*/("""{"""),format.raw/*592.37*/("""
                view:"contextmenu",
                id: "context-menu",
                data: [
                """),format.raw/*596.17*/("""{"""),format.raw/*596.18*/(""" id:"1",value:"Create...", submenu:[
                    """),format.raw/*597.21*/("""{"""),format.raw/*597.22*/("""id: "1.1", value:"Collection""""),format.raw/*597.51*/("""}"""),format.raw/*597.52*/(""",
                    """),format.raw/*598.21*/("""{"""),format.raw/*598.22*/("""id: "1.2", value:"Dataset""""),format.raw/*598.48*/("""}"""),format.raw/*598.49*/("""
                ]"""),format.raw/*599.18*/("""}"""),format.raw/*599.19*/(""",
                """),format.raw/*600.17*/("""{"""),format.raw/*600.18*/("""id:"5", value: "Attach/View Template""""),format.raw/*600.55*/("""}"""),format.raw/*600.56*/(""",
                """),format.raw/*601.17*/("""{"""),format.raw/*601.18*/("""id:"2", value:"Add files...""""),format.raw/*601.46*/("""}"""),format.raw/*601.47*/(""",
                """),format.raw/*602.17*/("""{"""),format.raw/*602.18*/("""id:"3", value:"Download""""),format.raw/*602.42*/("""}"""),format.raw/*602.43*/(""",
                """),format.raw/*603.17*/("""{"""),format.raw/*603.18*/("""id:"4", value:"Delete""""),format.raw/*603.40*/("""}"""),format.raw/*603.41*/("""
                ],
                master:$$("tree")
            """),format.raw/*606.13*/("""}"""),format.raw/*606.14*/(""");

            $$("tree").attachEvent("onBeforeContextMenu", function(id, e, node)"""),format.raw/*608.80*/("""{"""),format.raw/*608.81*/("""
                var dataType = this.getItem(id).type;
                if(dataType=="space") """),format.raw/*610.39*/("""{"""),format.raw/*610.40*/("""
                    contextMenu.enableItem("1");
                    contextMenu.enableItem("1.1");
                    contextMenu.enableItem("1.2");
                    contextMenu.disableItem("5");
                    contextMenu.disableItem("2");
                    contextMenu.disableItem("3");
                    contextMenu.disableItem("4");
                """),format.raw/*618.17*/("""}"""),format.raw/*618.18*/(""" else if (dataType=="collection") """),format.raw/*618.52*/("""{"""),format.raw/*618.53*/("""
                    contextMenu.enableItem("1");
                    contextMenu.enableItem("1.1");
                    contextMenu.enableItem("1.2");
                    contextMenu.disableItem("5");
                    contextMenu.disableItem("2");
                    contextMenu.enableItem("3");
                    contextMenu.enableItem("4");
                """),format.raw/*626.17*/("""}"""),format.raw/*626.18*/(""" else if (dataType=="dataset") """),format.raw/*626.49*/("""{"""),format.raw/*626.50*/("""
                    contextMenu.enableItem("2");
                    contextMenu.enableItem("3");
                    contextMenu.enableItem("4");
                    contextMenu.enableItem("1");
                    contextMenu.disableItem("1.1");
                    contextMenu.disableItem("1.2");
                    contextMenu.enableItem("5");
                """),format.raw/*634.17*/("""}"""),format.raw/*634.18*/(""" else if (dataType=="file") """),format.raw/*634.46*/("""{"""),format.raw/*634.47*/("""
                    contextMenu.disableItem("1");
                    contextMenu.disableItem("2");
                    contextMenu.enableItem("3");
                    contextMenu.enableItem("4");
                    contextMenu.disableItem("5");
                """),format.raw/*640.17*/("""}"""),format.raw/*640.18*/("""
            """),format.raw/*641.13*/("""}"""),format.raw/*641.14*/(""");


            $$("context-menu").attachEvent("onMenuItemClick", function(id)"""),format.raw/*644.75*/("""{"""),format.raw/*644.76*/("""
                var url = "";
                var nodeId = $$("tree").getSelectedId();
                var dataType = $$("tree").getItem(nodeId).type;
                // id 1.1 for create collection
                if(id == "1.1" && dataType=="space") """),format.raw/*649.54*/("""{"""),format.raw/*649.55*/("""
                    $("#collectionModal").modal();
                    document.getElementById('previewCollection').src = "./collections/new?"+dataType+"="+nodeId;
                """),format.raw/*652.17*/("""}"""),format.raw/*652.18*/("""
                else if (id=="1.1" && dataType =="collection") """),format.raw/*653.64*/("""{"""),format.raw/*653.65*/("""
                    $("#collectionModal").modal();
                    document.getElementById('previewCollection').src = "./collections/"+nodeId+"/newchildCollection";
                """),format.raw/*656.17*/("""}"""),format.raw/*656.18*/("""
                // id 1.2 for create dataset
                else if (id == "1.2") """),format.raw/*658.39*/("""{"""),format.raw/*658.40*/("""
                    $("#datasetModal").modal();
                    document.getElementById('previewDataset').src = "./datasets/new?"+dataType+"="+nodeId;
                """),format.raw/*661.17*/("""}"""),format.raw/*661.18*/(""" else if (id=="5") """),format.raw/*661.37*/("""{"""),format.raw/*661.38*/("""
                    // get4ceedMetaData(nodeId);
                    getTemplates(nodeId);
                    // getTemplate(nodeId);
                    $("#metadataModal").modal();
                """),format.raw/*666.17*/("""}"""),format.raw/*666.18*/("""
                // id 2 for add files
                else if (id == "2") """),format.raw/*668.37*/("""{"""),format.raw/*668.38*/("""
                    $("#addFilesModal").modal();
                    document.getElementById('previewFile').src = "./datasets/"+nodeId+"/addFiles";
                """),format.raw/*671.17*/("""}"""),format.raw/*671.18*/(""" // id 3 for download
                else if(id == "3") """),format.raw/*672.36*/("""{"""),format.raw/*672.37*/("""
                    if (dataType == "collection") """),format.raw/*673.51*/("""{"""),format.raw/*673.52*/("""
                        var downloadRoute = jsRoutes.api.Collections.download(nodeId).url;
                        window.open(downloadRoute, '_blank');
                    """),format.raw/*676.21*/("""}"""),format.raw/*676.22*/("""else if(dataType == "dataset") """),format.raw/*676.53*/("""{"""),format.raw/*676.54*/("""
                        var downloadRoute = jsRoutes.api.Datasets.download(nodeId).url;
                        window.open(downloadRoute, '_blank');
                    """),format.raw/*679.21*/("""}"""),format.raw/*679.22*/("""else if(dataType == "file")"""),format.raw/*679.49*/("""{"""),format.raw/*679.50*/("""
                        var downloadRoute = jsRoutes.api.Files.download(nodeId).url;
                        window.open(downloadRoute, '_blank');
                    """),format.raw/*682.21*/("""}"""),format.raw/*682.22*/("""

                """),format.raw/*684.17*/("""}"""),format.raw/*684.18*/(""" // id 3 for delete
                else if(id == "4") """),format.raw/*685.36*/("""{"""),format.raw/*685.37*/("""
                    var nodeId = $$("tree").getSelectedId();
                    var dataType = $$("tree").getItem(nodeId).type;
                    if (dataType == "collection") """),format.raw/*688.51*/("""{"""),format.raw/*688.52*/("""
                        $$("tree").remove(nodeId);
                        url = jsRoutes.api.Collections.removeCollectionAndContents(nodeId).url;
                    """),format.raw/*691.21*/("""}"""),format.raw/*691.22*/("""else if(dataType == "dataset") """),format.raw/*691.53*/("""{"""),format.raw/*691.54*/("""
                        $$("tree").remove(nodeId);
                        url = jsRoutes.api.Datasets.deleteDataset(nodeId).url;
                    """),format.raw/*694.21*/("""}"""),format.raw/*694.22*/("""else if(dataType == "file")"""),format.raw/*694.49*/("""{"""),format.raw/*694.50*/("""
                        $$("tree").remove(nodeId);
                        url = jsRoutes.api.Files.removeFile(nodeId).url;
                    """),format.raw/*697.21*/("""}"""),format.raw/*697.22*/("""
                    if(dataType == "collection" || dataType == "dataset") """),format.raw/*698.75*/("""{"""),format.raw/*698.76*/("""
                        $.ajax("""),format.raw/*699.32*/("""{"""),format.raw/*699.33*/("""
                            url : url,
                            type : "DELETE"
                        """),format.raw/*702.25*/("""}"""),format.raw/*702.26*/(""")
                    """),format.raw/*703.21*/("""}"""),format.raw/*703.22*/(""" else if(dataType == "file")"""),format.raw/*703.50*/("""{"""),format.raw/*703.51*/("""
                        $.ajax("""),format.raw/*704.32*/("""{"""),format.raw/*704.33*/("""
                            url : url,
                            type : "POST"
                        """),format.raw/*707.25*/("""}"""),format.raw/*707.26*/(""")
                    """),format.raw/*708.21*/("""}"""),format.raw/*708.22*/(""" else """),format.raw/*708.28*/("""{"""),format.raw/*708.29*/("""
                        webix.message("You cannot delete a space!");
                    """),format.raw/*710.21*/("""}"""),format.raw/*710.22*/("""
                """),format.raw/*711.17*/("""}"""),format.raw/*711.18*/("""
            """),format.raw/*712.13*/("""}"""),format.raw/*712.14*/(""");


            $$("tree").attachEvent("onDataRequest", function (id) """),format.raw/*715.67*/("""{"""),format.raw/*715.68*/("""
                // Using getItem() to get all attributes of JSON object
                var itemType = this.getItem(id).type;
                this.parse(
                    // URL for children
                    webix.ajax().get("api/fulltree/getChildrenOfNode?nodeId="+id+"&nodeType="+itemType).then(function(data) """),format.raw/*720.125*/("""{"""),format.raw/*720.126*/("""
                        var dataReturned = data.json();
                        return """),format.raw/*722.32*/("""{"""),format.raw/*722.33*/("""parent:id, data:dataReturned"""),format.raw/*722.61*/("""}"""),format.raw/*722.62*/(""";
                """),format.raw/*723.17*/("""}"""),format.raw/*723.18*/("""));
                webix.storage.local.put("state", $$("tree").getState());
                return false;
            """),format.raw/*726.13*/("""}"""),format.raw/*726.14*/(""");

            $$("tree").attachEvent("onEditorChange",function(id, value) """),format.raw/*728.73*/("""{"""),format.raw/*728.74*/("""
                var dataType = this.getItem(id).type;
                var url = "";
                if (dataType == "collection")"""),format.raw/*731.46*/("""{"""),format.raw/*731.47*/("""
                        url = jsRoutes.api.Collections.updateCollectionName(id).url;
                """),format.raw/*733.17*/("""}"""),format.raw/*733.18*/(""" else if(dataType == "dataset")"""),format.raw/*733.49*/("""{"""),format.raw/*733.50*/("""
                        url = jsRoutes.api.Datasets.updateName(id).url;
                """),format.raw/*735.17*/("""}"""),format.raw/*735.18*/(""" else if(dataType == "file")"""),format.raw/*735.46*/("""{"""),format.raw/*735.47*/("""
                        url = jsRoutes.api.Files.updateFileName(id).url;
                """),format.raw/*737.17*/("""}"""),format.raw/*737.18*/(""" else"""),format.raw/*737.23*/("""{"""),format.raw/*737.24*/("""
                        url = "";
                """),format.raw/*739.17*/("""}"""),format.raw/*739.18*/("""

            $.ajax("""),format.raw/*741.20*/("""{"""),format.raw/*741.21*/("""
                        url: url,
                        type:"PUT",
                        data: JSON.stringify("""),format.raw/*744.46*/("""{"""),format.raw/*744.47*/("""name: value"""),format.raw/*744.58*/("""}"""),format.raw/*744.59*/("""),
                        dataType: "json",
                        beforeSend: function(xhr)"""),format.raw/*746.50*/("""{"""),format.raw/*746.51*/("""
                            xhr.setRequestHeader("Content-Type", "application/json");
                            xhr.setRequestHeader("Accept", "application/json");
                        """),format.raw/*749.25*/("""}"""),format.raw/*749.26*/("""
                    """),format.raw/*750.21*/("""}"""),format.raw/*750.22*/(""");
            """),format.raw/*751.13*/("""}"""),format.raw/*751.14*/(""");

            // Edit a space
            $$("tree").attachEvent("onEditorChange",function(id, value) """),format.raw/*754.73*/("""{"""),format.raw/*754.74*/("""
                var dataType = this.getItem(id).type;
                var url = "";
                if(dataType == "space") """),format.raw/*757.41*/("""{"""),format.raw/*757.42*/("""
                    url = jsRoutes.controllers.Spaces.updateSpace(id).url;
                    webix.ajax().get(url, """),format.raw/*759.43*/("""{"""),format.raw/*759.44*/(""" name:value"""),format.raw/*759.55*/("""}"""),format.raw/*759.56*/(""");
                """),format.raw/*760.17*/("""}"""),format.raw/*760.18*/("""
            """),format.raw/*761.13*/("""}"""),format.raw/*761.14*/(""");

            // Filtering based on search - solely based on value provided by user
            $$("$text1").attachEvent("onTimedKeyPress",function() """),format.raw/*764.67*/("""{"""),format.raw/*764.68*/("""
                $$("tree").filter("#value#",this.getValue());
            """),format.raw/*766.13*/("""}"""),format.raw/*766.14*/(""");


            // onItemClick: storing state, this is the area that acts as a single page application.
            $$("tree").attachEvent("onItemClick", function(id, e, node) """),format.raw/*770.73*/("""{"""),format.raw/*770.74*/("""
                var dataType = this.getItem(id).type;
                if (dataType === "collection")"""),format.raw/*772.47*/("""{"""),format.raw/*772.48*/("""
                    webix.storage.local.put("state", $$("tree").getState());
                    document.getElementById('preview').src = "./collection/"+id;
                    // window.location.href = "/collection/"+id+"";
                """),format.raw/*776.17*/("""}"""),format.raw/*776.18*/("""else if(dataType === "dataset")"""),format.raw/*776.49*/("""{"""),format.raw/*776.50*/("""
                    webix.storage.local.put("state", $$("tree").getState());
                    document.getElementById('preview').src = "./datasets/"+id;

                    // window.location.href = "/datasets/"+id+"";
                """),format.raw/*781.17*/("""}"""),format.raw/*781.18*/("""else if(dataType === "file")"""),format.raw/*781.46*/("""{"""),format.raw/*781.47*/("""
                    webix.storage.local.put("state", $$("tree").getState());
                    //window.location.href = "/files/"+id+"";
                    document.getElementById('preview').src = "./files/"+id;
                    //webix.ajax("./files/"+id);
                """),format.raw/*786.17*/("""}"""),format.raw/*786.18*/("""else if(dataType === "space")"""),format.raw/*786.47*/("""{"""),format.raw/*786.48*/("""
                    webix.storage.local.put("state", $$("tree").getState());
                    document.getElementById('preview').src = "./spaces/"+id;

                    // window.location.href = "/spaces/"+id+"";
                """),format.raw/*791.17*/("""}"""),format.raw/*791.18*/(""" else """),format.raw/*791.24*/("""{"""),format.raw/*791.25*/("""

                """),format.raw/*793.17*/("""}"""),format.raw/*793.18*/("""
            """),format.raw/*794.13*/("""}"""),format.raw/*794.14*/(""");

            // onItemClick: storing state
            $$("tree").attachEvent("onItemDblClick", function(id, e, node) """),format.raw/*797.76*/("""{"""),format.raw/*797.77*/("""
                var dataType = this.getItem(id).type;
                if (dataType === "collection")"""),format.raw/*799.47*/("""{"""),format.raw/*799.48*/("""
                    // webix.storage.local.put("state", $$("tree").getState());
                    // // document.getElementById('preview').src = "./collection/"+id;
                    // window.location.href = "/collection/"+id+"";
                """),format.raw/*803.17*/("""}"""),format.raw/*803.18*/("""else if(dataType === "dataset")"""),format.raw/*803.49*/("""{"""),format.raw/*803.50*/("""
                    // webix.storage.local.put("state", $$("tree").getState());
                    // // document.getElementById('preview').src = "./datasets/"+id;

                    // window.location.href = "/datasets/"+id+"";
                """),format.raw/*808.17*/("""}"""),format.raw/*808.18*/("""else if(dataType === "file")"""),format.raw/*808.46*/("""{"""),format.raw/*808.47*/("""
                    // webix.storage.local.put("state", $$("tree").getState());
                    // //window.location.href = "/files/"+id+"";
                    // document.getElementById('preview').src = "./files/"+id;
                    //webix.ajax("./files/"+id);
                """),format.raw/*813.17*/("""}"""),format.raw/*813.18*/("""else if(dataType === "space")"""),format.raw/*813.47*/("""{"""),format.raw/*813.48*/("""
                    // webix.storage.local.put("state", $$("tree").getState());
                    // // document.getElementById('preview').src = "./spaces/"+id;

                    // window.location.href = "/spaces/"+id+"";
                """),format.raw/*818.17*/("""}"""),format.raw/*818.18*/(""" else """),format.raw/*818.24*/("""{"""),format.raw/*818.25*/("""

                """),format.raw/*820.17*/("""}"""),format.raw/*820.18*/("""
            """),format.raw/*821.13*/("""}"""),format.raw/*821.14*/(""");


            $('#metadataModal').on('hidden.bs.modal', function () """),format.raw/*824.67*/("""{"""),format.raw/*824.68*/("""
                // window.location.search = 'ds';
            """),format.raw/*826.13*/("""}"""),format.raw/*826.14*/(""")

            // onBeforeDrag: check's if the source is a space, it cannot be dragged anywhere
            $$("tree").attachEvent("onBeforeDrag", function(context, ev) """),format.raw/*829.74*/("""{"""),format.raw/*829.75*/("""
                if (this.getItem(context.source[0]).type == "space") """),format.raw/*830.70*/("""{"""),format.raw/*830.71*/("""
                    return false;
                """),format.raw/*832.17*/("""}"""),format.raw/*832.18*/("""
                return true;
            """),format.raw/*834.13*/("""}"""),format.raw/*834.14*/(""");

            // onBeforeDrop: calls the helper methods to perform d n d successfully
            $$("tree").attachEvent("onBeforeDrop", function(context, ev) """),format.raw/*837.74*/("""{"""),format.raw/*837.75*/("""
                var startItem = this.getItem(context.start);
                var sourceItem = this.getItem(context.source);
                var desItem = this.getItem(context.target);

                console.log(startItem.type+", "+desItem.type);


                if(startItem.type == "collection") """),format.raw/*845.52*/("""{"""),format.raw/*845.53*/("""
                    console.log(startItem.$parent+", "+context.parent+", "+context.start);
                    if (startItem.$parent == 0)"""),format.raw/*847.48*/("""{"""),format.raw/*847.49*/("""
                        moveCollection('#', context.parent, context.start);
                        return true;
                    """),format.raw/*850.21*/("""}"""),format.raw/*850.22*/(""" else if(this.getItem(startItem.$parent).type == "space") """),format.raw/*850.80*/("""{"""),format.raw/*850.81*/("""
                        return false;
                    """),format.raw/*852.21*/("""}"""),format.raw/*852.22*/(""" else """),format.raw/*852.28*/("""{"""),format.raw/*852.29*/("""
                        moveCollection(startItem.$parent, context.parent, context.start);
                        return true;
                    """),format.raw/*855.21*/("""}"""),format.raw/*855.22*/("""

                """),format.raw/*857.17*/("""}"""),format.raw/*857.18*/(""" else if (startItem.type == "dataset")"""),format.raw/*857.56*/("""{"""),format.raw/*857.57*/("""
                    if (startItem.$parent == 0)"""),format.raw/*858.48*/("""{"""),format.raw/*858.49*/("""
                        moveDataset('#', context.start, context.parent);
                        return true;
                    """),format.raw/*861.21*/("""}"""),format.raw/*861.22*/(""" else if(this.getItem(startItem.$parent).type == "space") """),format.raw/*861.80*/("""{"""),format.raw/*861.81*/("""
                        return false;
                    """),format.raw/*863.21*/("""}"""),format.raw/*863.22*/(""" else"""),format.raw/*863.27*/("""{"""),format.raw/*863.28*/("""
                        moveDataset(startItem.$parent, context.start, context.parent);
                        return true;
                    """),format.raw/*866.21*/("""}"""),format.raw/*866.22*/("""
                """),format.raw/*867.17*/("""}"""),format.raw/*867.18*/(""" else if (startItem.type == "file" && desItem.type == "file") """),format.raw/*867.80*/("""{"""),format.raw/*867.81*/("""
                    for (var i = 0; i < context.source.length; i++) """),format.raw/*868.69*/("""{"""),format.raw/*868.70*/("""
                        moveFile(this.getItem(context.start).$parent, context.parent, context.source[i]);
                    """),format.raw/*870.21*/("""}"""),format.raw/*870.22*/("""
                    return true;
                """),format.raw/*872.17*/("""}"""),format.raw/*872.18*/("""
                return false;
            """),format.raw/*874.13*/("""}"""),format.raw/*874.14*/(""");

            // Helper methods to call API's for d n d
            function moveCollection(originalCollection, newCollection, childCollectionId)"""),format.raw/*877.90*/("""{"""),format.raw/*877.91*/("""
                $.ajax("""),format.raw/*878.24*/("""{"""),format.raw/*878.25*/("""
                    url: jsRoutes.api.Collections.moveChildCollection(childCollectionId).url,
                    type:"POST",
                    data: JSON.stringify("""),format.raw/*881.42*/("""{"""),format.raw/*881.43*/(""" originalCollection:originalCollection, newCollection:newCollection"""),format.raw/*881.110*/("""}"""),format.raw/*881.111*/("""),
                    dataType: "json",
                    beforeSend: function(xhr) """),format.raw/*883.47*/("""{"""),format.raw/*883.48*/("""
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Accept", "application/json");
                    """),format.raw/*886.21*/("""}"""),format.raw/*886.22*/(""",
                    success: function(data) """),format.raw/*887.45*/("""{"""),format.raw/*887.46*/("""}"""),format.raw/*887.47*/("""
                """),format.raw/*888.17*/("""}"""),format.raw/*888.18*/(""")
            """),format.raw/*889.13*/("""}"""),format.raw/*889.14*/("""

            function copyCollection(collectionId, spaceId) """),format.raw/*891.60*/("""{"""),format.raw/*891.61*/("""
                $.ajax("""),format.raw/*892.24*/("""{"""),format.raw/*892.25*/("""
                    url: jsRoutes.api.Collections.copyCollectionToSpace(collectionId, spaceId).url,
                    type: "POST"
                """),format.raw/*895.17*/("""}"""),format.raw/*895.18*/(""")
            """),format.raw/*896.13*/("""}"""),format.raw/*896.14*/("""

            function moveDataset(oldCollectionId, datasetId, newCollectionId)"""),format.raw/*898.78*/("""{"""),format.raw/*898.79*/("""
                $.ajax("""),format.raw/*899.24*/("""{"""),format.raw/*899.25*/("""
                    url: jsRoutes.api.Collections.moveDatasetToNewCollection(oldCollectionId,datasetId,newCollectionId).url,
                    type:"POST"
                """),format.raw/*902.17*/("""}"""),format.raw/*902.18*/(""")
            """),format.raw/*903.13*/("""}"""),format.raw/*903.14*/("""

            function moveFile(oldDatasetId, newDatasetId, fileId) """),format.raw/*905.67*/("""{"""),format.raw/*905.68*/("""
                $.ajax("""),format.raw/*906.24*/("""{"""),format.raw/*906.25*/("""
                    url: jsRoutes.api.Datasets.moveFileBetweenDatasets(oldDatasetId, newDatasetId, fileId).url,
                    type:"POST"
                """),format.raw/*909.17*/("""}"""),format.raw/*909.18*/(""")
            """),format.raw/*910.13*/("""}"""),format.raw/*910.14*/("""

            webix.attachEvent('unload', function()"""),format.raw/*912.51*/("""{"""),format.raw/*912.52*/("""
                var state = webix.storage.local.get("state");
                if (state) """),format.raw/*914.28*/("""{"""),format.raw/*914.29*/("""
                    $$("tree").setState(state);
                """),format.raw/*916.17*/("""}"""),format.raw/*916.18*/("""
            """),format.raw/*917.13*/("""}"""),format.raw/*917.14*/(""");

            $(document).ready(function() """),format.raw/*919.42*/("""{"""),format.raw/*919.43*/("""

                $(".nav-tabs > li.active").removeClass("active")
                var url = window.location.href;
                let params = new URLSearchParams(location.search.slice(1));

                if (url.indexOf("?") >= 0)"""),format.raw/*925.43*/("""{"""),format.raw/*925.44*/("""
                    for (let p of params) """),format.raw/*926.43*/("""{"""),format.raw/*926.44*/("""
                        if (p[0] === "ds")"""),format.raw/*927.43*/("""{"""),format.raw/*927.44*/("""
                            $("#tab-metadata1").addClass("active");
                            $("#tab-metadata").addClass("active");
                            console.log("MD");
                        """),format.raw/*931.25*/("""}"""),format.raw/*931.26*/("""else"""),format.raw/*931.30*/("""{"""),format.raw/*931.31*/("""
                            $("#tab-files1").addClass("active");
                            $("#tab-files").addClass("active");
                            console.log("NOT MD");

                        """),format.raw/*936.25*/("""}"""),format.raw/*936.26*/("""
                    """),format.raw/*937.21*/("""}"""),format.raw/*937.22*/("""
                """),format.raw/*938.17*/("""}"""),format.raw/*938.18*/("""else"""),format.raw/*938.22*/("""{"""),format.raw/*938.23*/("""
                    $("#tab-files1").addClass("active");
                    $("#tab-files").addClass("active");
                """),format.raw/*941.17*/("""}"""),format.raw/*941.18*/("""

            """),format.raw/*943.13*/("""}"""),format.raw/*943.14*/(""");

            $(function () """),format.raw/*945.27*/("""{"""),format.raw/*945.28*/("""
                $('[data-toggle="tooltip"]').tooltip();

                $('.collapse')
                        .on('shown.bs.collapse', function()"""),format.raw/*949.60*/("""{"""),format.raw/*949.61*/("""
                            $(this).parent().find(".panel-icon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
                        """),format.raw/*951.25*/("""}"""),format.raw/*951.26*/(""")
                        .on('hidden.bs.collapse', function()"""),format.raw/*952.61*/("""{"""),format.raw/*952.62*/("""
                            $(this).parent().find(".panel-icon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                        """),format.raw/*954.25*/("""}"""),format.raw/*954.26*/(""");

                $('.tree li').on('click', function (e) """),format.raw/*956.56*/("""{"""),format.raw/*956.57*/("""
                    //console.log("clicked");
                    var children = $(this).find('> ul > li > ul > li');
                    if (children.is(":visible")) """),format.raw/*959.50*/("""{"""),format.raw/*959.51*/("""
                        children.hide('fast');
                        $(this).find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                    """),format.raw/*962.21*/("""}"""),format.raw/*962.22*/(""" else """),format.raw/*962.28*/("""{"""),format.raw/*962.29*/("""
                        children.show('fast');
                        $(this).find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
                    """),format.raw/*965.21*/("""}"""),format.raw/*965.22*/("""
                    e.stopPropagation();
                """),format.raw/*967.17*/("""}"""),format.raw/*967.18*/(""");
            """),format.raw/*968.13*/("""}"""),format.raw/*968.14*/(""")

            $(document).on('click', '#editMetaData', function(e)"""),format.raw/*970.65*/("""{"""),format.raw/*970.66*/("""
                $(".modal-body").empty();
                get4ceedMetaData();
            """),format.raw/*973.13*/("""}"""),format.raw/*973.14*/(""");

            $(document).on('click', '#addMetaData', function(e)"""),format.raw/*975.64*/("""{"""),format.raw/*975.65*/("""
                var div = $("<div />");
                div.html(createDiv());
                $(".modal-body").append(div);
            """),format.raw/*979.13*/("""}"""),format.raw/*979.14*/(""");

            $('#metadataModal').on('hidden.bs.modal', function () """),format.raw/*981.67*/("""{"""),format.raw/*981.68*/("""
                // window.location.search = 'ds';
            """),format.raw/*983.13*/("""}"""),format.raw/*983.14*/(""")

            $(document).on('click', '#submitMetaData', function(e)"""),format.raw/*985.67*/("""{"""),format.raw/*985.68*/("""
                var url = window.location.pathname;
                var id = url.substring(url.lastIndexOf('/') + 1);

                checkIfDatasetAttachedTemplate();
            """),format.raw/*990.13*/("""}"""),format.raw/*990.14*/(""");

            //Should we show a template for this dataset?
            function get4ceedMetaData() """),format.raw/*993.41*/("""{"""),format.raw/*993.42*/("""
                var url = window.location.pathname;
                var id = url.substring(url.lastIndexOf('/') + 1);
                $.ajax("""),format.raw/*996.24*/("""{"""),format.raw/*996.25*/("""
                    url: jsRoutes.api.T2C2.getDatasetWithAttachedVocab(id).url,
                    type:"GET",
                    dataType: "json",
                    beforeSend: function(xhr)"""),format.raw/*1000.46*/("""{"""),format.raw/*1000.47*/("""
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Accept", "application/json");
                    """),format.raw/*1003.21*/("""}"""),format.raw/*1003.22*/(""",
                    success: function(data)"""),format.raw/*1004.44*/("""{"""),format.raw/*1004.45*/("""
                        if (data.template.id === "none")"""),format.raw/*1005.57*/("""{"""),format.raw/*1005.58*/("""
                            // console.log(data.template.id);
                        """),format.raw/*1007.25*/("""}"""),format.raw/*1007.26*/("""else"""),format.raw/*1007.30*/("""{"""),format.raw/*1007.31*/("""
                            createBoxesForPreviousDataset(data);
                        """),format.raw/*1009.25*/("""}"""),format.raw/*1009.26*/("""
                    """),format.raw/*1010.21*/("""}"""),format.raw/*1010.22*/("""
                """),format.raw/*1011.17*/("""}"""),format.raw/*1011.18*/(""")

            """),format.raw/*1013.13*/("""}"""),format.raw/*1013.14*/("""

            //Does this ds have a template?
            function checkIfDatasetAttachedTemplate() """),format.raw/*1016.55*/("""{"""),format.raw/*1016.56*/("""

                var url = window.location.pathname;
                // var id = url.substring(url.lastIndexOf('/') + 1);
                var id = $$("tree").getSelectedId();

                postTemplate(false, id);

                $.ajax("""),format.raw/*1024.24*/("""{"""),format.raw/*1024.25*/("""
                    url: jsRoutes.api.Datasets.hasAttachedVocabulary(id).url,
                    type:"GET",
                    dataType: "json",
                    beforeSend: function(xhr)"""),format.raw/*1028.46*/("""{"""),format.raw/*1028.47*/("""
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Accept", "application/json");
                    """),format.raw/*1031.21*/("""}"""),format.raw/*1031.22*/(""",
                    success: function(data)"""),format.raw/*1032.44*/("""{"""),format.raw/*1032.45*/("""
                        if (data.length > 0)"""),format.raw/*1033.45*/("""{"""),format.raw/*1033.46*/("""
                            edit4CeedMetaData(data[0].id);
                        """),format.raw/*1035.25*/("""}"""),format.raw/*1035.26*/("""else"""),format.raw/*1035.30*/("""{"""),format.raw/*1035.31*/("""
                            postTemplate(false, id);
                        """),format.raw/*1037.25*/("""}"""),format.raw/*1037.26*/("""
                    """),format.raw/*1038.21*/("""}"""),format.raw/*1038.22*/("""
                """),format.raw/*1039.17*/("""}"""),format.raw/*1039.18*/(""")

            """),format.raw/*1041.13*/("""}"""),format.raw/*1041.14*/("""

            function postTemplate(templateType, datasetID)"""),format.raw/*1043.59*/("""{"""),format.raw/*1043.60*/("""
                console.log("PT");
                var templateTerms = buildTemplate();
                var tagName = $('.tagName').val();//.toUpperCase();
                var shareTemplate = $('#checkShareTemplate').is(":checked");
                var templateName = $("#templateName").val();
                var templateType = "false"; //templateType.toString();
                $.ajax("""),format.raw/*1050.24*/("""{"""),format.raw/*1050.25*/("""
                    url: jsRoutes.api.Vocabularies.createVocabularyFromJson(shareTemplate).url,
                    type:"POST",
                    data: JSON.stringify("""),format.raw/*1053.42*/("""{"""),format.raw/*1053.43*/(""" name: templateName, terms: templateTerms, tags: tagName, master: templateType"""),format.raw/*1053.121*/("""}"""),format.raw/*1053.122*/("""),
                    beforeSend: function(xhr)"""),format.raw/*1054.46*/("""{"""),format.raw/*1054.47*/("""
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Accept", "application/json");
                    """),format.raw/*1057.21*/("""}"""),format.raw/*1057.22*/(""",
                    success: function(data)"""),format.raw/*1058.44*/("""{"""),format.raw/*1058.45*/("""
                        addTemplateToDataset(datasetID, data.id);
                    """),format.raw/*1060.21*/("""}"""),format.raw/*1060.22*/(""",
                    error: function(xhr, status, error) """),format.raw/*1061.57*/("""{"""),format.raw/*1061.58*/("""
                        swal("""),format.raw/*1062.30*/("""{"""),format.raw/*1062.31*/("""
                            title: "Error",
                            text: "There was a problem creating this template",
                            type: "error",
                            timer: 1500,
                            showConfirmButton: false
                        """),format.raw/*1068.25*/("""}"""),format.raw/*1068.26*/(""");
                    """),format.raw/*1069.21*/("""}"""),format.raw/*1069.22*/("""
                """),format.raw/*1070.17*/("""}"""),format.raw/*1070.18*/(""")
            """),format.raw/*1071.13*/("""}"""),format.raw/*1071.14*/("""

            function buildTemplate() """),format.raw/*1073.38*/("""{"""),format.raw/*1073.39*/("""
                var metaDataKeys = $.map($(' .metaDataKey'), function (el) """),format.raw/*1074.76*/("""{"""),format.raw/*1074.77*/(""" return el.value; """),format.raw/*1074.95*/("""}"""),format.raw/*1074.96*/(""");
                var metaDataVals = $.map($(' .metaDataVal'), function (el) """),format.raw/*1075.76*/("""{"""),format.raw/*1075.77*/("""return el.value;"""),format.raw/*1075.93*/("""}"""),format.raw/*1075.94*/(""");
                var metaDataUnits = $.map($(' .metaDataUnit'), function (el) """),format.raw/*1076.78*/("""{"""),format.raw/*1076.79*/("""return el.value;"""),format.raw/*1076.95*/("""}"""),format.raw/*1076.96*/(""");
                var metaDataTypes = $.map($(' .metaDataType'), function (el) """),format.raw/*1077.78*/("""{"""),format.raw/*1077.79*/("""return el.value;"""),format.raw/*1077.95*/("""}"""),format.raw/*1077.96*/(""");
                var requireField = $.map($(' .requireField'), function (el) """),format.raw/*1078.77*/("""{"""),format.raw/*1078.78*/("""return el.value;"""),format.raw/*1078.94*/("""}"""),format.raw/*1078.95*/(""");

                var arr = [];

                $.each(metaDataKeys, function (idx, keyName) """),format.raw/*1082.62*/("""{"""),format.raw/*1082.63*/("""
                    if (keyName != '')"""),format.raw/*1083.39*/("""{"""),format.raw/*1083.40*/("""
                        var objCombined = """),format.raw/*1084.43*/("""{"""),format.raw/*1084.44*/("""}"""),format.raw/*1084.45*/(""";
                        objCombined['key'] = keyName;
                        objCombined['units'] = metaDataUnits[idx];;
                        objCombined['data_type'] = metaDataTypes[idx];;
                        objCombined['default_value'] = metaDataVals[idx];
                        objCombined['required'] = requireField[idx];

                        arr.push(objCombined);
                    """),format.raw/*1092.21*/("""}"""),format.raw/*1092.22*/("""
                """),format.raw/*1093.17*/("""}"""),format.raw/*1093.18*/(""");
                return(arr);

            """),format.raw/*1096.13*/("""}"""),format.raw/*1096.14*/("""

            function addTemplateToDataset(datasetid, templateID)"""),format.raw/*1098.65*/("""{"""),format.raw/*1098.66*/("""
                var url = "/t2c2/templates/" + templateID + "/attachToDataset/" + datasetid + "";
                $.ajax("""),format.raw/*1100.24*/("""{"""),format.raw/*1100.25*/("""
                    url: url,
                    // url: jsRoutes.T2C2.attachVocabToDataset(templateID,datasetid).url,
                    type:"PUT",
                    beforeSend: function(xhr)"""),format.raw/*1104.46*/("""{"""),format.raw/*1104.47*/("""
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Accept", "application/json");
                    """),format.raw/*1107.21*/("""}"""),format.raw/*1107.22*/(""",
                    data: JSON.stringify("""),format.raw/*1108.42*/("""{"""),format.raw/*1108.43*/(""" template_id: templateID, dataset_id: datasetid"""),format.raw/*1108.90*/("""}"""),format.raw/*1108.91*/("""),
                    success: function(data)"""),format.raw/*1109.44*/("""{"""),format.raw/*1109.45*/("""
                        //console.log("attaching");
                    """),format.raw/*1111.21*/("""}"""),format.raw/*1111.22*/(""",
                    error: function(xhr, status, error) """),format.raw/*1112.57*/("""{"""),format.raw/*1112.58*/("""
                    """),format.raw/*1113.21*/("""}"""),format.raw/*1113.22*/("""

                """),format.raw/*1115.17*/("""}"""),format.raw/*1115.18*/(""")

            """),format.raw/*1117.13*/("""}"""),format.raw/*1117.14*/("""

            function edit4CeedMetaData(templateId) """),format.raw/*1119.52*/("""{"""),format.raw/*1119.53*/("""
                var url = window.location.pathname;
                var id = url.substring(url.lastIndexOf('/') + 1);

                var templateTerms = buildTemplate();
                var tagName = ""; //$('.metaDataTags').val().toUpperCase();
                var datasetName = ""; //$(".metaDataName").val();
                var datasetDescription = ""; //$(".metaDataDesc").val();
                var templateId = templateId;
                $.ajax("""),format.raw/*1128.24*/("""{"""),format.raw/*1128.25*/("""
                    url: jsRoutes.api.Vocabularies.editVocabulary(templateId).url,
                    type:"PUT",
                    dataType: "json",
                    data: JSON.stringify("""),format.raw/*1132.42*/("""{"""),format.raw/*1132.43*/("""name: datasetName, description: datasetDescription, tags: tagName, isPublic: false, terms: templateTerms, attached_dataset: templateId"""),format.raw/*1132.177*/("""}"""),format.raw/*1132.178*/("""),
                    beforeSend: function(xhr)"""),format.raw/*1133.46*/("""{"""),format.raw/*1133.47*/("""
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Accept", "application/json");
                    """),format.raw/*1136.21*/("""}"""),format.raw/*1136.22*/(""",
                    success: function(data)"""),format.raw/*1137.44*/("""{"""),format.raw/*1137.45*/("""
                    """),format.raw/*1138.21*/("""}"""),format.raw/*1138.22*/(""",
                    error: function(xhr, status, error) """),format.raw/*1139.57*/("""{"""),format.raw/*1139.58*/("""
                    """),format.raw/*1140.21*/("""}"""),format.raw/*1140.22*/("""

                """),format.raw/*1142.17*/("""}"""),format.raw/*1142.18*/(""")
            """),format.raw/*1143.13*/("""}"""),format.raw/*1143.14*/("""

            function buildTemplate() """),format.raw/*1145.38*/("""{"""),format.raw/*1145.39*/("""
                var metaDataKeys = $.map($('.metaDataKey'), function (el) """),format.raw/*1146.75*/("""{"""),format.raw/*1146.76*/(""" return el.value; """),format.raw/*1146.94*/("""}"""),format.raw/*1146.95*/(""");
                var metaDataVals = $.map($('.metaDataVal'), function (el) """),format.raw/*1147.75*/("""{"""),format.raw/*1147.76*/("""return el.value;"""),format.raw/*1147.92*/("""}"""),format.raw/*1147.93*/(""");
                var metaDataUnits = $.map($('.metaDataUnit'), function (el) """),format.raw/*1148.77*/("""{"""),format.raw/*1148.78*/("""return el.value;"""),format.raw/*1148.94*/("""}"""),format.raw/*1148.95*/(""");

                var arr = [];

                $.each(metaDataKeys, function (idx, keyName) """),format.raw/*1152.62*/("""{"""),format.raw/*1152.63*/("""
                    if (keyName != '')"""),format.raw/*1153.39*/("""{"""),format.raw/*1153.40*/("""
                        var objCombined = """),format.raw/*1154.43*/("""{"""),format.raw/*1154.44*/("""}"""),format.raw/*1154.45*/(""";
                        objCombined['key'] = keyName;
                        objCombined['units'] = metaDataUnits[idx];;
                        objCombined['default_value'] = metaDataVals[idx];
                        arr.push(objCombined);
                    """),format.raw/*1159.21*/("""}"""),format.raw/*1159.22*/("""
                """),format.raw/*1160.17*/("""}"""),format.raw/*1160.18*/(""");
                return(arr);

            """),format.raw/*1163.13*/("""}"""),format.raw/*1163.14*/("""

            $(function () """),format.raw/*1165.27*/("""{"""),format.raw/*1165.28*/("""
                $('.delete-icon').unbind().on('click', function()"""),format.raw/*1166.66*/("""{"""),format.raw/*1166.67*/("""
                    var delete_icon = $(this);

                    var request = jsRoutes.api.Metadata.removeMetadata(this.id).ajax("""),format.raw/*1169.86*/("""{"""),format.raw/*1169.87*/("""
                        type: 'DELETE'
                    """),format.raw/*1171.21*/("""}"""),format.raw/*1171.22*/(""");

                    request.done(function (response, textStatus, jqXHR) """),format.raw/*1173.73*/("""{"""),format.raw/*1173.74*/("""
                        //console.log("success");
                        delete_icon.closest(".panel").remove();
                    """),format.raw/*1176.21*/("""}"""),format.raw/*1176.22*/(""");

                    request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*1178.75*/("""{"""),format.raw/*1178.76*/("""
                        console.error("The following error occured: " + textStatus, errorThrown);
                        var errMsg = "You must be logged in to add metadata";
                        if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*1181.68*/("""{"""),format.raw/*1181.69*/("""
                            notify("Metadata was not removed due to : " + errorThrown, "error");
                        """),format.raw/*1183.25*/("""}"""),format.raw/*1183.26*/("""
                    """),format.raw/*1184.21*/("""}"""),format.raw/*1184.22*/(""");
                """),format.raw/*1185.17*/("""}"""),format.raw/*1185.18*/(""");
            """),format.raw/*1186.13*/("""}"""),format.raw/*1186.14*/(""")

            // get metadata definitions
            function getMetadataContext(mid, uuid, key) """),format.raw/*1189.57*/("""{"""),format.raw/*1189.58*/("""
                var request = jsRoutes.api.ContextLD.getContextById(uuid).ajax("""),format.raw/*1190.80*/("""{"""),format.raw/*1190.81*/("""
                    type: 'GET',
                    contentType: "application/json"
                """),format.raw/*1193.17*/("""}"""),format.raw/*1193.18*/(""");

                request.done(function (response, textStatus, jqXHR) """),format.raw/*1195.69*/("""{"""),format.raw/*1195.70*/("""
                    var fields = response;
                    var context = "Context is not defined.";
                    if (fields['@context']) """),format.raw/*1198.46*/("""{"""),format.raw/*1198.47*/("""
                        context = JSON.stringify(fields['@context'][key]);
                    """),format.raw/*1200.21*/("""}"""),format.raw/*1200.22*/("""
                    $("#"+mid).popover("""),format.raw/*1201.40*/("""{"""),format.raw/*1201.41*/("""
                        content:context,
                        trigger:'hover',
                        placement:'top',
                        template: '<div class="popover" role="tooltip" style="max-width:600px;word-break:break-all"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
                    """),format.raw/*1206.21*/("""}"""),format.raw/*1206.22*/(""");
                    $("#"+mid).popover('show');
                """),format.raw/*1208.17*/("""}"""),format.raw/*1208.18*/(""");

                request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*1210.71*/("""{"""),format.raw/*1210.72*/("""
                    console.error("The following error occured: " + textStatus, errorThrown);
                    var errMsg = "You must be logged in to retrieve metadata definitions";
                    if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*1213.64*/("""{"""),format.raw/*1213.65*/("""
                        notify("Metadata context was not shown due to : " + errorThrown, "error");
                    """),format.raw/*1215.21*/("""}"""),format.raw/*1215.22*/("""
                """),format.raw/*1216.17*/("""}"""),format.raw/*1216.18*/(""");
            """),format.raw/*1217.13*/("""}"""),format.raw/*1217.14*/("""
            function leaveMetadataContext(mid) """),format.raw/*1218.48*/("""{"""),format.raw/*1218.49*/("""
                $("#"+mid).popover('hide');
            """),format.raw/*1220.13*/("""}"""),format.raw/*1220.14*/("""

            // get agent author definitions
            function getAgentContext(aid, uuid) """),format.raw/*1223.49*/("""{"""),format.raw/*1223.50*/("""
                var request = jsRoutes.api.Users.findById(uuid).ajax("""),format.raw/*1224.70*/("""{"""),format.raw/*1224.71*/("""
                    type: 'GET',
                    contentType: "application/json"
                """),format.raw/*1227.17*/("""}"""),format.raw/*1227.18*/(""");

                request.done(function (response, textStatus, jqXHR) """),format.raw/*1229.69*/("""{"""),format.raw/*1229.70*/("""
                    var fields = response;
                    //console.log(fields['fullName']);
                    $("#"+aid).popover("""),format.raw/*1232.40*/("""{"""),format.raw/*1232.41*/("""
                        content:fields['email'],
                        trigger:'hover',
                        placement:'top',
                        template: '<div class="popover" role="tooltip" style="max-width:600px;word-break:break-all"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
                    """),format.raw/*1237.21*/("""}"""),format.raw/*1237.22*/(""");
                    $("#"+aid).popover('show');
                """),format.raw/*1239.17*/("""}"""),format.raw/*1239.18*/(""");

                request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*1241.71*/("""{"""),format.raw/*1241.72*/("""
                    console.error("The following error occured: " + textStatus, errorThrown);
                    var errMsg = "You must be logged in to retrieve metadata definitions";
                    if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*1244.64*/("""{"""),format.raw/*1244.65*/("""
                        notify("Agent metadata context was not shown due to : " + errorThrown, "error");
                    """),format.raw/*1246.21*/("""}"""),format.raw/*1246.22*/("""
                """),format.raw/*1247.17*/("""}"""),format.raw/*1247.18*/(""");
            """),format.raw/*1248.13*/("""}"""),format.raw/*1248.14*/("""

            function leaveAgentContext(aid) """),format.raw/*1250.45*/("""{"""),format.raw/*1250.46*/("""
                $("#"+aid).popover('hide');
            """),format.raw/*1252.13*/("""}"""),format.raw/*1252.14*/("""

            function counter() """),format.raw/*1254.32*/("""{"""),format.raw/*1254.33*/("""
                var count = 0;

                this.reset = function() """),format.raw/*1257.41*/("""{"""),format.raw/*1257.42*/("""
                    count = 0;
                    return count;
                """),format.raw/*1260.17*/("""}"""),format.raw/*1260.18*/(""";

                this.add = function() """),format.raw/*1262.39*/("""{"""),format.raw/*1262.40*/("""
                    return ++count;
                """),format.raw/*1264.17*/("""}"""),format.raw/*1264.18*/(""";
            """),format.raw/*1265.13*/("""}"""),format.raw/*1265.14*/("""

            $("body").on("click", ".remove", function () """),format.raw/*1267.58*/("""{"""),format.raw/*1267.59*/("""
                $(this).closest(".top-buffer").remove();
            """),format.raw/*1269.13*/("""}"""),format.raw/*1269.14*/(""");

            var counter1 = new counter();

            //Create dynamic textbox
            function createOtherDiv(name, desc, tags) """),format.raw/*1274.55*/("""{"""),format.raw/*1274.56*/("""
                var valKeyName = $.trim(name);
                var valStr = $.trim(desc);
                var valUnits = $.trim(tags);

                return '<div class="row top-buffer"><div class="col-xs-4"><b>' + "<label for='metaDataName'>Name: " + '</b></label>' +
                        '<input class="metaDataName form-control" name="metaDataName" id="metaDataName" type="text" value=' + valKeyName.replace(/ /g,"&nbsp;") +'></div>' +

                        '<div class="col-xs-4"><b>' + "<label for='metaDataDesc'>Description:</b></label>" +
                        '<input class="metaDataDesc form-control" name="metaDataDesc" id="metaDataDesc" type="text" value=' + valStr.replace(/ /g,"&nbsp;") +'></div>' +

                        '<div class="col-xs-4"><b>' + "<label for='metaDataTags'>Tags: " + '</label></b>' +
                        '<input class="metaDataTags form-control" name="metaDataTags" id="metaDataTags" type="text" value=' + valUnits.replace(/ /g,"&nbsp;") +'></div></div><br />'

            """),format.raw/*1288.13*/("""}"""),format.raw/*1288.14*/("""

            //Create dynamic textbox
            function createDiv(keyName, val, units) """),format.raw/*1291.53*/("""{"""),format.raw/*1291.54*/("""

                var valKeyName = $.trim(keyName);
                var valStr = $.trim(val);
                var valUnits = $.trim(units);

                //format text
                var txtToWrite = "Value";

                var i = counter1.add();

                return '<div class="row top-buffer"><div class="col-xs-3"><b>' + "<label for='name'>Key: " + '</b></label>' +
                        '<input class="metaDataKey form-control" name="metaDataKey' + i + '" id="metaDataKey' + i + '" type="text" value=' + valKeyName.replace(/ /g,"&nbsp;") +'></div>' +

                        '<div class="col-xs-2" style="margin-left:-15px;"><b>' + "<label for='val'>"+ txtToWrite + '</b></label>' +
                        '<input class="metaDataVal form-control" name="metaDataVal' + i + '" id="metaDataVal' + i + '" type="text" value=' + valStr.replace(/ /g,"&nbsp;") +'></div>' +


                        '<div class="col-xs-2"><b>' + "<label for='name'>Units: " + '</label></b>' +
                        '<input class="metaDataUnit form-control" name="metaDataUnit' + i + '" id="metaDataUnit' + i + '" type="text" value=' + valUnits.replace(/ /g,"&nbsp;") +'></div>' +

                        '<div class="col-xs-1" style="margin-left:-15px;"><b>' + "<label for='val'>&nbsp;" + '</label></b>' +
                        '<input type="button" style="background-color:#d9534f;" value="Remove" class="remove btn btn-danger btnRemove" name="btnRemove' + i + '" id="btnRemove' + i + '"></div>' +
                        '<span class="up"></span><span class="down"></span></div>'


            """),format.raw/*1317.13*/("""}"""),format.raw/*1317.14*/("""

            function moveUp(element) """),format.raw/*1319.38*/("""{"""),format.raw/*1319.39*/("""
                if(element.previousElementSibling)
                    element.parentNode.insertBefore(element, element.previousElementSibling);
            """),format.raw/*1322.13*/("""}"""),format.raw/*1322.14*/("""
            function moveDown(element) """),format.raw/*1323.40*/("""{"""),format.raw/*1323.41*/("""
                if(element.nextElementSibling)
                    element.parentNode.insertBefore(element.nextElementSibling, element);
            """),format.raw/*1326.13*/("""}"""),format.raw/*1326.14*/("""
            document.querySelector('ul').addEventListener('click', function(e) """),format.raw/*1327.80*/("""{"""),format.raw/*1327.81*/("""
                if(e.target.className === 'down') moveDown(e.target.parentNode);
                else if(e.target.className === 'up') moveUp(e.target.parentNode);
            """),format.raw/*1330.13*/("""}"""),format.raw/*1330.14*/(""");

            //TEMPLATES
            function getTemplates() """),format.raw/*1333.37*/("""{"""),format.raw/*1333.38*/("""
                $.ajax("""),format.raw/*1334.24*/("""{"""),format.raw/*1334.25*/("""
                    url: jsRoutes.api.Vocabularies.list().url,
                    type:"GET",
                    dataType: "json",
                    beforeSend: function(xhr)"""),format.raw/*1338.46*/("""{"""),format.raw/*1338.47*/("""
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Accept", "application/json");
                    """),format.raw/*1341.21*/("""}"""),format.raw/*1341.22*/(""",
                    success: function(data)"""),format.raw/*1342.44*/("""{"""),format.raw/*1342.45*/("""
                        // console.log("yes");
                        $("#treeTemplates").empty();

                        $('<option>').val('').text('--Select One--').appendTo('#treeTemplates');

                        showTemplates(data);
                    """),format.raw/*1349.21*/("""}"""),format.raw/*1349.22*/(""",
                    error: function(xhr, status, error) """),format.raw/*1350.57*/("""{"""),format.raw/*1350.58*/("""
                        swal("""),format.raw/*1351.30*/("""{"""),format.raw/*1351.31*/("""
                            title: "Error",
                            text: "There was a problem returning custom templates",
                            type: "error",
                            timer: 1500,
                            showConfirmButton: false
                        """),format.raw/*1357.25*/("""}"""),format.raw/*1357.26*/(""");
                    """),format.raw/*1358.21*/("""}"""),format.raw/*1358.22*/("""
                """),format.raw/*1359.17*/("""}"""),format.raw/*1359.18*/(""")

            """),format.raw/*1361.13*/("""}"""),format.raw/*1361.14*/("""

            //Load user templates
            function showTemplates(data) """),format.raw/*1364.42*/("""{"""),format.raw/*1364.43*/("""
                $.each(data, function(key, val) """),format.raw/*1365.49*/("""{"""),format.raw/*1365.50*/("""
                    $("#treeTemplates").append($("<option class='placeholder'></option>").val(val.id).html(val.name));
                """),format.raw/*1367.17*/("""}"""),format.raw/*1367.18*/(""");

                $("#treeTemplates").focus();
            """),format.raw/*1370.13*/("""}"""),format.raw/*1370.14*/("""

            function getTemplate(id)"""),format.raw/*1372.37*/("""{"""),format.raw/*1372.38*/("""
                $.ajax("""),format.raw/*1373.24*/("""{"""),format.raw/*1373.25*/("""
                    url: jsRoutes.api.T2C2.getVocabulary(id).url,
                    type:"GET",
                    dataType: "json",
                    beforeSend: function(xhr)"""),format.raw/*1377.46*/("""{"""),format.raw/*1377.47*/("""
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Accept", "application/json");
                    """),format.raw/*1380.21*/("""}"""),format.raw/*1380.22*/(""",
                    success: function(data)"""),format.raw/*1381.44*/("""{"""),format.raw/*1381.45*/("""
                        createBoxes(data);
                    """),format.raw/*1383.21*/("""}"""),format.raw/*1383.22*/(""",
                    error: function(xhr, status, error) """),format.raw/*1384.57*/("""{"""),format.raw/*1384.58*/("""
                        swal("""),format.raw/*1385.30*/("""{"""),format.raw/*1385.31*/("""
                            title: "Error",
                            text: "There was a problem returning global templates",
                            type: "error",
                            timer: 1500,
                            showConfirmButton: false
                        """),format.raw/*1391.25*/("""}"""),format.raw/*1391.26*/(""");
                    """),format.raw/*1392.21*/("""}"""),format.raw/*1392.22*/("""
                """),format.raw/*1393.17*/("""}"""),format.raw/*1393.18*/(""")
            """),format.raw/*1394.13*/("""}"""),format.raw/*1394.14*/("""

            function createBoxes(data)"""),format.raw/*1396.39*/("""{"""),format.raw/*1396.40*/("""
                $(".templateData").empty();
                $(".btnDataset").show();
                $(".btnAdd").show();
                $("#btnTemplate").show();
                $(".otherOptions").show();
                // var menuName = $('.nav-tabs .active > a').attr("href");
                // var menuName = $("#treeTemplates"); 

                // console.log(menuName);

                //Get current tab and use name to determine what the label will say based on it's tab
                $.each(data.terms, function(key, val) """),format.raw/*1408.55*/("""{"""),format.raw/*1408.56*/("""
                    var div = $("<div />");
                    div.html(createDiv(val.key, val.default_value, val.units, val.data_type, val.required));
                    $(".templateData").append(div);
                """),format.raw/*1412.17*/("""}"""),format.raw/*1412.18*/(""");

            """),format.raw/*1414.13*/("""}"""),format.raw/*1414.14*/("""

            //Handle template load when new menu item is selected
            $(document).on('change', '#treeTemplates', function()"""),format.raw/*1417.66*/("""{"""),format.raw/*1417.67*/("""
                var id = $(this).val();
                if (id != '')"""),format.raw/*1419.30*/("""{"""),format.raw/*1419.31*/("""
                    getTemplate(id);
                """),format.raw/*1421.17*/("""}"""),format.raw/*1421.18*/("""
            """),format.raw/*1422.13*/("""}"""),format.raw/*1422.14*/(""");


            (function( $ )"""),format.raw/*1425.27*/("""{"""),format.raw/*1425.28*/("""
              $( function()"""),format.raw/*1426.28*/("""{"""),format.raw/*1426.29*/("""
                $(".enlarge.inline-demo").data("options", """),format.raw/*1427.59*/("""{"""),format.raw/*1427.60*/("""
                  // options here
                """),format.raw/*1429.17*/("""}"""),format.raw/*1429.18*/(""");


                $( document ).bind( "enhance", function()"""),format.raw/*1432.58*/("""{"""),format.raw/*1432.59*/("""
                  $( "body" ).addClass( "enhanced" );
                """),format.raw/*1434.17*/("""}"""),format.raw/*1434.18*/(""");

                $( document ).trigger( "enhance" );
              """),format.raw/*1437.15*/("""}"""),format.raw/*1437.16*/(""");
            """),format.raw/*1438.13*/("""}"""),format.raw/*1438.14*/("""( jQuery ));

            $(".enlarge.inline-demo").data("options", """),format.raw/*1440.55*/("""{"""),format.raw/*1440.56*/("""
              button: true,
              hoverZoomWithoutClick: true,
              delay: 300,
              flyout: """),format.raw/*1444.23*/("""{"""),format.raw/*1444.24*/("""
                width: 300,
                height: 300
              """),format.raw/*1447.15*/("""}"""),format.raw/*1447.16*/(""",
              placement: "flyoutloupe", // or inline
              magnification: 3
            """),format.raw/*1450.13*/("""}"""),format.raw/*1450.14*/(""");


            // function getTemplate(id)"""),format.raw/*1453.40*/("""{"""),format.raw/*1453.41*/("""
            //     $.ajax("""),format.raw/*1454.27*/("""{"""),format.raw/*1454.28*/("""

            //         url: "getExperimentTemplateById/"+id+"",
            //         type:"GET",
            //         dataType: "json",
            //         beforeSend: function(xhr)"""),format.raw/*1459.49*/("""{"""),format.raw/*1459.50*/("""
            //             xhr.setRequestHeader("Content-Type", "application/json");
            //             xhr.setRequestHeader("Accept", "application/json");
            //         """),format.raw/*1462.24*/("""}"""),format.raw/*1462.25*/(""",
            //         success: function(data)"""),format.raw/*1463.47*/("""{"""),format.raw/*1463.48*/("""
            //             createBoxes(data);
            //         """),format.raw/*1465.24*/("""}"""),format.raw/*1465.25*/(""",
            //         error: function(xhr, status, error) """),format.raw/*1466.60*/("""{"""),format.raw/*1466.61*/("""
            //             swal("""),format.raw/*1467.33*/("""{"""),format.raw/*1467.34*/("""
            //                 title: "Error",
            //                 text: "There was a problem returning global templates",
            //                 type: "error",
            //                 timer: 1500,
            //                 showConfirmButton: false
            //             """),format.raw/*1473.28*/("""}"""),format.raw/*1473.29*/(""");
            //         """),format.raw/*1474.24*/("""}"""),format.raw/*1474.25*/("""
            //     """),format.raw/*1475.20*/("""}"""),format.raw/*1475.21*/(""")
            // """),format.raw/*1476.16*/("""}"""),format.raw/*1476.17*/("""
            
        </script>
""")))})))}
    }
    
    def render(displayedName:String,newsfeed:List[models.Event],profile:User,datasetsList:List[Dataset],dscomments:Map[UUID, Int],collectionsList:List[Collection],spacesList:List[ProjectSpace],deletePermission:Boolean,followers:List[scala.Tuple4[UUID, String, String, String]],followedUsers:List[scala.Tuple4[UUID, String, String, String]],followedFiles:List[scala.Tuple3[UUID, String, String]],followedDatasets:List[scala.Tuple3[UUID, String, String]],followedCollections:List[scala.Tuple3[UUID, String, String]],followedSpaces:List[scala.Tuple3[UUID, String, String]],ownProfile:Option[Boolean],userSelections:List[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(displayedName,newsfeed,profile,datasetsList,dscomments,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections)(user)
    
    def f:((String,List[models.Event],User,List[Dataset],Map[UUID, Int],List[Collection],List[ProjectSpace],Boolean,List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],Option[Boolean],List[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (displayedName,newsfeed,profile,datasetsList,dscomments,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections) => (user) => apply(displayedName,newsfeed,profile,datasetsList,dscomments,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:46:54 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/home.scala.html
                    HASH: 685a892f71cd230cd3e5a44f13246006f2e4cb6a
                    MATRIX: 995->2|1834->633|1863->749|1901->752|1929->771|1969->773|2024->792|2039->798|2111->848|2200->901|2215->907|2284->954|2373->1007|2388->1013|2455->1058|2544->1111|2559->1117|2621->1157|2710->1210|2725->1216|2804->1273|2893->1326|2908->1332|2979->1381|3068->1434|3083->1440|3159->1494|3264->1563|3279->1569|3385->1652|4039->2270|4054->2276|4106->2305|4232->2395|4247->2401|4305->2436|4437->2532|4452->2538|4506->2569|4634->2661|4649->2667|4700->2696|10519->8486|10549->8487|10619->8529|10649->8530|10699->8551|10729->8552|10797->8592|10827->8593|10871->8608|10901->8609|10969->8649|10999->8650|11045->8667|11075->8668|11182->8747|11212->8748|11266->8773|11296->8774|11358->8808|11388->8809|11498->8890|11528->8891|11618->8953|11648->8954|11725->9002|11755->9003|11817->9037|11847->9038|11895->9057|11925->9058|12076->9181|12106->9182|12167->9214|12197->9215|12263->9253|12293->9254|12353->9285|12383->9286|12483->9358|12513->9359|12558->9375|12588->9376|12679->9439|12709->9440|12767->9469|12797->9470|12864->9509|12894->9510|12940->9527|12970->9528|13124->9654|13154->9655|13199->9671|13229->9672|13298->9713|13328->9714|13380->9737|13410->9738|13513->9813|13543->9814|13651->9894|13681->9895|13730->9915|13760->9916|13836->9963|13866->9964|13919->9988|13949->9989|14036->10047|14066->10048|14118->10071|14148->10072|14230->10125|14260->10126|14312->10149|14342->10150|14416->10195|14446->10196|14515->10236|14545->10237|14627->10290|14657->10291|14706->10311|14736->10312|14866->10413|14896->10414|14954->10443|14984->10444|15163->10594|15193->10595|15253->10626|15283->10627|15371->10686|15401->10687|15453->10710|15483->10711|15601->10800|15631->10801|15683->10824|15713->10825|15858->10941|15888->10942|15952->10977|15982->10978|16066->11033|16096->11034|16154->11063|16184->11064|16386->11237|16416->11238|16486->11279|16516->11280|16584->11319|16614->11320|16703->11380|16733->11381|16887->11506|16917->11507|16954->11516|16984->11517|17091->11596|17121->11597|17178->11625|17208->11626|17407->11796|17437->11797|17491->11822|17521->11823|17576->11849|17606->11850|17679->11894|17709->11895|17778->11935|17808->11936|17860->11959|17890->11960|17966->12007|17996->12008|18085->12068|18115->12069|18340->12265|18370->12266|18419->12286|18449->12287|18516->12325|18546->12326|18583->12335|18613->12336|18692->12386|18722->12387|18868->12505|18898->12506|18953->12532|18983->12533|19089->12611|19119->12612|19176->12640|19206->12641|19318->12725|19348->12726|19402->12751|19432->12752|19542->12834|19572->12835|19629->12863|19659->12864|19767->12944|19797->12945|19843->12962|19873->12963|20028->13090|20058->13091|20106->13110|20136->13111|20264->13211|20294->13212|20412->13301|20442->13302|20726->13557|20756->13558|20828->13593|20844->13599|20909->13641|21067->13770|21097->13771|21617->14262|21647->14263|21823->14410|21853->14411|21927->14456|21957->14457|22192->14663|22222->14664|22439->14852|22469->14853|22553->14908|22583->14909|22644->14941|22674->14942|22739->14978|22769->14979|22824->15005|22854->15006|22975->15098|23005->15099|23072->15137|23102->15138|23535->15542|23565->15543|23736->15685|23766->15686|23834->15725|23864->15726|24108->15941|24138->15942|24196->15971|24226->15972|24289->16006|24319->16007|24349->16008|24421->16051|24451->16052|24502->16074|24532->16075|24588->16102|24618->16103|24918->16374|24948->16375|25071->16469|25101->16470|25151->16491|25181->16492|25232->16514|25262->16515|25411->16635|25441->16636|25588->16754|25618->16755|25854->16962|25884->16963|26209->17259|26239->17260|26299->17291|26329->17292|26637->17571|26667->17572|26723->17599|26753->17600|27055->17873|27085->17874|27120->17880|27150->17881|27304->18006|27334->18007|27450->18094|27480->18095|27553->18139|27583->18140|27756->18284|27786->18285|27849->18319|27879->18320|27936->18348|27966->18349|28039->18393|28069->18394|28240->18536|28270->18537|28333->18571|28363->18572|28422->18602|28452->18603|28595->18717|28625->18718|28687->18751|28717->18752|28775->18781|28805->18782|28855->18803|28885->18804|28937->18827|28967->18828|29014->18846|29044->18847|29129->18903|29159->18904|29781->19497|29811->19498|29903->19561|29933->19562|30080->19680|30110->19681|30214->19756|30244->19757|30324->19808|30354->19809|30428->19854|30458->19855|30522->19890|30552->19891|30606->19916|30636->19917|30701->19953|30731->19954|30786->19980|30816->19981|30888->20024|30918->20025|31061->20139|31091->20140|31203->20223|31233->20224|31332->20294|31362->20295|31422->20326|31452->20327|31548->20394|31578->20395|31964->20752|31994->20753|32048->20778|32078->20779|32141->20813|32171->20814|32250->20864|32280->20865|32486->21042|32516->21043|32724->21222|32754->21223|32789->21229|32819->21230|32997->21379|33027->21380|33085->21409|33115->21410|33169->21435|33199->21436|33333->21541|33363->21542|33405->21555|33435->21556|33465->21557|33519->21582|33549->21583|33579->21584|33623->21599|33653->21600|33728->21646|33758->21647|33818->21678|33848->21679|33935->21737|33965->21738|34042->21786|34072->21787|34159->21845|34189->21846|34266->21894|34296->21895|34383->21953|34413->21954|34490->22002|34520->22003|34631->22085|34661->22086|34803->22199|34833->22200|34919->22257|34949->22258|35007->22287|35037->22288|35088->22310|35118->22311|35173->22337|35203->22338|35250->22356|35280->22357|35327->22375|35357->22376|35423->22413|35453->22414|35500->22432|35530->22433|35587->22461|35617->22462|35664->22480|35694->22481|35747->22505|35777->22506|35824->22524|35854->22525|35905->22547|35935->22548|36030->22614|36060->22615|36172->22698|36202->22699|36324->22792|36354->22793|36751->23161|36781->23162|36844->23196|36874->23197|37269->23563|37299->23564|37359->23595|37389->23596|37784->23962|37814->23963|37871->23991|37901->23992|38195->24257|38225->24258|38267->24271|38297->24272|38405->24351|38435->24352|38717->24605|38747->24606|38957->24787|38987->24788|39080->24852|39110->24853|39325->25039|39355->25040|39468->25124|39498->25125|39699->25297|39729->25298|39777->25317|39807->25318|40037->25519|40067->25520|40171->25595|40201->25596|40395->25761|40425->25762|40511->25819|40541->25820|40621->25871|40651->25872|40854->26046|40884->26047|40944->26078|40974->26079|41174->26250|41204->26251|41260->26278|41290->26279|41487->26447|41517->26448|41564->26466|41594->26467|41678->26522|41708->26523|41917->26703|41947->26704|42144->26872|42174->26873|42234->26904|42264->26905|42444->27056|42474->27057|42530->27084|42560->27085|42734->27230|42764->27231|42868->27306|42898->27307|42959->27339|42989->27340|43126->27448|43156->27449|43207->27471|43237->27472|43294->27500|43324->27501|43385->27533|43415->27534|43550->27640|43580->27641|43631->27663|43661->27664|43696->27670|43726->27671|43845->27761|43875->27762|43921->27779|43951->27780|43993->27793|44023->27794|44123->27865|44153->27866|44502->28185|44533->28186|44650->28274|44680->28275|44737->28303|44767->28304|44814->28322|44844->28323|44992->28442|45022->28443|45127->28519|45157->28520|45316->28650|45346->28651|45477->28753|45507->28754|45567->28785|45597->28786|45715->28875|45745->28876|45802->28904|45832->28905|45951->28995|45981->28996|46015->29001|46045->29002|46125->29053|46155->29054|46205->29075|46235->29076|46380->29192|46410->29193|46450->29204|46480->29205|46603->29299|46633->29300|46853->29491|46883->29492|46933->29513|46963->29514|47007->29529|47037->29530|47170->29634|47200->29635|47354->29760|47384->29761|47531->29879|47561->29880|47601->29891|47631->29892|47679->29911|47709->29912|47751->29925|47781->29926|47962->30078|47992->30079|48096->30154|48126->30155|48332->30332|48362->30333|48492->30434|48522->30435|48794->30678|48824->30679|48884->30710|48914->30711|49183->30951|49213->30952|49270->30980|49300->30981|49610->31262|49640->31263|49698->31292|49728->31293|49993->31529|50023->31530|50058->31536|50088->31537|50135->31555|50165->31556|50207->31569|50237->31570|50387->31691|50417->31692|50547->31793|50577->31794|50858->32046|50888->32047|50948->32078|50978->32079|51256->32328|51286->32329|51343->32357|51373->32358|51692->32648|51722->32649|51780->32678|51810->32679|52084->32924|52114->32925|52149->32931|52179->32932|52226->32950|52256->32951|52298->32964|52328->32965|52428->33036|52458->33037|52550->33100|52580->33101|52778->33270|52808->33271|52907->33341|52937->33342|53017->33393|53047->33394|53118->33436|53148->33437|53338->33598|53368->33599|53699->33901|53729->33902|53897->34041|53927->34042|54090->34176|54120->34177|54207->34235|54237->34236|54325->34295|54355->34296|54390->34302|54420->34303|54597->34451|54627->34452|54674->34470|54704->34471|54771->34509|54801->34510|54878->34558|54908->34559|55068->34690|55098->34691|55185->34749|55215->34750|55303->34809|55333->34810|55367->34815|55397->34816|55571->34961|55601->34962|55647->34979|55677->34980|55768->35042|55798->35043|55896->35112|55926->35113|56082->35240|56112->35241|56191->35291|56221->35292|56293->35335|56323->35336|56499->35483|56529->35484|56582->35508|56612->35509|56810->35678|56840->35679|56937->35746|56968->35747|57084->35834|57114->35835|57322->36014|57352->36015|57427->36061|57457->36062|57487->36063|57533->36080|57563->36081|57606->36095|57636->36096|57726->36157|57756->36158|57809->36182|57839->36183|58018->36333|58048->36334|58091->36348|58121->36349|58229->36428|58259->36429|58312->36453|58342->36454|58545->36628|58575->36629|58618->36643|58648->36644|58745->36712|58775->36713|58828->36737|58858->36738|59048->36899|59078->36900|59121->36914|59151->36915|59232->36967|59262->36968|59381->37058|59411->37059|59505->37124|59535->37125|59577->37138|59607->37139|59681->37184|59711->37185|59974->37419|60004->37420|60076->37463|60106->37464|60178->37507|60208->37508|60444->37715|60474->37716|60507->37720|60537->37721|60772->37927|60802->37928|60852->37949|60882->37950|60928->37967|60958->37968|60991->37972|61021->37973|61180->38103|61210->38104|61253->38118|61283->38119|61342->38149|61372->38150|61549->38298|61579->38299|61757->38448|61787->38449|61878->38511|61908->38512|62086->38661|62116->38662|62204->38721|62234->38722|62431->38890|62461->38891|62674->39075|62704->39076|62739->39082|62769->39083|62981->39266|63011->39267|63098->39325|63128->39326|63172->39341|63202->39342|63298->39409|63328->39410|63448->39501|63478->39502|63574->39569|63604->39570|63771->39708|63801->39709|63900->39779|63930->39780|64022->39843|64052->39844|64150->39913|64180->39914|64391->40096|64421->40097|64552->40199|64582->40200|64753->40342|64783->40343|65009->40539|65040->40540|65249->40719|65280->40720|65355->40765|65386->40766|65473->40823|65504->40824|65621->40911|65652->40912|65686->40916|65717->40917|65837->41007|65868->41008|65919->41029|65950->41030|65997->41047|66028->41048|66073->41063|66104->41064|66234->41164|66265->41165|66537->41407|66568->41408|66792->41602|66823->41603|67032->41782|67063->41783|67138->41828|67169->41829|67244->41874|67275->41875|67389->41959|67420->41960|67454->41964|67485->41965|67593->42043|67624->42044|67675->42065|67706->42066|67753->42083|67784->42084|67829->42099|67860->42100|67950->42160|67981->42161|68400->42550|68431->42551|68632->42722|68663->42723|68772->42801|68804->42802|68882->42850|68913->42851|69122->43030|69153->43031|69228->43076|69259->43077|69376->43164|69407->43165|69495->43223|69526->43224|69586->43254|69617->43255|69933->43541|69964->43542|70017->43565|70048->43566|70095->43583|70126->43584|70170->43598|70201->43599|70270->43638|70301->43639|70407->43715|70438->43716|70486->43734|70517->43735|70625->43813|70656->43814|70702->43830|70733->43831|70843->43911|70874->43912|70920->43928|70951->43929|71061->44009|71092->44010|71138->44026|71169->44027|71278->44106|71309->44107|71355->44123|71386->44124|71512->44220|71543->44221|71612->44260|71643->44261|71716->44304|71747->44305|71778->44306|72215->44713|72246->44714|72293->44731|72324->44732|72399->44777|72430->44778|72526->44844|72557->44845|72709->44967|72740->44968|72968->45166|72999->45167|73208->45346|73239->45347|73312->45390|73343->45391|73420->45438|73451->45439|73527->45485|73558->45486|73661->45559|73692->45560|73780->45618|73811->45619|73862->45640|73893->45641|73941->45659|73972->45660|74017->45675|74048->45676|74131->45729|74162->45730|74648->46186|74679->46187|74904->46382|74935->46383|75100->46517|75132->46518|75210->46566|75241->46567|75450->46746|75481->46747|75556->46792|75587->46793|75638->46814|75669->46815|75757->46873|75788->46874|75839->46895|75870->46896|75918->46914|75949->46915|75993->46929|76024->46930|76093->46969|76124->46970|76229->47045|76260->47046|76308->47064|76339->47065|76446->47142|76477->47143|76523->47159|76554->47160|76663->47239|76694->47240|76740->47256|76771->47257|76897->47353|76928->47354|76997->47393|77028->47394|77101->47437|77132->47438|77163->47439|77458->47704|77489->47705|77536->47722|77567->47723|77642->47768|77673->47769|77731->47797|77762->47798|77858->47864|77889->47865|78053->47999|78084->48000|78174->48060|78205->48061|78311->48137|78342->48138|78507->48273|78538->48274|78646->48352|78677->48353|78951->48597|78982->48598|79134->48720|79165->48721|79216->48742|79247->48743|79296->48762|79327->48763|79372->48778|79403->48779|79532->48878|79563->48879|79673->48959|79704->48960|79836->49062|79867->49063|79969->49135|80000->49136|80179->49286|80210->49287|80336->49384|80367->49385|80437->49425|80468->49426|80857->49785|80888->49786|80985->49853|81016->49854|81120->49928|81151->49929|81430->50178|81461->50179|81611->50299|81642->50300|81689->50317|81720->50318|81765->50333|81796->50334|81874->50382|81905->50383|81992->50440|82023->50441|82147->50535|82178->50536|82278->50606|82309->50607|82441->50709|82472->50710|82574->50782|82605->50783|82773->50921|82804->50922|83201->51289|83232->51290|83329->51357|83360->51358|83464->51432|83495->51433|83774->51682|83805->51683|83961->51809|83992->51810|84039->51827|84070->51828|84115->51843|84146->51844|84222->51890|84253->51891|84340->51948|84371->51949|84434->51982|84465->51983|84568->52056|84599->52057|84711->52139|84742->52140|84813->52181|84844->52182|84927->52235|84958->52236|85002->52250|85033->52251|85122->52310|85153->52311|85253->52381|85284->52382|85452->52520|85483->52521|86540->53548|86571->53549|86692->53640|86723->53641|88350->55238|88381->55239|88450->55278|88481->55279|88669->55437|88700->55438|88770->55478|88801->55479|88981->55629|89012->55630|89122->55710|89153->55711|89359->55887|89390->55888|89484->55952|89515->55953|89569->55977|89600->55978|89809->56157|89840->56158|90049->56337|90080->56338|90155->56383|90186->56384|90481->56649|90512->56650|90600->56708|90631->56709|90691->56739|90722->56740|91042->57030|91073->57031|91126->57054|91157->57055|91204->57072|91235->57073|91280->57088|91311->57089|91418->57166|91449->57167|91528->57216|91559->57217|91725->57353|91756->57354|91847->57415|91878->57416|91946->57454|91977->57455|92031->57479|92062->57480|92274->57662|92305->57663|92514->57842|92545->57843|92620->57888|92651->57889|92745->57953|92776->57954|92864->58012|92895->58013|92955->58043|92986->58044|93306->58334|93337->58335|93390->58358|93421->58359|93468->58376|93499->58377|93543->58391|93574->58392|93644->58432|93675->58433|94244->58972|94275->58973|94527->59195|94558->59196|94604->59212|94635->59213|94798->59346|94829->59347|94929->59417|94960->59418|95044->59472|95075->59473|95118->59486|95149->59487|95210->59518|95241->59519|95299->59547|95330->59548|95419->59607|95450->59608|95531->59659|95562->59660|95654->59722|95685->59723|95786->59794|95817->59795|95917->59865|95948->59866|95993->59881|96024->59882|96122->59950|96153->59951|96303->60071|96334->60072|96435->60143|96466->60144|96594->60242|96625->60243|96699->60287|96730->60288|96787->60315|96818->60316|97038->60506|97069->60507|97287->60695|97318->60696|97396->60744|97427->60745|97527->60815|97558->60816|97649->60877|97680->60878|97743->60911|97774->60912|98112->61220|98143->61221|98199->61247|98230->61248|98280->61268|98311->61269|98358->61286|98389->61287
                    LINES: 20->2|33->5|35->11|37->13|37->13|37->13|38->14|38->14|38->14|39->15|39->15|39->15|40->16|40->16|40->16|41->17|41->17|41->17|42->18|42->18|42->18|43->19|43->19|43->19|44->20|44->20|44->20|45->21|45->21|45->21|61->37|61->37|61->37|62->38|62->38|62->38|63->39|63->39|63->39|64->40|64->40|64->40|208->184|208->184|210->186|210->186|212->188|212->188|214->190|214->190|216->192|216->192|218->194|218->194|220->196|220->196|223->199|223->199|225->201|225->201|227->203|227->203|230->206|230->206|233->209|233->209|236->212|236->212|238->214|238->214|240->216|240->216|245->221|245->221|247->223|247->223|249->225|249->225|251->227|251->227|254->230|254->230|256->232|256->232|259->235|259->235|261->237|261->237|263->239|263->239|265->241|265->241|270->246|270->246|272->248|272->248|274->250|274->250|276->252|276->252|279->255|279->255|283->259|283->259|284->260|284->260|286->262|286->262|287->263|287->263|289->265|289->265|290->266|290->266|292->268|292->268|294->270|294->270|296->272|296->272|297->273|297->273|299->275|299->275|301->277|301->277|305->281|305->281|307->283|307->283|313->289|313->289|315->291|315->291|317->293|317->293|319->295|319->295|322->298|322->298|324->300|324->300|328->304|328->304|330->306|330->306|332->308|332->308|334->310|334->310|338->314|338->314|340->316|340->316|342->318|342->318|344->320|344->320|349->325|349->325|350->326|350->326|354->330|354->330|355->331|355->331|359->335|359->335|361->337|361->337|363->339|363->339|365->341|365->341|367->343|367->343|369->345|369->345|371->347|371->347|373->349|373->349|380->356|380->356|382->358|382->358|384->360|384->360|385->361|385->361|390->366|390->366|392->368|392->368|393->369|393->369|395->371|395->371|396->372|396->372|398->374|398->374|399->375|399->375|401->377|401->377|402->378|402->378|404->380|404->380|407->383|407->383|412->388|412->388|414->390|414->390|418->394|418->394|421->397|421->397|429->405|429->405|432->408|432->408|432->408|435->411|435->411|442->418|442->418|446->422|446->422|448->424|448->424|456->432|456->432|461->437|461->437|462->438|462->438|463->439|463->439|463->439|463->439|464->440|464->440|465->441|465->441|466->442|466->442|475->451|475->451|478->454|478->454|479->455|479->455|481->457|481->457|482->458|482->458|483->459|483->459|483->459|485->461|485->461|486->462|486->462|487->463|487->463|493->469|493->469|495->471|495->471|496->472|496->472|497->473|497->473|500->476|500->476|502->478|502->478|506->482|506->482|510->486|510->486|510->486|510->486|514->490|514->490|514->490|514->490|518->494|518->494|518->494|518->494|520->496|520->496|521->497|521->497|522->498|522->498|525->501|525->501|526->502|526->502|526->502|526->502|527->503|527->503|530->506|530->506|531->507|531->507|531->507|531->507|533->509|533->509|534->510|534->510|535->511|535->511|536->512|536->512|537->513|537->513|538->514|538->514|541->517|541->517|555->531|555->531|556->532|556->532|558->534|558->534|560->536|560->536|561->537|561->537|562->538|562->538|563->539|563->539|564->540|564->540|565->541|565->541|567->543|567->543|568->544|568->544|570->546|570->546|572->548|572->548|574->550|574->550|575->551|575->551|576->552|576->552|581->557|581->557|582->558|582->558|583->559|583->559|584->560|584->560|587->563|587->563|590->566|590->566|590->566|590->566|593->569|593->569|594->570|594->570|595->571|595->571|597->573|597->573|597->573|597->573|597->573|597->573|597->573|597->573|597->573|597->573|598->574|598->574|600->576|600->576|602->578|602->578|604->580|604->580|606->582|606->582|608->584|608->584|610->586|610->586|612->588|612->588|616->592|616->592|620->596|620->596|621->597|621->597|621->597|621->597|622->598|622->598|622->598|622->598|623->599|623->599|624->600|624->600|624->600|624->600|625->601|625->601|625->601|625->601|626->602|626->602|626->602|626->602|627->603|627->603|627->603|627->603|630->606|630->606|632->608|632->608|634->610|634->610|642->618|642->618|642->618|642->618|650->626|650->626|650->626|650->626|658->634|658->634|658->634|658->634|664->640|664->640|665->641|665->641|668->644|668->644|673->649|673->649|676->652|676->652|677->653|677->653|680->656|680->656|682->658|682->658|685->661|685->661|685->661|685->661|690->666|690->666|692->668|692->668|695->671|695->671|696->672|696->672|697->673|697->673|700->676|700->676|700->676|700->676|703->679|703->679|703->679|703->679|706->682|706->682|708->684|708->684|709->685|709->685|712->688|712->688|715->691|715->691|715->691|715->691|718->694|718->694|718->694|718->694|721->697|721->697|722->698|722->698|723->699|723->699|726->702|726->702|727->703|727->703|727->703|727->703|728->704|728->704|731->707|731->707|732->708|732->708|732->708|732->708|734->710|734->710|735->711|735->711|736->712|736->712|739->715|739->715|744->720|744->720|746->722|746->722|746->722|746->722|747->723|747->723|750->726|750->726|752->728|752->728|755->731|755->731|757->733|757->733|757->733|757->733|759->735|759->735|759->735|759->735|761->737|761->737|761->737|761->737|763->739|763->739|765->741|765->741|768->744|768->744|768->744|768->744|770->746|770->746|773->749|773->749|774->750|774->750|775->751|775->751|778->754|778->754|781->757|781->757|783->759|783->759|783->759|783->759|784->760|784->760|785->761|785->761|788->764|788->764|790->766|790->766|794->770|794->770|796->772|796->772|800->776|800->776|800->776|800->776|805->781|805->781|805->781|805->781|810->786|810->786|810->786|810->786|815->791|815->791|815->791|815->791|817->793|817->793|818->794|818->794|821->797|821->797|823->799|823->799|827->803|827->803|827->803|827->803|832->808|832->808|832->808|832->808|837->813|837->813|837->813|837->813|842->818|842->818|842->818|842->818|844->820|844->820|845->821|845->821|848->824|848->824|850->826|850->826|853->829|853->829|854->830|854->830|856->832|856->832|858->834|858->834|861->837|861->837|869->845|869->845|871->847|871->847|874->850|874->850|874->850|874->850|876->852|876->852|876->852|876->852|879->855|879->855|881->857|881->857|881->857|881->857|882->858|882->858|885->861|885->861|885->861|885->861|887->863|887->863|887->863|887->863|890->866|890->866|891->867|891->867|891->867|891->867|892->868|892->868|894->870|894->870|896->872|896->872|898->874|898->874|901->877|901->877|902->878|902->878|905->881|905->881|905->881|905->881|907->883|907->883|910->886|910->886|911->887|911->887|911->887|912->888|912->888|913->889|913->889|915->891|915->891|916->892|916->892|919->895|919->895|920->896|920->896|922->898|922->898|923->899|923->899|926->902|926->902|927->903|927->903|929->905|929->905|930->906|930->906|933->909|933->909|934->910|934->910|936->912|936->912|938->914|938->914|940->916|940->916|941->917|941->917|943->919|943->919|949->925|949->925|950->926|950->926|951->927|951->927|955->931|955->931|955->931|955->931|960->936|960->936|961->937|961->937|962->938|962->938|962->938|962->938|965->941|965->941|967->943|967->943|969->945|969->945|973->949|973->949|975->951|975->951|976->952|976->952|978->954|978->954|980->956|980->956|983->959|983->959|986->962|986->962|986->962|986->962|989->965|989->965|991->967|991->967|992->968|992->968|994->970|994->970|997->973|997->973|999->975|999->975|1003->979|1003->979|1005->981|1005->981|1007->983|1007->983|1009->985|1009->985|1014->990|1014->990|1017->993|1017->993|1020->996|1020->996|1024->1000|1024->1000|1027->1003|1027->1003|1028->1004|1028->1004|1029->1005|1029->1005|1031->1007|1031->1007|1031->1007|1031->1007|1033->1009|1033->1009|1034->1010|1034->1010|1035->1011|1035->1011|1037->1013|1037->1013|1040->1016|1040->1016|1048->1024|1048->1024|1052->1028|1052->1028|1055->1031|1055->1031|1056->1032|1056->1032|1057->1033|1057->1033|1059->1035|1059->1035|1059->1035|1059->1035|1061->1037|1061->1037|1062->1038|1062->1038|1063->1039|1063->1039|1065->1041|1065->1041|1067->1043|1067->1043|1074->1050|1074->1050|1077->1053|1077->1053|1077->1053|1077->1053|1078->1054|1078->1054|1081->1057|1081->1057|1082->1058|1082->1058|1084->1060|1084->1060|1085->1061|1085->1061|1086->1062|1086->1062|1092->1068|1092->1068|1093->1069|1093->1069|1094->1070|1094->1070|1095->1071|1095->1071|1097->1073|1097->1073|1098->1074|1098->1074|1098->1074|1098->1074|1099->1075|1099->1075|1099->1075|1099->1075|1100->1076|1100->1076|1100->1076|1100->1076|1101->1077|1101->1077|1101->1077|1101->1077|1102->1078|1102->1078|1102->1078|1102->1078|1106->1082|1106->1082|1107->1083|1107->1083|1108->1084|1108->1084|1108->1084|1116->1092|1116->1092|1117->1093|1117->1093|1120->1096|1120->1096|1122->1098|1122->1098|1124->1100|1124->1100|1128->1104|1128->1104|1131->1107|1131->1107|1132->1108|1132->1108|1132->1108|1132->1108|1133->1109|1133->1109|1135->1111|1135->1111|1136->1112|1136->1112|1137->1113|1137->1113|1139->1115|1139->1115|1141->1117|1141->1117|1143->1119|1143->1119|1152->1128|1152->1128|1156->1132|1156->1132|1156->1132|1156->1132|1157->1133|1157->1133|1160->1136|1160->1136|1161->1137|1161->1137|1162->1138|1162->1138|1163->1139|1163->1139|1164->1140|1164->1140|1166->1142|1166->1142|1167->1143|1167->1143|1169->1145|1169->1145|1170->1146|1170->1146|1170->1146|1170->1146|1171->1147|1171->1147|1171->1147|1171->1147|1172->1148|1172->1148|1172->1148|1172->1148|1176->1152|1176->1152|1177->1153|1177->1153|1178->1154|1178->1154|1178->1154|1183->1159|1183->1159|1184->1160|1184->1160|1187->1163|1187->1163|1189->1165|1189->1165|1190->1166|1190->1166|1193->1169|1193->1169|1195->1171|1195->1171|1197->1173|1197->1173|1200->1176|1200->1176|1202->1178|1202->1178|1205->1181|1205->1181|1207->1183|1207->1183|1208->1184|1208->1184|1209->1185|1209->1185|1210->1186|1210->1186|1213->1189|1213->1189|1214->1190|1214->1190|1217->1193|1217->1193|1219->1195|1219->1195|1222->1198|1222->1198|1224->1200|1224->1200|1225->1201|1225->1201|1230->1206|1230->1206|1232->1208|1232->1208|1234->1210|1234->1210|1237->1213|1237->1213|1239->1215|1239->1215|1240->1216|1240->1216|1241->1217|1241->1217|1242->1218|1242->1218|1244->1220|1244->1220|1247->1223|1247->1223|1248->1224|1248->1224|1251->1227|1251->1227|1253->1229|1253->1229|1256->1232|1256->1232|1261->1237|1261->1237|1263->1239|1263->1239|1265->1241|1265->1241|1268->1244|1268->1244|1270->1246|1270->1246|1271->1247|1271->1247|1272->1248|1272->1248|1274->1250|1274->1250|1276->1252|1276->1252|1278->1254|1278->1254|1281->1257|1281->1257|1284->1260|1284->1260|1286->1262|1286->1262|1288->1264|1288->1264|1289->1265|1289->1265|1291->1267|1291->1267|1293->1269|1293->1269|1298->1274|1298->1274|1312->1288|1312->1288|1315->1291|1315->1291|1341->1317|1341->1317|1343->1319|1343->1319|1346->1322|1346->1322|1347->1323|1347->1323|1350->1326|1350->1326|1351->1327|1351->1327|1354->1330|1354->1330|1357->1333|1357->1333|1358->1334|1358->1334|1362->1338|1362->1338|1365->1341|1365->1341|1366->1342|1366->1342|1373->1349|1373->1349|1374->1350|1374->1350|1375->1351|1375->1351|1381->1357|1381->1357|1382->1358|1382->1358|1383->1359|1383->1359|1385->1361|1385->1361|1388->1364|1388->1364|1389->1365|1389->1365|1391->1367|1391->1367|1394->1370|1394->1370|1396->1372|1396->1372|1397->1373|1397->1373|1401->1377|1401->1377|1404->1380|1404->1380|1405->1381|1405->1381|1407->1383|1407->1383|1408->1384|1408->1384|1409->1385|1409->1385|1415->1391|1415->1391|1416->1392|1416->1392|1417->1393|1417->1393|1418->1394|1418->1394|1420->1396|1420->1396|1432->1408|1432->1408|1436->1412|1436->1412|1438->1414|1438->1414|1441->1417|1441->1417|1443->1419|1443->1419|1445->1421|1445->1421|1446->1422|1446->1422|1449->1425|1449->1425|1450->1426|1450->1426|1451->1427|1451->1427|1453->1429|1453->1429|1456->1432|1456->1432|1458->1434|1458->1434|1461->1437|1461->1437|1462->1438|1462->1438|1464->1440|1464->1440|1468->1444|1468->1444|1471->1447|1471->1447|1474->1450|1474->1450|1477->1453|1477->1453|1478->1454|1478->1454|1483->1459|1483->1459|1486->1462|1486->1462|1487->1463|1487->1463|1489->1465|1489->1465|1490->1466|1490->1466|1491->1467|1491->1467|1497->1473|1497->1473|1498->1474|1498->1474|1499->1475|1499->1475|1500->1476|1500->1476
                    -- GENERATED --
                */
            