
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object metadataSearch extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.40*/("""

"""),_display_(Seq[Any](/*3.2*/main("Dataset user metadata search")/*3.38*/ {_display_(Seq[Any](format.raw/*3.40*/("""
<div class="page-header">
  <h1>Dataset user metadata search</h1>
</div>
<div class="row">
	<div class="col-md-12">
		<form class="form-inline">
			<h5>Select properties to search for (in strings, input * alone to search for any string value):</h5>
			<div id='queryUserMetadata' class='usr_md_'>
				&nbsp;&nbsp;&nbsp;<button class="usr_md_" type="button">Add property</button>
				<ul class="usr_md_ usr_md_search_list"></ul>
				<button class="usr_md_" type="button">Add disjunction</button>
				<br />
				<button class="usr_md_submit btn" type="button">Submit</button></div>
		</form>	
		<table id='resultTable' class="table table-bordered table-hover" style="display:none;">
			<thead>
				<tr>
					"""),_display_(Seq[Any](/*21.7*/if(user.isDefined)/*21.25*/ {_display_(Seq[Any](format.raw/*21.27*/("""
						<th style="width: 25%">Name</th>
						<th style="width: 15%">Created</th>
						<th style="width: 40%">Description</th>
						<th style="width: 10%"></th>
						<th style="width: 10%"></th>
					""")))})),format.raw/*27.7*/("""
					"""),_display_(Seq[Any](/*28.7*/if(!user.isDefined)/*28.26*/ {_display_(Seq[Any](format.raw/*28.28*/("""
						<th style="width: 25%">Name</th>
						<th style="width: 15%">Created</th>
						<th style="width: 50%">Description</th>
						<th style="width: 10%"></th>
					""")))})),format.raw/*33.7*/("""
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<ul class="pager">
			<li class="previous" style="visibility:hidden;"><a class="btn btn-link" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
			<li class ="next" style="visibility:hidden;"><a class="btn btn-link" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
		</ul>
	</div>
</div>
<script language="javascript">
	window["userDefined"] = false;
	window["userId"] = "";
</script>
"""),_display_(Seq[Any](/*49.2*/if(user.isDefined)/*49.20*/ {_display_(Seq[Any](format.raw/*49.22*/("""
	<script language="javascript">
		window["userDefined"] = true;
		window["userId"] = """"),_display_(Seq[Any](/*52.24*/user/*52.28*/.get.identityId.userId)),format.raw/*52.50*/("""";
	</script>
""")))})),format.raw/*54.2*/("""
<script language="javascript">	
	var queryIp = """"),_display_(Seq[Any](/*56.18*/api/*56.21*/.routes.Datasets.searchDatasetsUserMetadata)),format.raw/*56.64*/("""";
	var modelIp = """"),_display_(Seq[Any](/*57.18*/(routes.Assets.at("datasetsUserMetadataModel")))),format.raw/*57.65*/("""";
	var searchOn = "datasets";
	var searchFor = "userMetadata";
</script>
<script src=""""),_display_(Seq[Any](/*61.15*/routes/*61.21*/.Assets.at("javascripts/searchUserMetadata.js"))),format.raw/*61.68*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*62.15*/routes/*62.21*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*62.68*/("""" type="text/javascript"></script>
""")))})),format.raw/*63.2*/("""
"""))}
    }
    
    def render(user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply()(user)
    
    def f:(() => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = () => (user) => apply()(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:25 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/metadataSearch.scala.html
                    HASH: 52d50140decc3c5241a3efbceec834c7a6c1d5a0
                    MATRIX: 609->1|741->39|778->42|822->78|861->80|1604->788|1631->806|1671->808|1906->1012|1948->1019|1976->1038|2016->1040|2216->1209|2758->1716|2785->1734|2825->1736|2949->1824|2962->1828|3006->1850|3052->1865|3138->1915|3150->1918|3215->1961|3271->1981|3340->2028|3464->2116|3479->2122|3548->2169|3633->2218|3648->2224|3717->2271|3784->2307
                    LINES: 20->1|23->1|25->3|25->3|25->3|43->21|43->21|43->21|49->27|50->28|50->28|50->28|55->33|71->49|71->49|71->49|74->52|74->52|74->52|76->54|78->56|78->56|78->56|79->57|79->57|83->61|83->61|83->61|84->62|84->62|84->62|85->63
                    -- GENERATED --
                */
            