
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object startcontainer extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[String,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(URL: String, message: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.68*/("""

"""),format.raw/*7.5*/("""




<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*12.46*/routes/*12.52*/.Assets.at("stylesheets/dragdrop.css"))),format.raw/*12.90*/("""">
"""),_display_(Seq[Any](/*13.2*/if(message != "")/*13.19*/ {_display_(Seq[Any](format.raw/*13.21*/("""
<div class="row">
	<div class="col-md-12">
		<dt>"""),_display_(Seq[Any](/*16.8*/message)),format.raw/*16.15*/(""":</dt>
		<a href="""),_display_(Seq[Any](/*17.12*/URL)),format.raw/*17.15*/(""" target="_blank">"""),_display_(Seq[Any](/*17.33*/URL)),format.raw/*17.36*/("""</a>
	</div>
</div>
""")))})),format.raw/*20.2*/("""

"""))}
    }
    
    def render(URL:String,message:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(URL,message)(user)
    
    def f:((String,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (URL,message) => (user) => apply(URL,message)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:25 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/startcontainer.scala.html
                    HASH: b9818d6e374f01bdabc5427093f9b039579d9d26
                    MATRIX: 623->1|783->67|811->174|897->224|912->230|972->268|1011->272|1037->289|1077->291|1163->342|1192->349|1246->367|1271->370|1325->388|1350->391|1402->412
                    LINES: 20->1|23->1|25->7|30->12|30->12|30->12|31->13|31->13|31->13|34->16|34->16|35->17|35->17|35->17|35->17|38->20
                    -- GENERATED --
                */
            