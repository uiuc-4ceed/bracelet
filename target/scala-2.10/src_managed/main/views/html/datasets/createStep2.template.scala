
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object createStep2 extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Dataset,List[ProjectSpace],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset, spaces: List[ProjectSpace])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import _root_.util.Formatters._


Seq[Any](format.raw/*1.84*/("""
"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main(Messages("add.to", Messages("dataset.title")))/*5.53*/ {_display_(Seq[Any](format.raw/*5.55*/("""



    <!-- Custom items for the create dataset workflow -->
    <script src=""""),_display_(Seq[Any](/*10.19*/routes/*10.25*/.Assets.at("javascripts/dataset-attach-fileuploader.js"))),format.raw/*10.81*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*11.19*/routes/*11.25*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*11.67*/("""" type="text/javascript"></script>

    <div class="row">
    <ol class="breadcrumb">
        """),_display_(Seq[Any](/*15.10*/if(spaces.length == 1)/*15.32*/ {_display_(Seq[Any](format.raw/*15.34*/("""
            <li><span class="glyphicon glyphicon-tent"></span> <a href=""""),_display_(Seq[Any](/*16.74*/routes/*16.80*/.Spaces.getSpace(spaces.head.id))),format.raw/*16.112*/("""" title=""""),_display_(Seq[Any](/*16.122*/spaces/*16.128*/.head.name)),format.raw/*16.138*/(""""> """),_display_(Seq[Any](/*16.142*/Html(ellipsize(spaces.head.name, 18)))),format.raw/*16.179*/("""</a></li>
        """)))}/*17.11*/else/*17.16*/{_display_(Seq[Any](format.raw/*17.17*/("""
            """),_display_(Seq[Any](/*18.14*/if(spaces.length > 1)/*18.35*/ {_display_(Seq[Any](format.raw/*18.37*/("""
                <li>
                    <span class="dropdown">
                        <button class="btn-link dropdown-toggle" type="button" id="dropdown_space_list" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-tent"></span> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" arialanelledby="dropdown_space_list">
                        """),_display_(Seq[Any](/*26.26*/spaces/*26.32*/.map/*26.36*/{ s =>_display_(Seq[Any](format.raw/*26.42*/("""
                            <li><a href=""""),_display_(Seq[Any](/*27.43*/routes/*27.49*/.Spaces.getSpace(s.id))),format.raw/*27.71*/("""" title="s.name"><span class="glyphicon glyphicon-tent"></span> """),_display_(Seq[Any](/*27.136*/Html(ellipsize(s.name, 18)))),format.raw/*27.163*/("""</a></li>
                        """)))})),format.raw/*28.26*/("""
                        </ul>
                    </span>
                </li>
            """)))}/*32.15*/else/*32.20*/{_display_(Seq[Any](format.raw/*32.21*/("""
                <li><span class="glyphicon glyphicon-user"></span> <a href = """"),_display_(Seq[Any](/*33.80*/routes/*33.86*/.Profile.viewProfileUUID(dataset.author.id))),format.raw/*33.129*/(""""> """),_display_(Seq[Any](/*33.133*/dataset/*33.140*/.author.fullName)),format.raw/*33.156*/("""</a></li>
            """)))})),format.raw/*34.14*/("""
            <li><span class="glyphicon glyphicon-briefcase"></span> <a href=""""),_display_(Seq[Any](/*35.79*/routes/*35.85*/.Datasets.dataset(dataset.id))),format.raw/*35.114*/(""""> <span title=""""),_display_(Seq[Any](/*35.131*/dataset/*35.138*/.name)),format.raw/*35.143*/(""""> """),_display_(Seq[Any](/*35.147*/Html(ellipsize(dataset.name, 18)))),format.raw/*35.180*/("""</span> </a> </li>
            <li> <span class="glyphicon glyphicon-upload"></span> Upload Files</li>
        """)))})),format.raw/*37.10*/("""
    </ol>
    </div>
    <div class="page-header">
        <h1> """),_display_(Seq[Any](/*41.15*/Messages("dataset.title"))),format.raw/*41.40*/(""" Created</h1>
    </div>
    <div class="row message-row">
        <div class="col-md-12">
            <p>"""),_display_(Seq[Any](/*45.17*/Messages("create.step2.message", Messages("dataset.title").toLowerCase))),format.raw/*45.88*/(""" <a href=""""),_display_(Seq[Any](/*45.99*/routes/*45.105*/.Datasets.dataset(dataset.id))),format.raw/*45.134*/("""">"""),_display_(Seq[Any](/*45.137*/dataset/*45.144*/.name)),format.raw/*45.149*/("""</a> """),_display_(Seq[Any](/*45.155*/Messages("create.step2.message.2", Messages("dataset.title").toLowerCase))),format.raw/*45.228*/("""
            </p>
        </div>
    </div>
    <div class="row button-row">
        <div class="col-md-12 bottom-padding top-padding">
            <a class="btn btn-primary" href=""""),_display_(Seq[Any](/*51.47*/routes/*51.53*/.Datasets.dataset(dataset.id))),format.raw/*51.82*/("""">
                <span class="glyphicon glyphicon-list"></span> View """),_display_(Seq[Any](/*52.70*/Messages("dataset.title"))),format.raw/*52.95*/("""</a>
        </div>
    </div>
    <h3>Add Files</h3>
    """),_display_(Seq[Any](/*56.6*/datasets/*56.14*/.uploadFiles(dataset, None))),format.raw/*56.41*/("""
""")))})))}
    }
    
    def render(dataset:Dataset,spaces:List[ProjectSpace],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,spaces)(user)
    
    def f:((Dataset,List[ProjectSpace]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,spaces) => (user) => apply(dataset,spaces)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:49 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/createStep2.scala.html
                    HASH: 4303f2cca636a378b4d48b5beaa49aa27e449cb4
                    MATRIX: 642->1|881->83|908->148|944->150|1003->201|1042->203|1158->283|1173->289|1251->345|1340->398|1355->404|1419->446|1550->541|1581->563|1621->565|1731->639|1746->645|1801->677|1848->687|1864->693|1897->703|1938->707|1998->744|2036->764|2049->769|2088->770|2138->784|2168->805|2208->807|2754->1317|2769->1323|2782->1327|2826->1333|2905->1376|2920->1382|2964->1404|3066->1469|3116->1496|3183->1531|3296->1626|3309->1631|3348->1632|3464->1712|3479->1718|3545->1761|3586->1765|3603->1772|3642->1788|3697->1811|3812->1890|3827->1896|3879->1925|3933->1942|3950->1949|3978->1954|4019->1958|4075->1991|4219->2103|4321->2169|4368->2194|4511->2301|4604->2372|4651->2383|4667->2389|4719->2418|4759->2421|4776->2428|4804->2433|4847->2439|4943->2512|5161->2694|5176->2700|5227->2729|5335->2801|5382->2826|5476->2885|5493->2893|5542->2920
                    LINES: 20->1|26->1|27->4|28->5|28->5|28->5|33->10|33->10|33->10|34->11|34->11|34->11|38->15|38->15|38->15|39->16|39->16|39->16|39->16|39->16|39->16|39->16|39->16|40->17|40->17|40->17|41->18|41->18|41->18|49->26|49->26|49->26|49->26|50->27|50->27|50->27|50->27|50->27|51->28|55->32|55->32|55->32|56->33|56->33|56->33|56->33|56->33|56->33|57->34|58->35|58->35|58->35|58->35|58->35|58->35|58->35|58->35|60->37|64->41|64->41|68->45|68->45|68->45|68->45|68->45|68->45|68->45|68->45|68->45|68->45|74->51|74->51|74->51|75->52|75->52|79->56|79->56|79->56
                    -- GENERATED --
                */
            