
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object collections extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[Dataset,List[models.Collection],Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset, collectionsInside: List[models.Collection], canAddDatasetToCollection: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.libs.json.Json


Seq[Any](format.raw/*1.136*/("""

"""),format.raw/*5.1*/("""
<div class="row">
    <div class="col-md-12 col-sm-12 col-lg-12">
        <h4>"""),_display_(Seq[Any](/*8.14*/Messages("a.contains.b", Messages("collections.title"), Messages("dataset.title")))),format.raw/*8.96*/("""</h4>
    </div>
</div>

<div class="row bottom-padding">
    <div id="collectionsList" class="col-md-12 col-sm-12 col-lg-12">
        """),_display_(Seq[Any](/*14.10*/collectionsInside/*14.27*/.map/*14.31*/ { collection =>_display_(Seq[Any](format.raw/*14.47*/("""
            <div id="col_"""),_display_(Seq[Any](/*15.27*/collection/*15.37*/.id)),format.raw/*15.40*/("""" class="row bottom-padding">
                <div class="col-md-2 col-sm-2 col-lg-2">
                    """),_display_(Seq[Any](/*17.22*/if(!collection.thumbnail_id.isEmpty)/*17.58*/{_display_(Seq[Any](format.raw/*17.59*/("""
                        <a href=""""),_display_(Seq[Any](/*18.35*/(routes.Collections.collection(collection.id)))),format.raw/*18.81*/("""">
                            <img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*19.76*/(routes.Files.thumbnail(UUID(collection.thumbnail_id.get))))),format.raw/*19.135*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*19.156*/Html(collection.name))),format.raw/*19.177*/("""">
                        </a>
                    """)))}/*21.23*/else/*21.28*/{_display_(Seq[Any](format.raw/*21.29*/("""
                        <a href=""""),_display_(Seq[Any](/*22.35*/routes/*22.41*/.Collections.collection(collection.id))),format.raw/*22.79*/("""">
                            <span class="smallicon glyphicon glyphicon-th-large"></span>
                        </a>
                    """)))})),format.raw/*25.22*/("""
                </div>
                <div class="col-md-10 col-sm-10 col-lg-10">
                    <div>
                        <a href=""""),_display_(Seq[Any](/*29.35*/(routes.Collections.collection(collection.id)))),format.raw/*29.81*/("""" id='"""),_display_(Seq[Any](/*29.88*/collection/*29.98*/.id)),format.raw/*29.101*/("""' class ="collection">"""),_display_(Seq[Any](/*29.124*/Html(collection.name))),format.raw/*29.145*/("""</a>
                    </div>
                    <div>
                        """),_display_(Seq[Any](/*32.26*/if(collection.datasetCount == 1)/*32.58*/ {_display_(Seq[Any](format.raw/*32.60*/("""
                            """),_display_(Seq[Any](/*33.30*/collection/*33.40*/.datasetCount)),format.raw/*33.53*/(""" dataset
                        """)))})),format.raw/*34.26*/("""
                        """),_display_(Seq[Any](/*35.26*/if(collection.datasetCount != 1)/*35.58*/ {_display_(Seq[Any](format.raw/*35.60*/("""
                            """),_display_(Seq[Any](/*36.30*/collection/*36.40*/.datasetCount)),format.raw/*36.53*/(""" datasets
                        """)))})),format.raw/*37.26*/("""
                        <!-- If the user can edit the collection, the elements are enabled, otherwise they are present but disabled to provide consistent UE. -->
                        """),_display_(Seq[Any](/*39.26*/if(Permission.checkPermission(Permission.RemoveResourceFromCollection, ResourceRef(ResourceRef.collection, collection.id)))/*39.149*/ {_display_(Seq[Any](format.raw/*39.151*/("""
                            | <button onclick="confirmRemoveResourceFromResourceEvent('collection','collection','"""),_display_(Seq[Any](/*40.115*/(collection.id))),format.raw/*40.130*/("""','dataset','"""),_display_(Seq[Any](/*40.144*/(dataset.id))),format.raw/*40.156*/("""',event)" class="btn btn-link btn-xs" title="Remove the dataset from the collection.">
                                <span class="glyphicon glyphicon-remove"></span> Remove</button>
                        """)))}/*42.27*/else/*42.32*/{_display_(Seq[Any](format.raw/*42.33*/("""
                            |
                            <div class="inline" title="No permission to remove from the collection">
                                <button class="btn btn-link btn-xs disabled"><span class="glyphicon glyphicon-remove"></span> Remove</button>
                            </div>
                        """)))})),format.raw/*47.26*/("""
                    </div>
                </div>
            </div>
        """)))})),format.raw/*51.10*/("""
    </div>
</div>

"""),_display_(Seq[Any](/*55.2*/if(canAddDatasetToCollection)/*55.31*/ {_display_(Seq[Any](format.raw/*55.33*/("""
    <div class="form-inline bottom-padding">
        <div class="input-group input-group-sm col-md-8">
            <select id="collectionAddSelect" class="form-control add-resource">
            </select>
            <span class="input-group-btn">
                <a href="#" class="btn btn-default btn-large" id="addCollectionBtn" title="Add Dataset to Collection" onclick="addToCollection('"""),_display_(Seq[Any](/*61.146*/dataset/*61.153*/.id)),format.raw/*61.156*/("""')">
                    <span class="glyphicon glyphicon-plus"></span> Add
                </a>
            </span>
        </div>
    </div>
""")))})),format.raw/*67.2*/("""


<script language="javascript">
    $("#collectionAddSelect").select2("""),format.raw/*71.39*/("""{"""),format.raw/*71.40*/("""
        theme: "bootstrap",
        placeholder: "Select a collection",
        allowClear: true,
        ajax: """),format.raw/*75.15*/("""{"""),format.raw/*75.16*/("""
            url: function(params) """),format.raw/*76.35*/("""{"""),format.raw/*76.36*/("""
                return jsRoutes.api.Collections.addDatasetToCollectionOptions(""""),_display_(Seq[Any](/*77.81*/dataset/*77.88*/.id)),format.raw/*77.91*/("""", params.term, null, 5).url;
            """),format.raw/*78.13*/("""}"""),format.raw/*78.14*/(""",
            data: function(params) """),format.raw/*79.36*/("""{"""),format.raw/*79.37*/("""
                return """),format.raw/*80.24*/("""{"""),format.raw/*80.25*/(""" title: params.term """),format.raw/*80.45*/("""}"""),format.raw/*80.46*/(""";
            """),format.raw/*81.13*/("""}"""),format.raw/*81.14*/(""",
            processResults: function(data, page) """),format.raw/*82.50*/("""{"""),format.raw/*82.51*/("""
                return """),format.raw/*83.24*/("""{"""),format.raw/*83.25*/("""results: data.filter(function(x) """),format.raw/*83.58*/("""{"""),format.raw/*83.59*/("""
                    var ids = $('.collection').map(function() """),format.raw/*84.63*/("""{"""),format.raw/*84.64*/("""
                        return $(this).attr('id');
                    """),format.raw/*86.21*/("""}"""),format.raw/*86.22*/(""");
                    return $.inArray(x.id, ids) == -1;
                """),format.raw/*88.17*/("""}"""),format.raw/*88.18*/(""").map(function(x) """),format.raw/*88.36*/("""{"""),format.raw/*88.37*/("""
                    return """),format.raw/*89.28*/("""{"""),format.raw/*89.29*/("""
                        text: x.collectionname,
                        id: x.id
                    """),format.raw/*92.21*/("""}"""),format.raw/*92.22*/("""
                """),format.raw/*93.17*/("""}"""),format.raw/*93.18*/(""")"""),format.raw/*93.19*/("""}"""),format.raw/*93.20*/(""";
            """),format.raw/*94.13*/("""}"""),format.raw/*94.14*/("""
        """),format.raw/*95.9*/("""}"""),format.raw/*95.10*/("""
    """),format.raw/*96.5*/("""}"""),format.raw/*96.6*/(""");


</script>
<script src=""""),_display_(Seq[Any](/*100.15*/routes/*100.21*/.Assets.at("javascripts/datasets/collections.js"))),format.raw/*100.70*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*101.15*/routes/*101.21*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*101.61*/("""" type="text/javascript"></script>
"""))}
    }
    
    def render(dataset:Dataset,collectionsInside:List[models.Collection],canAddDatasetToCollection:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,collectionsInside,canAddDatasetToCollection)(user)
    
    def f:((Dataset,List[models.Collection],Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,collectionsInside,canAddDatasetToCollection) => (user) => apply(dataset,collectionsInside,canAddDatasetToCollection)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/collections.scala.html
                    HASH: cfdc472ba9122279c3018f987b38cd64ed734a8b
                    MATRIX: 655->1|938->135|966->192|1081->272|1184->354|1356->490|1382->507|1395->511|1449->527|1512->554|1531->564|1556->567|1700->675|1745->711|1784->712|1855->747|1923->793|2037->871|2119->930|2177->951|2221->972|2293->1026|2306->1031|2345->1032|2416->1067|2431->1073|2491->1111|2665->1253|2845->1397|2913->1443|2956->1450|2975->1460|3001->1463|3061->1486|3105->1507|3224->1590|3265->1622|3305->1624|3371->1654|3390->1664|3425->1677|3491->1711|3553->1737|3594->1769|3634->1771|3700->1801|3719->1811|3754->1824|3821->1859|4045->2047|4178->2170|4219->2172|4371->2287|4409->2302|4460->2316|4495->2328|4723->2538|4736->2543|4775->2544|5141->2878|5252->2957|5308->2978|5346->3007|5386->3009|5817->3403|5834->3410|5860->3413|6035->3557|6135->3629|6164->3630|6305->3743|6334->3744|6397->3779|6426->3780|6543->3861|6559->3868|6584->3871|6654->3913|6683->3914|6748->3951|6777->3952|6829->3976|6858->3977|6906->3997|6935->3998|6977->4012|7006->4013|7085->4064|7114->4065|7166->4089|7195->4090|7256->4123|7285->4124|7376->4187|7405->4188|7505->4260|7534->4261|7636->4335|7665->4336|7711->4354|7740->4355|7796->4383|7825->4384|7955->4486|7984->4487|8029->4504|8058->4505|8087->4506|8116->4507|8158->4521|8187->4522|8223->4531|8252->4532|8284->4537|8312->4538|8378->4567|8394->4573|8466->4622|8552->4671|8568->4677|8631->4717
                    LINES: 20->1|26->1|28->5|31->8|31->8|37->14|37->14|37->14|37->14|38->15|38->15|38->15|40->17|40->17|40->17|41->18|41->18|42->19|42->19|42->19|42->19|44->21|44->21|44->21|45->22|45->22|45->22|48->25|52->29|52->29|52->29|52->29|52->29|52->29|52->29|55->32|55->32|55->32|56->33|56->33|56->33|57->34|58->35|58->35|58->35|59->36|59->36|59->36|60->37|62->39|62->39|62->39|63->40|63->40|63->40|63->40|65->42|65->42|65->42|70->47|74->51|78->55|78->55|78->55|84->61|84->61|84->61|90->67|94->71|94->71|98->75|98->75|99->76|99->76|100->77|100->77|100->77|101->78|101->78|102->79|102->79|103->80|103->80|103->80|103->80|104->81|104->81|105->82|105->82|106->83|106->83|106->83|106->83|107->84|107->84|109->86|109->86|111->88|111->88|111->88|111->88|112->89|112->89|115->92|115->92|116->93|116->93|116->93|116->93|117->94|117->94|118->95|118->95|119->96|119->96|123->100|123->100|123->100|124->101|124->101|124->101
                    -- GENERATED --
                */
            