
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listCurationObjects extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[models.CurationObject],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObjects: List[models.CurationObject])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.84*/("""
"""),_display_(Seq[Any](/*2.2*/if(curationObjects.size > 0)/*2.30*/ {_display_(Seq[Any](format.raw/*2.32*/("""
    <div class="row bottom-padding">
        <div class="col-md-12">
            <h4>Related """),_display_(Seq[Any](/*5.26*/Messages("curationobject.label"))),format.raw/*5.58*/("""s</h4>

            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>"""),_display_(Seq[Any](/*11.30*/Messages("owner.label"))),format.raw/*11.53*/("""</th>
                        <th>Created</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                """),_display_(Seq[Any](/*17.18*/for(curObject <- curationObjects) yield /*17.51*/ {_display_(Seq[Any](format.raw/*17.53*/("""
                    <tr class="list-unstyled">

                        <td><a href=""""),_display_(Seq[Any](/*20.39*/routes/*20.45*/.CurationObjects.getCurationObject(curObject.id))),format.raw/*20.93*/("""">
                        """),_display_(Seq[Any](/*21.26*/curObject/*21.35*/.name)),format.raw/*21.40*/("""
                        </a>
                        </td>
                        <td>"""),_display_(Seq[Any](/*24.30*/curObject/*24.39*/.author.fullName)),format.raw/*24.55*/("""</td>
                        <td>"""),_display_(Seq[Any](/*25.30*/curObject/*25.39*/.created.format("MMM dd, yyyy"))),format.raw/*25.70*/("""</td>
                        <td>"""),_display_(Seq[Any](/*26.30*/curObject/*26.39*/.status)),format.raw/*26.46*/("""</td>
                    </tr>
                """)))})),format.raw/*28.18*/("""
                </tbody>
            </table>

        </div>
    </div>
""")))})))}
    }
    
    def render(curationObjects:List[models.CurationObject],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObjects)(user)
    
    def f:((List[models.CurationObject]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObjects) => (user) => apply(curationObjects)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/listCurationObjects.scala.html
                    HASH: 067bd4db1fd1ee7355eb8eeae35b1b9914773ca2
                    MATRIX: 651->1|827->83|863->85|899->113|938->115|1068->210|1121->242|1315->400|1360->423|1575->602|1624->635|1664->637|1787->724|1802->730|1872->778|1936->806|1954->815|1981->820|2106->909|2124->918|2162->934|2233->969|2251->978|2304->1009|2375->1044|2393->1053|2422->1060|2503->1109
                    LINES: 20->1|23->1|24->2|24->2|24->2|27->5|27->5|33->11|33->11|39->17|39->17|39->17|42->20|42->20|42->20|43->21|43->21|43->21|46->24|46->24|46->24|47->25|47->25|47->25|48->26|48->26|48->26|50->28
                    -- GENERATED --
                */
            