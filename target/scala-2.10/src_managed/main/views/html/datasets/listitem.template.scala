
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listitem extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[models.Dataset,Option[String],Map[UUID, Int],Call,Boolean,play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: models.Dataset, collectionId: Option[String], comments: Map[UUID, Int], redirect: Call, selected: Boolean)(implicit  flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.182*/("""
"""),format.raw/*3.1*/("""
<div class = "panel panel-default dataset-panel" id=""""),_display_(Seq[Any](/*4.55*/dataset/*4.62*/.id)),format.raw/*4.65*/("""-listitem">
"""),_display_(Seq[Any](/*5.2*/if(!dataset.thumbnail_id.isEmpty)/*5.35*/{_display_(Seq[Any](format.raw/*5.36*/("""
    <div class="row">
        <div class="pull-left col-xs-12">
            <span class="glyphicon glyphicon-briefcase"></span>
        </div>
    </div>
""")))})),format.raw/*11.2*/("""
    <div class="panel-body">
        """),_display_(Seq[Any](/*13.10*/if(user.isDefined)/*13.28*/ {_display_(Seq[Any](format.raw/*13.30*/("""
            """),_display_(Seq[Any](/*14.14*/if(selected)/*14.26*/ {_display_(Seq[Any](format.raw/*14.28*/("""
                <a href="javascript: void(0)" title="Click to deselect dataset" class="pull-right select" data-id=""""),_display_(Seq[Any](/*15.117*/dataset/*15.124*/.id)),format.raw/*15.127*/("""">
                    <span class="glyphicon glyphicon-ok"></span>
                </a>
            """)))}/*18.15*/else/*18.20*/{_display_(Seq[Any](format.raw/*18.21*/("""
                <a href="javascript: void(0)" title="Click to select dataset" class="pull-right select" data-id=""""),_display_(Seq[Any](/*19.115*/dataset/*19.122*/.id)),format.raw/*19.125*/("""">
                    <span class="glyphicon glyphicon-plus"></span>
                </a>
            """)))})),format.raw/*22.14*/("""
        """)))})),format.raw/*23.10*/("""
        <div class="row">
            <div class="col-xs-2">
                """),_display_(Seq[Any](/*26.18*/if(!dataset.thumbnail_id.isEmpty)/*26.51*/{_display_(Seq[Any](format.raw/*26.52*/("""
                    <img class="fit-in-space" src=""""),_display_(Seq[Any](/*27.53*/(routes.Files.thumbnail(UUID(dataset.thumbnail_id.toString().substring(5,dataset.thumbnail_id.toString().length-1)))))),format.raw/*27.170*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*27.191*/Html(dataset.name))),format.raw/*27.209*/("""">
                """)))}/*28.19*/else/*28.24*/{_display_(Seq[Any](format.raw/*28.25*/("""
                    <a href=""""),_display_(Seq[Any](/*29.31*/routes/*29.37*/.Datasets.dataset(dataset.id))),format.raw/*29.66*/("""">
                        <span class="bigicon glyphicon glyphicon-briefcase"></span>
                    </a>
                """)))})),format.raw/*32.18*/("""
            </div>
            <div class="col-xs-10">
                <div class="row">
                    <div class="col-md-8 col-lg-8 col-sm-8">
                        <div class="row">
                            <div class="col-xs-12">
                                <span class="h2"><a href=""""),_display_(Seq[Any](/*39.60*/(routes.Datasets.dataset(dataset.id)))),format.raw/*39.97*/("""">"""),_display_(Seq[Any](/*39.100*/Html(dataset.name))),format.raw/*39.118*/("""</a></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 abstractsummary">"""),_display_(Seq[Any](/*43.69*/Html(dataset.description.replace("\n","<br>")))),format.raw/*43.115*/("""</div>
                        </div>
                        <div class="row top-padding">
                            <div class="col-xs-12">
                                """),_display_(Seq[Any](/*47.34*/Messages("owner.label"))),format.raw/*47.57*/(""": <a href= """"),_display_(Seq[Any](/*47.70*/routes/*47.76*/.Profile.viewProfileUUID(dataset.author.id))),format.raw/*47.119*/(""""> """),_display_(Seq[Any](/*47.123*/dataset/*47.130*/.author.fullName)),format.raw/*47.146*/(""" </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">Created on """),_display_(Seq[Any](/*51.64*/dataset/*51.71*/.created.format("MMM dd, yyyy"))),format.raw/*51.102*/("""</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4">
                        """),_display_(Seq[Any](/*55.26*/if(user.isDefined)/*55.44*/ {_display_(Seq[Any](format.raw/*55.46*/("""
                            """),_display_(Seq[Any](/*56.30*/if(!dataset.followers.contains(user.get.id))/*56.74*/ {_display_(Seq[Any](format.raw/*56.76*/("""
                                <div>
<!--                                     <a id="followButton"
                                        class="btn btn-link"
                                        objectId=""""),_display_(Seq[Any](/*60.52*/dataset/*60.59*/.id)),format.raw/*60.62*/(""""
                                        objectName=""""),_display_(Seq[Any](/*61.54*/dataset/*61.61*/.name)),format.raw/*61.66*/(""""
                                        objectType="dataset">
                                            <span class='glyphicon glyphicon-star'></span> Follow
                                    </a>
 -->                                </div>
                            """)))}/*66.31*/else/*66.36*/{_display_(Seq[Any](format.raw/*66.37*/("""
                                <div>
<!--                                     <a id="followButton"
                                        class="btn btn-link"
                                        objectId=""""),_display_(Seq[Any](/*70.52*/dataset/*70.59*/.id)),format.raw/*70.62*/(""""
                                        objectName=""""),_display_(Seq[Any](/*71.54*/dataset/*71.61*/.name)),format.raw/*71.66*/(""""
                                        objectType="dataset">
                                            <span class='glyphicon glyphicon-star-empty'></span> Unfollow
                                    </a>
 -->                                </div>
                            """)))})),format.raw/*76.30*/("""
                        """)))})),format.raw/*77.26*/("""
                        """),_display_(Seq[Any](/*78.26*/if(dataset.files.length > 0 && Permission.checkPermission(Permission.DownloadFiles, ResourceRef(ResourceRef.dataset, dataset.id)))/*78.156*/ {_display_(Seq[Any](format.raw/*78.158*/("""
                            <div>
                                <a id='download-dataset-url' href="#"
                                    onclick="window.open(jsRoutes.api.Datasets.download('"""),_display_(Seq[Any](/*81.91*/dataset/*81.98*/.id)),format.raw/*81.101*/("""').url, '_blank');"
                                    class="btn btn-link" title="Download All Files as Zip" role="button">
                                        <span class="glyphicon glyphicon-download-alt"></span>
                                            Download All Files</a>
                            </div>
                        """)))})),format.raw/*86.26*/("""
                        """),_display_(Seq[Any](/*87.26*/if(user.isDefined)/*87.44*/ {_display_(Seq[Any](format.raw/*87.46*/("""
                            """),_display_(Seq[Any](/*88.30*/if(collectionId.isDefined)/*88.56*/ {_display_(Seq[Any](format.raw/*88.58*/("""
                                """),_display_(Seq[Any](/*89.34*/if(user.get.id.equals(dataset.author.id) || Permission.checkPermission(Permission.RemoveResourceFromCollection, ResourceRef(ResourceRef.collection, UUID(collectionId.get))))/*89.207*/{_display_(Seq[Any](format.raw/*89.208*/("""
                                    <button onclick="confirmRemoveResourceFromResource('collection','collection','"""),_display_(Seq[Any](/*90.116*/collectionId/*90.128*/.get)),format.raw/*90.132*/("""','dataset','"""),_display_(Seq[Any](/*90.146*/(dataset.id))),format.raw/*90.158*/("""','"""),_display_(Seq[Any](/*90.162*/(dataset.name.replace("'","&#39;")))),format.raw/*90.197*/("""',true,'"""),_display_(Seq[Any](/*90.206*/redirect/*90.214*/.url)),format.raw/*90.218*/("""')"
                                    class="btn btn-link" title="Remove the dataset from the collection.">
                                        <span class="glyphicon glyphicon-remove"></span> Remove</button>
                                """)))}/*93.35*/else/*93.40*/{_display_(Seq[Any](format.raw/*93.41*/("""
                                    <div class="inline" title="No permission to remove dataset from the collection.">
                                        <button disabled class="btn btn-link"><span class="glyphicon glyphicon-remove"></span> Remove</button>
                                    </div>
                                """)))})),format.raw/*97.34*/("""
                            """)))}/*98.31*/else/*98.36*/{_display_(Seq[Any](format.raw/*98.37*/("""
                                """),_display_(Seq[Any](/*99.34*/if(user.get.id.equals(dataset.author.id) || Permission.checkPermission(Permission.DeleteDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*99.177*/{_display_(Seq[Any](format.raw/*99.178*/("""
                                    <button onclick="confirmDeleteResource('dataset','dataset','"""),_display_(Seq[Any](/*100.98*/(dataset.id))),format.raw/*100.110*/("""','"""),_display_(Seq[Any](/*100.114*/(dataset.name.replace("'","&#39;")))),format.raw/*100.149*/("""',false,'"""),_display_(Seq[Any](/*100.159*/redirect/*100.167*/.url)),format.raw/*100.171*/("""')"
                                    class="btn btn-link" title="Delete the dataset and its contents">
                                        <span class="glyphicon glyphicon-trash"></span> Delete</button>
                                """)))}/*103.35*/else/*103.40*/{_display_(Seq[Any](format.raw/*103.41*/("""
                                    <div class="inline" title="No permission to delete the dataset">
                                        <button disabled class="btn btn-link"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                    </div>
                                """)))})),format.raw/*107.34*/("""
                            """)))})),format.raw/*108.30*/("""
                        """)))})),format.raw/*109.26*/("""
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src=""""),_display_(Seq[Any](/*121.19*/routes/*121.25*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*121.72*/("""" type="text/javascript"></script>
"""))}
    }
    
    def render(dataset:models.Dataset,collectionId:Option[String],comments:Map[UUID, Int],redirect:Call,selected:Boolean,flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,collectionId,comments,redirect,selected)(flash,user)
    
    def f:((models.Dataset,Option[String],Map[UUID, Int],Call,Boolean) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,collectionId,comments,redirect,selected) => (flash,user) => apply(dataset,collectionId,comments,redirect,selected)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/listitem.scala.html
                    HASH: 5e021ca91038db4cda5242a1d13bdfafe7b7db36
                    MATRIX: 689->1|986->181|1013->205|1103->260|1118->267|1142->270|1189->283|1230->316|1268->317|1455->473|1530->512|1557->530|1597->532|1647->546|1668->558|1708->560|1862->677|1879->684|1905->687|2026->790|2039->795|2078->796|2230->911|2247->918|2273->921|2409->1025|2451->1035|2566->1114|2608->1147|2647->1148|2736->1201|2876->1318|2934->1339|2975->1357|3014->1378|3027->1383|3066->1384|3133->1415|3148->1421|3199->1450|3360->1579|3700->1883|3759->1920|3799->1923|3840->1941|4064->2129|4133->2175|4346->2352|4391->2375|4440->2388|4455->2394|4521->2437|4562->2441|4579->2448|4618->2464|4831->2641|4847->2648|4901->2679|5088->2830|5115->2848|5155->2850|5221->2880|5274->2924|5314->2926|5563->3139|5579->3146|5604->3149|5695->3204|5711->3211|5738->3216|6032->3492|6045->3497|6084->3498|6333->3711|6349->3718|6374->3721|6465->3776|6481->3783|6508->3788|6823->4071|6881->4097|6943->4123|7083->4253|7124->4255|7355->4450|7371->4457|7397->4460|7777->4808|7839->4834|7866->4852|7906->4854|7972->4884|8007->4910|8047->4912|8117->4946|8300->5119|8340->5120|8493->5236|8515->5248|8542->5252|8593->5266|8628->5278|8669->5282|8727->5317|8773->5326|8791->5334|8818->5338|9085->5587|9098->5592|9137->5593|9507->5931|9556->5962|9569->5967|9608->5968|9678->6002|9831->6145|9871->6146|10006->6244|10042->6256|10084->6260|10143->6295|10191->6305|10210->6313|10238->6317|10501->6561|10515->6566|10555->6567|10908->6887|10971->6917|11030->6943|11317->7193|11333->7199|11403->7246
                    LINES: 20->1|24->1|25->3|26->4|26->4|26->4|27->5|27->5|27->5|33->11|35->13|35->13|35->13|36->14|36->14|36->14|37->15|37->15|37->15|40->18|40->18|40->18|41->19|41->19|41->19|44->22|45->23|48->26|48->26|48->26|49->27|49->27|49->27|49->27|50->28|50->28|50->28|51->29|51->29|51->29|54->32|61->39|61->39|61->39|61->39|65->43|65->43|69->47|69->47|69->47|69->47|69->47|69->47|69->47|69->47|73->51|73->51|73->51|77->55|77->55|77->55|78->56|78->56|78->56|82->60|82->60|82->60|83->61|83->61|83->61|88->66|88->66|88->66|92->70|92->70|92->70|93->71|93->71|93->71|98->76|99->77|100->78|100->78|100->78|103->81|103->81|103->81|108->86|109->87|109->87|109->87|110->88|110->88|110->88|111->89|111->89|111->89|112->90|112->90|112->90|112->90|112->90|112->90|112->90|112->90|112->90|112->90|115->93|115->93|115->93|119->97|120->98|120->98|120->98|121->99|121->99|121->99|122->100|122->100|122->100|122->100|122->100|122->100|122->100|125->103|125->103|125->103|129->107|130->108|131->109|143->121|143->121|143->121
                    -- GENERATED --
                */
            