
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object filesAndFolders extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template10[Dataset,Option[String],List[Folder],List[Folder],Int,Boolean,List[File],Map[UUID, Int],Option[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset, currentFolder: Option[String], foldersList: List[Folder], folderHierarchy: List[Folder], filepage: Int, next: Boolean, fileList:List[File], fileComments: Map[UUID, Int], space: Option[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.249*/("""
<div><h2 id='folderpath'>
"""),_display_(Seq[Any](/*3.2*/if(folderHierarchy.length > 0 )/*3.33*/ {_display_(Seq[Any](format.raw/*3.35*/("""
     <a href="javascript:updatePageAndFolder(0, '')"><span class="glyphicon glyphicon-home"></span></a>
        """),_display_(Seq[Any](/*5.10*/folderHierarchy/*5.25*/.map/*5.29*/ { cFolder =>_display_(Seq[Any](format.raw/*5.42*/("""
        > <a href="javascript:updatePageAndFolder(0, '"""),_display_(Seq[Any](/*6.56*/cFolder/*6.63*/.id.stringify)),format.raw/*6.76*/("""')"> """),_display_(Seq[Any](/*6.82*/cFolder/*6.89*/.displayName)),format.raw/*6.101*/("""</a>
        """)))})),format.raw/*7.10*/("""
    
""")))})),format.raw/*9.2*/("""
</h2>
"""),_display_(Seq[Any](/*11.2*/if(play.Play.application().configuration().getBoolean("sortInMemory"))/*11.72*/ {_display_(Seq[Any](format.raw/*11.74*/("""
<script type="text/javascript">
$(document).ready(function() """),format.raw/*13.30*/("""{"""),format.raw/*13.31*/("""
  $(".js-sort-single").select2("""),format.raw/*14.32*/("""{"""),format.raw/*14.33*/("""minimumResultsForSearch: Infinity"""),format.raw/*14.66*/("""}"""),format.raw/*14.67*/(""");
  var order = 'dateN';
  if($.cookie('sort-order') != null) """),format.raw/*16.38*/("""{"""),format.raw/*16.39*/("""
    order = $.cookie('sort-order').replace(/['"]+/g, '');
  """),format.raw/*18.3*/("""}"""),format.raw/*18.4*/("""
  $(".js-sort-single").val(order).trigger("change");
  $(".js-sort-single").on('select2:select', function (evt) """),format.raw/*20.60*/("""{"""),format.raw/*20.61*/("""   
  $.cookie('sort-order', $(".js-sort-single").val(), """),format.raw/*21.54*/("""{"""),format.raw/*21.55*/(""" path: '/' """),format.raw/*21.66*/("""}"""),format.raw/*21.67*/("""); 
  """),_display_(Seq[Any](/*22.4*/currentFolder/*22.17*/ match/*22.23*/ {/*23.9*/case None =>/*23.21*/ {_display_(Seq[Any](format.raw/*23.23*/(""" updatePageAndFolder("""),_display_(Seq[Any](/*23.45*/filepage)),format.raw/*23.53*/(""",'', $(".js-sort-single").val()); """)))}/*24.9*/case Some(s) =>/*24.24*/ {_display_(Seq[Any](format.raw/*24.26*/(""" updatePageAndFolder("""),_display_(Seq[Any](/*24.48*/filepage)),format.raw/*24.56*/(""",'"""),_display_(Seq[Any](/*24.59*/s)),format.raw/*24.60*/("""', $(".js-sort-single").val());""")))}})),format.raw/*25.6*/("""
	"""),format.raw/*26.2*/("""}"""),format.raw/*26.3*/(""");
"""),format.raw/*27.1*/("""}"""),format.raw/*27.2*/(""");
</script>
<label class="sortchoice" for="js-sort-single">Sort By:
<select class="js-sort-single">
  <option value="dateN">Newest</option>
  <option value="dateO">Oldest</option>
  <option value="titleA">Title (A-Z)</option>
  <option value="titleZ">Title (Z-A)</option>
  <option value="sizeL">Size (L)</option>
  <option value="sizeS">Size (S)</option>
  
</select>
</label>
""")))})),format.raw/*40.2*/("""
</div>

<div id="folderListDiv">
"""),_display_(Seq[Any](/*44.2*/foldersList/*44.13*/.map/*44.17*/ { folder =>_display_(Seq[Any](format.raw/*44.29*/("""
    """),_display_(Seq[Any](/*45.6*/folders/*45.13*/.listitem(folder, dataset.id))),format.raw/*45.42*/("""
""")))})),format.raw/*46.2*/("""
</div>
    <div style="display: none" id="empty-folder-div"><h4>Folder is empty</h4></div>

    <!-- Show message when the folder is empty -->
    """),_display_(Seq[Any](/*51.6*/if(foldersList.size == 0 && fileList.size == 0 )/*51.54*/{_display_(Seq[Any](format.raw/*51.55*/("""
        <script>$("#empty-folder-div").show();</script>
    """)))})),format.raw/*53.6*/("""

    """),_display_(Seq[Any](/*55.6*/currentFolder/*55.19*/ match/*55.25*/ {/*56.9*/case None =>/*56.21*/ {_display_(Seq[Any](format.raw/*56.23*/(""" """),_display_(Seq[Any](/*56.25*/files/*56.30*/.grid(fileList, fileComments, dataset.id, space, ResourceRef(ResourceRef.dataset, dataset.id), dataset.folders.length > 0))),format.raw/*56.152*/(""" """)))}/*57.9*/case Some(s) =>/*57.24*/ {_display_(Seq[Any](format.raw/*57.26*/(""" """),_display_(Seq[Any](/*57.28*/files/*57.33*/.grid(fileList, fileComments, dataset.id, space, ResourceRef(ResourceRef.folder, UUID(s)), dataset.folders.length > 0)))))}})),format.raw/*58.6*/("""

<div class="row">
    <div class="col-md-12">
        <ul class="pager">
                <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
            """),_display_(Seq[Any](/*64.14*/if(filepage > 0)/*64.30*/ {_display_(Seq[Any](format.raw/*64.32*/("""
                <li class="previous"><a id="prevlink" title="Page backwards" href="javascript:updatePageAndFolder("""),_display_(Seq[Any](/*65.116*/(filepage-1))),format.raw/*65.128*/(""", '"""),_display_(Seq[Any](/*65.132*/currentFolder)),format.raw/*65.145*/("""')"><span class="glyphicon glyphicon-chevron-left"></span>Previous</a></li>
            """)))})),format.raw/*66.14*/("""
            """),_display_(Seq[Any](/*67.14*/if(next)/*67.22*/ {_display_(Seq[Any](format.raw/*67.24*/("""
                <li class ="next"><a id="nextlink" title="Page forwards"  href="javascript:updatePageAndFolder("""),_display_(Seq[Any](/*68.113*/(filepage+1))),format.raw/*68.125*/(""",'"""),_display_(Seq[Any](/*68.128*/currentFolder)),format.raw/*68.141*/("""')">Next<span class="glyphicon glyphicon-chevron-right"></span></a></li>
            """)))})),format.raw/*69.14*/("""
        </ul>
    </div>
</div>
<script src=""""),_display_(Seq[Any](/*73.15*/routes/*73.21*/.Assets.at("javascripts/follow-button.js"))),format.raw/*73.63*/("""" type="text/javascript"></script>"""))}
    }
    
    def render(dataset:Dataset,currentFolder:Option[String],foldersList:List[Folder],folderHierarchy:List[Folder],filepage:Int,next:Boolean,fileList:List[File],fileComments:Map[UUID, Int],space:Option[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,currentFolder,foldersList,folderHierarchy,filepage,next,fileList,fileComments,space)(user)
    
    def f:((Dataset,Option[String],List[Folder],List[Folder],Int,Boolean,List[File],Map[UUID, Int],Option[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,currentFolder,foldersList,folderHierarchy,filepage,next,fileList,fileComments,space) => (user) => apply(dataset,currentFolder,foldersList,folderHierarchy,filepage,next,fileList,fileComments,space)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/filesAndFolders.scala.html
                    HASH: ca08469b45ff49b479378aa1e1d07322e3982364
                    MATRIX: 722->1|1064->248|1126->276|1165->307|1204->309|1353->423|1376->438|1388->442|1438->455|1529->511|1544->518|1578->531|1619->537|1634->544|1668->556|1713->570|1750->577|1793->585|1872->655|1912->657|2002->719|2031->720|2091->752|2120->753|2181->786|2210->787|2301->850|2330->851|2418->912|2446->913|2587->1026|2616->1027|2701->1084|2730->1085|2769->1096|2798->1097|2840->1104|2862->1117|2877->1123|2887->1134|2908->1146|2948->1148|3006->1170|3036->1178|3089->1222|3113->1237|3153->1239|3211->1261|3241->1269|3280->1272|3303->1273|3367->1311|3396->1313|3424->1314|3454->1317|3482->1318|3893->1698|3963->1733|3983->1744|3996->1748|4046->1760|4087->1766|4103->1773|4154->1802|4187->1804|4371->1953|4428->2001|4467->2002|4560->2064|4602->2071|4624->2084|4639->2090|4649->2101|4670->2113|4710->2115|4748->2117|4762->2122|4907->2244|4927->2255|4951->2270|4991->2272|5029->2274|5043->2279|5188->2404|5450->2630|5475->2646|5515->2648|5668->2764|5703->2776|5744->2780|5780->2793|5901->2882|5951->2896|5968->2904|6008->2906|6158->3019|6193->3031|6233->3034|6269->3047|6387->3133|6470->3180|6485->3186|6549->3228
                    LINES: 20->1|23->1|25->3|25->3|25->3|27->5|27->5|27->5|27->5|28->6|28->6|28->6|28->6|28->6|28->6|29->7|31->9|33->11|33->11|33->11|35->13|35->13|36->14|36->14|36->14|36->14|38->16|38->16|40->18|40->18|42->20|42->20|43->21|43->21|43->21|43->21|44->22|44->22|44->22|44->23|44->23|44->23|44->23|44->23|44->24|44->24|44->24|44->24|44->24|44->24|44->24|44->25|45->26|45->26|46->27|46->27|59->40|63->44|63->44|63->44|63->44|64->45|64->45|64->45|65->46|70->51|70->51|70->51|72->53|74->55|74->55|74->55|74->56|74->56|74->56|74->56|74->56|74->56|74->57|74->57|74->57|74->57|74->57|74->58|80->64|80->64|80->64|81->65|81->65|81->65|81->65|82->66|83->67|83->67|83->67|84->68|84->68|84->68|84->68|85->69|89->73|89->73|89->73
                    -- GENERATED --
                */
            