
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object editLicense extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Dataset,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.56*/("""

<div id="collapseSix" class="accordion-body collapse">
    <div class="panel panel-primary">
        <div class="panel-body">
        """),_display_(Seq[Any](/*6.10*/if(user.isDefined)/*6.28*/ {_display_(Seq[Any](format.raw/*6.30*/("""
            """),_display_(Seq[Any](/*7.14*/licenseform(dataset.id.stringify, dataset.licenseData, "dataset", dataset.author.fullName))),format.raw/*7.104*/("""
        """)))})),format.raw/*8.10*/("""
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
    $(document).ready(function() """),format.raw/*13.34*/("""{"""),format.raw/*13.35*/("""
        //Will have to modify the if check to see if there is data that specifies what should be selected
        //Incoming data may specifiy the type of license, the name of the owner of the rights, the text
        //describing the license rights, the URL for the license, and whether or not downloading is
        //allowed.
        var datasetLicenseType = """"),_display_(Seq[Any](/*18.36*/dataset/*18.43*/.licenseData.m_licenseType)),format.raw/*18.69*/("""";
        var datasetRightsHolder = """"),_display_(Seq[Any](/*19.37*/dataset/*19.44*/.licenseData.m_rightsHolder)),format.raw/*19.71*/("""";
        var datasetLicenseText = """"),_display_(Seq[Any](/*20.36*/dataset/*20.43*/.licenseData.m_licenseText)),format.raw/*20.69*/("""";
        var datasetLicenseUrl = """"),_display_(Seq[Any](/*21.35*/dataset/*21.42*/.licenseData.m_licenseUrl)),format.raw/*21.67*/("""";
        var datasetAllowDownload = """"),_display_(Seq[Any](/*22.38*/dataset/*22.45*/.licenseData.isDownloadAllowed(user))),format.raw/*22.81*/("""";
        var datasetImageBase = '"""),_display_(Seq[Any](/*23.34*/routes/*23.40*/.Assets.at("images"))),format.raw/*23.60*/("""';
        var datasetAuthorName = '"""),_display_(Seq[Any](/*24.35*/dataset/*24.42*/.author.fullName)),format.raw/*24.58*/("""';

        if (!"""),_display_(Seq[Any](/*26.15*/user/*26.19*/.isDefined)),format.raw/*26.29*/(""") """),format.raw/*26.31*/("""{"""),format.raw/*26.32*/("""
            updateInterface(datasetLicenseType, datasetRightsHolder, datasetLicenseText, datasetLicenseUrl, datasetAllowDownload,
            datasetImageBase, datasetAuthorName);
        """),format.raw/*29.9*/("""}"""),format.raw/*29.10*/("""
    """),format.raw/*30.5*/("""}"""),format.raw/*30.6*/(""");
</script>
<!-- End License elements -->
"""))}
    }
    
    def render(dataset:Dataset,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset)(user)
    
    def f:((Dataset) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset) => (user) => apply(dataset)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/editLicense.scala.html
                    HASH: 4f3d87ed0988f569960ffe81cd903ae461e7b311
                    MATRIX: 623->1|771->55|943->192|969->210|1008->212|1057->226|1169->316|1210->326|1359->447|1388->448|1789->813|1805->820|1853->846|1928->885|1944->892|1993->919|2067->957|2083->964|2131->990|2204->1027|2220->1034|2267->1059|2343->1099|2359->1106|2417->1142|2489->1178|2504->1184|2546->1204|2619->1241|2635->1248|2673->1264|2727->1282|2740->1286|2772->1296|2802->1298|2831->1299|3047->1488|3076->1489|3108->1494|3136->1495
                    LINES: 20->1|23->1|28->6|28->6|28->6|29->7|29->7|30->8|35->13|35->13|40->18|40->18|40->18|41->19|41->19|41->19|42->20|42->20|42->20|43->21|43->21|43->21|44->22|44->22|44->22|45->23|45->23|45->23|46->24|46->24|46->24|48->26|48->26|48->26|48->26|48->26|51->29|51->29|52->30|52->30
                    -- GENERATED --
                */
            