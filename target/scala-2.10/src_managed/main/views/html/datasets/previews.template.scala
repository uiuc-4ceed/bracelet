
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object previews extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Dataset,List[Previewer],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset, previewers: List[Previewer]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.Play

def /*3.2*/showPreview/*3.13*/(name: String, path: String, url: String, datasetId: String):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*3.77*/("""
    <script>
        var Configuration = """),format.raw/*5.29*/("""{"""),format.raw/*5.30*/("""}"""),format.raw/*5.31*/(""";
        Configuration.div = "#previewer_"""),_display_(Seq[Any](/*6.42*/name)),format.raw/*6.46*/("""";
        Configuration.path = """"),_display_(Seq[Any](/*7.32*/path)),format.raw/*7.36*/("""";
        Configuration.url = """"),_display_(Seq[Any](/*8.31*/url)),format.raw/*8.34*/("""";
        Configuration.dataset_id = """"),_display_(Seq[Any](/*9.38*/datasetId)),format.raw/*9.47*/("""";
        Configuration.name = """"),_display_(Seq[Any](/*10.32*/name)),format.raw/*10.36*/("""";
    </script>
    <script type="text/javascript" src=""""),_display_(Seq[Any](/*12.42*/(url))),format.raw/*12.47*/(""""></script>
""")))};
Seq[Any](format.raw/*1.49*/("""

"""),format.raw/*13.2*/("""

"""),format.raw/*15.44*/("""
"""),_display_(Seq[Any](/*17.2*/if(Play.current.configuration.getString("google.maps.key").isDefined)/*17.71*/ {_display_(Seq[Any](format.raw/*17.73*/("""
    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key="""),_display_(Seq[Any](/*19.59*/Play/*19.63*/.current.configuration.getString("google.maps.key"))),format.raw/*19.114*/("""">
    </script>
""")))})),format.raw/*21.2*/("""

"""),_display_(Seq[Any](/*23.2*/for((p,i) <- previewers.zipWithIndex) yield /*23.39*/ {_display_(Seq[Any](format.raw/*23.41*/("""
    <div class="col-md-12 dataset-preview" id="previewer_"""),_display_(Seq[Any](/*24.59*/(p.id))),format.raw/*24.65*/("""_"""),_display_(Seq[Any](/*24.67*/i)),format.raw/*24.68*/(""""></div>
    """),_display_(Seq[Any](/*25.6*/showPreview(p.id + "_" + i,
        routes.Assets.at("javascripts/previewers") + "/" + p.id + "/",
        routes.Assets.at("javascripts/previewers") + "/" + p.id + "/" + p.main,
        dataset.id.stringify))),format.raw/*28.30*/("""
""")))})),format.raw/*29.2*/("""
"""))}
    }
    
    def render(dataset:Dataset,previewers:List[Previewer]): play.api.templates.HtmlFormat.Appendable = apply(dataset,previewers)
    
    def f:((Dataset,List[Previewer]) => play.api.templates.HtmlFormat.Appendable) = (dataset,previewers) => apply(dataset,previewers)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/previews.scala.html
                    HASH: d8fc2e89570ea4b20898215de0e664aaf3b22c0d
                    MATRIX: 616->1|761->51|780->62|924->126|993->168|1021->169|1049->170|1127->213|1152->217|1221->251|1246->255|1314->288|1338->291|1413->331|1443->340|1513->374|1539->378|1633->436|1660->441|1712->48|1741->454|1771->499|1808->523|1886->592|1926->594|2056->688|2069->692|2143->743|2192->761|2230->764|2283->801|2323->803|2418->862|2446->868|2484->870|2507->871|2556->885|2786->1093|2819->1095
                    LINES: 20->1|23->3|23->3|25->3|27->5|27->5|27->5|28->6|28->6|29->7|29->7|30->8|30->8|31->9|31->9|32->10|32->10|34->12|34->12|36->1|38->13|40->15|41->17|41->17|41->17|43->19|43->19|43->19|45->21|47->23|47->23|47->23|48->24|48->24|48->24|48->24|49->25|52->28|53->29
                    -- GENERATED --
                */
            