
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object addFiles extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[Dataset,Option[Folder],List[ProjectSpace],List[Folder],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset, folder: Option[Folder], spaces: List[ProjectSpace], folderHierarchy: List[Folder])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters._


Seq[Any](format.raw/*1.139*/("""

"""),_display_(Seq[Any](/*4.2*/main("Add Files")/*4.19*/ {_display_(Seq[Any](format.raw/*4.21*/("""

    <!-- Custom items for the create dataset workflow -->
    <script src=""""),_display_(Seq[Any](/*7.19*/routes/*7.25*/.Assets.at("javascripts/dataset-attach-fileuploader.js"))),format.raw/*7.81*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*8.19*/routes/*8.25*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*8.67*/("""" type="text/javascript"></script>

    <div class="row">
        <ol class="breadcrumb navigate-bread">
        """),_display_(Seq[Any](/*12.10*/if(spaces.length == 1)/*12.32*/ {_display_(Seq[Any](format.raw/*12.34*/("""
            <li><span class="glyphicon glyphicon-tent"></span> <a href=""""),_display_(Seq[Any](/*13.74*/routes/*13.80*/.Spaces.getSpace(spaces.head.id))),format.raw/*13.112*/("""" title=""""),_display_(Seq[Any](/*13.122*/spaces/*13.128*/.head.name)),format.raw/*13.138*/(""""> """),_display_(Seq[Any](/*13.142*/Html(ellipsize(spaces.head.name, 18)))),format.raw/*13.179*/("""</a></li>
        """)))}/*14.11*/else/*14.16*/{_display_(Seq[Any](format.raw/*14.17*/("""
            """),_display_(Seq[Any](/*15.14*/if(spaces.length > 1)/*15.35*/ {_display_(Seq[Any](format.raw/*15.37*/("""
                <li>
                    <span class="dropdown">
                        <button class="btn-link dropdown-toggle" type="button" id="dropdown_space_list" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-tent"></span> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" arialanelledby="dropdown_space_list">
                        """),_display_(Seq[Any](/*23.26*/spaces/*23.32*/.map/*23.36*/ { s =>_display_(Seq[Any](format.raw/*23.43*/("""
                            <li><a href=""""),_display_(Seq[Any](/*24.43*/routes/*24.49*/.Spaces.getSpace(s.id))),format.raw/*24.71*/("""" title="s.name"><span class="glyphicon glyphicon-tent"></span> """),_display_(Seq[Any](/*24.136*/Html(ellipsize(s.name, 18)))),format.raw/*24.163*/("""</a></li>
                        """)))})),format.raw/*25.26*/("""
                        </ul>
                    </span>
                </li>
            """)))}/*29.15*/else/*29.20*/{_display_(Seq[Any](format.raw/*29.21*/("""
                <li><span class="glyphicon glyphicon-user"></span> <a href = """"),_display_(Seq[Any](/*30.80*/routes/*30.86*/.Profile.viewProfileUUID(dataset.author.id))),format.raw/*30.129*/(""""> """),_display_(Seq[Any](/*30.133*/dataset/*30.140*/.author.fullName)),format.raw/*30.156*/("""</a></li>
            """)))})),format.raw/*31.14*/("""
        """)))})),format.raw/*32.10*/("""
            <li><span class="glyphicon glyphicon-briefcase"></span> <a href=""""),_display_(Seq[Any](/*33.79*/routes/*33.85*/.Datasets.dataset(dataset.id))),format.raw/*33.114*/(""""> <span title=""""),_display_(Seq[Any](/*33.131*/dataset/*33.138*/.name)),format.raw/*33.143*/(""""> """),_display_(Seq[Any](/*33.147*/Html(ellipsize(dataset.name, 18)))),format.raw/*33.180*/("""</span> </a> </li>
            """),_display_(Seq[Any](/*34.14*/folderHierarchy/*34.29*/.map/*34.33*/ { folder =>_display_(Seq[Any](format.raw/*34.45*/("""
              <li><span class="glyphicon glyphicon-folder-close"></span> <a href=""""),_display_(Seq[Any](/*35.84*/routes/*35.90*/.Datasets.dataset(dataset.id))),format.raw/*35.119*/("""#folderId="""),_display_(Seq[Any](/*35.130*/folder/*35.136*/.id)),format.raw/*35.139*/(""""> """),_display_(Seq[Any](/*35.143*/folder/*35.149*/.displayName)),format.raw/*35.161*/("""</a></li>

            """)))})),format.raw/*37.14*/("""
            <li> <span class="glyphicon glyphicon-upload"></span> Add Files to
                """),_display_(Seq[Any](/*39.18*/folder/*39.24*/ match/*39.30*/ {/*40.21*/case Some(f) =>/*40.36*/ {_display_(Seq[Any](format.raw/*40.38*/("""Folder""")))}/*41.21*/case None =>/*41.33*/  {_display_(Seq[Any](format.raw/*41.36*/("""Dataset""")))}})),format.raw/*42.14*/("""
            </li>
        </ol>
    </div>
    <div class="page-header header">
        <h1>

            <a href="
            """),_display_(Seq[Any](/*50.14*/folder/*50.20*/ match/*50.26*/ {/*51.17*/case Some(f) =>/*51.32*/ {_display_(Seq[Any](_display_(Seq[Any](/*51.35*/routes/*51.41*/.Datasets.dataset(dataset.id))),format.raw/*51.70*/("""#folderId="""),_display_(Seq[Any](/*51.81*/f/*51.82*/.id))))}/*52.17*/case None =>/*52.29*/ {_display_(Seq[Any](_display_(Seq[Any](/*52.32*/routes/*52.38*/.Datasets.dataset(dataset.id)))))}})),format.raw/*53.14*/("""
            " class="back-button btn btn-link" title="Navigate back to dataset">
                <span class="glyphicon glyphicon-chevron-left" style="font-size: 150%;"></span>
            </a>
            Add Files to
            """),_display_(Seq[Any](/*58.14*/folder/*58.20*/ match/*58.26*/ {/*59.17*/case Some(f) =>/*59.32*/ {_display_(Seq[Any](format.raw/*59.34*/("""Folder""")))}/*60.17*/case None =>/*60.29*/  {_display_(Seq[Any](format.raw/*60.32*/("""Dataset""")))}})),format.raw/*61.14*/("""
        </h1>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="list-unstyled">
                """),_display_(Seq[Any](/*67.18*/folder/*67.24*/ match/*67.30*/ {/*68.21*/case Some(f) =>/*68.36*/ {_display_(Seq[Any](format.raw/*68.38*/("""
                        <li> Folder <a href=""""),_display_(Seq[Any](/*69.47*/routes/*69.53*/.Datasets.dataset(dataset.id))),format.raw/*69.82*/("""#folderId="""),_display_(Seq[Any](/*69.93*/f/*69.94*/.id)),format.raw/*69.97*/("""">"""),_display_(Seq[Any](/*69.100*/f/*69.101*/.displayName)),format.raw/*69.113*/("""</a></li>
                        <li>Parent Dataset <a href=""""),_display_(Seq[Any](/*70.54*/routes/*70.60*/.Datasets.dataset(dataset.id))),format.raw/*70.89*/("""">"""),_display_(Seq[Any](/*70.92*/dataset/*70.99*/.name)),format.raw/*70.104*/("""</a></li>
                    """)))}/*73.21*/case None =>/*73.33*/ {_display_(Seq[Any](format.raw/*73.35*/("""<li>Dataset <a href=""""),_display_(Seq[Any](/*73.57*/routes/*73.63*/.Datasets.dataset(dataset.id))),format.raw/*73.92*/("""">"""),_display_(Seq[Any](/*73.95*/dataset/*73.102*/.name)),format.raw/*73.107*/("""</a></li>""")))}})),format.raw/*74.18*/("""

                <li>"""),_display_(Seq[Any](/*76.22*/Messages("owner.label"))),format.raw/*76.45*/(""": <a href= """"),_display_(Seq[Any](/*76.58*/routes/*76.64*/.Profile.viewProfileUUID(dataset.author.id))),format.raw/*76.107*/(""""> """),_display_(Seq[Any](/*76.111*/dataset/*76.118*/.author.fullName)),format.raw/*76.134*/(""" </a></li>
                <li>Created on """),_display_(Seq[Any](/*77.33*/dataset/*77.40*/.created.format("MMM dd, yyyy"))),format.raw/*77.71*/("""</li>
            </ul>
        </div>
    </div>
    """),_display_(Seq[Any](/*81.6*/folder/*81.12*/ match/*81.18*/ {/*82.9*/case Some(f) =>/*82.24*/ {_display_(Seq[Any](_display_(Seq[Any](/*82.27*/datasets/*82.35*/.uploadFiles(dataset, Some(f.id.stringify)))),format.raw/*82.78*/(""" """)))}/*83.9*/case None =>/*83.21*/ {_display_(Seq[Any](_display_(Seq[Any](/*83.24*/datasets/*83.32*/.uploadFiles(dataset, None))),format.raw/*83.59*/(""" """)))}})),format.raw/*84.6*/("""

""")))})))}
    }
    
    def render(dataset:Dataset,folder:Option[Folder],spaces:List[ProjectSpace],folderHierarchy:List[Folder],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,folder,spaces,folderHierarchy)(user)
    
    def f:((Dataset,Option[Folder],List[ProjectSpace],List[Folder]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,folder,spaces,folderHierarchy) => (user) => apply(dataset,folder,spaces,folderHierarchy)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:49 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/addFiles.scala.html
                    HASH: b25e6614888c4424d988c89d265c33928821a9fa
                    MATRIX: 667->1|931->138|968->174|993->191|1032->193|1145->271|1159->277|1236->333|1324->386|1338->392|1401->434|1551->548|1582->570|1622->572|1732->646|1747->652|1802->684|1849->694|1865->700|1898->710|1939->714|1999->751|2037->771|2050->776|2089->777|2139->791|2169->812|2209->814|2755->1324|2770->1330|2783->1334|2828->1341|2907->1384|2922->1390|2966->1412|3068->1477|3118->1504|3185->1539|3298->1634|3311->1639|3350->1640|3466->1720|3481->1726|3547->1769|3588->1773|3605->1780|3644->1796|3699->1819|3741->1829|3856->1908|3871->1914|3923->1943|3977->1960|3994->1967|4022->1972|4063->1976|4119->2009|4187->2041|4211->2056|4224->2060|4274->2072|4394->2156|4409->2162|4461->2191|4509->2202|4525->2208|4551->2211|4592->2215|4608->2221|4643->2233|4699->2257|4832->2354|4847->2360|4862->2366|4873->2389|4897->2404|4937->2406|4963->2434|4984->2446|5025->2449|5066->2471|5232->2601|5247->2607|5262->2613|5273->2632|5297->2647|5346->2650|5361->2656|5412->2685|5459->2696|5469->2697|5486->2718|5507->2730|5556->2733|5571->2739|5628->2783|5897->3016|5912->3022|5927->3028|5938->3047|5962->3062|6002->3064|6028->3088|6049->3100|6090->3103|6131->3125|6303->3261|6318->3267|6333->3273|6344->3296|6368->3311|6408->3313|6491->3360|6506->3366|6557->3395|6604->3406|6614->3407|6639->3410|6679->3413|6690->3414|6725->3426|6824->3489|6839->3495|6890->3524|6929->3527|6945->3534|6973->3539|7023->3592|7044->3604|7084->3606|7142->3628|7157->3634|7208->3663|7247->3666|7264->3673|7292->3678|7335->3706|7394->3729|7439->3752|7488->3765|7503->3771|7569->3814|7610->3818|7627->3825|7666->3841|7745->3884|7761->3891|7814->3922|7904->3977|7919->3983|7934->3989|7944->4000|7968->4015|8017->4018|8034->4026|8099->4069|8119->4080|8140->4092|8189->4095|8206->4103|8255->4130|8289->4138
                    LINES: 20->1|24->1|26->4|26->4|26->4|29->7|29->7|29->7|30->8|30->8|30->8|34->12|34->12|34->12|35->13|35->13|35->13|35->13|35->13|35->13|35->13|35->13|36->14|36->14|36->14|37->15|37->15|37->15|45->23|45->23|45->23|45->23|46->24|46->24|46->24|46->24|46->24|47->25|51->29|51->29|51->29|52->30|52->30|52->30|52->30|52->30|52->30|53->31|54->32|55->33|55->33|55->33|55->33|55->33|55->33|55->33|55->33|56->34|56->34|56->34|56->34|57->35|57->35|57->35|57->35|57->35|57->35|57->35|57->35|57->35|59->37|61->39|61->39|61->39|61->40|61->40|61->40|61->41|61->41|61->41|61->42|69->50|69->50|69->50|69->51|69->51|69->51|69->51|69->51|69->51|69->51|69->52|69->52|69->52|69->52|69->53|74->58|74->58|74->58|74->59|74->59|74->59|74->60|74->60|74->60|74->61|80->67|80->67|80->67|80->68|80->68|80->68|81->69|81->69|81->69|81->69|81->69|81->69|81->69|81->69|81->69|82->70|82->70|82->70|82->70|82->70|82->70|83->73|83->73|83->73|83->73|83->73|83->73|83->73|83->73|83->73|83->74|85->76|85->76|85->76|85->76|85->76|85->76|85->76|85->76|86->77|86->77|86->77|90->81|90->81|90->81|90->82|90->82|90->82|90->82|90->82|90->83|90->83|90->83|90->83|90->83|90->84
                    -- GENERATED --
                */
            