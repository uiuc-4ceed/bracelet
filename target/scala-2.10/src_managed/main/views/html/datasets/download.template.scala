
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object download extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Dataset,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.56*/("""

"""),_display_(Seq[Any](/*3.2*/if(user.isDefined)/*3.20*/ {_display_(Seq[Any](format.raw/*3.22*/("""
    """),_display_(Seq[Any](/*4.6*/if(dataset.files.length > 0)/*4.34*/{_display_(Seq[Any](format.raw/*4.35*/("""
        <div class="btn-group btn-group-xs">
            <button id="downloadDatasetButton" onclick="window.open(jsRoutes.api.Datasets.download('"""),_display_(Seq[Any](/*6.102*/(dataset.id))),format.raw/*6.114*/("""').url, '_blank')"
            type="button" title="Download all files in this dataset as a zip file" class="btn btn-default">
                <span class="glyphicon glyphicon-download-alt"></span>
                Download All Files
            </button>
        </div>
    """)))})),format.raw/*12.6*/("""

""")))})),format.raw/*14.2*/("""
"""))}
    }
    
    def render(dataset:Dataset,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset)(user)
    
    def f:((Dataset) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset) => (user) => apply(dataset)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/download.scala.html
                    HASH: 5f2a45c5da7b3101907fc0e6b065c0db80e79cd8
                    MATRIX: 620->1|768->55|805->58|831->76|870->78|910->84|946->112|984->113|1167->260|1201->272|1507->547|1541->550
                    LINES: 20->1|23->1|25->3|25->3|25->3|26->4|26->4|26->4|28->6|28->6|34->12|36->14
                    -- GENERATED --
                */
            