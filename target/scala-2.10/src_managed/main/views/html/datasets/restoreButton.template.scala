
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object restoreButton extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Dataset,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.56*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/if(user.isDefined)/*5.20*/ {_display_(Seq[Any](format.raw/*5.22*/("""
    <!-- If the user can edit the dataset, the elements are enabled, otherwise they are present but disabled to provide consistent UE. -->
    """),_display_(Seq[Any](/*7.6*/if(Permission.checkPermission(Permission.DeleteDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*7.108*/ {_display_(Seq[Any](format.raw/*7.110*/("""
        <a id="restoreButton" onclick="restoreDataset('"""),_display_(Seq[Any](/*8.57*/(dataset.id))),format.raw/*8.69*/("""', true, '"""),_display_(Seq[Any](/*8.80*/(routes.Datasets.list("",owner=Some(user.get.id.stringify),showTrash=true)))),format.raw/*8.155*/("""')"
            class="btn btn-link" href="#"><span class="glyphicon glyphicon-exclamation-sign"></span> Restore</a>
    """)))}/*10.7*/else/*10.12*/{_display_(Seq[Any](format.raw/*10.13*/("""
        <a id="restoreButton" class="btn btn-link disabled" href="#"><span class="glyphicon glyphicon-exclamation-sign"></span> Restore</a>
    """)))})),format.raw/*12.6*/("""
""")))})),format.raw/*13.2*/("""
"""))}
    }
    
    def render(dataset:Dataset,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset)(user)
    
    def f:((Dataset) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset) => (user) => apply(dataset)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/restoreButton.scala.html
                    HASH: e72c30e3661b680611ab2090b8306cb9421ee1af
                    MATRIX: 625->1|795->55|823->80|859->82|885->100|924->102|1103->247|1214->349|1254->351|1346->408|1379->420|1425->431|1522->506|1662->629|1675->634|1714->635|1891->781|1924->783
                    LINES: 20->1|24->1|26->4|27->5|27->5|27->5|29->7|29->7|29->7|30->8|30->8|30->8|30->8|32->10|32->10|32->10|34->12|35->13
                    -- GENERATED --
                */
            