
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template9[models.Dataset,Option[String],Map[UUID, Int],String,Boolean,Boolean,Call,Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: models.Dataset, space: Option[String], comments: Map[UUID, Int],classes:String, isFollowing: Boolean, isSpace: Boolean, redirect: Call, selected: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.i18n.Messages


Seq[Any](format.raw/*1.202*/("""
"""),format.raw/*4.1*/("""<div class="post-box """),_display_(Seq[Any](/*4.23*/classes)),format.raw/*4.30*/("""" id=""""),_display_(Seq[Any](/*4.37*/dataset/*4.44*/.id)),format.raw/*4.47*/("""-tile">
    <div class="panel panel-default dataset-panel">
        """),_display_(Seq[Any](/*6.10*/if(user.isDefined)/*6.28*/ {_display_(Seq[Any](format.raw/*6.30*/("""
            """),_display_(Seq[Any](/*7.14*/if(selected)/*7.26*/ {_display_(Seq[Any](format.raw/*7.28*/("""
                <a href="javascript: void(0)" title="Click to deselect dataset" class="pull-right select" data-id=""""),_display_(Seq[Any](/*8.117*/dataset/*8.124*/.id)),format.raw/*8.127*/("""">
                    <span class="glyphicon glyphicon-ok"></span>
                </a>
            """)))}/*11.15*/else/*11.20*/{_display_(Seq[Any](format.raw/*11.21*/("""
                <a href="javascript: void(0)" title="Click to select dataset" class="pull-right select" data-id=""""),_display_(Seq[Any](/*12.115*/dataset/*12.122*/.id)),format.raw/*12.125*/("""">
                    <span class="glyphicon glyphicon-plus"></span>
                </a>
            """)))})),format.raw/*15.14*/("""
        """)))})),format.raw/*16.10*/("""
        <div class="panel-body">
            """),_display_(Seq[Any](/*18.14*/if(!dataset.thumbnail_id.isEmpty)/*18.47*/{_display_(Seq[Any](format.raw/*18.48*/("""
                <a href=""""),_display_(Seq[Any](/*19.27*/(routes.Datasets.dataset(dataset.id,space)))),format.raw/*19.70*/("""">
                    <img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*20.68*/(routes.Files.thumbnail(UUID(dataset.thumbnail_id.toString().substring(5,dataset.thumbnail_id.toString().length-1)))))),format.raw/*20.185*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*20.206*/Html(dataset.name))),format.raw/*20.224*/("""">
                </a>
            """)))})),format.raw/*22.14*/("""
            <div class="caption break-word">
                <h4><a href=""""),_display_(Seq[Any](/*24.31*/(routes.Datasets.dataset(dataset.id, space)))),format.raw/*24.75*/("""">"""),_display_(Seq[Any](/*24.78*/Html(dataset.name))),format.raw/*24.96*/("""</a></h4>
                <p class='abstractsummary'>"""),_display_(Seq[Any](/*25.45*/Html(dataset.description.replace("\n", "<br>")))),format.raw/*25.92*/("""</p>
            </div>
        </div>
            <!-- Dataset Info -->
        <ul class="list-group">
            <li class="list-group-item dataset-panel-footer">
                <span class="glyphicon glyphicon-folder-close" title=""""),_display_(Seq[Any](/*31.72*/dataset/*31.79*/.folders.size)),format.raw/*31.92*/(""" folders"></span> """),_display_(Seq[Any](/*31.111*/dataset/*31.118*/.folders.size)),format.raw/*31.131*/("""
                <span class="glyphicon glyphicon-file" title=""""),_display_(Seq[Any](/*32.64*/dataset/*32.71*/.files.size)),format.raw/*32.82*/(""" files"></span> """),_display_(Seq[Any](/*32.99*/dataset/*32.106*/.files.size)),format.raw/*32.117*/("""
                <span class="glyphicon glyphicon-tags" title=""""),_display_(Seq[Any](/*33.64*/dataset/*33.71*/.tags.size)),format.raw/*33.81*/(""" tags"></span> """),_display_(Seq[Any](/*33.97*/dataset/*33.104*/.tags.size)),format.raw/*33.114*/("""
                <span class="glyphicon glyphicon-list" title=""""),_display_(Seq[Any](/*34.64*/(dataset.metadataCount))),format.raw/*34.87*/(""" metadata fields"></span> """),_display_(Seq[Any](/*34.114*/(dataset.metadataCount))),format.raw/*34.137*/("""
                """),_display_(Seq[Any](/*35.18*/if(!comments.isEmpty)/*35.39*/ {_display_(Seq[Any](format.raw/*35.41*/("""
                    <span class="glyphicon glyphicon glyphicon-comment" title=""""),_display_(Seq[Any](/*36.81*/(comments.get(dataset.id)))),format.raw/*36.107*/(""" comments"></span> """),_display_(Seq[Any](/*36.127*/(comments.get(dataset.id)))),format.raw/*36.153*/("""
                """)))})),format.raw/*37.18*/("""
                """),_display_(Seq[Any](/*38.18*/if(user.isDefined)/*38.36*/ {_display_(Seq[Any](format.raw/*38.38*/("""
                    """),_display_(Seq[Any](/*39.22*/if(space.isDefined)/*39.41*/ {_display_(Seq[Any](format.raw/*39.43*/("""

                            """),_display_(Seq[Any](/*41.30*/if(user.get.id.equals(dataset.author.id) || Permission.checkPermission(Permission.RemoveResourceFromSpace, ResourceRef(ResourceRef.space,UUID(space.get))))/*41.185*/ {_display_(Seq[Any](format.raw/*41.187*/("""
                                <button onclick="confirmRemoveResourceFromResource('space','"""),_display_(Seq[Any](/*42.94*/Messages("space.title"))),format.raw/*42.117*/("""','"""),_display_(Seq[Any](/*42.121*/(UUID(space.getOrElse(""))))),format.raw/*42.148*/("""','dataset','"""),_display_(Seq[Any](/*42.162*/(dataset.id))),format.raw/*42.174*/("""','"""),_display_(Seq[Any](/*42.178*/(dataset.name.replace("'","&#39;")))),format.raw/*42.213*/("""',true,'"""),_display_(Seq[Any](/*42.222*/redirect/*42.230*/.url)),format.raw/*42.234*/("""')"
                                class="btn btn-link" title="Remove the dataset from the """),_display_(Seq[Any](/*43.90*/Messages("space.title"))),format.raw/*43.113*/("""">
                                    <span class="glyphicon glyphicon-remove"></span></button>
                            """)))}/*45.31*/else/*45.36*/{_display_(Seq[Any](format.raw/*45.37*/("""
                                <div class="inline" title="No permission to remove the dataset from the """),_display_(Seq[Any](/*46.106*/Messages("space.title"))),format.raw/*46.129*/("""">
                                    <button disabled class="btn btn-link"><span class="glyphicon glyphicon-remove"></span></button>
                                </div>
                            """)))})),format.raw/*49.30*/("""

                    """)))}/*51.23*/else/*51.28*/{_display_(Seq[Any](format.raw/*51.29*/("""
                        """),_display_(Seq[Any](/*52.26*/if(user.get.id.equals(dataset.author.id) || Permission.checkPermission(Permission.DeleteDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*52.169*/ {_display_(Seq[Any](format.raw/*52.171*/("""
                            <button onclick="confirmDeleteResource('dataset','dataset','"""),_display_(Seq[Any](/*53.90*/(dataset.id))),format.raw/*53.102*/("""','"""),_display_(Seq[Any](/*53.106*/(dataset.name.replace("'","&#39;")))),format.raw/*53.141*/("""',false,'"""),_display_(Seq[Any](/*53.151*/redirect/*53.159*/.url)),format.raw/*53.163*/("""')" class="btn btn-link" title="Delete the dataset and its contents">
                                <span class="glyphicon glyphicon-trash"></span></button>
                        """)))}/*55.27*/else/*55.32*/{_display_(Seq[Any](format.raw/*55.33*/("""
                            <div class="inline" title="No permission to delete the dataset">
                                <button disabled class="btn btn-link"><span class="glyphicon glyphicon-trash"></span></button>
                            </div>
                        """)))})),format.raw/*59.26*/("""
                    """)))})),format.raw/*60.22*/("""
                """)))})),format.raw/*61.18*/("""
            </li>
        </ul>
        """),_display_(Seq[Any](/*64.10*/if(isFollowing)/*64.25*/{_display_(Seq[Any](format.raw/*64.26*/("""
            <ul class="list-group center-margin">
            """),_display_(Seq[Any](/*66.14*/user/*66.18*/ match/*66.24*/ {/*67.17*/case Some(viewer) =>/*67.37*/ {_display_(Seq[Any](format.raw/*67.39*/("""
                    <a id="followButton" type="button" class="btn-link" data-toggle="button" autocomplete="off" objectType="dataset" objectId=""""),_display_(Seq[Any](/*68.145*/dataset/*68.152*/.id.stringify)),format.raw/*68.165*/("""">
                    """),_display_(Seq[Any](/*69.22*/if(viewer.followedEntities.filter(x => (x.id == dataset.id)).nonEmpty)/*69.92*/ {_display_(Seq[Any](format.raw/*69.94*/("""
                        <span class='glyphicon glyphicon-star-empty'></span>  Unfollow
                    """)))}/*71.23*/else/*71.28*/{_display_(Seq[Any](format.raw/*71.29*/("""
                        <span class='glyphicon glyphicon-star'></span> Follow
                    """)))})),format.raw/*73.22*/("""
                    </a>
                """)))}})),format.raw/*76.14*/("""
            </ul>
        """)))})),format.raw/*78.10*/("""
    </div>
</div>"""))}
    }
    
    def render(dataset:models.Dataset,space:Option[String],comments:Map[UUID, Int],classes:String,isFollowing:Boolean,isSpace:Boolean,redirect:Call,selected:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,space,comments,classes,isFollowing,isSpace,redirect,selected)(user)
    
    def f:((models.Dataset,Option[String],Map[UUID, Int],String,Boolean,Boolean,Call,Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,space,comments,classes,isFollowing,isSpace,redirect,selected) => (user) => apply(dataset,space,comments,classes,isFollowing,isSpace,redirect,selected)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/tile.scala.html
                    HASH: b802befeb2a7be080be925fd5fafdc0797ac26e7
                    MATRIX: 689->1|1037->201|1064->256|1121->278|1149->285|1191->292|1206->299|1230->302|1334->371|1360->389|1399->391|1448->405|1468->417|1507->419|1660->536|1676->543|1701->546|1822->649|1835->654|1874->655|2026->770|2043->777|2069->780|2205->884|2247->894|2330->941|2372->974|2411->975|2474->1002|2539->1045|2645->1115|2785->1232|2843->1253|2884->1271|2953->1308|3065->1384|3131->1428|3170->1431|3210->1449|3300->1503|3369->1550|3643->1788|3659->1795|3694->1808|3750->1827|3767->1834|3803->1847|3903->1911|3919->1918|3952->1929|4005->1946|4022->1953|4056->1964|4156->2028|4172->2035|4204->2045|4256->2061|4273->2068|4306->2078|4406->2142|4451->2165|4515->2192|4561->2215|4615->2233|4645->2254|4685->2256|4802->2337|4851->2363|4908->2383|4957->2409|5007->2427|5061->2445|5088->2463|5128->2465|5186->2487|5214->2506|5254->2508|5321->2539|5486->2694|5527->2696|5657->2790|5703->2813|5744->2817|5794->2844|5845->2858|5880->2870|5921->2874|5979->2909|6025->2918|6043->2926|6070->2930|6199->3023|6245->3046|6390->3173|6403->3178|6442->3179|6585->3285|6631->3308|6866->3511|6908->3535|6921->3540|6960->3541|7022->3567|7175->3710|7216->3712|7342->3802|7377->3814|7418->3818|7476->3853|7523->3863|7541->3871|7568->3875|7771->4060|7784->4065|7823->4066|8136->4347|8190->4369|8240->4387|8318->4429|8342->4444|8381->4445|8481->4509|8494->4513|8509->4519|8520->4538|8549->4558|8589->4560|8771->4705|8788->4712|8824->4725|8884->4749|8963->4819|9003->4821|9131->4931|9144->4936|9183->4937|9315->5037|9391->5094|9451->5122
                    LINES: 20->1|26->1|27->4|27->4|27->4|27->4|27->4|27->4|29->6|29->6|29->6|30->7|30->7|30->7|31->8|31->8|31->8|34->11|34->11|34->11|35->12|35->12|35->12|38->15|39->16|41->18|41->18|41->18|42->19|42->19|43->20|43->20|43->20|43->20|45->22|47->24|47->24|47->24|47->24|48->25|48->25|54->31|54->31|54->31|54->31|54->31|54->31|55->32|55->32|55->32|55->32|55->32|55->32|56->33|56->33|56->33|56->33|56->33|56->33|57->34|57->34|57->34|57->34|58->35|58->35|58->35|59->36|59->36|59->36|59->36|60->37|61->38|61->38|61->38|62->39|62->39|62->39|64->41|64->41|64->41|65->42|65->42|65->42|65->42|65->42|65->42|65->42|65->42|65->42|65->42|65->42|66->43|66->43|68->45|68->45|68->45|69->46|69->46|72->49|74->51|74->51|74->51|75->52|75->52|75->52|76->53|76->53|76->53|76->53|76->53|76->53|76->53|78->55|78->55|78->55|82->59|83->60|84->61|87->64|87->64|87->64|89->66|89->66|89->66|89->67|89->67|89->67|90->68|90->68|90->68|91->69|91->69|91->69|93->71|93->71|93->71|95->73|97->76|99->78
                    -- GENERATED --
                */
            