
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tools extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[List[UUID],scala.collection.mutable.Map[UUID, services.ToolInstance],UUID,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(instances: List[UUID], instanceMap: scala.collection.mutable.Map[UUID, services.ToolInstance], datasetID: UUID, datasetName: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.171*/("""

"""),format.raw/*4.1*/("""
<div class="row bottom-padding">
  <div class="col-md-12">
    <h4>Analysis Environment Instances</h4>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div id="toolsList">
      """),_display_(Seq[Any](/*14.8*/instances/*14.17*/.map/*14.21*/ { instanceID =>_display_(Seq[Any](format.raw/*14.37*/("""
        <div id="col_"""),_display_(Seq[Any](/*15.23*/instanceID)),format.raw/*15.33*/("""" class="row bottom-padding">
          <div class="col-md-10">
            <div>
              """),_display_(Seq[Any](/*18.16*/{
                // Create entry for each ToolInstance that this dataset has been uploaded to
                var htmlStr = ""
                instanceMap.get(instanceID) match {
                  case Some(inst) => {
                    if (inst.url == "") {
                      htmlStr += inst.name+" (not yet ready)</br>"
                      htmlStr += inst.id
                    }
                    else htmlStr += "<a href="+inst.url+" target='_blank'>"+inst.name+"</a>"

                    htmlStr += "</br>"
                    if (inst.uploadHistory.keys.toList.length == 1)
                      htmlStr += "1 dataset uploaded"
                    else
                      htmlStr += inst.uploadHistory.keys.toList.length.toString + " datasets uploaded"
                  }
                  case None => {}
                }
                Html(htmlStr)
              })),format.raw/*38.16*/("""
            </div>
          </div>
        </div>
      """)))})),format.raw/*42.8*/("""
    </div>
  </div>
</div>

<div class="row bottom-padding">
  <div class="col-md-12">
    <a class="btn btn-link" onclick="refreshToolSidebar()">
      <span class="glyphicon glyphicon-refresh"></span>&nbsp;Refresh List
    </a>
    <a class="btn btn-link" href="""),_display_(Seq[Any](/*52.35*/controllers/*52.46*/.routes.ToolManager.toolManager().url)),format.raw/*52.83*/(""">
      <span class="glyphicon glyphicon-log-in"></span>&nbsp;Environment Manager
    </a>
  </div>

  <div class="col-md-12">
    <h4>Launch new instance with dataset</h4>
    <div class="form-inline">
      <div class="input-group input-group-sm col-md-8">
        <input type="text" id="newToolNameField" class="form-control add-resource" placeholder="Enter instance name" value=""""),_display_(Seq[Any](/*61.126*/{
            user match {
              case Some(u) => u.fullName + "'s instance"
              case None => "My instance"}})),format.raw/*64.43*/("""">
        <select id="launchNewToolInstance" class="form-control add-resource"></select>
        <span class="input-group-btn">
          <a href="#" onclick="launchToolRequest()" class="btn btn-default btn-large" id="launchtoolBtn" title="Launch New Environment">
            <span class="glyphicon glyphicon-plus"></span> Launch
          </a>
        </span>
      </div>
    </div>
  </div>

  <div class="col-md-12">
    <h4>Upload dataset to existing instance</h4>
    <div class="form-inline">
      <div class="input-group input-group-sm col-md-8">
        <select id="addToExistingToolInstance" class="form-control add-resource">
        </select>
        <span class="input-group-btn">
          <a href="#" onclick="launchUploadRequest()" class="btn btn-default btn-large" id="addToolSessionBtn" title="Upload Dataset to Environment">
            <span class="glyphicon glyphicon-plus"></span> Upload
          </a>
        </span>
      </div>
      <div id="addToExistingInstanceFooter"></div>
    </div>
  </div>
</div>

<script language="javascript">
  $("#launchNewToolInstance").select2("""),format.raw/*93.39*/("""{"""),format.raw/*93.40*/("""
    theme: "bootstrap",
    placeholder: "Select analysis environment",
    allowClear: false,
    ajax: """),format.raw/*97.11*/("""{"""),format.raw/*97.12*/("""
      url: function() """),format.raw/*98.23*/("""{"""),format.raw/*98.24*/("""
        return jsRoutes.controllers.ToolManager.getLaunchableTools().url;
      """),format.raw/*100.7*/("""}"""),format.raw/*100.8*/(""",
      processResults: function(data) """),format.raw/*101.38*/("""{"""),format.raw/*101.39*/("""
        var results = """),format.raw/*102.23*/("""{"""),format.raw/*102.24*/("""
          results: Object.keys(data).map(function(key) """),format.raw/*103.56*/("""{"""),format.raw/*103.57*/("""
            return """),format.raw/*104.20*/("""{"""),format.raw/*104.21*/("""
              text: data[key]["name"],
              id: key
            """),format.raw/*107.13*/("""}"""),format.raw/*107.14*/("""
          """),format.raw/*108.11*/("""}"""),format.raw/*108.12*/(""")"""),format.raw/*108.13*/("""}"""),format.raw/*108.14*/(""";
        return results
      """),format.raw/*110.7*/("""}"""),format.raw/*110.8*/("""
    """),format.raw/*111.5*/("""}"""),format.raw/*111.6*/("""
  """),format.raw/*112.3*/("""}"""),format.raw/*112.4*/(""");

  $("#addToExistingToolInstance").select2("""),format.raw/*114.43*/("""{"""),format.raw/*114.44*/("""
    theme: "bootstrap",
    placeholder: "Select an instance",
    allowClear: true,
    ajax: """),format.raw/*118.11*/("""{"""),format.raw/*118.12*/("""
      url: function() """),format.raw/*119.23*/("""{"""),format.raw/*119.24*/("""
        return jsRoutes.controllers.ToolManager.getInstances().url;
      """),format.raw/*121.7*/("""}"""),format.raw/*121.8*/(""",
      processResults: function(data) """),format.raw/*122.38*/("""{"""),format.raw/*122.39*/("""
        var existingInstances = []
        for (var instanceID in data) """),format.raw/*124.38*/("""{"""),format.raw/*124.39*/("""
          existingInstances.push("""),format.raw/*125.34*/("""{"""),format.raw/*125.35*/("""
            text: data[instanceID],
            id: instanceID.toString()
          """),format.raw/*128.11*/("""}"""),format.raw/*128.12*/(""")
        """),format.raw/*129.9*/("""}"""),format.raw/*129.10*/("""
        return """),format.raw/*130.16*/("""{"""),format.raw/*130.17*/("""results: existingInstances"""),format.raw/*130.43*/("""}"""),format.raw/*130.44*/("""
      """),format.raw/*131.7*/("""}"""),format.raw/*131.8*/("""
    """),format.raw/*132.5*/("""}"""),format.raw/*132.6*/("""
  """),format.raw/*133.3*/("""}"""),format.raw/*133.4*/(""").on("change", function(e) """),format.raw/*133.31*/("""{"""),format.raw/*133.32*/("""
    """),_display_(Seq[Any](/*134.6*/instances/*134.15*/.map/*134.19*/ { instanceID =>_display_(Seq[Any](format.raw/*134.35*/("""
      if (e.currentTarget.value == """"),_display_(Seq[Any](/*135.38*/instanceID/*135.48*/.toString)),format.raw/*135.57*/("""") """),format.raw/*135.60*/("""{"""),format.raw/*135.61*/("""
          notify("The current dataset has previously been uploaded to this instance. " +
          "Uploading it again will overwrite any duplicate filenames.", "warning")
      """),format.raw/*138.7*/("""}"""),format.raw/*138.8*/("""
    """)))})),format.raw/*139.6*/("""
  """),format.raw/*140.3*/("""}"""),format.raw/*140.4*/(""");

  function launchToolRequest() """),format.raw/*142.32*/("""{"""),format.raw/*142.33*/("""
    // Send request to ToolManagerPlugin to launch a ToolInstance and upload current dataset.
    var instanceName = $('#newToolNameField').val()
    var toolType = $('#launchNewToolInstance').val()

    if (instanceName == undefined || toolType == undefined) """),format.raw/*147.61*/("""{"""),format.raw/*147.62*/("""
      notify("Please select a name and environment for the new instance.", "error")
    """),format.raw/*149.5*/("""}"""),format.raw/*149.6*/("""
    else """),format.raw/*150.10*/("""{"""),format.raw/*150.11*/("""
      var request = new XMLHttpRequest();
      request.onreadystatechange = function() """),format.raw/*152.47*/("""{"""),format.raw/*152.48*/("""
        if (request.readyState == 4 && request.status == 200) """),format.raw/*153.63*/("""{"""),format.raw/*153.64*/("""
          var sessionId = request.response
          console.log("launchToolRequest OK. sessionId created: "+ sessionId)
          refreshToolSidebar();
        """),format.raw/*157.9*/("""}"""),format.raw/*157.10*/("""
        else if (request.readyState == 4 && (request.status != 200))
          console.log("launchToolRequest came back with "+request.status.toString());
      """),format.raw/*160.7*/("""}"""),format.raw/*160.8*/("""

      request.open("GET", jsRoutes.controllers.ToolManager.launchTool(instanceName, toolType, """"),_display_(Seq[Any](/*162.97*/datasetID)),format.raw/*162.106*/("""", """"),_display_(Seq[Any](/*162.111*/datasetName)),format.raw/*162.122*/("""").url, true);
      request.send();
    """),format.raw/*164.5*/("""}"""),format.raw/*164.6*/("""
  """),format.raw/*165.3*/("""}"""),format.raw/*165.4*/("""

  function launchUploadRequest() """),format.raw/*167.34*/("""{"""),format.raw/*167.35*/("""
    // Send request to ToolManagerPlugin to upload current dataset to existing ToolInstance.
    var instanceID = $('#addToExistingToolInstance').val()

    if (instanceID == undefined) """),format.raw/*171.34*/("""{"""),format.raw/*171.35*/("""
      notify("Please select a target instance for uploading the dataset.", "error")
    """),format.raw/*173.5*/("""}"""),format.raw/*173.6*/("""
    else """),format.raw/*174.10*/("""{"""),format.raw/*174.11*/("""
      var request = new XMLHttpRequest();
      request.onreadystatechange = function() """),format.raw/*176.47*/("""{"""),format.raw/*176.48*/("""
        if (request.readyState == 4 && request.status == 200) """),format.raw/*177.63*/("""{"""),format.raw/*177.64*/("""
          refreshToolSidebar();
        """),format.raw/*179.9*/("""}"""),format.raw/*179.10*/("""
        else if (request.readyState == 4 && (request.status != 200))
          console.log("launchUploadRequest came back with "+request.status.toString());
      """),format.raw/*182.7*/("""}"""),format.raw/*182.8*/("""

      request.open("GET", jsRoutes.controllers.ToolManager.uploadDatasetToTool(instanceID, """"),_display_(Seq[Any](/*184.94*/datasetID)),format.raw/*184.103*/("""", """"),_display_(Seq[Any](/*184.108*/datasetName)),format.raw/*184.119*/("""").url, true);
      request.send();
    """),format.raw/*186.5*/("""}"""),format.raw/*186.6*/("""
  """),format.raw/*187.3*/("""}"""),format.raw/*187.4*/("""
</script>
"""))}
    }
    
    def render(instances:List[UUID],instanceMap:scala.collection.mutable.Map[UUID, services.ToolInstance],datasetID:UUID,datasetName:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(instances,instanceMap,datasetID,datasetName)(user)
    
    def f:((List[UUID],scala.collection.mutable.Map[UUID, services.ToolInstance],UUID,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (instances,instanceMap,datasetID,datasetName) => (user) => apply(instances,instanceMap,datasetID,datasetName)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/tools.scala.html
                    HASH: 4e682a71c22d70c61da2a3674fd123b3cb2819c6
                    MATRIX: 690->1|976->170|1004->195|1236->392|1254->401|1267->405|1321->421|1380->444|1412->454|1545->551|2458->1442|2548->1501|2849->1766|2869->1777|2928->1814|3349->2198|3497->2324|4630->3429|4659->3430|4793->3536|4822->3537|4873->3560|4902->3561|5011->3642|5040->3643|5108->3682|5138->3683|5190->3706|5220->3707|5305->3763|5335->3764|5384->3784|5414->3785|5517->3859|5547->3860|5587->3871|5617->3872|5647->3873|5677->3874|5736->3905|5765->3906|5798->3911|5827->3912|5858->3915|5887->3916|5962->3962|5992->3963|6117->4059|6147->4060|6199->4083|6229->4084|6332->4159|6361->4160|6429->4199|6459->4200|6561->4273|6591->4274|6654->4308|6684->4309|6798->4394|6828->4395|6866->4405|6896->4406|6941->4422|6971->4423|7026->4449|7056->4450|7091->4457|7120->4458|7153->4463|7182->4464|7213->4467|7242->4468|7298->4495|7328->4496|7370->4502|7389->4511|7403->4515|7458->4531|7533->4569|7553->4579|7585->4588|7617->4591|7647->4592|7854->4771|7883->4772|7921->4778|7952->4781|7981->4782|8045->4817|8075->4818|8365->5079|8395->5080|8512->5169|8541->5170|8580->5180|8610->5181|8728->5270|8758->5271|8850->5334|8880->5335|9070->5497|9100->5498|9290->5660|9319->5661|9454->5759|9487->5768|9530->5773|9565->5784|9634->5825|9663->5826|9694->5829|9723->5830|9787->5865|9817->5866|10033->6053|10063->6054|10180->6143|10209->6144|10248->6154|10278->6155|10396->6244|10426->6245|10518->6308|10548->6309|10617->6350|10647->6351|10839->6515|10868->6516|11000->6611|11033->6620|11076->6625|11111->6636|11180->6677|11209->6678|11240->6681|11269->6682
                    LINES: 20->1|24->1|26->4|36->14|36->14|36->14|36->14|37->15|37->15|40->18|60->38|64->42|74->52|74->52|74->52|83->61|86->64|115->93|115->93|119->97|119->97|120->98|120->98|122->100|122->100|123->101|123->101|124->102|124->102|125->103|125->103|126->104|126->104|129->107|129->107|130->108|130->108|130->108|130->108|132->110|132->110|133->111|133->111|134->112|134->112|136->114|136->114|140->118|140->118|141->119|141->119|143->121|143->121|144->122|144->122|146->124|146->124|147->125|147->125|150->128|150->128|151->129|151->129|152->130|152->130|152->130|152->130|153->131|153->131|154->132|154->132|155->133|155->133|155->133|155->133|156->134|156->134|156->134|156->134|157->135|157->135|157->135|157->135|157->135|160->138|160->138|161->139|162->140|162->140|164->142|164->142|169->147|169->147|171->149|171->149|172->150|172->150|174->152|174->152|175->153|175->153|179->157|179->157|182->160|182->160|184->162|184->162|184->162|184->162|186->164|186->164|187->165|187->165|189->167|189->167|193->171|193->171|195->173|195->173|196->174|196->174|198->176|198->176|199->177|199->177|201->179|201->179|204->182|204->182|206->184|206->184|206->184|206->184|208->186|208->186|209->187|209->187
                    -- GENERATED --
                */
            