
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object editInfo extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Dataset,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.56*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/if(user.isDefined)/*5.20*/ {_display_(Seq[Any](format.raw/*5.22*/("""
<a class=" accordion-toggle collapsed" data-toggle="collapse"
data-parent="#accordion7" href="#collapseSeven" id="editabout"
title="Edit Dataset Information"><span class="glyphicon glyphicon-chevron-down"></span> Edit Info</a>
<div id="collapseSeven" class="accordion-body collapse">
    <div class="panel panel-info">
        <div class="panel-body">
        <!-- If the user can edit the dataset, the elements are enabled, otherwise they are present but disabled to provide consistent UE. -->
        """),_display_(Seq[Any](/*13.10*/if(Permission.checkPermission(Permission.EditDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*13.110*/ {_display_(Seq[Any](format.raw/*13.112*/("""
            <form class="form-inline" id="form2">
                <table>
                    <tr>
                        <td style="width: 30%; vertical-align:top;">Name: </td>
                        <td>
                            <textarea cols=30 rows=4 type="text" id="editname">"""),_display_(Seq[Any](/*19.81*/Html(dataset.name.replace("<br>", "\n")))),format.raw/*19.121*/("""</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%; vertical-align:top;">Description: </td>
                        <td>
                            <textarea cols=30 rows=4 type="text" id="editdesc">"""),_display_(Seq[Any](/*25.81*/Html(dataset.description.replace("<br>", "\n")))),format.raw/*25.128*/("""</textarea>
                        </td>
                    </tr>
                </table>

                <button class="btn btn-primary" onclick="return updateAboutData('"""),_display_(Seq[Any](/*30.83*/dataset/*30.90*/.id.stringify)),format.raw/*30.103*/("""');"
                title="Update Information"><span class="glyphicon glyphicon-ok"></span> Submit</button>
                <button class="btn btn-default" onclick="return closeAboutEdit();"
                title="Close Editor"><span class="glyphicon glyphicon-eject"></span> Close</button>
            </form>
        """)))}/*35.11*/else/*35.16*/{_display_(Seq[Any](format.raw/*35.17*/("""
            <form class="form-inline" id="form2">
                <table>
                    <tr>
                        <td style="width: 30%; vertical-align:top;">Name: </td>
                        <td>
                            <textarea disabled cols=30 rows=4 type="text" id="editname">"""),_display_(Seq[Any](/*41.90*/dataset/*41.97*/.name)),format.raw/*41.102*/("""</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%; vertical-align:top;">Description: </td>
                        <td>
                            <textarea disabled cols=30 rows=4 type="text" id="editdesc">"""),_display_(Seq[Any](/*47.90*/dataset/*47.97*/.description)),format.raw/*47.109*/("""</textarea>
                        </td>
                    </tr>
                </table>

                <button disabled class="btn btn-primary" onclick="return updateAboutData('"""),_display_(Seq[Any](/*52.92*/dataset/*52.99*/.id.stringify)),format.raw/*52.112*/("""');"
                title="Update Information"><span class="glyphicon glyphicon-ok"></span> Submit</button>
                <button disabled class="btn btn-default" onclick="return closeAboutEdit();"
                title="Close Editor"><span class="glyphicon glyphicon-eject"></span> Close</button>
            </form>
        """)))})),format.raw/*57.10*/("""
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">

function closeAboutEdit() """),format.raw/*63.27*/("""{"""),format.raw/*63.28*/("""
    //Reset the data to the current values
    var text = $("#aboutdesc").html().trim();
    text = text.replace(/<br>/g, "\n");
    $("#editdesc").val(htmlDecode(text));
    text = $("#datasettitle").html().trim();
    text = text.replace(/<br>/g, "\n");
    $("#editname").val(htmlDecode(text));

    //Close the edit form
    $("#editabout").addClass('collapsed');
    $("#collapseSeven").collapse('toggle');
    return false;
"""),format.raw/*76.1*/("""}"""),format.raw/*76.2*/("""

function updateAboutData(datasetId) """),format.raw/*78.37*/("""{"""),format.raw/*78.38*/("""
    var description = $("#editdesc").val();
    var name = $("#editname").val();
    var encName = htmlEncode(name);
    var encDescription = htmlEncode(description);
    var jsonData = JSON.stringify("""),format.raw/*83.35*/("""{"""),format.raw/*83.36*/(""""description":encDescription, "name":encName"""),format.raw/*83.80*/("""}"""),format.raw/*83.81*/(""");

    var request = jsRoutes.api.Datasets.updateInformation(datasetId).ajax("""),format.raw/*85.75*/("""{"""),format.raw/*85.76*/("""
        data: jsonData,
        type: 'POST',
        contentType: "application/json"
    """),format.raw/*89.5*/("""}"""),format.raw/*89.6*/(""");

    request.done(function (response, textStatus, jqXHR)"""),format.raw/*91.56*/("""{"""),format.raw/*91.57*/("""
        ///console.log("Response " + response);
        //Sucessful update of the DB - update the interface

        $("#aboutdesc").html(encDescription.replace(/\n/g, "<br>"));
        $("#datasettitle").html(encName.replace(/\n/g, "<br>"));

        closeAboutEdit();
    """),format.raw/*99.5*/("""}"""),format.raw/*99.6*/(""");

    request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*101.59*/("""{"""),format.raw/*101.60*/("""
        console.error("The following error occured: "+textStatus, errorThrown);
        var errMsg = "You must be logged in to update the information about a dataset.";
        if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*104.52*/("""{"""),format.raw/*104.53*/("""
            notify("The dataset information was not updated due to : " + errorThrown, "error");
        """),format.raw/*106.9*/("""}"""),format.raw/*106.10*/("""
    """),format.raw/*107.5*/("""}"""),format.raw/*107.6*/(""");

    return false;
"""),format.raw/*110.1*/("""}"""),format.raw/*110.2*/("""
</script>

<script src=""""),_display_(Seq[Any](/*113.15*/routes/*113.21*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*113.66*/("""" language="javascript"></script>
<!-- End dataset information elements -->
""")))})),format.raw/*115.2*/("""
"""))}
    }
    
    def render(dataset:Dataset,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset)(user)
    
    def f:((Dataset) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset) => (user) => apply(dataset)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/editInfo.scala.html
                    HASH: d7c5fde4b96acad2960a58e98cb9a4d9cace408e
                    MATRIX: 620->1|790->55|818->80|854->82|880->100|919->102|1460->607|1570->707|1611->709|1936->998|1999->1038|2324->1327|2394->1374|2606->1550|2622->1557|2658->1570|2998->1892|3011->1897|3050->1898|3384->2196|3400->2203|3428->2208|3762->2506|3778->2513|3813->2525|4034->2710|4050->2717|4086->2730|4448->3060|4591->3175|4620->3176|5078->3607|5106->3608|5172->3646|5201->3647|5431->3849|5460->3850|5532->3894|5561->3895|5667->3973|5696->3974|5814->4065|5842->4066|5929->4125|5958->4126|6260->4401|6288->4402|6379->4464|6409->4465|6659->4686|6689->4687|6822->4792|6852->4793|6885->4798|6914->4799|6964->4821|6993->4822|7056->4848|7072->4854|7140->4899|7249->4976
                    LINES: 20->1|24->1|26->4|27->5|27->5|27->5|35->13|35->13|35->13|41->19|41->19|47->25|47->25|52->30|52->30|52->30|57->35|57->35|57->35|63->41|63->41|63->41|69->47|69->47|69->47|74->52|74->52|74->52|79->57|85->63|85->63|98->76|98->76|100->78|100->78|105->83|105->83|105->83|105->83|107->85|107->85|111->89|111->89|113->91|113->91|121->99|121->99|123->101|123->101|126->104|126->104|128->106|128->106|129->107|129->107|132->110|132->110|135->113|135->113|135->113|137->115
                    -- GENERATED --
                */
            