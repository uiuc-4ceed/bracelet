
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object follow extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Dataset,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.56*/("""

"""),_display_(Seq[Any](/*3.2*/if(user.isDefined)/*3.20*/ {_display_(Seq[Any](format.raw/*3.22*/("""
    """),_display_(Seq[Any](/*4.6*/if(!dataset.followers.contains(user.get.id))/*4.50*/ {_display_(Seq[Any](format.raw/*4.52*/("""
<!--         <button id="followButton"
                class="btn btn-link"
                objectId=""""),_display_(Seq[Any](/*7.28*/dataset/*7.35*/.id)),format.raw/*7.38*/(""""
                objectName=""""),_display_(Seq[Any](/*8.30*/dataset/*8.37*/.name)),format.raw/*8.42*/(""""
                objectType="dataset"><span class="glyphicon glyphicon-star"></span> Follow
        </button>
 -->    """)))}/*11.11*/else/*11.16*/{_display_(Seq[Any](format.raw/*11.17*/("""
<!--         <button id="followButton"
                class="btn btn-link"
                objectId=""""),_display_(Seq[Any](/*14.28*/dataset/*14.35*/.id)),format.raw/*14.38*/(""""
                objectName=""""),_display_(Seq[Any](/*15.30*/dataset/*15.37*/.name)),format.raw/*15.42*/(""""
                objectType="dataset"><span class="glyphicon glyphicon-star-empty"></span> Unfollow
        </button>
 -->    """)))})),format.raw/*18.10*/("""

    <div id="recommendPanel" class="panel panel-default" style="display : none;">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-parent="#accordion"
                href="#collapseThree"
                aria-expanded="true"
                style="float:left;">
                    Also follow these?
                </a>
                <a style="float:right;" href="javascript:$('#recommendPanel').slideToggle('slow');">x</a>
                <div style="clear : both;"></div>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse in" aria-expanded="true">
            <div id="recommendDiv" class="panel-body">
            </div>
        </div>
    </div>

    <script src=""""),_display_(Seq[Any](/*39.19*/routes/*39.25*/.Assets.at("javascripts/recommendation.js"))),format.raw/*39.68*/("""" type="text/javascript"></script>
    <script>
        $(document).on('click', '.followButton', function() """),format.raw/*41.61*/("""{"""),format.raw/*41.62*/("""
            var id = $(this).attr('objectId');
            var name = $(this).attr('objectName');
            var type = $(this).attr('objectType');
            if ($(this).attr('id') === '') """),format.raw/*45.44*/("""{"""),format.raw/*45.45*/("""
              followHandler.call(this, jsRoutes, id, name, type, undefined, undefined);
            """),format.raw/*47.13*/("""}"""),format.raw/*47.14*/(""" else """),format.raw/*47.20*/("""{"""),format.raw/*47.21*/("""
              followHandler.call(this, jsRoutes, id, name, type, function(data) """),format.raw/*48.81*/("""{"""),format.raw/*48.82*/("""
                    recommendationHandler(jsRoutes, $('#recommendPanel'), $('#recommendDiv'),
                                  data['recommendations']);
                """),format.raw/*51.17*/("""}"""),format.raw/*51.18*/(""", undefined);
            """),format.raw/*52.13*/("""}"""),format.raw/*52.14*/("""
        """),format.raw/*53.9*/("""}"""),format.raw/*53.10*/(""");
    </script>
""")))})),format.raw/*55.2*/("""

"""))}
    }
    
    def render(dataset:Dataset,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset)(user)
    
    def f:((Dataset) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset) => (user) => apply(dataset)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/follow.scala.html
                    HASH: 35146d2918f09938430a7590b74ba8e809f3079b
                    MATRIX: 618->1|766->55|803->58|829->76|868->78|908->84|960->128|999->130|1138->234|1153->241|1177->244|1243->275|1258->282|1284->287|1423->408|1436->413|1475->414|1615->518|1631->525|1656->528|1723->559|1739->566|1766->571|1926->699|2732->1469|2747->1475|2812->1518|2948->1626|2977->1627|3198->1820|3227->1821|3356->1922|3385->1923|3419->1929|3448->1930|3557->2011|3586->2012|3785->2183|3814->2184|3868->2210|3897->2211|3933->2220|3962->2221|4011->2239
                    LINES: 20->1|23->1|25->3|25->3|25->3|26->4|26->4|26->4|29->7|29->7|29->7|30->8|30->8|30->8|33->11|33->11|33->11|36->14|36->14|36->14|37->15|37->15|37->15|40->18|61->39|61->39|61->39|63->41|63->41|67->45|67->45|69->47|69->47|69->47|69->47|70->48|70->48|73->51|73->51|74->52|74->52|75->53|75->53|77->55
                    -- GENERATED --
                */
            