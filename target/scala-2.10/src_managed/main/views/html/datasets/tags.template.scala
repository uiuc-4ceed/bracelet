
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tags extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[Dataset,List[File],Set[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset, fileList: List[File], filesTags: Set[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.Play.current


Seq[Any](format.raw/*1.102*/("""

"""),format.raw/*5.1*/("""
<div class="row">
    <div class="col-md-12">
        <h4>Tags</h4>
    </div>
</div>

<!-- If the user can view tags for this dataset, they are displayed, the area is present but empty to provide a consistent UE. -->
"""),_display_(Seq[Any](/*13.2*/if(Permission.checkPermission(Permission.ViewTags, ResourceRef(ResourceRef.dataset, dataset.id)))/*13.99*/ {_display_(Seq[Any](format.raw/*13.101*/("""
    <div class="row bottom-padding">
        <div id ="tagList" class="col-md-12">
        <!-- The data-id attribute is needed since a) the removeTag for some reason is based off the tag name (text), which is what the id attribute is
             and b) identifiying elements by the tag text can sometimes break, especially in the special cases where there are characters that need encoding.
        -->
        """),_display_(Seq[Any](/*19.10*/dataset/*19.17*/.tags.map/*19.26*/ { tag =>_display_(Seq[Any](format.raw/*19.35*/("""
            <div id='"""),_display_(Seq[Any](/*20.23*/tag/*20.26*/.name)),format.raw/*20.31*/("""' class="tag" data-id=""""),_display_(Seq[Any](/*20.55*/tag/*20.58*/.id)),format.raw/*20.61*/("""">
                <a href=""""),_display_(Seq[Any](/*21.27*/routes/*21.33*/.Tags.search(tag.name))),format.raw/*21.55*/("""">"""),_display_(Seq[Any](/*21.58*/Html(tag.name))),format.raw/*21.72*/("""</a>
                <!-- If the user can delete the tag, the link is enabled, otherwise the link is not present to save space. -->
                """),_display_(Seq[Any](/*23.18*/if(Permission.checkPermission(Permission.DeleteTag, ResourceRef(ResourceRef.dataset, dataset.id)))/*23.116*/ {_display_(Seq[Any](format.raw/*23.118*/("""
                    <a href="#"> <span id=""""),_display_(Seq[Any](/*24.45*/tag/*24.48*/.name)),format.raw/*24.53*/("""" data-id=""""),_display_(Seq[Any](/*24.65*/tag/*24.68*/.id)),format.raw/*24.71*/("""" class="glyphicon glyphicon-remove tag-delete"></span></a>
                """)))})),format.raw/*25.18*/("""
            </div>
        """)))})),format.raw/*27.10*/("""
        </div>
    </div>
""")))})),format.raw/*30.2*/("""

<div class="row bottom-padding">
    <div class="col-md-12">
        <!-- If the user can add tags to the dataset, the form is enabled, otherwise the form is present but disabled to provide consistent UE. -->
        """),_display_(Seq[Any](/*35.10*/if(Permission.checkPermission(Permission.AddTag, ResourceRef(ResourceRef.dataset, dataset.id)))/*35.105*/ {_display_(Seq[Any](format.raw/*35.107*/("""
            <form class="form-inline">
                <div class="input-group input-group-sm">
                    <input maxlength=""""),_display_(Seq[Any](/*38.40*/play/*38.44*/.api.Play.configuration.getInt("clowder.tagLength").getOrElse(100))),format.raw/*38.110*/("""" type="text" id="tagField" class="form-control add-resource">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-large" id="tagB" title="Add Tag">
                            <span class="glyphicon glyphicon-tag"></span> Tag
                        </button>
                    </span>
                </div>
            </form>
        """)))}/*46.11*/else/*46.16*/{_display_(Seq[Any](format.raw/*46.17*/("""
            <form class="form-inline">
                <div class="input-group input-group-sm">
                    <input disabled type="text" id="tagField" class="form-control add-resource">
                    <span class="input-group-btn">
                        <button disabled class="btn btn-default btn-large" id="tagB" title="Add Tag">
                            <span class="glyphicon glyphicon-tag"></span> Tag
                        </button>
                    </span>
                </div>
            </form>
        """)))})),format.raw/*57.10*/("""
    </div>
</div>

"""),_display_(Seq[Any](/*61.2*/if(user.isEmpty)/*61.18*/ {_display_(Seq[Any](format.raw/*61.20*/("""
    <script language="javascript">
    window[ "userDefined" ] = false ;
    </script>
""")))})),format.raw/*65.2*/("""

"""),_display_(Seq[Any](/*67.2*/if(user.isDefined)/*67.20*/ {_display_(Seq[Any](format.raw/*67.22*/("""
    <script language="javascript">
        window["userDefined"] = true;
    </script>
""")))})),format.raw/*71.2*/("""

<script language="javascript">
    //The removeTag code is almost exactly the same as that in files.scala.html. It should probably be unified.
    function removeTag()"""),format.raw/*75.25*/("""{"""),format.raw/*75.26*/("""
        var tagId = $(this).attr("id");
        console.log("Removing tag " + tagId);
        //The data-id attribute is needed since a) the removeTag for some reason is based off the tag name (text), which is what the id attribute is
        //and b) identifiying elements by the tag text can sometimes break, especially in the special cases where there are characters that need encoding.
        //
        //The real question is since that tags have UUIDs, why aren't they simply removed in that manner. In the add, the dom element isn't added until success
        //so the ID could be returned there and added into the elements as needed.
        var tagDataId = $(this).attr("data-id");
        console.log("data-id to remove is " + tagDataId);

        var request = jsRoutes.api.Datasets.removeTags('"""),_display_(Seq[Any](/*86.58*/dataset/*86.65*/.id)),format.raw/*86.68*/("""').ajax("""),format.raw/*86.76*/("""{"""),format.raw/*86.77*/("""
            data: JSON.stringify("""),format.raw/*87.34*/("""{"""),format.raw/*87.35*/(""""tags":[tagId]"""),format.raw/*87.49*/("""}"""),format.raw/*87.50*/("""),
            type: 'POST',
            contentType: "application/json"
        """),format.raw/*90.9*/("""}"""),format.raw/*90.10*/(""");

        request.done(function (response, textStatus, jqXHR) """),format.raw/*92.61*/("""{"""),format.raw/*92.62*/("""
            console.log("Response " + textStatus);
            $("[data-id=" + tagDataId + "].tag").remove();
        """),format.raw/*95.9*/("""}"""),format.raw/*95.10*/(""");

        request.fail(function (jqXHR, textStatus, errorThrown) """),format.raw/*97.64*/("""{"""),format.raw/*97.65*/("""
            console.error("The following error occured: " + textStatus, errorThrown);
            var errMsg = "You must be logged in to remove a tag from a dataset.";
            if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*100.56*/("""{"""),format.raw/*100.57*/("""
                notify("The tag was not removed from the dataset due to : " + errorThrown, "error");
            """),format.raw/*102.13*/("""}"""),format.raw/*102.14*/("""
        """),format.raw/*103.9*/("""}"""),format.raw/*103.10*/(""");

        return false;
    """),format.raw/*106.5*/("""}"""),format.raw/*106.6*/("""

    $(function() """),format.raw/*108.18*/("""{"""),format.raw/*108.19*/("""

        var tmpId = 1;

        $("#tagList").find(".glyphicon-remove").click(removeTag);

        $('#tagB').click(function() """),format.raw/*114.37*/("""{"""),format.raw/*114.38*/("""
            var tag = $('#tagField').val();
            tag = htmlEncode(tag);

            var isTagPresent = false;
            $("#tagList").children("div").each(function (index, tagLi) """),format.raw/*119.72*/("""{"""),format.raw/*119.73*/("""
                if($(tagLi).attr("name")===tag)"""),format.raw/*120.48*/("""{"""),format.raw/*120.49*/("""
                    isTagPresent = true;
                """),format.raw/*122.17*/("""}"""),format.raw/*122.18*/("""
            """),format.raw/*123.13*/("""}"""),format.raw/*123.14*/(""");

            if (tag !== "" && isTagPresent != true) """),format.raw/*125.53*/("""{"""),format.raw/*125.54*/("""
                console.log("submitting tag " + tag);
                var request = jsRoutes.api.Datasets.addTags('"""),_display_(Seq[Any](/*127.63*/dataset/*127.70*/.id)),format.raw/*127.73*/("""').ajax("""),format.raw/*127.81*/("""{"""),format.raw/*127.82*/("""
                    data: JSON.stringify("""),format.raw/*128.42*/("""{"""),format.raw/*128.43*/(""""tags":[tag]"""),format.raw/*128.55*/("""}"""),format.raw/*128.56*/("""),
                    type: 'POST',
                    contentType: "application/json"
                """),format.raw/*131.17*/("""}"""),format.raw/*131.18*/(""");

                request.done(function (response, textStatus, jqXHR)"""),format.raw/*133.68*/("""{"""),format.raw/*133.69*/("""
                    console.log("Response " + response);
                    var url = jsRoutes.controllers.Tags.search(tag).url;
                    //The data-id attribute is needed since a) the removeTag for some reason is based off the tag name (text), which is what the id attribute is
                    //and b) identifiying elements by the tag text can sometimes break, especially in the special cases where there are characters that need encoding.
                    //
                    //The real question is since that tags have UUIDs, why aren't they simply removed in that manner. In the add, the dom element isn't added until success
                    //so the ID could be returned there and added into the elements as needed.
                    $newTag = $("<div id='"+htmlEncode(tag)+"' class='tag' data-id='newId"+tmpId+"'><a href='" + url + "'>" + tag + "</a><a href='#'> <span id='" + htmlEncode(tag) +"' data-id='newId" + tmpId + "' class='glyphicon glyphicon-remove tag-delete'></span></a></div>").appendTo('#tagList');
                    tmpId++;
                    $newTag.find(".glyphicon-remove").click(removeTag);
                    $('#tagField').val("");
                """),format.raw/*145.17*/("""}"""),format.raw/*145.18*/(""");

                request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*147.71*/("""{"""),format.raw/*147.72*/("""
                    console.error("The following error occured: "+textStatus, errorThrown);
                    var errMsg = "You must be logged in to add a tag to a dataset.";
                    if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*150.64*/("""{"""),format.raw/*150.65*/("""
                        notify("The tag was not added to the dataset due to : " + errorThrown, "error");
                    """),format.raw/*152.21*/("""}"""),format.raw/*152.22*/("""
                """),format.raw/*153.17*/("""}"""),format.raw/*153.18*/(""");
                return false;
            """),format.raw/*155.13*/("""}"""),format.raw/*155.14*/("""
        """),format.raw/*156.9*/("""}"""),format.raw/*156.10*/(""");

        $('#tagField').keypress(function (e) """),format.raw/*158.46*/("""{"""),format.raw/*158.47*/("""
            if (e.which == 13) """),format.raw/*159.32*/("""{"""),format.raw/*159.33*/("""
                console.log("enter");
                $('#tagB').click();
                return false;
            """),format.raw/*163.13*/("""}"""),format.raw/*163.14*/("""
        """),format.raw/*164.9*/("""}"""),format.raw/*164.10*/(""");

    """),format.raw/*166.5*/("""}"""),format.raw/*166.6*/(""");
</script>
"""))}
    }
    
    def render(dataset:Dataset,fileList:List[File],filesTags:Set[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,fileList,filesTags)(user)
    
    def f:((Dataset,List[File],Set[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,fileList,filesTags) => (user) => apply(dataset,fileList,filesTags)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/tags.scala.html
                    HASH: 47542a9c041fae840638aa86f19b063dca1beec7
                    MATRIX: 639->1|886->101|914->156|1169->376|1275->473|1316->475|1767->890|1783->897|1801->906|1848->915|1907->938|1919->941|1946->946|2006->970|2018->973|2043->976|2108->1005|2123->1011|2167->1033|2206->1036|2242->1050|2427->1199|2535->1297|2576->1299|2657->1344|2669->1347|2696->1352|2744->1364|2756->1367|2781->1370|2890->1447|2951->1476|3010->1504|3266->1724|3371->1819|3412->1821|3584->1957|3597->1961|3686->2027|4104->2427|4117->2432|4156->2433|4727->2972|4783->2993|4808->3009|4848->3011|4968->3100|5006->3103|5033->3121|5073->3123|5193->3212|5390->3381|5419->3382|6265->4192|6281->4199|6306->4202|6342->4210|6371->4211|6433->4245|6462->4246|6504->4260|6533->4261|6641->4342|6670->4343|6762->4407|6791->4408|6937->4527|6966->4528|7061->4595|7090->4596|7343->4820|7373->4821|7516->4935|7546->4936|7583->4945|7613->4946|7671->4976|7700->4977|7748->4996|7778->4997|7936->5126|7966->5127|8185->5317|8215->5318|8292->5366|8322->5367|8409->5425|8439->5426|8481->5439|8511->5440|8596->5496|8626->5497|8780->5614|8797->5621|8823->5624|8860->5632|8890->5633|8961->5675|8991->5676|9032->5688|9062->5689|9196->5794|9226->5795|9326->5866|9356->5867|10596->7078|10626->7079|10729->7153|10759->7154|11029->7395|11059->7396|11214->7522|11244->7523|11290->7540|11320->7541|11394->7586|11424->7587|11461->7596|11491->7597|11569->7646|11599->7647|11660->7679|11690->7680|11836->7797|11866->7798|11903->7807|11933->7808|11969->7816|11998->7817
                    LINES: 20->1|26->1|28->5|36->13|36->13|36->13|42->19|42->19|42->19|42->19|43->20|43->20|43->20|43->20|43->20|43->20|44->21|44->21|44->21|44->21|44->21|46->23|46->23|46->23|47->24|47->24|47->24|47->24|47->24|47->24|48->25|50->27|53->30|58->35|58->35|58->35|61->38|61->38|61->38|69->46|69->46|69->46|80->57|84->61|84->61|84->61|88->65|90->67|90->67|90->67|94->71|98->75|98->75|109->86|109->86|109->86|109->86|109->86|110->87|110->87|110->87|110->87|113->90|113->90|115->92|115->92|118->95|118->95|120->97|120->97|123->100|123->100|125->102|125->102|126->103|126->103|129->106|129->106|131->108|131->108|137->114|137->114|142->119|142->119|143->120|143->120|145->122|145->122|146->123|146->123|148->125|148->125|150->127|150->127|150->127|150->127|150->127|151->128|151->128|151->128|151->128|154->131|154->131|156->133|156->133|168->145|168->145|170->147|170->147|173->150|173->150|175->152|175->152|176->153|176->153|178->155|178->155|179->156|179->156|181->158|181->158|182->159|182->159|186->163|186->163|187->164|187->164|189->166|189->166
                    -- GENERATED --
                */
            