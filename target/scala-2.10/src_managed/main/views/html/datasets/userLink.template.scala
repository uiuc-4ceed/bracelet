
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object userLink extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Dataset,play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset)(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.83*/("""

<a id="dataset-users" class="btn btn-link" href=""""),_display_(Seq[Any](/*3.51*/routes/*3.57*/.Datasets.users(dataset.id))),format.raw/*3.84*/("""">
    <span class="glyphicon glyphicon-user"></span> Collaborators
</a>

"""))}
    }
    
    def render(dataset:Dataset,flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset)(flash,user)
    
    def f:((Dataset) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset) => (flash,user) => apply(dataset)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/userLink.scala.html
                    HASH: 8438994ad7cb6a257b64b3436b765616600c5d74
                    MATRIX: 639->1|814->82|901->134|915->140|963->167
                    LINES: 20->1|23->1|25->3|25->3|25->3
                    -- GENERATED --
                */
            