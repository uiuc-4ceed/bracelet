
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object create extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template10[List[models.ProjectSpace],Boolean,Boolean,Option[String],Option[String],Option[Collection],List[String],Boolean,play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(spaceList: List[models.ProjectSpace], isNameRequired: Boolean, isDescriptionRequired: Boolean, spaceId: Option[String], spaceName: Option[String], collection: Option[Collection],collectionSpaces : List[String], showAccess: Boolean)(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import models.DatasetStatus

import play.api.i18n.Messages

import _root_.util.Formatters._


Seq[Any](format.raw/*1.297*/("""

"""),format.raw/*6.1*/("""
"""),_display_(Seq[Any](/*7.2*/main(Messages("create.title", Messages("dataset.title")))/*7.59*/ {_display_(Seq[Any](format.raw/*7.61*/("""


    <!-- Custom items for the create dataset workflow -->
    <script src=""""),_display_(Seq[Any](/*11.19*/routes/*11.25*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*11.70*/("""" language="javascript"></script>
    <script src=""""),_display_(Seq[Any](/*12.19*/routes/*12.25*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*12.67*/("""" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
            //Global so that the javascript for the dataset creation can reference this.
            var isNameRequired = """),_display_(Seq[Any](/*16.35*/isNameRequired)),format.raw/*16.49*/(""";
            var isDescRequired = """),_display_(Seq[Any](/*17.35*/isDescriptionRequired)),format.raw/*17.56*/(""";
    </script>
    <div>
        <ol class="breadcrumb navigate-bread">
            """),_display_(Seq[Any](/*21.14*/(spaceId, spaceName)/*21.34*/ match/*21.40*/ {/*22.17*/case (Some(id), Some(name)) =>/*22.47*/ {_display_(Seq[Any](format.raw/*22.49*/("""
                    <li><span class="glyphicon glyphicon-hdd"></span> <a href=""""),_display_(Seq[Any](/*23.81*/routes/*23.87*/.Spaces.getSpace(UUID(id)))),format.raw/*23.113*/("""" title=""""),_display_(Seq[Any](/*23.123*/name)),format.raw/*23.127*/(""""> """),_display_(Seq[Any](/*23.131*/Html(ellipsize(name, 18)))),format.raw/*23.156*/("""</a></li>
                    """),_display_(Seq[Any](/*24.22*/collection/*24.32*/ match/*24.38*/ {/*25.25*/case Some(coll) =>/*25.43*/ {_display_(Seq[Any](format.raw/*25.45*/("""
                            <li><span class="glyphicon glyphicon-th-large"></span><a href=""""),_display_(Seq[Any](/*26.93*/routes/*26.99*/.Collections.collection(coll.id))),format.raw/*26.131*/("""" title=""""),_display_(Seq[Any](/*26.141*/name)),format.raw/*26.145*/(""""> """),_display_(Seq[Any](/*26.149*/Html(ellipsize(coll.name,18)))),format.raw/*26.178*/("""</a></li>

                        """)))}/*29.25*/case None =>/*29.37*/ {}})),format.raw/*30.23*/("""
                """)))}/*32.17*/case (_, _) =>/*32.31*/ {_display_(Seq[Any](format.raw/*32.33*/("""
                    """),_display_(Seq[Any](/*33.22*/collection/*33.32*/ match/*33.38*/ {/*34.25*/case Some(coll) =>/*34.43*/ {_display_(Seq[Any](format.raw/*34.45*/("""
                           <li> <span class="glyphicon glyphicon-th-large"></span><a href=""""),_display_(Seq[Any](/*35.93*/routes/*35.99*/.Collections.collection(coll.id))),format.raw/*35.131*/("""" title=""""),_display_(Seq[Any](/*35.141*/coll/*35.145*/.name)),format.raw/*35.150*/(""""> """),_display_(Seq[Any](/*35.154*/Html(ellipsize(coll.name, 18)))),format.raw/*35.184*/("""</a></li>
                        """)))}/*37.25*/case None =>/*37.37*/ {_display_(Seq[Any](format.raw/*37.39*/("""
                            """),_display_(Seq[Any](/*38.30*/user/*38.34*/ match/*38.40*/ {/*39.33*/case Some(u) =>/*39.48*/ {_display_(Seq[Any](format.raw/*39.50*/("""<li> <span class="glyphicon glyphicon-user"></span> <a href= """"),_display_(Seq[Any](/*39.113*/routes/*39.119*/.Profile.viewProfileUUID(u.id))),format.raw/*39.149*/(""""> """),_display_(Seq[Any](/*39.153*/Html(u.fullName))),format.raw/*39.169*/(""" </a></li>""")))}/*40.33*/case None =>/*40.45*/ {}})),format.raw/*41.30*/("""
                        """)))}})),format.raw/*43.22*/("""

                """)))}})),format.raw/*46.14*/("""
           <li><span class="glyphicon glyphicon-briefcase"></span> """),_display_(Seq[Any](/*47.69*/Messages("create.header", Messages("dataset.title")))),format.raw/*47.121*/("""</li>
        </ol>
    </div>
    <div class="page-header">
        <h1>"""),_display_(Seq[Any](/*51.14*/Messages("create.header", Messages("dataset.title")))),format.raw/*51.66*/("""</h1>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>"""),_display_(Seq[Any](/*55.17*/Messages("dataset.create.message", Messages("datasets.title"), Messages("dataset.title").toLowerCase, Messages("space.title")))),format.raw/*55.143*/("""</p>

            """),_display_(Seq[Any](/*57.14*/if(collection.isDefined)/*57.38*/{_display_(Seq[Any](format.raw/*57.39*/("""
                <p>"""),_display_(Seq[Any](/*58.21*/Messages("dataset.create.collection.message", Messages("dataset.title").toLowerCase, Messages("collection.title").toLowerCase))),format.raw/*58.147*/(""" <a href=""""),_display_(Seq[Any](/*58.158*/routes/*58.164*/.Collections.collection(collection.get.id))),format.raw/*58.206*/("""">"""),_display_(Seq[Any](/*58.209*/collection/*58.219*/.get.name)),format.raw/*58.228*/("""</a></p>
            """)))})),format.raw/*59.14*/("""
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div>
                <span id="status" class="success hiddencomplete alert alert-success" role="alert">A Status Message</span>
                <span class="error hiddencomplete alert alert-danger" id="messageerror">An Error Message</span>
            </div>
            <div class="form-group">
                <label id="namelabel" for="name">Name</label>
                <input type="text" class="form-control" id="name" placeholder="A short name">
                <span class="error hiddencomplete" id="nameerror">The name is a required field</span>
            </div>
            <div class="form-group">
                <label id="desclabel" for="description">Description</label>
                <textarea cols=40 rows=4 type="text" id="description" class="form-control"
                    placeholder="A longer description" style="height: 54px;"></textarea>
                <span class="error hiddencomplete" id="descerror">This description is a required field</span>
            </div>
            <div class="form-group">
                <label id="spacelabel" for="space">Share with
                    """),_display_(Seq[Any](/*81.22*/if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*81.93*/ {_display_(Seq[Any](format.raw/*81.95*/("""
                        """),_display_(Seq[Any](/*82.26*/Messages("spaces.title"))),format.raw/*82.50*/("""
                     """)))}/*83.24*/else/*83.29*/{_display_(Seq[Any](format.raw/*83.30*/("""
                        a """),_display_(Seq[Any](/*84.28*/Messages("space.title"))),format.raw/*84.51*/("""
                    """)))})),format.raw/*85.22*/("""
                </label>
                <select name="space" id="spaceid" class ="chosen-select" """),_display_(Seq[Any](/*87.75*/if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*87.146*/{_display_(Seq[Any](format.raw/*87.147*/("""multiple""")))})),format.raw/*87.156*/(""">
                """),_display_(Seq[Any](/*88.18*/if(!play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*88.90*/ {_display_(Seq[Any](format.raw/*88.92*/("""
                    <option value="">Select a """),_display_(Seq[Any](/*89.48*/Messages("space.title"))),format.raw/*89.71*/(""" to share the """),_display_(Seq[Any](/*89.86*/Messages("dataset.title")/*89.111*/.toLowerCase)),format.raw/*89.123*/(""" with (Optional)</option>
                """)))})),format.raw/*90.18*/("""
                """),_display_(Seq[Any](/*91.18*/spaceList/*91.27*/.map/*91.31*/ { space =>_display_(Seq[Any](format.raw/*91.42*/("""
                        <option id=""""),_display_(Seq[Any](/*92.38*/(space.id))),format.raw/*92.48*/("""" value=""""),_display_(Seq[Any](/*92.58*/(space.id))),format.raw/*92.68*/("""">"""),_display_(Seq[Any](/*92.71*/(space.name))),format.raw/*92.83*/("""</option>
                    """)))})),format.raw/*93.22*/("""
                    </select>
                """),_display_(Seq[Any](/*95.18*/if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*95.89*/ {_display_(Seq[Any](format.raw/*95.91*/("""
                    <p class="help-block">"""),_display_(Seq[Any](/*96.44*/Messages("create.share.multiple.message", Messages("dataset.title").toLowerCase, Messages("spaces.title")))),format.raw/*96.150*/("""</p>
                """)))}/*97.19*/else/*97.24*/{_display_(Seq[Any](format.raw/*97.25*/("""
                    <p class="help-block">"""),_display_(Seq[Any](/*98.44*/Messages("create.share.one.message", Messages("dataset.title").toLowerCase, Messages("space.title")))),format.raw/*98.144*/("""</p>
                """)))})),format.raw/*99.18*/("""

            </div>
            """),_display_(Seq[Any](/*102.14*/if(showAccess)/*102.28*/ {_display_(Seq[Any](format.raw/*102.30*/("""
                <div class="form-group">Access:
                    <label class="radio-inline"><input type="radio" name="access" id="access-default" value=""""),_display_(Seq[Any](/*104.111*/DatasetStatus/*104.124*/.DEFAULT.toString)),format.raw/*104.141*/("""" checked>Default</label>
                    <label class="radio-inline"><input type="radio" name="access" id="access-private" value=""""),_display_(Seq[Any](/*105.111*/DatasetStatus/*105.124*/.PRIVATE.toString)),format.raw/*105.141*/("""">Private</label>
                    <label class="radio-inline"><input type="radio" name="access" id="access-public" value=""""),_display_(Seq[Any](/*106.110*/DatasetStatus/*106.123*/.PUBLIC.toString)),format.raw/*106.139*/("""">Public</label>
                </div>
            """)))})),format.raw/*108.14*/("""

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary start"  id="createdataset" title="Create the Dataset">
                <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*115.63*/Messages("create.title", ""))),format.raw/*115.91*/("""
            </button>
            <button class="btn btn-default" title="Start the dataset creation process over" onclick="return resetValues();">
                <span class="glyphicon glyphicon-unchecked"></span> Reset
            </button>
        </div>
    </div>

    <script src="""),_display_(Seq[Any](/*123.18*/routes/*123.24*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*123.66*/(""" type="text/javascript"></script>
    <link rel="stylesheet" href="""),_display_(Seq[Any](/*124.34*/routes/*124.40*/.Assets.at("stylesheets/chosen.css"))),format.raw/*124.76*/(""">

    <script language = "javascript">
        $(".chosen-select").chosen("""),format.raw/*127.36*/("""{"""),format.raw/*127.37*/("""
            width: "100%",
            placeholder_text_multiple: "(optional)"
        """),format.raw/*130.9*/("""}"""),format.raw/*130.10*/(""")
    </script>

    <script language = "javascript">

        function resetValues() """),format.raw/*135.32*/("""{"""),format.raw/*135.33*/("""
            $('#name').val("");
            $('#description').val("");
            $('#spaceid').val('').trigger('chosen:updated');
        """),format.raw/*139.9*/("""}"""),format.raw/*139.10*/("""

        var idNameObj = """),format.raw/*141.25*/("""{"""),format.raw/*141.26*/("""}"""),format.raw/*141.27*/(""";

        function isUndefined(value) """),format.raw/*143.37*/("""{"""),format.raw/*143.38*/("""
            return typeof value === 'undefined';
        """),format.raw/*145.9*/("""}"""),format.raw/*145.10*/("""

        function getName(id) """),format.raw/*147.30*/("""{"""),format.raw/*147.31*/("""
            return idNameObj[id];
        """),format.raw/*149.9*/("""}"""),format.raw/*149.10*/("""

        $(document).ready(function() """),format.raw/*151.38*/("""{"""),format.raw/*151.39*/("""
            """),_display_(Seq[Any](/*152.14*/for(space <- spaceList) yield /*152.37*/ {_display_(Seq[Any](format.raw/*152.39*/("""
                idNameObj[""""),_display_(Seq[Any](/*153.29*/space/*153.34*/.name)),format.raw/*153.39*/(""""] = """"),_display_(Seq[Any](/*153.46*/space/*153.51*/.id)),format.raw/*153.54*/("""";
            """)))})),format.raw/*154.14*/("""
             $(".chosen-choices").addClass("form-control");
        """),format.raw/*156.9*/("""}"""),format.raw/*156.10*/(""");

        $(".chosen-select").chosen().change(function(event, params) """),format.raw/*158.69*/("""{"""),format.raw/*158.70*/("""
            var targetId = this.getAttribute("id");
            if(!isUndefined(params.selected)) """),format.raw/*160.47*/("""{"""),format.raw/*160.48*/("""
                var addedId = params.selected;
                """),_display_(Seq[Any](/*162.18*/for(space <- spaceList) yield /*162.41*/ {_display_(Seq[Any](format.raw/*162.43*/("""
                var currentId = """"),_display_(Seq[Any](/*163.35*/space/*163.40*/.id)),format.raw/*163.43*/("""";
                if(currentId != targetId) """),format.raw/*164.43*/("""{"""),format.raw/*164.44*/("""
                    $('#"""),_display_(Seq[Any](/*165.26*/space/*165.31*/.id)),format.raw/*165.34*/(""" option [value='+addedId+']').remove();
                    $('#"""),_display_(Seq[Any](/*166.26*/space/*166.31*/.id)),format.raw/*166.34*/("""').trigger("chosen:updated");

                """),format.raw/*168.17*/("""}"""),format.raw/*168.18*/("""
                """)))})),format.raw/*169.18*/("""
                $('ul.chosen-choices li.search-field').css("width", "0");
            """),format.raw/*171.13*/("""}"""),format.raw/*171.14*/(""" else if(!isUndefined(params.deselected)) """),format.raw/*171.56*/("""{"""),format.raw/*171.57*/("""
                var removedId = params.deselected;
                """),_display_(Seq[Any](/*173.18*/for(space <- spaceList) yield /*173.41*/ {_display_(Seq[Any](format.raw/*173.43*/("""
                    var currentId = """"),_display_(Seq[Any](/*174.39*/space/*174.44*/.id)),format.raw/*174.47*/("""";
                    if(currentId != targetId) """),format.raw/*175.47*/("""{"""),format.raw/*175.48*/("""
                        $('#"""),_display_(Seq[Any](/*176.30*/space/*176.35*/.id)),format.raw/*176.38*/("""').prepend($("<option></option>").attr("value", removedId).text(getName(removedId)));
                        $('#"""),_display_(Seq[Any](/*177.30*/space/*177.35*/.id)),format.raw/*177.38*/("""').trigger("chosen:updated");
                    """),format.raw/*178.21*/("""}"""),format.raw/*178.22*/("""
                """)))})),format.raw/*179.18*/("""
            """),format.raw/*180.13*/("""}"""),format.raw/*180.14*/("""
        """),format.raw/*181.9*/("""}"""),format.raw/*181.10*/(""");

        if("""),_display_(Seq[Any](/*183.13*/spaceId/*183.20*/.isDefined)),format.raw/*183.30*/(""") """),format.raw/*183.32*/("""{"""),format.raw/*183.33*/("""
            $('#"""),_display_(Seq[Any](/*184.18*/spaceId)),format.raw/*184.25*/("""').prop('selected','selected');
            $('#spaceid').trigger("chosen:updated");
        """),format.raw/*186.9*/("""}"""),format.raw/*186.10*/("""

        //todd_n add for loop to select id
        if(!"""),_display_(Seq[Any](/*189.14*/collectionSpaces/*189.30*/.isEmpty)),format.raw/*189.38*/(""")"""),format.raw/*189.39*/("""{"""),format.raw/*189.40*/("""
            """),_display_(Seq[Any](/*190.14*/for(eachId <- collectionSpaces) yield /*190.45*/{_display_(Seq[Any](format.raw/*190.46*/("""
            $('#"""),_display_(Seq[Any](/*191.18*/eachId)),format.raw/*191.24*/("""').prop('selected','selected');
            $('#spaceid').trigger("chosen:updated");
            """)))})),format.raw/*193.14*/("""
        """),format.raw/*194.9*/("""}"""),format.raw/*194.10*/("""


        $(function () """),format.raw/*197.23*/("""{"""),format.raw/*197.24*/("""
            $('#createdataset').click(function () """),format.raw/*198.51*/("""{"""),format.raw/*198.52*/("""
                var name = $('#name');
                var desc = $('#description');
                var spaceList = [];
                $('#spaceid').find(":selected").each(function(i, selected) """),format.raw/*202.76*/("""{"""),format.raw/*202.77*/("""
                    if($(selected).val() != "") """),format.raw/*203.49*/("""{"""),format.raw/*203.50*/("""
                        spaceList[i] = $(selected).val();
                    """),format.raw/*205.21*/("""}"""),format.raw/*205.22*/("""
                """),format.raw/*206.17*/("""}"""),format.raw/*206.18*/(""");
                // In case we want choose multiple collections for dataset like spaces in this page, collection is stored in a list here
                var collectionList = [];
                """),_display_(Seq[Any](/*209.18*/if(collection.isDefined)/*209.42*/ {_display_(Seq[Any](format.raw/*209.44*/("""
                    collectionList[0] = '"""),_display_(Seq[Any](/*210.43*/collection/*210.53*/.get.id)),format.raw/*210.60*/("""';
                """)))})),format.raw/*211.18*/("""
                var error = false;
                if (!name.val() && isNameRequired) """),format.raw/*213.52*/("""{"""),format.raw/*213.53*/("""
                    $('#nameerror').show();
                    error = true;
                """),format.raw/*216.17*/("""}"""),format.raw/*216.18*/("""
                if (!desc.val() && isDescRequired) """),format.raw/*217.52*/("""{"""),format.raw/*217.53*/("""
                    $('#descerror').show();
                    error = true;
                """),format.raw/*220.17*/("""}"""),format.raw/*220.18*/("""
                var access = $("input[name='access']:checked").val();

                if (!error) """),format.raw/*223.29*/("""{"""),format.raw/*223.30*/("""
                    var encName = htmlEncode(name.val());
                    var encDescription = htmlEncode(desc.val());
                    var jsonData = JSON.stringify("""),format.raw/*226.51*/("""{"""),format.raw/*226.52*/(""""name": encName, "description": encDescription, "space": spaceList, "collection": collectionList, "access":access"""),format.raw/*226.165*/("""}"""),format.raw/*226.166*/(""");
                    var request = jsRoutes.api.Datasets.createEmptyDataset().ajax("""),format.raw/*227.83*/("""{"""),format.raw/*227.84*/("""
                        data: jsonData,
                        type: 'POST',
                        contentType: "application/json"
                    """),format.raw/*231.21*/("""}"""),format.raw/*231.22*/(""");

                    request.done(function (response, textStatus, jqXHR)"""),format.raw/*233.72*/("""{"""),format.raw/*233.73*/("""
                        location.href = jsRoutes.controllers.Datasets.createStep2(response["id"]).url;
                    """),format.raw/*235.21*/("""}"""),format.raw/*235.22*/(""");

                    request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*237.75*/("""{"""),format.raw/*237.76*/("""
                        console.error("The following error occured: " + textStatus, errorThrown);
                        var errMsg = "You must be logged in to create a new dataset.";
                        if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*240.68*/("""{"""),format.raw/*240.69*/("""
                            notify("Error in creating dataset. : " + errorThrown, "error");
                        """),format.raw/*242.25*/("""}"""),format.raw/*242.26*/("""
                    """),format.raw/*243.21*/("""}"""),format.raw/*243.22*/(""");
                    return false;
                """),format.raw/*245.17*/("""}"""),format.raw/*245.18*/("""
            """),format.raw/*246.13*/("""}"""),format.raw/*246.14*/(""");
        """),format.raw/*247.9*/("""}"""),format.raw/*247.10*/(""");
    </script>
""")))})))}
    }
    
    def render(spaceList:List[models.ProjectSpace],isNameRequired:Boolean,isDescriptionRequired:Boolean,spaceId:Option[String],spaceName:Option[String],collection:Option[Collection],collectionSpaces:List[String],showAccess:Boolean,flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(spaceList,isNameRequired,isDescriptionRequired,spaceId,spaceName,collection,collectionSpaces,showAccess)(flash,user)
    
    def f:((List[models.ProjectSpace],Boolean,Boolean,Option[String],Option[String],Option[Collection],List[String],Boolean) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (spaceList,isNameRequired,isDescriptionRequired,spaceId,spaceName,collection,collectionSpaces,showAccess) => (flash,user) => apply(spaceList,isNameRequired,isDescriptionRequired,spaceId,spaceName,collection,collectionSpaces,showAccess)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:49 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/create.scala.html
                    HASH: 9c3e09f9b49452d14a2c9dea8599d7936d06fc28
                    MATRIX: 742->1|1224->296|1252->391|1288->393|1353->450|1392->452|1507->531|1522->537|1589->582|1677->634|1692->640|1756->682|2009->899|2045->913|2117->949|2160->970|2282->1056|2311->1076|2326->1082|2337->1101|2376->1131|2416->1133|2533->1214|2548->1220|2597->1246|2644->1256|2671->1260|2712->1264|2760->1289|2827->1320|2846->1330|2861->1336|2872->1363|2899->1381|2939->1383|3068->1476|3083->1482|3138->1514|3185->1524|3212->1528|3253->1532|3305->1561|3360->1622|3381->1634|3407->1660|3444->1695|3467->1709|3507->1711|3565->1733|3584->1743|3599->1749|3610->1776|3637->1794|3677->1796|3806->1889|3821->1895|3876->1927|3923->1937|3937->1941|3965->1946|4006->1950|4059->1980|4113->2040|4134->2052|4174->2054|4240->2084|4253->2088|4268->2094|4279->2129|4303->2144|4343->2146|4443->2209|4459->2215|4512->2245|4553->2249|4592->2265|4622->2309|4643->2321|4669->2354|4728->2402|4780->2435|4885->2504|4960->2556|5070->2630|5144->2682|5267->2769|5416->2895|5471->2914|5504->2938|5543->2939|5600->2960|5749->3086|5797->3097|5813->3103|5878->3145|5918->3148|5938->3158|5970->3167|6024->3189|7268->4397|7348->4468|7388->4470|7450->4496|7496->4520|7538->4544|7551->4549|7590->4550|7654->4578|7699->4601|7753->4623|7889->4723|7970->4794|8010->4795|8052->4804|8107->4823|8188->4895|8228->4897|8312->4945|8357->4968|8408->4983|8443->5008|8478->5020|8553->5063|8607->5081|8625->5090|8638->5094|8687->5105|8761->5143|8793->5153|8839->5163|8871->5173|8910->5176|8944->5188|9007->5219|9091->5267|9171->5338|9211->5340|9291->5384|9420->5490|9461->5513|9474->5518|9513->5519|9593->5563|9716->5663|9770->5685|9841->5719|9865->5733|9906->5735|10103->5894|10127->5907|10168->5924|10342->6060|10366->6073|10407->6090|10572->6217|10596->6230|10636->6246|10722->6299|11015->6555|11066->6583|11391->6871|11407->6877|11472->6919|11576->6986|11592->6992|11651->7028|11755->7103|11785->7104|11901->7192|11931->7193|12046->7279|12076->7280|12245->7421|12275->7422|12330->7448|12360->7449|12390->7450|12458->7489|12488->7490|12574->7548|12604->7549|12664->7580|12694->7581|12765->7624|12795->7625|12863->7664|12893->7665|12944->7679|12984->7702|13025->7704|13091->7733|13106->7738|13134->7743|13178->7750|13193->7755|13219->7758|13268->7774|13365->7843|13395->7844|13496->7916|13526->7917|13654->8016|13684->8017|13786->8082|13826->8105|13867->8107|13939->8142|13954->8147|13980->8150|14054->8195|14084->8196|14147->8222|14162->8227|14188->8230|14290->8295|14305->8300|14331->8303|14407->8350|14437->8351|14488->8369|14604->8456|14634->8457|14705->8499|14735->8500|14841->8569|14881->8592|14922->8594|14998->8633|15013->8638|15039->8641|15117->8690|15147->8691|15214->8721|15229->8726|15255->8729|15407->8844|15422->8849|15448->8852|15527->8902|15557->8903|15608->8921|15650->8934|15680->8935|15717->8944|15747->8945|15800->8961|15817->8968|15850->8978|15881->8980|15911->8981|15966->8999|15996->9006|16117->9099|16147->9100|16242->9158|16268->9174|16299->9182|16329->9183|16359->9184|16410->9198|16458->9229|16498->9230|16553->9248|16582->9254|16713->9352|16750->9361|16780->9362|16834->9387|16864->9388|16944->9439|16974->9440|17200->9637|17230->9638|17308->9687|17338->9688|17446->9767|17476->9768|17522->9785|17552->9786|17787->9984|17821->10008|17862->10010|17942->10053|17962->10063|17992->10070|18045->10090|18161->10177|18191->10178|18315->10273|18345->10274|18426->10326|18456->10327|18580->10422|18610->10423|18739->10523|18769->10524|18972->10698|19002->10699|19145->10812|19176->10813|19290->10898|19320->10899|19504->11054|19534->11055|19638->11130|19668->11131|19821->11255|19851->11256|19958->11334|19988->11335|20270->11588|20300->11589|20446->11706|20476->11707|20526->11728|20556->11729|20638->11782|20668->11783|20710->11796|20740->11797|20779->11808|20809->11809
                    LINES: 20->1|28->1|30->6|31->7|31->7|31->7|35->11|35->11|35->11|36->12|36->12|36->12|40->16|40->16|41->17|41->17|45->21|45->21|45->21|45->22|45->22|45->22|46->23|46->23|46->23|46->23|46->23|46->23|46->23|47->24|47->24|47->24|47->25|47->25|47->25|48->26|48->26|48->26|48->26|48->26|48->26|48->26|50->29|50->29|50->30|51->32|51->32|51->32|52->33|52->33|52->33|52->34|52->34|52->34|53->35|53->35|53->35|53->35|53->35|53->35|53->35|53->35|54->37|54->37|54->37|55->38|55->38|55->38|55->39|55->39|55->39|55->39|55->39|55->39|55->39|55->39|55->40|55->40|55->41|56->43|58->46|59->47|59->47|63->51|63->51|67->55|67->55|69->57|69->57|69->57|70->58|70->58|70->58|70->58|70->58|70->58|70->58|70->58|71->59|93->81|93->81|93->81|94->82|94->82|95->83|95->83|95->83|96->84|96->84|97->85|99->87|99->87|99->87|99->87|100->88|100->88|100->88|101->89|101->89|101->89|101->89|101->89|102->90|103->91|103->91|103->91|103->91|104->92|104->92|104->92|104->92|104->92|104->92|105->93|107->95|107->95|107->95|108->96|108->96|109->97|109->97|109->97|110->98|110->98|111->99|114->102|114->102|114->102|116->104|116->104|116->104|117->105|117->105|117->105|118->106|118->106|118->106|120->108|127->115|127->115|135->123|135->123|135->123|136->124|136->124|136->124|139->127|139->127|142->130|142->130|147->135|147->135|151->139|151->139|153->141|153->141|153->141|155->143|155->143|157->145|157->145|159->147|159->147|161->149|161->149|163->151|163->151|164->152|164->152|164->152|165->153|165->153|165->153|165->153|165->153|165->153|166->154|168->156|168->156|170->158|170->158|172->160|172->160|174->162|174->162|174->162|175->163|175->163|175->163|176->164|176->164|177->165|177->165|177->165|178->166|178->166|178->166|180->168|180->168|181->169|183->171|183->171|183->171|183->171|185->173|185->173|185->173|186->174|186->174|186->174|187->175|187->175|188->176|188->176|188->176|189->177|189->177|189->177|190->178|190->178|191->179|192->180|192->180|193->181|193->181|195->183|195->183|195->183|195->183|195->183|196->184|196->184|198->186|198->186|201->189|201->189|201->189|201->189|201->189|202->190|202->190|202->190|203->191|203->191|205->193|206->194|206->194|209->197|209->197|210->198|210->198|214->202|214->202|215->203|215->203|217->205|217->205|218->206|218->206|221->209|221->209|221->209|222->210|222->210|222->210|223->211|225->213|225->213|228->216|228->216|229->217|229->217|232->220|232->220|235->223|235->223|238->226|238->226|238->226|238->226|239->227|239->227|243->231|243->231|245->233|245->233|247->235|247->235|249->237|249->237|252->240|252->240|254->242|254->242|255->243|255->243|257->245|257->245|258->246|258->246|259->247|259->247
                    -- GENERATED --
                */
            