
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object deleteButton extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Dataset,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.56*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/if(user.isDefined)/*5.20*/ {_display_(Seq[Any](format.raw/*5.22*/("""
    <!-- If the user can edit the dataset, the elements are enabled, otherwise they are present but disabled to provide consistent UE. -->
    """),_display_(Seq[Any](/*7.6*/if(Permission.checkPermission(Permission.DeleteDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*7.108*/ {_display_(Seq[Any](format.raw/*7.110*/("""
        <a id="deleteButton" onclick="confirmDeleteResource('dataset','dataset','"""),_display_(Seq[Any](/*8.83*/(dataset.id))),format.raw/*8.95*/("""','"""),_display_(Seq[Any](/*8.99*/(dataset.name.replace("'","&#39;")))),format.raw/*8.134*/("""',true, '"""),_display_(Seq[Any](/*8.144*/(routes.Datasets.list("")))),format.raw/*8.170*/("""')"
            class="btn btn-link" href="#"><span class="glyphicon glyphicon-trash"></span> Delete</a>
    """)))}/*10.7*/else/*10.12*/{_display_(Seq[Any](format.raw/*10.13*/("""
        <a id="deleteButton" class="btn btn-link disabled" href="#"><span class="glyphicon glyphicon-trash"></span> Delete</a>
    """)))})),format.raw/*12.6*/("""
""")))})),format.raw/*13.2*/("""
"""))}
    }
    
    def render(dataset:Dataset,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset)(user)
    
    def f:((Dataset) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset) => (user) => apply(dataset)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/deleteButton.scala.html
                    HASH: 307be93bf70ec6c542fb9bcdb3eb1f50ec21637b
                    MATRIX: 624->1|794->55|822->80|858->82|884->100|923->102|1102->247|1213->349|1253->351|1371->434|1404->446|1443->450|1500->485|1546->495|1594->521|1722->632|1735->637|1774->638|1938->771|1971->773
                    LINES: 20->1|24->1|26->4|27->5|27->5|27->5|29->7|29->7|29->7|30->8|30->8|30->8|30->8|30->8|30->8|32->10|32->10|32->10|34->12|35->13
                    -- GENERATED --
                */
            