
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object uploadFiles extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Dataset,Option[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: Dataset, folderId: Option[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages


Seq[Any](format.raw/*1.82*/("""
"""),format.raw/*3.1*/("""<!--
The sections of this file dealing with the multi file uploader library are loosely based on the demo
of the blueimp jQuery File Upload library. An open source project located here: http://blueimp.github.io/jQuery-File-Upload/
 -->

    <!-- Force latest IE rendering engine or ChromeFrame if installed -->
    <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->

    <!-- blueimp Gallery styles - downloaded to make the resource local -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*14.31*/routes/*14.37*/.Assets.at("stylesheets/file-uploader/blueimp-gallery.min.css"))),format.raw/*14.100*/("""">
    <!-- Generic page styles -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*16.31*/routes/*16.37*/.Assets.at("stylesheets/file-uploader/style.css"))),format.raw/*16.86*/("""">
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*18.31*/routes/*18.37*/.Assets.at("stylesheets/file-uploader/jquery.fileupload.css"))),format.raw/*18.98*/("""">
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*19.31*/routes/*19.37*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-ui.css"))),format.raw/*19.101*/("""">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href=""""),_display_(Seq[Any](/*21.41*/routes/*21.47*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-noscript.css"))),format.raw/*21.117*/(""""></noscript>
<noscript><link rel="stylesheet" href=""""),_display_(Seq[Any](/*22.41*/routes/*22.47*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-ui-noscript.css"))),format.raw/*22.120*/(""""></noscript>


<!-- Custom items for the create dataset workflow -->
<script src=""""),_display_(Seq[Any](/*26.15*/routes/*26.21*/.Assets.at("javascripts/dataset-attach-fileuploader.js"))),format.raw/*26.77*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*27.15*/routes/*27.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*27.63*/("""" type="text/javascript"></script>

<div class="row">
    <div class="col-md-12">
        <p>Select files using the button below or drag files directly from the desktop to this window.
        Then click the upload button to upload them to the """),_display_(Seq[Any](/*32.61*/Messages("dataset.title")/*32.86*/.toLowerCase)),format.raw/*32.98*/(""".</p>
            <!-- The file upload form used as target for the file upload widget -->
        <form id="fileupload" action='"""),_display_(Seq[Any](/*34.40*/routes/*34.46*/.Datasets.submit(folderId))),format.raw/*34.72*/("""' method="POST" enctype="multipart/form-data">
                <!-- Redirect browsers with JavaScript disabled to the origin page -->
            <noscript>Javascript is required in order to use the uploader to create a new dataset.</noscript>

            <input type="hidden" name="datasetid" id="hiddenid" value=""""),_display_(Seq[Any](/*38.73*/dataset/*38.80*/.id)),format.raw/*38.83*/("""">
            <input type="hidden" name="multiple" id="hiddenmt" value=false>
                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
            <div class="row fileupload-buttonbar button-margins upload-files-buttons">
                <div class="col-lg-7">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-default fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Select Files</span>
                            <!-- The file input had "multiple" removed for the current dataset creation method. -->
                        <input type="file" name="files[]" multiple>
                    </span>
                    <button type="submit" class="btn btn-primary start" id="uploadcreate" onclick="checkZeroFiles('"""),_display_(Seq[Any](/*50.117*/dataset/*50.124*/.id)),format.raw/*50.127*/("""',  """),_display_(Seq[Any](/*50.132*/folderId/*50.140*/.isDefined)),format.raw/*50.150*/(""");" title="Upload selected files">
                        <span class="glyphicon glyphicon-upload"></span> Upload
                    </button>
                    <button type="submit" class="btn btn-default cancel">
                        <span class="glyphicon glyphicon-remove"></span> Cancel Upload
                    </button>
                    <!-- The global file processing state -->
                    <span class="fileupload-process"></span>
                </div>
                    <!-- The global progress state -->
                <div class="col-lg-5 fileupload-progress fade">
                        <!-- The global progress bar -->
                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                    </div>
                        <!-- The extended global progress state -->
                    <div class="progress-extended">&nbsp;</div>
                </div>
            </div>
            <div id="ds-files-delete" class="row hidden fileupload-buttonbar">
                <div class="col-lg-12">
                    <label>
                        <input type="checkbox" class="toggle"> Select all
                    </label>
                    <button type="submit" class="btn btn-link delete" style="margin-bottom: 0px">
                        <span class="glyphicon glyphicon-trash"></span> Delete selected
                    </button>
                </div>
            </div>
                <!-- The table listing the files available for upload/download -->
            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
        </form>

            <!-- The blueimp Gallery widget -->
        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>
    </div>
</div>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
    """),format.raw/*98.5*/("""{"""),format.raw/*98.6*/("""% for (var i=0, file; file=o.files[i]; i++) """),format.raw/*98.50*/("""{"""),format.raw/*98.51*/(""" %"""),format.raw/*98.53*/("""}"""),format.raw/*98.54*/("""
        <tr class="template-upload fade">
            <td>
                <span class="preview"></span>
            </td>
            <td>
                <p class="name">"""),format.raw/*104.33*/("""{"""),format.raw/*104.34*/("""%=file.name%"""),format.raw/*104.46*/("""}"""),format.raw/*104.47*/("""</p>
                <strong class="error text-danger"></strong>
            </td>
            <td>
                <p class="size">Processing...</p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            </td>
            <td>
                """),format.raw/*112.17*/("""{"""),format.raw/*112.18*/("""% if (!i && !o.options.autoUpload) """),format.raw/*112.53*/("""{"""),format.raw/*112.54*/(""" %"""),format.raw/*112.56*/("""}"""),format.raw/*112.57*/("""
                    <button class="btn btn-primary start" disabled>
                        <span class="glyphicon glyphicon-upload"></span> Start
                    </button>
                """),format.raw/*116.17*/("""{"""),format.raw/*116.18*/("""% """),format.raw/*116.20*/("""}"""),format.raw/*116.21*/(""" %"""),format.raw/*116.23*/("""}"""),format.raw/*116.24*/("""
                """),format.raw/*117.17*/("""{"""),format.raw/*117.18*/("""% if (!i) """),format.raw/*117.28*/("""{"""),format.raw/*117.29*/(""" %"""),format.raw/*117.31*/("""}"""),format.raw/*117.32*/("""
                    <button type="submit" class="btn btn-default cancel">
                        <span class="glyphicon glyphicon-remove"></span> Cancel
                    </button>
                """),format.raw/*121.17*/("""{"""),format.raw/*121.18*/("""% """),format.raw/*121.20*/("""}"""),format.raw/*121.21*/(""" %"""),format.raw/*121.23*/("""}"""),format.raw/*121.24*/("""
            </td>
        </tr>
    """),format.raw/*124.5*/("""{"""),format.raw/*124.6*/("""% """),format.raw/*124.8*/("""}"""),format.raw/*124.9*/(""" %"""),format.raw/*124.11*/("""}"""),format.raw/*124.12*/("""
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    """),format.raw/*128.5*/("""{"""),format.raw/*128.6*/("""% for (var i=0, file; file=o.files[i]; i++) """),format.raw/*128.50*/("""{"""),format.raw/*128.51*/(""" %"""),format.raw/*128.53*/("""}"""),format.raw/*128.54*/("""
        <tr class="template-download fade">
            """),format.raw/*130.13*/("""{"""),format.raw/*130.14*/("""% if (file.deleteUrl) """),format.raw/*130.36*/("""{"""),format.raw/*130.37*/(""" %"""),format.raw/*130.39*/("""}"""),format.raw/*130.40*/("""
            <td>
                <input type="checkbox" name="delete" value="1" class="toggle">
            </td>
            """),format.raw/*134.13*/("""{"""),format.raw/*134.14*/("""% """),format.raw/*134.16*/("""}"""),format.raw/*134.17*/(""" %"""),format.raw/*134.19*/("""}"""),format.raw/*134.20*/("""
            <td>
                <span class="preview">
                    """),format.raw/*137.21*/("""{"""),format.raw/*137.22*/("""% if (file.thumbnailUrl) """),format.raw/*137.47*/("""{"""),format.raw/*137.48*/(""" %"""),format.raw/*137.50*/("""}"""),format.raw/*137.51*/("""
                        <a href=""""),format.raw/*138.34*/("""{"""),format.raw/*138.35*/("""%=file.url%"""),format.raw/*138.46*/("""}"""),format.raw/*138.47*/("""" title=""""),format.raw/*138.56*/("""{"""),format.raw/*138.57*/("""%=file.name%"""),format.raw/*138.69*/("""}"""),format.raw/*138.70*/("""" download=""""),format.raw/*138.82*/("""{"""),format.raw/*138.83*/("""%=file.name%"""),format.raw/*138.95*/("""}"""),format.raw/*138.96*/("""" data-gallery><img src=""""),format.raw/*138.121*/("""{"""),format.raw/*138.122*/("""%=file.thumbnailUrl%"""),format.raw/*138.142*/("""}"""),format.raw/*138.143*/(""""></a>
                    """),format.raw/*139.21*/("""{"""),format.raw/*139.22*/("""% """),format.raw/*139.24*/("""}"""),format.raw/*139.25*/(""" %"""),format.raw/*139.27*/("""}"""),format.raw/*139.28*/("""
                </span>
            </td>
            <td>
                <p class="name">
                    """),format.raw/*144.21*/("""{"""),format.raw/*144.22*/("""% if (file.url) """),format.raw/*144.38*/("""{"""),format.raw/*144.39*/(""" %"""),format.raw/*144.41*/("""}"""),format.raw/*144.42*/("""
                        <a href=""""),format.raw/*145.34*/("""{"""),format.raw/*145.35*/("""%=file.url%"""),format.raw/*145.46*/("""}"""),format.raw/*145.47*/("""" title=""""),format.raw/*145.56*/("""{"""),format.raw/*145.57*/("""%=file.name%"""),format.raw/*145.69*/("""}"""),format.raw/*145.70*/("""" target="_blank" """),format.raw/*145.88*/("""{"""),format.raw/*145.89*/("""%=file.thumbnailUrl?'data-gallery':''%"""),format.raw/*145.127*/("""}"""),format.raw/*145.128*/(""">"""),format.raw/*145.129*/("""{"""),format.raw/*145.130*/("""%=file.name%"""),format.raw/*145.142*/("""}"""),format.raw/*145.143*/("""</a>
                    """),format.raw/*146.21*/("""{"""),format.raw/*146.22*/("""% """),format.raw/*146.24*/("""}"""),format.raw/*146.25*/(""" else """),format.raw/*146.31*/("""{"""),format.raw/*146.32*/(""" %"""),format.raw/*146.34*/("""}"""),format.raw/*146.35*/("""
                        <span>"""),format.raw/*147.31*/("""{"""),format.raw/*147.32*/("""%=file.name%"""),format.raw/*147.44*/("""}"""),format.raw/*147.45*/("""</span>
                    """),format.raw/*148.21*/("""{"""),format.raw/*148.22*/("""% """),format.raw/*148.24*/("""}"""),format.raw/*148.25*/(""" %"""),format.raw/*148.27*/("""}"""),format.raw/*148.28*/("""
                </p>
                """),format.raw/*150.17*/("""{"""),format.raw/*150.18*/("""% if (file.error) """),format.raw/*150.36*/("""{"""),format.raw/*150.37*/(""" %"""),format.raw/*150.39*/("""}"""),format.raw/*150.40*/("""
                    <div><span class="label label-danger">Error</span> """),format.raw/*151.72*/("""{"""),format.raw/*151.73*/("""%=file.error%"""),format.raw/*151.86*/("""}"""),format.raw/*151.87*/("""</div>
                """),format.raw/*152.17*/("""{"""),format.raw/*152.18*/("""% """),format.raw/*152.20*/("""}"""),format.raw/*152.21*/(""" %"""),format.raw/*152.23*/("""}"""),format.raw/*152.24*/("""
            </td>
            <td>
                <span class="size">"""),format.raw/*155.36*/("""{"""),format.raw/*155.37*/("""%=o.formatFileSize(file.size)%"""),format.raw/*155.67*/("""}"""),format.raw/*155.68*/("""</span>
            </td>
            <td>
                """),format.raw/*158.17*/("""{"""),format.raw/*158.18*/("""% if (file.deleteUrl) """),format.raw/*158.40*/("""{"""),format.raw/*158.41*/(""" %"""),format.raw/*158.43*/("""}"""),format.raw/*158.44*/("""
                    <button type="submit" class="btn btn-link delete" data-type=""""),format.raw/*159.82*/("""{"""),format.raw/*159.83*/("""%=file.deleteType%"""),format.raw/*159.101*/("""}"""),format.raw/*159.102*/("""" data-url=""""),format.raw/*159.114*/("""{"""),format.raw/*159.115*/("""%=file.deleteUrl%"""),format.raw/*159.132*/("""}"""),format.raw/*159.133*/("""""""),format.raw/*159.134*/("""{"""),format.raw/*159.135*/("""% if (file.deleteWithCredentials) """),format.raw/*159.169*/("""{"""),format.raw/*159.170*/(""" %"""),format.raw/*159.172*/("""}"""),format.raw/*159.173*/(""" data-xhr-fields='"""),format.raw/*159.191*/("""{"""),format.raw/*159.192*/(""""withCredentials":true"""),format.raw/*159.214*/("""}"""),format.raw/*159.215*/("""'"""),format.raw/*159.216*/("""{"""),format.raw/*159.217*/("""% """),format.raw/*159.219*/("""}"""),format.raw/*159.220*/(""" %"""),format.raw/*159.222*/("""}"""),format.raw/*159.223*/(""">
                        <span class="glyphicon glyphicon-trash"></span> Delete File
                    </button>
                """),format.raw/*162.17*/("""{"""),format.raw/*162.18*/("""% """),format.raw/*162.20*/("""}"""),format.raw/*162.21*/(""" else """),format.raw/*162.27*/("""{"""),format.raw/*162.28*/(""" %"""),format.raw/*162.30*/("""}"""),format.raw/*162.31*/("""
                    <button type="submit" class="btn btn-default cancel">
                        <span class="glyphicon glyphicon-remove"></span> Cancel
                    </button>
                """),format.raw/*166.17*/("""{"""),format.raw/*166.18*/("""% """),format.raw/*166.20*/("""}"""),format.raw/*166.21*/(""" %"""),format.raw/*166.23*/("""}"""),format.raw/*166.24*/("""
            </td>
        </tr>
    """),format.raw/*169.5*/("""{"""),format.raw/*169.6*/("""% """),format.raw/*169.8*/("""}"""),format.raw/*169.9*/(""" %"""),format.raw/*169.11*/("""}"""),format.raw/*169.12*/("""
</script>

<!-- The Templates plugin is included to render the upload/download listings - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*173.15*/routes/*173.21*/.Assets.at("javascripts/file-uploader/tmpl.min.js"))),format.raw/*173.72*/(""""></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*175.15*/routes/*175.21*/.Assets.at("javascripts/file-uploader/load-image.all.min.js"))),format.raw/*175.82*/(""""></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*177.15*/routes/*177.21*/.Assets.at("javascripts/file-uploader/canvas-to-blob.min.js"))),format.raw/*177.82*/(""""></script>
<!-- blueimp Gallery script - downloaded to make the resource local-->
<script src=""""),_display_(Seq[Any](/*179.15*/routes/*179.21*/.Assets.at("javascripts/file-uploader/jquery.blueimp-gallery.min.js"))),format.raw/*179.90*/(""""></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src=""""),_display_(Seq[Any](/*181.15*/routes/*181.21*/.Assets.at("javascripts/file-uploader/jquery.iframe-transport.js"))),format.raw/*181.87*/(""""></script>
<!-- The basic File Upload plugin -->
<script src=""""),_display_(Seq[Any](/*183.15*/routes/*183.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload.js"))),format.raw/*183.81*/(""""></script>
<!-- The File Upload processing plugin -->
<script src=""""),_display_(Seq[Any](/*185.15*/routes/*185.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-process.js"))),format.raw/*185.89*/(""""></script>
"""),_display_(Seq[Any](/*186.2*/if(play.Play.application.configuration.getBoolean("clowder.upload.previews", true))/*186.85*/ {_display_(Seq[Any](format.raw/*186.87*/("""
    <!-- The File Upload image preview & resize plugin -->
    <script src=""""),_display_(Seq[Any](/*188.19*/routes/*188.25*/.Assets.at("javascripts/file-uploader/jquery.fileupload-image.js"))),format.raw/*188.91*/(""""></script>
    <!-- The File Upload audio preview plugin -->
    <script src=""""),_display_(Seq[Any](/*190.19*/routes/*190.25*/.Assets.at("javascripts/file-uploader/jquery.fileupload-audio.js"))),format.raw/*190.91*/(""""></script>
    <!-- The File Upload video preview plugin -->
    <script src=""""),_display_(Seq[Any](/*192.19*/routes/*192.25*/.Assets.at("javascripts/file-uploader/jquery.fileupload-video.js"))),format.raw/*192.91*/(""""></script>
""")))})),format.raw/*193.2*/("""
<!-- The File Upload validation plugin -->
<script src=""""),_display_(Seq[Any](/*195.15*/routes/*195.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-validate.js"))),format.raw/*195.90*/(""""></script>
<!-- The File Upload user interface plugin -->
<script src=""""),_display_(Seq[Any](/*197.15*/routes/*197.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-ui.js"))),format.raw/*197.84*/(""""></script>
<!-- The main application script -->
<script src=""""),_display_(Seq[Any](/*199.15*/routes/*199.21*/.Assets.at("javascripts/file-uploader/main.js"))),format.raw/*199.68*/(""""></script>

"""))}
    }
    
    def render(dataset:Dataset,folderId:Option[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,folderId)(user)
    
    def f:((Dataset,Option[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,folderId) => (user) => apply(dataset,folderId)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/uploadFiles.scala.html
                    HASH: 8a00c2a11a66054aa702e345abcfcc18270c5523
                    MATRIX: 638->1|842->81|869->113|1416->624|1431->630|1517->693|1619->759|1634->765|1705->814|1870->943|1885->949|1968->1010|2037->1043|2052->1049|2139->1113|2285->1223|2300->1229|2393->1299|2483->1353|2498->1359|2594->1432|2714->1516|2729->1522|2807->1578|2892->1627|2907->1633|2971->1675|3252->1920|3286->1945|3320->1957|3485->2086|3500->2092|3548->2118|3901->2435|3917->2442|3942->2445|4913->3379|4930->3386|4956->3389|4998->3394|5016->3402|5049->3412|7424->5760|7452->5761|7524->5805|7553->5806|7583->5808|7612->5809|7814->5982|7844->5983|7885->5995|7915->5996|8360->6412|8390->6413|8454->6448|8484->6449|8515->6451|8545->6452|8768->6646|8798->6647|8829->6649|8859->6650|8890->6652|8920->6653|8966->6670|8996->6671|9035->6681|9065->6682|9096->6684|9126->6685|9356->6886|9386->6887|9417->6889|9447->6890|9478->6892|9508->6893|9573->6930|9602->6931|9632->6933|9661->6934|9692->6936|9722->6937|9878->7065|9907->7066|9980->7110|10010->7111|10041->7113|10071->7114|10157->7171|10187->7172|10238->7194|10268->7195|10299->7197|10329->7198|10485->7325|10515->7326|10546->7328|10576->7329|10607->7331|10637->7332|10743->7409|10773->7410|10827->7435|10857->7436|10888->7438|10918->7439|10981->7473|11011->7474|11051->7485|11081->7486|11119->7495|11149->7496|11190->7508|11220->7509|11261->7521|11291->7522|11332->7534|11362->7535|11417->7560|11448->7561|11498->7581|11529->7582|11585->7609|11615->7610|11646->7612|11676->7613|11707->7615|11737->7616|11879->7729|11909->7730|11954->7746|11984->7747|12015->7749|12045->7750|12108->7784|12138->7785|12178->7796|12208->7797|12246->7806|12276->7807|12317->7819|12347->7820|12394->7838|12424->7839|12492->7877|12523->7878|12554->7879|12585->7880|12627->7892|12658->7893|12712->7918|12742->7919|12773->7921|12803->7922|12838->7928|12868->7929|12899->7931|12929->7932|12989->7963|13019->7964|13060->7976|13090->7977|13147->8005|13177->8006|13208->8008|13238->8009|13269->8011|13299->8012|13366->8050|13396->8051|13443->8069|13473->8070|13504->8072|13534->8073|13635->8145|13665->8146|13707->8159|13737->8160|13789->8183|13819->8184|13850->8186|13880->8187|13911->8189|13941->8190|14041->8261|14071->8262|14130->8292|14160->8293|14248->8352|14278->8353|14329->8375|14359->8376|14390->8378|14420->8379|14531->8461|14561->8462|14609->8480|14640->8481|14682->8493|14713->8494|14760->8511|14791->8512|14822->8513|14853->8514|14917->8548|14948->8549|14980->8551|15011->8552|15059->8570|15090->8571|15142->8593|15173->8594|15204->8595|15235->8596|15267->8598|15298->8599|15330->8601|15361->8602|15522->8734|15552->8735|15583->8737|15613->8738|15648->8744|15678->8745|15709->8747|15739->8748|15969->8949|15999->8950|16030->8952|16060->8953|16091->8955|16121->8956|16186->8993|16215->8994|16245->8996|16274->8997|16305->8999|16335->9000|16519->9147|16535->9153|16609->9204|16811->9369|16827->9375|16911->9436|17094->9582|17110->9588|17194->9649|17328->9746|17344->9752|17436->9821|17591->9939|17607->9945|17696->10011|17797->10075|17813->10081|17896->10141|18002->10210|18018->10216|18109->10284|18158->10297|18251->10380|18292->10382|18407->10460|18423->10466|18512->10532|18629->10612|18645->10618|18734->10684|18851->10764|18867->10770|18956->10836|19001->10849|19096->10907|19112->10913|19204->10982|19314->11055|19330->11061|19416->11124|19516->11187|19532->11193|19602->11240
                    LINES: 20->1|24->1|25->3|36->14|36->14|36->14|38->16|38->16|38->16|40->18|40->18|40->18|41->19|41->19|41->19|43->21|43->21|43->21|44->22|44->22|44->22|48->26|48->26|48->26|49->27|49->27|49->27|54->32|54->32|54->32|56->34|56->34|56->34|60->38|60->38|60->38|72->50|72->50|72->50|72->50|72->50|72->50|120->98|120->98|120->98|120->98|120->98|120->98|126->104|126->104|126->104|126->104|134->112|134->112|134->112|134->112|134->112|134->112|138->116|138->116|138->116|138->116|138->116|138->116|139->117|139->117|139->117|139->117|139->117|139->117|143->121|143->121|143->121|143->121|143->121|143->121|146->124|146->124|146->124|146->124|146->124|146->124|150->128|150->128|150->128|150->128|150->128|150->128|152->130|152->130|152->130|152->130|152->130|152->130|156->134|156->134|156->134|156->134|156->134|156->134|159->137|159->137|159->137|159->137|159->137|159->137|160->138|160->138|160->138|160->138|160->138|160->138|160->138|160->138|160->138|160->138|160->138|160->138|160->138|160->138|160->138|160->138|161->139|161->139|161->139|161->139|161->139|161->139|166->144|166->144|166->144|166->144|166->144|166->144|167->145|167->145|167->145|167->145|167->145|167->145|167->145|167->145|167->145|167->145|167->145|167->145|167->145|167->145|167->145|167->145|168->146|168->146|168->146|168->146|168->146|168->146|168->146|168->146|169->147|169->147|169->147|169->147|170->148|170->148|170->148|170->148|170->148|170->148|172->150|172->150|172->150|172->150|172->150|172->150|173->151|173->151|173->151|173->151|174->152|174->152|174->152|174->152|174->152|174->152|177->155|177->155|177->155|177->155|180->158|180->158|180->158|180->158|180->158|180->158|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|184->162|184->162|184->162|184->162|184->162|184->162|184->162|184->162|188->166|188->166|188->166|188->166|188->166|188->166|191->169|191->169|191->169|191->169|191->169|191->169|195->173|195->173|195->173|197->175|197->175|197->175|199->177|199->177|199->177|201->179|201->179|201->179|203->181|203->181|203->181|205->183|205->183|205->183|207->185|207->185|207->185|208->186|208->186|208->186|210->188|210->188|210->188|212->190|212->190|212->190|214->192|214->192|214->192|215->193|217->195|217->195|217->195|219->197|219->197|219->197|221->199|221->199|221->199
                    -- GENERATED --
                */
            