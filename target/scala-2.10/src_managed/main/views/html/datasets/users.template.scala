
package views.html.datasets

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object users extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[models.Dataset,Map[UUID, List[Tuple2[String, String]]],List[User],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: models.Dataset, userListSpaceRoleTupleMap: Map[UUID, List[Tuple2[String,String]]], users: List[User])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import _root_.util.Formatters._


Seq[Any](format.raw/*1.149*/("""
"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Dataset")/*5.17*/ {_display_(Seq[Any](format.raw/*5.19*/("""

    <div class="row">
        <ol class="breadcrumb">
            <li><span class="glyphicon glyphicon-briefcase"></span><a href= """"),_display_(Seq[Any](/*9.79*/routes/*9.85*/.Datasets.dataset(dataset.id))),format.raw/*9.114*/("""" title=""""),_display_(Seq[Any](/*9.124*/dataset/*9.131*/.name)),format.raw/*9.136*/(""""> """),_display_(Seq[Any](/*9.140*/Html(ellipsize(dataset.name, 18)))),format.raw/*9.173*/("""</a></li>
            <li><span class="glyphicon glyphicon-user"></span> Collaborators</li>
        </ol>
        <div class="row bottom-padding">
            <div class="col-md-12" id="ds-title">
                <h1 id="datasettitle">"""),_display_(Seq[Any](/*14.40*/dataset/*14.47*/.name)),format.raw/*14.52*/("""</h1>
            </div>
        </div>

        <div class="col-md-12">
            """),_display_(Seq[Any](/*19.14*/if(users.isEmpty)/*19.31*/ {_display_(Seq[Any](format.raw/*19.33*/("""
                """),_display_(Seq[Any](/*20.18*/Html("No users with access to the dataset"))),format.raw/*20.61*/("""
            """)))}/*21.15*/else/*21.20*/{_display_(Seq[Any](format.raw/*21.21*/("""
                <h3>Users with access to the dataset</h3>

                <table id='user-space-role' class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="col-md-4">User</th>

                                <th>
                                    <table id='nested-space-role-headers' class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="col-md-6">"""),_display_(Seq[Any](/*33.71*/Messages("space.title"))),format.raw/*33.94*/("""</th>
                                                <th class="col-md-2">Role</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </th>

                        </tr>
                    </thead>
                    <tbody>
                        """),_display_(Seq[Any](/*43.26*/users/*43.31*/.filter(!_.fullName.isEmpty).map/*43.63*/ { u =>_display_(Seq[Any](format.raw/*43.70*/("""
                            <tr>
                                <td class="col-md-4">"""),_display_(Seq[Any](/*45.55*/u/*45.56*/.fullName)),format.raw/*45.65*/("""</td>
                                """),_display_(Seq[Any](/*46.34*/if(userListSpaceRoleTupleMap contains u.id)/*46.77*/ {_display_(Seq[Any](format.raw/*46.79*/("""
                                    <td>
                                        <table id='nested-space-role-pairs' class="table table-hover">
                                            <tbody>
                                                """),_display_(Seq[Any](/*50.50*/for(tupleSpaceRole <- userListSpaceRoleTupleMap(u.id)) yield /*50.104*/ {_display_(Seq[Any](format.raw/*50.106*/("""
                                                    <tr>
                                                        <td class="col-md-6">"""),_display_(Seq[Any](/*52.79*/tupleSpaceRole/*52.93*/._1)),format.raw/*52.96*/("""</td>
                                                        <td class="col-md-2">"""),_display_(Seq[Any](/*53.79*/tupleSpaceRole/*53.93*/._2)),format.raw/*53.96*/("""</td>
                                                    </tr>
                                                """)))})),format.raw/*55.50*/("""
                                            </tbody>
                                        </table>
                                    </td>
                                """)))})),format.raw/*59.34*/("""
                            </tr>
                        """)))})),format.raw/*61.26*/("""
                    </tbody>
                </table>
            """)))})),format.raw/*64.14*/("""
        </div>

    </div>
""")))})),format.raw/*68.2*/("""
"""))}
    }
    
    def render(dataset:models.Dataset,userListSpaceRoleTupleMap:Map[UUID, List[Tuple2[String, String]]],users:List[User],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,userListSpaceRoleTupleMap,users)(user)
    
    def f:((models.Dataset,Map[UUID, List[Tuple2[String, String]]],List[User]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,userListSpaceRoleTupleMap,users) => (user) => apply(dataset,userListSpaceRoleTupleMap,users)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasets/users.scala.html
                    HASH: f1dc83137493cf657ef6a5d33c16e6862f1fc379
                    MATRIX: 675->1|980->148|1007->213|1043->215|1066->230|1105->232|1274->366|1288->372|1339->401|1385->411|1401->418|1428->423|1468->427|1523->460|1795->696|1811->703|1838->708|1960->794|1986->811|2026->813|2080->831|2145->874|2178->889|2191->894|2230->895|2834->1463|2879->1486|3295->1866|3309->1871|3350->1903|3395->1910|3519->1998|3529->1999|3560->2008|3635->2047|3687->2090|3727->2092|4009->2338|4080->2392|4121->2394|4293->2530|4316->2544|4341->2547|4461->2631|4484->2645|4509->2648|4654->2761|4864->2939|4956->2999|5056->3067|5116->3096
                    LINES: 20->1|26->1|27->4|28->5|28->5|28->5|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|37->14|37->14|37->14|42->19|42->19|42->19|43->20|43->20|44->21|44->21|44->21|56->33|56->33|66->43|66->43|66->43|66->43|68->45|68->45|68->45|69->46|69->46|69->46|73->50|73->50|73->50|75->52|75->52|75->52|76->53|76->53|76->53|78->55|82->59|84->61|87->64|91->68
                    -- GENERATED --
                */
            