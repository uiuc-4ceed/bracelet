
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tos extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Option[String],Option[models.User],RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(redirect: Option[String])(implicit user: Option[models.User] = None, request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services.AppConfiguration


Seq[Any](format.raw/*1.95*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Terms of Service")/*5.26*/ {_display_(Seq[Any](format.raw/*5.28*/("""
    
    <h2>"""),_display_(Seq[Any](/*7.10*/AppConfiguration/*7.26*/.getDisplayName)),format.raw/*7.41*/(""" Terms of Service</h2>

    <p>Version: """),_display_(Seq[Any](/*9.18*/AppConfiguration/*9.34*/.getTermsOfServicesVersionString)),format.raw/*9.66*/("""</p>
    """),_display_(Seq[Any](/*10.6*/if(user.isDefined && user.get.termsOfServices.isDefined)/*10.62*/ {_display_(Seq[Any](format.raw/*10.64*/("""
        """),_display_(Seq[Any](/*11.10*/if(!user.get.termsOfServices.get.accepted)/*11.52*/ {_display_(Seq[Any](format.raw/*11.54*/("""
            <p><strong>The Terms of Services have changed since you accepted them on """),_display_(Seq[Any](/*12.87*/user/*12.91*/.map(u => u.termsOfServices.map(_.acceptedDate)))),format.raw/*12.139*/("""</strong></p>
        """)))}/*13.11*/else/*13.16*/{_display_(Seq[Any](format.raw/*13.17*/("""
            <p>You accepted these Terms of Service on """),_display_(Seq[Any](/*14.56*/user/*14.60*/.map(u => u.termsOfServices.map(_.acceptedDate)))),format.raw/*14.108*/("""</p>
        """)))})),format.raw/*15.10*/("""
    """)))})),format.raw/*16.6*/("""

    <br/>
    """),_display_(Seq[Any](/*19.6*/if(AppConfiguration.isDefaultTermsOfServices || AppConfiguration.isTermOfServicesHtml)/*19.92*/ {_display_(Seq[Any](format.raw/*19.94*/("""
        """),_display_(Seq[Any](/*20.10*/Html(AppConfiguration.getTermsOfServicesText))),format.raw/*20.55*/("""
    """)))}/*21.7*/else/*21.12*/{_display_(Seq[Any](format.raw/*21.13*/("""
        """),_display_(Seq[Any](/*22.10*/Html(HtmlFormat.escape(AppConfiguration.getTermsOfServicesText).toString.replace("\n", "<br />")))),format.raw/*22.107*/("""
    """)))})),format.raw/*23.6*/("""
    <br/>
    <br/>

    """),_display_(Seq[Any](/*27.6*/if(redirect.isDefined)/*27.28*/ {_display_(Seq[Any](format.raw/*27.30*/("""
        <a href=""""),_display_(Seq[Any](/*28.19*/routes/*28.25*/.Users.acceptTermsOfServices(redirect).url)),format.raw/*28.67*/("""" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Accept</a>
    """)))})),format.raw/*29.6*/("""
    <p></p>
""")))})))}
    }
    
    def render(redirect:Option[String],user:Option[models.User],request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(redirect)(user,request)
    
    def f:((Option[String]) => (Option[models.User],RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (redirect) => (user,request) => apply(redirect)(user,request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:30 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/tos.scala.html
                    HASH: d23836b774b35b5dea684a42c5aeaa15755e8f28
                    MATRIX: 627->1|847->94|875->130|911->132|943->156|982->158|1032->173|1056->189|1092->204|1168->245|1192->261|1245->293|1290->303|1355->359|1395->361|1441->371|1492->413|1532->415|1655->502|1668->506|1739->554|1781->578|1794->583|1833->584|1925->640|1938->644|2009->692|2055->706|2092->712|2144->729|2239->815|2279->817|2325->827|2392->872|2416->879|2429->884|2468->885|2514->895|2634->992|2671->998|2733->1025|2764->1047|2804->1049|2859->1068|2874->1074|2938->1116|3056->1203
                    LINES: 20->1|24->1|26->4|27->5|27->5|27->5|29->7|29->7|29->7|31->9|31->9|31->9|32->10|32->10|32->10|33->11|33->11|33->11|34->12|34->12|34->12|35->13|35->13|35->13|36->14|36->14|36->14|37->15|38->16|41->19|41->19|41->19|42->20|42->20|43->21|43->21|43->21|44->22|44->22|45->23|49->27|49->27|49->27|50->28|50->28|50->28|51->29
                    -- GENERATED --
                */
            