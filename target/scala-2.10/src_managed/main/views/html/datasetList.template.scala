
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object datasetList extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template18[List[models.Dataset],Map[UUID, Int],String,String,Int,Option[String],Option[String],Option[String],Option[String],Option[String],Option[String],Option[String],String,String,List[String],Boolean,play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(datasetsList: List[models.Dataset], comments: Map[UUID, Int], prev: String, next: String, limit: Int,
        mode: Option[String], space: Option[String], spaceName: Option[String], status: Option[String],
        title: Option[String], owner:Option[String], ownerName: Option[String], when: String, date: String,
        userSelections: List[String], showTrash: Boolean = false)(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*4.129*/("""

"""),_display_(Seq[Any](/*7.2*/main(Messages("datasets.title"))/*7.34*/ {_display_(Seq[Any](format.raw/*7.36*/("""

"""),_display_(Seq[Any](/*9.2*/util/*9.6*/.masonry())),format.raw/*9.16*/("""
<script src=""""),_display_(Seq[Any](/*10.15*/routes/*10.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*10.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*11.15*/routes/*11.21*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*11.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*12.15*/routes/*12.21*/.Assets.at("javascripts/spaceModify.js"))),format.raw/*12.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*13.15*/routes/*13.21*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*13.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*14.15*/routes/*14.21*/.Assets.at("javascripts/follow-button.js"))),format.raw/*14.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*15.15*/routes/*15.21*/.Assets.at("javascripts/displayPanels.js"))),format.raw/*15.63*/("""" type="text/javascript"></script>

<div class="row">
    <ol class="breadcrumb">
        """),_display_(Seq[Any](/*19.10*/(owner, ownerName)/*19.28*/ match/*19.34*/ {/*20.13*/case (Some(o), Some(n)) =>/*20.39*/ {_display_(Seq[Any](format.raw/*20.41*/("""
                <li> <span class="glyphicon glyphicon-user"></span> <a href=""""),_display_(Seq[Any](/*21.79*/routes/*21.85*/.Profile.viewProfileUUID(UUID(o)))),format.raw/*21.118*/(""""> """),_display_(Seq[Any](/*21.122*/n)),format.raw/*21.123*/("""</a></li>
            """)))}/*23.13*/case (_, _) =>/*23.27*/ {}})),format.raw/*24.10*/("""
        """),_display_(Seq[Any](/*25.10*/(space, spaceName)/*25.28*/ match/*25.34*/ {/*26.13*/case (Some(s), Some(sn)) =>/*26.40*/ {_display_(Seq[Any](format.raw/*26.42*/("""
                <li><span class="glyphicon glyphicon-hdd"></span> <a href=""""),_display_(Seq[Any](/*27.77*/routes/*27.83*/.Spaces.getSpace(UUID(s)))),format.raw/*27.108*/(""""> """),_display_(Seq[Any](/*27.112*/sn)),format.raw/*27.114*/("""</a></li>
            """)))}/*29.13*/case (_,_) =>/*29.26*/ {}})),format.raw/*30.10*/("""

        <li><span class="glyphicon glyphicon-briefcase hideTree"> </span> Datasets</li>
    </ol>
</div>
    <div class="row">
        <div class="col-md-12">
            <h1>"""),_display_(Seq[Any](/*37.18*/Html(title.getOrElse("Datasets")))),format.raw/*37.51*/("""</h1>
            """),_display_(Seq[Any](/*38.14*/status/*38.20*/ match/*38.26*/ {/*39.17*/case Some("publicAll") =>/*39.42*/ {_display_(Seq[Any](format.raw/*39.44*/("""
                    <p>The """),_display_(Seq[Any](/*40.29*/Messages("space.title"))),format.raw/*40.52*/(""" team has made the following """),_display_(Seq[Any](/*40.82*/Messages("datasets.title")/*40.108*/.toLowerCase)),format.raw/*40.120*/(""" publicly available.</p>
                """)))}/*42.17*/case _ =>/*42.26*/ {_display_(Seq[Any](format.raw/*42.28*/("""
                    """),_display_(Seq[Any](/*43.22*/if(showTrash)/*43.35*/{_display_(Seq[Any](format.raw/*43.36*/("""
                        <p>"""),_display_(Seq[Any](/*44.29*/Messages("dataset.trash.message", Messages("datasets.title").toLowerCase, Messages("dataset.title").toLowerCase))),format.raw/*44.141*/("""</p>

                    """)))}/*46.23*/else/*46.28*/{_display_(Seq[Any](format.raw/*46.29*/("""
                        <p>"""),_display_(Seq[Any](/*47.29*/Messages("dataset.list.message", Messages("datasets.title").toLowerCase, Messages("dataset.title").toLowerCase))),format.raw/*47.140*/("""</p>
                    """)))})),format.raw/*48.22*/("""
                """)))}})),format.raw/*50.14*/("""
        </div>
    </div>

<div class="row">
    <div class="btn-toolbar">
     <!-- pull-right class stacks things to the right - first item to the farthest right -->
        <div class="btn-group btn-group-sm pull-right">
                <button type="button" class="btn btn-default active" href="#tile-view" id="tile-view-btn"><span class="glyphicon glyphicon-th-large"></span></button>
                <button type="button" class="btn btn-default" href="#list-view" id="list-view-btn"><span class="glyphicon glyphicon-th-list"></span></button>
        </div>
        <div class="btn-group btn-group-sm pull-right" id="number-displayed-dropdown">
            <select id="numPageItems" class="form-control" onchange="getValue()">
                <option value="12">12</option>
                <option value="24">24</option>
                <option value="48">48</option>
                <option value="96">96</option>
            </select>
        </div>

  """),_display_(Seq[Any](/*70.4*/if(play.Play.application().configuration().getBoolean("sortInMemory"))/*70.74*/ {_display_(Seq[Any](format.raw/*70.76*/("""
  """),_display_(Seq[Any](/*71.4*/space/*71.9*/ match/*71.15*/ {/*72.5*/case Some(s) =>/*72.20*/ {_display_(Seq[Any](format.raw/*72.22*/("""
      <div class="btn-group btn-group-sm pull-right">
      <label class = "sortchoice" for="js-sort-single">Sort By:
      <select class="js-sort-single">
        <option value="dateN">Newest</option>
        <option value="dateO">Oldest</option>
        <option value="titleA">Title (A-Z)</option>
        <option value="titleZ">Title (Z-A)</option>
        <option value="sizeL">Size (L)</option>
        <option value="sizeS">Size (S)</option>
      </select>
      </label>
        </div>
<script type="text/javascript">
  $(function() """),format.raw/*86.16*/("""{"""),format.raw/*86.17*/("""
	$(".js-sort-single").select2("""),format.raw/*87.31*/("""{"""),format.raw/*87.32*/("""minimumResultsForSearch: Infinity"""),format.raw/*87.65*/("""}"""),format.raw/*87.66*/(""");
  	//Set starting value based on cookie
  	var order = 'dateN';
  	if($.cookie('sort-order') != null) """),format.raw/*90.39*/("""{"""),format.raw/*90.40*/("""
  	  //removing quotes from around cookie value
  	  order = $.cookie('sort-order').replace(/['"]+/g, '');
  	"""),format.raw/*93.4*/("""}"""),format.raw/*93.5*/("""
    $('.js-sort-single').val(order).trigger("change");

    var currentSpace = null;
    currentSpace = '"""),_display_(Seq[Any](/*97.22*/s)),format.raw/*97.23*/("""';

    $(".js-sort-single").on('select2:select', function (evt) """),format.raw/*99.62*/("""{"""),format.raw/*99.63*/("""
         $(window).trigger("sortchange");
    """),format.raw/*101.5*/("""}"""),format.raw/*101.6*/(""");



    $(window).on('sortchange', function() """),format.raw/*105.43*/("""{"""),format.raw/*105.44*/("""
      var sort = $(".js-sort-single").val();
      //Update cookie
      $.cookie('sort-order', sort, """),format.raw/*108.36*/("""{"""),format.raw/*108.37*/("""path: '/'"""),format.raw/*108.46*/("""}"""),format.raw/*108.47*/(""");
      //Go get the list sorted the new way
      """),_display_(Seq[Any](/*110.8*/if(prev != "")/*110.22*/ {_display_(Seq[Any](format.raw/*110.24*/("""
      var request = jsRoutes.controllers.Datasets.sortedListInSpace(currentSpace, """),_display_(Seq[Any](/*111.84*/prev/*111.88*/.toInt)),format.raw/*111.94*/(""", """),_display_(Seq[Any](/*111.97*/limit)),format.raw/*111.102*/(""");
      """)))}/*112.9*/else/*112.14*/{_display_(Seq[Any](format.raw/*112.15*/("""
      var request = jsRoutes.controllers.Datasets.sortedListInSpace(currentSpace, 0, """),_display_(Seq[Any](/*113.87*/limit)),format.raw/*113.92*/(""");
      """)))})),format.raw/*114.8*/("""
      window.location = request.url;
    """),format.raw/*116.5*/("""}"""),format.raw/*116.6*/(""");
  """),format.raw/*117.3*/("""}"""),format.raw/*117.4*/(""");
</script>
    """)))}/*120.5*/case None =>/*120.17*/ {}})),format.raw/*121.6*/("""
""")))})),format.raw/*122.2*/("""

    """),_display_(Seq[Any](/*124.6*/status/*124.12*/ match/*124.18*/ {/*125.9*/case Some(st) =>/*125.25*/ {}/*126.9*/case None =>/*126.21*/ {_display_(Seq[Any](format.raw/*126.23*/("""

        """),_display_(Seq[Any](/*128.10*/user/*128.14*/ match/*128.20*/ {/*129.13*/case Some(u) =>/*129.28*/ {_display_(Seq[Any](format.raw/*129.30*/("""
                """),_display_(Seq[Any](/*130.18*/(space, owner)/*130.32*/ match/*130.38*/ {/*131.21*/case (Some(s), Some(o)) =>/*131.47*/ {_display_(Seq[Any](format.raw/*131.49*/("""
                        """),_display_(Seq[Any](/*132.26*/if(Permission.checkPermission(Permission.CreateCollection, ResourceRef(ResourceRef.space, UUID(s))))/*132.126*/ {_display_(Seq[Any](format.raw/*132.128*/("""
                            """),_display_(Seq[Any](/*133.30*/if(o.equalsIgnoreCase(u.id.stringify))/*133.68*/ {_display_(Seq[Any](format.raw/*133.70*/("""
                                <a id="create-dataset" href=""""),_display_(Seq[Any](/*134.63*/routes/*134.69*/.Datasets.newDataset(Some(s), None))),format.raw/*134.104*/("""" class="btn btn-primary btn-sm pull-right" title=""""),_display_(Seq[Any](/*134.156*/Messages("create.title", Messages("dataset.title")))),format.raw/*134.207*/("""">
                                    <span class="glyphicon glyphicon-ok"></span>  """),_display_(Seq[Any](/*135.84*/Messages("create.title", ""))),format.raw/*135.112*/("""</a>
                            """)))})),format.raw/*136.30*/("""
                        """)))})),format.raw/*137.26*/("""
                    """)))}/*139.21*/case (Some(s), _) =>/*139.41*/ {_display_(Seq[Any](format.raw/*139.43*/("""
                        """),_display_(Seq[Any](/*140.26*/if(Permission.checkPermission(Permission.CreateCollection, ResourceRef(ResourceRef.space, UUID(s))))/*140.126*/ {_display_(Seq[Any](format.raw/*140.128*/("""
                            <a id="create-dataset" href=""""),_display_(Seq[Any](/*141.59*/routes/*141.65*/.Datasets.newDataset(Some(s), None))),format.raw/*141.100*/("""" class="btn btn-primary btn-sm pull-right" title=""""),_display_(Seq[Any](/*141.152*/Messages("create.title", Messages("dataset.title")))),format.raw/*141.203*/("""">
                                <span class="glyphicon glyphicon-ok"></span>  """),_display_(Seq[Any](/*142.80*/Messages("create.title", ""))),format.raw/*142.108*/("""</a>
                        """)))})),format.raw/*143.26*/("""
                    """)))}/*145.21*/case (_, Some(o)) =>/*145.41*/ {_display_(Seq[Any](format.raw/*145.43*/("""
                        """),_display_(Seq[Any](/*146.26*/if(o.equalsIgnoreCase(u.id.stringify))/*146.64*/ {_display_(Seq[Any](format.raw/*146.66*/("""
                            """),_display_(Seq[Any](/*147.30*/if(showTrash)/*147.43*/{_display_(Seq[Any](format.raw/*147.44*/("""
                                <a id="clear-trash" onclick="confirmClearTrash('dataset', '"""),_display_(Seq[Any](/*148.93*/(routes.Datasets.list("",owner=Some(user.get.id.stringify),showTrash=true)))),format.raw/*148.168*/("""')"
                                class="btn btn-primary btn-sm pull-right" href="#"> <span class="glyphicon glyphicon-trash"></span>  """),_display_(Seq[Any](/*149.135*/Messages("cleartrash.title", Messages("datasets.title")))),format.raw/*149.191*/("""</a>
                            """)))}/*150.31*/else/*150.36*/{_display_(Seq[Any](format.raw/*150.37*/("""
                                <a id="create-dataset" class="btn btn-primary btn-sm pull-right" href=""""),_display_(Seq[Any](/*151.105*/routes/*151.111*/.Datasets.newDataset(space, None))),format.raw/*151.144*/("""" title=""""),_display_(Seq[Any](/*151.154*/Messages("create.title", Messages("dataset.title")))),format.raw/*151.205*/(""""><span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*151.253*/Messages("create.title", ""))),format.raw/*151.281*/("""</a>
                            """)))})),format.raw/*152.30*/("""
                        """)))})),format.raw/*153.26*/("""
                    """)))}/*156.21*/case (_, _) =>/*156.35*/ {_display_(Seq[Any](format.raw/*156.37*/("""
                        <a id="create-dataset" class="btn btn-primary btn-sm pull-right" href=""""),_display_(Seq[Any](/*157.97*/routes/*157.103*/.Datasets.newDataset(space, None))),format.raw/*157.136*/("""" title=""""),_display_(Seq[Any](/*157.146*/Messages("create.title", Messages("dataset.title")))),format.raw/*157.197*/("""">
                            <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*158.75*/Messages("create.title", ""))),format.raw/*158.103*/("""</a>
                    """)))}})),format.raw/*160.18*/("""
            """)))}/*162.13*/case _ =>/*162.22*/ {}})),format.raw/*163.10*/("""
                """)))}})),format.raw/*165.6*/("""
    </div>
        <script type="text/javascript" language="javascript">
            var removeIndicator = false;
	        var viewMode = '"""),_display_(Seq[Any](/*169.27*/mode/*169.31*/.getOrElse("tile"))),format.raw/*169.49*/("""';
            var status = '"""),_display_(Seq[Any](/*170.28*/status/*170.34*/.getOrElse(""))),format.raw/*170.48*/("""';
	        $.cookie.raw = true;
	        $.cookie.json = true;       	
            $(function() """),format.raw/*173.26*/("""{"""),format.raw/*173.27*/("""
                $('#tile-view-btn').click(function() """),format.raw/*174.54*/("""{"""),format.raw/*174.55*/("""
                  $('#tile-view').removeClass('hidden');
                  $('#list-view').addClass('hidden');
                  $('#tile-view-btn').addClass('active');
                  $('#list-view-btn').removeClass('active');
                  viewMode = "tile";
                  updatePage();
                  $.cookie('view-mode', 'tile', """),format.raw/*181.49*/("""{"""),format.raw/*181.50*/(""" path: '/' """),format.raw/*181.61*/("""}"""),format.raw/*181.62*/(""");
                  $('#masonry').masonry().masonry("""),format.raw/*182.51*/("""{"""),format.raw/*182.52*/("""
                      itemSelector: '.post-box',
                      columnWidth: '.post-box',
                      transitionDuration: 4
                  """),format.raw/*186.19*/("""}"""),format.raw/*186.20*/(""");
                """),format.raw/*187.17*/("""}"""),format.raw/*187.18*/(""");
                $('#list-view-btn').click(function() """),format.raw/*188.54*/("""{"""),format.raw/*188.55*/("""
                  $('#tile-view').addClass('hidden');
                  $('#list-view').removeClass('hidden');
                  $('#list-view-btn').addClass('active');
                  $('#tile-view-btn').removeClass('active');
                  viewMode = "list";
                  updatePage();
                  //Utilizing library from https://github.com/carhartl/jquery-cookie/tree/v1.4.1
                  $.cookie("view-mode", "list", """),format.raw/*196.49*/("""{"""),format.raw/*196.50*/(""" path: '/' """),format.raw/*196.61*/("""}"""),format.raw/*196.62*/(""");
                """),format.raw/*197.17*/("""}"""),format.raw/*197.18*/(""");
            """),format.raw/*198.13*/("""}"""),format.raw/*198.14*/(""");
            
            $(document).ready(function() """),format.raw/*200.42*/("""{"""),format.raw/*200.43*/(""" 
            	//Set the cookie, for the case when it is passed in by the parameter
                $.cookie("view-mode", viewMode, """),format.raw/*202.49*/("""{"""),format.raw/*202.50*/(""" path: '/' """),format.raw/*202.61*/("""}"""),format.raw/*202.62*/(""");
                if (viewMode == "list") """),format.raw/*203.41*/("""{"""),format.raw/*203.42*/("""
                    $('#tile-view').addClass('hidden');
                    $('#list-view').removeClass('hidden');
                    $('#list-view-btn').addClass('active');
                    $('#tile-view-btn').removeClass('active');                      
                """),format.raw/*208.17*/("""}"""),format.raw/*208.18*/("""
                else """),format.raw/*209.22*/("""{"""),format.raw/*209.23*/("""
                    $('#tile-view').removeClass('hidden');
                    $('#list-view').addClass('hidden');
                    $('#tile-view-btn').addClass('active');
                    $('#list-view-btn').removeClass('active');                      
                """),format.raw/*214.17*/("""}"""),format.raw/*214.18*/("""
            	updatePage();            	
            """),format.raw/*216.13*/("""}"""),format.raw/*216.14*/(""");
            
            //Function to unify the changing of the href for the next/previous links. Called on button activation for
            //viewMode style, as well as on initial load of page.
	        function updatePage() """),format.raw/*220.32*/("""{"""),format.raw/*220.33*/("""
"""),_display_(Seq[Any](/*221.2*/if(play.Play.application().configuration().getBoolean("sortInMemory")&&space.isDefined)/*221.89*/ {_display_(Seq[Any](format.raw/*221.91*/("""
                """),_display_(Seq[Any](/*222.18*/if(next != "")/*222.32*/ {_display_(Seq[Any](format.raw/*222.34*/("""
                    $('#nextlink').attr('href', """"),_display_(Seq[Any](/*223.51*/(routes.Datasets.sortedListInSpace(space.getOrElse(""), next.toInt, limit)))),format.raw/*223.126*/("""");
                """)))})),format.raw/*224.18*/("""
                """),_display_(Seq[Any](/*225.18*/if(prev != "")/*225.32*/ {_display_(Seq[Any](format.raw/*225.34*/("""
                    $('#prevlink').attr('href', """"),_display_(Seq[Any](/*226.51*/(routes.Datasets.sortedListInSpace(space.getOrElse(""), prev.toInt-limit, limit)))),format.raw/*226.132*/("""");
                """)))})),format.raw/*227.18*/("""
""")))}/*228.3*/else/*228.8*/{_display_(Seq[Any](format.raw/*228.9*/("""
                if (status == "publicAll") """),format.raw/*229.44*/("""{"""),format.raw/*229.45*/("""
                    $('#nextlink').attr('href', """"),_display_(Seq[Any](/*230.51*/(routes.Datasets.list("a", next, limit, space, Some("publicAll"), "", owner)))),format.raw/*230.128*/("""");
                    $('#prevlink').attr('href', """"),_display_(Seq[Any](/*231.51*/(routes.Datasets.list("b", prev, limit, space, Some("publicAll"), "", owner)))),format.raw/*231.128*/("""");
                """),format.raw/*232.17*/("""}"""),format.raw/*232.18*/(""" else """),format.raw/*232.24*/("""{"""),format.raw/*232.25*/("""
                    $('#nextlink').attr('href', """"),_display_(Seq[Any](/*233.51*/(routes.Datasets.list("a", next, limit, space, None, "", owner)))),format.raw/*233.115*/("""");
                    $('#prevlink').attr('href', """"),_display_(Seq[Any](/*234.51*/(routes.Datasets.list("b", prev, limit, space, None, "", owner)))),format.raw/*234.115*/("""");
                """),format.raw/*235.17*/("""}"""),format.raw/*235.18*/("""
""")))})),format.raw/*236.2*/("""
                doSummarizeAbstracts();
			"""),format.raw/*238.4*/("""}"""),format.raw/*238.5*/("""
        </script>

<div class="row hidden" id="tile-view">
	<div class="col-md-12">
		<div id="masonry">
		"""),_display_(Seq[Any](/*244.4*/datasetsList/*244.16*/.map/*244.20*/ { dataset =>_display_(Seq[Any](format.raw/*244.33*/("""
            """),_display_(Seq[Any](/*245.14*/if(userSelections.indexOf(dataset.id.toString()) != -1)/*245.69*/ {_display_(Seq[Any](format.raw/*245.71*/("""
                """),_display_(Seq[Any](/*246.18*/datasets/*246.26*/.tile(dataset, space, comments, "col-lg-3 col-md-3 col-sm-3", false, false, routes.Datasets.list(when, date, limit, space, None, "", owner), true))),format.raw/*246.172*/("""
            """)))}/*247.15*/else/*247.20*/{_display_(Seq[Any](format.raw/*247.21*/("""
                """),_display_(Seq[Any](/*248.18*/datasets/*248.26*/.tile(dataset, space, comments, "col-lg-3 col-md-3 col-sm-3", false, false, routes.Datasets.list(when, date, limit, space, None, "", owner), false))),format.raw/*248.173*/("""
            """)))})),format.raw/*249.14*/("""
		""")))})),format.raw/*250.4*/("""
		</div>
	</div>
</div>

<div class="row hidden" id="list-view">
	<div class="col-md-12">
        """),_display_(Seq[Any](/*257.10*/datasetsList/*257.22*/.map/*257.26*/ { dataset =>_display_(Seq[Any](format.raw/*257.39*/("""
            """),_display_(Seq[Any](/*258.14*/if(userSelections.indexOf(dataset.id.toString()) != -1)/*258.69*/ {_display_(Seq[Any](format.raw/*258.71*/("""
                """),_display_(Seq[Any](/*259.18*/datasets/*259.26*/.listitem(dataset,None,comments, routes.Datasets.list(when, date, limit, space, None, "", owner), true))),format.raw/*259.129*/("""
            """)))}/*260.15*/else/*260.20*/{_display_(Seq[Any](format.raw/*260.21*/("""
                """),_display_(Seq[Any](/*261.18*/datasets/*261.26*/.listitem(dataset,None,comments, routes.Datasets.list(when, date, limit, space, None, "", owner), false))),format.raw/*261.130*/("""
            """)))})),format.raw/*262.14*/("""
        """)))})),format.raw/*263.10*/("""
	</div>
</div>

"""),_display_(Seq[Any](/*267.2*/util/*267.6*/.masonry())),format.raw/*267.16*/("""

<div class="row">
    <div class="col-md-12">
        <ul class="pager">
            <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
            """),_display_(Seq[Any](/*273.14*/if(prev != "")/*273.28*/ {_display_(Seq[Any](format.raw/*273.30*/("""
                <li class="previous"><a class="btn btn-link" id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
            """)))})),format.raw/*275.14*/("""
            """),_display_(Seq[Any](/*276.14*/if(next != "")/*276.28*/ {_display_(Seq[Any](format.raw/*276.30*/("""
                <li class ="next"><a class="btn btn-link" id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
            """)))})),format.raw/*278.14*/("""
        </ul>
    </div>
</div>

<script src=""""),_display_(Seq[Any](/*283.15*/routes/*283.21*/.Assets.at("javascripts/select.js"))),format.raw/*283.56*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*284.15*/routes/*284.21*/.Assets.at("javascripts/descriptionSummary.js"))),format.raw/*284.68*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*285.15*/routes/*285.21*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*285.66*/("""" type="text/javascript"></script>
""")))})),format.raw/*286.2*/("""
"""))}
    }
    
    def render(datasetsList:List[models.Dataset],comments:Map[UUID, Int],prev:String,next:String,limit:Int,mode:Option[String],space:Option[String],spaceName:Option[String],status:Option[String],title:Option[String],owner:Option[String],ownerName:Option[String],when:String,date:String,userSelections:List[String],showTrash:Boolean,flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(datasetsList,comments,prev,next,limit,mode,space,spaceName,status,title,owner,ownerName,when,date,userSelections,showTrash)(flash,user)
    
    def f:((List[models.Dataset],Map[UUID, Int],String,String,Int,Option[String],Option[String],Option[String],Option[String],Option[String],Option[String],Option[String],String,String,List[String],Boolean) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (datasetsList,comments,prev,next,limit,mode,space,spaceName,status,title,owner,ownerName,when,date,userSelections,showTrash) => (flash,user) => apply(datasetsList,comments,prev,next,limit,mode,space,spaceName,status,title,owner,ownerName,when,date,userSelections,showTrash)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/datasetList.scala.html
                    HASH: 5ce58d652323e361c3300e5d93669130920ee6fb
                    MATRIX: 820->1|1380->444|1417->470|1457->502|1496->504|1533->507|1544->511|1575->521|1626->536|1641->542|1705->584|1790->633|1805->639|1867->679|1952->728|1967->734|2029->774|2114->823|2129->829|2193->871|2278->920|2293->926|2357->968|2442->1017|2457->1023|2521->1065|2648->1156|2675->1174|2690->1180|2701->1195|2736->1221|2776->1223|2891->1302|2906->1308|2962->1341|3003->1345|3027->1346|3069->1382|3092->1396|3118->1409|3164->1419|3191->1437|3206->1443|3217->1458|3253->1485|3293->1487|3406->1564|3421->1570|3469->1595|3510->1599|3535->1601|3577->1637|3599->1650|3625->1663|3839->1841|3894->1874|3949->1893|3964->1899|3979->1905|3990->1924|4024->1949|4064->1951|4129->1980|4174->2003|4240->2033|4276->2059|4311->2071|4372->2130|4390->2139|4430->2141|4488->2163|4510->2176|4549->2177|4614->2206|4749->2318|4795->2346|4808->2351|4847->2352|4912->2381|5046->2492|5104->2518|5155->2550|6152->3512|6231->3582|6271->3584|6310->3588|6323->3593|6338->3599|6348->3606|6372->3621|6412->3623|6982->4165|7011->4166|7070->4197|7099->4198|7160->4231|7189->4232|7322->4337|7351->4338|7489->4449|7517->4450|7660->4557|7683->4558|7776->4623|7805->4624|7880->4671|7909->4672|7986->4720|8016->4721|8148->4824|8178->4825|8216->4834|8246->4835|8335->4888|8359->4902|8400->4904|8521->4988|8535->4992|8564->4998|8604->5001|8633->5006|8662->5017|8676->5022|8716->5023|8840->5110|8868->5115|8910->5125|8980->5167|9009->5168|9042->5173|9071->5174|9108->5197|9130->5209|9156->5218|9190->5220|9233->5227|9249->5233|9265->5239|9276->5250|9302->5266|9314->5278|9336->5290|9377->5292|9425->5303|9439->5307|9455->5313|9467->5328|9492->5343|9533->5345|9588->5363|9612->5377|9628->5383|9640->5406|9676->5432|9717->5434|9780->5460|9891->5560|9933->5562|10000->5592|10048->5630|10089->5632|10189->5695|10205->5701|10264->5736|10354->5788|10429->5839|10552->5925|10604->5953|10671->5987|10730->6013|10772->6056|10802->6076|10843->6078|10906->6104|11017->6204|11059->6206|11155->6265|11171->6271|11230->6306|11320->6358|11395->6409|11514->6491|11566->6519|11629->6549|11671->6592|11701->6612|11742->6614|11805->6640|11853->6678|11894->6680|11961->6710|11984->6723|12024->6724|12154->6817|12253->6892|12429->7030|12509->7086|12563->7121|12577->7126|12617->7127|12760->7232|12777->7238|12834->7271|12882->7281|12957->7332|13043->7380|13095->7408|13162->7442|13221->7468|13263->7512|13287->7526|13328->7528|13462->7625|13479->7631|13536->7664|13584->7674|13659->7725|13773->7802|13825->7830|13885->7874|13919->7901|13938->7910|13965->7923|14016->7947|14194->8088|14208->8092|14249->8110|14316->8140|14332->8146|14369->8160|14495->8257|14525->8258|14608->8312|14638->8313|15015->8661|15045->8662|15085->8673|15115->8674|15197->8727|15227->8728|15416->8888|15446->8889|15494->8908|15524->8909|15609->8965|15639->8966|16113->9411|16143->9412|16183->9423|16213->9424|16261->9443|16291->9444|16335->9459|16365->9460|16451->9517|16481->9518|16642->9650|16672->9651|16712->9662|16742->9663|16814->9706|16844->9707|17150->9984|17180->9985|17231->10007|17261->10008|17567->10285|17597->10286|17679->10339|17709->10340|17969->10571|17999->10572|18037->10574|18134->10661|18175->10663|18230->10681|18254->10695|18295->10697|18383->10748|18482->10823|18536->10844|18591->10862|18615->10876|18656->10878|18744->10929|18849->11010|18903->11031|18924->11034|18937->11039|18976->11040|19049->11084|19079->11085|19167->11136|19268->11213|19359->11267|19460->11344|19509->11364|19539->11365|19574->11371|19604->11372|19692->11423|19780->11487|19871->11541|19959->11605|20008->11625|20038->11626|20072->11628|20144->11672|20173->11673|20318->11782|20340->11794|20354->11798|20406->11811|20457->11825|20522->11880|20563->11882|20618->11900|20636->11908|20806->12054|20840->12069|20854->12074|20894->12075|20949->12093|20967->12101|21138->12248|21185->12262|21221->12266|21358->12366|21380->12378|21394->12382|21446->12395|21497->12409|21562->12464|21603->12466|21658->12484|21676->12492|21803->12595|21837->12610|21851->12615|21891->12616|21946->12634|21964->12642|22092->12746|22139->12760|22182->12770|22236->12788|22249->12792|22282->12802|22541->13024|22565->13038|22606->13040|22833->13234|22884->13248|22908->13262|22949->13264|23169->13451|23254->13499|23270->13505|23328->13540|23414->13589|23430->13595|23500->13642|23586->13691|23602->13697|23670->13742|23738->13778
                    LINES: 20->1|27->4|29->7|29->7|29->7|31->9|31->9|31->9|32->10|32->10|32->10|33->11|33->11|33->11|34->12|34->12|34->12|35->13|35->13|35->13|36->14|36->14|36->14|37->15|37->15|37->15|41->19|41->19|41->19|41->20|41->20|41->20|42->21|42->21|42->21|42->21|42->21|43->23|43->23|43->24|44->25|44->25|44->25|44->26|44->26|44->26|45->27|45->27|45->27|45->27|45->27|46->29|46->29|46->30|53->37|53->37|54->38|54->38|54->38|54->39|54->39|54->39|55->40|55->40|55->40|55->40|55->40|56->42|56->42|56->42|57->43|57->43|57->43|58->44|58->44|60->46|60->46|60->46|61->47|61->47|62->48|63->50|83->70|83->70|83->70|84->71|84->71|84->71|84->72|84->72|84->72|98->86|98->86|99->87|99->87|99->87|99->87|102->90|102->90|105->93|105->93|109->97|109->97|111->99|111->99|113->101|113->101|117->105|117->105|120->108|120->108|120->108|120->108|122->110|122->110|122->110|123->111|123->111|123->111|123->111|123->111|124->112|124->112|124->112|125->113|125->113|126->114|128->116|128->116|129->117|129->117|131->120|131->120|131->121|132->122|134->124|134->124|134->124|134->125|134->125|134->126|134->126|134->126|136->128|136->128|136->128|136->129|136->129|136->129|137->130|137->130|137->130|137->131|137->131|137->131|138->132|138->132|138->132|139->133|139->133|139->133|140->134|140->134|140->134|140->134|140->134|141->135|141->135|142->136|143->137|144->139|144->139|144->139|145->140|145->140|145->140|146->141|146->141|146->141|146->141|146->141|147->142|147->142|148->143|149->145|149->145|149->145|150->146|150->146|150->146|151->147|151->147|151->147|152->148|152->148|153->149|153->149|154->150|154->150|154->150|155->151|155->151|155->151|155->151|155->151|155->151|155->151|156->152|157->153|158->156|158->156|158->156|159->157|159->157|159->157|159->157|159->157|160->158|160->158|161->160|162->162|162->162|162->163|163->165|167->169|167->169|167->169|168->170|168->170|168->170|171->173|171->173|172->174|172->174|179->181|179->181|179->181|179->181|180->182|180->182|184->186|184->186|185->187|185->187|186->188|186->188|194->196|194->196|194->196|194->196|195->197|195->197|196->198|196->198|198->200|198->200|200->202|200->202|200->202|200->202|201->203|201->203|206->208|206->208|207->209|207->209|212->214|212->214|214->216|214->216|218->220|218->220|219->221|219->221|219->221|220->222|220->222|220->222|221->223|221->223|222->224|223->225|223->225|223->225|224->226|224->226|225->227|226->228|226->228|226->228|227->229|227->229|228->230|228->230|229->231|229->231|230->232|230->232|230->232|230->232|231->233|231->233|232->234|232->234|233->235|233->235|234->236|236->238|236->238|242->244|242->244|242->244|242->244|243->245|243->245|243->245|244->246|244->246|244->246|245->247|245->247|245->247|246->248|246->248|246->248|247->249|248->250|255->257|255->257|255->257|255->257|256->258|256->258|256->258|257->259|257->259|257->259|258->260|258->260|258->260|259->261|259->261|259->261|260->262|261->263|265->267|265->267|265->267|271->273|271->273|271->273|273->275|274->276|274->276|274->276|276->278|281->283|281->283|281->283|282->284|282->284|282->284|283->285|283->285|283->285|284->286
                    -- GENERATED --
                */
            