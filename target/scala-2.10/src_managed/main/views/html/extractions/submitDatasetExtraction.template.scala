
package views.html.extractions

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object submitDatasetExtraction extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[List[ExtractorInfo],Dataset,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(extractors: List[ExtractorInfo], ds: Dataset)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.84*/("""

"""),_display_(Seq[Any](/*3.2*/main("Extractions")/*3.21*/ {_display_(Seq[Any](format.raw/*3.23*/("""
    <div class="row">
        <div class="col-xs-12">
            <h1>Submit dataset for extraction</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p>Submit this dataset to a specific extractor below by providing parameters as a JSON document and clicking
                the submit button. The paramaters field can be left empty.</p>
            <p>Dataset name: <a href=""""),_display_(Seq[Any](/*13.40*/routes/*13.46*/.Datasets.dataset(ds.id))),format.raw/*13.70*/("""">"""),_display_(Seq[Any](/*13.73*/ds/*13.75*/.name)),format.raw/*13.80*/("""</a></p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Extractor's Name</th>
                        """),format.raw/*22.48*/("""
                        <th>Submit</th>
                    </tr>
                </thead>
                <tbody>
                    """),_display_(Seq[Any](/*27.22*/for(e <- extractors) yield /*27.42*/ {_display_(Seq[Any](format.raw/*27.44*/("""
                        <tr>
                            <td>"""),_display_(Seq[Any](/*29.34*/e/*29.35*/.name)),format.raw/*29.40*/("""</td>
                            """),format.raw/*30.110*/("""
                            <td><button class="btn btn-primary" onclick="submit('"""),_display_(Seq[Any](/*31.83*/e/*31.84*/.name)),format.raw/*31.89*/("""','"""),_display_(Seq[Any](/*31.93*/(e.name.replaceAll("\\.", "_")))),format.raw/*31.124*/("""_textarea','"""),_display_(Seq[Any](/*31.137*/ds/*31.139*/.id)),format.raw/*31.142*/("""')">Submit</button></td>
                        </tr>
                    """)))})),format.raw/*33.22*/("""
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        function submit(extractor_name, textbox_id, ds_id) """),format.raw/*39.60*/("""{"""),format.raw/*39.61*/("""
            //TODO: comment out since no extractors use parameters, may enable it in future.
            //var params = $('#'+textbox_id).val();
            //if (params === "") params = """"),format.raw/*42.44*/("""{"""),format.raw/*42.45*/("""}"""),format.raw/*42.46*/("""";
            var dataBody = """),format.raw/*43.28*/("""{"""),format.raw/*43.29*/("""'extractor': extractor_name/*, 'parameters': JSON.parse(params)*/"""),format.raw/*43.94*/("""}"""),format.raw/*43.95*/(""";
            var request = jsRoutes.api.Extractions.submitDatasetToExtractor(ds_id).ajax("""),format.raw/*44.89*/("""{"""),format.raw/*44.90*/("""
                data: JSON.stringify(dataBody),
                type: 'POST',
                contentType: "application/json",
            """),format.raw/*48.13*/("""}"""),format.raw/*48.14*/(""");

            request.done(function (response, textStatus, jqXHR)"""),format.raw/*50.64*/("""{"""),format.raw/*50.65*/("""
                notify("Submitted successfully", "success");
            """),format.raw/*52.13*/("""}"""),format.raw/*52.14*/(""");

            request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*54.67*/("""{"""),format.raw/*54.68*/("""
                console.error("The following error occured: " + textStatus, errorThrown);
            """),format.raw/*56.13*/("""}"""),format.raw/*56.14*/(""");
        """),format.raw/*57.9*/("""}"""),format.raw/*57.10*/("""
    </script>
""")))})),format.raw/*59.2*/("""

"""))}
    }
    
    def render(extractors:List[ExtractorInfo],ds:Dataset,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(extractors,ds)(user)
    
    def f:((List[ExtractorInfo],Dataset) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (extractors,ds) => (user) => apply(extractors,ds)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/extractions/submitDatasetExtraction.scala.html
                    HASH: 34f3cc5feb4db2c28b8337b02a3a128a2f1c578a
                    MATRIX: 658->1|834->83|871->86|898->105|937->107|1398->532|1413->538|1459->562|1498->565|1509->567|1536->572|1810->841|1983->978|2019->998|2059->1000|2158->1063|2168->1064|2195->1069|2258->1184|2377->1267|2387->1268|2414->1273|2454->1277|2508->1308|2558->1321|2570->1323|2596->1326|2704->1402|2900->1570|2929->1571|3146->1760|3175->1761|3204->1762|3262->1792|3291->1793|3384->1858|3413->1859|3531->1949|3560->1950|3728->2090|3757->2091|3852->2158|3881->2159|3983->2233|4012->2234|4110->2304|4139->2305|4270->2408|4299->2409|4337->2420|4366->2421|4413->2437
                    LINES: 20->1|23->1|25->3|25->3|25->3|35->13|35->13|35->13|35->13|35->13|35->13|44->22|49->27|49->27|49->27|51->29|51->29|51->29|52->30|53->31|53->31|53->31|53->31|53->31|53->31|53->31|53->31|55->33|61->39|61->39|64->42|64->42|64->42|65->43|65->43|65->43|65->43|66->44|66->44|70->48|70->48|72->50|72->50|74->52|74->52|76->54|76->54|78->56|78->56|79->57|79->57|81->59
                    -- GENERATED --
                */
            