
package views.html.extractions

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object submitFileExtraction extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[List[ExtractorInfo],File,List[Folder],List[ProjectSpace],List[Dataset],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(extractors: List[ExtractorInfo], file: File, folderHierarchy: List[Folder], spaces:List[ProjectSpace], allDatasets: List[Dataset])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters._


Seq[Any](format.raw/*1.169*/("""
"""),format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*4.2*/main("Extractions")/*4.21*/ {_display_(Seq[Any](format.raw/*4.23*/("""
    <ol class="breadcrumb">
        """),_display_(Seq[Any](/*6.10*/if(spaces.length == 1 )/*6.33*/ {_display_(Seq[Any](format.raw/*6.35*/("""
            <li><span class="glyphicon glyphicon-hdd"></span> <a href=""""),_display_(Seq[Any](/*7.73*/routes/*7.79*/.Spaces.getSpace(spaces.head.id))),format.raw/*7.111*/("""" title=""""),_display_(Seq[Any](/*7.121*/spaces/*7.127*/.head.name)),format.raw/*7.137*/(""""> """),_display_(Seq[Any](/*7.141*/Html(ellipsize(spaces.head.name, 18)))),format.raw/*7.178*/("""</a></li>

        """)))}/*9.11*/else/*9.16*/{_display_(Seq[Any](format.raw/*9.17*/("""
            """),_display_(Seq[Any](/*10.14*/if(spaces.length > 1)/*10.35*/ {_display_(Seq[Any](format.raw/*10.37*/("""
                <li>
                    <span class="dropdown">
                        <button class="btn-link dropdown-toggle" type="button" id="dropdown_space_list" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="true">
                            <span class="glyphicon glyphicon-hdd"></span> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" arialanelledby="dropdown_space_list">
                        """),_display_(Seq[Any](/*18.26*/spaces/*18.32*/.map/*18.36*/{ s =>_display_(Seq[Any](format.raw/*18.42*/("""
                            <li><a href=""""),_display_(Seq[Any](/*19.43*/routes/*19.49*/.Spaces.getSpace(s.id))),format.raw/*19.71*/("""" title="s.name"><span class="glyphicon glyphicon-hdd"></span> """),_display_(Seq[Any](/*19.135*/Html(ellipsize(s.name, 18)))),format.raw/*19.162*/("""</a></li>
                        """)))})),format.raw/*20.26*/("""
                        </ul>

                    </span>
                </li>
            """)))}/*25.15*/else/*25.20*/{_display_(Seq[Any](format.raw/*25.21*/("""
                <li><span class="glyphicon glyphicon-user"></span> <a href = """"),_display_(Seq[Any](/*26.80*/routes/*26.86*/.Profile.viewProfileUUID(file.author.id))),format.raw/*26.126*/(""""> """),_display_(Seq[Any](/*26.130*/file/*26.134*/.author.fullName)),format.raw/*26.150*/("""</a></li>
            """)))})),format.raw/*27.14*/("""
        """)))})),format.raw/*28.10*/("""
        """),_display_(Seq[Any](/*29.10*/if(allDatasets.length == 1 )/*29.38*/ {_display_(Seq[Any](format.raw/*29.40*/("""
            """),_display_(Seq[Any](/*30.14*/allDatasets/*30.25*/.map/*30.29*/ { ds =>_display_(Seq[Any](format.raw/*30.37*/("""
                <li> <span class="glyphicon glyphicon-briefcase"></span> <a href=""""),_display_(Seq[Any](/*31.84*/routes/*31.90*/.Datasets.dataset(ds.id))),format.raw/*31.114*/("""" title=""""),_display_(Seq[Any](/*31.124*/ds/*31.126*/.name)),format.raw/*31.131*/(""""> """),_display_(Seq[Any](/*31.135*/Html(ellipsize(ds.name, 18)))),format.raw/*31.163*/("""</a></li>
            """)))})),format.raw/*32.14*/("""
        """)))})),format.raw/*33.10*/("""
        """),_display_(Seq[Any](/*34.10*/folderHierarchy/*34.25*/.map/*34.29*/ { fd =>_display_(Seq[Any](format.raw/*34.37*/("""
            <li><span class="glyphicon glyphicon-folder-close"></span> <a href=""""),_display_(Seq[Any](/*35.82*/routes/*35.88*/.Datasets.dataset(allDatasets(0).id))),format.raw/*35.124*/("""#folderId="""),_display_(Seq[Any](/*35.135*/fd/*35.137*/.id)),format.raw/*35.140*/("""" title=""""),_display_(Seq[Any](/*35.150*/fd/*35.152*/.displayName)),format.raw/*35.164*/("""">"""),_display_(Seq[Any](/*35.167*/Html(ellipsize(fd.displayName, 18)))),format.raw/*35.202*/("""</a></li>
        """)))})),format.raw/*36.10*/("""
        <li><span class="glyphicon glyphicon-file"></span> <a href=""""),_display_(Seq[Any](/*37.70*/routes/*37.76*/.Files.file(file.id))),format.raw/*37.96*/(""""title=""""),_display_(Seq[Any](/*37.105*/file/*37.109*/.filename)),format.raw/*37.118*/("""">"""),_display_(Seq[Any](/*37.121*/Html(ellipsize(file.filename, 18)))),format.raw/*37.155*/("""</a></li>
        <li><span class="glyphicon glyphicon-fullscreen"></span> Submit for Extraction</li>

    </ol>
    <div class="row">
        <div class="col-xs-12">
            <h1>Submit file for extraction</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p>Submit this file to a specific extractor below by providing parameters as a JSON document and clicking
                the submit button. The paramaters field can be left empty.</p>
            <p>File name: <a href=""""),_display_(Seq[Any](/*50.37*/routes/*50.43*/.Files.file(file.id))),format.raw/*50.63*/("""">"""),_display_(Seq[Any](/*50.66*/file/*50.70*/.filename)),format.raw/*50.79*/("""</a></p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Extractor's Name</th>
                        """),format.raw/*59.48*/("""
                        <th>Submit</th>
                    </tr>
                </thead>
                <tbody>
                    """),_display_(Seq[Any](/*64.22*/for(e <- extractors) yield /*64.42*/ {_display_(Seq[Any](format.raw/*64.44*/("""
                        <tr>
                            <td>"""),_display_(Seq[Any](/*66.34*/e/*66.35*/.name)),format.raw/*66.40*/("""</td>
                            """),format.raw/*67.110*/("""
                            <td><button class="btn btn-primary" onclick="submit('"""),_display_(Seq[Any](/*68.83*/e/*68.84*/.name)),format.raw/*68.89*/("""','"""),_display_(Seq[Any](/*68.93*/(e.name.replaceAll("\\.", "_")))),format.raw/*68.124*/("""_textarea','"""),_display_(Seq[Any](/*68.137*/file/*68.141*/.id)),format.raw/*68.144*/("""')">Submit</button></td>
                        </tr>
                    """)))})),format.raw/*70.22*/("""
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        function submit(extractor_name, textbox_id, file_id) """),format.raw/*76.62*/("""{"""),format.raw/*76.63*/("""
            //TODO: comment out since no extractors use parameters, may enable it in future.
            //var params = $('#'+textbox_id).val();
            //if (params === "") params = """"),format.raw/*79.44*/("""{"""),format.raw/*79.45*/("""}"""),format.raw/*79.46*/("""";
            var dataBody = """),format.raw/*80.28*/("""{"""),format.raw/*80.29*/("""'extractor': extractor_name/*, 'parameters': JSON.parse(params)*/"""),format.raw/*80.94*/("""}"""),format.raw/*80.95*/(""";
            var request = jsRoutes.api.Extractions.submitFileToExtractor(file_id).ajax("""),format.raw/*81.88*/("""{"""),format.raw/*81.89*/("""
                data: JSON.stringify(dataBody),
                type: 'POST',
                contentType: "application/json",
            """),format.raw/*85.13*/("""}"""),format.raw/*85.14*/(""");

            request.done(function (response, textStatus, jqXHR)"""),format.raw/*87.64*/("""{"""),format.raw/*87.65*/("""
                notify("Submitted successfully", "success");
            """),format.raw/*89.13*/("""}"""),format.raw/*89.14*/(""");

            request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*91.67*/("""{"""),format.raw/*91.68*/("""
                console.error("The following error occured: " + textStatus, errorThrown);
            """),format.raw/*93.13*/("""}"""),format.raw/*93.14*/(""");
        """),format.raw/*94.9*/("""}"""),format.raw/*94.10*/("""
    </script>
""")))})),format.raw/*96.2*/("""

"""))}
    }
    
    def render(extractors:List[ExtractorInfo],file:File,folderHierarchy:List[Folder],spaces:List[ProjectSpace],allDatasets:List[Dataset],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(extractors,file,folderHierarchy,spaces,allDatasets)(user)
    
    def f:((List[ExtractorInfo],File,List[Folder],List[ProjectSpace],List[Dataset]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (extractors,file,folderHierarchy,spaces,allDatasets) => (user) => apply(extractors,file,folderHierarchy,spaces,allDatasets)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/extractions/submitFileExtraction.scala.html
                    HASH: 9429790fb7058711969669d43e2e8c94b402a885
                    MATRIX: 698->1|992->168|1019->202|1055->204|1082->223|1121->225|1194->263|1225->286|1264->288|1372->361|1386->367|1440->399|1486->409|1501->415|1533->425|1573->429|1632->466|1670->487|1682->492|1720->493|1770->507|1800->528|1840->530|2385->1039|2400->1045|2413->1049|2457->1055|2536->1098|2551->1104|2595->1126|2696->1190|2746->1217|2813->1252|2927->1348|2940->1353|2979->1354|3095->1434|3110->1440|3173->1480|3214->1484|3228->1488|3267->1504|3322->1527|3364->1537|3410->1547|3447->1575|3487->1577|3537->1591|3557->1602|3570->1606|3616->1614|3736->1698|3751->1704|3798->1728|3845->1738|3857->1740|3885->1745|3926->1749|3977->1777|4032->1800|4074->1810|4120->1820|4144->1835|4157->1839|4203->1847|4321->1929|4336->1935|4395->1971|4443->1982|4455->1984|4481->1987|4528->1997|4540->1999|4575->2011|4615->2014|4673->2049|4724->2068|4830->2138|4845->2144|4887->2164|4933->2173|4947->2177|4979->2186|5019->2189|5076->2223|5640->2751|5655->2757|5697->2777|5736->2780|5749->2784|5780->2793|6054->3062|6227->3199|6263->3219|6303->3221|6402->3284|6412->3285|6439->3290|6502->3405|6621->3488|6631->3489|6658->3494|6698->3498|6752->3529|6802->3542|6816->3546|6842->3549|6950->3625|7148->3795|7177->3796|7394->3985|7423->3986|7452->3987|7510->4017|7539->4018|7632->4083|7661->4084|7778->4173|7807->4174|7975->4314|8004->4315|8099->4382|8128->4383|8230->4457|8259->4458|8357->4528|8386->4529|8517->4632|8546->4633|8584->4644|8613->4645|8660->4661
                    LINES: 20->1|24->1|25->3|26->4|26->4|26->4|28->6|28->6|28->6|29->7|29->7|29->7|29->7|29->7|29->7|29->7|29->7|31->9|31->9|31->9|32->10|32->10|32->10|40->18|40->18|40->18|40->18|41->19|41->19|41->19|41->19|41->19|42->20|47->25|47->25|47->25|48->26|48->26|48->26|48->26|48->26|48->26|49->27|50->28|51->29|51->29|51->29|52->30|52->30|52->30|52->30|53->31|53->31|53->31|53->31|53->31|53->31|53->31|53->31|54->32|55->33|56->34|56->34|56->34|56->34|57->35|57->35|57->35|57->35|57->35|57->35|57->35|57->35|57->35|57->35|57->35|58->36|59->37|59->37|59->37|59->37|59->37|59->37|59->37|59->37|72->50|72->50|72->50|72->50|72->50|72->50|81->59|86->64|86->64|86->64|88->66|88->66|88->66|89->67|90->68|90->68|90->68|90->68|90->68|90->68|90->68|90->68|92->70|98->76|98->76|101->79|101->79|101->79|102->80|102->80|102->80|102->80|103->81|103->81|107->85|107->85|109->87|109->87|111->89|111->89|113->91|113->91|115->93|115->93|116->94|116->94|118->96
                    -- GENERATED --
                */
            