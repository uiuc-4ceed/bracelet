
package views.html.extractions

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object submitExtraction extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[ExtractorInfo],File,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(extractors: List[ExtractorInfo], file: File):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.47*/("""

"""),_display_(Seq[Any](/*3.2*/main("Extractions")/*3.21*/ {_display_(Seq[Any](format.raw/*3.23*/("""
    <div class="row">
        <div class="col-xs-12">
            <h1>Submit file for extraction</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p>Submit this file to a specific extractor below by providing parameters as a JSON document and clicking
                the submit button. The paramaters field can be left empty.</p>
            <p>File name: <a href=""""),_display_(Seq[Any](/*13.37*/routes/*13.43*/.Files.file(file.id))),format.raw/*13.63*/("""">"""),_display_(Seq[Any](/*13.66*/file/*13.70*/.filename)),format.raw/*13.79*/("""</a></p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Extractor's Name</th>
                        <th>Parameters</th>
                        <th>Submit</th>
                    </tr>
                </thead>
                <tbody>
                    """),_display_(Seq[Any](/*27.22*/for(e <- extractors) yield /*27.42*/ {_display_(Seq[Any](format.raw/*27.44*/("""
                        <tr>
                            <td>"""),_display_(Seq[Any](/*29.34*/e/*29.35*/.name)),format.raw/*29.40*/("""</td>
                            <td><textarea id=""""),_display_(Seq[Any](/*30.48*/(e.name.replaceAll("\\.", "_")))),format.raw/*30.79*/("""_textarea"></textarea></td>
                            <td><button class="btn btn-primary" onclick="submit('"""),_display_(Seq[Any](/*31.83*/e/*31.84*/.name)),format.raw/*31.89*/("""','"""),_display_(Seq[Any](/*31.93*/(e.name.replaceAll("\\.", "_")))),format.raw/*31.124*/("""_textarea','"""),_display_(Seq[Any](/*31.137*/file/*31.141*/.id)),format.raw/*31.144*/("""')">Submit</button></td>
                        </tr>
                    """)))})),format.raw/*33.22*/("""
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        function submit(extractor_name, textbox_id, file_id) """),format.raw/*39.62*/("""{"""),format.raw/*39.63*/("""
            var params = $('#'+textbox_id).val();
            if (params === "") params = """"),format.raw/*41.42*/("""{"""),format.raw/*41.43*/("""}"""),format.raw/*41.44*/("""";
            var dataBody = """),format.raw/*42.28*/("""{"""),format.raw/*42.29*/("""'extractor': extractor_name, 'parameters': JSON.parse(params)"""),format.raw/*42.90*/("""}"""),format.raw/*42.91*/(""";
            var request = jsRoutes.api.Extractions.submitToExtractor(file_id).ajax("""),format.raw/*43.84*/("""{"""),format.raw/*43.85*/("""
                data: JSON.stringify(dataBody),
                type: 'POST',
                contentType: "application/json",
            """),format.raw/*47.13*/("""}"""),format.raw/*47.14*/(""");

            request.done(function (response, textStatus, jqXHR)"""),format.raw/*49.64*/("""{"""),format.raw/*49.65*/("""
                notify("Submitted successfully", "success");
            """),format.raw/*51.13*/("""}"""),format.raw/*51.14*/(""");

            request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*53.67*/("""{"""),format.raw/*53.68*/("""
                console.error("The following error occured: " + textStatus, errorThrown);
            """),format.raw/*55.13*/("""}"""),format.raw/*55.14*/(""");
        """),format.raw/*56.9*/("""}"""),format.raw/*56.10*/("""
    </script>
""")))})),format.raw/*58.2*/("""

"""))}
    }
    
    def render(extractors:List[ExtractorInfo],file:File): play.api.templates.HtmlFormat.Appendable = apply(extractors,file)
    
    def f:((List[ExtractorInfo],File) => play.api.templates.HtmlFormat.Appendable) = (extractors,file) => apply(extractors,file)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/extractions/submitExtraction.scala.html
                    HASH: 6d2db32d8d8409d9ea85fed16ba4b2492291fd59
                    MATRIX: 628->1|767->46|804->49|831->68|870->70|1322->486|1337->492|1379->512|1418->515|1431->519|1462->528|1900->930|1936->950|1976->952|2075->1015|2085->1016|2112->1021|2201->1074|2254->1105|2400->1215|2410->1216|2437->1221|2477->1225|2531->1256|2581->1269|2595->1273|2621->1276|2729->1352|2927->1522|2956->1523|3076->1615|3105->1616|3134->1617|3192->1647|3221->1648|3310->1709|3339->1710|3452->1795|3481->1796|3649->1936|3678->1937|3773->2004|3802->2005|3904->2079|3933->2080|4031->2150|4060->2151|4191->2254|4220->2255|4258->2266|4287->2267|4334->2283
                    LINES: 20->1|23->1|25->3|25->3|25->3|35->13|35->13|35->13|35->13|35->13|35->13|49->27|49->27|49->27|51->29|51->29|51->29|52->30|52->30|53->31|53->31|53->31|53->31|53->31|53->31|53->31|53->31|55->33|61->39|61->39|63->41|63->41|63->41|64->42|64->42|64->42|64->42|65->43|65->43|69->47|69->47|71->49|71->49|73->51|73->51|75->53|75->53|77->55|77->55|78->56|78->56|80->58
                    -- GENERATED --
                */
            