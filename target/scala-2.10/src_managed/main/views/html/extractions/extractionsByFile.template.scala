
package views.html.extractions

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object extractionsByFile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[List[Extraction],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(extractions: List[Extraction]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.33*/("""

<div class="well">
    """),_display_(Seq[Any](/*4.6*/if(extractions.size == 0)/*4.31*/ {_display_(Seq[Any](format.raw/*4.33*/("""
        <p>No extraction events recorded.</p>
    """)))}/*6.7*/else/*6.12*/{_display_(Seq[Any](format.raw/*6.13*/("""
        <table class="table">
            <thead><th>Extractor</th><th>Start</th><th>End</th><th>Status</th></thead>
            <tbody>
            """),_display_(Seq[Any](/*10.14*/for(e <- extractions) yield /*10.35*/ {_display_(Seq[Any](format.raw/*10.37*/("""
                <tr>
                    <td>"""),_display_(Seq[Any](/*12.26*/e/*12.27*/.extractor_id)),format.raw/*12.40*/("""</td>
                    <td>"""),_display_(Seq[Any](/*13.26*/e/*13.27*/.start)),format.raw/*13.33*/("""</td>
                    <td>"""),_display_(Seq[Any](/*14.26*/e/*14.27*/.end.getOrElse("N/A"))),format.raw/*14.48*/("""</td>
                    <td>"""),_display_(Seq[Any](/*15.26*/e/*15.27*/.status)),format.raw/*15.34*/("""</td>
                </tr>
            """)))})),format.raw/*17.14*/("""
            </tbody>
        </table>
    """)))})),format.raw/*20.6*/("""
</div>
"""))}
    }
    
    def render(extractions:List[Extraction]): play.api.templates.HtmlFormat.Appendable = apply(extractions)
    
    def f:((List[Extraction]) => play.api.templates.HtmlFormat.Appendable) = (extractions) => apply(extractions)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/extractions/extractionsByFile.scala.html
                    HASH: 278e4844eef7ec99b6f0a808840c7c92f5d4d7ca
                    MATRIX: 621->1|746->32|806->58|839->83|878->85|947->138|959->143|997->144|1184->295|1221->316|1261->318|1344->365|1354->366|1389->379|1456->410|1466->411|1494->417|1561->448|1571->449|1614->470|1681->501|1691->502|1720->509|1793->550|1868->594
                    LINES: 20->1|23->1|26->4|26->4|26->4|28->6|28->6|28->6|32->10|32->10|32->10|34->12|34->12|34->12|35->13|35->13|35->13|36->14|36->14|36->14|37->15|37->15|37->15|39->17|42->20
                    -- GENERATED --
                */
            