
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object extractorsInputTypes extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[String],Integer,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dtsinputtype:List[String], size:Integer):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import collection.JavaConverters._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.43*/("""
"""),format.raw/*4.75*/("""

"""),_display_(Seq[Any](/*6.2*/main("Extractors InputType")/*6.30*/{_display_(Seq[Any](format.raw/*6.31*/("""
	<div class="page-header">
		<h1>Extractors Input Types</h1>
	</div>
	"""),_display_(Seq[Any](/*10.3*/if(size == 0)/*10.16*/ {_display_(Seq[Any](format.raw/*10.18*/("""
		<div class="row">
			<div class="col-md-12">
				No Extractors found. Sorry!
			</div>
		</div>
	""")))}/*16.3*/else/*16.7*/{_display_(Seq[Any](format.raw/*16.8*/("""
		<div class="container" > 
			<div class="row-fluid" >
				<div>
					<table id='tablec'  style="table-layout: fixed;width: 30%" cellpadding="10"  cellspacing="0" border="1">
					<thead>
						<tr>
						<th>Input Types</th>
						</tr>
					</thead>
					<tbody>
						"""),_display_(Seq[Any](/*27.8*/for(req <- dtsinputtype) yield /*27.32*/{_display_(Seq[Any](format.raw/*27.33*/("""
							<tr>
							<td>"""),_display_(Seq[Any](/*29.13*/req)),format.raw/*29.16*/("""</td>
							</tr>			
	                      """)))})),format.raw/*31.25*/("""
					</tbody>
					</table>
	  	         </div>
	       </div>
	   </div>
	""")))})),format.raw/*37.3*/("""
""")))})),format.raw/*38.2*/("""
"""))}
    }
    
    def render(dtsinputtype:List[String],size:Integer): play.api.templates.HtmlFormat.Appendable = apply(dtsinputtype,size)
    
    def f:((List[String],Integer) => play.api.templates.HtmlFormat.Appendable) = (dtsinputtype,size) => apply(dtsinputtype,size)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/extractorsInputTypes.scala.html
                    HASH: 7fe46adfc5c9bd2d5e2f4dd17c6e119e01398d84
                    MATRIX: 616->1|795->97|827->121|906->42|934->170|971->173|1007->201|1045->202|1152->274|1174->287|1214->289|1333->390|1345->394|1383->395|1692->669|1732->693|1771->694|1832->719|1857->722|1935->768|2043->845|2076->847
                    LINES: 20->1|25->4|25->4|26->1|27->4|29->6|29->6|29->6|33->10|33->10|33->10|39->16|39->16|39->16|50->27|50->27|50->27|52->29|52->29|54->31|60->37|61->38
                    -- GENERATED --
                */
            