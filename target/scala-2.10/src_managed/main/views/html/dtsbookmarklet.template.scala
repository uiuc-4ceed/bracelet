
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object dtsbookmarklet extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(baseUrl: String):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import collection.JavaConverters._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.19*/("""
"""),format.raw/*4.75*/("""

"""),_display_(Seq[Any](/*6.2*/main("Bookmarklet")/*6.21*/ {_display_(Seq[Any](format.raw/*6.23*/("""

<center>
    <h1>Data Tilling Service (DTS)</h1>
    <h2 style="color:gray;">Drag icon to bookmark toolbar to install</h2>

    <!-- This implements a check to see if the bookmarklet has already been called, and if so, it simply re-runs the init function. You'll need to customize MyBookmarklet to the name of your object -->
    <a href="javascript:if(!window.DTSBookmarklet)"""),format.raw/*13.51*/("""{"""),format.raw/*13.52*/("""
          (function(d,t)"""),format.raw/*14.25*/("""{"""),format.raw/*14.26*/("""
            var timestamp=new Date();
            var g=d.createElement(t), s=d.getElementsByTagName(t)[0];
            g.src='"""),_display_(Seq[Any](/*17.21*/{baseUrl})),format.raw/*17.30*/("""/bookmarklet.js?v=' + timestamp.getTime();
            s.parentNode.insertBefore(g,s)
          """),format.raw/*19.11*/("""}"""),format.raw/*19.12*/("""(document,'script'));
      """),format.raw/*20.7*/("""}"""),format.raw/*20.8*/(""" else """),format.raw/*20.14*/("""{"""),format.raw/*20.15*/("""
        DTSBookmarklet.init();
      """),format.raw/*22.7*/("""}"""),format.raw/*22.8*/("""">
        <img src=""""),_display_(Seq[Any](/*23.20*/{baseUrl})),format.raw/*23.29*/("""/assets/javascripts/DTSbookmarklet/browndog-large-transparent.png")" width="200" alt="DTS">
    </a>
    <h3 style="font-family:arial;">dts_bookmarklet.js (alpha)</h3>
</center>

""")))})),format.raw/*28.2*/("""
"""))}
    }
    
    def render(baseUrl:String): play.api.templates.HtmlFormat.Appendable = apply(baseUrl)
    
    def f:((String) => play.api.templates.HtmlFormat.Appendable) = (baseUrl) => apply(baseUrl)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:27 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/dtsbookmarklet.scala.html
                    HASH: 5d3446f87b8dc4ad8bf35ea5a5d05668a9beb593
                    MATRIX: 596->1|751->73|783->97|862->18|890->146|927->149|954->168|993->170|1399->548|1428->549|1481->574|1510->575|1675->704|1706->713|1830->809|1859->810|1914->838|1942->839|1976->845|2005->846|2070->884|2098->885|2156->907|2187->916|2398->1096
                    LINES: 20->1|25->4|25->4|26->1|27->4|29->6|29->6|29->6|36->13|36->13|37->14|37->14|40->17|40->17|42->19|42->19|43->20|43->20|43->20|43->20|45->22|45->22|46->23|46->23|51->28
                    -- GENERATED --
                */
            