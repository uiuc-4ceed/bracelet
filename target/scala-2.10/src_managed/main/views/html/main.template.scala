
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object main extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[String,Html,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(title: String)(conhdd: Html)(implicit user: Option[models.User] = None):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services.AppConfiguration

import play.api.i18n.Messages


Seq[Any](format.raw/*1.74*/("""

"""),format.raw/*5.1*/("""<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" conhdd="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="description" conhdd="">
    <meta name="author" conhdd="">
    <title>"""),_display_(Seq[Any](/*13.13*/title)),format.raw/*13.18*/("""</title>

    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*15.35*/routes/*15.41*/.Assets.at("javascripts/webix/codebase/skins/flat.css"))),format.raw/*15.96*/("""" type="text/css">
    <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*16.50*/routes/*16.56*/.Assets.at("stylesheets/main.css"))),format.raw/*16.90*/("""">
    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*17.35*/routes/*17.41*/.Assets.at("stylesheets/themes/" + AppConfiguration.getTheme))),format.raw/*17.102*/("""">
    <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*18.50*/routes/*18.56*/.Assets.at("javascripts/jquery-ui-1.10.3.custom.min.css"))),format.raw/*18.113*/("""">

    <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*20.50*/routes/*20.56*/.Assets.at("stylesheets/pdf.css"))),format.raw/*20.89*/("""">
    <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*21.50*/routes/*21.56*/.Assets.at("stylesheets/tableborder.css"))),format.raw/*21.97*/("""">
    <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*22.50*/routes/*22.56*/.Assets.at("stylesheets/select2.min.css"))),format.raw/*22.97*/("""">
    <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*23.50*/routes/*23.56*/.Assets.at("stylesheets/select2-bootstrap.min.css"))),format.raw/*23.107*/("""">
    <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*24.50*/routes/*24.56*/.Assets.at("javascripts/jstree/themes/default/style.min.css"))),format.raw/*24.117*/("""">
    <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*25.50*/routes/*25.56*/.Assets.at("javascripts/sweetalert/css/sweetalert.css"))),format.raw/*25.111*/("""">
    <link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*26.50*/routes/*26.56*/.Assets.at("javascripts/jqueryUploader/css/uploadfile.css"))),format.raw/*26.115*/("""">

    <link rel="shortcut icon" type="image/png" href=""""),_display_(Seq[Any](/*28.55*/api/*28.58*/.routes.Logos.downloadPath("GLOBAL", "favicon", Some("images/favicon.png")))),format.raw/*28.133*/("""&x="""),_display_(Seq[Any](/*28.137*/System/*28.143*/.currentTimeMillis())),format.raw/*28.163*/("""">

    <script src=""""),_display_(Seq[Any](/*30.19*/routes/*30.25*/.Assets.at("javascripts/webix/codebase/webix.js"))),format.raw/*30.74*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*31.19*/routes/*31.25*/.Assets.at("javascripts/jquery-1.10.2.js"))),format.raw/*31.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*32.19*/routes/*32.25*/.Assets.at("javascripts/jquery-ui-1.10.3.custom.min.js"))),format.raw/*32.81*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*33.19*/routes/*33.25*/.Assets.at("javascripts/bootstrap.min.js"))),format.raw/*33.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*34.19*/routes/*34.25*/.Assets.at("javascripts/noty/packaged/jquery.noty.packaged.min.js"))),format.raw/*34.92*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*35.19*/routes/*35.25*/.Assets.at("javascripts/main.js"))),format.raw/*35.58*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*36.19*/routes/*36.25*/.Assets.at("javascripts/select2.min.js"))),format.raw/*36.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*37.19*/routes/*37.25*/.Assets.at("javascripts/jstree/jstree.js"))),format.raw/*37.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*38.19*/routes/*38.25*/.Assets.at("javascripts/clipboard.js"))),format.raw/*38.63*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*39.19*/routes/*39.25*/.Assets.at("javascripts/sweetalert/js/sweetalert-dev.min.js"))),format.raw/*39.86*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*40.19*/routes/*40.25*/.Assets.at("javascripts/jquery.validate.js"))),format.raw/*40.69*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*41.19*/routes/*41.25*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*41.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*42.19*/routes/*42.25*/.Assets.at("javascripts/jqueryUploader/jquery.form.js"))),format.raw/*42.80*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*43.19*/routes/*43.25*/.Assets.at("javascripts/jqueryUploader/jquery.uploadfile.js"))),format.raw/*43.86*/("""" type="text/javascript"></script>
    <script type="text/javascript" src=""""),_display_(Seq[Any](/*44.42*/routes/*44.48*/.Application.javascriptRoutes)),format.raw/*44.77*/(""""></script>
    <script>
    $(document).ready(function()"""),format.raw/*46.33*/("""{"""),format.raw/*46.34*/("""
        $('[data-toggle="tooltip"]').tooltip(); 
    """),format.raw/*48.5*/("""}"""),format.raw/*48.6*/(""");
    </script>
    <style>

        @media only screen and (max-width:1100px) """),format.raw/*52.52*/("""{"""),format.raw/*52.53*/("""
/*            .navbar a, li"""),format.raw/*53.28*/("""{"""),format.raw/*53.29*/("""
               display: none;
            """),format.raw/*55.13*/("""}"""),format.raw/*55.14*/("""*/

            .breadcrumb """),format.raw/*57.25*/("""{"""),format.raw/*57.26*/("""
                display: none;
            """),format.raw/*59.13*/("""}"""),format.raw/*59.14*/("""

            .navbar-brand """),format.raw/*61.27*/("""{"""),format.raw/*61.28*/("""
                display: none;
            """),format.raw/*63.13*/("""}"""),format.raw/*63.14*/("""

            .container """),format.raw/*65.24*/("""{"""),format.raw/*65.25*/("""
                padding-left: 0px;
                margin-left: 30px;
            """),format.raw/*68.13*/("""}"""),format.raw/*68.14*/("""

            h1 """),format.raw/*70.16*/("""{"""),format.raw/*70.17*/("""
                font-size: 24px;
            """),format.raw/*72.13*/("""}"""),format.raw/*72.14*/("""

            .nav>li>a """),format.raw/*74.23*/("""{"""),format.raw/*74.24*/("""
                display: none;
            """),format.raw/*76.13*/("""}"""),format.raw/*76.14*/("""

        """),format.raw/*78.9*/("""}"""),format.raw/*78.10*/("""

        @media only screen and (max-width: 550px) """),format.raw/*80.52*/("""{"""),format.raw/*80.53*/("""

            .button-list """),format.raw/*82.26*/("""{"""),format.raw/*82.27*/("""
                display: none;
            """),format.raw/*84.13*/("""}"""),format.raw/*84.14*/("""

            #downloadAndDelete """),format.raw/*86.32*/("""{"""),format.raw/*86.33*/("""
                display: none;
            """),format.raw/*88.13*/("""}"""),format.raw/*88.14*/("""

            #extractionsRow """),format.raw/*90.29*/("""{"""),format.raw/*90.30*/("""
                display: none;
            """),format.raw/*92.13*/("""}"""),format.raw/*92.14*/("""

            #commentsSection """),format.raw/*94.30*/("""{"""),format.raw/*94.31*/("""
                display: none;
            """),format.raw/*96.13*/("""}"""),format.raw/*96.14*/("""

            #aboutdesc """),format.raw/*98.24*/("""{"""),format.raw/*98.25*/("""
                display: none;
            """),format.raw/*100.13*/("""}"""),format.raw/*100.14*/("""

            .navbar-brand """),format.raw/*102.27*/("""{"""),format.raw/*102.28*/("""
                display: none;
            """),format.raw/*104.13*/("""}"""),format.raw/*104.14*/("""

            .breadcrumb """),format.raw/*106.25*/("""{"""),format.raw/*106.26*/("""
                display: none;
            """),format.raw/*108.13*/("""}"""),format.raw/*108.14*/("""

           #file-name-div """),format.raw/*110.27*/("""{"""),format.raw/*110.28*/("""
                display: none;
            """),format.raw/*112.13*/("""}"""),format.raw/*112.14*/("""

            #table-content """),format.raw/*114.28*/("""{"""),format.raw/*114.29*/("""
                display: none;
            """),format.raw/*116.13*/("""}"""),format.raw/*116.14*/("""

            .license-sec """),format.raw/*118.26*/("""{"""),format.raw/*118.27*/("""
                display: none;
            """),format.raw/*120.13*/("""}"""),format.raw/*120.14*/("""

            .datafile """),format.raw/*122.23*/("""{"""),format.raw/*122.24*/("""
                display: none;
            """),format.raw/*124.13*/("""}"""),format.raw/*124.14*/("""

            .datatags """),format.raw/*126.23*/("""{"""),format.raw/*126.24*/("""
                display: none;
            """),format.raw/*128.13*/("""}"""),format.raw/*128.14*/("""

            #moveToDatasetSec """),format.raw/*130.31*/("""{"""),format.raw/*130.32*/("""
                display: none;
            """),format.raw/*132.13*/("""}"""),format.raw/*132.14*/("""

            .hideTree """),format.raw/*134.23*/("""{"""),format.raw/*134.24*/("""
                display: none;
            """),format.raw/*136.13*/("""}"""),format.raw/*136.14*/("""

            .navbar button"""),format.raw/*138.27*/("""{"""),format.raw/*138.28*/("""
                display: none;
            """),format.raw/*140.13*/("""}"""),format.raw/*140.14*/("""

            .tab-pane """),format.raw/*142.23*/("""{"""),format.raw/*142.24*/("""
                display: block;
            """),format.raw/*144.13*/("""}"""),format.raw/*144.14*/("""

        """),format.raw/*146.9*/("""}"""),format.raw/*146.10*/("""

    </style>        
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
<!--     <style>
        .uploader-bg """),format.raw/*155.22*/("""{"""),format.raw/*155.23*/("""
            background-color:#004466;
             border-right:1px solid gray;
             border-left:1px solid gray;
        """),format.raw/*159.9*/("""}"""),format.raw/*159.10*/(""" 

        .search-bg """),format.raw/*161.20*/("""{"""),format.raw/*161.21*/("""
            background-color:gray;
            border:1px solid gray;
        """),format.raw/*164.9*/("""}"""),format.raw/*164.10*/("""

        .up, .down        """),format.raw/*166.27*/("""{"""),format.raw/*166.28*/(""" cursor: pointer; """),format.raw/*166.46*/("""}"""),format.raw/*166.47*/("""
        .up:after         """),format.raw/*167.27*/("""{"""),format.raw/*167.28*/(""" conhdd: '△'; """),format.raw/*167.42*/("""}"""),format.raw/*167.43*/("""
        .up:hover:after   """),format.raw/*168.27*/("""{"""),format.raw/*168.28*/(""" conhdd: '▲'; """),format.raw/*168.42*/("""}"""),format.raw/*168.43*/("""
        .down:after       """),format.raw/*169.27*/("""{"""),format.raw/*169.28*/(""" conhdd: '▽'; """),format.raw/*169.42*/("""}"""),format.raw/*169.43*/("""
        .down:hover:after """),format.raw/*170.27*/("""{"""),format.raw/*170.28*/(""" conhdd: '▼'; """),format.raw/*170.42*/("""}"""),format.raw/*170.43*/("""
    </style> -->
</head>
<body>
    <div id="wrap">
      <div class="navbar navbar-fixed-top navbar-inverse" id="navv">
        <div class="container-fluid">
            <div class="navbar-header">
            <a class="navbar-brand" href=""""),_display_(Seq[Any](/*178.44*/routes/*178.50*/.Application.index())),format.raw/*178.70*/("""">
            <img src=""""),_display_(Seq[Any](/*179.24*/routes/*179.30*/.Assets.at("images/atom_white.png"))),format.raw/*179.65*/("""" height="35" style="margin-top:-9px;"></a>
  
                <!-- <button type="button" class="btn btn-dark navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  Options
                </button> -->
                <button class="btn btn-dark navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarToggleExternalConhdd" aria-expanded="false" aria-label="Toggle navigation">
                    Options
                </button>

                

<!--                 """),_display_(Seq[Any](/*190.23*/services/*190.31*/.DI.injector.getInstance(classOf[services.LogoService]).get("GLOBAL", "logo")/*190.108*/ match/*190.114*/ {/*191.21*/case Some(logo) =>/*191.39*/ {_display_(Seq[Any](format.raw/*191.41*/("""
                        <a class="navbar-brand" style="padding-top: 5px; padding-bottom: 5px" href=""""),_display_(Seq[Any](/*192.102*/routes/*192.108*/.Application.index())),format.raw/*192.128*/("""">
                            <img class="pull-left" style="height: 100%" src=""""),_display_(Seq[Any](/*193.79*/api/*193.82*/.routes.Logos.downloadPath("GLOBAL", "logo"))),format.raw/*193.126*/("""" alt="LOGO">
                        </a>
                        """),_display_(Seq[Any](/*195.26*/if(logo.showText)/*195.43*/ {_display_(Seq[Any](format.raw/*195.45*/("""
                            <a class="navbar-brand" href=""""),_display_(Seq[Any](/*196.60*/routes/*196.66*/.Application.index())),format.raw/*196.86*/("""">
                                """),_display_(Seq[Any](/*197.34*/(AppConfiguration.getDisplayName))),format.raw/*197.67*/("""
                            </a>
                        """)))})),format.raw/*199.26*/("""
                    """)))}/*201.21*/case None =>/*201.33*/ {_display_(Seq[Any](format.raw/*201.35*/("""
                        <a class="navbar-brand" href=""""),_display_(Seq[Any](/*202.56*/routes/*202.62*/.Application.index())),format.raw/*202.82*/("""">
                            """),_display_(Seq[Any](/*203.30*/(AppConfiguration.getDisplayName))),format.raw/*203.63*/("""
                        </a>
                    """)))}})),format.raw/*206.18*/(""" -->
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                """),_display_(Seq[Any](/*210.18*/if(user.isDefined)/*210.36*/ {_display_(Seq[Any](format.raw/*210.38*/("""
                     <li class="divider-vertical"></li>

                    <li class="dropdown visible-lg">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            You <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
<!--                             <li><a href=""""),_display_(Seq[Any](/*218.48*/routes/*218.54*/.Application.index())),format.raw/*218.74*/(""""><span class="glyphicon glyphicon-user"></span> My Dashboard</a></li>
 -->                            """),_display_(Seq[Any](/*219.34*/if(play.Play.application().configuration().getBoolean("enablePublic"))/*219.104*/ {_display_(Seq[Any](format.raw/*219.106*/("""
                                <li><a href=""""),_display_(Seq[Any](/*220.47*/routes/*220.53*/.Spaces.list(owner=Some(user.get.id.stringify),showPublic=false))),format.raw/*220.117*/(""""><span class="glyphicon glyphicon-hdd"></span> My Spaces</a></li>
                                <li><a href=""""),_display_(Seq[Any](/*221.47*/routes/*221.53*/.Collections.list(owner=Some(user.get.id.stringify),showPublic=false))),format.raw/*221.122*/(""""><span class="glyphicon glyphicon-folder-close"></span> My Collections</a></li>
                                
                                <li><a href=""""),_display_(Seq[Any](/*223.47*/routes/*223.53*/.Datasets.list(owner=Some(user.get.id.stringify),showPublic=false))),format.raw/*223.119*/(""""><span class="glyphicon glyphicon-briefcase"></span> My Datasets</a></li>
				<!-- <li><a href=""""),_display_(Seq[Any](/*224.24*/routes/*224.30*/.DataAnalysis.listSessions())),format.raw/*224.58*/(""""><span class="glyphicon glyphicon-folder-close"></span> My Jupyter Sessions</a></li> -->
<!--                                 <li><a href=""""),_display_(Seq[Any](/*225.52*/routes/*225.58*/.Notebook.listNotebooks())),format.raw/*225.83*/(""""><span class="glyphicon glyphicon-folder-close"></span> My Notebooks</a></li>
 -->                            """)))}/*226.35*/else/*226.40*/{_display_(Seq[Any](format.raw/*226.41*/("""
                                <li><a href=""""),_display_(Seq[Any](/*227.47*/routes/*227.53*/.Spaces.list(owner=Some(user.get.id.stringify)))),format.raw/*227.100*/(""""><span class="glyphicon glyphicon-hdd"></span> My Spaces</a></li>
                                <li><a href=""""),_display_(Seq[Any](/*228.47*/routes/*228.53*/.Datasets.list(owner=Some(user.get.id.stringify)))),format.raw/*228.102*/(""""><span class="glyphicon glyphicon-briefcase"></span> My Datasets</a></li>
                                <li><a href=""""),_display_(Seq[Any](/*229.47*/routes/*229.53*/.Collections.list(owner=Some(user.get.id.stringify)))),format.raw/*229.105*/(""""><span class="glyphicon glyphicon-folder-close"></span> My Collections</a></li>
				 <!-- <li><a href=""""),_display_(Seq[Any](/*230.25*/routes/*230.31*/.DataAnalysis.listSessions())),format.raw/*230.59*/(""""><span class="glyphicon glyphicon-folder-close"></span> My Jupyter Sessions</a></li> -->
<!--                                 <li><a href=""""),_display_(Seq[Any](/*231.52*/routes/*231.58*/.Notebook.listNotebooks())),format.raw/*231.83*/(""""><span class="glyphicon glyphicon-folder-close"></span> My Notebooks</a></li>
 -->
                            """)))})),format.raw/*233.30*/("""

                        </ul>
                    </li>
                """)))})),format.raw/*237.18*/("""
                    """),_display_(Seq[Any](/*238.22*/if(user.isDefined)/*238.40*/ {_display_(Seq[Any](format.raw/*238.42*/("""
                        <li class="dropdown visible-lg">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Shared <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href=""""),_display_(Seq[Any](/*244.47*/routes/*244.53*/.Spaces.list(showOnlyShared = play.Play.application().configuration().getBoolean("showOnlySharedInExplore")))),format.raw/*244.161*/(""""><span class="glyphicon glyphicon-hdd"> </span> Shared Spaces</a></li>
                                """),_display_(Seq[Any](/*245.34*/user/*245.38*/ match/*245.44*/ {/*246.37*/case Some(x) =>/*246.52*/ {_display_(Seq[Any](format.raw/*246.54*/("""
                                        """),_display_(Seq[Any](/*247.42*/if(play.Play.application().configuration().getBoolean("verifySpaces") && api.Permission.checkServerAdmin(user))/*247.153*/ {_display_(Seq[Any](format.raw/*247.155*/("""
                                            """),_display_(Seq[Any](/*248.46*/if(play.Play.application().configuration().getBoolean("showOnlySharedInExplore"))/*248.127*/ {_display_(Seq[Any](format.raw/*248.129*/("""
                                                <li><a href=""""),_display_(Seq[Any](/*249.63*/routes/*249.69*/.Spaces.list(onlyTrial = true, showOnlyShared = true))),format.raw/*249.122*/(""""><span class="glyphicon glyphicon-hdd"> </span>
                                                    Trial """),_display_(Seq[Any](/*250.60*/Messages("spaces.title.shared"))),format.raw/*250.91*/("""</a></li>
                                            """)))}/*251.47*/else/*251.52*/{_display_(Seq[Any](format.raw/*251.53*/("""
                                                <li><a href=""""),_display_(Seq[Any](/*252.63*/routes/*252.69*/.Spaces.list(onlyTrial = true))),format.raw/*252.99*/(""""><span class="glyphicon glyphicon-hdd"> </span>
                                                    Trial """),_display_(Seq[Any](/*253.60*/Messages("spaces.title.shared"))),format.raw/*253.91*/("""</a></li>
                                            """)))})),format.raw/*254.46*/("""

                                        """)))})),format.raw/*256.42*/("""
                                    """)))}/*258.37*/case None =>/*258.49*/ {_display_(Seq[Any](format.raw/*258.51*/("""

                                    """)))}})),format.raw/*261.34*/("""
                                """),_display_(Seq[Any](/*262.34*/if(user.isDefined)/*262.52*/ {_display_(Seq[Any](format.raw/*262.54*/("""
                                    """),_display_(Seq[Any](/*263.38*/if(play.Play.application.configuration().getBoolean("showOnlySharedInExplore"))/*263.117*/ {_display_(Seq[Any](format.raw/*263.119*/("""
                                        <li><a href=""""),_display_(Seq[Any](/*264.55*/routes/*264.61*/.Datasets.list(showOnlyShared = true))),format.raw/*264.98*/(""""><span class="glyphicon glyphicon-briefcase"> </span> Shared Datasets</a></li>
                                        <li><a href=""""),_display_(Seq[Any](/*265.55*/routes/*265.61*/.Collections.list(showOnlyShared = true))),format.raw/*265.101*/(""""><span class="glyphicon glyphicon-folder-close"> </span> Shared Collections</a></li>
                                    """)))}/*266.39*/else/*266.44*/{_display_(Seq[Any](format.raw/*266.45*/("""
                                        <li><a href=""""),_display_(Seq[Any](/*267.55*/routes/*267.61*/.Datasets.list(""))),format.raw/*267.79*/(""""><span class="glyphicon glyphicon-briefcase"> </span> Shared Datasets</a></li>
                                        <li><a href=""""),_display_(Seq[Any](/*268.55*/routes/*268.61*/.Collections.list(""))),format.raw/*268.82*/(""""><span class="glyphicon glyphicon-folder-close"> </span> Shared Collections</a></li>
                                    """)))})),format.raw/*269.38*/("""


<!--                                     <li><a href=""""),_display_(Seq[Any](/*272.56*/routes/*272.62*/.Users.getUsers())),format.raw/*272.79*/(""""><span class="glyphicon glyphicon-user"></span>
                                        Users</a></li>
 -->                                """)))})),format.raw/*274.38*/("""
                                    <!--                             <li><a href=""""),_display_(Seq[Any](/*275.84*/routes/*275.90*/.Tags.tagListOrdered)),format.raw/*275.110*/(""""> <span class="glyphicon glyphicon-tags"> </span>
                                Ordered Tags</a></li>
                                <li><a href=""""),_display_(Seq[Any](/*277.47*/routes/*277.53*/.Tags.tagListWeighted)),format.raw/*277.74*/(""""><span class="glyphicon glyphicon-tags"> </span>
                                    Weighted Tags</a></li>
                                <li><a href=""""),_display_(Seq[Any](/*279.47*/routes/*279.53*/.Metadata.search)),format.raw/*279.69*/(""""> <span class="glyphicon glyphicon-search"> </span>
                                    Advanced Search</a></li>
                                -->                        </ul>
                        </li>
                    """)))})),format.raw/*283.22*/("""
                """),_display_(Seq[Any](/*284.18*/if(user.isDefined)/*284.36*/ {_display_(Seq[Any](format.raw/*284.38*/("""
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Create <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
<!--                             <li><a href=""""),_display_(Seq[Any](/*290.48*/routes/*290.54*/.Spaces.newSpace())),format.raw/*290.72*/("""" class="visible-lg"><span class="glyphicon glyphicon-hdd"> </span>
                                """),_display_(Seq[Any](/*291.34*/Messages("spaces.title"))),format.raw/*291.58*/("""
                            </a></li>
 -->                            <li><a href=""""),_display_(Seq[Any](/*293.47*/routes/*293.53*/.Collections.newCollection(None))),format.raw/*293.85*/(""""><span class="glyphicon glyphicon-folder-close"> </span>
                                """),_display_(Seq[Any](/*294.34*/Messages("collections.title"))),format.raw/*294.63*/("""</a></li>
                            <li><a href=""""),_display_(Seq[Any](/*295.43*/routes/*295.49*/.Datasets.newDataset(None, None))),format.raw/*295.81*/(""""><span class="glyphicon glyphicon-briefcase"> </span>
                                """),_display_(Seq[Any](/*296.34*/Messages("datasets.title"))),format.raw/*296.60*/("""</a></li>
<!--                             <li><a href=""""),_display_(Seq[Any](/*297.48*/routes/*297.54*/.T2C2.newTemplate(None))),format.raw/*297.77*/("""" class="visible-lg"><span class="glyphicon glyphicon-th-list"> </span>
                                Templates</a></li>
 -->                        </ul>
                    </li>
                """)))})),format.raw/*301.18*/("""

                    """),_display_(Seq[Any](/*303.22*/if(user.isDefined)/*303.40*/ {_display_(Seq[Any](format.raw/*303.42*/("""
                        <li class="divider-vertical"></li>
<!-- 
                        <li class="dropdown visible-lg">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Trash <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                """),_display_(Seq[Any](/*311.34*/if(play.Play.application().configuration().getBoolean("enablePublic"))/*311.104*/ {_display_(Seq[Any](format.raw/*311.106*/("""
                                    <li><a href=""""),_display_(Seq[Any](/*312.51*/routes/*312.57*/.Datasets.list(owner=Some(user.get.id.stringify),showPublic=false,showTrash=true))),format.raw/*312.138*/(""""><span class="glyphicon glyphicon-briefcase"></span> """),_display_(Seq[Any](/*312.193*/Messages("datasets.title"))),format.raw/*312.219*/("""</a></li>
                                    <li><a href=""""),_display_(Seq[Any](/*313.51*/routes/*313.57*/.Collections.list(owner=Some(user.get.id.stringify),showPublic=false,showTrash=true))),format.raw/*313.141*/(""""><span class="glyphicon glyphicon-folder-close"></span>&nbsp;Collections</a></li>
                                """)))}/*314.35*/else/*314.40*/{_display_(Seq[Any](format.raw/*314.41*/("""
                                    <li><a href=""""),_display_(Seq[Any](/*315.51*/routes/*315.57*/.Datasets.list(owner=Some(user.get.id.stringify),showTrash=true))),format.raw/*315.121*/(""""><span class="glyphicon glyphicon-briefcase"></span> """),_display_(Seq[Any](/*315.176*/Messages("datasets.title"))),format.raw/*315.202*/("""</a></li>
                                    <li><a href=""""),_display_(Seq[Any](/*316.51*/routes/*316.57*/.Collections.list(owner=Some(user.get.id.stringify),showTrash=true))),format.raw/*316.124*/(""""><span class="glyphicon glyphicon-folder-close"></span>Collections</a></li>
                                """)))})),format.raw/*317.34*/("""

                            </ul>
                        </li> -->
                    """)))})),format.raw/*321.22*/("""
                    <li class="dropdown visible-lg">
                        <a href="#" class="dropdown-toggle nav-icon" data-toggle="dropdown" title="Help">Help<b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="http://4ceed.github.io/docs" target="_blank">Docs</a></li>
                            <li><a target="_blank" href=""""),_display_(Seq[Any](/*326.59*/routes/*326.65*/.Application.email(""))),format.raw/*326.87*/("""">Contact support</a></li>
                            """),format.raw/*331.32*/("""
                            <li><a targe"tos" href=""""),_display_(Seq[Any](/*332.54*/routes/*332.60*/.Application.tos(None))),format.raw/*332.82*/("""">Terms of service</a></li>
                            """),_display_(Seq[Any](/*333.30*/play/*333.34*/.Play.application.configuration.getConfigList("helpMenu").map/*333.95*/ { helpItem =>_display_(Seq[Any](format.raw/*333.109*/("""
                              <li><a href=""""),_display_(Seq[Any](/*334.45*/helpItem/*334.53*/.getString("url"))),format.raw/*334.70*/("""" target="_blank">"""),_display_(Seq[Any](/*334.89*/helpItem/*334.97*/.getString("label"))),format.raw/*334.116*/("""</a></li>

                            """)))})),format.raw/*336.30*/("""
                        </ul>
                    </li>
                    """),_display_(Seq[Any](/*339.22*/if(play.api.Play.current.plugin[services.PostgresPlugin].isDefined)/*339.89*/ {_display_(Seq[Any](format.raw/*339.91*/("""
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">"""),_display_(Seq[Any](/*341.89*/(AppConfiguration.getSensorsTitle))),format.raw/*341.123*/(""" <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href=""""),_display_(Seq[Any](/*343.47*/routes/*343.53*/.Geostreams.newSensor)),format.raw/*343.74*/("""">Create</a></li>
                                <li><a href=""""),_display_(Seq[Any](/*344.47*/routes/*344.53*/.Geostreams.list)),format.raw/*344.69*/("""">List</a></li>
                                <li><a href=""""),_display_(Seq[Any](/*345.47*/routes/*345.53*/.Geostreams.map)),format.raw/*345.68*/("""">Map</a></li>
                            </ul>
                        </li>
                    """)))})),format.raw/*348.22*/("""

                </ul>

                <ul class="nav navbar-nav navbar-right">
                """),_display_(Seq[Any](/*353.18*/if(user.isDefined)/*353.36*/ {_display_(Seq[Any](format.raw/*353.38*/("""

<!--                     <li class="dropdown uploader-bg visible-lg">
                        <a style="color:white;" href="#" class="dropdown-toggle nav-icon" data-toggle="dropdown" title="">Uploaders</span>
                        <b class="caret"></b></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href=""""),_display_(Seq[Any](/*360.43*/routes/*360.49*/.T2C2.uploader())),format.raw/*360.65*/("""">Standard Upload</a>
                            </li>
                            <li>
                                <a href=""""),_display_(Seq[Any](/*363.43*/routes/*363.49*/.T2C2.zipUploader())),format.raw/*363.68*/("""">Zip Upload</a>
                            </li>
                                                          
                        </ul>
                     </li>     -->
               
                """),_display_(Seq[Any](/*369.18*/if(play.api.Play.current.plugin[services.ElasticsearchPlugin].isDefined)/*369.90*/ {_display_(Seq[Any](format.raw/*369.92*/("""
                        <li>
                            <form class="navbar-form" role="search" action=""""),_display_(Seq[Any](/*371.78*/routes/*371.84*/.Search.search(""))),format.raw/*371.102*/("""">
                                <div class="input-group" style="margin-top:5px;">
                                    <input type="text" class="form-control" placeholder="Search" name="query">
                                    <div class="input-group-btn">
                                        <button style="border:1px solid gray;" class="btn btn-default search-bg"  type="submit">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </li>
                    """)))})),format.raw/*382.22*/("""
<!--                 <li class="visible-lg"><a href="https://jupyter.4ceed.illinois.edu">Jupyter Hub</a></li>
 -->                """)))})),format.raw/*384.22*/("""

                    """),_display_(Seq[Any](/*386.22*/user/*386.26*/ match/*386.32*/ {/*387.25*/case Some(x) =>/*387.40*/ {_display_(Seq[Any](format.raw/*387.42*/("""
                            """),_display_(Seq[Any](/*388.30*/if(api.Permission.checkServerAdmin(user))/*388.71*/ {_display_(Seq[Any](format.raw/*388.73*/("""
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle nav-icon" data-toggle="dropdown" title="Administration">
                                        <span class="glyphicon glyphicon-cog"></span><b class="caret"></b></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href=""""),_display_(Seq[Any](/*393.55*/routes/*393.61*/.Admin.customize)),format.raw/*393.77*/("""">Customize</a></li>
                                        <li><a href=""""),_display_(Seq[Any](/*394.55*/routes/*394.61*/.Admin.tos)),format.raw/*394.71*/("""">Terms of Service</a></li>
                                        <li><a href=""""),_display_(Seq[Any](/*395.55*/routes/*395.61*/.Admin.users)),format.raw/*395.73*/("""">Users</a></li>
                                        <li><a href=""""),_display_(Seq[Any](/*396.55*/routes/*396.61*/.Admin.listRoles)),format.raw/*396.77*/("""">List Roles</a></li>
                                        <li><a href=""""),_display_(Seq[Any](/*397.55*/routes/*397.61*/.Admin.createRole)),format.raw/*397.78*/("""">Add Role</a></li>
                                        <li><a href=""""),_display_(Seq[Any](/*398.55*/routes/*398.61*/.Extractors.listAllExtractions)),format.raw/*398.91*/("""">Extractions</a></li>
                                        <li><a href=""""),_display_(Seq[Any](/*399.55*/routes/*399.61*/.Admin.adminIndex)),format.raw/*399.78*/("""">Indexes</a></li>
                                        <li><a href=""""),_display_(Seq[Any](/*400.55*/routes/*400.61*/.Admin.viewDumpers)),format.raw/*400.79*/("""">Dumps</a></li>
                                        """),format.raw/*403.44*/("""
                                        """),_display_(Seq[Any](/*404.42*/play/*404.46*/.api.Play.current.plugin[services.PostgresPlugin]/*404.95*/ match/*404.101*/ {/*405.45*/case Some(db) =>/*405.61*/ {_display_(Seq[Any](format.raw/*405.63*/("""
                                                <li><a href=""""),_display_(Seq[Any](/*406.63*/routes/*406.69*/.Admin.sensors)),format.raw/*406.83*/("""">"""),_display_(Seq[Any](/*406.86*/(AppConfiguration.getSensorsTitle))),format.raw/*406.120*/("""</a></li>
                                            """)))}/*408.45*/case None =>/*408.57*/ {}})),format.raw/*409.42*/("""
                                        """),_display_(Seq[Any](/*410.42*/if(user.exists(_.superAdminMode))/*410.75*/ {_display_(Seq[Any](format.raw/*410.77*/("""
                                            <li><a href="javascript:toggelServerAdmin();">Drop SuperAdmin</a></li>
                                        """)))}/*412.43*/else/*412.48*/{_display_(Seq[Any](format.raw/*412.49*/("""
                                            <li><a href="javascript:toggelServerAdmin();">Enable SuperAdmin</a></li>
                                        """)))})),format.raw/*414.42*/("""

                                    </ul>
                                </li>
                            """)))})),format.raw/*418.30*/("""



                            <li class="dropdown">
                            <a href=""""),_display_(Seq[Any](/*423.39*/securesocial/*423.51*/.controllers.routes.LoginPage.logout)),format.raw/*423.87*/("""" title="Logout from the system">
                                            <span class="glyphicon glyphicon-log-out"></span> Logout </a>
                            </li>
<!--                             <li class=dropdown">
                                <a href="#" class="dropdown-toggle nav-icon" data-toggle="dropdown" title=""""),_display_(Seq[Any](/*427.109*/x/*427.110*/.format(false))),format.raw/*427.124*/("""">
                                    <span class="glyphicon glyphicon-user"></span><b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href=""""),_display_(Seq[Any](/*432.51*/routes/*432.57*/.Profile.viewProfileUUID(x.id))),format.raw/*432.87*/("""">View Profile</a>
                                    </li>
                                    
                                    <li>
                                        <a href=""""),_display_(Seq[Any](/*436.51*/securesocial/*436.63*/.controllers.routes.LoginPage.logout)),format.raw/*436.99*/("""" title="Logout from the system">
                                            <span class="glyphicon glyphicon-log-out"></span> Logout </a>
                                    </li>
                                </ul>
                              </li> -->

                        """)))}/*443.25*/case None =>/*443.37*/ {_display_(Seq[Any](format.raw/*443.39*/("""
<!--                              <li>
                                <a href=""""),_display_(Seq[Any](/*445.43*/securesocial/*445.55*/.core.providers.utils.RoutesHelper.startSignUp())),format.raw/*445.103*/("""" title="Sign up to the system">Sign up</a>
                             </li>
 -->                            <li>
                                <a href=""""),_display_(Seq[Any](/*448.43*/securesocial/*448.55*/.controllers.routes.LoginPage.login)),format.raw/*448.90*/("""" title="Login to the system"><span class="glyphicon glyphicon-log-in"></span> Login</a>
                            </li>
                        """)))}})),format.raw/*451.22*/("""
                </ul>
            </div>
    """),_display_(Seq[Any](/*454.6*/if(user.exists(_.superAdminMode))/*454.39*/ {_display_(Seq[Any](format.raw/*454.41*/("""
        <div class="alert alert-warning" role="alert">
            <span class="glyphicon glyphicon-warning-sign"></span>
            You have temporary access to administrative functions. <a href="javascript:toggelServerAdmin();">Drop access</a> if you no longer require it.
        </div>
    """)))})),format.raw/*459.6*/("""
    </div>
    </div>
    <div class="container">
        """),_display_(Seq[Any](/*463.10*/conhdd)),format.raw/*463.16*/(""" 
    </div>
    <div id="push"></div>
  </div> 
  """),_display_(Seq[Any](/*467.4*/if(user.isDefined)/*467.22*/ {_display_(Seq[Any](format.raw/*467.24*/("""

      <div id="footer">
        <div class="container">
            <p class="text-muted">&nbsp;</p>
        </div>
      </div>
  """)))})),format.raw/*474.4*/("""
"""),_display_(Seq[Any](/*475.2*/if(AppConfiguration.getGoogleAnalytics != "")/*475.47*/ {_display_(Seq[Any](format.raw/*475.49*/("""
    <script>
            (function(i,s,o,g,r,a,m)"""),format.raw/*477.37*/("""{"""),format.raw/*477.38*/("""i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()"""),format.raw/*477.88*/("""{"""),format.raw/*477.89*/("""
                        (i[r].q=i[r].q||[]).push(arguments)"""),format.raw/*478.60*/("""}"""),format.raw/*478.61*/(""",i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            """),format.raw/*480.13*/("""}"""),format.raw/*480.14*/(""")(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', '"""),_display_(Seq[Any](/*481.28*/AppConfiguration/*481.44*/.getGoogleAnalytics)),format.raw/*481.63*/("""', 'auto');
            ga('send', 'pageview');
    </script>
""")))})),format.raw/*484.2*/("""

<script>
    function toggelServerAdmin() """),format.raw/*487.34*/("""{"""),format.raw/*487.35*/("""
        $.cookie("superAdmin", $.cookie("superAdmin") != "true", """),format.raw/*488.66*/("""{"""),format.raw/*488.67*/(""" path: '/' """),format.raw/*488.78*/("""}"""),format.raw/*488.79*/(""");
        location.reload();
    """),format.raw/*490.5*/("""}"""),format.raw/*490.6*/("""
</script>

<script>
    var repeatInt = 1; // check for session expiration in minutes

    if ("""),_display_(Seq[Any](/*496.10*/user/*496.14*/.isDefined)),format.raw/*496.24*/(""") """),format.raw/*496.26*/("""{"""),format.raw/*496.27*/("""
        var intervalCount = setInterval(function()"""),format.raw/*497.51*/("""{"""),format.raw/*497.52*/("""checkSession()"""),format.raw/*497.66*/("""}"""),format.raw/*497.67*/(""", repeatInt*60000);
    """),format.raw/*498.5*/("""}"""),format.raw/*498.6*/("""

    function checkSession() """),format.raw/*500.29*/("""{"""),format.raw/*500.30*/("""
        nowTime = new Date();
        var request = jsRoutes.controllers.Login.isLoggedIn().ajax("""),format.raw/*502.68*/("""{"""),format.raw/*502.69*/("""
            type: 'GET'
        """),format.raw/*504.9*/("""}"""),format.raw/*504.10*/(""");
        request.done(function(response, textStatus, jqXHR) """),format.raw/*505.60*/("""{"""),format.raw/*505.61*/("""
            //console.log(response);
            if (response === "no") """),format.raw/*507.36*/("""{"""),format.raw/*507.37*/("""
                clearInterval(intervalCount);    //stop the timer
                var modalHTML = '<div class="modal" role="dialog">';
                modalHTML += '<div class="modal-dialog">';
                modalHTML += '<div class="modal-conhdd">';
                modalHTML += '<div class="modal-body">';
                modalHTML += '<p>Your session has expired as of ' + nowTime.toTimeString() + '.<br />Press OK to log in again.</p>';
                modalHTML += '</div>';
                modalHTML += '<div class="modal-footer">';
                modalHTML += '<a type="button" class="btn btn-primary" href=""""),_display_(Seq[Any](/*516.79*/securesocial/*516.91*/.controllers.routes.LoginPage.logout)),format.raw/*516.127*/(""""><span class="glyphicon glyphicon-ok"></span> OK</a>';
                modalHTML += '</div>';
                modalHTML += '</div>';
                modalHTML += '</div>';
                modalHTML += '</div>';
                var confirmModal = $(modalHTML);
                confirmModal.modal("""),format.raw/*522.36*/("""{"""),format.raw/*522.37*/("""backdrop: "static", keyboard: false"""),format.raw/*522.72*/("""}"""),format.raw/*522.73*/(""");
                confirmModal.modal("show");
            """),format.raw/*524.13*/("""}"""),format.raw/*524.14*/("""
        """),format.raw/*525.9*/("""}"""),format.raw/*525.10*/(""");
        request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*526.63*/("""{"""),format.raw/*526.64*/("""
            console.error("The following error occurred: " + textStatus, errorThrown);
        """),format.raw/*528.9*/("""}"""),format.raw/*528.10*/(""");
    """),format.raw/*529.5*/("""}"""),format.raw/*529.6*/("""
</script>
</body>
</html>
"""))}
    }
    
    def render(title:String,conhdd:Html,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(title)(conhdd)(user)
    
    def f:((String) => (Html) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (title) => (conhdd) => (user) => apply(title)(conhdd)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:46:55 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/main.scala.html
                    HASH: 4c7e91c20465badc4e193666684f007eb5efde5e
                    MATRIX: 611->1|841->73|869->140|1167->402|1194->407|1274->451|1289->457|1366->512|1470->580|1485->586|1541->620|1614->657|1629->663|1713->724|1801->776|1816->782|1896->839|1985->892|2000->898|2055->931|2143->983|2158->989|2221->1030|2309->1082|2324->1088|2387->1129|2475->1181|2490->1187|2564->1238|2652->1290|2667->1296|2751->1357|2839->1409|2854->1415|2932->1470|3020->1522|3035->1528|3117->1587|3211->1645|3223->1648|3321->1723|3362->1727|3378->1733|3421->1753|3479->1775|3494->1781|3565->1830|3654->1883|3669->1889|3733->1931|3822->1984|3837->1990|3915->2046|4004->2099|4019->2105|4083->2147|4172->2200|4187->2206|4276->2273|4365->2326|4380->2332|4435->2365|4524->2418|4539->2424|4601->2464|4690->2517|4705->2523|4769->2565|4858->2618|4873->2624|4933->2662|5022->2715|5037->2721|5120->2782|5209->2835|5224->2841|5290->2885|5379->2938|5394->2944|5458->2986|5547->3039|5562->3045|5639->3100|5728->3153|5743->3159|5826->3220|5938->3296|5953->3302|6004->3331|6089->3388|6118->3389|6199->3443|6227->3444|6335->3525|6364->3526|6420->3554|6449->3555|6520->3598|6549->3599|6605->3627|6634->3628|6706->3672|6735->3673|6791->3701|6820->3702|6892->3746|6921->3747|6974->3772|7003->3773|7114->3856|7143->3857|7188->3874|7217->3875|7291->3921|7320->3922|7372->3946|7401->3947|7473->3991|7502->3992|7539->4002|7568->4003|7648->4056|7677->4057|7732->4084|7761->4085|7833->4129|7862->4130|7923->4163|7952->4164|8024->4208|8053->4209|8111->4239|8140->4240|8212->4284|8241->4285|8300->4316|8329->4317|8401->4361|8430->4362|8483->4387|8512->4388|8585->4432|8615->4433|8672->4461|8702->4462|8775->4506|8805->4507|8860->4533|8890->4534|8963->4578|8993->4579|9050->4607|9080->4608|9153->4652|9183->4653|9241->4682|9271->4683|9344->4727|9374->4728|9430->4755|9460->4756|9533->4800|9563->4801|9616->4825|9646->4826|9719->4870|9749->4871|9802->4895|9832->4896|9905->4940|9935->4941|9996->4973|10026->4974|10099->5018|10129->5019|10182->5043|10212->5044|10285->5088|10315->5089|10372->5117|10402->5118|10475->5162|10505->5163|10558->5187|10588->5188|10662->5233|10692->5234|10730->5244|10760->5245|11096->5552|11126->5553|11284->5683|11314->5684|11365->5706|11395->5707|11502->5786|11532->5787|11589->5815|11619->5816|11666->5834|11696->5835|11752->5862|11782->5863|11825->5877|11855->5878|11911->5905|11941->5906|11984->5920|12014->5921|12070->5948|12100->5949|12143->5963|12173->5964|12229->5991|12259->5992|12302->6006|12332->6007|12612->6250|12628->6256|12671->6276|12734->6302|12750->6308|12808->6343|13400->6898|13418->6906|13506->6983|13523->6989|13535->7012|13563->7030|13604->7032|13744->7134|13761->7140|13805->7160|13923->7241|13936->7244|14004->7288|14109->7356|14136->7373|14177->7375|14274->7435|14290->7441|14333->7461|14406->7497|14462->7530|14554->7589|14596->7632|14618->7644|14659->7646|14752->7702|14768->7708|14811->7728|14880->7760|14936->7793|15021->7862|15194->7998|15222->8016|15263->8018|15676->8394|15692->8400|15735->8420|15876->8524|15957->8594|15999->8596|16083->8643|16099->8649|16187->8713|16337->8826|16353->8832|16446->8901|16643->9061|16659->9067|16749->9133|16884->9231|16900->9237|16951->9265|17129->9406|17145->9412|17193->9437|17325->9550|17339->9555|17379->9556|17463->9603|17479->9609|17550->9656|17700->9769|17716->9775|17789->9824|17947->9945|17963->9951|18039->10003|18181->10108|18197->10114|18248->10142|18426->10283|18442->10289|18490->10314|18636->10427|18744->10502|18803->10524|18831->10542|18872->10544|19250->10885|19266->10891|19398->10999|19540->11104|19554->11108|19570->11114|19582->11153|19607->11168|19648->11170|19727->11212|19849->11323|19891->11325|19974->11371|20066->11452|20108->11454|20208->11517|20224->11523|20301->11576|20446->11684|20500->11715|20575->11771|20589->11776|20629->11777|20729->11840|20745->11846|20798->11876|20943->11984|20997->12015|21085->12070|21161->12113|21219->12188|21241->12200|21282->12202|21355->12275|21426->12309|21454->12327|21495->12329|21570->12367|21660->12446|21702->12448|21794->12503|21810->12509|21870->12546|22041->12680|22057->12686|22121->12726|22264->12850|22278->12855|22318->12856|22410->12911|22426->12917|22467->12935|22638->13069|22654->13075|22698->13096|22854->13219|22949->13277|22965->13283|23005->13300|23179->13441|23300->13525|23316->13531|23360->13551|23548->13702|23564->13708|23608->13729|23800->13884|23816->13890|23855->13906|24118->14136|24173->14154|24201->14172|24242->14174|24590->14485|24606->14491|24647->14509|24785->14610|24832->14634|24954->14719|24970->14725|25025->14757|25153->14848|25205->14877|25294->14929|25310->14935|25365->14967|25490->15055|25539->15081|25633->15138|25649->15144|25695->15167|25928->15367|25988->15390|26016->15408|26057->15410|26486->15802|26567->15872|26609->15874|26697->15925|26713->15931|26818->16012|26911->16067|26961->16093|27058->16153|27074->16159|27182->16243|27318->16360|27332->16365|27372->16366|27460->16417|27476->16423|27564->16487|27657->16542|27707->16568|27804->16628|27820->16634|27911->16701|28054->16811|28178->16902|28625->17312|28641->17318|28686->17340|28770->17627|28861->17681|28877->17687|28922->17709|29016->17766|29030->17770|29101->17831|29155->17845|29237->17890|29255->17898|29295->17915|29351->17934|29369->17942|29412->17961|29485->18001|29600->18079|29677->18146|29718->18148|29890->18283|29948->18317|30113->18445|30129->18451|30173->18472|30274->18536|30290->18542|30329->18558|30428->18620|30444->18626|30482->18641|30615->18741|30751->18840|30779->18858|30820->18860|31256->19259|31272->19265|31311->19281|31479->19412|31495->19418|31537->19437|31782->19645|31864->19717|31905->19719|32049->19826|32065->19832|32107->19850|32843->20553|33008->20685|33068->20708|33082->20712|33098->20718|33110->20745|33135->20760|33176->20762|33243->20792|33294->20833|33335->20835|33795->21258|33811->21264|33850->21280|33962->21355|33978->21361|34011->21371|34130->21453|34146->21459|34181->21471|34289->21542|34305->21548|34344->21564|34457->21640|34473->21646|34513->21663|34624->21737|34640->21743|34693->21773|34807->21850|34823->21856|34863->21873|34973->21946|34989->21952|35030->21970|35116->22195|35195->22237|35209->22241|35268->22290|35285->22296|35297->22343|35323->22359|35364->22361|35464->22424|35480->22430|35517->22444|35557->22447|35615->22481|35690->22581|35712->22593|35739->22638|35818->22680|35861->22713|35902->22715|36079->22873|36093->22878|36133->22879|36325->23038|36469->23149|36598->23241|36620->23253|36679->23289|37053->23625|37065->23626|37103->23640|37446->23946|37462->23952|37515->23982|37741->24171|37763->24183|37822->24219|38128->24530|38150->24542|38191->24544|38310->24626|38332->24638|38404->24686|38599->24844|38621->24856|38679->24891|38861->25061|38944->25108|38987->25141|39028->25143|39357->25440|39454->25500|39483->25506|39571->25558|39599->25576|39640->25578|39806->25712|39844->25714|39899->25759|39940->25761|40019->25811|40049->25812|40128->25862|40158->25863|40247->25923|40277->25924|40461->26079|40491->26080|40631->26183|40657->26199|40699->26218|40794->26281|40867->26325|40897->26326|40992->26392|41022->26393|41062->26404|41092->26405|41154->26439|41183->26440|41317->26537|41331->26541|41364->26551|41395->26553|41425->26554|41505->26605|41535->26606|41578->26620|41608->26621|41660->26645|41689->26646|41748->26676|41778->26677|41905->26775|41935->26776|41996->26809|42026->26810|42117->26872|42147->26873|42249->26946|42279->26947|42936->27567|42958->27579|43018->27615|43343->27911|43373->27912|43437->27947|43467->27948|43555->28007|43585->28008|43622->28017|43652->28018|43746->28083|43776->28084|43900->28180|43930->28181|43965->28188|43994->28189
                    LINES: 20->1|26->1|28->5|36->13|36->13|38->15|38->15|38->15|39->16|39->16|39->16|40->17|40->17|40->17|41->18|41->18|41->18|43->20|43->20|43->20|44->21|44->21|44->21|45->22|45->22|45->22|46->23|46->23|46->23|47->24|47->24|47->24|48->25|48->25|48->25|49->26|49->26|49->26|51->28|51->28|51->28|51->28|51->28|51->28|53->30|53->30|53->30|54->31|54->31|54->31|55->32|55->32|55->32|56->33|56->33|56->33|57->34|57->34|57->34|58->35|58->35|58->35|59->36|59->36|59->36|60->37|60->37|60->37|61->38|61->38|61->38|62->39|62->39|62->39|63->40|63->40|63->40|64->41|64->41|64->41|65->42|65->42|65->42|66->43|66->43|66->43|67->44|67->44|67->44|69->46|69->46|71->48|71->48|75->52|75->52|76->53|76->53|78->55|78->55|80->57|80->57|82->59|82->59|84->61|84->61|86->63|86->63|88->65|88->65|91->68|91->68|93->70|93->70|95->72|95->72|97->74|97->74|99->76|99->76|101->78|101->78|103->80|103->80|105->82|105->82|107->84|107->84|109->86|109->86|111->88|111->88|113->90|113->90|115->92|115->92|117->94|117->94|119->96|119->96|121->98|121->98|123->100|123->100|125->102|125->102|127->104|127->104|129->106|129->106|131->108|131->108|133->110|133->110|135->112|135->112|137->114|137->114|139->116|139->116|141->118|141->118|143->120|143->120|145->122|145->122|147->124|147->124|149->126|149->126|151->128|151->128|153->130|153->130|155->132|155->132|157->134|157->134|159->136|159->136|161->138|161->138|163->140|163->140|165->142|165->142|167->144|167->144|169->146|169->146|178->155|178->155|182->159|182->159|184->161|184->161|187->164|187->164|189->166|189->166|189->166|189->166|190->167|190->167|190->167|190->167|191->168|191->168|191->168|191->168|192->169|192->169|192->169|192->169|193->170|193->170|193->170|193->170|201->178|201->178|201->178|202->179|202->179|202->179|213->190|213->190|213->190|213->190|213->191|213->191|213->191|214->192|214->192|214->192|215->193|215->193|215->193|217->195|217->195|217->195|218->196|218->196|218->196|219->197|219->197|221->199|222->201|222->201|222->201|223->202|223->202|223->202|224->203|224->203|226->206|230->210|230->210|230->210|238->218|238->218|238->218|239->219|239->219|239->219|240->220|240->220|240->220|241->221|241->221|241->221|243->223|243->223|243->223|244->224|244->224|244->224|245->225|245->225|245->225|246->226|246->226|246->226|247->227|247->227|247->227|248->228|248->228|248->228|249->229|249->229|249->229|250->230|250->230|250->230|251->231|251->231|251->231|253->233|257->237|258->238|258->238|258->238|264->244|264->244|264->244|265->245|265->245|265->245|265->246|265->246|265->246|266->247|266->247|266->247|267->248|267->248|267->248|268->249|268->249|268->249|269->250|269->250|270->251|270->251|270->251|271->252|271->252|271->252|272->253|272->253|273->254|275->256|276->258|276->258|276->258|278->261|279->262|279->262|279->262|280->263|280->263|280->263|281->264|281->264|281->264|282->265|282->265|282->265|283->266|283->266|283->266|284->267|284->267|284->267|285->268|285->268|285->268|286->269|289->272|289->272|289->272|291->274|292->275|292->275|292->275|294->277|294->277|294->277|296->279|296->279|296->279|300->283|301->284|301->284|301->284|307->290|307->290|307->290|308->291|308->291|310->293|310->293|310->293|311->294|311->294|312->295|312->295|312->295|313->296|313->296|314->297|314->297|314->297|318->301|320->303|320->303|320->303|328->311|328->311|328->311|329->312|329->312|329->312|329->312|329->312|330->313|330->313|330->313|331->314|331->314|331->314|332->315|332->315|332->315|332->315|332->315|333->316|333->316|333->316|334->317|338->321|343->326|343->326|343->326|344->331|345->332|345->332|345->332|346->333|346->333|346->333|346->333|347->334|347->334|347->334|347->334|347->334|347->334|349->336|352->339|352->339|352->339|354->341|354->341|356->343|356->343|356->343|357->344|357->344|357->344|358->345|358->345|358->345|361->348|366->353|366->353|366->353|373->360|373->360|373->360|376->363|376->363|376->363|382->369|382->369|382->369|384->371|384->371|384->371|395->382|397->384|399->386|399->386|399->386|399->387|399->387|399->387|400->388|400->388|400->388|405->393|405->393|405->393|406->394|406->394|406->394|407->395|407->395|407->395|408->396|408->396|408->396|409->397|409->397|409->397|410->398|410->398|410->398|411->399|411->399|411->399|412->400|412->400|412->400|413->403|414->404|414->404|414->404|414->404|414->405|414->405|414->405|415->406|415->406|415->406|415->406|415->406|416->408|416->408|416->409|417->410|417->410|417->410|419->412|419->412|419->412|421->414|425->418|430->423|430->423|430->423|434->427|434->427|434->427|439->432|439->432|439->432|443->436|443->436|443->436|449->443|449->443|449->443|451->445|451->445|451->445|454->448|454->448|454->448|456->451|459->454|459->454|459->454|464->459|468->463|468->463|472->467|472->467|472->467|479->474|480->475|480->475|480->475|482->477|482->477|482->477|482->477|483->478|483->478|485->480|485->480|486->481|486->481|486->481|489->484|492->487|492->487|493->488|493->488|493->488|493->488|495->490|495->490|501->496|501->496|501->496|501->496|501->496|502->497|502->497|502->497|502->497|503->498|503->498|505->500|505->500|507->502|507->502|509->504|509->504|510->505|510->505|512->507|512->507|521->516|521->516|521->516|527->522|527->522|527->522|527->522|529->524|529->524|530->525|530->525|531->526|531->526|533->528|533->528|534->529|534->529
                    -- GENERATED --
                */
            