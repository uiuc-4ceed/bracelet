
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object comment extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[models.Comment],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(comments: List[models.Comment])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.70*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/comments/*5.10*/.reverse.map/*5.22*/ { c =>_display_(Seq[Any](format.raw/*5.29*/("""
    <!-- If the user can view the comment, it will be displayed. If not, nothing is displayed. -->
    """),_display_(Seq[Any](/*7.6*/if(Permission.checkPermission(Permission.ViewComments, ResourceRef(ResourceRef.comment, c.id)))/*7.101*/ {_display_(Seq[Any](format.raw/*7.103*/("""
        <div class="comment" id="comment_"""),_display_(Seq[Any](/*8.43*/c/*8.44*/.id)),format.raw/*8.47*/("""">
            <div class="media">
                <a class="pull-left" href=""""),_display_(Seq[Any](/*10.45*/routes/*10.51*/.Profile.viewProfileUUID(c.author.id))),format.raw/*10.88*/("""">
                    <div class="thumbnail">
                        <img class="avatar" src='"""),_display_(Seq[Any](/*12.51*/c/*12.52*/.author.avatarURL)),format.raw/*12.69*/("""'>
                    </div>
                </a>
                <div class="media-body">
                    <div class="comment-header">
                        <a href=""""),_display_(Seq[Any](/*17.35*/routes/*17.41*/.Profile.viewProfileUUID(c.author.id))),format.raw/*17.78*/("""">"""),_display_(Seq[Any](/*17.81*/c/*17.82*/.author.fullName)),format.raw/*17.98*/("""</a>
                        <span>•</span>
                        <span>"""),_display_(Seq[Any](/*19.32*/c/*19.33*/.posted.format("MMM dd, yyyy HH:mm:ss"))),format.raw/*19.72*/("""</span>
                    </div>
                    <div class="comment-body" id="comment-body_"""),_display_(Seq[Any](/*21.65*/c/*21.66*/.id)),format.raw/*21.69*/("""">"""),_display_(Seq[Any](/*21.72*/Html(c.text))),format.raw/*21.84*/("""</div>
                    <div class="comment-footer">
                            <!-- If the user can add a comment, reply will be displayed. If not, nothing is displayed. -->
                        """),_display_(Seq[Any](/*24.26*/if(Permission.checkPermission(Permission.AddComment, ResourceRef(ResourceRef.comment, c.id)))/*24.119*/ {_display_(Seq[Any](format.raw/*24.121*/("""
                            <a href="#"
                            onclick="return showReplyComment('"""),_display_(Seq[Any](/*26.64*/c/*26.65*/.id)),format.raw/*26.68*/("""');"
                            class="btn btn-link btn-xs" title="Reply To Comment">
                                <span class="glyphicon glyphicon-share-alt"></span> Reply
                            </a>
                        """)))})),format.raw/*30.26*/("""
                            &nbsp;
                            &nbsp;
                            <!-- If the user can edit the comment, it will be displayed. If not, nothing is displayed. -->
                        """),_display_(Seq[Any](/*34.26*/if(Permission.checkPermission(Permission.EditComment, ResourceRef(ResourceRef.comment, c.id)))/*34.120*/ {_display_(Seq[Any](format.raw/*34.122*/("""
                            <a href="#" onclick="return showEditComment('"""),_display_(Seq[Any](/*35.75*/c/*35.76*/.id)),format.raw/*35.79*/("""');" class="btn btn-link btn-xs" title="Edit Comment">
                                <span class="glyphicon glyphicon-edit"></span> Edit
                            </a>
                        """)))})),format.raw/*38.26*/("""
                            &nbsp;
                            &nbsp;
                            <!-- If the user can delete the comment, it will be displayed. If not, nothing is displayed. -->
                        """),_display_(Seq[Any](/*42.26*/if(Permission.checkPermission(Permission.DeleteComment, ResourceRef(ResourceRef.comment, c.id)))/*42.122*/ {_display_(Seq[Any](format.raw/*42.124*/("""
                            <a href="#" onclick="return deleteComment('"""),_display_(Seq[Any](/*43.73*/c/*43.74*/.id)),format.raw/*43.77*/("""');" class="btn btn-link btn-xs" title="Delete Comment">
                                <span class="glyphicon glyphicon-trash"></span> Delete
                            </a>
                        """)))})),format.raw/*46.26*/("""
                    </div>
                    <div class="comment-reply">
                        <div class="comment-threads unstyled" id="reply_"""),_display_(Seq[Any](/*49.74*/c/*49.75*/.id)),format.raw/*49.78*/("""">
                        """),_display_(Seq[Any](/*50.26*/comment(c.replies))),format.raw/*50.44*/("""
                        </div>
                    </div>
                </div>
            </div>
        </div>
    """)))})),format.raw/*56.6*/("""
""")))})),format.raw/*57.2*/("""
<script src=""""),_display_(Seq[Any](/*58.15*/routes/*58.21*/.Assets.at("javascripts/comment-delete.js"))),format.raw/*58.64*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*59.15*/routes/*59.21*/.Assets.at("javascripts/comment-edit.js"))),format.raw/*59.62*/("""" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
    function deleteCommentCallback(id) """),format.raw/*62.40*/("""{"""),format.raw/*62.41*/("""
    	$("#comment_" + id).remove();
    """),format.raw/*64.5*/("""}"""),format.raw/*64.6*/("""    
</script>"""))}
    }
    
    def render(comments:List[models.Comment],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(comments)(user)
    
    def f:((List[models.Comment]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (comments) => (user) => apply(comments)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/comment.scala.html
                    HASH: 468b0a9832d33cca4a09f69bd18549ad2406c433
                    MATRIX: 623->1|807->69|835->94|871->96|887->104|907->116|951->123|1090->228|1194->323|1234->325|1312->368|1321->369|1345->372|1460->451|1475->457|1534->494|1667->591|1677->592|1716->609|1927->784|1942->790|2001->827|2040->830|2050->831|2088->847|2199->922|2209->923|2270->962|2405->1061|2415->1062|2440->1065|2479->1068|2513->1080|2753->1284|2856->1377|2897->1379|3037->1483|3047->1484|3072->1487|3339->1722|3594->1941|3698->2035|3739->2037|3850->2112|3860->2113|3885->2116|4114->2313|4371->2534|4477->2630|4518->2632|4627->2705|4637->2706|4662->2709|4896->2911|5081->3060|5091->3061|5116->3064|5180->3092|5220->3110|5372->3231|5405->3233|5456->3248|5471->3254|5536->3297|5621->3346|5636->3352|5699->3393|5860->3526|5889->3527|5956->3567|5984->3568
                    LINES: 20->1|24->1|26->4|27->5|27->5|27->5|27->5|29->7|29->7|29->7|30->8|30->8|30->8|32->10|32->10|32->10|34->12|34->12|34->12|39->17|39->17|39->17|39->17|39->17|39->17|41->19|41->19|41->19|43->21|43->21|43->21|43->21|43->21|46->24|46->24|46->24|48->26|48->26|48->26|52->30|56->34|56->34|56->34|57->35|57->35|57->35|60->38|64->42|64->42|64->42|65->43|65->43|65->43|68->46|71->49|71->49|71->49|72->50|72->50|78->56|79->57|80->58|80->58|80->58|81->59|81->59|81->59|84->62|84->62|86->64|86->64
                    -- GENERATED --
                */
            