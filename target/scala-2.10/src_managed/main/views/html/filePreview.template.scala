
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object filePreview extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template19[models.File,String,List[Comment],Map[models.File, Array[scala.Tuple8[java.lang.String, String, String, String, java.lang.String, String, Long, String]]],List[models.Section],Boolean,List[models.Dataset],List[Folder],List[models.Metadata],Boolean,List[Extraction],Option[List[String]],Option[String],String,List[Folder],List[ProjectSpace],List[Dataset],Option[models.User],RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*2.2*/(file: models.File, id: String, comments: List[Comment],
        previews : Map[models.File, Array[(java.lang.String, String, String, String, java.lang.String, String, Long, String)]],
        sections: List[models.Section], beingProcessed: Boolean, datasets: List[models.Dataset], folders: List[Folder],
        mds: List[models.Metadata],
        rdfExported: Boolean,
        extractionsStatus: List[Extraction], outputFormats: Option[List[String]], spaceId: Option[String], access:String,
        folderHierarchy: List[Folder], spaces:List[ProjectSpace], allDatasets: List[Dataset])(implicit user: Option[models.User], request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import play.api.Play.current

import api.Permission

import _root_.util.FileUtils

import _root_.util.Formatters._

def /*20.2*/showPreview/*20.13*/(id: String, p: (java.lang.String, String, String, String, java.lang.String, String, Long, String), filename: String, contentType: String):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*20.155*/("""
    <script>
            Configuration.tab = "#previewer_"""),_display_(Seq[Any](/*22.46*/id)),format.raw/*22.48*/("""";
            Configuration.url = """"),_display_(Seq[Any](/*23.35*/p/*23.36*/._5)),format.raw/*23.39*/("""";
            Configuration.fileid = """"),_display_(Seq[Any](/*24.38*/p/*24.39*/._1)),format.raw/*24.42*/("""";
            Configuration.previewer = """"),_display_(Seq[Any](/*25.41*/routes/*25.47*/.Assets.at(p._3))),format.raw/*25.63*/("""";
            if(""""),_display_(Seq[Any](/*26.18*/p/*26.19*/._2)),format.raw/*26.22*/("""" === "Thumbnail")"""),format.raw/*26.40*/("""{"""),format.raw/*26.41*/("""
                Configuration.fileType = """"),_display_(Seq[Any](/*27.44*/p/*27.45*/._6)),format.raw/*27.48*/("""";
                Configuration.fileSize = """),_display_(Seq[Any](/*28.43*/p/*28.44*/._7)),format.raw/*28.47*/(""";
            """),format.raw/*29.13*/("""}"""),format.raw/*29.14*/("""
            else if(""""),_display_(Seq[Any](/*30.23*/p/*30.24*/._2)),format.raw/*30.27*/("""" === "X3d")"""),format.raw/*30.39*/("""{"""),format.raw/*30.40*/("""
                Configuration.annotationsEditPath = """"),_display_(Seq[Any](/*31.55*/api/*31.58*/.routes.Previews.editAnnotation(UUID(p._1)))),format.raw/*31.101*/("""";
                Configuration.annotationsListPath = """"),_display_(Seq[Any](/*32.55*/api/*32.58*/.routes.Previews.listAnnotations(UUID(p._1)))),format.raw/*32.102*/("""";
                Configuration.annotationsAttachPath = """"),_display_(Seq[Any](/*33.57*/api/*33.60*/.routes.Previews.attachAnnotation(UUID(p._1)))),format.raw/*33.105*/("""";
                Configuration.wasPTM = """"),_display_(Seq[Any](/*34.42*/(filename.endsWith(".ptm") || contentType.contains("ptmimages")))),format.raw/*34.106*/("""";
                Configuration.calledFrom = "file";
            """),format.raw/*36.13*/("""}"""),format.raw/*36.14*/("""
            else if(""""),_display_(Seq[Any](/*37.23*/p/*37.24*/._2)),format.raw/*37.27*/("""" === "Oni")"""),format.raw/*37.39*/("""{"""),format.raw/*37.40*/("""
            """),format.raw/*38.13*/("""}"""),format.raw/*38.14*/("""
            else if(""""),_display_(Seq[Any](/*39.23*/p/*39.24*/._2)),format.raw/*39.27*/("""" === "Video")"""),format.raw/*39.41*/("""{"""),format.raw/*39.42*/("""
                Configuration.fileType = """"),_display_(Seq[Any](/*40.44*/p/*40.45*/._6)),format.raw/*40.48*/("""";
            """),format.raw/*41.13*/("""}"""),format.raw/*41.14*/("""
            else if(""""),_display_(Seq[Any](/*42.23*/p/*42.24*/._2)),format.raw/*42.27*/("""" === "Video presentation")"""),format.raw/*42.54*/("""{"""),format.raw/*42.55*/("""
                Configuration.authenticatedFileModify = false;
                """),_display_(Seq[Any](/*44.18*/if(user.isDefined)/*44.36*/ {_display_(Seq[Any](format.raw/*44.38*/("""
                """),_display_(Seq[Any](/*45.18*/if(user.get.id.equals(file.author.id))/*45.56*/{_display_(Seq[Any](format.raw/*45.57*/("""
                Configuration.authenticatedFileModify = true;
                """)))})),format.raw/*47.18*/("""
                """)))})),format.raw/*48.18*/("""
            """),format.raw/*49.13*/("""}"""),format.raw/*49.14*/("""
            else if(""""),_display_(Seq[Any](/*50.23*/p/*50.24*/._2)),format.raw/*50.27*/("""" === "External resource")"""),format.raw/*50.53*/("""{"""),format.raw/*50.54*/("""
                Configuration.authenticatedFileModify = false;
                """),_display_(Seq[Any](/*52.18*/if(user.isDefined)/*52.36*/ {_display_(Seq[Any](format.raw/*52.38*/("""
                """),_display_(Seq[Any](/*53.18*/if(user.get.id.equals(file.author.id))/*53.56*/{_display_(Seq[Any](format.raw/*53.57*/("""
                Configuration.authenticatedFileModify = true;
                """)))})),format.raw/*55.18*/("""
                """)))})),format.raw/*56.18*/("""
            """),format.raw/*57.13*/("""}"""),format.raw/*57.14*/("""
    </script>
    <script type="text/javascript" src=""""),_display_(Seq[Any](/*59.42*/(routes.Assets.at(p._3) + "/" + p._4))),format.raw/*59.79*/(""""></script>
""")))};implicit def /*16.2*/implicitFieldConstructor/*16.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*8.154*/("""

"""),format.raw/*15.1*/("""
"""),format.raw/*16.75*/("""


    """),format.raw/*19.122*/("""
"""),format.raw/*60.2*/("""

"""),_display_(Seq[Any](/*62.2*/main("File")/*62.14*/ {_display_(Seq[Any](format.raw/*62.16*/("""

    <style>


            .navbar """),format.raw/*67.21*/("""{"""),format.raw/*67.22*/("""
                display: none;
            """),format.raw/*69.13*/("""}"""),format.raw/*69.14*/("""




    </style>

    """),format.raw/*76.105*/("""
    """),format.raw/*77.108*/("""
    """),format.raw/*78.114*/("""
    <script src=""""),_display_(Seq[Any](/*79.19*/routes/*79.25*/.Assets.at("javascripts/previews.js"))),format.raw/*79.62*/("""" type="text/javascript"></script>
    """),format.raw/*80.112*/("""
    """),format.raw/*81.106*/("""
    """),format.raw/*82.107*/("""
    """),format.raw/*83.103*/("""
    """),format.raw/*84.111*/("""
    """),format.raw/*85.106*/("""
    <script>
            var spaceId = """"),_display_(Seq[Any](/*87.29*/spaceId)),format.raw/*87.36*/("""";
            var isPageLoaded = false;
            var isx3dActive = false;
            var Configuration = """),format.raw/*90.33*/("""{"""),format.raw/*90.34*/("""}"""),format.raw/*90.35*/(""";
            Configuration.id  = """"),_display_(Seq[Any](/*91.35*/file/*91.39*/.id)),format.raw/*91.42*/("""";
            Configuration.authenticated = """),_display_(Seq[Any](/*92.44*/user/*92.48*/.isDefined)),format.raw/*92.58*/(""";
            Configuration.ptmAppletPath = """"),_display_(Seq[Any](/*93.45*/(routes.Assets.at("plugins") + "/" + "envlib.jar"))),format.raw/*93.95*/("""";
            Configuration.expressInstallPath = """"),_display_(Seq[Any](/*94.50*/(routes.Assets.at("plugins") + "/" + "expressInstall.swf"))),format.raw/*94.108*/("""";
            Configuration.iipZoomPath = """"),_display_(Seq[Any](/*95.43*/(routes.Assets.at("plugins") + "/" + "IIPZoom.swf"))),format.raw/*95.94*/("""";
            Configuration.jsPath = """"),_display_(Seq[Any](/*96.38*/(routes.Assets.at("javascripts")))),format.raw/*96.71*/("""";
            Configuration.imagesPath = """"),_display_(Seq[Any](/*97.42*/(routes.Assets.at("images")))),format.raw/*97.70*/("""";
            Configuration.appContext = """"),_display_(Seq[Any](/*98.42*/play/*98.46*/.api.Play.configuration.getString("application.context").getOrElse(""))),format.raw/*98.116*/("""";
            var cur_description;

            function urlify(text) """),format.raw/*101.35*/("""{"""),format.raw/*101.36*/("""
                var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                return text.replace(urlRegex, function(url) """),format.raw/*103.61*/("""{"""),format.raw/*103.62*/("""
                    return '<a href="' + url + '">' + url + '</a>';
                """),format.raw/*105.17*/("""}"""),format.raw/*105.18*/(""")
            """),format.raw/*106.13*/("""}"""),format.raw/*106.14*/("""
    </script>
    <script>
            window.onload = function() """),format.raw/*109.40*/("""{"""),format.raw/*109.41*/("""
                isPageLoaded = true;
            """),format.raw/*111.13*/("""}"""),format.raw/*111.14*/("""
    </script>



            """),_display_(Seq[Any](/*116.14*/for((f, pvs) <- previews) yield /*116.39*/ {_display_(Seq[Any](format.raw/*116.41*/("""
                <div class="row" id="filePrevContainer_"""),_display_(Seq[Any](/*117.57*/(f.id.toString))),format.raw/*117.72*/("""" data-previewsCount=""""),_display_(Seq[Any](/*117.95*/(pvs.length))),format.raw/*117.107*/("""" data-filename=""""),_display_(Seq[Any](/*117.125*/f/*117.126*/.filename)),format.raw/*117.135*/("""">
                            <!--
		    	First check if there is only one previewer, and if the value of the URL for it is "null". If so
		    	that means there was no preview generated and the file is not allowed to be used due to the license
		    	that is on the file.
		    	 -->
                        """),_display_(Seq[Any](/*123.26*/if(pvs.length == 1 && pvs(0)._5 == "null")/*123.68*/ {_display_(Seq[Any](format.raw/*123.70*/("""
                            <h4>No previews currently available for this file</h4>
                        """)))}/*125.27*/else/*125.32*/{_display_(Seq[Any](format.raw/*125.33*/("""
                            <div class="tabbable">
                                <ul class="nav nav-pills" id="myTab_"""),_display_(Seq[Any](/*127.70*/(f.id.toString))),format.raw/*127.85*/("""">
                                """),_display_(Seq[Any](/*128.34*/for(i <- pvs.indices) yield /*128.55*/ {_display_(Seq[Any](format.raw/*128.57*/("""
                                        <!-- Use preview.title as tab title if defined; otherwise use previewer name. -->
                                    """),_display_(Seq[Any](/*130.38*/if(pvs(i)._8 != "")/*130.57*/ {_display_(Seq[Any](format.raw/*130.59*/("""
                                        <li class=""><a href="#previewer_"""),_display_(Seq[Any](/*131.75*/(f.id.toString))),format.raw/*131.90*/("""_"""),_display_(Seq[Any](/*131.92*/i)),format.raw/*131.93*/("""" data-toggle="tab">"""),_display_(Seq[Any](/*131.114*/pvs(i)/*131.120*/._8)),format.raw/*131.123*/("""</a></li>
                                    """)))}/*132.39*/else/*132.44*/{_display_(Seq[Any](format.raw/*132.45*/("""
                                        <li class=""><a href="#previewer_"""),_display_(Seq[Any](/*133.75*/(f.id.toString))),format.raw/*133.90*/("""_"""),_display_(Seq[Any](/*133.92*/i)),format.raw/*133.93*/("""" data-toggle="tab">"""),_display_(Seq[Any](/*133.114*/pvs(i)/*133.120*/._2)),format.raw/*133.123*/("""</a></li>
                                    """)))})),format.raw/*134.38*/("""
                                """)))})),format.raw/*135.34*/("""
                                </ul>
                                <div class="tab-content"  id="previewsContent_"""),_display_(Seq[Any](/*137.80*/(f.id.toString))),format.raw/*137.95*/("""">
                                    """),_display_(Seq[Any](/*138.38*/for(i <- pvs.indices) yield /*138.59*/ {_display_(Seq[Any](format.raw/*138.61*/("""
                                        <div class="tab-pane previewDiv" id="previewer_"""),_display_(Seq[Any](/*139.89*/(f.id.toString))),format.raw/*139.104*/("""_"""),_display_(Seq[Any](/*139.106*/i)),format.raw/*139.107*/("""" data-previewId=""""),_display_(Seq[Any](/*139.126*/(pvs(i)._1))),format.raw/*139.137*/("""" data-previewerId=""""),_display_(Seq[Any](/*139.158*/(pvs(i)._2))),format.raw/*139.169*/(""""></div>
                                        """),_display_(Seq[Any](/*140.42*/showPreview(f.id.toString + "_" + i, pvs(i), f.filename, f.contentType))),format.raw/*140.113*/("""

                                    """)))})),format.raw/*142.38*/("""
                                    """),format.raw/*143.72*/("""
                                            """),format.raw/*144.64*/("""
                                                """),format.raw/*145.102*/("""
                                            """),format.raw/*146.51*/("""
                                    """),format.raw/*147.50*/("""
                                </div>
                            </div>
                        """)))})),format.raw/*150.26*/("""
                </div>
                """),_display_(Seq[Any](/*152.18*/if(pvs.length <= 1)/*152.37*/ {_display_(Seq[Any](format.raw/*152.39*/("""
                    """),format.raw/*153.33*/("""
                            """),format.raw/*154.93*/("""
                    """),format.raw/*155.34*/("""
                """)))})),format.raw/*156.18*/("""

            """)))})),format.raw/*158.14*/("""


            <!-- right column -->



""")))})),format.raw/*165.2*/("""
"""))}
    }
    
    def render(file:models.File,id:String,comments:List[Comment],previews:Map[models.File, Array[scala.Tuple8[java.lang.String, String, String, String, java.lang.String, String, Long, String]]],sections:List[models.Section],beingProcessed:Boolean,datasets:List[models.Dataset],folders:List[Folder],mds:List[models.Metadata],rdfExported:Boolean,extractionsStatus:List[Extraction],outputFormats:Option[List[String]],spaceId:Option[String],access:String,folderHierarchy:List[Folder],spaces:List[ProjectSpace],allDatasets:List[Dataset],user:Option[models.User],request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(file,id,comments,previews,sections,beingProcessed,datasets,folders,mds,rdfExported,extractionsStatus,outputFormats,spaceId,access,folderHierarchy,spaces,allDatasets)(user,request)
    
    def f:((models.File,String,List[Comment],Map[models.File, Array[scala.Tuple8[java.lang.String, String, String, String, java.lang.String, String, Long, String]]],List[models.Section],Boolean,List[models.Dataset],List[Folder],List[models.Metadata],Boolean,List[Extraction],Option[List[String]],Option[String],String,List[Folder],List[ProjectSpace],List[Dataset]) => (Option[models.User],RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (file,id,comments,previews,sections,beingProcessed,datasets,folders,mds,rdfExported,extractionsStatus,outputFormats,spaceId,access,folderHierarchy,spaces,allDatasets) => (user,request) => apply(file,id,comments,previews,sections,beingProcessed,datasets,folders,mds,rdfExported,extractionsStatus,outputFormats,spaceId,access,folderHierarchy,spaces,allDatasets)(user,request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/filePreview.scala.html
                    HASH: 3608620874356c75dfe1d9f04f1357521aefeabf
                    MATRIX: 973->2|1829->984|1849->995|2073->1137|2168->1196|2192->1198|2265->1235|2275->1236|2300->1239|2376->1279|2386->1280|2411->1283|2490->1326|2505->1332|2543->1348|2599->1368|2609->1369|2634->1372|2680->1390|2709->1391|2789->1435|2799->1436|2824->1439|2905->1484|2915->1485|2940->1488|2982->1502|3011->1503|3070->1526|3080->1527|3105->1530|3145->1542|3174->1543|3265->1598|3277->1601|3343->1644|3436->1701|3448->1704|3515->1748|3610->1807|3622->1810|3690->1855|3770->1899|3857->1963|3951->2029|3980->2030|4039->2053|4049->2054|4074->2057|4114->2069|4143->2070|4184->2083|4213->2084|4272->2107|4282->2108|4307->2111|4349->2125|4378->2126|4458->2170|4468->2171|4493->2174|4536->2189|4565->2190|4624->2213|4634->2214|4659->2217|4714->2244|4743->2245|4860->2326|4887->2344|4927->2346|4981->2364|5028->2402|5067->2403|5179->2483|5229->2501|5270->2514|5299->2515|5358->2538|5368->2539|5393->2542|5447->2568|5476->2569|5593->2650|5620->2668|5660->2670|5714->2688|5761->2726|5800->2727|5912->2807|5962->2825|6003->2838|6032->2839|6124->2895|6183->2932|6228->785|6261->809|6341->648|6370->783|6399->858|6435->982|6463->2945|6501->2948|6522->2960|6562->2962|6626->2998|6655->2999|6727->3043|6756->3044|6808->3167|6842->3275|6876->3389|6931->3408|6946->3414|7005->3451|7073->3597|7107->3703|7141->3810|7175->3913|7209->4024|7243->4130|7321->4172|7350->4179|7488->4289|7517->4290|7546->4291|7618->4327|7631->4331|7656->4334|7738->4380|7751->4384|7783->4394|7865->4440|7937->4490|8025->4542|8106->4600|8187->4645|8260->4696|8336->4736|8391->4769|8471->4813|8521->4841|8601->4885|8614->4889|8707->4959|8807->5030|8837->5031|9036->5203|9066->5204|9180->5289|9210->5290|9253->5304|9283->5305|9379->5372|9409->5373|9488->5423|9518->5424|9586->5455|9628->5480|9669->5482|9763->5539|9801->5554|9861->5577|9897->5589|9953->5607|9965->5608|9998->5617|10346->5928|10398->5970|10439->5972|10568->6082|10582->6087|10622->6088|10780->6209|10818->6224|10891->6260|10929->6281|10970->6283|11167->6443|11196->6462|11237->6464|11349->6539|11387->6554|11426->6556|11450->6557|11509->6578|11526->6584|11553->6587|11620->6635|11634->6640|11674->6641|11786->6716|11824->6731|11863->6733|11887->6734|11946->6755|11963->6761|11990->6764|12070->6811|12137->6845|12292->6963|12330->6978|12407->7018|12445->7039|12486->7041|12612->7130|12651->7145|12691->7147|12716->7148|12773->7167|12808->7178|12867->7199|12902->7210|12989->7260|13084->7331|13156->7370|13222->7442|13296->7506|13375->7608|13449->7659|13515->7709|13648->7809|13726->7850|13755->7869|13796->7871|13846->7904|13904->7997|13954->8031|14005->8049|14053->8064|14126->8105
                    LINES: 20->2|37->20|37->20|39->20|41->22|41->22|42->23|42->23|42->23|43->24|43->24|43->24|44->25|44->25|44->25|45->26|45->26|45->26|45->26|45->26|46->27|46->27|46->27|47->28|47->28|47->28|48->29|48->29|49->30|49->30|49->30|49->30|49->30|50->31|50->31|50->31|51->32|51->32|51->32|52->33|52->33|52->33|53->34|53->34|55->36|55->36|56->37|56->37|56->37|56->37|56->37|57->38|57->38|58->39|58->39|58->39|58->39|58->39|59->40|59->40|59->40|60->41|60->41|61->42|61->42|61->42|61->42|61->42|63->44|63->44|63->44|64->45|64->45|64->45|66->47|67->48|68->49|68->49|69->50|69->50|69->50|69->50|69->50|71->52|71->52|71->52|72->53|72->53|72->53|74->55|75->56|76->57|76->57|78->59|78->59|79->16|79->16|80->8|82->15|83->16|86->19|87->60|89->62|89->62|89->62|94->67|94->67|96->69|96->69|103->76|104->77|105->78|106->79|106->79|106->79|107->80|108->81|109->82|110->83|111->84|112->85|114->87|114->87|117->90|117->90|117->90|118->91|118->91|118->91|119->92|119->92|119->92|120->93|120->93|121->94|121->94|122->95|122->95|123->96|123->96|124->97|124->97|125->98|125->98|125->98|128->101|128->101|130->103|130->103|132->105|132->105|133->106|133->106|136->109|136->109|138->111|138->111|143->116|143->116|143->116|144->117|144->117|144->117|144->117|144->117|144->117|144->117|150->123|150->123|150->123|152->125|152->125|152->125|154->127|154->127|155->128|155->128|155->128|157->130|157->130|157->130|158->131|158->131|158->131|158->131|158->131|158->131|158->131|159->132|159->132|159->132|160->133|160->133|160->133|160->133|160->133|160->133|160->133|161->134|162->135|164->137|164->137|165->138|165->138|165->138|166->139|166->139|166->139|166->139|166->139|166->139|166->139|166->139|167->140|167->140|169->142|170->143|171->144|172->145|173->146|174->147|177->150|179->152|179->152|179->152|180->153|181->154|182->155|183->156|185->158|192->165
                    -- GENERATED --
                */
            