
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object multimediaSearchResults extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[String,Option[models.UUID],Option[String],List[scala.Tuple2[VersusIndex, List[PreviewFilesSearchResult]]],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(fileName:String="Query file", fileId:Option[models.UUID], queryThumbnailId:Option[String], resultsList:List[(VersusIndex,  List[PreviewFilesSearchResult])])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import collection.JavaConverters._

import models.VersusIndex

import models.PreviewFilesSearchResult

import models.SearchResultFile

import models.SearchResultPreview

implicit def /*16.2*/implicitFieldConstructor/*16.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.195*/("""

"""),format.raw/*3.73*/("""
"""),format.raw/*4.73*/("""
"""),format.raw/*5.73*/("""
"""),format.raw/*6.73*/("""

"""),format.raw/*10.1*/("""
"""),format.raw/*15.1*/("""
"""),format.raw/*16.75*/("""

"""),_display_(Seq[Any](/*18.2*/main("Search Results")/*18.24*/ {_display_(Seq[Any](format.raw/*18.26*/("""	

	"""),format.raw/*20.74*/("""

	<div class="page-header">
		<h1>Search Results For <medium>"""),_display_(Seq[Any](/*23.35*/fileName)),format.raw/*23.43*/("""</medium></h1>
		   		  
	     """),_display_(Seq[Any](/*25.8*/if(!queryThumbnailId.isEmpty)/*25.37*/{_display_(Seq[Any](format.raw/*25.38*/(""" 
	       <img class='mmd-img media-object' 
            src='"""),_display_(Seq[Any](/*27.19*/(routes.Files.thumbnail(UUID(queryThumbnailId.get))))),format.raw/*27.71*/("""'
            title='Thumbnail of query file'>
          </img>
       """)))})),format.raw/*30.9*/("""
  
    <h4> All distances are normalized to be between 0.0 and 1.0</h4>		
	</div>
  
  """),_display_(Seq[Any](/*35.4*/if(resultsList.size == 0)/*35.29*/{_display_(Seq[Any](format.raw/*35.30*/("""
	<div class="row">
		<div class="col-md-12">
			No results found
		</div>
	</div>
	""")))}/*41.4*/else/*41.9*/{_display_(Seq[Any](format.raw/*41.10*/("""	
			
	<table>

	"""),format.raw/*45.123*/("""
	<tbody valign="top">	
	<tr>  
	"""),_display_(Seq[Any](/*48.3*/for((index, previewsAndFiles) <- resultsList) yield /*48.48*/ {_display_(Seq[Any](format.raw/*48.50*/("""
	  """),format.raw/*49.54*/("""
	  """),_display_(Seq[Any](/*50.5*/if(previewsAndFiles.length > 0)/*50.36*/{_display_(Seq[Any](format.raw/*50.37*/("""
		<td>
		<h3>Index name</h3>
		Extractor:"""),_display_(Seq[Any](/*53.14*/index/*53.19*/.extractorID)),format.raw/*53.31*/("""<br>Measure:"""),_display_(Seq[Any](/*53.44*/index/*53.49*/.measureID)),format.raw/*53.59*/("""<br>Indexer:"""),_display_(Seq[Any](/*53.72*/index/*53.77*/.indexerType)),format.raw/*53.89*/("""<p><p>
		<table >		
		"""),_display_(Seq[Any](/*55.4*/for(oneResult <- previewsAndFiles) yield /*55.38*/ {_display_(Seq[Any](format.raw/*55.40*/("""			
			<tr >			
				<td style="vertical-align:middle; height:150px;">			
					<div class="media">
					  
						"""),format.raw/*60.37*/("""
						"""),_display_(Seq[Any](/*61.8*/if(oneResult.fileOrPreview.equalsIgnoreCase("file"))/*61.60*/{_display_(Seq[Any](format.raw/*61.61*/("""
						
							<a class="pull-left" href="""),_display_(Seq[Any](/*63.35*/oneResult/*63.44*/.searchResultFile.url)),format.raw/*63.65*/(""">							
							"""),_display_(Seq[Any](/*64.9*/if(!oneResult.searchResultFile.thumbnailId.isEmpty)/*64.60*/{_display_(Seq[Any](format.raw/*64.61*/("""
								<img class='mmd-img media-object' 
									src='"""),_display_(Seq[Any](/*66.16*/(routes.Files.thumbnail(UUID(oneResult.searchResultFile.thumbnailId))))),format.raw/*66.86*/("""'
									alt='Thumbnail of """),_display_(Seq[Any](/*67.29*/(oneResult.searchResultFile.title))),format.raw/*67.63*/("""' 
									title='Distance """),_display_(Seq[Any](/*68.27*/oneResult/*68.36*/.searchResultFile.distance)),format.raw/*68.62*/("""'>
								</img>
								
							""")))}/*71.10*/else/*71.15*/{_display_(Seq[Any](format.raw/*71.16*/("""No thumbnail available""")))})),format.raw/*71.39*/("""						
							<br>
							
							</a>
					  <div class="media-body">
					    <h4 class="media-heading">"""),_display_(Seq[Any](/*76.37*/oneResult/*76.46*/.searchResultFile.title)),format.raw/*76.69*/("""</h4>
					    <p>Distance: """),_display_(Seq[Any](/*77.24*/oneResult/*77.33*/.searchResultFile.distance)),format.raw/*77.59*/("""</p>			    

						
					    """),_display_(Seq[Any](/*80.11*/if(oneResult.searchResultFile.datasetIdList.size==0)/*80.63*/{_display_(Seq[Any](format.raw/*80.64*/("""
					    	No dataset associated with this image 
					    """)))})),format.raw/*82.11*/("""
					  """),_display_(Seq[Any](/*83.9*/if(oneResult.searchResultFile.datasetIdList.size>0)/*83.60*/{_display_(Seq[Any](format.raw/*83.61*/("""
					  	<b>Belongs to  """),_display_(Seq[Any](/*84.25*/oneResult/*84.34*/.searchResultFile.datasetIdList.size)),format.raw/*84.70*/(""" dataset(s)</b>
					  	"""),_display_(Seq[Any](/*85.10*/oneResult/*85.19*/.searchResultFile.datasetIdList.map/*85.54*/{id=>_display_(Seq[Any](format.raw/*85.59*/("""					   
					     		<br><a href="""),_display_(Seq[Any](/*86.26*/routes/*86.32*/.Datasets.dataset(UUID(id)))),format.raw/*86.59*/("""> View Dataset </a>
					    	""")))})),format.raw/*87.12*/("""  						  	 
					    """)))})),format.raw/*88.11*/("""	 
					    
					    <br>					    	
					    	<a href="""),_display_(Seq[Any](/*91.20*/routes/*91.26*/.Search.findSimilarToExistingFile(oneResult.searchResultFile.id))),format.raw/*91.90*/(""">Find Similar</a>					 
					     	<br>
					     	<a href=""""),_display_(Seq[Any](/*93.22*/(routes.Files.file(oneResult.searchResultFile.id)))),format.raw/*93.72*/("""">View File </a>
					     	
						""")))})),format.raw/*95.8*/("""	"""),format.raw/*95.27*/("""		
						
    
   					 """),format.raw/*98.37*/("""
						"""),_display_(Seq[Any](/*99.8*/if(oneResult.fileOrPreview.equalsIgnoreCase("preview"))/*99.63*/{_display_(Seq[Any](format.raw/*99.64*/("""					
						<a class="pull-left" href="""),_display_(Seq[Any](/*100.34*/oneResult/*100.43*/.searchResultPreview.url)),format.raw/*100.67*/(""">	
						<img class='mmd-img media-object' 
							src='"""),_display_(Seq[Any](/*102.14*/api/*102.17*/.routes.Previews.download(oneResult.searchResultPreview.id))),format.raw/*102.76*/("""' 
							alt='"""),_display_(Seq[Any](/*103.14*/oneResult/*103.23*/.searchResultPreview.previewName)),format.raw/*103.55*/("""' 
							title='Distance """),_display_(Seq[Any](/*104.25*/oneResult/*104.34*/.searchResultPreview.distance)),format.raw/*104.63*/("""'>
						</img>
						
						</a>
					  <div class="media-body">

					    <h4 class="media-heading">"""),_display_(Seq[Any](/*110.37*/oneResult/*110.46*/.searchResultPreview.previewName)),format.raw/*110.78*/("""</h4>
					    <p>Distance (normalized): """),_display_(Seq[Any](/*111.37*/oneResult/*111.46*/.searchResultPreview.distance)),format.raw/*111.75*/("""</p>				    
						
					    """),format.raw/*113.39*/("""
					    """),_display_(Seq[Any](/*114.11*/if(oneResult.searchResultPreview.datasetIdList.size==0)/*114.66*/{_display_(Seq[Any](format.raw/*114.67*/("""
					    	No dataset associated with this image 
					    """)))})),format.raw/*116.11*/("""

					  """),_display_(Seq[Any](/*118.9*/if(oneResult.searchResultPreview.datasetIdList.size>0)/*118.63*/{_display_(Seq[Any](format.raw/*118.64*/("""
					  		<b>Belongs to  """),_display_(Seq[Any](/*119.26*/oneResult/*119.35*/.searchResultPreview.datasetIdList.size)),format.raw/*119.74*/(""" dataset(s)</b>
					  		"""),_display_(Seq[Any](/*120.11*/oneResult/*120.20*/.searchResultPreview.datasetIdList.map/*120.58*/{id=>_display_(Seq[Any](format.raw/*120.63*/("""					   
					     			<br><a href="""),_display_(Seq[Any](/*121.27*/routes/*121.33*/.Datasets.dataset(UUID(id)))),format.raw/*121.60*/("""> View Dataset </a>
					     			<br>					     		
					    	""")))})),format.raw/*123.12*/("""  						  	 
					    """)))})),format.raw/*124.11*/("""					    
					     """),format.raw/*125.48*/("""
					    <a href=""""),_display_(Seq[Any](/*126.20*/(routes.Files.file(UUID(oneResult.searchResultPreview.fileIdString))))),format.raw/*126.89*/("""">
					     		View File """),_display_(Seq[Any](/*127.24*/oneResult/*127.33*/.searchResultPreview.fileTitle)),format.raw/*127.63*/("""</a>
					     <br>
					     		Shot start time: """),_display_(Seq[Any](/*129.31*/oneResult/*129.40*/.searchResultPreview.shotStartTime)),format.raw/*129.74*/("""					     		
						""")))})),format.raw/*130.31*/("""

					  </div>
					</div>
				</td>
			</tr>
		""")))})),format.raw/*136.4*/("""  """),format.raw/*136.56*/("""
		</tbody>	

		</table>
		</td>		
		""")))})),format.raw/*141.4*/(""" """),format.raw/*141.51*/("""
	""")))})),format.raw/*142.3*/(""" """),format.raw/*142.42*/("""
	</tr>

	</tbody></table>
	"""),format.raw/*146.73*/("""
	
	<br>
	    """),format.raw/*149.89*/("""
      """),format.raw/*152.77*/("""
      
	"""),_display_(Seq[Any](/*154.3*/if(resultsList.size >=2 && fileId.isDefined)/*154.47*/ {_display_(Seq[Any](format.raw/*154.49*/("""
		 """),_display_(Seq[Any](/*155.5*/form(action = routes.Search.findSimilarWeightedIndexes, 'id->"form-weights", 'enctype -> "multipart/form-data", 'class -> "form-horizontal")/*155.145*/ {_display_(Seq[Any](format.raw/*155.147*/("""
		       
		      <div id="validation"></div> 
		       
			    <fieldset>
			      <legend>Combine search results</legend>			     
		  	      <input type="hidden" name="FileID" value=""""),_display_(Seq[Any](/*161.55*/fileId)),format.raw/*161.61*/("""" id="file_hidden">			    	 		    
			     	  """),_display_(Seq[Any](/*162.13*/for((index, previewsAndFiles) <- resultsList) yield /*162.58*/ {_display_(Seq[Any](format.raw/*162.60*/("""
			     	   """),format.raw/*163.96*/("""
			     	   """),_display_(Seq[Any](/*164.14*/if(previewsAndFiles.size > 0)/*164.43*/{_display_(Seq[Any](format.raw/*164.44*/("""	
			     		    <p>Please select weight for index """),_display_(Seq[Any](/*165.50*/index/*165.55*/.extractorID)),format.raw/*165.67*/(""" / """),_display_(Seq[Any](/*165.71*/index/*165.76*/.measureID)),format.raw/*165.86*/("""  			
			     		    <input type="text" name="Weight" id=""""),_display_(Seq[Any](/*166.53*/index/*166.58*/.id)),format.raw/*166.61*/("""">			
						      <input type="hidden" name="IndexID" value=""""),_display_(Seq[Any](/*167.57*/index/*167.62*/.id)),format.raw/*167.65*/("""">
						   """)))})),format.raw/*168.11*/("""												
					   """)))})),format.raw/*169.10*/("""			     
					   <div id="checkboxes" style="padding-left:15px">
			    </fieldset>
		      <div class="form-actions">
				  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-filter"></span> Combine Results</button>
		      </div>
		""")))})),format.raw/*175.4*/("""
	""")))})),format.raw/*176.3*/("""<!--end of if(resultsList.size >=2) -->
"""),format.raw/*177.40*/("""

<script language="javascript">
		//============================================================================
		//validate input on submit
		//============================================================================
		document.getElementById('form-weights').onsubmit =function()"""),format.raw/*183.63*/("""{"""),format.raw/*183.64*/("""
			//alert('top of validation');
			
			var errors = "";
			
			//reset errors display
			document.getElementById("validation").innerHTML = "";
	        
			console.log("hidden file = " + document.getElementById('file_hidden'));
			
			var w;
			var sum = 0.0;
			var floatVal=0.0;			
			"""),_display_(Seq[Any](/*196.5*/for((index, previewsAndFiles) <- resultsList) yield /*196.50*/ {_display_(Seq[Any](format.raw/*196.52*/("""
				//get weight for index
				w = document.getElementById(""""),_display_(Seq[Any](/*198.35*/index/*198.40*/.id)),format.raw/*198.43*/("""");
				w.style.backgroundColor = "#FFFFFF";
				//make sure weight is a number
				floatVal = parseFloat( w.value);
				
				if (isNaN(floatVal))"""),format.raw/*203.25*/("""{"""),format.raw/*203.26*/("""	
				 //not a number
				 w.style.backgroundColor = "#FFCCCC";				 
				 w.setSelectionRange(0, w.value.length);				 
				 errors += "<li>Weights must be numbers</li>";
				"""),format.raw/*208.5*/("""}"""),format.raw/*208.6*/("""
				else if (floatVal <0 || floatVal >1)"""),format.raw/*209.41*/("""{"""),format.raw/*209.42*/("""
				 //not within 0..1
				 w.style.backgroundColor = "#FFCCCC";				 
				 w.setSelectionRange(0, w.value.length);				 
				 errors += "<li>Weights must be between 0 and 1</li>";
				"""),format.raw/*214.5*/("""}"""),format.raw/*214.6*/("""				
				sum += floatVal;
				
			""")))})),format.raw/*217.5*/("""//end of for((index, previewsAndFiles) <- resultsList) 
			if (sum != 1.0) """),format.raw/*218.20*/("""{"""),format.raw/*218.21*/("""
				 errors += "<li>Sum of weights must be 1 </li>";
			"""),format.raw/*220.4*/("""}"""),format.raw/*220.5*/("""
				
			if (errors.length > 0) """),format.raw/*222.27*/("""{"""),format.raw/*222.28*/("""
		        document.getElementById("validation").innerHTML = "Please correct the following errors:<ul>" + errors + "</ul>";
		        document.getElementById("validation").style.backgroundColor = "#FFCCCC";
		        return false;
		    """),format.raw/*226.7*/("""}"""),format.raw/*226.8*/(""" else """),format.raw/*226.14*/("""{"""),format.raw/*226.15*/("""
		    	//alert("input OK");
		    	return true;
		    """),format.raw/*229.7*/("""}"""),format.raw/*229.8*/("""
			//return false;
		"""),format.raw/*231.3*/("""}"""),format.raw/*231.4*/("""
			
</script>

	""")))})),format.raw/*235.3*/("""		
""")))})),format.raw/*236.2*/("""
"""))}
    }
    
    def render(fileName:String,fileId:Option[models.UUID],queryThumbnailId:Option[String],resultsList:List[scala.Tuple2[VersusIndex, List[PreviewFilesSearchResult]]],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(fileName,fileId,queryThumbnailId,resultsList)(user)
    
    def f:((String,Option[models.UUID],Option[String],List[scala.Tuple2[VersusIndex, List[PreviewFilesSearchResult]]]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (fileName,fileId,queryThumbnailId,resultsList) => (user) => apply(fileName,fileId,queryThumbnailId,resultsList)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:30 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/multimediaSearchResults.scala.html
                    HASH: 5714f7e650296273ebcfc29adb91ac4ef393bcea
                    MATRIX: 724->1|1190->679|1223->703|1303->194|1332->268|1360->341|1388->414|1416->487|1445->542|1473->677|1502->752|1540->755|1571->777|1611->779|1643->855|1742->918|1772->926|1839->958|1877->987|1916->988|2015->1051|2089->1103|2192->1175|2316->1264|2350->1289|2389->1290|2492->1376|2504->1381|2543->1382|2589->1520|2658->1554|2719->1599|2759->1601|2791->1655|2831->1660|2871->1691|2910->1692|2989->1735|3003->1740|3037->1752|3086->1765|3100->1770|3132->1780|3181->1793|3195->1798|3229->1810|3287->1833|3337->1867|3377->1869|3517->2011|3560->2019|3621->2071|3660->2072|3738->2114|3756->2123|3799->2144|3851->2161|3911->2212|3950->2213|4045->2272|4137->2342|4203->2372|4259->2406|4324->2435|4342->2444|4390->2470|4444->2506|4457->2511|4496->2512|4551->2535|4694->2642|4712->2651|4757->2674|4822->2703|4840->2712|4888->2738|4954->2768|5015->2820|5054->2821|5146->2881|5190->2890|5250->2941|5289->2942|5350->2967|5368->2976|5426->3012|5487->3037|5505->3046|5549->3081|5592->3086|5662->3120|5677->3126|5726->3153|5789->3184|5844->3207|5936->3263|5951->3269|6037->3333|6134->3394|6206->3444|6273->3480|6302->3499|6354->3550|6397->3558|6461->3613|6500->3614|6576->3653|6595->3662|6642->3686|6736->3743|6749->3746|6831->3805|6884->3821|6903->3830|6958->3862|7022->3889|7041->3898|7093->3927|7233->4030|7252->4039|7307->4071|7386->4113|7405->4122|7457->4151|7515->4209|7563->4220|7628->4275|7668->4276|7761->4336|7807->4346|7871->4400|7911->4401|7974->4427|7993->4436|8055->4475|8118->4501|8137->4510|8185->4548|8229->4553|8301->4588|8317->4594|8367->4621|8461->4682|8517->4705|8566->4762|8623->4782|8715->4851|8778->4877|8797->4886|8850->4916|8937->4966|8956->4975|9013->5009|9066->5052|9148->5102|9179->5154|9249->5192|9279->5239|9314->5242|9344->5281|9401->5380|9444->5477|9480->5715|9526->5725|9580->5769|9621->5771|9662->5776|9813->5916|9855->5918|10079->6105|10108->6111|10192->6158|10254->6203|10295->6205|10337->6301|10388->6315|10427->6344|10467->6345|10555->6396|10570->6401|10605->6413|10646->6417|10661->6422|10694->6432|10789->6490|10804->6495|10830->6498|10929->6560|10944->6565|10970->6568|11016->6581|11071->6603|11366->6866|11401->6869|11470->6948|11785->7234|11815->7235|12141->7525|12203->7570|12244->7572|12343->7634|12358->7639|12384->7642|12559->7788|12589->7789|12791->7963|12820->7964|12890->8005|12920->8006|13132->8190|13161->8191|13228->8226|13332->8301|13362->8302|13447->8359|13476->8360|13537->8392|13567->8393|13832->8630|13861->8631|13896->8637|13926->8638|14009->8693|14038->8694|14088->8716|14117->8717|14167->8735|14203->8739
                    LINES: 20->1|33->16|33->16|34->1|36->3|37->4|38->5|39->6|41->10|42->15|43->16|45->18|45->18|45->18|47->20|50->23|50->23|52->25|52->25|52->25|54->27|54->27|57->30|62->35|62->35|62->35|68->41|68->41|68->41|72->45|75->48|75->48|75->48|76->49|77->50|77->50|77->50|80->53|80->53|80->53|80->53|80->53|80->53|80->53|80->53|80->53|82->55|82->55|82->55|87->60|88->61|88->61|88->61|90->63|90->63|90->63|91->64|91->64|91->64|93->66|93->66|94->67|94->67|95->68|95->68|95->68|98->71|98->71|98->71|98->71|103->76|103->76|103->76|104->77|104->77|104->77|107->80|107->80|107->80|109->82|110->83|110->83|110->83|111->84|111->84|111->84|112->85|112->85|112->85|112->85|113->86|113->86|113->86|114->87|115->88|118->91|118->91|118->91|120->93|120->93|122->95|122->95|125->98|126->99|126->99|126->99|127->100|127->100|127->100|129->102|129->102|129->102|130->103|130->103|130->103|131->104|131->104|131->104|137->110|137->110|137->110|138->111|138->111|138->111|140->113|141->114|141->114|141->114|143->116|145->118|145->118|145->118|146->119|146->119|146->119|147->120|147->120|147->120|147->120|148->121|148->121|148->121|150->123|151->124|152->125|153->126|153->126|154->127|154->127|154->127|156->129|156->129|156->129|157->130|163->136|163->136|168->141|168->141|169->142|169->142|173->146|176->149|177->152|179->154|179->154|179->154|180->155|180->155|180->155|186->161|186->161|187->162|187->162|187->162|188->163|189->164|189->164|189->164|190->165|190->165|190->165|190->165|190->165|190->165|191->166|191->166|191->166|192->167|192->167|192->167|193->168|194->169|200->175|201->176|202->177|208->183|208->183|221->196|221->196|221->196|223->198|223->198|223->198|228->203|228->203|233->208|233->208|234->209|234->209|239->214|239->214|242->217|243->218|243->218|245->220|245->220|247->222|247->222|251->226|251->226|251->226|251->226|254->229|254->229|256->231|256->231|260->235|261->236
                    -- GENERATED --
                */
            