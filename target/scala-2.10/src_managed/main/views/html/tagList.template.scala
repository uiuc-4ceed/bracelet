
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tagList extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[scala.Tuple2[String, Double]],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(weightedTags: List[(String, Double)])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.76*/("""

"""),_display_(Seq[Any](/*3.2*/main("Tag list")/*3.18*/ {_display_(Seq[Any](format.raw/*3.20*/("""
    <div class="row">
        <div class="col-md-12">
            <h1>Tags</h1>
        </div>
    </div>
    <div class="row">
        <div id="tagList" class="col-md-12" style="text-align: justify">
            """),_display_(Seq[Any](/*11.14*/if(weightedTags.isEmpty)/*11.38*/ {_display_(Seq[Any](format.raw/*11.40*/("""
                No tags found.
            """)))}/*13.15*/else/*13.20*/{_display_(Seq[Any](format.raw/*13.21*/("""
                """),_display_(Seq[Any](/*14.18*/for(weightedTag <- weightedTags.sorted) yield /*14.57*/ {_display_(Seq[Any](format.raw/*14.59*/("""
                  <span class="tag-padding">
                      <a href=""""),_display_(Seq[Any](/*16.33*/routes/*16.39*/.Tags.search(weightedTag._1))),format.raw/*16.67*/("""" style="font-size: """),_display_(Seq[Any](/*16.88*/(weightedTag._2))),format.raw/*16.104*/("""em">"""),_display_(Seq[Any](/*16.109*/weightedTag/*16.120*/._1)),format.raw/*16.123*/("""</a>
                  </span>
                """)))})),format.raw/*18.18*/("""
            """)))})),format.raw/*19.14*/("""
        </div>
    </div>
""")))})),format.raw/*22.2*/("""
"""))}
    }
    
    def render(weightedTags:List[scala.Tuple2[String, Double]],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(weightedTags)(user)
    
    def f:((List[scala.Tuple2[String, Double]]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (weightedTags) => (user) => apply(weightedTags)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:24 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/tagList.scala.html
                    HASH: 10dab94bfde7919feb0979114601369e5cd308ad
                    MATRIX: 637->1|805->75|842->78|866->94|905->96|1156->311|1189->335|1229->337|1293->383|1306->388|1345->389|1399->407|1454->446|1494->448|1608->526|1623->532|1673->560|1730->581|1769->597|1811->602|1832->613|1858->616|1938->664|1984->678|2043->706
                    LINES: 20->1|23->1|25->3|25->3|25->3|33->11|33->11|33->11|35->13|35->13|35->13|36->14|36->14|36->14|38->16|38->16|38->16|38->16|38->16|38->16|38->16|38->16|40->18|41->19|44->22
                    -- GENERATED --
                */
            