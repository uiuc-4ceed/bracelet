
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object collectionofdatasets extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template16[List[models.Dataset],List[models.Collection],Option[List[models.Collection]],models.Collection,List[Previewer],Map[UUID, Int],Option[Map[ProjectSpace, Boolean]],Int,Int,Int,Int,Int,Boolean,List[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(datasetsInCollection: List[models.Dataset],
  childCollections: List[models.Collection],
  parentCollections: Option[List[models.Collection]],
  collection: models.Collection,
  previewers: List[Previewer],
  commentMap : Map[UUID, Int],
  collectionSpaces_canRemove : Option[Map[ProjectSpace, Boolean]],
  prevd: Int, nextd: Int, prevcc: Int, nextcc: Int, limit: Int, canAddToParent: Boolean,
  userSelections: List[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import models.ResourceRef

import models.UUID

import api.Permission

import _root_.util.Formatters._

def /*16.2*/showPreview/*16.13*/(name: String, url: String, collection_id: String):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*16.67*/("""
<script>
    var Configuration = """),format.raw/*18.25*/("""{"""),format.raw/*18.26*/("""}"""),format.raw/*18.27*/(""";
    Configuration.tab = "#previewer_"""),_display_(Seq[Any](/*19.38*/name)),format.raw/*19.42*/("""";
    Configuration.url = """"),_display_(Seq[Any](/*20.27*/url)),format.raw/*20.30*/("""";
    Configuration.collection_id = """"),_display_(Seq[Any](/*21.37*/collection_id)),format.raw/*21.50*/("""";
</script>
<script type="text/javascript" src=""""),_display_(Seq[Any](/*23.38*/(url))),format.raw/*23.43*/(""""></script>
""")))};
Seq[Any](format.raw/*9.95*/("""

"""),format.raw/*15.1*/("""
"""),format.raw/*24.2*/("""

"""),_display_(Seq[Any](/*26.2*/main(collection.name)/*26.23*/ {_display_(Seq[Any](format.raw/*26.25*/("""
<script src=""""),_display_(Seq[Any](/*27.15*/routes/*27.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*27.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*28.15*/routes/*28.21*/.Assets.at("javascripts/collectionDatasetsList.js"))),format.raw/*28.72*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*29.15*/routes/*29.21*/.Assets.at("javascripts/collectionListProcess.js"))),format.raw/*29.71*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*30.15*/routes/*30.21*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*30.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*31.15*/routes/*31.21*/.Assets.at("javascripts/spaceModify.js"))),format.raw/*31.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*32.15*/routes/*32.21*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*32.66*/("""" language="javascript"></script>
<script src=""""),_display_(Seq[Any](/*33.15*/routes/*33.21*/.Assets.at("javascripts/select.js"))),format.raw/*33.56*/("""" type="text/javascript"></script>

	<div class="row">
		<!-- left column -->
		<div class="col-md-8">
			<div class="row">
				<ol class="breadcrumb">

				"""),_display_(Seq[Any](/*41.6*/collectionSpaces_canRemove/*41.32*/ match/*41.38*/ {/*42.6*/case Some(spaces_map) =>/*42.30*/ {_display_(Seq[Any](format.raw/*42.32*/("""
						"""),_display_(Seq[Any](/*43.8*/if(spaces_map.size == 1)/*43.32*/ {_display_(Seq[Any](format.raw/*43.34*/("""
								<li><span class="glyphicon glyphicon-hdd"></span> <a href=""""),_display_(Seq[Any](/*44.69*/routes/*44.75*/.Spaces.getSpace(spaces_map.head._1.id))),format.raw/*44.114*/("""" title=""""),_display_(Seq[Any](/*44.124*/spaces_map/*44.134*/.head._1.name)),format.raw/*44.147*/(""""> """),_display_(Seq[Any](/*44.151*/Html(ellipsize(spaces_map.head._1.name,18)))),format.raw/*44.194*/("""</a></li>


						""")))}/*47.9*/else/*47.15*/{_display_(Seq[Any](format.raw/*47.16*/(""" """),_display_(Seq[Any](/*47.18*/if(spaces_map.size > 1)/*47.41*/ {_display_(Seq[Any](format.raw/*47.43*/("""
							<li>
							<span class="dropdown">

									<button class="btn-link dropdown-toggle" type="button" id="dropdown_space_list" data-toggle="dropdown"
									aria-haspopup="true" aria-expanded="true">
										<span class="glyphicon glyphicon-hdd"></span> <span class="caret"></span>
									</button>
									<ul class="dropdown-menu" arialanelledby="dropdown_space_list">
									"""),_display_(Seq[Any](/*56.11*/spaces_map/*56.21*/.map/*56.25*/ { case (s,v) =>_display_(Seq[Any](format.raw/*56.41*/("""
									<li><a href=""""),_display_(Seq[Any](/*57.24*/routes/*57.30*/.Spaces.getSpace(s.id))),format.raw/*57.52*/("""" title=""""),_display_(Seq[Any](/*57.62*/s/*57.63*/.name)),format.raw/*57.68*/(""""><span class="glyphicon glyphicon-hdd"></span> """),_display_(Seq[Any](/*57.117*/Html(ellipsize(s.name, 18)))),format.raw/*57.144*/("""</a></li>
									""")))})),format.raw/*58.11*/("""
									</ul>


							</span>
							</li>

						""")))}/*65.9*/else/*65.14*/{_display_(Seq[Any](format.raw/*65.15*/("""
							<li><span class="glyphicon glyphicon-user"></span>  <a href= """"),_display_(Seq[Any](/*66.71*/routes/*66.77*/.Profile.viewProfileUUID(collection.author.id))),format.raw/*66.123*/(""""> """),_display_(Seq[Any](/*66.127*/collection/*66.137*/.author.fullName)),format.raw/*66.153*/(""" </a></li>
						""")))})),format.raw/*67.8*/("""
						""")))})),format.raw/*68.8*/("""

					""")))}/*71.6*/case None =>/*71.18*/ {}})),format.raw/*72.6*/("""
					<li><span class="glyphicon glyphicon-th-large"></span> <span title=""""),_display_(Seq[Any](/*73.75*/collection/*73.85*/.name)),format.raw/*73.90*/("""">"""),_display_(Seq[Any](/*73.93*/Html(ellipsize(collection.name, 18)))),format.raw/*73.129*/("""</span></li>

				</ol>
				<div class="col-xs-12 collection-title" id="coll-title">
					<h1 id ="collectiontitle" class="inline"><span class="glyphicon glyphicon-th-large"></span> """),_display_(Seq[Any](/*77.99*/Html(collection.name))),format.raw/*77.120*/("""</h1>
					"""),_display_(Seq[Any](/*78.7*/if(!collection.trash && Permission.checkPermission(Permission.EditCollection, ResourceRef(ResourceRef.collection, collection.id)))/*78.137*/ {_display_(Seq[Any](format.raw/*78.139*/("""
						<h3 id="h-edit-title" class="hiddencomplete">
							<a id="edit-title" href="javascript:updateTitle()" edit="Click to edit title">
								<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
							</a>
						</h3>
					""")))})),format.raw/*84.7*/("""
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<ul class="list-unstyled">
						<li>"""),_display_(Seq[Any](/*90.12*/Messages("owner.label"))),format.raw/*90.35*/(""": <a href= """"),_display_(Seq[Any](/*90.48*/routes/*90.54*/.Profile.viewProfileUUID(collection.author.id))),format.raw/*90.100*/(""""> """),_display_(Seq[Any](/*90.104*/collection/*90.114*/.author.fullName)),format.raw/*90.130*/(""" </a></li>
						<li>Created on """),_display_(Seq[Any](/*91.23*/collection/*91.33*/.created.format("MMM dd, yyyy"))),format.raw/*91.64*/("""</li>
					</ul>
					<div id="aboutdesc" class="col-xs-12 box-white-space"><p id="collection_desc"  class="inline abstract 
					  	"""),_display_(Seq[Any](/*94.10*/if(collection.description.contains("<br>"))/*94.53*/ {_display_(Seq[Any](format.raw/*94.55*/("""
                    		abstract-panel
                    	""")))})),format.raw/*96.23*/("""
						">"""),_display_(Seq[Any](/*97.10*/if(collection.description.length > 0)/*97.47*/ {_display_(Seq[Any](format.raw/*97.49*/(""" """),_display_(Seq[Any](/*97.51*/Html(collection.description)))))}/*97.81*/else/*97.86*/{_display_(Seq[Any](format.raw/*97.87*/("""Add a description""")))})),format.raw/*97.105*/("""</p>
						"""),_display_(Seq[Any](/*98.8*/if(Permission.checkPermission(Permission.EditCollection, ResourceRef(ResourceRef.collection, collection.id)) && !collection.trash)/*98.138*/ {_display_(Seq[Any](format.raw/*98.140*/("""
							<a class="hiddencomplete" id="edit-description" href="javascript:updateDescription()" title ="Click to edit description">
								<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
							</a>
						""")))})),format.raw/*102.8*/("""
					</div>
				</div>
			</div>
			"""),_display_(Seq[Any](/*106.5*/if(user.isDefined)/*106.23*/ {_display_(Seq[Any](format.raw/*106.25*/("""
				<div class="row">
					<div class="col-xs-12">
						<hr/>
						"""),_display_(Seq[Any](/*110.8*/if(collection.trash)/*110.28*/{_display_(Seq[Any](format.raw/*110.29*/("""
							<button id="restoreButton" onclick="restoreCollection('"""),_display_(Seq[Any](/*111.64*/(collection.id))),format.raw/*111.79*/("""', true, '"""),_display_(Seq[Any](/*111.90*/(routes.Collections.list("",owner=Some(user.get.id.stringify),showTrash=true)))),format.raw/*111.168*/("""')"
							class="btn btn-link" title="Restore Collection but not Enclosed Datasets">
								<span class="glyphicon glyphicon-exclamation-sign"></span> Restore</button>
						""")))})),format.raw/*114.8*/("""
							<!-- If user can delete, the button is enabled, otherwise the button is present but disabled to provide consistent UE. -->
                        """),_display_(Seq[Any](/*116.26*/if(Permission.checkPermission(Permission.DeleteCollection, ResourceRef(ResourceRef.collection, collection.id)))/*116.137*/{_display_(Seq[Any](format.raw/*116.138*/("""
                            <button id="deleteButton" onclick="confirmDeleteResource('collection','collection','"""),_display_(Seq[Any](/*117.114*/(collection.id))),format.raw/*117.129*/("""','"""),_display_(Seq[Any](/*117.133*/(collection.name.replace("'","&#39;")))),format.raw/*117.171*/("""', true, '"""),_display_(Seq[Any](/*117.182*/(routes.Collections.list("")))),format.raw/*117.211*/("""')"
                                class="btn btn-link" title="Delete Collection but not Enclosed Datasets">
                                    <span class="glyphicon glyphicon-trash"></span> Delete</button>
                        """)))}/*120.27*/else/*120.32*/{_display_(Seq[Any](format.raw/*120.33*/("""
                            <button disabled id="deleteButton"
                                class="btn btn-link" title="Delete Collection but not Enclosed Datasets">
                                    <span class="glyphicon glyphicon-trash"></span> Delete</button>
                        """)))})),format.raw/*124.26*/("""

						"""),_display_(Seq[Any](/*126.8*/if(user.isDefined && collectionSpaces_canRemove.isDefined && !collectionSpaces_canRemove.isEmpty)/*126.105*/{_display_(Seq[Any](format.raw/*126.106*/("""
<!-- 							<a href=""""),_display_(Seq[Any](/*127.23*/routes/*127.29*/.Collections.users(collection.id))),format.raw/*127.62*/("""" class="btn btn-link">
								<span class="glyphicon glyphicon-user"></span> Collaborators</a>
 -->						""")))})),format.raw/*129.12*/("""
						"""),_display_(Seq[Any](/*130.8*/if(Permission.checkPermission(Permission.DownloadFiles, ResourceRef(ResourceRef.collection, collection.id)))/*130.116*/ {_display_(Seq[Any](format.raw/*130.118*/("""
							<a id='download-url' href="#"
							onclick="window.open(jsRoutes.api.Collections.download('"""),_display_(Seq[Any](/*132.65*/collection/*132.75*/.id)),format.raw/*132.78*/("""').url, '_blank');"
							class="btn btn-link" title="Download All Files as Zip" role="button">
								<span class="glyphicon glyphicon-download-alt"></span>
								Download All Files
							</a>
						""")))}/*137.9*/else/*137.14*/{_display_(Seq[Any](format.raw/*137.15*/("""
							<a id='download-url' href="#" disabled
							class="btn btn-link disabled" title="You don't have permission to Download this collection" role="button">
								<span class="glyphicon glyphicon-download-alt"></span>
								Download All Files
							</a>
						""")))})),format.raw/*143.8*/("""
						<hr/>
					</div>
				</div>
			""")))})),format.raw/*147.5*/("""
			<div class="row">
				<div style="clear: both;"></div>

				<div id="recommendPanel"
						class="panel panel-default"
						style="margin-top: 20px; display : none;">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-parent="#accordion"
							href="#collapseThree"
							aria-expanded="true"
							style="float:left;">
								Also follow these?
							</a>
							<a style="float:right;" href="javascript:$('#recommendPanel').slideToggle('slow');">x</a>
							<div style="clear : both;"></div>
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse in" aria-expanded="true">
						<div id="recommendDiv" class="panel-body">
						</div>
					</div>
				</div>

				<script src=""""),_display_(Seq[Any](/*172.19*/routes/*172.25*/.Assets.at("javascripts/recommendation.js"))),format.raw/*172.68*/("""" type="text/javascript"></script>

				<script>
					$(document).on('click', '.followButton', function() """),format.raw/*175.58*/("""{"""),format.raw/*175.59*/("""
						var id = $(this).attr('objectId');
						var name = $(this).attr('objectName');
						var type = $(this).attr('objectType');
						if ($(this).attr('id') === '') """),format.raw/*179.38*/("""{"""),format.raw/*179.39*/("""
						  followHandler.call(this, jsRoutes, id, name, type, undefined, undefined);
						"""),format.raw/*181.7*/("""}"""),format.raw/*181.8*/(""" else """),format.raw/*181.14*/("""{"""),format.raw/*181.15*/("""
						  followHandler.call(this, jsRoutes, id, name, type, function(data) """),format.raw/*182.75*/("""{"""),format.raw/*182.76*/("""
								recommendationHandler(jsRoutes, $('#recommendPanel'), $('#recommendDiv'),
											  data['recommendations']);
							"""),format.raw/*185.8*/("""}"""),format.raw/*185.9*/(""", undefined);
						"""),format.raw/*186.7*/("""}"""),format.raw/*186.8*/("""
					"""),format.raw/*187.6*/("""}"""),format.raw/*187.7*/(""");
				</script>
			</div>
			<div class="row">
				<div class="col-md-12">
					"""),_display_(Seq[Any](/*192.7*/for((p,i) <- previewers.zipWithIndex) yield /*192.44*/ {_display_(Seq[Any](format.raw/*192.46*/("""
						<h3>"""),_display_(Seq[Any](/*193.12*/p/*193.13*/.id)),format.raw/*193.16*/("""</h3>
						<div class="previewDiv" id="previewer_"""),_display_(Seq[Any](/*194.46*/(p.id))),format.raw/*194.52*/("""_"""),_display_(Seq[Any](/*194.54*/i)),format.raw/*194.55*/(""""></div>
						"""),_display_(Seq[Any](/*195.8*/showPreview(p.id + "_" + i, routes.Assets.at("javascripts/previewers") + "/" + p.id + "/" + p.main, collection.id.stringify))),format.raw/*195.132*/("""
					""")))})),format.raw/*196.7*/("""
				</div>
			</div>
			<div class="row">
			  <div class="col-md-8"></div>
				<script type="text/javascript">
				  $(document).ready(function() """),format.raw/*202.36*/("""{"""),format.raw/*202.37*/("""
  				  $(".js-sort-single").select2("""),format.raw/*203.38*/("""{"""),format.raw/*203.39*/("""minimumResultsForSearch: Infinity"""),format.raw/*203.72*/("""}"""),format.raw/*203.73*/(""");
  				  var order = 'dateN';
  				  if($.cookie('sort-order') != null) """),format.raw/*205.44*/("""{"""),format.raw/*205.45*/("""
				    //removing quotes from around cookie value 
			   	    order = $.cookie('sort-order').replace(/['"]+/g, '');
			  	  """),format.raw/*208.9*/("""}"""),format.raw/*208.10*/(""" 
  				  $(".js-sort-single").val(order).trigger("change");
				  $(".js-sort-single").on('select2:select', function (evt) """),format.raw/*210.64*/("""{"""),format.raw/*210.65*/(""" 
		             $(window).trigger("sortchange");
	    		  """),format.raw/*212.10*/("""}"""),format.raw/*212.11*/(""");
	    		  $(window).on('sortchange', function() """),format.raw/*213.48*/("""{"""),format.raw/*213.49*/("""
				      var sort = $(".js-sort-single").val();
	      			  //Update cookie
			          $.cookie('sort-order', sort, """),format.raw/*216.43*/("""{"""),format.raw/*216.44*/(""" path: '/' """),format.raw/*216.55*/("""}"""),format.raw/*216.56*/(""");
			          //Go get the list sorted the new way     
          	          var request = jsRoutes.controllers.Collections.collection('"""),_display_(Seq[Any](/*218.82*/collection/*218.92*/.id.toString())),format.raw/*218.106*/("""');
			          window.location = request.url;
        		   """),format.raw/*220.14*/("""}"""),format.raw/*220.15*/(""");
				  """),format.raw/*221.7*/("""}"""),format.raw/*221.8*/(""");
				</script>
				  """),_display_(Seq[Any](/*223.8*/if(play.Play.application().configuration().getBoolean("sortInMemory"))/*223.78*/ {_display_(Seq[Any](format.raw/*223.80*/("""
					  <label class='sortchoice' for="js-sort-single">Sort By:
						  <select class="js-sort-single">
							  <option value="dateN">Newest</option>
							  <option value="dateO">Oldest</option>
							  <option value="titleA">Title (A-Z)</option>
							  <option value="titleZ">Title (Z-A)</option>
							  <option value="sizeL">Size (L)</option>
							  <option value="sizeS">Size (S)</option>
						  </select>
					  </label>
				  """)))})),format.raw/*234.8*/("""
					
			  </div>
			</div>
			<div class="row hideTree">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-8">
							<h2>"""),_display_(Seq[Any](/*242.13*/Messages("a.in.b", Messages("datasets.title"), Messages("collection.title")))),format.raw/*242.89*/("""</h2>
						</div>
						<div class="col-xs-4 valign-link">
						"""),_display_(Seq[Any](/*245.8*/if(Permission.checkPermission(Permission.EditCollection, ResourceRef(ResourceRef.collection, collection.id)))/*245.117*/{_display_(Seq[Any](format.raw/*245.118*/("""
							<a id="create_dataset" href=""""),_display_(Seq[Any](/*246.38*/routes/*246.44*/.Datasets.newDataset(None, Some(collection.id.toString())))),format.raw/*246.102*/(""""
							class="btn btn-link pull-right" title="Create Dataset">
								<span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*248.55*/Messages("create.title", Messages("dataset.title")))),format.raw/*248.106*/("""</a>
						""")))})),format.raw/*249.8*/("""
						</div>
					</div>
					<div id="datasets">
					"""),_display_(Seq[Any](/*253.7*/collections/*253.18*/.datasetsInCollection(datasetsInCollection, commentMap, collection.id, prevd, nextd, userSelections))),format.raw/*253.118*/("""
					</div>
				</div>
			</div>
			<div class="row top-padding hideTree">
				<div class="col-xs-12" id="childCollectionsDiv">
					<div class="row">
						<div class="col-xs-8">
							<h2>"""),_display_(Seq[Any](/*261.13*/Messages("a.in.b", "Child " +Messages("collections.title"), Messages("collection.title")))),format.raw/*261.102*/("""</h2>
						</div>
						<div class="col-xs-4 valign-link">
							"""),_display_(Seq[Any](/*264.9*/if(Permission.checkPermission(Permission.EditCollection, ResourceRef(ResourceRef.collection, collection.id)))/*264.118*/{_display_(Seq[Any](format.raw/*264.119*/("""
								<a id="create_childcollection"
								href=""""),_display_(Seq[Any](/*266.16*/routes/*266.22*/.Collections.newCollectionWithParent(collection.id))),format.raw/*266.73*/(""""
								class="btn btn-link pull-right" title="Create Child Collection">
									<span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*268.56*/Messages("create.title", Messages("collection.child.title")))),format.raw/*268.116*/("""</a>
							""")))})),format.raw/*269.9*/("""
						</div>
					</div>
					<div id="childCollections">
						"""),_display_(Seq[Any](/*273.8*/collections/*273.19*/.childCollections(childCollections, collection, prevcc, nextcc))),format.raw/*273.82*/("""
					</div>
				</div>
			</div>

		</div>

		<!-- right column -->
		<div class="col-md-4 hideTree">
			<div class="bottom-padding">
				"""),_display_(Seq[Any](/*283.6*/spaces/*283.12*/.spaceAllocation(collection.id, ResourceRef.collection, collectionSpaces_canRemove.getOrElse(Map.empty), Some(collection)))),format.raw/*283.134*/("""
			</div>
				"""),_display_(Seq[Any](/*285.6*/collections/*285.17*/.parentAllocation(collection,collection.id,ResourceRef.collection, parentCollections.getOrElse(List.empty),canAddToParent))),format.raw/*285.139*/("""
		</div>
	</div>


<script language="javascript">

	var collectionId = """"),_display_(Seq[Any](/*292.23*/collection/*292.33*/.id.stringify)),format.raw/*292.46*/("""";
	var cur_title;
	function updateTitle() """),format.raw/*294.25*/("""{"""),format.raw/*294.26*/("""
	 cur_title = $('#collectiontitle').text().trim();
	 $('<div class="inline edit_title_div"> </span>').insertAfter($('#collectiontitle'));
	 $('.edit_title_div').append('<input type = "text" id="title_input" class="form-control"/>');
	 $('.edit_title_div').append('<div class="hiddencomplete" id="title-error"> <span class="error"> Title is required </span> </div>');
	 $('.edit_title_div').append('<button id="update_title" class="btn btn-sm btn-primary btn-margins" onclick="saveTitle()"> <span class="glyphicon glyphicon-send"></span> Save </button>');
	 $('.edit_title_div').append('<button id="cancel_title" class="btn btn-sm btn-default btn-margins edit-tab" onclick="cancelTitle()"> <span class="glyphicon glyphicon-remove"></span> Cancel </button>');

	 $('#title_input').val(cur_title);
	 $('#collectiontitle').text("");
	 $('#h-edit-title').css("display", "none");
	 $('#h-edit-title').addClass("hiddencomplete");
	 $('#coll-title').removeClass("collection-title");
	"""),format.raw/*307.2*/("""}"""),format.raw/*307.3*/("""

	function saveTitle() """),format.raw/*309.23*/("""{"""),format.raw/*309.24*/("""
		if($('#title_input').val().length < 1) """),format.raw/*310.42*/("""{"""),format.raw/*310.43*/("""
			$('#title-error').show();
			return false;
		"""),format.raw/*313.3*/("""}"""),format.raw/*313.4*/(""" else """),format.raw/*313.10*/("""{"""),format.raw/*313.11*/("""
			$('#title-error').addClass('hiddencomplete');
		"""),format.raw/*315.3*/("""}"""),format.raw/*315.4*/("""
		var name = $('#title_input').val();
		var encName = htmlEncode(name);
		jsonData = JSON.stringify("""),format.raw/*318.29*/("""{"""),format.raw/*318.30*/(""""name": encName"""),format.raw/*318.45*/("""}"""),format.raw/*318.46*/(""");

		var request = jsRoutes.api.Collections.updateCollectionName(collectionId).ajax("""),format.raw/*320.82*/("""{"""),format.raw/*320.83*/("""
			data: jsonData,
			type: 'PUT',
			contentType: "application/json"
		"""),format.raw/*324.3*/("""}"""),format.raw/*324.4*/(""");

		request.done(function(response, textStatus, jqXHR) """),format.raw/*326.54*/("""{"""),format.raw/*326.55*/("""
			$('#collectiontitle').text(encName.replace(/\n/g, "<br>"));
			$('#collectiontitle').prepend('<span class="glyphicon glyphicon-th-large"></span> ');
            $('.edit_title_div').remove();
			$('#collectiontitle').css("display", "inline");
			$('#h-edit-title').removeClass("inline");
			$('#h-edit-title').css("display", "");
			$('#coll-title').addClass("collection-title");
			$('#coll-title').mouseleave();
		"""),format.raw/*335.3*/("""}"""),format.raw/*335.4*/(""");

		request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*337.57*/("""{"""),format.raw/*337.58*/("""
			console.error("The following error occurred: " + textStatus, errorThrown);
			var errMsg = "Yoy must be logged in to update the information about a collection.";
			if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*340.46*/("""{"""),format.raw/*340.47*/("""
				notify("The collection information was not updated due to: "+ errorThrown, "error");
			"""),format.raw/*342.4*/("""}"""),format.raw/*342.5*/("""
		"""),format.raw/*343.3*/("""}"""),format.raw/*343.4*/(""");

	"""),format.raw/*345.2*/("""}"""),format.raw/*345.3*/("""
	function cancelTitle() """),format.raw/*346.25*/("""{"""),format.raw/*346.26*/("""
		$('#collectiontitle').text(cur_title);
		$('#collectiontitle').prepend('<span class="glyphicon glyphicon-th-large"></span> ');
        $('.edit_title_div').remove();
		$('#collectiontitle').css("display", "inline");
		$('#h-edit-title').removeClass("inline");
		$('#h-edit-title').css("display", "");
		$('#coll-title').addClass("collection-title");
		$('#coll-title').mouseleave();
	"""),format.raw/*355.2*/("""}"""),format.raw/*355.3*/("""

	var cur_description;

	function updateDescription() """),format.raw/*359.31*/("""{"""),format.raw/*359.32*/("""
		cur_description = $('#collection_desc').html().trim();
		$('<div class = "edit_desc"></div>').insertAfter($('#collection_desc'));
		$(".edit_desc").append('<textarea type ="text" id = "desc_input" class="form-control"/>');
		$(".edit_desc").append('<br/>');
		$(".edit_desc").append('<button id = "update_desc" class = "btn btn-sm btn-primary" onclick = "saveDescription()"> <span class="glyphicon glyphicon-send"></span>  Save </button>');
		$(".edit_desc").append('<button id = "cancel_desc" class = "btn btn-default btn-sm edit-tab" onclick = "cancelDescription()"> <span class="glyphicon glyphicon-remove"></span>  Cancel </button>');
		if(cur_description.indexOf("Add a description") != 0) """),format.raw/*366.57*/("""{"""),format.raw/*366.58*/("""
			$('#desc_input').val(htmlDecode(cur_description.replace(new RegExp("<br>", "g"), "\n")));
		"""),format.raw/*368.3*/("""}"""),format.raw/*368.4*/("""
		$('#collection_desc').text("");
		$('#edit-description').css("display", "none");
		$('#edit-description').addClass("hiddencomplete");
	"""),format.raw/*372.2*/("""}"""),format.raw/*372.3*/("""

	function saveDescription() """),format.raw/*374.29*/("""{"""),format.raw/*374.30*/("""
		var description = $('#desc_input').val();
		var encDescription = htmlEncode(description);
		jsonData = JSON.stringify("""),format.raw/*377.29*/("""{"""),format.raw/*377.30*/(""""description": encDescription"""),format.raw/*377.59*/("""}"""),format.raw/*377.60*/(""");

		var request = jsRoutes.api.Collections.updateCollectionDescription(collectionId).ajax("""),format.raw/*379.89*/("""{"""),format.raw/*379.90*/("""
			data: jsonData,
			type: 'PUT',
			contentType: "application/json"
		"""),format.raw/*383.3*/("""}"""),format.raw/*383.4*/(""");

		request.done(function(response, textStatus,jqXHR) """),format.raw/*385.53*/("""{"""),format.raw/*385.54*/("""
			$('#collection_desc').html(urlify(htmlEncode(description).replace(/\n/g, "<br>")));
			$('.edit_desc').remove();
			$('#edit-description').css("display", "");
			$('#edit-description').removeClass("inline");
			if ($('#collection_desc').html().indexOf("<br>")>=0) """),format.raw/*390.57*/("""{"""),format.raw/*390.58*/("""
			$('#collection_desc').addClass("abstract-panel");
			"""),format.raw/*392.4*/("""}"""),format.raw/*392.5*/(""" else """),format.raw/*392.11*/("""{"""),format.raw/*392.12*/("""
			$('#collection_desc').removeClass("abstract-panel");
			"""),format.raw/*394.4*/("""}"""),format.raw/*394.5*/("""
			$('#collection_desc').css("display", "");
			$('#collection_desc').mouseleave();
			notify("Collection description updated successfully", "success", false, 2000);
		"""),format.raw/*398.3*/("""}"""),format.raw/*398.4*/(""");

		request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*400.57*/("""{"""),format.raw/*400.58*/("""
			console.error("The following error occurred: " + textStatus, errorThrown);
			var errMsg = " You must be logged in to update the information about a collection.";
			if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*403.46*/("""{"""),format.raw/*403.47*/("""
				notify("The collection information was not updated due to: " + errorThrown, "error");
			"""),format.raw/*405.4*/("""}"""),format.raw/*405.5*/("""
		"""),format.raw/*406.3*/("""}"""),format.raw/*406.4*/(""");
	"""),format.raw/*407.2*/("""}"""),format.raw/*407.3*/("""

	function cancelDescription() """),format.raw/*409.31*/("""{"""),format.raw/*409.32*/("""
		$('#collection_desc').html(cur_description);
		$('.edit_desc').remove();
		$('#edit-description').css("display", "");
		$('#edit-description').removeClass("inline");
		$('#collection_desc').css("display", "");
		$('#collection_desc').mouseleave();
	"""),format.raw/*416.2*/("""}"""),format.raw/*416.3*/("""
	
	function urlify(text) """),format.raw/*418.24*/("""{"""),format.raw/*418.25*/("""
        var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return text.replace(urlRegex, function(url) """),format.raw/*420.53*/("""{"""),format.raw/*420.54*/("""
            return '<a href="' + url + '">' + url + '</a>';
        """),format.raw/*422.9*/("""}"""),format.raw/*422.10*/(""")
    """),format.raw/*423.5*/("""}"""),format.raw/*423.6*/("""

	$(document).ready(function()"""),format.raw/*425.30*/("""{"""),format.raw/*425.31*/("""
		if("""),_display_(Seq[Any](/*426.7*/collection/*426.17*/.spaces.size)),format.raw/*426.29*/(""" < 1 ) """),format.raw/*426.36*/("""{"""),format.raw/*426.37*/("""
			$('#collection-users').hide();
		"""),format.raw/*428.3*/("""}"""),format.raw/*428.4*/("""
		if("""),_display_(Seq[Any](/*429.7*/Permission/*429.17*/.checkPermission(Permission.EditCollection, ResourceRef(ResourceRef.collection, collection.id)))),format.raw/*429.112*/(""") """),format.raw/*429.114*/("""{"""),format.raw/*429.115*/("""
			    $(document).on('mouseenter', '#coll-title', function() """),format.raw/*430.63*/("""{"""),format.raw/*430.64*/("""
                    $('#h-edit-title').removeClass("hiddencomplete");
                    $('#h-edit-title').addClass("inline");
                 """),format.raw/*433.18*/("""}"""),format.raw/*433.19*/(""").on('mouseleave', '#coll-title', function() """),format.raw/*433.64*/("""{"""),format.raw/*433.65*/("""
                    $('#h-edit-title').removeClass("inline");
                    $('#h-edit-title').addClass("hiddencomplete");
                 """),format.raw/*436.18*/("""}"""),format.raw/*436.19*/(""");

                 $(document).on('mouseenter', '#aboutdesc', function() """),format.raw/*438.72*/("""{"""),format.raw/*438.73*/("""
                    $('#edit-description').removeClass("hiddencomplete");
                    $('#edit-description').addClass("inline");
                 """),format.raw/*441.18*/("""}"""),format.raw/*441.19*/(""").on('mouseleave', '#aboutdesc', function() """),format.raw/*441.63*/("""{"""),format.raw/*441.64*/("""
                    $('#edit-description').removeClass("inline");
                    $('#edit-description').addClass("hiddencomplete");
                 """),format.raw/*444.18*/("""}"""),format.raw/*444.19*/(""");
		"""),format.raw/*445.3*/("""}"""),format.raw/*445.4*/("""
	"""),format.raw/*446.2*/("""}"""),format.raw/*446.3*/(""");

	function updateDatasets(page) """),format.raw/*448.32*/("""{"""),format.raw/*448.33*/("""
		var request = jsRoutes.controllers.Collections.getUpdatedDatasets(""""),_display_(Seq[Any](/*449.71*/collection/*449.81*/.id)),format.raw/*449.84*/("""", page).ajax("""),format.raw/*449.98*/("""{"""),format.raw/*449.99*/("""
			type: 'GET'
		"""),format.raw/*451.3*/("""}"""),format.raw/*451.4*/(""");
		request.done(function(response, textStatus, jsXHR)"""),format.raw/*452.53*/("""{"""),format.raw/*452.54*/("""
			$('#datasets').html("");
			$('#datasets').html(response);
		"""),format.raw/*455.3*/("""}"""),format.raw/*455.4*/(""");
		request.fail(function(jqXHR, testStatus, errorThrown)"""),format.raw/*456.56*/("""{"""),format.raw/*456.57*/("""
			console.error("The following error occurred: " + textStatus, errorThrown);
			errMsg = "You must be logged in to see datasets.";
			if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*459.46*/("""{"""),format.raw/*459.47*/("""
				notify("Error in getting datasets within collection: " + errorThrown, "error");
			"""),format.raw/*461.4*/("""}"""),format.raw/*461.5*/("""
		"""),format.raw/*462.3*/("""}"""),format.raw/*462.4*/(""")
	"""),format.raw/*463.2*/("""}"""),format.raw/*463.3*/("""

	function updateChildCollections(page) """),format.raw/*465.40*/("""{"""),format.raw/*465.41*/("""
		var request = jsRoutes.controllers.Collections.getUpdatedChildCollections(""""),_display_(Seq[Any](/*466.79*/collection/*466.89*/.id)),format.raw/*466.92*/("""", page).ajax("""),format.raw/*466.106*/("""{"""),format.raw/*466.107*/("""
			type: 'GET'
		"""),format.raw/*468.3*/("""}"""),format.raw/*468.4*/(""");
		request.done(function(response, textStatus, jsXHR)"""),format.raw/*469.53*/("""{"""),format.raw/*469.54*/("""
			$('#childCollections').html("");
			$('#childCollections').html(response);
		"""),format.raw/*472.3*/("""}"""),format.raw/*472.4*/(""");
		request.fail(function(jqXHR, testStatus, errorThrown)"""),format.raw/*473.56*/("""{"""),format.raw/*473.57*/("""
			console.error("The following error occurred: " + textStatus, errorThrown);
			errMsg = "You must be logged in to see child collections.";
			if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*476.46*/("""{"""),format.raw/*476.47*/("""
				notify("Error in getting child collections: " + errorThrown, "error");
			"""),format.raw/*478.4*/("""}"""),format.raw/*478.5*/("""
		"""),format.raw/*479.3*/("""}"""),format.raw/*479.4*/(""")
	"""),format.raw/*480.2*/("""}"""),format.raw/*480.3*/("""

</script>
<script src=""""),_display_(Seq[Any](/*483.15*/routes/*483.21*/.Assets.at("javascripts/handlebars-v1.3.0.js"))),format.raw/*483.67*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*484.15*/routes/*484.21*/.Assets.at("javascripts/handlebars-loader.js"))),format.raw/*484.67*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*485.15*/routes/*485.21*/.Assets.at("javascripts/follow-button.js"))),format.raw/*485.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*486.15*/routes/*486.21*/.Assets.at("javascripts/descriptionSummary.js"))),format.raw/*486.68*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*487.15*/routes/*487.21*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*487.66*/("""" type="text/javascript"></script>
""")))})),format.raw/*488.2*/("""
"""))}
    }
    
    def render(datasetsInCollection:List[models.Dataset],childCollections:List[models.Collection],parentCollections:Option[List[models.Collection]],collection:models.Collection,previewers:List[Previewer],commentMap:Map[UUID, Int],collectionSpaces_canRemove:Option[Map[ProjectSpace, Boolean]],prevd:Int,nextd:Int,prevcc:Int,nextcc:Int,limit:Int,canAddToParent:Boolean,userSelections:List[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(datasetsInCollection,childCollections,parentCollections,collection,previewers,commentMap,collectionSpaces_canRemove,prevd,nextd,prevcc,nextcc,limit,canAddToParent,userSelections)(flash,user)
    
    def f:((List[models.Dataset],List[models.Collection],Option[List[models.Collection]],models.Collection,List[Previewer],Map[UUID, Int],Option[Map[ProjectSpace, Boolean]],Int,Int,Int,Int,Int,Boolean,List[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (datasetsInCollection,childCollections,parentCollections,collection,previewers,commentMap,collectionSpaces_canRemove,prevd,nextd,prevcc,nextcc,limit,canAddToParent,userSelections) => (flash,user) => apply(datasetsInCollection,childCollections,parentCollections,collection,previewers,commentMap,collectionSpaces_canRemove,prevd,nextd,prevcc,nextcc,limit,canAddToParent,userSelections)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:47 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collectionofdatasets.scala.html
                    HASH: 482fc13016cc4cda1feeb9e83a6060e69801d713
                    MATRIX: 837->1|1506->597|1526->608|1661->662|1723->696|1752->697|1781->698|1856->737|1882->741|1947->770|1972->773|2047->812|2082->825|2168->875|2195->880|2247->490|2276->595|2304->893|2342->896|2372->917|2412->919|2463->934|2478->940|2542->982|2627->1031|2642->1037|2715->1088|2800->1137|2815->1143|2887->1193|2972->1242|2987->1248|3049->1288|3134->1337|3149->1343|3211->1383|3296->1432|3311->1438|3378->1483|3462->1531|3477->1537|3534->1572|3727->1730|3762->1756|3777->1762|3787->1770|3820->1794|3860->1796|3903->1804|3936->1828|3976->1830|4081->1899|4096->1905|4158->1944|4205->1954|4225->1964|4261->1977|4302->1981|4368->2024|4405->2044|4418->2050|4457->2051|4495->2053|4527->2076|4567->2078|4998->2473|5017->2483|5030->2487|5084->2503|5144->2527|5159->2533|5203->2555|5249->2565|5259->2566|5286->2571|5372->2620|5422->2647|5474->2667|5546->2722|5559->2727|5598->2728|5705->2799|5720->2805|5789->2851|5830->2855|5850->2865|5889->2881|5938->2899|5977->2907|6003->2921|6024->2933|6049->2942|6160->3017|6179->3027|6206->3032|6245->3035|6304->3071|6523->3254|6567->3275|6614->3287|6754->3417|6795->3419|7069->3662|7219->3776|7264->3799|7313->3812|7328->3818|7397->3864|7438->3868|7458->3878|7497->3894|7566->3927|7585->3937|7638->3968|7808->4102|7860->4145|7900->4147|7992->4207|8038->4217|8084->4254|8124->4256|8162->4258|8204->4288|8217->4293|8256->4294|8307->4312|8354->4324|8494->4454|8535->4456|8790->4679|8864->4717|8892->4735|8933->4737|9040->4808|9070->4828|9110->4829|9211->4893|9249->4908|9297->4919|9399->4997|9608->5174|9801->5330|9923->5441|9964->5442|10116->5556|10155->5571|10197->5575|10259->5613|10308->5624|10361->5653|10616->5889|10630->5894|10670->5895|10998->6190|11043->6199|11151->6296|11192->6297|11252->6320|11268->6326|11324->6359|11465->6467|11509->6475|11628->6583|11670->6585|11809->6687|11829->6697|11855->6700|12080->6907|12094->6912|12134->6913|12436->7183|12508->7223|13292->7970|13308->7976|13374->8019|13509->8125|13539->8126|13737->8295|13767->8296|13884->8385|13913->8386|13948->8392|13978->8393|14082->8468|14112->8469|14269->8598|14298->8599|14346->8619|14375->8620|14409->8626|14438->8627|14556->8709|14610->8746|14651->8748|14700->8760|14711->8761|14737->8764|14825->8815|14854->8821|14893->8823|14917->8824|14969->8840|15117->8964|15156->8971|15333->9119|15363->9120|15430->9158|15460->9159|15522->9192|15552->9193|15656->9268|15686->9269|15840->9395|15870->9396|16023->9520|16053->9521|16141->9580|16171->9581|16250->9631|16280->9632|16429->9752|16459->9753|16499->9764|16529->9765|16705->9904|16725->9914|16763->9928|16853->9989|16883->9990|16920->9999|16949->10000|17009->10024|17089->10094|17130->10096|17605->10539|17793->10690|17892->10766|17995->10833|18115->10942|18156->10943|18231->10981|18247->10987|18329->11045|18485->11164|18560->11215|18604->11227|18697->11284|18718->11295|18842->11395|19072->11588|19185->11677|19289->11745|19409->11854|19450->11855|19542->11910|19558->11916|19632->11967|19799->12097|19883->12157|19928->12170|20030->12236|20051->12247|20137->12310|20313->12450|20329->12456|20475->12578|20527->12594|20548->12605|20694->12727|20805->12801|20825->12811|20861->12824|20933->12867|20963->12868|21968->13845|21997->13846|22050->13870|22080->13871|22151->13913|22181->13914|22258->13963|22287->13964|22322->13970|22352->13971|22432->14023|22461->14024|22591->14125|22621->14126|22665->14141|22695->14142|22809->14227|22839->14228|22940->14301|22969->14302|23055->14359|23085->14360|23533->14780|23562->14781|23651->14841|23681->14842|23921->15053|23951->15054|24072->15147|24101->15148|24132->15151|24161->15152|24194->15157|24223->15158|24277->15183|24307->15184|24722->15571|24751->15572|24835->15627|24865->15628|25592->16326|25622->16327|25746->16423|25775->16424|25941->16562|25970->16563|26029->16593|26059->16594|26209->16715|26239->16716|26297->16745|26327->16746|26448->16838|26478->16839|26579->16912|26608->16913|26693->16969|26723->16970|27020->17238|27050->17239|27135->17296|27164->17297|27199->17303|27229->17304|27317->17364|27346->17365|27543->17534|27572->17535|27661->17595|27691->17596|27932->17808|27962->17809|28084->17903|28113->17904|28144->17907|28173->17908|28205->17912|28234->17913|28295->17945|28325->17946|28605->18198|28634->18199|28689->18225|28719->18226|28902->18382|28932->18383|29029->18452|29059->18453|29093->18459|29122->18460|29182->18491|29212->18492|29255->18499|29275->18509|29310->18521|29346->18528|29376->18529|29441->18566|29470->18567|29513->18574|29533->18584|29652->18679|29684->18681|29715->18682|29807->18745|29837->18746|30013->18893|30043->18894|30117->18939|30147->18940|30323->19087|30353->19088|30457->19163|30487->19164|30671->19319|30701->19320|30774->19364|30804->19365|30988->19520|31018->19521|31051->19526|31080->19527|31110->19529|31139->19530|31203->19565|31233->19566|31341->19637|31361->19647|31387->19650|31430->19664|31460->19665|31506->19683|31535->19684|31619->19739|31649->19740|31742->19805|31771->19806|31858->19864|31888->19865|32095->20043|32125->20044|32241->20132|32270->20133|32301->20136|32330->20137|32361->20140|32390->20141|32460->20182|32490->20183|32606->20262|32626->20272|32652->20275|32696->20289|32727->20290|32773->20308|32802->20309|32886->20364|32916->20365|33025->20446|33054->20447|33141->20505|33171->20506|33387->20693|33417->20694|33524->20773|33553->20774|33584->20777|33613->20778|33644->20781|33673->20782|33736->20808|33752->20814|33821->20860|33907->20909|33923->20915|33992->20961|34078->21010|34094->21016|34159->21058|34245->21107|34261->21113|34331->21160|34417->21209|34433->21215|34501->21260|34569->21296
                    LINES: 20->1|37->16|37->16|39->16|41->18|41->18|41->18|42->19|42->19|43->20|43->20|44->21|44->21|46->23|46->23|48->9|50->15|51->24|53->26|53->26|53->26|54->27|54->27|54->27|55->28|55->28|55->28|56->29|56->29|56->29|57->30|57->30|57->30|58->31|58->31|58->31|59->32|59->32|59->32|60->33|60->33|60->33|68->41|68->41|68->41|68->42|68->42|68->42|69->43|69->43|69->43|70->44|70->44|70->44|70->44|70->44|70->44|70->44|70->44|73->47|73->47|73->47|73->47|73->47|73->47|82->56|82->56|82->56|82->56|83->57|83->57|83->57|83->57|83->57|83->57|83->57|83->57|84->58|91->65|91->65|91->65|92->66|92->66|92->66|92->66|92->66|92->66|93->67|94->68|96->71|96->71|96->72|97->73|97->73|97->73|97->73|97->73|101->77|101->77|102->78|102->78|102->78|108->84|114->90|114->90|114->90|114->90|114->90|114->90|114->90|114->90|115->91|115->91|115->91|118->94|118->94|118->94|120->96|121->97|121->97|121->97|121->97|121->97|121->97|121->97|121->97|122->98|122->98|122->98|126->102|130->106|130->106|130->106|134->110|134->110|134->110|135->111|135->111|135->111|135->111|138->114|140->116|140->116|140->116|141->117|141->117|141->117|141->117|141->117|141->117|144->120|144->120|144->120|148->124|150->126|150->126|150->126|151->127|151->127|151->127|153->129|154->130|154->130|154->130|156->132|156->132|156->132|161->137|161->137|161->137|167->143|171->147|196->172|196->172|196->172|199->175|199->175|203->179|203->179|205->181|205->181|205->181|205->181|206->182|206->182|209->185|209->185|210->186|210->186|211->187|211->187|216->192|216->192|216->192|217->193|217->193|217->193|218->194|218->194|218->194|218->194|219->195|219->195|220->196|226->202|226->202|227->203|227->203|227->203|227->203|229->205|229->205|232->208|232->208|234->210|234->210|236->212|236->212|237->213|237->213|240->216|240->216|240->216|240->216|242->218|242->218|242->218|244->220|244->220|245->221|245->221|247->223|247->223|247->223|258->234|266->242|266->242|269->245|269->245|269->245|270->246|270->246|270->246|272->248|272->248|273->249|277->253|277->253|277->253|285->261|285->261|288->264|288->264|288->264|290->266|290->266|290->266|292->268|292->268|293->269|297->273|297->273|297->273|307->283|307->283|307->283|309->285|309->285|309->285|316->292|316->292|316->292|318->294|318->294|331->307|331->307|333->309|333->309|334->310|334->310|337->313|337->313|337->313|337->313|339->315|339->315|342->318|342->318|342->318|342->318|344->320|344->320|348->324|348->324|350->326|350->326|359->335|359->335|361->337|361->337|364->340|364->340|366->342|366->342|367->343|367->343|369->345|369->345|370->346|370->346|379->355|379->355|383->359|383->359|390->366|390->366|392->368|392->368|396->372|396->372|398->374|398->374|401->377|401->377|401->377|401->377|403->379|403->379|407->383|407->383|409->385|409->385|414->390|414->390|416->392|416->392|416->392|416->392|418->394|418->394|422->398|422->398|424->400|424->400|427->403|427->403|429->405|429->405|430->406|430->406|431->407|431->407|433->409|433->409|440->416|440->416|442->418|442->418|444->420|444->420|446->422|446->422|447->423|447->423|449->425|449->425|450->426|450->426|450->426|450->426|450->426|452->428|452->428|453->429|453->429|453->429|453->429|453->429|454->430|454->430|457->433|457->433|457->433|457->433|460->436|460->436|462->438|462->438|465->441|465->441|465->441|465->441|468->444|468->444|469->445|469->445|470->446|470->446|472->448|472->448|473->449|473->449|473->449|473->449|473->449|475->451|475->451|476->452|476->452|479->455|479->455|480->456|480->456|483->459|483->459|485->461|485->461|486->462|486->462|487->463|487->463|489->465|489->465|490->466|490->466|490->466|490->466|490->466|492->468|492->468|493->469|493->469|496->472|496->472|497->473|497->473|500->476|500->476|502->478|502->478|503->479|503->479|504->480|504->480|507->483|507->483|507->483|508->484|508->484|508->484|509->485|509->485|509->485|510->486|510->486|510->486|511->487|511->487|511->487|512->488
                    -- GENERATED --
                */
            