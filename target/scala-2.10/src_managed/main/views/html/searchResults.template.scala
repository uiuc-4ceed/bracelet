
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object searchResults extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[String,Array[models.File],Array[models.Dataset],Array[models.Collection],scala.collection.mutable.HashMap[String, scala.collection.mutable.ListBuffer[scala.Tuple2[String, String]]],scala.collection.mutable.HashMap[String, scala.collection.mutable.ListBuffer[scala.Tuple2[String, String]]],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(query: String, files: Array[models.File], datasets: Array[models.Dataset], collections: Array[models.Collection], mapdatasetIds:scala.collection.mutable.HashMap[String,scala.collection.mutable.ListBuffer[(String, String)]], mapcollectionIds:scala.collection.mutable.HashMap[String,scala.collection.mutable.ListBuffer[(String, String)]])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import scala.collection.mutable.ListBuffer


Seq[Any](format.raw/*1.375*/("""


"""),format.raw/*5.1*/("""
"""),_display_(Seq[Any](/*6.2*/main("Search Results")/*6.24*/ {_display_(Seq[Any](format.raw/*6.26*/("""
	<div class="page-header">
		<h1>Search Results <small>"""),_display_(Seq[Any](/*8.30*/query)),format.raw/*8.35*/("""</small></h1>
	</div>
	"""),_display_(Seq[Any](/*10.3*/if(files.size == 0 && datasets.size == 0 && collections.size == 0)/*10.69*/ {_display_(Seq[Any](format.raw/*10.71*/("""
	<div class="row">
		<div class="col-md-12">
			No results found. Sorry!
		</div>
	</div>
	""")))})),format.raw/*16.3*/("""
	<div class="row">
	  """),_display_(Seq[Any](/*18.5*/if(files.size!=0)/*18.22*/{_display_(Seq[Any](format.raw/*18.23*/("""
	  		"""),_display_(Seq[Any](/*19.7*/if(files.size != 0 && datasets.size != 0 && collections.size != 0)/*19.73*/ {_display_(Seq[Any](format.raw/*19.75*/("""
		    	<div class="col-md-4">
		    """)))})),format.raw/*21.8*/("""
		    """),_display_(Seq[Any](/*22.8*/if(files.size == 0 || datasets.size == 0 || collections.size == 0)/*22.74*/ {_display_(Seq[Any](format.raw/*22.76*/("""
		    	<div class="col-md-6">
		    """)))})),format.raw/*24.8*/("""
		   
		    <h3 id="search-file-title"><span class="glyphicon glyphicon-file"></span> Files</h3>
		    """),_display_(Seq[Any](/*27.8*/for(file <- files) yield /*27.26*/ {_display_(Seq[Any](format.raw/*27.28*/("""
			  <div class="row elasticsearchResult" id="filediv_"""),_display_(Seq[Any](/*28.56*/file/*28.60*/.id.toString)),format.raw/*28.72*/("""">
				<div class="col-md-2">
					"""),_display_(Seq[Any](/*30.7*/if(!file.thumbnail_id.isEmpty)/*30.37*/{_display_(Seq[Any](format.raw/*30.38*/("""
						<a href=""""),_display_(Seq[Any](/*31.17*/(routes.Files.file(file.id)))),format.raw/*31.45*/("""">
							<img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*32.55*/(routes.Files.thumbnail(UUID(file.thumbnail_id.toString().substring(5,file.thumbnail_id.toString().length-1)))))),format.raw/*32.166*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*32.187*/Html(file.filename))),format.raw/*32.206*/("""">
						</a>
					""")))})),format.raw/*34.7*/("""
				</div>
				<div class="col-md-10">
					<dl class="dl-horizontal">
						<dt>Filename:</dt>
						<dd><a href=""""),_display_(Seq[Any](/*39.21*/(routes.Files.file(file.id)))),format.raw/*39.49*/("""">"""),_display_(Seq[Any](/*39.52*/file/*39.56*/.filename)),format.raw/*39.65*/("""</a></dd>
						<dt>Type:</dt>
						<dd>"""),_display_(Seq[Any](/*41.12*/file/*41.16*/.contentType)),format.raw/*41.28*/("""</dd>
						<dt>Uploaded on:</dt>
						<dd>"""),_display_(Seq[Any](/*43.12*/file/*43.16*/.uploadDate)),format.raw/*43.27*/("""</dd>
						<dt>Dataset name(s):</dt>
						<dd>
							"""),_display_(Seq[Any](/*46.9*/for(currMapping <- mapdatasetIds.get(file.id.toString).get) yield /*46.68*/{_display_(Seq[Any](format.raw/*46.69*/("""
								<a href=""""),_display_(Seq[Any](/*47.19*/(routes.Datasets.dataset(UUID(currMapping._1.trim))))),format.raw/*47.71*/("""">"""),_display_(Seq[Any](/*47.74*/currMapping/*47.85*/._2.trim)),format.raw/*47.93*/("""</a><br/>
							""")))})),format.raw/*48.9*/("""
						</dd>
					</dl>
				</div>
			</div>
			<script>

				$("#filediv_"""),_display_(Seq[Any](/*55.18*/file/*55.22*/.id.toString)),format.raw/*55.34*/(""" br").last().remove();

			</script>
			""")))})),format.raw/*58.5*/("""
			</div>
		""")))})),format.raw/*60.4*/("""
	"""),_display_(Seq[Any](/*61.3*/if(datasets.size!=0)/*61.23*/{_display_(Seq[Any](format.raw/*61.24*/("""
			"""),_display_(Seq[Any](/*62.5*/if(files.size != 0 && datasets.size != 0 && collections.size != 0)/*62.71*/ {_display_(Seq[Any](format.raw/*62.73*/("""
		    	<div class="col-md-4">
		    """)))})),format.raw/*64.8*/("""
		    """),_display_(Seq[Any](/*65.8*/if(files.size == 0 || datasets.size == 0 || collections.size == 0)/*65.74*/ {_display_(Seq[Any](format.raw/*65.76*/("""
		    	<div class="col-md-6">
		    """)))})),format.raw/*67.8*/("""
						    
	       <h3 id="search-dataset-title"> <span class="glyphicon glyphicon-briefcase"></span> Datasets</h3>
	    """),_display_(Seq[Any](/*70.7*/for(dataset <- datasets) yield /*70.31*/ {_display_(Seq[Any](format.raw/*70.33*/("""
		  <div class="row elasticsearchResult" id="datasetdiv_"""),_display_(Seq[Any](/*71.58*/dataset/*71.65*/.id.toString)),format.raw/*71.77*/("""">
		    <div class="col-md-2">
				"""),_display_(Seq[Any](/*73.6*/if(!dataset.thumbnail_id.isEmpty)/*73.39*/{_display_(Seq[Any](format.raw/*73.40*/("""
					<a href=""""),_display_(Seq[Any](/*74.16*/(routes.Datasets.dataset(dataset.id)))),format.raw/*74.53*/("""">
						<img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*75.54*/(routes.Files.thumbnail(UUID(dataset.thumbnail_id.toString().substring(5,dataset.thumbnail_id.toString().length-1)))))),format.raw/*75.171*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*75.192*/Html(dataset.name))),format.raw/*75.210*/("""">
					</a>
				""")))})),format.raw/*77.6*/("""
			</div>
			<div class="col-md-10">
				<dl class="dl-horizontal">
					<dt> Name:</dt>
					<dd><a href=""""),_display_(Seq[Any](/*82.20*/(routes.Datasets.dataset(dataset.id)))),format.raw/*82.57*/("""">"""),_display_(Seq[Any](/*82.60*/Html(dataset.name))),format.raw/*82.78*/("""</a></dd>
					<dt>  Description:</dt>
					<dd> """),_display_(Seq[Any](/*84.12*/Html(dataset.description.replace("\n","<br/>")))),format.raw/*84.59*/("""</dd>
					<dt>Collection name(s):</dt>
					<dd>
						"""),_display_(Seq[Any](/*87.8*/for(currMapping <- mapcollectionIds.get(dataset.id.toString).get) yield /*87.73*/{_display_(Seq[Any](format.raw/*87.74*/("""
							<a href=""""),_display_(Seq[Any](/*88.18*/(routes.Collections.collection(UUID(currMapping._1.trim))))),format.raw/*88.76*/("""">"""),_display_(Seq[Any](/*88.79*/currMapping/*88.90*/._2.trim)),format.raw/*88.98*/("""</a><br/>
						""")))})),format.raw/*89.8*/("""
					</dd>
				</dl>
			</div>
		</div>
		<script>
				$("#datasetdiv_"""),_display_(Seq[Any](/*95.21*/dataset/*95.28*/.id.toString)),format.raw/*95.40*/(""" br").last().remove();
		</script>
		""")))})),format.raw/*97.4*/("""
		</div>
	""")))})),format.raw/*99.3*/("""
	
	"""),_display_(Seq[Any](/*101.3*/if(collections.size!=0)/*101.26*/{_display_(Seq[Any](format.raw/*101.27*/("""
			"""),_display_(Seq[Any](/*102.5*/if(files.size != 0 && datasets.size != 0 && collections.size != 0)/*102.71*/ {_display_(Seq[Any](format.raw/*102.73*/("""
		    	<div class="col-md-4">
		    """)))})),format.raw/*104.8*/("""
		    """),_display_(Seq[Any](/*105.8*/if(files.size == 0 || datasets.size == 0 || collections.size == 0)/*105.74*/ {_display_(Seq[Any](format.raw/*105.76*/("""
		    	<div class="col-md-6">
		    """)))})),format.raw/*107.8*/("""
			    
	       <h3 id="search-collection-title"> <span class="glyphicon glyphicon-th-large"></span> Collections</h3>
	    """),_display_(Seq[Any](/*110.7*/for(collection <- collections) yield /*110.37*/ {_display_(Seq[Any](format.raw/*110.39*/("""
		  <div class="row elasticsearchResult">
		    <div class="col-md-2">
				"""),_display_(Seq[Any](/*113.6*/if(!collection.thumbnail_id.isEmpty)/*113.42*/{_display_(Seq[Any](format.raw/*113.43*/("""
					<a href=""""),_display_(Seq[Any](/*114.16*/(routes.Collections.collection(collection.id)))),format.raw/*114.62*/("""">
						<img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*115.54*/(routes.Files.thumbnail(UUID(collection.thumbnail_id.toString().substring(5,collection.thumbnail_id.toString().length-1)))))),format.raw/*115.177*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*115.198*/Html(collection.name))),format.raw/*115.219*/("""">
					</a>
				""")))})),format.raw/*117.6*/("""
			</div>
			<div class="col-md-10">
				<dl class="dl-horizontal">
					<dt> Name:</dt>
					<dd><a href=""""),_display_(Seq[Any](/*122.20*/(routes.Collections.collection(collection.id)))),format.raw/*122.66*/("""">"""),_display_(Seq[Any](/*122.69*/Html(collection.name))),format.raw/*122.90*/("""</a></dd>
					<dt>  Description:</dt>
					<dd> """),_display_(Seq[Any](/*124.12*/Html(collection.description.replace("\n","<br>")))),format.raw/*124.61*/("""</dd>
				</dl>
			</div>
		</div>
		""")))})),format.raw/*128.4*/("""
		</div>
	""")))})),format.raw/*130.3*/("""
	
	
	</div>
	<script>
		$(".col-md-4 > div > div > img").css("width", "100");
		
		$(".dl-horizontal").css("position", "relative");
		
		$(".col-md-4 .dl-horizontal").each(function () """),format.raw/*139.50*/("""{"""),format.raw/*139.51*/("""
	        this.style.top = "-10px"   ;
	    """),format.raw/*141.6*/("""}"""),format.raw/*141.7*/(""");
		$(".col-md-6 .dl-horizontal").each(function () """),format.raw/*142.50*/("""{"""),format.raw/*142.51*/("""
	        this.style.top = "-10px"   ;
	    """),format.raw/*144.6*/("""}"""),format.raw/*144.7*/(""");
		
	</script>
""")))})),format.raw/*147.2*/("""
"""))}
    }
    
    def render(query:String,files:Array[models.File],datasets:Array[models.Dataset],collections:Array[models.Collection],mapdatasetIds:scala.collection.mutable.HashMap[String, scala.collection.mutable.ListBuffer[scala.Tuple2[String, String]]],mapcollectionIds:scala.collection.mutable.HashMap[String, scala.collection.mutable.ListBuffer[scala.Tuple2[String, String]]],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(query,files,datasets,collections,mapdatasetIds,mapcollectionIds)(user)
    
    def f:((String,Array[models.File],Array[models.Dataset],Array[models.Collection],scala.collection.mutable.HashMap[String, scala.collection.mutable.ListBuffer[scala.Tuple2[String, String]]],scala.collection.mutable.HashMap[String, scala.collection.mutable.ListBuffer[scala.Tuple2[String, String]]]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (query,files,datasets,collections,mapdatasetIds,mapcollectionIds) => (user) => apply(query,files,datasets,collections,mapdatasetIds,mapcollectionIds)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/searchResults.scala.html
                    HASH: 49741164a4626df52dba3a766b2c4cefbff6b398
                    MATRIX: 897->1|1408->374|1437->421|1473->423|1503->445|1542->447|1634->504|1660->509|1719->533|1794->599|1834->601|1958->694|2017->718|2043->735|2082->736|2124->743|2199->809|2239->811|2308->849|2351->857|2426->923|2466->925|2535->963|2675->1068|2709->1086|2749->1088|2841->1144|2854->1148|2888->1160|2959->1196|2998->1226|3037->1227|3090->1244|3140->1272|3233->1329|3367->1440|3425->1461|3467->1480|3518->1500|3671->1617|3721->1645|3760->1648|3773->1652|3804->1661|3882->1703|3895->1707|3929->1719|4010->1764|4023->1768|4056->1779|4148->1836|4223->1895|4262->1896|4317->1915|4391->1967|4430->1970|4450->1981|4480->1989|4529->2007|4640->2082|4653->2086|4687->2098|4759->2139|4804->2153|4842->2156|4871->2176|4910->2177|4950->2182|5025->2248|5065->2250|5134->2288|5177->2296|5252->2362|5292->2364|5361->2402|5519->2525|5559->2549|5599->2551|5693->2609|5709->2616|5743->2628|5815->2665|5857->2698|5896->2699|5948->2715|6007->2752|6099->2808|6239->2925|6297->2946|6338->2964|6387->2982|6532->3091|6591->3128|6630->3131|6670->3149|6756->3199|6825->3246|6917->3303|6998->3368|7037->3369|7091->3387|7171->3445|7210->3448|7230->3459|7260->3467|7308->3484|7416->3556|7432->3563|7466->3575|7535->3613|7578->3625|7619->3630|7652->3653|7692->3654|7733->3659|7809->3725|7850->3727|7920->3765|7964->3773|8040->3839|8081->3841|8151->3879|8312->4004|8359->4034|8400->4036|8513->4113|8559->4149|8599->4150|8652->4166|8721->4212|8814->4268|8961->4391|9020->4412|9065->4433|9115->4451|9261->4560|9330->4606|9370->4609|9414->4630|9501->4680|9573->4729|9643->4767|9687->4779|9901->4964|9931->4965|10003->5009|10032->5010|10113->5062|10143->5063|10215->5107|10244->5108|10294->5126
                    LINES: 20->1|24->1|27->5|28->6|28->6|28->6|30->8|30->8|32->10|32->10|32->10|38->16|40->18|40->18|40->18|41->19|41->19|41->19|43->21|44->22|44->22|44->22|46->24|49->27|49->27|49->27|50->28|50->28|50->28|52->30|52->30|52->30|53->31|53->31|54->32|54->32|54->32|54->32|56->34|61->39|61->39|61->39|61->39|61->39|63->41|63->41|63->41|65->43|65->43|65->43|68->46|68->46|68->46|69->47|69->47|69->47|69->47|69->47|70->48|77->55|77->55|77->55|80->58|82->60|83->61|83->61|83->61|84->62|84->62|84->62|86->64|87->65|87->65|87->65|89->67|92->70|92->70|92->70|93->71|93->71|93->71|95->73|95->73|95->73|96->74|96->74|97->75|97->75|97->75|97->75|99->77|104->82|104->82|104->82|104->82|106->84|106->84|109->87|109->87|109->87|110->88|110->88|110->88|110->88|110->88|111->89|117->95|117->95|117->95|119->97|121->99|123->101|123->101|123->101|124->102|124->102|124->102|126->104|127->105|127->105|127->105|129->107|132->110|132->110|132->110|135->113|135->113|135->113|136->114|136->114|137->115|137->115|137->115|137->115|139->117|144->122|144->122|144->122|144->122|146->124|146->124|150->128|152->130|161->139|161->139|163->141|163->141|164->142|164->142|166->144|166->144|169->147
                    -- GENERATED --
                */
            