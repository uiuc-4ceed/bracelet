
package views.html.sensors

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object relatedSensors extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[List[scala.Tuple3[String, String, String]],String,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(sensors: List[(String, String, String)],
        resource_type: String, // What resource is calling this form? "Collection", "Dataset", or "File"
        item_id: String // What is the UUID of the resource (for the Collection, Dataset, or File)
        )(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.Play.current


Seq[Any](format.raw/*4.46*/("""

<div class="row ds-section-sm">
    <h4>Sensors</h4>
    <ul class="list-unstyled" id="sensors-list">
        """),_display_(Seq[Any](/*9.10*/for(s <- sensors) yield /*9.27*/ {_display_(Seq[Any](format.raw/*9.29*/("""
            <li><a href=""""),_display_(Seq[Any](/*10.27*/s/*10.28*/._3)),format.raw/*10.31*/("""">"""),_display_(Seq[Any](/*10.34*/s/*10.35*/._2)),format.raw/*10.38*/("""</a>
            """),_display_(Seq[Any](/*11.14*/if(user.isDefined)/*11.32*/ {_display_(Seq[Any](format.raw/*11.34*/("""
                | <a href="#" onclick="javascript:removeRelation('"""),_display_(Seq[Any](/*12.68*/s/*12.69*/._1)),format.raw/*12.72*/("""')"><span class="glyphicon glyphicon-remove"></span> Remove</a></li>
            """)))})),format.raw/*13.14*/("""

        """)))})),format.raw/*15.10*/("""
    </ul>
    """),format.raw/*18.1*/("""    """),_display_(Seq[Any](/*18.6*/defining(play.api.Play.configuration.getString("geostream.dashboard.url").getOrElse("http://localhost:9000") + "#detail/location/")/*18.137*/ { base_url =>_display_(Seq[Any](format.raw/*18.151*/("""
        <button class="btn btn-default btn-xs" onclick="javascript:associateWithSensor('"""),_display_(Seq[Any](/*19.90*/resource_type)),format.raw/*19.103*/("""', '"""),_display_(Seq[Any](/*19.108*/item_id)),format.raw/*19.115*/("""', '"""),_display_(Seq[Any](/*19.120*/base_url)),format.raw/*19.128*/("""')"><span class="glyphicon glyphicon-plus"></span> Add</button>
    """)))})),format.raw/*20.6*/("""
</div>
<script src=""""),_display_(Seq[Any](/*22.15*/routes/*22.21*/.Assets.at("javascripts/handlebars-v1.3.0.js"))),format.raw/*22.67*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*23.15*/routes/*23.21*/.Assets.at("javascripts/handlebars-loader.js"))),format.raw/*23.67*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*24.15*/routes/*24.21*/.Assets.at("javascripts/sensors/assign.js"))),format.raw/*24.64*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*25.15*/routes/*25.21*/.Assets.at("javascripts/sensors/removeRelation.js"))),format.raw/*25.72*/("""" type="text/javascript"></script>
"""))}
    }
    
    def render(sensors:List[scala.Tuple3[String, String, String]],resource_type:String,item_id:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(sensors,resource_type,item_id)(user)
    
    def f:((List[scala.Tuple3[String, String, String]],String,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (sensors,resource_type,item_id) => (user) => apply(sensors,resource_type,item_id)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/sensors/relatedSensors.scala.html
                    HASH: 81391e9fc6077e41e51c545cfef25b26f51d3157
                    MATRIX: 674->1|1088->292|1236->405|1268->422|1307->424|1370->451|1380->452|1405->455|1444->458|1454->459|1479->462|1533->480|1560->498|1600->500|1704->568|1714->569|1739->572|1853->654|1896->665|1938->710|1978->715|2119->846|2172->860|2298->950|2334->963|2376->968|2406->975|2448->980|2479->988|2579->1057|2637->1079|2652->1085|2720->1131|2805->1180|2820->1186|2888->1232|2973->1281|2988->1287|3053->1330|3138->1379|3153->1385|3226->1436
                    LINES: 20->1|27->4|32->9|32->9|32->9|33->10|33->10|33->10|33->10|33->10|33->10|34->11|34->11|34->11|35->12|35->12|35->12|36->13|38->15|40->18|40->18|40->18|40->18|41->19|41->19|41->19|41->19|41->19|41->19|42->20|44->22|44->22|44->22|45->23|45->23|45->23|46->24|46->24|46->24|47->25|47->25|47->25
                    -- GENERATED --
                */
            