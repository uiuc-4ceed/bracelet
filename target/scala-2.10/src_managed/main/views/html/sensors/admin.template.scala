
package views.html.sensors

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object admin extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[String,String,String,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(sensors: String, sensor: String, parameters: String, parameter: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services.AppConfiguration

def /*4.2*/title/*4.7*/ = {{
    AppConfiguration.getSensorsTitle + " Administration"
}};
Seq[Any](format.raw/*1.110*/("""

"""),format.raw/*6.2*/("""

"""),_display_(Seq[Any](/*8.2*/main(title)/*8.13*/ {_display_(Seq[Any](format.raw/*8.15*/("""
    <div class="page-header">
        <h1>"""),_display_(Seq[Any](/*10.14*/(AppConfiguration.getSensorsTitle))),format.raw/*10.48*/(""" Administration</h1>
    </div>

    <div class="row">
        <div class="col-md-4">
            <form class="form-horizontal" role="form" id="sensors-admin">
                <div class="form-group">
                    """),_display_(Seq[Any](/*17.22*/play/*17.26*/.api.Play.current.plugin[services.PostgresPlugin]/*17.75*/ match/*17.81*/ {/*18.25*/case Some(db) if db.isEnabled =>/*18.57*/ {_display_(Seq[Any](format.raw/*18.59*/("""
                            <label for="sensorsTitle">Sensors title (plural form)</label>
                            <input class="form-control" id="sensorsTitle" name="sensorsTitle" type="text" value=""""),_display_(Seq[Any](/*20.115*/(sensors))),format.raw/*20.124*/("""" required />
                            <span id="sensorsTitle_error" style="display:none;">Sensors must have a value.</span>
                            <br />

                            <label for="sensorTitle">Sensor title (singular form)</label>
                            <input class="form-control" id="sensorTitle" name="sensorTitle" type="text" value=""""),_display_(Seq[Any](/*25.113*/(sensor))),format.raw/*25.121*/("""" required />
                            <span id="sensorTitle_error" style="display:none;">Sensor must have a value.</span>

                            <br /><br />

                            <label for="parametersTitle">Parameters title (plural form)</label>
                            <input id="parametersTitle" name="parametersTitle" type="text" class="form-control" value=""""),_display_(Seq[Any](/*31.121*/(parameters))),format.raw/*31.133*/("""" required />
                            <span id="parametersTitle_error" style="display:none;">Parameters must have a title.</span>
                            <br />

                            <label for="parameterTitle">Parameter title (singular form)</label>
                            <input id="parameterTitle" name="parameterTitle" type="text" class="form-control" value=""""),_display_(Seq[Any](/*36.119*/(parameter))),format.raw/*36.130*/("""" required />
                            <span id="parameterTitle_error" style="display:none;">Parameter must have a title.</span>

                            <br /><br />
                        """)))}/*41.25*/case _ =>/*41.34*/ {}})),format.raw/*42.22*/("""

                    <button class="btn btn-primary" id="sensors-admin-submit"><span class="glyphicon glyphicon-saved"></span> Submit</button>
                </div>
            </form>
        </div>
    </div>
    <script src=""""),_display_(Seq[Any](/*49.19*/routes/*49.25*/.Assets.at("javascripts/jquery.validate.js"))),format.raw/*49.69*/(""""></script>

    <script language="javascript">
    $(document).ready(function() """),format.raw/*52.34*/("""{"""),format.raw/*52.35*/("""

        $("#sensors-admin-submit").click(function(event) """),format.raw/*54.58*/("""{"""),format.raw/*54.59*/("""
            event.preventDefault();

            var sensorsTitle = $("#sensorsTitle").val();
            var sensorTitle =  $("#sensorTitle").val();
            var parametersTitle = $("#parametersTitle").val();
            var parameterTitle = $("#parameterTitle").val();

            // setup form validation
            var sensorsForm = $('#sensors-admin');
            sensorsForm.validate("""),format.raw/*64.34*/("""{"""),format.raw/*64.35*/("""
                messages: """),format.raw/*65.27*/("""{"""),format.raw/*65.28*/("""
                    sensorsTitle: "You must provide a title for Sensors",
                    sensorTitle: "You must provide a title for Sensor",
                    parametersTitle: "You must provide a title for Parameters",
                    parameterTitle: "You must provide a title for Parameter"
                """),format.raw/*70.17*/("""}"""),format.raw/*70.18*/("""
            """),format.raw/*71.13*/("""}"""),format.raw/*71.14*/(""");

            if (!sensorsForm.valid()) """),format.raw/*73.39*/("""{"""),format.raw/*73.40*/("""
                return false;
            """),format.raw/*75.13*/("""}"""),format.raw/*75.14*/(""" else """),format.raw/*75.20*/("""{"""),format.raw/*75.21*/("""
                var request = $.ajax("""),format.raw/*76.38*/("""{"""),format.raw/*76.39*/("""
                    url:  """"),_display_(Seq[Any](/*77.29*/api/*77.32*/.routes.Admin.sensorsConfig)),format.raw/*77.59*/("""",
                    data: JSON.stringify("""),format.raw/*78.42*/("""{"""),format.raw/*78.43*/(""" sensors: sensorsTitle, sensor: sensorTitle, parameters: parametersTitle, parameter: parameterTitle """),format.raw/*78.143*/("""}"""),format.raw/*78.144*/("""),
                    type: "POST",
                    contentType: "application/json"
                """),format.raw/*81.17*/("""}"""),format.raw/*81.18*/(""");
                request.done(function (response, textStatus, jqXHR)"""),format.raw/*82.68*/("""{"""),format.raw/*82.69*/("""
                    console.log("Response: " + response);
                    notify("Preferences successfully updated. Please refresh page.", "success", 5000);
                """),format.raw/*85.17*/("""}"""),format.raw/*85.18*/(""");
                request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*86.71*/("""{"""),format.raw/*86.72*/("""
                    console.error("The following error occurred: " + textStatus, errorThrown);
                    notify("The application preferences was not updated due to : " + errorThrown, "error");
                """),format.raw/*89.17*/("""}"""),format.raw/*89.18*/(""");
                return false;
            """),format.raw/*91.13*/("""}"""),format.raw/*91.14*/("""
        """),format.raw/*92.9*/("""}"""),format.raw/*92.10*/(""");
    """),format.raw/*93.5*/("""}"""),format.raw/*93.6*/(""");
    </script>
""")))})),format.raw/*95.2*/("""
"""))}
    }
    
    def render(sensors:String,sensor:String,parameters:String,parameter:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(sensors,sensor,parameters,parameter)(user)
    
    def f:((String,String,String,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (sensors,sensor,parameters,parameter) => (user) => apply(sensors,sensor,parameters,parameter)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/sensors/admin.scala.html
                    HASH: 0e9a454c2c30603575ec98148dcf4f97c83a0b43
                    MATRIX: 636->1|854->146|866->151|961->109|989->215|1026->218|1045->229|1084->231|1164->275|1220->309|1478->531|1491->535|1549->584|1564->590|1575->617|1616->649|1656->651|1898->856|1930->865|2333->1231|2364->1239|2786->1624|2821->1636|3242->2020|3276->2031|3494->2255|3512->2264|3538->2289|3805->2520|3820->2526|3886->2570|3995->2651|4024->2652|4111->2711|4140->2712|4565->3109|4594->3110|4649->3137|4678->3138|5026->3458|5055->3459|5096->3472|5125->3473|5195->3515|5224->3516|5295->3559|5324->3560|5358->3566|5387->3567|5453->3605|5482->3606|5547->3635|5559->3638|5608->3665|5680->3709|5709->3710|5838->3810|5868->3811|6001->3916|6030->3917|6128->3987|6157->3988|6363->4166|6392->4167|6493->4240|6522->4241|6770->4461|6799->4462|6872->4507|6901->4508|6937->4517|6966->4518|7000->4525|7028->4526|7077->4544
                    LINES: 20->1|23->4|23->4|26->1|28->6|30->8|30->8|30->8|32->10|32->10|39->17|39->17|39->17|39->17|39->18|39->18|39->18|41->20|41->20|46->25|46->25|52->31|52->31|57->36|57->36|61->41|61->41|61->42|68->49|68->49|68->49|71->52|71->52|73->54|73->54|83->64|83->64|84->65|84->65|89->70|89->70|90->71|90->71|92->73|92->73|94->75|94->75|94->75|94->75|95->76|95->76|96->77|96->77|96->77|97->78|97->78|97->78|97->78|100->81|100->81|101->82|101->82|104->85|104->85|105->86|105->86|108->89|108->89|110->91|110->91|111->92|111->92|112->93|112->93|114->95
                    -- GENERATED --
                */
            