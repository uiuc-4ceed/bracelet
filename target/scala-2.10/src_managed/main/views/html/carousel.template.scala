
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object carousel extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[models.File],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(files: List[models.File])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.64*/("""
<h2 style="text-align: center">Latest Contributions</h2>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
        <ol class="carousel-indicators">
            """),_display_(Seq[Any](/*6.14*/for((f,i) <- files.zipWithIndex) yield /*6.46*/ {_display_(Seq[Any](format.raw/*6.48*/("""
                """),_display_(Seq[Any](/*7.18*/if(i==0)/*7.26*/ {_display_(Seq[Any](format.raw/*7.28*/("""
                    <li data-target="#carousel-example-generic" data-slide-to=""""),_display_(Seq[Any](/*8.81*/i)),format.raw/*8.82*/("""" class="active"></li>
                """)))}/*9.19*/else/*9.24*/{_display_(Seq[Any](format.raw/*9.25*/("""
                    <li data-target="#carousel-example-generic" data-slide-to=""""),_display_(Seq[Any](/*10.81*/i)),format.raw/*10.82*/(""""></li>
                """)))})),format.raw/*11.18*/("""
            """)))})),format.raw/*12.14*/("""
        </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  	"""),_display_(Seq[Any](/*17.5*/for((f,i) <- files.zipWithIndex) yield /*17.37*/ {_display_(Seq[Any](format.raw/*17.39*/("""
        """),_display_(Seq[Any](/*18.10*/if(i==0)/*18.18*/ {_display_(Seq[Any](format.raw/*18.20*/("""
            <div class="item active">
        """)))}/*20.11*/else/*20.16*/{_display_(Seq[Any](format.raw/*20.17*/("""
            <div class="item">
        """)))})),format.raw/*22.10*/("""

        <a href=""""),_display_(Seq[Any](/*24.19*/(routes.Files.file(f.id)))),format.raw/*24.44*/("""">
            """),_display_(Seq[Any](/*25.14*/if(user.isDefined)/*25.32*/ {_display_(Seq[Any](format.raw/*25.34*/("""
                <img src=""""),_display_(Seq[Any](/*26.28*/routes/*26.34*/.Files.download(f.id))),format.raw/*26.55*/("""" alt=""""),_display_(Seq[Any](/*26.63*/(f.filename))),format.raw/*26.75*/("""" class="center-block" style="max-height:300px;">
            """)))}/*27.15*/else/*27.20*/{_display_(Seq[Any](format.raw/*27.21*/("""
                <img src="http://placehold.it/900x500" alt="placeholder image" class="center-block">
            """)))})),format.raw/*29.14*/("""

        </a>
        <div class="carousel-caption">
            """),_display_(Seq[Any](/*33.14*/f/*33.15*/.filename)),format.raw/*33.24*/("""
        </div>
        </div>
    """)))})),format.raw/*36.6*/("""
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
  </a>
</div>

<script type="text/javascript">
$(document).ready(function () """),format.raw/*51.31*/("""{"""),format.raw/*51.32*/("""
    $('.carousel').carousel("""),format.raw/*52.29*/("""{"""),format.raw/*52.30*/("""
        interval: 10000
    """),format.raw/*54.5*/("""}"""),format.raw/*54.6*/(""");
    $('.carousel').carousel('cycle');
"""),format.raw/*56.1*/("""}"""),format.raw/*56.2*/(""");
</script>  
"""))}
    }
    
    def render(files:List[models.File],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(files)(user)
    
    def f:((List[models.File]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (files) => (user) => apply(files)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:24 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/carousel.scala.html
                    HASH: 03b1107746a6021478476394b888a002c428c1ed
                    MATRIX: 621->1|777->63|1040->291|1087->323|1126->325|1179->343|1195->351|1234->353|1350->434|1372->435|1430->476|1442->481|1480->482|1597->563|1620->564|1677->589|1723->603|1854->699|1902->731|1942->733|1988->743|2005->751|2045->753|2112->802|2125->807|2164->808|2237->849|2293->869|2340->894|2392->910|2419->928|2459->930|2523->958|2538->964|2581->985|2625->993|2659->1005|2741->1069|2754->1074|2793->1075|2940->1190|3043->1257|3053->1258|3084->1267|3151->1303|3742->1866|3771->1867|3828->1896|3857->1897|3913->1926|3941->1927|4009->1968|4037->1969
                    LINES: 20->1|23->1|28->6|28->6|28->6|29->7|29->7|29->7|30->8|30->8|31->9|31->9|31->9|32->10|32->10|33->11|34->12|39->17|39->17|39->17|40->18|40->18|40->18|42->20|42->20|42->20|44->22|46->24|46->24|47->25|47->25|47->25|48->26|48->26|48->26|48->26|48->26|49->27|49->27|49->27|51->29|55->33|55->33|55->33|58->36|73->51|73->51|74->52|74->52|76->54|76->54|78->56|78->56
                    -- GENERATED --
                */
            