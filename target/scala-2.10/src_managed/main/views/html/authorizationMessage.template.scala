
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object authorizationMessage extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[String,models.ProjectSpace,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(msg: String, space: models.ProjectSpace)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.79*/("""

"""),_display_(Seq[Any](/*3.2*/main("Authorization Request")/*3.31*/ {_display_(Seq[Any](format.raw/*3.33*/("""

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12 text-center">
			<h2>
				"""),_display_(Seq[Any](/*8.6*/if(msg != null)/*8.21*/{_display_(Seq[Any](format.raw/*8.22*/("""
					"""),_display_(Seq[Any](/*9.7*/msg)),format.raw/*9.10*/("""
				""")))}/*10.6*/else/*10.10*/{_display_(Seq[Any](format.raw/*10.11*/("""
					You are not authorized to proceed with your request
				""")))})),format.raw/*12.6*/("""
			</h2>
			<a class="btn btn-link" href=""""),_display_(Seq[Any](/*14.35*/routes/*14.41*/.Spaces.getSpace(space.id))),format.raw/*14.67*/("""">"""),_display_(Seq[Any](/*14.70*/space/*14.75*/.name)),format.raw/*14.80*/("""</a>
		</div>
	</div>

""")))})),format.raw/*18.2*/("""
"""))}
    }
    
    def render(msg:String,space:models.ProjectSpace,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(msg,space)(user)
    
    def f:((String,models.ProjectSpace) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (msg,space) => (user) => apply(msg,space)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:24 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/authorizationMessage.scala.html
                    HASH: e3869c62f3e6dda165a5d156ebd0ea4db7806f54
                    MATRIX: 642->1|813->78|850->81|887->110|926->112|1052->204|1075->219|1113->220|1154->227|1178->230|1202->236|1215->240|1254->241|1348->304|1428->348|1443->354|1491->380|1530->383|1544->388|1571->393|1626->417
                    LINES: 20->1|23->1|25->3|25->3|25->3|30->8|30->8|30->8|31->9|31->9|32->10|32->10|32->10|34->12|36->14|36->14|36->14|36->14|36->14|36->14|40->18
                    -- GENERATED --
                */
            