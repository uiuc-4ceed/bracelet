
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object playMode extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply():play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](_display_(Seq[Any](/*1.2*/play/*1.6*/.api.Play.maybeApplication.map(_.mode)/*1.44*/ match/*1.50*/ {/*2.3*/case Some(play.api.Mode.Dev) =>/*2.34*/ {_display_(Seq[Any](format.raw/*2.36*/("""Development""")))}/*3.3*/case Some(play.api.Mode.Prod) =>/*3.35*/ {_display_(Seq[Any](format.raw/*3.37*/("""Production""")))}/*4.3*/case Some(play.api.Mode.Test) =>/*4.35*/ {_display_(Seq[Any](format.raw/*4.37*/("""Test""")))}/*5.3*/case _ =>/*5.12*/ {_display_(Seq[Any](format.raw/*5.14*/("""Unknown""")))}})),format.raw/*6.2*/("""
"""))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:23 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/playMode.scala.html
                    HASH: a0ccce2e33278fa9f9f8ec1e8fd9d56f4ffb0f65
                    MATRIX: 680->1|691->5|737->43|751->49|760->54|799->85|838->87|867->102|907->134|946->136|974->150|1014->182|1053->184|1075->192|1092->201|1131->203|1170->213
                    LINES: 23->1|23->1|23->1|23->1|23->2|23->2|23->2|23->3|23->3|23->3|23->4|23->4|23->4|23->5|23->5|23->5|23->6
                    -- GENERATED --
                */
            