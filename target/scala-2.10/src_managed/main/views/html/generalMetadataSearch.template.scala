
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object generalMetadataSearch extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.40*/("""

"""),_display_(Seq[Any](/*3.2*/main("Dataset metadata search")/*3.33*/ {_display_(Seq[Any](format.raw/*3.35*/("""
<div class="page-header">
  <h1>Dataset metadata search</h1>
</div>
<div class="row-fluid">
	<div class="span12">
		<form class="form-inline">
			<h5>Select properties to search for (in value strings, input * alone to search for any string value):</h5>
			<div id='queryUserMetadata' class='usr_md_'>
				&nbsp;&nbsp;&nbsp;<button class="usr_md_" type="button">Add property</button>
				<ul class="usr_md_ usr_md_search_list"></ul>
				<button class="usr_md_" type="button">Add disjunction</button>
				<br />
				<button class="usr_md_submit btn" type="button">Submit</button></div>
		</form>	
		<table id='resultTable' class="table table-bordered table-hover" style="display:none;">
			<thead>
				<tr>
					"""),_display_(Seq[Any](/*21.7*/if(user.isDefined)/*21.25*/ {_display_(Seq[Any](format.raw/*21.27*/("""
						<th style="width: 25%">Name</th>
						<th style="width: 15%">Created</th>
						<th style="width: 40%">Description</th>
						<th style="width: 10%"></th>
						<th style="width: 10%"></th>
					""")))})),format.raw/*27.7*/("""
					"""),_display_(Seq[Any](/*28.7*/if(!user.isDefined)/*28.26*/ {_display_(Seq[Any](format.raw/*28.28*/("""
						<th style="width: 25%">Name</th>
						<th style="width: 15%">Created</th>
						<th style="width: 50%">Description</th>
						<th style="width: 10%"></th>
					""")))})),format.raw/*33.7*/("""
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<ul class="pager">
			<li class="previous" style="visibility:hidden;"><a class="btn btn-link" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
			<li class ="next" style="visibility:hidden;"><a class="btn btn-link" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
		</ul>
	</div>
</div>
<script language="javascript">
	window["userDefined"] = false;
	window["userId"] = "";
</script>
"""),_display_(Seq[Any](/*49.2*/if(user.isDefined)/*49.20*/ {_display_(Seq[Any](format.raw/*49.22*/("""
	<script language="javascript">
		window["userDefined"] = true;
		window["userId"] = """"),_display_(Seq[Any](/*52.24*/user/*52.28*/.get.identityId.userId)),format.raw/*52.50*/("""";
	</script>
""")))})),format.raw/*54.2*/("""
<script language="javascript">	
	var queryIp = """"),_display_(Seq[Any](/*56.18*/api/*56.21*/.routes.Datasets.searchDatasetsGeneralMetadata)),format.raw/*56.67*/("""";
	var searchOn = "datasets";
	var searchFor = "all";
</script>
<script src=""""),_display_(Seq[Any](/*60.15*/routes/*60.21*/.Assets.at("javascripts/searchUserMetadata.js"))),format.raw/*60.68*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*61.15*/routes/*61.21*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*61.68*/("""" type="text/javascript"></script>
""")))})),format.raw/*62.2*/("""
"""))}
    }
    
    def render(user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply()(user)
    
    def f:(() => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = () => (user) => apply()(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:29 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/generalMetadataSearch.scala.html
                    HASH: 9f40a54d39efeb7e913cb52817658cd9ce61545c
                    MATRIX: 616->1|748->39|785->42|824->73|863->75|1610->787|1637->805|1677->807|1912->1011|1954->1018|1982->1037|2022->1039|2222->1208|2764->1715|2791->1733|2831->1735|2955->1823|2968->1827|3012->1849|3058->1864|3144->1914|3156->1917|3224->1963|3339->2042|3354->2048|3423->2095|3508->2144|3523->2150|3592->2197|3659->2233
                    LINES: 20->1|23->1|25->3|25->3|25->3|43->21|43->21|43->21|49->27|50->28|50->28|50->28|55->33|71->49|71->49|71->49|74->52|74->52|74->52|76->54|78->56|78->56|78->56|82->60|82->60|82->60|83->61|83->61|83->61|84->62
                    -- GENERATED --
                */
            