
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object commentform extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[String,String,String,Option[models.User],RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(id: String, resourceType: String, resourceName: String)(implicit user: Option[models.User], request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.118*/("""
<link href=""""),_display_(Seq[Any](/*2.14*/routes/*2.20*/.Assets.at("javascripts/mention/jquery.mentionsInput.css"))),format.raw/*2.78*/("""" rel='stylesheet' type='text/css'>

<form class="form-inline">
<textarea id="commentField_"""),_display_(Seq[Any](/*5.29*/id)),format.raw/*5.31*/("""" class="comment-input-box mention"></textarea>
<br />
<button class="btn btn-primary" onclick="return postComment('"""),_display_(Seq[Any](/*7.63*/id)),format.raw/*7.65*/("""', '"""),_display_(Seq[Any](/*7.70*/resourceType)),format.raw/*7.82*/("""');" title="Post Comment"><span class="glyphicon glyphicon-comment"></span> Post</button>
</form>
  <script src=""""),_display_(Seq[Any](/*9.17*/routes/*9.23*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*9.68*/("""" language="javascript"></script>
  <script src='//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js' type='text/javascript'></script>
  <script src=""""),_display_(Seq[Any](/*11.17*/routes/*11.23*/.Assets.at("javascripts/mention/jquery.elastic.js"))),format.raw/*11.74*/("""" language="javascript"></script>
  <script src=""""),_display_(Seq[Any](/*12.17*/routes/*12.23*/.Assets.at("javascripts/mention/jquery.mentionsInput.js"))),format.raw/*12.80*/("""" language="javascript"></script>
  <script language="javascript">
       var origText = "";
       $("#commentField_"""),_display_(Seq[Any](/*15.26*/id)),format.raw/*15.28*/("""").mentionsInput(getMentionsInputParams('"""),_display_(Seq[Any](/*15.70*/resourceType)),format.raw/*15.82*/("""', '"""),_display_(Seq[Any](/*15.87*/id)),format.raw/*15.89*/("""'));
  
     function showEditComment(id) """),format.raw/*17.35*/("""{"""),format.raw/*17.36*/("""
       var commentText = $("#comment-body_" + id).html().replace(/<br>/g, "\n");
       origText = commentText;
       commentText = htmlDecode(commentText.replace(/<br>/g, "\n"));
       if ($("#commentField_" + id).length == 0) """),format.raw/*21.50*/("""{"""),format.raw/*21.51*/("""
               var form="<form class=\"form-inline\">";
               form += "<textarea id=\"editField_" + id + "\" class=\"comment-input-box mention\"></textarea>";
               form += "<br />";
               form += "<button class=\"btn btn-primary btn-sm\" title=\"Submit Edit\" onclick=\"return updateComment('" + id + "');\" style=\"margin-right:5px;\"><span class=\"glyphicon glyphicon-saved\"></span> Submit</button>";
               form += "<button class=\"btn btn-default btn-sm\" title=\"Cancel\" onclick=\"return cancelEditComment('" + id + "');\"><span class=\"glyphicon glyphicon-remove\"></span> Cancel</button>";
               form += "</form>";
               $("#comment-body_" + id).html("<div class=\"comment-edit\" id=\"edit-form_" + id + "\">" + form + "</div>");
               $("#editField_" + id).mentionsInput(getMentionsInputParams());
               $("#editField_" + id).val(commentText);
           """),format.raw/*31.12*/("""}"""),format.raw/*31.13*/("""
       else """),format.raw/*32.13*/("""{"""),format.raw/*32.14*/("""
         notify("Error editing comment", "error");
       """),format.raw/*34.8*/("""}"""),format.raw/*34.9*/("""
       return false;
    """),format.raw/*36.5*/("""}"""),format.raw/*36.6*/("""

 function updateComment(id) """),format.raw/*38.29*/("""{"""),format.raw/*38.30*/("""
   var text = $("#editField_" + id).val();
   text = htmlEncode(text);
   editComment(id, text, """"),_display_(Seq[Any](/*41.28*/user/*41.32*/.get.fullName)),format.raw/*41.45*/("""", """"),_display_(Seq[Any](/*41.50*/user/*41.54*/.get.email.getOrElse(""))),format.raw/*41.78*/("""", """"),_display_(Seq[Any](/*41.83*/routes/*41.89*/.Datasets.dataset(UUID(id)).absoluteURL())),format.raw/*41.130*/("""");
   return false;
 """),format.raw/*43.2*/("""}"""),format.raw/*43.3*/("""

 function cancelEditComment(id) """),format.raw/*45.33*/("""{"""),format.raw/*45.34*/("""
       $("#comment-body_" + id).html(origText);
       return false;
   """),format.raw/*48.4*/("""}"""),format.raw/*48.5*/("""

function showReplyComment(id) """),format.raw/*50.31*/("""{"""),format.raw/*50.32*/("""
  if ($("#commentField_" + id).length == 0) """),format.raw/*51.45*/("""{"""),format.raw/*51.46*/("""
    var form="<form class=\"form-inline\">";
    form += "<textarea id=\"commentField_" + id + "\" class=\"comment-input-box mention\"></textarea>";
    form += "<br />";
    form += "<button class=\"btn btn-primary btn-sm\" onclick=\"return replyComment('" + id + "')\" title=\"Confirm\" style=\"margin-right:5px;\">";
    form += "<span class=\"glyphicon glyphicon-ok\"></span> Submit</button>";
    form += "<button class=\"btn btn-default btn-sm\" onclick=\"return cancelComment('" + id + "')\" title=\"Cancel\">";
    form += "<span class=\"glyphicon glyphicon-remove\"></span> Cancel</button>";
    form += "</form>";
    $("#reply_" + id).prepend("<div class=\"comment-thread\" id=\"form_" + id + "\">" + form + "</div>");

    $("#commentField_" + id).mentionsInput(getMentionsInputParams());

  """),format.raw/*64.3*/("""}"""),format.raw/*64.4*/("""
  return false;
"""),format.raw/*66.1*/("""}"""),format.raw/*66.2*/("""

function replyComment(id) """),format.raw/*68.27*/("""{"""),format.raw/*68.28*/("""
  postComment(id, "comment");
  return false;
"""),format.raw/*71.1*/("""}"""),format.raw/*71.2*/("""

function cancelComment(id) """),format.raw/*73.28*/("""{"""),format.raw/*73.29*/("""
  $("#form_" + id).remove();
  return false;
"""),format.raw/*76.1*/("""}"""),format.raw/*76.2*/("""

function postComment(id, resourceType) """),format.raw/*78.40*/("""{"""),format.raw/*78.41*/("""
  var text = $('#commentField_' + id).val();
  var encText = htmlEncode(text);
  $('#commentField_' + id).val("");

  //console.log("Posting " + text);
  if (text !== "") """),format.raw/*84.20*/("""{"""),format.raw/*84.21*/("""
    var request;
    if (resourceType == "comment") """),format.raw/*86.36*/("""{"""),format.raw/*86.37*/("""
        request = jsRoutes.api.Comments.comment(id).ajax("""),format.raw/*87.58*/("""{"""),format.raw/*87.59*/("""
            data : JSON.stringify("""),format.raw/*88.35*/("""{"""),format.raw/*88.36*/("""text: encText"""),format.raw/*88.49*/("""}"""),format.raw/*88.50*/("""),
            type : 'POST',
            contentType : "application/json"
        """),format.raw/*91.9*/("""}"""),format.raw/*91.10*/(""");
    """),format.raw/*92.5*/("""}"""),format.raw/*92.6*/(""" else if (resourceType == "dataset") """),format.raw/*92.43*/("""{"""),format.raw/*92.44*/("""
        request = jsRoutes.api.Datasets.comment(id).ajax("""),format.raw/*93.58*/("""{"""),format.raw/*93.59*/("""
            data : JSON.stringify("""),format.raw/*94.35*/("""{"""),format.raw/*94.36*/("""text: encText"""),format.raw/*94.49*/("""}"""),format.raw/*94.50*/("""),
            type : 'POST',
            contentType : "application/json"
        """),format.raw/*97.9*/("""}"""),format.raw/*97.10*/(""");
    """),format.raw/*98.5*/("""}"""),format.raw/*98.6*/(""" else if (resourceType == "section") """),format.raw/*98.43*/("""{"""),format.raw/*98.44*/("""
        request = jsRoutes.api.Section.comment(id).ajax("""),format.raw/*99.57*/("""{"""),format.raw/*99.58*/("""
            data : JSON.stringify("""),format.raw/*100.35*/("""{"""),format.raw/*100.36*/("""text: encText"""),format.raw/*100.49*/("""}"""),format.raw/*100.50*/("""),
            type : 'POST',
            contentType : "application/json"
        """),format.raw/*103.9*/("""}"""),format.raw/*103.10*/(""");
    """),format.raw/*104.5*/("""}"""),format.raw/*104.6*/(""" else if (resourceType == "file") """),format.raw/*104.40*/("""{"""),format.raw/*104.41*/("""
        request = jsRoutes.api.Files.comment(id).ajax("""),format.raw/*105.55*/("""{"""),format.raw/*105.56*/("""
            data : JSON.stringify("""),format.raw/*106.35*/("""{"""),format.raw/*106.36*/("""text: encText"""),format.raw/*106.49*/("""}"""),format.raw/*106.50*/("""),
            type : 'POST',
            contentType : "application/json"
        """),format.raw/*109.9*/("""}"""),format.raw/*109.10*/(""");
    """),format.raw/*110.5*/("""}"""),format.raw/*110.6*/(""" else """),format.raw/*110.12*/("""{"""),format.raw/*110.13*/("""
        notify("Error with comment type", "error");
        return;
    """),format.raw/*113.5*/("""}"""),format.raw/*113.6*/("""

    request.done ( function ( response, textStatus, jqXHR ) """),format.raw/*115.61*/("""{"""),format.raw/*115.62*/("""
        $('#commentField_' + id).mentionsInput('getMentions', function(data) """),format.raw/*116.78*/("""{"""),format.raw/*116.79*/("""
            // Send email to any users tagged in this comment, and subscribe them to this resource
            data.forEach(function(mentioned)"""),format.raw/*118.45*/("""{"""),format.raw/*118.46*/("""
                var emailtext = "";
                var commenterId = """),_display_(Seq[Any](/*120.36*/user/*120.40*/ match/*120.46*/ {/*121.21*/case Some(u) =>/*121.36*/ {_display_(Seq[Any](format.raw/*121.38*/("""
                        """"),_display_(Seq[Any](/*122.27*/u/*122.28*/.id)),format.raw/*122.31*/(""""
                    """)))}/*123.23*/case None =>/*123.35*/ {_display_(Seq[Any](format.raw/*123.37*/("""
                        ""
                    """)))}})),format.raw/*126.18*/("""
                """),_display_(Seq[Any](/*127.18*/resourceType/*127.30*/ match/*127.36*/ {/*128.21*/case "dataset" =>/*128.38*/ {_display_(Seq[Any](format.raw/*128.40*/("""
                        emailtext += '"""),_display_(Seq[Any](/*129.40*/user/*129.44*/.get.fullName)),format.raw/*129.57*/(""" mentioned you in a comment: """),_display_(Seq[Any](/*129.87*/routes/*129.93*/.Datasets.dataset(UUID(id)).absoluteURL())),format.raw/*129.134*/("""\n\n';
                        jsRoutes.api.Comments.mentionInComment(mentioned.id, id, """"),_display_(Seq[Any](/*130.84*/resourceName)),format.raw/*130.96*/("""", "dataset", commenterId).ajax("""),format.raw/*130.128*/("""{"""),format.raw/*130.129*/("""type: 'POST'"""),format.raw/*130.141*/("""}"""),format.raw/*130.142*/(""");
                    """)))}/*132.21*/case "file" =>/*132.35*/ {_display_(Seq[Any](format.raw/*132.37*/("""
                        emailtext += '"""),_display_(Seq[Any](/*133.40*/user/*133.44*/.get.fullName)),format.raw/*133.57*/(""" mentioned you in a comment: """),_display_(Seq[Any](/*133.87*/routes/*133.93*/.Files.file(UUID(id)).absoluteURL())),format.raw/*133.128*/("""\n\n';
                        jsRoutes.api.Comments.mentionInComment(mentioned.id, id, """"),_display_(Seq[Any](/*134.84*/resourceName)),format.raw/*134.96*/("""", "file", commenterId).ajax("""),format.raw/*134.125*/("""{"""),format.raw/*134.126*/("""type: 'POST'"""),format.raw/*134.138*/("""}"""),format.raw/*134.139*/(""");
                    """)))}/*136.21*/case _ =>/*136.30*/ {_display_(Seq[Any](format.raw/*136.32*/("""
                        emailtext += '"""),_display_(Seq[Any](/*137.40*/user/*137.44*/.get.fullName)),format.raw/*137.57*/(""" mentioned you in a comment\n\n';
                    """)))}})),format.raw/*140.18*/("""

                emailtext += text
                request = jsRoutes.controllers.Users.sendEmail("New Clowder comment", """"),_display_(Seq[Any](/*143.89*/user/*143.93*/.get.email.getOrElse(""))),format.raw/*143.117*/("""", mentioned.email, emailtext).ajax("""),format.raw/*143.153*/("""{"""),format.raw/*143.154*/("""
                    type: 'POST'
                """),format.raw/*145.17*/("""}"""),format.raw/*145.18*/(""").done(function(response)"""),format.raw/*145.43*/("""{"""),format.raw/*145.44*/("""
                    console.log(response);
                """),format.raw/*147.17*/("""}"""),format.raw/*147.18*/(""")
            """),format.raw/*148.13*/("""}"""),format.raw/*148.14*/(""")
        """),format.raw/*149.9*/("""}"""),format.raw/*149.10*/(""");

      var dateNow = new Date();
      var hour = ('0'+dateNow.getHours()).slice(-2);
      var minute = ('0'+dateNow.getMinutes()).slice(-2)
      var second = ('0'+dateNow.getSeconds()).slice(-2)

      var formatted = $.datepicker.formatDate('MM dd, yy', dateNow) + " " + hour+":"+minute+":"+second;
      var post = "<div class=\"comment\" id=\"comment_" + response + "\">";
      post += "<div class=\"media\">";
      post += "<a class='pull-left' href='"""),_display_(Seq[Any](/*159.44*/routes/*159.50*/.Profile.viewProfileUUID(user.get.id))),format.raw/*159.87*/("""'>";
      post += "<div class='thumbnail'>";
      post += "<img class='avatar' src='"""),_display_(Seq[Any](/*161.42*/(user.get.getAvatarUrl()))),format.raw/*161.67*/("""'>";
      post += "</div></a>";
      post += "<div class='media-body'>";
      post += "<div class=\"comment-header\">";
      post += "<a href=\""""),_display_(Seq[Any](/*165.27*/routes/*165.33*/.Profile.viewProfileUUID(user.get.id))),format.raw/*165.70*/("""\">"""),_display_(Seq[Any](/*165.74*/user/*165.78*/.get.fullName)),format.raw/*165.91*/("""</a>";
      post += "<span> • </span>";
      post += "<span>" + formatted + "</span>";
      post += "</div>";
      post += "<div class=\"comment-body\" id=\"comment-body_" + response + "\">" + encText.replace(/\n/g, "<br>") + "</div>";
      //Note, no check for comment owner in this case because this case is only displayed immediately after the comment creator posts the comment. MMF - 09/2014
      post += "<div class=\"comment-footer\"><a href=\"#\" class=\"btn btn-link btn-xs\" title=\"Reply To Comment\" onclick=\"return showReplyComment('" + response + "');\"><span class=\"glyphicon glyphicon-share-alt\"></span> Reply</a>&nbsp;&nbsp;<a href=\"#\" class=\"btn btn-link btn-xs\" title=\"Edit Comment\" onclick=\"return showEditComment('" + response + "');\"><span class=\"glyphicon glyphicon-edit\"></span> Edit</a>&nbsp;&nbsp;<a href=\"#\" class=\"btn btn-link btn-xs\" title=\"Delete Comment\" onclick=\"return deleteComment('" + response + "');\"><span class=\"glyphicon glyphicon-trash\"></span> Delete</a> </div>";
      post += "<div class=\"comment-reply\">";
      post += "<div class=\"comment-threads unstyled\" id=\"reply_" + response + "\">";
      post += "</div>";
      post += "</div>";
      post += "</div>";
      post += "</div>";
      post += "</div>";
      $("#reply_" + id).prepend("<div class=\"comment-thread\">" + post + "</div>");

      $("#form_" + id).remove();
    """),format.raw/*182.5*/("""}"""),format.raw/*182.6*/(""");

    request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*184.59*/("""{"""),format.raw/*184.60*/("""
      console.error("The following error occured: " + textStatus, errorThrown);
      var errMsg = "You must be logged in to post a comment.";
      if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*187.50*/("""{"""),format.raw/*187.51*/("""
        notify("Error posting comment : " + errorThrown, "error");
      """),format.raw/*189.7*/("""}"""),format.raw/*189.8*/("""
    """),format.raw/*190.5*/("""}"""),format.raw/*190.6*/(""");
  """),format.raw/*191.3*/("""}"""),format.raw/*191.4*/("""
  return false;
"""),format.raw/*193.1*/("""}"""),format.raw/*193.2*/("""

       function getMentionsInputParams(resourceType, resourceId) """),format.raw/*195.66*/("""{"""),format.raw/*195.67*/("""
           if (resourceType == "dataset") """),format.raw/*196.43*/("""{"""),format.raw/*196.44*/("""
               var geturl = jsRoutes.api.Datasets.users(resourceId).url
           """),format.raw/*198.12*/("""}"""),format.raw/*198.13*/(""" else if (resourceType == "file") """),format.raw/*198.47*/("""{"""),format.raw/*198.48*/("""
               var geturl = jsRoutes.api.Files.users(resourceId).url
           """),format.raw/*200.12*/("""}"""),format.raw/*200.13*/(""" else """),format.raw/*200.19*/("""{"""),format.raw/*200.20*/("""
               var geturl = jsRoutes.api.Users.list().url
           """),format.raw/*202.12*/("""}"""),format.raw/*202.13*/("""

           return """),format.raw/*204.19*/("""{"""),format.raw/*204.20*/("""
               onDataRequest:function (mode, query, callback) """),format.raw/*205.63*/("""{"""),format.raw/*205.64*/("""
                   $.ajax("""),format.raw/*206.27*/("""{"""),format.raw/*206.28*/("""
                       type : 'GET',
                       url: geturl,
                       success : function(apidata) """),format.raw/*209.52*/("""{"""),format.raw/*209.53*/("""
                           var data= [];
                           console.log(apidata)
                           apidata.forEach(function(userRecord)"""),format.raw/*212.64*/("""{"""),format.raw/*212.65*/("""
                               data.push("""),format.raw/*213.42*/("""{"""),format.raw/*213.43*/("""
                                   id: userRecord.id,
                                   name: userRecord.identityProvider,
                                   value: userRecord.fullName,
                                   avatar: userRecord.avatar,
                                   email: userRecord.email,
                                   type: 'contact'
                               """),format.raw/*220.32*/("""}"""),format.raw/*220.33*/(""")
                           """),format.raw/*221.28*/("""}"""),format.raw/*221.29*/(""");
                           data = _.filter(data, function(item) """),format.raw/*222.65*/("""{"""),format.raw/*222.66*/(""" return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 """),format.raw/*222.132*/("""}"""),format.raw/*222.133*/(""");
                           callback.call(this, data);
                       """),format.raw/*224.24*/("""}"""),format.raw/*224.25*/("""
                   """),format.raw/*225.20*/("""}"""),format.raw/*225.21*/(""");
               """),format.raw/*226.16*/("""}"""),format.raw/*226.17*/(""",
               "elastic": false,
               "allowRepeat": true
           """),format.raw/*229.12*/("""}"""),format.raw/*229.13*/("""
       """),format.raw/*230.8*/("""}"""),format.raw/*230.9*/("""
</script>

"""))}
    }
    
    def render(id:String,resourceType:String,resourceName:String,user:Option[models.User],request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(id,resourceType,resourceName)(user,request)
    
    def f:((String,String,String) => (Option[models.User],RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (id,resourceType,resourceName) => (user,request) => apply(id,resourceType,resourceName)(user,request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/commentform.scala.html
                    HASH: 80811c794e17cb63f469674fc0cba9593d9092f0
                    MATRIX: 641->1|852->117|901->131|915->137|994->195|1121->287|1144->289|1296->406|1319->408|1359->413|1392->425|1541->539|1555->545|1621->590|1827->760|1842->766|1915->817|2001->867|2016->873|2095->930|2249->1048|2273->1050|2351->1092|2385->1104|2426->1109|2450->1111|2520->1153|2549->1154|2808->1385|2837->1386|3803->2324|3832->2325|3873->2338|3902->2339|3988->2398|4016->2399|4069->2425|4097->2426|4155->2456|4184->2457|4319->2556|4332->2560|4367->2573|4408->2578|4421->2582|4467->2606|4508->2611|4523->2617|4587->2658|4636->2680|4664->2681|4726->2715|4755->2716|4855->2789|4883->2790|4943->2822|4972->2823|5045->2868|5074->2869|5906->3674|5934->3675|5978->3692|6006->3693|6062->3721|6091->3722|6165->3769|6193->3770|6250->3799|6279->3800|6352->3846|6380->3847|6449->3888|6478->3889|6678->4061|6707->4062|6788->4115|6817->4116|6903->4174|6932->4175|6995->4210|7024->4211|7065->4224|7094->4225|7204->4308|7233->4309|7267->4316|7295->4317|7360->4354|7389->4355|7475->4413|7504->4414|7567->4449|7596->4450|7637->4463|7666->4464|7776->4547|7805->4548|7839->4555|7867->4556|7932->4593|7961->4594|8046->4651|8075->4652|8139->4687|8169->4688|8211->4701|8241->4702|8352->4785|8382->4786|8417->4793|8446->4794|8509->4828|8539->4829|8623->4884|8653->4885|8717->4920|8747->4921|8789->4934|8819->4935|8930->5018|8960->5019|8995->5026|9024->5027|9059->5033|9089->5034|9190->5107|9219->5108|9310->5170|9340->5171|9447->5249|9477->5250|9650->5394|9680->5395|9789->5467|9803->5471|9819->5477|9831->5500|9856->5515|9897->5517|9961->5544|9972->5545|9998->5548|10041->5572|10063->5584|10104->5586|10187->5653|10242->5671|10264->5683|10280->5689|10292->5712|10319->5729|10360->5731|10437->5771|10451->5775|10487->5788|10554->5818|10570->5824|10635->5865|10762->5955|10797->5967|10859->5999|10890->6000|10932->6012|10963->6013|11007->6058|11031->6072|11072->6074|11149->6114|11163->6118|11199->6131|11266->6161|11282->6167|11341->6202|11468->6292|11503->6304|11562->6333|11593->6334|11635->6346|11666->6347|11710->6392|11729->6401|11770->6403|11847->6443|11861->6447|11897->6460|11986->6534|12147->6658|12161->6662|12209->6686|12275->6722|12306->6723|12385->6773|12415->6774|12469->6799|12499->6800|12588->6860|12618->6861|12661->6875|12691->6876|12729->6886|12759->6887|13260->7351|13276->7357|13336->7394|13460->7481|13508->7506|13694->7655|13710->7661|13770->7698|13811->7702|13825->7706|13861->7719|15301->9131|15330->9132|15421->9194|15451->9195|15673->9388|15703->9389|15805->9463|15834->9464|15867->9469|15896->9470|15929->9475|15958->9476|16003->9493|16032->9494|16128->9561|16158->9562|16230->9605|16260->9606|16373->9690|16403->9691|16466->9725|16496->9726|16606->9807|16636->9808|16671->9814|16701->9815|16800->9885|16830->9886|16879->9906|16909->9907|17001->9970|17031->9971|17087->9998|17117->9999|17271->10124|17301->10125|17483->10278|17513->10279|17584->10321|17614->10322|18035->10714|18065->10715|18123->10744|18153->10745|18249->10812|18279->10813|18375->10879|18406->10880|18515->10960|18545->10961|18594->10981|18624->10982|18671->11000|18701->11001|18811->11082|18841->11083|18877->11091|18906->11092
                    LINES: 20->1|23->1|24->2|24->2|24->2|27->5|27->5|29->7|29->7|29->7|29->7|31->9|31->9|31->9|33->11|33->11|33->11|34->12|34->12|34->12|37->15|37->15|37->15|37->15|37->15|37->15|39->17|39->17|43->21|43->21|53->31|53->31|54->32|54->32|56->34|56->34|58->36|58->36|60->38|60->38|63->41|63->41|63->41|63->41|63->41|63->41|63->41|63->41|63->41|65->43|65->43|67->45|67->45|70->48|70->48|72->50|72->50|73->51|73->51|86->64|86->64|88->66|88->66|90->68|90->68|93->71|93->71|95->73|95->73|98->76|98->76|100->78|100->78|106->84|106->84|108->86|108->86|109->87|109->87|110->88|110->88|110->88|110->88|113->91|113->91|114->92|114->92|114->92|114->92|115->93|115->93|116->94|116->94|116->94|116->94|119->97|119->97|120->98|120->98|120->98|120->98|121->99|121->99|122->100|122->100|122->100|122->100|125->103|125->103|126->104|126->104|126->104|126->104|127->105|127->105|128->106|128->106|128->106|128->106|131->109|131->109|132->110|132->110|132->110|132->110|135->113|135->113|137->115|137->115|138->116|138->116|140->118|140->118|142->120|142->120|142->120|142->121|142->121|142->121|143->122|143->122|143->122|144->123|144->123|144->123|146->126|147->127|147->127|147->127|147->128|147->128|147->128|148->129|148->129|148->129|148->129|148->129|148->129|149->130|149->130|149->130|149->130|149->130|149->130|150->132|150->132|150->132|151->133|151->133|151->133|151->133|151->133|151->133|152->134|152->134|152->134|152->134|152->134|152->134|153->136|153->136|153->136|154->137|154->137|154->137|155->140|158->143|158->143|158->143|158->143|158->143|160->145|160->145|160->145|160->145|162->147|162->147|163->148|163->148|164->149|164->149|174->159|174->159|174->159|176->161|176->161|180->165|180->165|180->165|180->165|180->165|180->165|197->182|197->182|199->184|199->184|202->187|202->187|204->189|204->189|205->190|205->190|206->191|206->191|208->193|208->193|210->195|210->195|211->196|211->196|213->198|213->198|213->198|213->198|215->200|215->200|215->200|215->200|217->202|217->202|219->204|219->204|220->205|220->205|221->206|221->206|224->209|224->209|227->212|227->212|228->213|228->213|235->220|235->220|236->221|236->221|237->222|237->222|237->222|237->222|239->224|239->224|240->225|240->225|241->226|241->226|244->229|244->229|245->230|245->230
                    -- GENERATED --
                */
            