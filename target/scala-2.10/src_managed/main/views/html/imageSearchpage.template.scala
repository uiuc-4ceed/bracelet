
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object imageSearchpage extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[scala.collection.mutable.ArrayBuffer[String],String,String,Integer,scala.collection.mutable.HashMap[String, scala.collection.mutable.ArrayBuffer[scala.Tuple4[String, String, Double, String]]],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(keysArray:scala.collection.mutable.ArrayBuffer[String],name: String, id: String, size:Integer, results: scala.collection.mutable.HashMap[String, scala.collection.mutable.ArrayBuffer[(String,String,Double,String)]]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import models.IncrementCounter


Seq[Any](format.raw/*1.217*/(""" 
"""),format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*4.2*/main("Search Results")/*4.24*/ {_display_(Seq[Any](format.raw/*4.26*/("""
	<div class="page-header">
		<h1>Search Results For <medium>"""),_display_(Seq[Any](/*6.35*/name)),format.raw/*6.39*/("""</medium></h1>
	</div>
	"""),_display_(Seq[Any](/*8.3*/if(size == 0)/*8.16*/ {_display_(Seq[Any](format.raw/*8.18*/("""
	<div class="row">
		<div class="col-md-12">
			No results found. Sorry!
		</div>
	</div>
	""")))})),format.raw/*14.3*/("""
	
<div class="container" > 
	<div class="row" >
	<div>
	
	<table id='tablec' border="1">
	"""),_display_(Seq[Any](/*21.3*/defining(new IncrementCounter)/*21.33*/{c=>_display_(Seq[Any](format.raw/*21.37*/("""
	  """),_display_(Seq[Any](/*22.5*/for(index<-keysArray) yield /*22.26*/{_display_(Seq[Any](format.raw/*22.27*/("""
		<thead>
	  	"""),_display_(Seq[Any](/*24.6*/{c.count+=1})),format.raw/*24.18*/("""
	  	"""),_display_(Seq[Any](/*25.6*/if(c.count==1)/*25.20*/{_display_(Seq[Any](format.raw/*25.21*/("""
	  		<script>
	  		console.log("if : c.count=","""),_display_(Seq[Any](/*27.35*/c/*27.36*/.count)),format.raw/*27.42*/(""");
	  			
	  		</script>
			<tr>
	    	<th colspan="3">IndexID: """),_display_(Seq[Any](/*31.33*/index)),format.raw/*31.38*/("""</th>
			</tr>
		""")))}/*33.4*/else/*33.8*/{_display_(Seq[Any](format.raw/*33.9*/("""
			<script>
			console.log("if:c.count=","""),_display_(Seq[Any](/*35.31*/c/*35.32*/.count)),format.raw/*35.38*/(""");
			var row0=document.getElementById("tablec").rows[0];
			var x=row0.insertCell(-1);
				x.innerHTML="<h5>IndexID:"""),_display_(Seq[Any](/*38.31*/index)),format.raw/*38.36*/("""<h5>";
				row0.cells["""),_display_(Seq[Any](/*39.17*/{c.count-1})),format.raw/*39.28*/("""].colSpan="2";
			</script>	
			""")))})),format.raw/*41.5*/("""
		<tr>
		"""),_display_(Seq[Any](/*43.4*/if(c.count==1)/*43.18*/{_display_(Seq[Any](format.raw/*43.19*/("""
			<th>Rank</th>
			<th>ID</th>
			<th>Proximity</th>
		""")))}/*47.4*/else/*47.8*/{_display_(Seq[Any](format.raw/*47.9*/("""
		   <script>
			var row1=document.getElementById("tablec").rows[1];
			//console.log("row1.innerHTML=",row1.innerHTML);
			var x=row1.insertCell(-1);
			x.align="center";
			x.innerHTML="<h5>ID </h5>";
			 var row11=document.getElementById("tablec").rows[1];
			 var x1=row11.insertCell(-1);
			 x1.align="center";
			x1.innerHTML="<h5>Proximity</h5>";
			</script>	
			""")))})),format.raw/*59.5*/("""
		
		</tr>
	</thead>
	
		"""),_display_(Seq[Any](/*64.4*/results/*64.11*/.get(index).map/*64.26*/ {list =>_display_(Seq[Any](format.raw/*64.35*/("""
				"""),_display_(Seq[Any](/*65.6*/defining(new IncrementCounter)/*65.36*/{ i=>_display_(Seq[Any](format.raw/*65.41*/("""   
					<script>
					 console.log("i.count=","""),_display_(Seq[Any](/*67.31*/i/*67.32*/.count)),format.raw/*67.38*/("""," list.length=","""),_display_(Seq[Any](/*67.56*/list/*67.60*/.length)),format.raw/*67.67*/(""");
					
					</script>
				
				 """),_display_(Seq[Any](/*71.7*/for(ele<-list) yield /*71.21*/{_display_(Seq[Any](format.raw/*71.22*/("""	
					
		 	    <!-- list.map  case (id,link, distance, fname) -->
		 		  
		 		  
							"""),_display_(Seq[Any](/*76.9*/if(c.count==1)/*76.23*/{_display_(Seq[Any](format.raw/*76.24*/("""	
								"""),_display_(Seq[Any](/*77.10*/if(i.count<5)/*77.23*/{_display_(Seq[Any](format.raw/*77.24*/("""
								<script>
								console.log("i="""),_display_(Seq[Any](/*79.25*/i/*79.26*/.count)),format.raw/*79.32*/(""" indexID:"""),_display_(Seq[Any](/*79.42*/index)),format.raw/*79.47*/(""" fileId="""),_display_(Seq[Any](/*79.56*/ele/*79.59*/._4)),format.raw/*79.62*/(""" distance="""),_display_(Seq[Any](/*79.73*/ele/*79.76*/._3)),format.raw/*79.79*/("""");
								    var row2=document.getElementById("tablec").insertRow(-1);
								 // var row2=document.getElementById("tablec").rows["""),_display_(Seq[Any](/*81.62*/{i.count+1})),format.raw/*81.73*/("""];
								  var z=row2.insertCell(0);
								  var t=row2.insertCell(1);
								  var u=row2.insertCell(2);
								  z.innerHTML=""""),_display_(Seq[Any](/*85.25*/i/*85.26*/.count)),format.raw/*85.32*/("""";
								  t.innerHTML="<a href="""),_display_(Seq[Any](/*86.33*/routes/*86.39*/.Files.file(UUID(ele._1)))),format.raw/*86.64*/(""">"""),_display_(Seq[Any](/*86.66*/ele/*86.69*/._4)),format.raw/*86.72*/("""</a>";
								  u.innerHTML=""""),_display_(Seq[Any](/*87.25*/ele/*87.28*/._3)),format.raw/*87.31*/("""";
								 // console.log("if:c=1,i.count=","""),_display_(Seq[Any](/*88.44*/i/*88.45*/.count)),format.raw/*88.51*/(""","rows["+"""),_display_(Seq[Any](/*88.61*/{i.count+1})),format.raw/*88.72*/("""+"].cells[1]=",row2.cells[1].innerHTML);
								 // console.log("if:c=1,i.count=","""),_display_(Seq[Any](/*89.44*/i/*89.45*/.count)),format.raw/*89.51*/(""","rows["+"""),_display_(Seq[Any](/*89.61*/{i.count+1})),format.raw/*89.72*/("""+"].cells[2]=",row2.cells[2].innerHTML);
								</script>
								""")))})),format.raw/*91.10*/("""
							""")))}/*92.9*/else/*92.13*/{_display_(Seq[Any](format.raw/*92.14*/("""
								<script>
								"""),_display_(Seq[Any](/*94.10*/if(i.count<5)/*94.23*/{_display_(Seq[Any](format.raw/*94.24*/("""
								 """),_display_(Seq[Any](/*95.11*/{i.count+=1})),format.raw/*95.23*/("""
								
								//console.log("else: c.count=","""),_display_(Seq[Any](/*97.41*/c/*97.42*/.count)),format.raw/*97.48*/("""," i.count=","""),_display_(Seq[Any](/*97.62*/{i.count+1})),format.raw/*97.73*/(""");
								console.log("i="""),_display_(Seq[Any](/*98.25*/i/*98.26*/.count)),format.raw/*98.32*/(""" indexID:"""),_display_(Seq[Any](/*98.42*/index)),format.raw/*98.47*/(""" fileId="""),_display_(Seq[Any](/*98.56*/ele/*98.59*/._4)),format.raw/*98.62*/(""" distance="""),_display_(Seq[Any](/*98.73*/ele/*98.76*/._3)),format.raw/*98.79*/("""");
								var col3=document.getElementById("tablec").rows["""),_display_(Seq[Any](/*99.58*/{i.count+1})),format.raw/*99.69*/("""];
								//var z=col3.insertCell(-1);
									 var z=col3.insertCell("""),_display_(Seq[Any](/*101.34*/{2*c.count-1})),format.raw/*101.47*/(""");
								     var y=col3.insertCell("""),_display_(Seq[Any](/*102.37*/{2*c.count})),format.raw/*102.48*/(""");
								
								// var y=col3.insertCell(-1);
								
								 z.innerHTML="<a href="""),_display_(Seq[Any](/*106.32*/routes/*106.38*/.Files.file(UUID(ele._1)))),format.raw/*106.63*/(""">"""),_display_(Seq[Any](/*106.65*/ele/*106.68*/._4)),format.raw/*106.71*/("""</a>";
								 y.innerHTML="""),_display_(Seq[Any](/*107.23*/ele/*107.26*/._3)),format.raw/*107.29*/(""";
								  var row3=document.getElementById("tablec").rows["""),_display_(Seq[Any](/*108.60*/{i.count+1})),format.raw/*108.71*/("""];
								 console.log("row2.cells["+"""),_display_(Seq[Any](/*109.37*/{2*c.count-1})),format.raw/*109.50*/("""+"].innerHTML=",row3.cells["""),_display_(Seq[Any](/*109.78*/{2*c.count-1})),format.raw/*109.91*/("""].innerHTML);
								 console.log("row2.cells["+"""),_display_(Seq[Any](/*110.37*/{2*c.count})),format.raw/*110.48*/("""+"].innerHTML=",row3.cells["""),_display_(Seq[Any](/*110.76*/{2*c.count})),format.raw/*110.87*/("""].innerHTML);
								""")))})),format.raw/*111.10*/("""
								
								</script>	
							""")))})),format.raw/*114.9*/("""	
						
			     """)))})),format.raw/*116.10*/(""" <!--for  list ends here  -->
			     """),_display_(Seq[Any](/*117.10*/if(c.count>1 && list.length<5 )/*117.41*/{_display_(Seq[Any](format.raw/*117.42*/("""
		 		  			<script>
		 		  			 console.log("inside if length");
		 		  			 
		 		  			   for(var j="""),_display_(Seq[Any](/*121.25*/i/*121.26*/.count)),format.raw/*121.32*/("""+1;j<6;j++)"""),format.raw/*121.43*/("""{"""),format.raw/*121.44*/("""
								//var j="""),_display_(Seq[Any](/*122.18*/i/*122.19*/.count)),format.raw/*122.25*/("""+1;	
								
								console.log("else: c.count=","""),_display_(Seq[Any](/*124.39*/c/*124.40*/.count)),format.raw/*124.46*/("""," i.count=","""),_display_(Seq[Any](/*124.60*/{i.count+1})),format.raw/*124.71*/("""," j=",j);
								
								var col3=document.getElementById("tablec").rows[j+1];
								
									 var z=col3.insertCell("""),_display_(Seq[Any](/*128.34*/{2*c.count-1})),format.raw/*128.47*/(""");
								     var y=col3.insertCell("""),_display_(Seq[Any](/*129.37*/{2*c.count})),format.raw/*129.48*/(""");
								
								
								
								 z.innerHTML="";
								 y.innerHTML="";
								  var row3=document.getElementById("tablec").rows[j+1];
								 console.log("row2.cells["+"""),_display_(Seq[Any](/*136.37*/{2*c.count-1})),format.raw/*136.50*/("""+"].innerHTML=",row3.cells["""),_display_(Seq[Any](/*136.78*/{2*c.count-1})),format.raw/*136.91*/("""].innerHTML);
								 console.log("row2.cells["+"""),_display_(Seq[Any](/*137.37*/{2*c.count})),format.raw/*137.48*/("""+"].innerHTML=",row3.cells["""),_display_(Seq[Any](/*137.76*/{2*c.count})),format.raw/*137.87*/("""].innerHTML);
								"""),format.raw/*138.9*/("""}"""),format.raw/*138.10*/("""
								
								</script>	
		 		          	 """)))})),format.raw/*141.19*/("""	
			
				""")))})),format.raw/*143.6*/("""<!--Inc counter ends here-->
			""")))})),format.raw/*144.5*/("""<!--results map ends here-->
          """)))})),format.raw/*145.12*/("""<!--for ends here--> 
         """)))})),format.raw/*146.11*/("""<!--c def ends here-->      
    </table>
    
	</div>
		
	</div>
	</div>
	""")))})))}
    }
    
    def render(keysArray:scala.collection.mutable.ArrayBuffer[String],name:String,id:String,size:Integer,results:scala.collection.mutable.HashMap[String, scala.collection.mutable.ArrayBuffer[scala.Tuple4[String, String, Double, String]]]): play.api.templates.HtmlFormat.Appendable = apply(keysArray,name,id,size,results)
    
    def f:((scala.collection.mutable.ArrayBuffer[String],String,String,Integer,scala.collection.mutable.HashMap[String, scala.collection.mutable.ArrayBuffer[scala.Tuple4[String, String, Double, String]]]) => play.api.templates.HtmlFormat.Appendable) = (keysArray,name,id,size,results) => apply(keysArray,name,id,size,results)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/imageSearchpage.scala.html
                    HASH: 2d9106d55b4bc5bb1761c66568b30b9f6f5e8114
                    MATRIX: 782->1|1123->216|1151->250|1187->252|1217->274|1256->276|1353->338|1378->342|1437->367|1458->380|1497->382|1621->475|1748->567|1787->597|1829->601|1869->606|1906->627|1945->628|1996->644|2030->656|2071->662|2094->676|2133->677|2218->726|2228->727|2256->733|2357->798|2384->803|2420->821|2432->825|2470->826|2549->869|2559->870|2587->876|2741->994|2768->999|2827->1022|2860->1033|2924->1066|2970->1077|2993->1091|3032->1092|3108->1150|3120->1154|3158->1155|3562->1528|3624->1555|3640->1562|3664->1577|3711->1586|3752->1592|3791->1622|3834->1627|3918->1675|3928->1676|3956->1682|4010->1700|4023->1704|4052->1711|4122->1746|4152->1760|4191->1761|4317->1852|4340->1866|4379->1867|4426->1878|4448->1891|4487->1892|4565->1934|4575->1935|4603->1941|4649->1951|4676->1956|4721->1965|4733->1968|4758->1971|4805->1982|4817->1985|4842->1988|5013->2123|5046->2134|5217->2269|5227->2270|5255->2276|5326->2311|5341->2317|5388->2342|5426->2344|5438->2347|5463->2350|5530->2381|5542->2384|5567->2387|5649->2433|5659->2434|5687->2440|5733->2450|5766->2461|5886->2545|5896->2546|5924->2552|5970->2562|6003->2573|6103->2641|6130->2650|6143->2654|6182->2655|6245->2682|6267->2695|6306->2696|6353->2707|6387->2719|6473->2769|6483->2770|6511->2776|6561->2790|6594->2801|6657->2828|6667->2829|6695->2835|6741->2845|6768->2850|6813->2859|6825->2862|6850->2865|6897->2876|6909->2879|6934->2882|7031->2943|7064->2954|7174->3027|7210->3040|7286->3079|7320->3090|7447->3180|7463->3186|7511->3211|7550->3213|7563->3216|7589->3219|7655->3248|7668->3251|7694->3254|7792->3315|7826->3326|7902->3365|7938->3378|8003->3406|8039->3419|8126->3469|8160->3480|8225->3508|8259->3519|8315->3542|8384->3579|8435->3597|8511->3636|8552->3667|8592->3668|8729->3768|8740->3769|8769->3775|8809->3786|8839->3787|8894->3805|8905->3806|8934->3812|9023->3864|9034->3865|9063->3871|9114->3885|9148->3896|9309->4020|9345->4033|9421->4072|9455->4083|9672->4263|9708->4276|9773->4304|9809->4317|9896->4367|9930->4378|9995->4406|10029->4417|10079->4439|10109->4440|10189->4487|10232->4498|10297->4531|10370->4571|10435->4603
                    LINES: 20->1|24->1|25->3|26->4|26->4|26->4|28->6|28->6|30->8|30->8|30->8|36->14|43->21|43->21|43->21|44->22|44->22|44->22|46->24|46->24|47->25|47->25|47->25|49->27|49->27|49->27|53->31|53->31|55->33|55->33|55->33|57->35|57->35|57->35|60->38|60->38|61->39|61->39|63->41|65->43|65->43|65->43|69->47|69->47|69->47|81->59|86->64|86->64|86->64|86->64|87->65|87->65|87->65|89->67|89->67|89->67|89->67|89->67|89->67|93->71|93->71|93->71|98->76|98->76|98->76|99->77|99->77|99->77|101->79|101->79|101->79|101->79|101->79|101->79|101->79|101->79|101->79|101->79|101->79|103->81|103->81|107->85|107->85|107->85|108->86|108->86|108->86|108->86|108->86|108->86|109->87|109->87|109->87|110->88|110->88|110->88|110->88|110->88|111->89|111->89|111->89|111->89|111->89|113->91|114->92|114->92|114->92|116->94|116->94|116->94|117->95|117->95|119->97|119->97|119->97|119->97|119->97|120->98|120->98|120->98|120->98|120->98|120->98|120->98|120->98|120->98|120->98|120->98|121->99|121->99|123->101|123->101|124->102|124->102|128->106|128->106|128->106|128->106|128->106|128->106|129->107|129->107|129->107|130->108|130->108|131->109|131->109|131->109|131->109|132->110|132->110|132->110|132->110|133->111|136->114|138->116|139->117|139->117|139->117|143->121|143->121|143->121|143->121|143->121|144->122|144->122|144->122|146->124|146->124|146->124|146->124|146->124|150->128|150->128|151->129|151->129|158->136|158->136|158->136|158->136|159->137|159->137|159->137|159->137|160->138|160->138|163->141|165->143|166->144|167->145|168->146
                    -- GENERATED --
                */
            