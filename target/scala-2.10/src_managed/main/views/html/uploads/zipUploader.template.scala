
package views.html.uploads

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object zipUploader extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.67*/("""
"""),_display_(Seq[Any](/*2.2*/main("Upload files")/*2.22*/ {_display_(Seq[Any](format.raw/*2.24*/("""

<style>
.ajax-file-upload-statusbar"""),format.raw/*5.28*/("""{"""),format.raw/*5.29*/("""
	width:800px;
"""),format.raw/*7.1*/("""}"""),format.raw/*7.2*/("""
</style>

<script src=""""),_display_(Seq[Any](/*10.15*/routes/*10.21*/.Assets.at("javascripts/t2c2BatchUpload.js"))),format.raw/*10.65*/("""" type="text/javascript"></script>

<div class="container2" style="width:800px; margin:auto;">
<div class="page-header">
	<h3>4CeeD Zip Uploader</h3>
	<input type="hidden" id="userId" value="">
</div>		
	<div class="alert alert-success" id="colLink" style="margin-bottom: 10px; text-align:left;">
	    <p><a href="/?tv" id="lnkTreeView"> < Dashboard</a></p>
	</div>
	<div class="alert alert-info spaceInfo">
		<h4>Do you want to place this data in a shared space?</h4> 
		<label class="radio-inline">
		<input type="radio" name="showSharedPanel" class="showSharedPanel" value="yes">Yes</label>
		<label class="radio-inline"><input type="radio" name="showSharedPanel" class="showSharedPanel" value="no">No</label>
	</div>
	<form id="formGetSpaces" class="" method="get" action="" name="">
		<hr />

		<div class="jumbotron">
			<div class="form-group spacePanel">
				<label>Choose a shared space...</label>
				<span><a style="font-height:12px;" href="#" data-toggle="popover" title="Shared Spaces" data-trigger="hover" data-content="Shared Spaces allow you to share data with others." title=" ">What's this?</a></span>	
			</div>		
			<div class="panel-group" id="accordionSpace">
				<div class="panel panel-default">
					<div class="panel-heading panel-info ">
						<h3 class="panel-title ">
							<a data-toggle="collapse" data-parent="#accordionSpace" href="#space1">Existing spaces</a>
						</h3>
					</div>
					<div id="space1" class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="wells">								
								<div class="alert alert-info showDangerSpace hidden">
									<span class="glyphicon glyphicon-exclamation-sign"></span> You have no spaces. Create one below. 
								</div>									
								<div class="wells viewSpaces">
									<label>Your Shared Spaces:</label>						
									<select id="spaces" name="spaces" class="form-control ignore" >
										<option value="">--</option>
									</select>
								</div>
							</div>
						</div>
					</div><!-- collapse1 -->
				</div><!-- panel default -->

				<div class="panel panel-success newSpacePanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordionSpace" href="#space2">Create New Shared Space</a>
						</h4>
					</div>
					<div id="space2" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="wells">
								<p>
								<div class="form-group">
									<label>Choose a name for the new space: <span style="color:red;">*</label>
									<input type="text" class="form-control" name="spaceName" id="spaceName" placeholder="Example... Sample Name, Project Name" required>
								</div>
								<div class="form-group">
									<label>Choose a description for the new space:</label>
									<input type="text" class="form-control" name="spaceDescription" id="spaceDescription" placeholder="Example... Shared Space Description" >
								</div>	
								<div class="form-group">
									<label>Search existing users to give access to the space:</label>
									<input type="text" class="form-control" name="spaceActiveUsers" id="spaceActiveUsers" placeholder="Type to search users">
								</div>	
								<div class="form-group" id="spaceShowSelectedUsers" style="padding-left:25px;">
									<label>Choose a permission level for users.<br /></label>
									<div class="userRoleInfo alert alert-warning">
									</div>
								</div>
								<div class="form-group">
									<label>Send email invite to non-users accounts:</label>
									<div class="row">
										<div class="col-md-7">
											<input type="email" class="form-control spaceNonUsers" placeholder="Type valid email address" >
											<div id="emailLabel">
												<label style="color:red;">Enter a valid email.</label>
											</div>
										</div>
										<div class="col-md-3">
											<select class="form-control spaceEmailRole">
												<option value="Admin">Admin</option>
												<option value="Editor" selected>Editor</option>
												<option value="Viewer">Viewer</option>
											</select>
										</div>
										<div class="col-md-2">
											<input type="button" class="btn btn-success form-control createEmailUser" value="Add">										
										</div>										
									</div>
									<div id="spaceShowEmailUsers"><br />
										<div class="userEmailInfo alert alert-warning ">
										</div>
									</div>
								</div>																									
								<div class="form-group" id="btnPostSpace"><input type="button" class="btn btn-success form-control createSpace required" value="Create Shared Space">
								</div>
								</p>
							</div>					
						</div>
					</div><!-- collapse2 -->
				</div><!-- panel-success -->
	 		</div><!-- accordion1 -->
		</div><!-- jumbotron -->
	</form>	
	<div id="fileSubmit">
			<div id="mulitplefileuploader" class="" style="margin: 10px 0;">Browse</div>
			<div class="userQuestion alert alert-warning ">
			The contents and metadata of large zip files may take time to populate. An email will be send to you when this is complete.
			</div>

			<div class="form-group ">									
<!-- 				<input class="btn btn-success form-control" type="button" id="btnSubmit" value="Submit" style="background:green; margin-top:10px; width:200px;">
 -->			</div>
		</div>
	</div>	
""")))})))}
    }
    
    def render(flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply()(flash,user)
    
    def f:(() => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = () => (flash,user) => apply()(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/uploads/zipUploader.scala.html
                    HASH: d90bb898a11770e10aeaa75e1dd499da3f7582f8
                    MATRIX: 633->1|792->66|828->68|856->88|895->90|959->127|987->128|1028->143|1055->144|1116->169|1131->175|1197->219
                    LINES: 20->1|23->1|24->2|24->2|24->2|27->5|27->5|29->7|29->7|32->10|32->10|32->10
                    -- GENERATED --
                */
            