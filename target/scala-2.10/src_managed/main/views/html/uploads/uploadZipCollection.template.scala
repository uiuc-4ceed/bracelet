
package views.html.uploads

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object uploadZipCollection extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[FileMD],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[FileMD])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

implicit def /*6.2*/implicitFieldConstructor/*6.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.60*/("""



"""),format.raw/*6.75*/("""

<!--
The majority of this file has now been changed to be based off elements from the demonstration HTML page
of the blueimp jQuery File Upload library. An open source project locatd here: http://blueimp.github.io/jQuery-File-Upload/
 -->

<!-- Force latest IE rendering engine or ChromeFrame if installed -->
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->

<!-- blueimp Gallery styles - downloaded to make the resource local -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*19.31*/routes/*19.37*/.Assets.at("stylesheets/file-uploader/blueimp-gallery.min.css"))),format.raw/*19.100*/("""">
<!-- Generic page styles -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*21.31*/routes/*21.37*/.Assets.at("stylesheets/file-uploader/style.css"))),format.raw/*21.86*/("""">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*23.31*/routes/*23.37*/.Assets.at("stylesheets/file-uploader/jquery.fileupload.css"))),format.raw/*23.98*/("""">
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*24.31*/routes/*24.37*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-ui.css"))),format.raw/*24.101*/("""">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href=""""),_display_(Seq[Any](/*26.41*/routes/*26.47*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-noscript.css"))),format.raw/*26.117*/(""""></noscript>
<noscript><link rel="stylesheet" href=""""),_display_(Seq[Any](/*27.41*/routes/*27.47*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-ui-noscript.css"))),format.raw/*27.120*/(""""></noscript>

"""),_display_(Seq[Any](/*29.2*/main("Clowder")/*29.17*/ {_display_(Seq[Any](format.raw/*29.19*/("""
<div class="container">
  <div class="page-header">
    <h1>Upload zipped folder</h1>
  </div>

<div class="container2" style="width:800px; margin:auto;">
<div class="page-header">
    <h3>4CeeD Zip Uploader</h3>
    <input type="hidden" id="userId" value="">
</div>      
    <div class="alert alert-success" id="colLink" style="margin-bottom: 10px;">
        <p class=".text-center"><span class="glyphicon glyphicon-folder-close" style="padding-right:5px;"></span><a style="text-decoration:underline;" href="" id="linkToCollection">Last uploaded collection</a></p>
    </div>
    <div class="alert alert-info spaceInfo">
        <h4>Do you want to place this data in a shared space?</h4> 
        <label class="radio-inline">
        <input type="radio" name="showSharedPanel" class="showSharedPanel" value="yes">Yes</label>
        <label class="radio-inline"><input type="radio" name="showSharedPanel" class="showSharedPanel" value="no">No</label>
    </div>
    <form id="formGetSpaces" class="" method="get" action="" name="">
        <hr />

        <div class="jumbotron">
            <div class="form-group spacePanel">
                <label>Choose a shared space...</label>
                <span><a style="font-height:12px;" href="#" data-toggle="popover" title="Shared Spaces" data-trigger="hover" data-content="Shared Spaces allow you to share data with others." title=" ">What's this?</a></span>    
            </div>      
            <div class="panel-group" id="accordionSpace">
                <div class="panel panel-default">
                    <div class="panel-heading panel-info ">
                        <h3 class="panel-title ">
                            <a data-toggle="collapse" data-parent="#accordionSpace" href="#space1">Existing spaces</a>
                        </h3>
                    </div>
                    <div id="space1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="wells">                             
                                <div class="alert alert-info showDangerSpace hidden">
                                    <span class="glyphicon glyphicon-exclamation-sign"></span> You have no spaces. Create one below. 
                                </div>                                  
                                <div class="wells viewSpaces">
                                    <label>Your Shared Spaces:</label>                      
                                    <select id="spaces" name="spaces" class="form-control ignore" >
                                        <option value="">--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div><!-- collapse1 -->
                </div><!-- panel default -->

                <div class="panel panel-success newSpacePanel">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordionSpace" href="#space2">Create New Shared Space</a>
                        </h4>
                    </div>
                    <div id="space2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="wells">
                                <p>
                                <div class="form-group">
                                    <label>Choose a name for the new space: <span style="color:red;">*</label>
                                    <input type="text" class="form-control" name="spaceName" id="spaceName" placeholder="Example... Sample Name, Project Name" required>
                                </div>
                                <div class="form-group">
                                    <label>Choose a description for the new space:</label>
                                    <input type="text" class="form-control" name="spaceDescription" id="spaceDescription" placeholder="Example... Shared Space Description" >
                                </div>  
                                <div class="form-group">
                                    <label>Search existing users to give access to the space:</label>
                                    <input type="text" class="form-control" name="spaceActiveUsers" id="spaceActiveUsers" placeholder="Type to search users">
                                </div>  
                                <div class="form-group" id="spaceShowSelectedUsers" style="padding-left:25px;">
                                    <label>Choose a permission level for users.<br /></label>
                                    <div class="userRoleInfo alert alert-warning">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Send email invite to non-users accounts:</label>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <input type="email" class="form-control spaceNonUsers" placeholder="Type valid email address" >
                                            <div id="emailLabel">
                                                <label style="color:red;">Enter a valid email.</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control spaceEmailRole">
                                                <option value="Admin">Admin</option>
                                                <option value="Editor" selected>Editor</option>
                                                <option value="Viewer">Viewer</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="button" class="btn btn-success form-control createEmailUser" value="Add">                                      
                                        </div>                                      
                                    </div>
                                    <div id="spaceShowEmailUsers"><br />
                                        <div class="userEmailInfo alert alert-warning ">
                                        </div>
                                    </div>
                                </div>                                                                                                  
                                <div class="form-group" id="btnPostSpace"><input type="button" class="btn btn-success form-control createSpace required" value="Create Shared Space">
                                </div>
                                </p>
                            </div>                  
                        </div>
                    </div><!-- collapse2 -->
                </div><!-- panel-success -->
            </div><!-- accordion1 -->
        </div><!-- jumbotron -->
    </form> 


    <!-- The file upload form used as target for the file upload widget -->
    """),format.raw/*146.114*/("""
        """),format.raw/*147.83*/("""
        """),format.raw/*148.82*/("""
        """),format.raw/*149.111*/("""
        """),format.raw/*150.51*/("""
            """),format.raw/*151.39*/("""
                """),format.raw/*152.103*/("""
                """),format.raw/*153.65*/("""
                    """),format.raw/*154.65*/("""
                    """),format.raw/*155.47*/("""
                        """),format.raw/*156.116*/("""
                    """),format.raw/*157.95*/("""
                    """),format.raw/*158.87*/("""
                """),format.raw/*159.28*/("""
                """),format.raw/*160.84*/("""
                    """),format.raw/*161.86*/("""
                """),format.raw/*162.30*/("""
                """),format.raw/*163.70*/("""
                    """),format.raw/*164.87*/("""
                """),format.raw/*165.30*/("""
                """),format.raw/*166.69*/("""
                """),format.raw/*167.28*/("""
                    """),format.raw/*168.74*/("""
                """),format.raw/*169.29*/("""
                """),format.raw/*170.98*/("""
                    """),format.raw/*171.88*/("""
                """),format.raw/*172.30*/("""
                """),format.raw/*173.62*/("""
                """),format.raw/*174.61*/("""

            """),format.raw/*176.23*/("""
            """),format.raw/*177.51*/("""
            """),format.raw/*178.64*/("""
                """),format.raw/*179.53*/("""
                """),format.raw/*180.124*/("""
                    """),format.raw/*181.96*/("""
                """),format.raw/*182.27*/("""
                """),format.raw/*183.64*/("""
                """),format.raw/*184.64*/("""
            """),format.raw/*185.23*/("""
        """),format.raw/*186.19*/("""
        """),format.raw/*187.79*/("""
        """),format.raw/*188.105*/("""
    """),format.raw/*189.16*/("""
</div>

	<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
"""),format.raw/*205.1*/("""{"""),format.raw/*205.2*/("""% for (var i=0, file; file=o.files[i]; i++) """),format.raw/*205.46*/("""{"""),format.raw/*205.47*/(""" %"""),format.raw/*205.49*/("""}"""),format.raw/*205.50*/("""
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">"""),format.raw/*211.29*/("""{"""),format.raw/*211.30*/("""%=file.name%"""),format.raw/*211.42*/("""}"""),format.raw/*211.43*/("""</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            """),format.raw/*219.13*/("""{"""),format.raw/*219.14*/("""% if (!i && !o.options.autoUpload) """),format.raw/*219.49*/("""{"""),format.raw/*219.50*/(""" %"""),format.raw/*219.52*/("""}"""),format.raw/*219.53*/("""
                <button type="button" class="btn btn-link start" disabled>
                    <span class="glyphicon glyphicon-play"></span> Start
                </button>
            """),format.raw/*223.13*/("""{"""),format.raw/*223.14*/("""% """),format.raw/*223.16*/("""}"""),format.raw/*223.17*/(""" %"""),format.raw/*223.19*/("""}"""),format.raw/*223.20*/("""
            """),format.raw/*224.13*/("""{"""),format.raw/*224.14*/("""% if (!i) """),format.raw/*224.24*/("""{"""),format.raw/*224.25*/(""" %"""),format.raw/*224.27*/("""}"""),format.raw/*224.28*/("""
                <button type="button" class="btn btn-link cancel">
                    <span class="glyphicon glyphicon-remove"></span> Cancel
                </button>
            """),format.raw/*228.13*/("""{"""),format.raw/*228.14*/("""% """),format.raw/*228.16*/("""}"""),format.raw/*228.17*/(""" %"""),format.raw/*228.19*/("""}"""),format.raw/*228.20*/("""
        </td>
    </tr>
"""),format.raw/*231.1*/("""{"""),format.raw/*231.2*/("""% """),format.raw/*231.4*/("""}"""),format.raw/*231.5*/(""" %"""),format.raw/*231.7*/("""}"""),format.raw/*231.8*/("""
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
"""),format.raw/*235.1*/("""{"""),format.raw/*235.2*/("""% for (var i=0, file; file=o.files[i]; i++) """),format.raw/*235.46*/("""{"""),format.raw/*235.47*/(""" %"""),format.raw/*235.49*/("""}"""),format.raw/*235.50*/("""
    <tr class="template-download fade">
        """),format.raw/*237.9*/("""{"""),format.raw/*237.10*/("""% if (file.deleteUrl) """),format.raw/*237.32*/("""{"""),format.raw/*237.33*/(""" %"""),format.raw/*237.35*/("""}"""),format.raw/*237.36*/("""
        <td>
            <input type="checkbox" name="delete" value="1" class="toggle">
        </td>
        """),format.raw/*241.9*/("""{"""),format.raw/*241.10*/("""% """),format.raw/*241.12*/("""}"""),format.raw/*241.13*/(""" %"""),format.raw/*241.15*/("""}"""),format.raw/*241.16*/("""
        <td>
            <span class="preview">
                """),format.raw/*244.17*/("""{"""),format.raw/*244.18*/("""% if (file.thumbnailUrl) """),format.raw/*244.43*/("""{"""),format.raw/*244.44*/(""" %"""),format.raw/*244.46*/("""}"""),format.raw/*244.47*/("""
                    <a href=""""),format.raw/*245.30*/("""{"""),format.raw/*245.31*/("""%=file.url%"""),format.raw/*245.42*/("""}"""),format.raw/*245.43*/("""" title=""""),format.raw/*245.52*/("""{"""),format.raw/*245.53*/("""%=file.name%"""),format.raw/*245.65*/("""}"""),format.raw/*245.66*/("""" download=""""),format.raw/*245.78*/("""{"""),format.raw/*245.79*/("""%=file.name%"""),format.raw/*245.91*/("""}"""),format.raw/*245.92*/("""" data-gallery><img src=""""),format.raw/*245.117*/("""{"""),format.raw/*245.118*/("""%=file.thumbnailUrl%"""),format.raw/*245.138*/("""}"""),format.raw/*245.139*/(""""></a>
                """),format.raw/*246.17*/("""{"""),format.raw/*246.18*/("""% """),format.raw/*246.20*/("""}"""),format.raw/*246.21*/(""" %"""),format.raw/*246.23*/("""}"""),format.raw/*246.24*/("""
            </span>
        </td>
        <td>
            <p class="name">
                """),format.raw/*251.17*/("""{"""),format.raw/*251.18*/("""% if (file.url) """),format.raw/*251.34*/("""{"""),format.raw/*251.35*/(""" %"""),format.raw/*251.37*/("""}"""),format.raw/*251.38*/("""
                    <a href=""""),format.raw/*252.30*/("""{"""),format.raw/*252.31*/("""%=file.url%"""),format.raw/*252.42*/("""}"""),format.raw/*252.43*/("""" title=""""),format.raw/*252.52*/("""{"""),format.raw/*252.53*/("""%=file.name%"""),format.raw/*252.65*/("""}"""),format.raw/*252.66*/("""" target="_blank" """),format.raw/*252.84*/("""{"""),format.raw/*252.85*/("""%=file.thumbnailUrl?'data-gallery':''%"""),format.raw/*252.123*/("""}"""),format.raw/*252.124*/(""">"""),format.raw/*252.125*/("""{"""),format.raw/*252.126*/("""%=file.name%"""),format.raw/*252.138*/("""}"""),format.raw/*252.139*/("""</a>
                """),format.raw/*253.17*/("""{"""),format.raw/*253.18*/("""% """),format.raw/*253.20*/("""}"""),format.raw/*253.21*/(""" else """),format.raw/*253.27*/("""{"""),format.raw/*253.28*/(""" %"""),format.raw/*253.30*/("""}"""),format.raw/*253.31*/("""
                    <span>"""),format.raw/*254.27*/("""{"""),format.raw/*254.28*/("""%=file.name%"""),format.raw/*254.40*/("""}"""),format.raw/*254.41*/("""</span>
                """),format.raw/*255.17*/("""{"""),format.raw/*255.18*/("""% """),format.raw/*255.20*/("""}"""),format.raw/*255.21*/(""" %"""),format.raw/*255.23*/("""}"""),format.raw/*255.24*/("""
            </p>
            """),format.raw/*257.13*/("""{"""),format.raw/*257.14*/("""% if (file.error) """),format.raw/*257.32*/("""{"""),format.raw/*257.33*/(""" %"""),format.raw/*257.35*/("""}"""),format.raw/*257.36*/("""
                <div><span class="label label-danger">Error</span> """),format.raw/*258.68*/("""{"""),format.raw/*258.69*/("""%=file.error%"""),format.raw/*258.82*/("""}"""),format.raw/*258.83*/("""</div>
            """),format.raw/*259.13*/("""{"""),format.raw/*259.14*/("""% """),format.raw/*259.16*/("""}"""),format.raw/*259.17*/(""" %"""),format.raw/*259.19*/("""}"""),format.raw/*259.20*/("""
        </td>
        <td>
            <span class="size">"""),format.raw/*262.32*/("""{"""),format.raw/*262.33*/("""%=o.formatFileSize(file.size)%"""),format.raw/*262.63*/("""}"""),format.raw/*262.64*/("""</span>
        </td>
        <td>
            """),format.raw/*265.13*/("""{"""),format.raw/*265.14*/("""% if (file.deleteUrl) """),format.raw/*265.36*/("""{"""),format.raw/*265.37*/(""" %"""),format.raw/*265.39*/("""}"""),format.raw/*265.40*/("""
                <button type="button" class="btn btn-link delete" data-type=""""),format.raw/*266.78*/("""{"""),format.raw/*266.79*/("""%=file.deleteType%"""),format.raw/*266.97*/("""}"""),format.raw/*266.98*/("""" data-url=""""),format.raw/*266.110*/("""{"""),format.raw/*266.111*/("""%=file.deleteUrl%"""),format.raw/*266.128*/("""}"""),format.raw/*266.129*/("""""""),format.raw/*266.130*/("""{"""),format.raw/*266.131*/("""% if (file.deleteWithCredentials) """),format.raw/*266.165*/("""{"""),format.raw/*266.166*/(""" %"""),format.raw/*266.168*/("""}"""),format.raw/*266.169*/(""" data-xhr-fields='"""),format.raw/*266.187*/("""{"""),format.raw/*266.188*/(""""withCredentials":true"""),format.raw/*266.210*/("""}"""),format.raw/*266.211*/("""'"""),format.raw/*266.212*/("""{"""),format.raw/*266.213*/("""% """),format.raw/*266.215*/("""}"""),format.raw/*266.216*/(""" %"""),format.raw/*266.218*/("""}"""),format.raw/*266.219*/(""">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </button>
            """),format.raw/*269.13*/("""{"""),format.raw/*269.14*/("""% """),format.raw/*269.16*/("""}"""),format.raw/*269.17*/(""" else """),format.raw/*269.23*/("""{"""),format.raw/*269.24*/(""" %"""),format.raw/*269.26*/("""}"""),format.raw/*269.27*/("""
                <button type="button" class="btn btn-link cancel">
                    <span class="glyphicon glyphicon-remove"></span> Cancel
                </button>
            """),format.raw/*273.13*/("""{"""),format.raw/*273.14*/("""% """),format.raw/*273.16*/("""}"""),format.raw/*273.17*/(""" %"""),format.raw/*273.19*/("""}"""),format.raw/*273.20*/("""
        </td>
    </tr>
"""),format.raw/*276.1*/("""{"""),format.raw/*276.2*/("""% """),format.raw/*276.4*/("""}"""),format.raw/*276.5*/(""" %"""),format.raw/*276.7*/("""}"""),format.raw/*276.8*/("""
</script>

<!-- The Templates plugin is included to render the upload/download listings - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*280.15*/routes/*280.21*/.Assets.at("javascripts/file-uploader/tmpl.min.js"))),format.raw/*280.72*/(""""></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*282.15*/routes/*282.21*/.Assets.at("javascripts/file-uploader/load-image.all.min.js"))),format.raw/*282.82*/(""""></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*284.15*/routes/*284.21*/.Assets.at("javascripts/file-uploader/canvas-to-blob.min.js"))),format.raw/*284.82*/(""""></script>
<!-- blueimp Gallery script - downloaded to make the resource local-->
<script src=""""),_display_(Seq[Any](/*286.15*/routes/*286.21*/.Assets.at("javascripts/file-uploader/jquery.blueimp-gallery.min.js"))),format.raw/*286.90*/(""""></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src=""""),_display_(Seq[Any](/*288.15*/routes/*288.21*/.Assets.at("javascripts/file-uploader/jquery.iframe-transport.js"))),format.raw/*288.87*/(""""></script>
<!-- The basic File Upload plugin -->
<script src=""""),_display_(Seq[Any](/*290.15*/routes/*290.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload.js"))),format.raw/*290.81*/(""""></script>
<!-- The File Upload processing plugin -->
<script src=""""),_display_(Seq[Any](/*292.15*/routes/*292.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-process.js"))),format.raw/*292.89*/(""""></script>
<!-- The File Upload image preview & resize plugin -->
<script src=""""),_display_(Seq[Any](/*294.15*/routes/*294.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-image.js"))),format.raw/*294.87*/(""""></script>
<!-- The File Upload audio preview plugin -->
<script src=""""),_display_(Seq[Any](/*296.15*/routes/*296.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-audio.js"))),format.raw/*296.87*/(""""></script>
<!-- The File Upload video preview plugin -->
<script src=""""),_display_(Seq[Any](/*298.15*/routes/*298.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-video.js"))),format.raw/*298.87*/(""""></script>
<!-- The File Upload validation plugin -->
<script src=""""),_display_(Seq[Any](/*300.15*/routes/*300.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-validate.js"))),format.raw/*300.90*/(""""></script>
<!-- The File Upload user interface plugin -->
<script src=""""),_display_(Seq[Any](/*302.15*/routes/*302.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-ui.js"))),format.raw/*302.84*/(""""></script>
<!-- The main application script -->
<script src=""""),_display_(Seq[Any](/*304.15*/routes/*304.21*/.Assets.at("javascripts/file-uploader/main.js"))),format.raw/*304.68*/(""""></script>
<!-- Scripts below to facilitate authentication checking on callback before the library uploading is invoked. -->
<script src=""""),_display_(Seq[Any](/*306.15*/routes/*306.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-medici-auth.js"))),format.raw/*306.93*/(""""></script>
<script src=""""),_display_(Seq[Any](/*307.15*/routes/*307.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*307.63*/("""" type="text/javascript"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="js/cors/jquery.xdr-transport.js"></script>
<![endif]-->

""")))})),format.raw/*313.2*/("""
"""))}
    }
    
    def render(myForm:Form[FileMD],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm)(user)
    
    def f:((Form[FileMD]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm) => (user) => apply(myForm)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/uploads/uploadZipCollection.scala.html
                    HASH: 9b1f1e7b0dc233d9cadbf8887cb90411759643b6
                    MATRIX: 635->1|795->81|827->105|906->59|937->154|1477->658|1492->664|1578->727|1676->789|1691->795|1762->844|1923->969|1938->975|2021->1036|2090->1069|2105->1075|2192->1139|2334->1245|2349->1251|2442->1321|2532->1375|2547->1381|2643->1454|2694->1470|2718->1485|2758->1487|10238->9046|10276->9129|10314->9211|10353->9322|10391->9373|10433->9412|10480->9515|10526->9580|10576->9645|10626->9692|10681->9808|10731->9903|10781->9990|10827->10018|10873->10102|10923->10188|10969->10218|11015->10288|11065->10375|11111->10405|11157->10474|11203->10502|11253->10576|11299->10605|11345->10703|11395->10791|11441->10821|11487->10883|11533->10944|11576->10968|11618->11019|11660->11083|11706->11136|11753->11260|11803->11356|11849->11383|11895->11447|11941->11511|11983->11534|12021->11553|12059->11632|12098->11737|12132->11753|12620->12213|12649->12214|12722->12258|12752->12259|12783->12261|12813->12262|12991->12411|13021->12412|13062->12424|13092->12425|13505->12809|13535->12810|13599->12845|13629->12846|13660->12848|13690->12849|13906->13036|13936->13037|13967->13039|13997->13040|14028->13042|14058->13043|14100->13056|14130->13057|14169->13067|14199->13068|14230->13070|14260->13071|14471->13253|14501->13254|14532->13256|14562->13257|14593->13259|14623->13260|14676->13285|14705->13286|14735->13288|14764->13289|14794->13291|14823->13292|14975->13416|15004->13417|15077->13461|15107->13462|15138->13464|15168->13465|15245->13514|15275->13515|15326->13537|15356->13538|15387->13540|15417->13541|15556->13652|15586->13653|15617->13655|15647->13656|15678->13658|15708->13659|15802->13724|15832->13725|15886->13750|15916->13751|15947->13753|15977->13754|16036->13784|16066->13785|16106->13796|16136->13797|16174->13806|16204->13807|16245->13819|16275->13820|16316->13832|16346->13833|16387->13845|16417->13846|16472->13871|16503->13872|16553->13892|16584->13893|16636->13916|16666->13917|16697->13919|16727->13920|16758->13922|16788->13923|16910->14016|16940->14017|16985->14033|17015->14034|17046->14036|17076->14037|17135->14067|17165->14068|17205->14079|17235->14080|17273->14089|17303->14090|17344->14102|17374->14103|17421->14121|17451->14122|17519->14160|17550->14161|17581->14162|17612->14163|17654->14175|17685->14176|17735->14197|17765->14198|17796->14200|17826->14201|17861->14207|17891->14208|17922->14210|17952->14211|18008->14238|18038->14239|18079->14251|18109->14252|18162->14276|18192->14277|18223->14279|18253->14280|18284->14282|18314->14283|18373->14313|18403->14314|18450->14332|18480->14333|18511->14335|18541->14336|18638->14404|18668->14405|18710->14418|18740->14419|18788->14438|18818->14439|18849->14441|18879->14442|18910->14444|18940->14445|19028->14504|19058->14505|19117->14535|19147->14536|19223->14583|19253->14584|19304->14606|19334->14607|19365->14609|19395->14610|19502->14688|19532->14689|19579->14707|19609->14708|19651->14720|19682->14721|19729->14738|19760->14739|19791->14740|19822->14741|19886->14775|19917->14776|19949->14778|19980->14779|20028->14797|20059->14798|20111->14820|20142->14821|20173->14822|20204->14823|20236->14825|20267->14826|20299->14828|20330->14829|20474->14944|20504->14945|20535->14947|20565->14948|20600->14954|20630->14955|20661->14957|20691->14958|20902->15140|20932->15141|20963->15143|20993->15144|21024->15146|21054->15147|21107->15172|21136->15173|21166->15175|21195->15176|21225->15178|21254->15179|21438->15326|21454->15332|21528->15383|21730->15548|21746->15554|21830->15615|22013->15761|22029->15767|22113->15828|22247->15925|22263->15931|22355->16000|22510->16118|22526->16124|22615->16190|22716->16254|22732->16260|22815->16320|22921->16389|22937->16395|23028->16463|23146->16544|23162->16550|23251->16616|23360->16688|23376->16694|23465->16760|23574->16832|23590->16838|23679->16904|23785->16973|23801->16979|23893->17048|24003->17121|24019->17127|24105->17190|24205->17253|24221->17259|24291->17306|24468->17446|24484->17452|24579->17524|24642->17550|24658->17556|24723->17598|24992->17835
                    LINES: 20->1|23->6|23->6|24->1|28->6|41->19|41->19|41->19|43->21|43->21|43->21|45->23|45->23|45->23|46->24|46->24|46->24|48->26|48->26|48->26|49->27|49->27|49->27|51->29|51->29|51->29|168->146|169->147|170->148|171->149|172->150|173->151|174->152|175->153|176->154|177->155|178->156|179->157|180->158|181->159|182->160|183->161|184->162|185->163|186->164|187->165|188->166|189->167|190->168|191->169|192->170|193->171|194->172|195->173|196->174|198->176|199->177|200->178|201->179|202->180|203->181|204->182|205->183|206->184|207->185|208->186|209->187|210->188|211->189|227->205|227->205|227->205|227->205|227->205|227->205|233->211|233->211|233->211|233->211|241->219|241->219|241->219|241->219|241->219|241->219|245->223|245->223|245->223|245->223|245->223|245->223|246->224|246->224|246->224|246->224|246->224|246->224|250->228|250->228|250->228|250->228|250->228|250->228|253->231|253->231|253->231|253->231|253->231|253->231|257->235|257->235|257->235|257->235|257->235|257->235|259->237|259->237|259->237|259->237|259->237|259->237|263->241|263->241|263->241|263->241|263->241|263->241|266->244|266->244|266->244|266->244|266->244|266->244|267->245|267->245|267->245|267->245|267->245|267->245|267->245|267->245|267->245|267->245|267->245|267->245|267->245|267->245|267->245|267->245|268->246|268->246|268->246|268->246|268->246|268->246|273->251|273->251|273->251|273->251|273->251|273->251|274->252|274->252|274->252|274->252|274->252|274->252|274->252|274->252|274->252|274->252|274->252|274->252|274->252|274->252|274->252|274->252|275->253|275->253|275->253|275->253|275->253|275->253|275->253|275->253|276->254|276->254|276->254|276->254|277->255|277->255|277->255|277->255|277->255|277->255|279->257|279->257|279->257|279->257|279->257|279->257|280->258|280->258|280->258|280->258|281->259|281->259|281->259|281->259|281->259|281->259|284->262|284->262|284->262|284->262|287->265|287->265|287->265|287->265|287->265|287->265|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|288->266|291->269|291->269|291->269|291->269|291->269|291->269|291->269|291->269|295->273|295->273|295->273|295->273|295->273|295->273|298->276|298->276|298->276|298->276|298->276|298->276|302->280|302->280|302->280|304->282|304->282|304->282|306->284|306->284|306->284|308->286|308->286|308->286|310->288|310->288|310->288|312->290|312->290|312->290|314->292|314->292|314->292|316->294|316->294|316->294|318->296|318->296|318->296|320->298|320->298|320->298|322->300|322->300|322->300|324->302|324->302|324->302|326->304|326->304|326->304|328->306|328->306|328->306|329->307|329->307|329->307|335->313
                    -- GENERATED --
                */
            