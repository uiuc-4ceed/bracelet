
package views.html.uploads

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object uploader extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.67*/("""
"""),_display_(Seq[Any](/*2.2*/main("Upload files")/*2.22*/ {_display_(Seq[Any](format.raw/*2.24*/("""

<script src=""""),_display_(Seq[Any](/*4.15*/routes/*4.21*/.Assets.at("javascripts/t2c2StandardUpload.js"))),format.raw/*4.68*/("""" type="text/javascript"></script>
	
	<div class="container2">
	<div class="page-header">
		<h3>4CeeD Standard Uploader</h3>
<!-- 		<ol>
			<li>Organize your uploads into the folder like collections.</li>
			<li>Create or reuse a metadata template to describe the files your dataset will contain.</li>
			<li>Browse or drag your files and then hit submit.</li>
		</ol>-->
		<input type="hidden" id="userId" value="">
	</div>		
	<div class="alert alert-success" id="colLink" style="margin-bottom: 10px;">
		<p class=".text-center"><span class="glyphicon glyphicon-folder-close" style="padding-right:5px;"></span><a style="text-decoration:underline;" href="" id="linkToCollection">Last uploaded collection</a></p>
	</div>
	<div class="alert alert-info spaceInfo">
		<h4>Do you want to place this data in a shared space?</h4> 
		<label class="radio-inline">
		<input type="radio" name="showSharedPanel" class="showSharedPanel" value="yes">Yes</label>
		<label class="radio-inline"><input type="radio" name="showSharedPanel" class="showSharedPanel" value="no">No</label>
	</div>
	<form id="formGetSpaces" class="" method="get" action="" name="" >
		<hr />

		<div class="jumbotron">
			<div class="form-group spacePanel">
				<label>Choose a shared space...</label>
				<span><a style="font-height:12px;" href="#" data-toggle="popover" title="Shared Spaces" data-trigger="hover" data-content="Shared Spaces allow you to share data with others." title=" ">What's this?</a></span>	
			</div>		
			<div class="panel-group" id="accordionSpace">
				<div class="panel panel-default">
					<div class="panel-heading panel-info ">
						<h3 class="panel-title ">
							<a data-toggle="collapse" data-parent="#accordionSpace" href="#space1">Existing spaces</a>
						</h3>
					</div>
					<div id="space1" class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="wells">								
								<div class="alert alert-info showDangerSpace hidden">
									<span class="glyphicon glyphicon-exclamation-sign"></span> You have no spaces. Create one below. 
								</div>									
								<div class="wells viewSpaces">
									<label>Your Shared Spaces:</label>						
									<select id="spaces" name="spaces" class="form-control ignore" >
										<option value="">--</option>
									</select>
								</div>
							</div>
						</div>
					</div><!-- collapse1 -->
				</div><!-- panel default -->

				<div class="panel panel-success newSpacePanel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordionSpace" href="#space2">Create New Shared Space</a>
						</h4>
					</div>
					<div id="space2" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="wells">
								<p>
								<div class="form-group">
									<label>Choose a name for the new space: <span style="color:red;">*</label>
									<input type="text" class="form-control" name="spaceName" id="spaceName" placeholder="Example... Sample Name, Project Name" required>
								</div>
								<div class="form-group">
									<label>Choose a description for the new space:</label>
									<input type="text" class="form-control" name="spaceDescription" id="spaceDescription" placeholder="Example... Shared Space Description" >
								</div>	
								<div class="form-group">
									<label>Search existing users to give access to the space:</label>
									<input type="text" class="form-control" name="spaceActiveUsers" id="spaceActiveUsers" placeholder="Type to search users">
								</div>	
								<div class="form-group" id="spaceShowSelectedUsers" style="padding-left:25px;">
									<label>Choose a permission level for users.<br /></label>
									<div class="userRoleInfo alert alert-warning">
									</div>
								</div>
								<div class="form-group">
									<label>Send email invite to non-users accounts:</label>
									<div class="row">
										<div class="col-md-7">
											<input type="email" class="form-control spaceNonUsers" placeholder="Type valid email address" >
											<div id="emailLabel">
												<label style="color:red;">Enter a valid email.</label>
											</div>
										</div>
										<div class="col-md-3">
											<select class="form-control spaceEmailRole">
												<option value="Admin">Admin</option>
												<option value="Editor" selected>Editor</option>
												<option value="Viewer">Viewer</option>
											</select>
										</div>
										<div class="col-md-2">
											<input type="button" class="btn btn-success form-control createEmailUser" value="Add">										
										</div>										
									</div>
									<div id="spaceShowEmailUsers"><br />
										<div class="userEmailInfo alert alert-warning ">
										</div>
									</div>
								</div>																									
								<div class="form-group" id="btnPostSpace"><input type="button" class="btn btn-success form-control createSpace required" value="Create Shared Space">
								</div>
								</p>
							</div>					
						</div>
					</div><!-- collapse2 -->
				</div><!-- panel-success -->
	 		</div><!-- accordion1 -->
		</div><!-- jumbotron -->
	</form>	
	<form id="formGetCollections" class="" method="get" action="" name="">
		<hr />

			<div class="jumbotron">
				<div class="form-group colPanel">

                    <img src=""""),_display_(Seq[Any](/*125.32*/routes/*125.38*/.Assets.at("images/1.png"))),format.raw/*125.64*/("""" width="40" />  
					<label>Choose a collection...</label>
					<span><a style="font-height:12px;" href="#" data-toggle="popover" title="Collections" data-trigger="hover" data-content="Collections are like folders and used to organize a project/experiment" title=" ">What's this?</a></span>	
				</div>		
				<div class="panel-group" id="accordion1">
					<div class="panel panel-default">
						<div class="panel-heading panel-info ">
							<h3 class="panel-title ">
								<a data-toggle="collapse" data-parent="#accordion1" href="#collapse1">Existing collections</a>
							</h3>
						</div>
						<div id="collapse1" class="panel-collapse collapse in">
							<div class="panel-body">
								<div class="wells">
								    <div class="showSearch">
										<input class="search-input form-control" placeholder="Search your collections"></input><br />
									</div>
									<div class="alert alert-info showInfo">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<span class="glyphicon glyphicon-info-sign"></span> Right click a collection to create or rename a sub-collection. 
									</div>	
									<div class="alert alert-info showDanger hidden">
										<span class="glyphicon glyphicon-exclamation-sign"></span> You have no collections. Create one below. 
									</div>	
									<div id="collections" required class="pad_this_top">
									</div>
<!-- 									<span class="validCollection">Collection is required.</span>
 -->							</div>
							</div>
						</div><!-- collapse1 -->
					</div><!-- panel default -->
					<div class="panel panel-success newColPanel">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion1" href="#collapse2" id="rootCollection">New Root Collection</a>
							</h4>
						</div>
						<div id="collapse2" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="wells">
									<p>
									<div class="form-group">
										<label>Choose a name for the new collection:</label>
										<input type="text" class="form-control" name="collectionName" id="collectionName" placeholder="Example... Sample Name, Project Name, TuB2" required>
									</div>
									<div class="form-group">
										<label>Choose a description for the new collection:</label>
										<input type="text" class="form-control" name="collectionDescription" id="collectionDescription" placeholder="Example... Collection Description" >
									</div>									
									<div class="form-group" id="btnPostCollection">									
										<input type="button" class="btn btn-success form-control createCollection" value="Create Collection">
									</div>
									</p>
								</div>					
							</div>
						</div><!-- collapse2 -->
					</div><!-- panel-success -->
		 		</div><!-- accordion1 -->
			</div><!-- jumbotron -->
		</form>

		<!-- Dataset --> 
		<form id="formGetDatasets" class="hidden" method="get" action="">
		<hr />
		<div class="jumbotron">
			<div class="form-group dsPanel" >
                <img src=""""),_display_(Seq[Any](/*191.28*/routes/*191.34*/.Assets.at("images/2.png"))),format.raw/*191.60*/("""" width="40" /> 
				<label>Choose a dataset...</label>
				<span><a style="font-height:12px;" href="#" data-toggle="popover" title="Datasets" data-trigger="hover" data-content="Files are grouped into datasets. Datasets can have metadata." title=" ">What's this?</a></span>	
			</div>		
			<div class="panel-group" id="accordion2">
				<div class="panel panel-default existingDS">
					<div class="panel-heading existingDS">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion2" href="#collapse3">Existing Datasets</a>
						</h4>
					</div>
					<div id="collapse3" class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="wells">
								<label>Your Datasets:</label>						
								<select id="datasets" name="datasets" class="form-control ignore" required >
									<option value="">--</option>
								</select>
							</div>
						</div>
					</div><!-- collapse3 -->
				</div><!-- panel-default -->

				<div class="panel panel-success dsNewPanel">
					<div class="panel-heading newDS">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion2" href="#collapse4">New Dataset</a>
						</h4>
					</div>
					<div id="collapse4" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="wells">
								<ul class="nav nav-tabs">
								  <li class="active"><a data-toggle="tab" href="#basicMenu" class="clearMenu">Basic</a></li>
								  <li><a data-toggle="tab" href="#custMenu" class="custMenu">Load Template</a></li>
								  <li><a data-toggle="tab" href="#createMenu" class="createMenu">Create Template</a></li>
								  <li><a data-toggle="tab" href="#prevMenu" class="prevMenu">Load Previous</a></li>
								</ul>	
								<div class="tab-content padTop">
								  <div id="basicMenu" class="tab-pane fade in active ">
									<div class="form-group">
										<label>Choose a name for the new dataset:</label>
										<input type="text" class="form-control ignore datasetName" placeholder="Example... Sample Name, PECVD Oxide, Diffusion" required>
									</div>
									<div class="form-group">
										<label>User defined metadata:</label>
										<input type="text" class="form-control ignore datasetDescription" name="datasetDescription" id="datasetDescription" placeholder="Example... Time, Temp, Pressure, Current" required >
									</div>	
									<div class="form-group">	
								    	<input class="btn btn-success form-control btnDataset" type="button" value="Create Dataset">
									</div>
								  </div>
								  <div id="custMenu" class="tab-pane fade">
								    <div class="form-group padTop">
										<div class="showTemplates">

											<div class="container">
												<div class="row">
													<div class="col-lg-4">
														<label>My Templates:</label><br />
														<select class="templates form-control"></select>
													</div>
													<div class="col-sm-4">
														<label>Global Templates:</label><br />
														<select class="globalTemplates form-control"></select>
													</div>
													<div class="col-sm-4">
														<label>Template Tag Search:</label><br />											
														<input class="form-control templateSearch" type="text" placeholder="Search by name or tag">
													</div>
												</div>
											</div>
										</div>
										<hr />
								    	<div class="tagData">
											<label>Template Name:</label>

											<select class="form-control tagTemplates">
											</select>
											<br />
								    	</div>										
										<div class="otherOptions">
											<label>Choose a name for your dataset:</label>
											<input type="text" class="form-control datasetName" placeholder="Example... Sample Name, PECVD Oxide, Diffusion" required><br />

 											<label>Dataset Description:</label>
											<input type="text" class="form-control datasetDescription" name="datasetDescription" id="datasetDescription" ><br />

											<input type="button" value="Add New Field" class="btn btn-success btnAdd"/>	
									    	<button type="button" class="clearTemplate btn btn-danger">Clear Template</button><br />
									    	<hr />
								    	</div>

										<div class="metaDataSettings">
										</div>	
										<div class="templateData">
											<!--Template boxes will be added here -->
											<br />

										</div>
								    </div>

									<div class="form-group">	
								    	<input class="btn btn-success form-control btnDataset" type="button" value="Create Dataset">
									</div>
								  </div>
								  <div id="createMenu" class="tab-pane fade">
								    <div class="form-group">
									    	<div class="showTemplates">
												<label>Load another template to start with (optional):</label><br />
												<select class="form-control templates">
												</select>
												<br />
											</div>

												<label>Choose a name for your template:</label>
												<input type="text" class="form-control datasetName" placeholder="<Example> Sample Name, PECVD Oxide, Diffusion" required><br />
												

												<label>Add a description:</label>
												<input type="text" class="form-control datasetDescription" placeholder="<Example> User defined metadata for gold nano particles"><br />

												<label>Create tags to describe your template: (optional)</label> 				
												<a style="font-height:12px;" href="#" data-toggle="popover" title="Template Tags" data-trigger="hover" data-content="Tags allow users to search personal and global templates through descriptive tags." title=" ">
												<span class="glyphicon glyphicon-question-sign"></a></span>	
												<br />
												<input type="text" class="form-control tagName" placeholder="<Example> SEM, Diffusion" required><br />
												<input type="button" value="Add New Field" class="btn btn-success btnAdd"/>		
										    	<button class="btn btn-danger clearTemplate" type="button">Clear Template</button>
										    	<br /><br />
												<label>Share this template with others?</label> <input type="checkbox" id="checkShareTemplate"><br />
												
												<div class="metaDataSettings">
												</div>	
												<div class="templateData">
													<!--Template boxes will be added here -->
													<br />
												</div>
												<br />
											<div class="form-group" id="btnTemplate">	
													<input type="button" value="Create Template" class="btn btn-success btnTemplate form-control">
		 										</div>
 										</div>
								  </div>
								  <div id="prevMenu" class="tab-pane fade">
								    <div class="form-group">
								    	<div class="showTemplates">
											<label>Load previous dataset: 											
												<a style="font-height:12px;" href="#" data-toggle="popover" title="Load Previous Datasets" data-trigger="hover" data-content="Create a NEW dataset from one of your previous 10 datasets. Useful for large collections where only one or more values changes each time." title=" ">
												<span class="glyphicon glyphicon-question-sign"></a></span>	
											</label><br />


											<select class="form-control prevTemplates">
											</select>
											<br />
										</div>
										<div class="prevOptions">
											<label>Choose a name for the NEW Dataset:</label>
											<input type="text" class="form-control datasetName" placeholder="Example... Sample Name, PECVD Oxide, Diffusion" required><br />
											<input type="button" value="Add New Field" class="btn btn-success btnAdd"/>		
									    	<button class="btn btn-danger clearTemplate" type="button">Clear Template</button><br /><br />

											<div class="metaDataSettings">
											</div>	
											<div class="templateData">
												<!--Template boxes will be added here -->
											<br />
											</div>
											<br />
											<div class="form-group" id="btnTemplate">	
												<input type="button" value="Create New Dataset" class="btn btn-success btnDataset form-control">
		 									</div>
		 								</div>
 									</div>
 									
								  </div>

						  		</div>
							</div>					
						</div>
					</div><!-- collapse4 -->
				</div><!-- panel-success -->
			</div><!-- accordion2 -->	
		</div><!-- jumbotron --> 	
		</form>

		<div id="fileSubmit" class="hidden">
			<hr />
			<div class="jumbotron"><br />				
				<div class="form-group">
	                <img src=""""),_display_(Seq[Any](/*382.29*/routes/*382.35*/.Assets.at("images/3.png"))),format.raw/*382.61*/("""" width="40" /> 
					<label>Click browse or drag and drop files..</label>
				</div>
				<div id="mulitplefileuploader">Browse</div>
			</div><!-- jumbotron -->

			<div class="form-group">									
				<input class="btn btn-success form-control" type="button" id="btnSubmit" value="Submit">
			</div>
		</div>
	</div>	
""")))})))}
    }
    
    def render(flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply()(flash,user)
    
    def f:(() => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = () => (flash,user) => apply()(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/uploads/uploader.scala.html
                    HASH: d06450f471741f65227e0a3b5df6473571028510
                    MATRIX: 630->1|789->66|825->68|853->88|892->90|943->106|957->112|1025->159|6457->5554|6473->5560|6522->5586|9674->8701|9690->8707|9739->8733|18345->17302|18361->17308|18410->17334
                    LINES: 20->1|23->1|24->2|24->2|24->2|26->4|26->4|26->4|147->125|147->125|147->125|213->191|213->191|213->191|404->382|404->382|404->382
                    -- GENERATED --
                */
            