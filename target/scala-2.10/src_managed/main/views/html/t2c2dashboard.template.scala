
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object t2c2dashboard extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template16[String,List[models.Event],User,List[Dataset],List[Collection],List[ProjectSpace],Boolean,List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],Option[Boolean],List[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*2.2*/(displayedName: String,  newsfeed: List[models.Event], profile:User, datasetsList: List[Dataset], collectionsList:List[Collection], spacesList: List[ProjectSpace],
        deletePermission: Boolean,  followers: List[(UUID, String, String, String)], followedUsers: List[(UUID, String, String, String)], followedFiles: List[(UUID, String, String)],
        followedDatasets: List[(UUID, String, String)], followedCollections: List[(UUID, String, String)],
        followedSpaces: List[(UUID, String, String)], ownProfile: Option[Boolean], userSelections: List[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters


Seq[Any](format.raw/*5.149*/("""

"""),format.raw/*8.1*/("""
"""),_display_(Seq[Any](/*9.2*/main(displayedName)/*9.21*/ {_display_(Seq[Any](format.raw/*9.23*/("""
    <script src=""""),_display_(Seq[Any](/*10.19*/routes/*10.25*/.Assets.at("javascripts/collectionListProcess.js"))),format.raw/*10.75*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*11.19*/routes/*11.25*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*11.72*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*12.19*/routes/*12.25*/.Assets.at("javascripts/spaceListProcess.js"))),format.raw/*12.70*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*13.19*/routes/*13.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*13.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*14.19*/routes/*14.25*/.Assets.at("javascripts/stickytabs/jquery.stickytabs.js"))),format.raw/*14.82*/("""" type="text/javascript"></script>

    <div class="row">
        <h2>"""),_display_(Seq[Any](/*17.14*/profile/*17.21*/.fullName)),format.raw/*17.30*/("""</h2>
        <div class="button-margins">
            <a href=""""),_display_(Seq[Any](/*19.23*/routes/*19.29*/.Profile.viewProfileUUID(profile.id))),format.raw/*19.65*/("""" class="btn btn-link" title="Show profile">
                <span class="glyphicon glyphicon-user"></span> Profile</a>
            <a href=""""),_display_(Seq[Any](/*21.23*/routes/*21.29*/.Spaces.newSpace())),format.raw/*21.47*/("""" class="btn btn-link" title="Create a new """),_display_(Seq[Any](/*21.91*/Messages("space.title"))),format.raw/*21.114*/("""">
                <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*22.63*/Messages("create.title", Messages("space.title")))),format.raw/*22.112*/("""</a>
            <a href=""""),_display_(Seq[Any](/*23.23*/routes/*23.29*/.Datasets.newDataset(None, None))),format.raw/*23.61*/("""" class="btn btn-link" title="Create a new dataset">
                <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*24.63*/Messages("create.title", Messages("dataset.title")))),format.raw/*24.114*/("""</a>
            <a href=""""),_display_(Seq[Any](/*25.23*/routes/*25.29*/.Collections.newCollection(None))),format.raw/*25.61*/("""" class="btn btn-link" title="Create a new collection">
                <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*26.63*/Messages("create.title", Messages("collection.title")))),format.raw/*26.117*/("""</a>
        </div>
    <div class="row">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#events" aria-controls="events" role="tab" data-toggle="tab"><b>Activity</b></a></li>
            <li role="presentation"><a href="#spaces" aria-controls="spaces" role="tab" data-toggle="tab"><b>"""),_display_(Seq[Any](/*32.111*/play/*32.115*/.api.i18n.Messages("home.title", play.api.i18n.Messages("spaces.title")))),format.raw/*32.187*/("""</b></a></li>
            <li role="presentation"><a href="#datasets" aria-controls="datasets" role="tab" data-toggle="tab"><b>"""),_display_(Seq[Any](/*33.115*/play/*33.119*/.api.i18n.Messages("home.title", play.api.i18n.Messages("datasets.title")))),format.raw/*33.193*/("""</b></a></li>
            <li role="presentation"><a href="#collections" aria-controls="collections" role="tab" data-toggle="tab"><b>"""),_display_(Seq[Any](/*34.121*/play/*34.125*/.api.i18n.Messages("home.title", play.api.i18n.Messages("collections.title")))),format.raw/*34.202*/("""</b></a></li>
            <li role="presentation"><a href="#followers" aria-controls="followers" role="tab" data-toggle="tab"><b>Followers</b></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="events">
                <div class="top-padding">
                    """),_display_(Seq[Any](/*41.22*/eventsList(newsfeed))),format.raw/*41.42*/("""
                    <div id="moreevent"></div>
                    <div class="text-center more-events">
                        <a  id="moreeventbutton" href="#/" onClick="moreEvents()" class="btn btn-link">
                            <span class="glyphicon glyphicon-refresh"> </span>
                            View More Events
                        </a>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="spaces">
                """),format.raw/*52.29*/("""
                <div class="row top-padding">
                    <div class="col-xs-12">
                        <p>"""),_display_(Seq[Any](/*55.29*/Messages("space.list.message", Messages("spaces.title")))),format.raw/*55.85*/("""</p>
                    </div>
                    <div class="col-xs-6">
                    </div>
                    <div class="col-xs-6">
                        <span class="pull-right">
                            <a href=""""),_display_(Seq[Any](/*61.39*/routes/*61.45*/.Spaces.list("","",12, "", Some(profile.id.stringify)))),format.raw/*61.99*/("""">See More</a>
                        </span>
                    </div>
                </div>
                <div class="row" id="masonry-spaces">
                """),_display_(Seq[Any](/*66.18*/spacesList/*66.28*/.map/*66.32*/ { space =>_display_(Seq[Any](format.raw/*66.43*/("""
                    """),_display_(Seq[Any](/*67.22*/spaces/*67.28*/.tile(space, "col-xs-3", routes.Application.index(), false))),format.raw/*67.87*/("""
                """)))})),format.raw/*68.18*/("""
                </div>
                """),_display_(Seq[Any](/*70.18*/if(spacesList.size < 1)/*70.41*/ {_display_(Seq[Any](format.raw/*70.43*/("""
                    <div class="text-center">
                        <div>"""),_display_(Seq[Any](/*72.31*/Messages("home.empty.message", Messages("spaces.title")))),format.raw/*72.87*/(""" </div>

                        <div> <a class="btn-link" href=""""),_display_(Seq[Any](/*74.58*/routes/*74.64*/.Spaces.newSpace())),format.raw/*74.82*/("""" title="Create a new """),_display_(Seq[Any](/*74.105*/Messages("space.title"))),format.raw/*74.128*/("""">"""),_display_(Seq[Any](/*74.131*/Messages("create.title", Messages("space.title")))),format.raw/*74.180*/("""</a></div>
                    </div>
                """)))})),format.raw/*76.18*/("""
            </div>
            <div role="tabpanel" class="tab-pane" id="datasets">
                """),format.raw/*79.31*/("""
                <div class="row top-padding">
                    <div class="col-xs-12">
                        <p>"""),_display_(Seq[Any](/*82.29*/Messages("dataset.list.message", Messages("datasets.title").toLowerCase, Messages("dataset.title").toLowerCase))),format.raw/*82.140*/("""  </p>
                    </div>
                    <div class="col-xs-6">
                    </div>
                    <div class="col-xs-6">
                        <span class="pull-right"><a href=""""),_display_(Seq[Any](/*87.60*/routes/*87.66*/.Datasets.list("","",12, None, None, "", Some(profile.id.stringify)))),format.raw/*87.134*/("""">See More</a></span>
                    </div>
                </div>

            </div>
            <div role="tabpanel" class="tab-pane" id="collections">
                """),format.raw/*93.34*/("""
                <div class="row top-padding">
                    <div class="col-xs-12">
                        <p>"""),_display_(Seq[Any](/*96.29*/Messages("collection.list.message", Messages("collections.title").toLowerCase,  Messages("datasets.title").toLowerCase ))),format.raw/*96.149*/("""</p>
                    </div>
                    <div class="col-xs-6">
                    </div>
                    <div class="col-xs-6">
                        <span class="pull-right">
                            <a href=""""),_display_(Seq[Any](/*102.39*/routes/*102.45*/.Collections.list("","",12, None, "", Some(profile.id.stringify)))),format.raw/*102.110*/("""">See More</a>
                        </span>
                    </div>
                </div>
                <div class="row" id ="masonry-collections">
                """),_display_(Seq[Any](/*107.18*/collectionsList/*107.33*/.map/*107.37*/ { collection =>_display_(Seq[Any](format.raw/*107.53*/("""
                    """),_display_(Seq[Any](/*108.22*/collections/*108.33*/.tile(collection, routes.Application.index(), None, "col-xs-3", false))),format.raw/*108.103*/("""
                """)))})),format.raw/*109.18*/("""
                </div>
                """),_display_(Seq[Any](/*111.18*/if(collectionsList.size < 1)/*111.46*/ {_display_(Seq[Any](format.raw/*111.48*/("""
                    <div class="text-center">
                        <div>"""),_display_(Seq[Any](/*113.31*/Messages("home.empty.message", Messages("collections.title").toLowerCase))),format.raw/*113.104*/(""" </div>

                        <div> <a class="btn-link" href=""""),_display_(Seq[Any](/*115.58*/routes/*115.64*/.Collections.newCollection(None))),format.raw/*115.96*/("""" title="Create a new """),_display_(Seq[Any](/*115.119*/Messages("collection.title"))),format.raw/*115.147*/("""">"""),_display_(Seq[Any](/*115.150*/Messages("create.title", Messages("collection.title")))),format.raw/*115.204*/("""</a></div>
                    </div>
                """)))})),format.raw/*117.18*/("""
            </div>
            <div role="tabpanel" class="tab-pane" id="followers">
                <div class="row top-padding">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <h2> Followers</h2>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*125.61*/routes/*125.67*/.Users.getFollowers())),format.raw/*125.88*/("""">See More</a></span>
                    </div>
                </div>
                <div class="row">

                    """),_display_(Seq[Any](/*130.22*/for(row <- followers) yield /*130.43*/ {_display_(Seq[Any](format.raw/*130.45*/("""
                        """),_display_(Seq[Any](/*131.26*/users/*131.31*/.tile(row, "col-xs-2", false))),format.raw/*131.60*/("""
                    """)))})),format.raw/*132.22*/("""
                    """),_display_(Seq[Any](/*133.22*/if(followers.size < 1)/*133.44*/ {_display_(Seq[Any](format.raw/*133.46*/("""
                        <div class="text-center col-xs-12">
                            This area will show users that follow you. Ask other users to follow you <a class="btn-link" href=""""),_display_(Seq[Any](/*135.129*/routes/*135.135*/.Profile.viewProfileUUID(profile.id))),format.raw/*135.171*/("""">here</a>
                        </div>
                    """)))})),format.raw/*137.22*/("""
                </div>
                <h2>Following</h2>
                <div class="panel-group" id="accordion" style="margin-top: 15px">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Users</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*150.77*/routes/*150.83*/.Users.getFollowing())),format.raw/*150.104*/("""">See More</a></span>
                                    </div>
                                </div>

                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body caption break-word">
                                <div class="row" style="margin-top : 15px;">
                                """),_display_(Seq[Any](/*159.34*/for(userInfo <- followedUsers) yield /*159.64*/ {_display_(Seq[Any](format.raw/*159.66*/("""
                                    """),_display_(Seq[Any](/*160.38*/users/*160.43*/.tile(userInfo, "col-xs-2", true))),format.raw/*160.76*/("""
                                """)))})),format.raw/*161.34*/("""
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSpaces">"""),_display_(Seq[Any](/*172.116*/Messages("spaces.title"))),format.raw/*172.140*/("""</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*175.77*/routes/*175.83*/.Spaces.followingSpaces(0, 12, ""))),format.raw/*175.117*/("""">See More</a></span>
                                    </div>
                                </div>

                            </h4>
                        </div>
                        <div id="collapseSpaces" class="panel-collapse collapse">
                            <div class="panel-body">
                            """),_display_(Seq[Any](/*183.30*/for(info <- followedSpaces) yield /*183.57*/ {_display_(Seq[Any](format.raw/*183.59*/("""
                                <div class="col-xs-3" style="margin-top : 30 px" id=""""),_display_(Seq[Any](/*184.87*/info/*184.91*/._1)),format.raw/*184.94*/("""-tile">
                                    <div class="panel panel-default space-panel">
                                        <div class="pull-left">
                                            <span class="glyphicon glyphicon-hdd"></span>
                                        </div>
                                        <div class="panel-body caption break-word">
                                            <h4><a href=""""),_display_(Seq[Any](/*190.59*/routes/*190.65*/.Spaces.getSpace(info._1))),format.raw/*190.90*/("""">"""),_display_(Seq[Any](/*190.93*/info/*190.97*/._2.toString)),format.raw/*190.109*/("""</a></h4>
                                        """),_display_(Seq[Any](/*191.42*/info/*191.46*/._3)),format.raw/*191.49*/("""
                                        </div>
                                        <ul class="list-group space-panel-footer">
                                        """),_display_(Seq[Any](/*194.42*/user/*194.46*/ match/*194.52*/ {/*195.45*/case Some(viewer) =>/*195.65*/ {_display_(Seq[Any](format.raw/*195.67*/("""
                                                """),_display_(Seq[Any](/*196.50*/ownProfile/*196.60*/ match/*196.66*/ {/*197.53*/case Some(sameProfile) =>/*197.78*/ {_display_(Seq[Any](format.raw/*197.80*/("""

                                                            <button
                                                            id="followButton"
                                                            type="button"
                                                            class="
                                                                """),_display_(Seq[Any](/*203.66*/if(viewer.followedEntities.filter(x => (x.id == info._1)).nonEmpty)/*203.133*/ {_display_(Seq[Any](format.raw/*203.135*/("""
                                                                    btn btn-link
                                                                """)))}/*205.67*/else/*205.72*/{_display_(Seq[Any](format.raw/*205.73*/("""
                                                                    btn btn-link
                                                                """)))})),format.raw/*207.66*/("""
                                                            "
                                                            data-toggle="button"
                                                            aria-pressed="
                                                                """),_display_(Seq[Any](/*211.66*/if(viewer.followedEntities.filter(x => (x.id == info._1)).nonEmpty)/*211.133*/ {_display_(Seq[Any](format.raw/*211.135*/("""
                                                                    true
                                                                """)))}/*213.67*/else/*213.72*/{_display_(Seq[Any](format.raw/*213.73*/("""
                                                                    false
                                                                """)))})),format.raw/*215.66*/("""
                                                            "
                                                            autocomplete="off"
                                                            objectType="space"
                                                            objectId=""""),_display_(Seq[Any](/*219.72*/info/*219.76*/._1.stringify)),format.raw/*219.89*/(""""
                                                            >
                                                            """),_display_(Seq[Any](/*221.62*/if(viewer.followedEntities.filter(x => (x.id == info._1)).nonEmpty)/*221.129*/ {_display_(Seq[Any](format.raw/*221.131*/("""
                                                                <span class="glyphicon glyphicon-star-empty"></span> Unfollow
                                                            """)))}/*223.63*/else/*223.68*/{_display_(Seq[Any](format.raw/*223.69*/("""
                                                                <span class="glyphicon glyphicon-star"></span> Follow
                                                            """)))})),format.raw/*225.62*/("""
                                                            </button>

                                                    """)))}/*229.53*/case None =>/*229.65*/ {_display_(Seq[Any](format.raw/*229.67*/("""
                                                    """)))}})),format.raw/*231.50*/("""
                                            """)))}/*233.45*/case None =>/*233.57*/ {}})),format.raw/*234.42*/("""
                                        </ul>
                                    </div>
                                </div>
                            """)))})),format.raw/*238.30*/("""
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Files</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*251.77*/routes/*251.83*/.Files.followingFiles(0, 12, ""))),format.raw/*251.115*/("""">See More</a></span>
                                    </div>
                                </div>

                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                            """),_display_(Seq[Any](/*259.30*/for(fileInfo <- followedFiles) yield /*259.60*/ {_display_(Seq[Any](format.raw/*259.62*/("""
                                <div class="col-xs-3" style="margin-top: 30px" id=""""),_display_(Seq[Any](/*260.85*/fileInfo/*260.93*/._1)),format.raw/*260.96*/("""-tile">
                                    <div class="panel panel-default file-panel">
                                        <div class="pull-left">
                                            <span class="glyphicon glyphicon-file"></span>
                                        </div>
                                        <div class="panel-body caption break-word">
                                            <h4><a href=""""),_display_(Seq[Any](/*266.59*/routes/*266.65*/.Files.file(fileInfo._1))),format.raw/*266.89*/("""">"""),_display_(Seq[Any](/*266.92*/fileInfo/*266.100*/._2.toString)),format.raw/*266.112*/("""</a></h4>
                                        """),_display_(Seq[Any](/*267.42*/fileInfo/*267.50*/._3)),format.raw/*267.53*/("""
                                        </div>
                                        <ul class="list-group file-panel-footer">
                                        """),_display_(Seq[Any](/*270.42*/user/*270.46*/ match/*270.52*/ {/*271.45*/case Some(viewer) =>/*271.65*/ {_display_(Seq[Any](format.raw/*271.67*/("""
                                                """),_display_(Seq[Any](/*272.50*/ownProfile/*272.60*/ match/*272.66*/ {/*273.53*/case Some(sameProfile) =>/*273.78*/ {_display_(Seq[Any](format.raw/*273.80*/("""

                                                            <button
                                                            id="followButton"
                                                            type="button"
                                                            class="
                                                                """),_display_(Seq[Any](/*279.66*/if(viewer.followedEntities.filter(x => (x.id == fileInfo._1)).nonEmpty)/*279.137*/ {_display_(Seq[Any](format.raw/*279.139*/("""
                                                                    btn btn-link
                                                                """)))}/*281.66*/else/*281.71*/{_display_(Seq[Any](format.raw/*281.72*/("""
                                                                    btn btn-link
                                                                """)))})),format.raw/*283.66*/("""
                                                            "
                                                            data-toggle="button"
                                                            aria-pressed="
                                                                """),_display_(Seq[Any](/*287.66*/if(viewer.followedEntities.filter(x => (x.id == fileInfo._1)).nonEmpty)/*287.137*/ {_display_(Seq[Any](format.raw/*287.139*/("""
                                                                    true
                                                                """)))}/*289.66*/else/*289.71*/{_display_(Seq[Any](format.raw/*289.72*/("""
                                                                    false
                                                                """)))})),format.raw/*291.66*/("""
                                                            "
                                                            autocomplete="off"
                                                            objectId = """"),_display_(Seq[Any](/*294.74*/fileInfo/*294.82*/._1.stringify)),format.raw/*294.95*/(""""
                                                            objectType = "file"
                                                            >
                                                            """),_display_(Seq[Any](/*297.62*/if(viewer.followedEntities.filter(x => (x.id == fileInfo._1)).nonEmpty)/*297.133*/ {_display_(Seq[Any](format.raw/*297.135*/("""
                                                                <span class="glyphicon glyphicon-star-empty"></span> Unfollow
                                                            """)))}/*299.63*/else/*299.68*/{_display_(Seq[Any](format.raw/*299.69*/("""
                                                                <span class="glyphicon glyphicon-star"></span> Follow
                                                            """)))})),format.raw/*301.62*/("""
                                                            </button>

                                                    """)))}/*305.53*/case None =>/*305.65*/ {_display_(Seq[Any](format.raw/*305.67*/("""
                                                    """)))}})),format.raw/*307.50*/("""
                                            """)))}/*309.45*/case None =>/*309.57*/ {}})),format.raw/*310.42*/("""
                                        </ul>
                                    </div>
                                </div>
                            """)))})),format.raw/*314.30*/("""
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Datasets</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*326.77*/routes/*326.83*/.Datasets.followingDatasets(0, 12, ""))),format.raw/*326.121*/("""">See More</a></span>
                                    </div>
                                </div>

                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                            """),_display_(Seq[Any](/*334.30*/for(datasetInfo <- followedDatasets) yield /*334.66*/ {_display_(Seq[Any](format.raw/*334.68*/("""
                                <div class="col-xs-3" style="margin-top: 30px" id=""""),_display_(Seq[Any](/*335.85*/datasetInfo/*335.96*/._1)),format.raw/*335.99*/("""-tile">
                                    <div class="panel panel-default dataset-panel">
                                        <div class="pull-left">
                                            <span class="glyphicon glyphicon-briefcase"></span>
                                        </div>
                                        <div class="panel-body caption break-word">
                                            <h4> <a href=""""),_display_(Seq[Any](/*341.60*/routes/*341.66*/.Datasets.dataset(datasetInfo._1))),format.raw/*341.99*/("""">"""),_display_(Seq[Any](/*341.102*/datasetInfo/*341.113*/._2.toString)),format.raw/*341.125*/("""</a></h4>
                                        """),_display_(Seq[Any](/*342.42*/datasetInfo/*342.53*/._3)),format.raw/*342.56*/("""
                                        </div>
                                        <ul class="list-group dataset-panel-footer">
                                        """),_display_(Seq[Any](/*345.42*/user/*345.46*/ match/*345.52*/ {/*346.45*/case Some(viewer) =>/*346.65*/ {_display_(Seq[Any](format.raw/*346.67*/("""
                                                """),_display_(Seq[Any](/*347.50*/ownProfile/*347.60*/ match/*347.66*/ {/*348.53*/case Some(sameProfile) =>/*348.78*/ {_display_(Seq[Any](format.raw/*348.80*/("""

                                                            <button
                                                            id="followButton"
                                                            type="button"
                                                            class="
                                                                """),_display_(Seq[Any](/*354.66*/if(viewer.followedEntities.filter(x => (x.id == datasetInfo._1)).nonEmpty)/*354.140*/ {_display_(Seq[Any](format.raw/*354.142*/("""
                                                                    btn btn-link
                                                                """)))}/*356.66*/else/*356.71*/{_display_(Seq[Any](format.raw/*356.72*/("""
                                                                    btn btn-link
                                                                """)))})),format.raw/*358.66*/("""
                                                            "
                                                            data-toggle="button"
                                                            aria-pressed="
                                                                """),_display_(Seq[Any](/*362.66*/if(viewer.followedEntities.filter(x => (x.id == datasetInfo._1)).nonEmpty)/*362.140*/ {_display_(Seq[Any](format.raw/*362.142*/("""
                                                                    true
                                                                """)))}/*364.66*/else/*364.71*/{_display_(Seq[Any](format.raw/*364.72*/("""
                                                                    false
                                                                """)))})),format.raw/*366.66*/("""
                                                            "
                                                            autocomplete="off"
                                                            objectType="dataset"
                                                            objectId=""""),_display_(Seq[Any](/*370.72*/datasetInfo/*370.83*/._1.stringify)),format.raw/*370.96*/(""""
                                                            >
                                                            """),_display_(Seq[Any](/*372.62*/if(viewer.followedEntities.filter(x => (x.id == datasetInfo._1)).nonEmpty)/*372.136*/ {_display_(Seq[Any](format.raw/*372.138*/("""
                                                                <span class="glyphicon glyphicon-star-empty"></span> Unfollow
                                                            """)))}/*374.63*/else/*374.68*/{_display_(Seq[Any](format.raw/*374.69*/("""
                                                                <span class="glyphicon glyphicon-star"></span> Follow
                                                            """)))})),format.raw/*376.62*/("""
                                                            </button>

                                                    """)))}/*380.53*/case None =>/*380.65*/ {_display_(Seq[Any](format.raw/*380.67*/("""
                                                    """)))}})),format.raw/*382.50*/("""
                                            """)))}/*384.45*/case None =>/*384.57*/ {}})),format.raw/*385.42*/("""
                                        </ul>
                                    </div>
                                </div>
                            """)))})),format.raw/*389.30*/("""
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Collections</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*401.77*/routes/*401.83*/.Collections.followingCollections(0, 12, ""))),format.raw/*401.127*/("""">See More</a></span>
                                    </div>
                                </div>

                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                            """),_display_(Seq[Any](/*409.30*/for(collectionInfo <- followedCollections) yield /*409.72*/ {_display_(Seq[Any](format.raw/*409.74*/("""
                                <div class="col-xs-3" style="margin-top: 30px" id=""""),_display_(Seq[Any](/*410.85*/collectionInfo/*410.99*/._1)),format.raw/*410.102*/("""-tile">
                                    <div class="panel panel-default collection-panel">
                                        <div class="pull-left">
                                            <span class="glyphicon glyphicon-th-large"></span>
                                        </div>
                                        <div class="panel-body caption break-word">
                                            <h4><a href=""""),_display_(Seq[Any](/*416.59*/routes/*416.65*/.Collections.collection(collectionInfo._1))),format.raw/*416.107*/("""">"""),_display_(Seq[Any](/*416.110*/collectionInfo/*416.124*/._2.toString)),format.raw/*416.136*/("""</a></h4>
                                        """),_display_(Seq[Any](/*417.42*/collectionInfo/*417.56*/._3)),format.raw/*417.59*/("""
                                        </div>
                                        <ul class="list-group collection-panel-footer">
                                        """),_display_(Seq[Any](/*420.42*/user/*420.46*/ match/*420.52*/ {/*421.45*/case Some(viewer) =>/*421.65*/ {_display_(Seq[Any](format.raw/*421.67*/("""
                                                """),_display_(Seq[Any](/*422.50*/ownProfile/*422.60*/ match/*422.66*/ {/*423.53*/case Some(sameProfile) =>/*423.78*/ {_display_(Seq[Any](format.raw/*423.80*/("""

                                                            <button
                                                            id="followButton"
                                                            type="button"
                                                            class="
                                                                """),_display_(Seq[Any](/*429.66*/if(viewer.followedEntities.filter(x => (x.id == collectionInfo._1)).nonEmpty)/*429.143*/ {_display_(Seq[Any](format.raw/*429.145*/("""
                                                                    btn btn-link
                                                                """)))}/*431.66*/else/*431.71*/{_display_(Seq[Any](format.raw/*431.72*/("""
                                                                    btn btn-link
                                                                """)))})),format.raw/*433.66*/("""
                                                            "
                                                            data-toggle="button"
                                                            aria-pressed="
                                                                """),_display_(Seq[Any](/*437.66*/if(viewer.followedEntities.filter(x => (x.id == collectionInfo._1)).nonEmpty)/*437.143*/ {_display_(Seq[Any](format.raw/*437.145*/("""
                                                                    true
                                                                """)))}/*439.66*/else/*439.71*/{_display_(Seq[Any](format.raw/*439.72*/("""
                                                                    false
                                                                """)))})),format.raw/*441.66*/("""
                                                            "
                                                            autocomplete="off"
                                                            objectType="collection"
                                                            objectId=""""),_display_(Seq[Any](/*445.72*/collectionInfo/*445.86*/._1.stringify)),format.raw/*445.99*/(""""
                                                            >
                                                            """),_display_(Seq[Any](/*447.62*/if(viewer.followedEntities.filter(x => (x.id == collectionInfo._1)).nonEmpty)/*447.139*/ {_display_(Seq[Any](format.raw/*447.141*/("""
                                                                <span class="glyphicon glyphicon-star-empty"></span> Unfollow
                                                            """)))}/*449.63*/else/*449.68*/{_display_(Seq[Any](format.raw/*449.69*/("""
                                                                <span class="glyphicon glyphicon-star"></span> Follow
                                                            """)))})),format.raw/*451.62*/("""
                                                            </button>

                                                    """)))}/*455.53*/case None =>/*455.65*/ {_display_(Seq[Any](format.raw/*455.67*/("""
                                                    """)))}})),format.raw/*457.50*/("""
                                            """)))}/*459.45*/case None =>/*459.57*/ {}})),format.raw/*460.42*/("""
                                        </ul>
                                    </div>
                                </div>
                            """)))})),format.raw/*464.30*/("""

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script src=""""),_display_(Seq[Any](/*475.19*/routes/*475.25*/.Assets.at("javascripts/lib/masonry.pkgd.min.js"))),format.raw/*475.74*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*476.19*/routes/*476.25*/.Assets.at("javascripts/lib/imagesloaded.pkgd.min.js"))),format.raw/*476.79*/("""" type="text/javascript"></script>

    <script>
        var removeIndicator=true;
        function activateOne(id) """),format.raw/*480.34*/("""{"""),format.raw/*480.35*/("""
            // initialize Masonry
            var $container = $('#'+id).masonry();
            // layout Masonry again after all images have loaded
            imagesLoaded( '#masonry', function() """),format.raw/*484.50*/("""{"""),format.raw/*484.51*/("""
                $container.masonry("""),format.raw/*485.36*/("""{"""),format.raw/*485.37*/("""
                itemSelector: '.post-box',
                columnWidth: '.post-box',
                transitionDuration: 4
                """),format.raw/*489.17*/("""}"""),format.raw/*489.18*/(""");
            """),format.raw/*490.13*/("""}"""),format.raw/*490.14*/(""");
        """),format.raw/*491.9*/("""}"""),format.raw/*491.10*/("""

        function activate()"""),format.raw/*493.28*/("""{"""),format.raw/*493.29*/("""
            activateOne("masonry-datasets");
            activateOne("masonry-collections");
            activateOne("masonry-spaces");
        """),format.raw/*497.9*/("""}"""),format.raw/*497.10*/("""

        $(document).ready(function() """),format.raw/*499.38*/("""{"""),format.raw/*499.39*/("""
            activate();
            $('.nav-tabs').stickyTabs();
        """),format.raw/*502.9*/("""}"""),format.raw/*502.10*/(""");

        // fire when showing from tab
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) """),format.raw/*505.67*/("""{"""),format.raw/*505.68*/("""
            activate();
        """),format.raw/*507.9*/("""}"""),format.raw/*507.10*/(""")

        var eventCount = 3;
        function moreEvents()"""),format.raw/*510.30*/("""{"""),format.raw/*510.31*/("""
            var request = jsRoutes.controllers.Events.getEvents(eventCount).ajax("""),format.raw/*511.82*/("""{"""),format.raw/*511.83*/("""
                type: 'GET'
            """),format.raw/*513.13*/("""}"""),format.raw/*513.14*/(""");

            request.done(function (response, textStatus, jqXHR) """),format.raw/*515.65*/("""{"""),format.raw/*515.66*/("""
                eventCount = eventCount + 1;
                $("#moreevent").append(response);
            """),format.raw/*518.13*/("""}"""),format.raw/*518.14*/(""");

            request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*520.67*/("""{"""),format.raw/*520.68*/("""
                console.error("The following error occured: " + textStatus, errorThrown);
                notify("Could not get moe events: " + errorThrown, "error");
            """),format.raw/*523.13*/("""}"""),format.raw/*523.14*/(""");
        """),format.raw/*524.9*/("""}"""),format.raw/*524.10*/("""
    </script>
    <script src=""""),_display_(Seq[Any](/*526.19*/routes/*526.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*526.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*527.19*/routes/*527.25*/.Assets.at("javascripts/select.js"))),format.raw/*527.60*/("""" type="text/javascript"></script>
    """),format.raw/*532.14*/("""
""")))})),format.raw/*533.2*/("""
"""))}
    }
    
    def render(displayedName:String,newsfeed:List[models.Event],profile:User,datasetsList:List[Dataset],collectionsList:List[Collection],spacesList:List[ProjectSpace],deletePermission:Boolean,followers:List[scala.Tuple4[UUID, String, String, String]],followedUsers:List[scala.Tuple4[UUID, String, String, String]],followedFiles:List[scala.Tuple3[UUID, String, String]],followedDatasets:List[scala.Tuple3[UUID, String, String]],followedCollections:List[scala.Tuple3[UUID, String, String]],followedSpaces:List[scala.Tuple3[UUID, String, String]],ownProfile:Option[Boolean],userSelections:List[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(displayedName,newsfeed,profile,datasetsList,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections)(user)
    
    def f:((String,List[models.Event],User,List[Dataset],List[Collection],List[ProjectSpace],Boolean,List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],Option[Boolean],List[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (displayedName,newsfeed,profile,datasetsList,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections) => (user) => apply(displayedName,newsfeed,profile,datasetsList,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:46:59 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/t2c2dashboard.scala.html
                    HASH: d2ff75c661b0216d503b2c85c372b67d2fba09c4
                    MATRIX: 989->2|1716->604|1744->637|1780->639|1807->658|1846->660|1901->679|1916->685|1988->735|2077->788|2092->794|2161->841|2250->894|2265->900|2332->945|2421->998|2436->1004|2498->1044|2587->1097|2602->1103|2681->1160|2788->1231|2804->1238|2835->1247|2936->1312|2951->1318|3009->1354|3187->1496|3202->1502|3242->1520|3322->1564|3368->1587|3469->1652|3541->1701|3604->1728|3619->1734|3673->1766|3824->1881|3898->1932|3961->1959|3976->1965|4030->1997|4184->2115|4261->2169|4671->2542|4685->2546|4780->2618|4945->2746|4959->2750|5056->2824|5227->2958|5241->2962|5341->3039|5737->3399|5779->3419|6318->3942|6473->4061|6551->4117|6820->4350|6835->4356|6911->4410|7115->4578|7134->4588|7147->4592|7196->4603|7254->4625|7269->4631|7350->4690|7400->4708|7477->4749|7509->4772|7549->4774|7662->4851|7740->4907|7842->4973|7857->4979|7897->4997|7957->5020|8003->5043|8043->5046|8115->5095|8202->5150|8331->5265|8486->5384|8620->5495|8862->5701|8877->5707|8968->5775|9172->5968|9327->6087|9470->6207|9740->6440|9756->6446|9845->6511|10056->6685|10081->6700|10095->6704|10150->6720|10209->6742|10230->6753|10324->6823|10375->6841|10453->6882|10491->6910|10532->6912|10646->6989|10743->7062|10846->7128|10862->7134|10917->7166|10978->7189|11030->7217|11071->7220|11149->7274|11237->7329|11677->7732|11693->7738|11737->7759|11902->7887|11940->7908|11981->7910|12044->7936|12059->7941|12111->7970|12166->7992|12225->8014|12257->8036|12298->8038|12525->8227|12542->8233|12602->8269|12698->8332|13444->9041|13460->9047|13505->9068|13973->9499|14020->9529|14061->9531|14136->9569|14151->9574|14207->9607|14274->9641|14829->10158|14877->10182|15097->10365|15113->10371|15171->10405|15542->10739|15586->10766|15627->10768|15751->10855|15765->10859|15791->10862|16261->11295|16277->11301|16325->11326|16365->11329|16379->11333|16415->11345|16503->11396|16517->11400|16543->11403|16752->11575|16766->11579|16782->11585|16794->11632|16824->11652|16865->11654|16952->11704|16972->11714|16988->11720|17000->11775|17035->11800|17076->11802|17468->12157|17546->12224|17588->12226|17755->12374|17769->12379|17809->12380|17989->12527|18310->12811|18388->12878|18430->12880|18589->13020|18603->13025|18643->13026|18816->13166|19145->13458|19159->13462|19195->13475|19357->13600|19435->13667|19477->13669|19685->13858|19699->13863|19739->13864|19952->14044|20097->14222|20119->14234|20160->14236|20248->14340|20314->14431|20336->14443|20363->14488|20554->14646|21253->15308|21269->15314|21325->15346|21693->15677|21740->15707|21781->15709|21903->15794|21921->15802|21947->15805|22417->16238|22433->16244|22480->16268|22520->16271|22539->16279|22575->16291|22663->16342|22681->16350|22707->16353|22915->16524|22929->16528|22945->16534|22957->16581|22987->16601|23028->16603|23115->16653|23135->16663|23151->16669|23163->16724|23198->16749|23239->16751|23631->17106|23713->17177|23755->17179|23922->17326|23936->17331|23976->17332|24156->17479|24477->17763|24559->17834|24601->17836|24760->17975|24774->17980|24814->17981|24987->18121|25239->18336|25257->18344|25293->18357|25535->18562|25617->18633|25659->18635|25867->18824|25881->18829|25921->18830|26134->19010|26279->19188|26301->19200|26342->19202|26430->19306|26496->19397|26518->19409|26545->19454|26736->19612|27439->20278|27455->20284|27517->20322|27887->20655|27940->20691|27981->20693|28103->20778|28124->20789|28150->20792|28629->21234|28645->21240|28701->21273|28742->21276|28764->21287|28800->21299|28888->21350|28909->21361|28935->21364|29146->21538|29160->21542|29176->21548|29188->21595|29218->21615|29259->21617|29346->21667|29366->21677|29382->21683|29394->21738|29429->21763|29470->21765|29862->22120|29947->22194|29989->22196|30156->22343|30170->22348|30210->22349|30390->22496|30711->22780|30796->22854|30838->22856|30997->22995|31011->23000|31051->23001|31224->23141|31555->23435|31576->23446|31612->23459|31774->23584|31859->23658|31901->23660|32109->23849|32123->23854|32163->23855|32376->24035|32521->24213|32543->24225|32584->24227|32672->24331|32738->24422|32760->24434|32787->24479|32978->24637|33683->25305|33699->25311|33767->25355|34136->25687|34195->25729|34236->25731|34358->25816|34382->25830|34409->25833|34889->26276|34905->26282|34971->26324|35012->26327|35037->26341|35073->26353|35161->26404|35185->26418|35211->26421|35425->26598|35439->26602|35455->26608|35467->26655|35497->26675|35538->26677|35625->26727|35645->26737|35661->26743|35673->26798|35708->26823|35749->26825|36141->27180|36229->27257|36271->27259|36438->27406|36452->27411|36492->27412|36672->27559|36993->27843|37081->27920|37123->27922|37282->28061|37296->28066|37336->28067|37509->28207|37843->28504|37867->28518|37903->28531|38065->28656|38153->28733|38195->28735|38403->28924|38417->28929|38457->28930|38670->29110|38815->29288|38837->29300|38878->29302|38966->29406|39032->29497|39054->29509|39081->29554|39272->29712|39492->29895|39508->29901|39580->29950|39670->30003|39686->30009|39763->30063|39908->30179|39938->30180|40166->30379|40196->30380|40261->30416|40291->30417|40460->30557|40490->30558|40534->30573|40564->30574|40603->30585|40633->30586|40691->30615|40721->30616|40894->30761|40924->30762|40992->30801|41022->30802|41124->30876|41154->30877|41291->30985|41321->30986|41382->31019|41412->31020|41501->31080|41531->31081|41642->31163|41672->31164|41742->31205|41772->31206|41869->31274|41899->31275|42036->31383|42066->31384|42165->31454|42195->31455|42404->31635|42434->31636|42473->31647|42503->31648|42573->31681|42589->31687|42654->31729|42744->31782|42760->31788|42818->31823|42886->31943|42920->31945
                    LINES: 20->2|27->5|29->8|30->9|30->9|30->9|31->10|31->10|31->10|32->11|32->11|32->11|33->12|33->12|33->12|34->13|34->13|34->13|35->14|35->14|35->14|38->17|38->17|38->17|40->19|40->19|40->19|42->21|42->21|42->21|42->21|42->21|43->22|43->22|44->23|44->23|44->23|45->24|45->24|46->25|46->25|46->25|47->26|47->26|53->32|53->32|53->32|54->33|54->33|54->33|55->34|55->34|55->34|62->41|62->41|73->52|76->55|76->55|82->61|82->61|82->61|87->66|87->66|87->66|87->66|88->67|88->67|88->67|89->68|91->70|91->70|91->70|93->72|93->72|95->74|95->74|95->74|95->74|95->74|95->74|95->74|97->76|100->79|103->82|103->82|108->87|108->87|108->87|114->93|117->96|117->96|123->102|123->102|123->102|128->107|128->107|128->107|128->107|129->108|129->108|129->108|130->109|132->111|132->111|132->111|134->113|134->113|136->115|136->115|136->115|136->115|136->115|136->115|136->115|138->117|146->125|146->125|146->125|151->130|151->130|151->130|152->131|152->131|152->131|153->132|154->133|154->133|154->133|156->135|156->135|156->135|158->137|171->150|171->150|171->150|180->159|180->159|180->159|181->160|181->160|181->160|182->161|193->172|193->172|196->175|196->175|196->175|204->183|204->183|204->183|205->184|205->184|205->184|211->190|211->190|211->190|211->190|211->190|211->190|212->191|212->191|212->191|215->194|215->194|215->194|215->195|215->195|215->195|216->196|216->196|216->196|216->197|216->197|216->197|222->203|222->203|222->203|224->205|224->205|224->205|226->207|230->211|230->211|230->211|232->213|232->213|232->213|234->215|238->219|238->219|238->219|240->221|240->221|240->221|242->223|242->223|242->223|244->225|247->229|247->229|247->229|248->231|249->233|249->233|249->234|253->238|266->251|266->251|266->251|274->259|274->259|274->259|275->260|275->260|275->260|281->266|281->266|281->266|281->266|281->266|281->266|282->267|282->267|282->267|285->270|285->270|285->270|285->271|285->271|285->271|286->272|286->272|286->272|286->273|286->273|286->273|292->279|292->279|292->279|294->281|294->281|294->281|296->283|300->287|300->287|300->287|302->289|302->289|302->289|304->291|307->294|307->294|307->294|310->297|310->297|310->297|312->299|312->299|312->299|314->301|317->305|317->305|317->305|318->307|319->309|319->309|319->310|323->314|335->326|335->326|335->326|343->334|343->334|343->334|344->335|344->335|344->335|350->341|350->341|350->341|350->341|350->341|350->341|351->342|351->342|351->342|354->345|354->345|354->345|354->346|354->346|354->346|355->347|355->347|355->347|355->348|355->348|355->348|361->354|361->354|361->354|363->356|363->356|363->356|365->358|369->362|369->362|369->362|371->364|371->364|371->364|373->366|377->370|377->370|377->370|379->372|379->372|379->372|381->374|381->374|381->374|383->376|386->380|386->380|386->380|387->382|388->384|388->384|388->385|392->389|404->401|404->401|404->401|412->409|412->409|412->409|413->410|413->410|413->410|419->416|419->416|419->416|419->416|419->416|419->416|420->417|420->417|420->417|423->420|423->420|423->420|423->421|423->421|423->421|424->422|424->422|424->422|424->423|424->423|424->423|430->429|430->429|430->429|432->431|432->431|432->431|434->433|438->437|438->437|438->437|440->439|440->439|440->439|442->441|446->445|446->445|446->445|448->447|448->447|448->447|450->449|450->449|450->449|452->451|455->455|455->455|455->455|456->457|457->459|457->459|457->460|461->464|472->475|472->475|472->475|473->476|473->476|473->476|477->480|477->480|481->484|481->484|482->485|482->485|486->489|486->489|487->490|487->490|488->491|488->491|490->493|490->493|494->497|494->497|496->499|496->499|499->502|499->502|502->505|502->505|504->507|504->507|507->510|507->510|508->511|508->511|510->513|510->513|512->515|512->515|515->518|515->518|517->520|517->520|520->523|520->523|521->524|521->524|523->526|523->526|523->526|524->527|524->527|524->527|525->532|526->533
                    -- GENERATED --
                */
            