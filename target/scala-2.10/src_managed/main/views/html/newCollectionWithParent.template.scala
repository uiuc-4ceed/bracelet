
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object newCollectionWithParent extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template8[String,Boolean,Boolean,Option[String],Option[String],Option[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(errorString: String, isNameRequired: Boolean, isDescriptionRequired: Boolean, spaceId: Option[String], parentId : Option[String], parentName : Option[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import play.api.i18n.Messages

import _root_.util.Formatters._

implicit def /*7.2*/implicitFieldConstructor/*7.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.224*/("""

"""),format.raw/*6.1*/("""
"""),format.raw/*7.75*/("""

"""),_display_(Seq[Any](/*9.2*/main(Messages("collection.title"))/*9.36*/ {_display_(Seq[Any](format.raw/*9.38*/("""

<style>
        .row """),format.raw/*12.14*/("""{"""),format.raw/*12.15*/("""
            margin-left : 0px;
            margin-right : 0px;
        """),format.raw/*15.9*/("""}"""),format.raw/*15.10*/("""
    
        .create-row """),format.raw/*17.21*/("""{"""),format.raw/*17.22*/("""
            margin-left: -15px !important;
        """),format.raw/*19.9*/("""}"""),format.raw/*19.10*/("""
    
        .column-create """),format.raw/*21.24*/("""{"""),format.raw/*21.25*/("""
            padding-left: 0px !important;
        """),format.raw/*23.9*/("""}"""),format.raw/*23.10*/("""

        .start """),format.raw/*25.16*/("""{"""),format.raw/*25.17*/("""
            margin-left: 0px !important;
        """),format.raw/*27.9*/("""}"""),format.raw/*27.10*/("""

        @media only screen and (max-width: 1000px) """),format.raw/*29.53*/("""{"""),format.raw/*29.54*/("""

        """),format.raw/*31.9*/("""}"""),format.raw/*31.10*/("""
        </style>
  <!-- Custom items for the create collection workflow -->
  <script src=""""),_display_(Seq[Any](/*34.17*/routes/*34.23*/.Assets.at("javascripts/collection-create.js"))),format.raw/*34.69*/("""" type="text/javascript"></script>
  <script src=""""),_display_(Seq[Any](/*35.17*/routes/*35.23*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*35.68*/("""" language="javascript"></script>
  <script type="text/javascript" language="javascript">
      //Global so that the javascript for the collection creation can reference this.
      var isNameRequired = """),_display_(Seq[Any](/*38.29*/isNameRequired)),format.raw/*38.43*/(""";
      var isDescRequired = """),_display_(Seq[Any](/*39.29*/isDescriptionRequired)),format.raw/*39.50*/(""";
      var parentCollectionId = """"),_display_(Seq[Any](/*40.34*/parentId)),format.raw/*40.42*/("""";
  </script>
    <script src=""""),_display_(Seq[Any](/*42.19*/routes/*42.25*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*42.67*/("""" type="text/javascript"></script>
    <link rel="stylesheet" href=""""),_display_(Seq[Any](/*43.35*/routes/*43.41*/.Assets.at("stylesheets/chosen.css"))),format.raw/*43.77*/("""">
    <ol class="breadcrumb navigate-bread">
        """),_display_(Seq[Any](/*45.10*/(parentId, parentName)/*45.32*/ match/*45.38*/ {/*46.13*/case (Some(id), Some(name)) =>/*46.43*/ {_display_(Seq[Any](format.raw/*46.45*/("""
                <li>  <span class="glyphicon glyphicon-th-large"></span> <a href=""""),_display_(Seq[Any](/*47.84*/routes/*47.90*/.Collections.collection(UUID(id)))),format.raw/*47.123*/("""" title=""""),_display_(Seq[Any](/*47.133*/name)),format.raw/*47.137*/(""""> """),_display_(Seq[Any](/*47.141*/Html(ellipsize(name, 18)))),format.raw/*47.166*/("""</a></li>
                <li><span class="glyphicon glyphicon-th-large"></span> Create Child Collection</li>
            """)))}/*50.13*/case (_, _) =>/*50.27*/ {}})),format.raw/*51.10*/("""
    </ol>
  <div class="page-header">
    <h1>Create Child Collection in """),_display_(Seq[Any](/*54.37*/parentName)),format.raw/*54.47*/(""" </h1>
  </div>
  <div class="row">
	<div class="col-md-12 column-create">
	
	<div>
        <span id="status" class="success hiddencomplete alert alert-success" role="alert">A Status Message</span>     
        """),_display_(Seq[Any](/*61.10*/if(errorString != null)/*61.33*/{_display_(Seq[Any](format.raw/*61.34*/("""
            <span class="error alert alert-danger" id="messageerror">"""),_display_(Seq[Any](/*62.71*/errorString)),format.raw/*62.82*/("""</span>
        """)))}/*63.10*/else/*63.14*/{_display_(Seq[Any](format.raw/*63.15*/("""           
            <span class="error hiddencomplete alert alert-danger" id="messageerror">An Error Message</span>
        """)))})),format.raw/*65.10*/("""
    </div>
    <!-- Basic required elements for creating a new collection. -->
    <!-- <table style="width: 100%; margin-bottom: 40px; margin-top: 40px;">
        <tr>
            <td style="width: 20%; vertical-align:top;" class="input-table-cell">Name:</td>
            <td style="width: 80%; vertical-align:top;" class="input-table-cell">
                <textarea class="form-control" cols=40 rows=4 type="text" id="name"></textarea>
                <span class="error hiddencomplete" id="nameerror"> The name is a required field</span><br>
            </td>
            <td style="vertical-align:top;" class="input-table-cell">

            </td>
        </tr>
        <tr>
            <td style="width: 20%; vertical-align:top;" class="input-table-cell">Description:</td>
            <td style="width: 80%; vertical-align:top;" class="input-table-cell" id="desccell">
                <textarea class="form-control" cols=40 rows=4 type="text" id="description"></textarea>
                <span class="error hiddencomplete" id="descerror"> This description is a required field</span><br>
            </td>
        </tr>
        <tr>
            <td>	                            
            </td>
            <td>
                <button style="margin-top: 10px; margin-right: 10px;" class="btn btn-primary" title="Create the collection" onclick="return createCollection();">
                  <span class="glyphicon glyphicon-ok"></span> Create
                </button>
                <button style="margin-top: 10px;" class="btn btn-default" title="Clear the input fields" onclick="return clearFields();">
                  <span class="glyphicon glyphicon-unchecked"></span> Reset
                </button>
            </td>
        </tr>
    </table>	 -->

    <div class="form-group">
            <label id="namelabel" for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="A short name">
            <span class="error hiddencomplete" id="nameerror">The name is a required field</span>
    </div>
    <div class="form-group">
            <label id="desclabel" for="description">Description</label>
            <textarea cols=40 rows=4 type="text" id="description" class="form-control"
                placeholder="A longer description"></textarea>
            <span class="error hiddencomplete" id="descerror">This description is a required field</span>
    </div>
    
    <div class="row create-row visible-lg">
            <div class="col-md-12 create-col">
                <button style="margin-left: 10px;" type="submit" class="btn btn-primary start" title="Create the collection" onclick="return createCollection();">
                    <span class="glyphicon glyphicon-ok"></span> Create
                </button>
                <button class="btn btn-default" title="Clear the input fields" onclick="return clearFields();">
                    <span class="glyphicon glyphicon-unchecked"></span> Reset
                </button>
            </div>
        </div>
        <div class="row create-row visible-xs visible sm">
            <div class="col-md-12 create-col">
                <button style="margin-left: 10px;" type="submit" class="btn btn-primary start" title="Create the collection" onclick="return createCollectionDiffRedirect();">
                    <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*125.67*/Messages("create.title", ""))),format.raw/*125.95*/("""
                </button>
                <button class="btn btn-default" title="Clear the input fields" onclick="return clearFields();">
                    <span class="glyphicon glyphicon-unchecked"></span> Reset
                </button>
            </div>
        </div>
    <form id="collectioncreate" action='"""),_display_(Seq[Any](/*132.42*/routes/*132.48*/.Collections.submit)),format.raw/*132.67*/("""' method="POST" enctype="multipart/form-data">
                    <!-- Redirect browsers with JavaScript disabled to the origin page -->
                    <noscript>Javascript is required in order to use the uploader to create a new dataset.</noscript>
                    
                    <input type="hidden" name="name" id="hiddenname" value="not set">
                    <input type="hidden" name="description" id="hiddendescription" value="not set">
                    <input type="hidden" name="space" id="hiddenspace" value="not set">
                    <input type="hidden" name = "parentcolid" id = "hiddenparentcolid" value = """"),_display_(Seq[Any](/*139.98*/parentId)),format.raw/*139.106*/("""">
                    <input type="hidden" name="redirect" id="hiddenredirect" value="not set">
    </form>
    </div>
  </div>
<script language="javascript">
    $(".chosen-select").chosen("""),format.raw/*145.32*/("""{"""),format.raw/*145.33*/("""
        width: "100%",
        placeholder_text_multiple: "Select spaces for this collection."
    """),format.raw/*148.5*/("""}"""),format.raw/*148.6*/(""");
</script>

""")))})),format.raw/*151.2*/("""
"""))}
    }
    
    def render(errorString:String,isNameRequired:Boolean,isDescriptionRequired:Boolean,spaceId:Option[String],parentId:Option[String],parentName:Option[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(errorString,isNameRequired,isDescriptionRequired,spaceId,parentId,parentName)(flash,user)
    
    def f:((String,Boolean,Boolean,Option[String],Option[String],Option[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (errorString,isNameRequired,isDescriptionRequired,spaceId,parentId,parentName) => (flash,user) => apply(errorString,isNameRequired,isDescriptionRequired,spaceId,parentId,parentName)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/newCollectionWithParent.scala.html
                    HASH: 7722841f8d59c304796b71895d253bf8e7d59b11
                    MATRIX: 705->1|1093->308|1125->332|1205->223|1233->306|1261->381|1298->384|1340->418|1379->420|1430->443|1459->444|1558->516|1587->517|1641->543|1670->544|1749->596|1778->597|1835->626|1864->627|1942->678|1971->679|2016->696|2045->697|2122->747|2151->748|2232->802|2261->803|2298->813|2327->814|2456->907|2471->913|2539->959|2626->1010|2641->1016|2708->1061|2948->1265|2984->1279|3050->1309|3093->1330|3164->1365|3194->1373|3263->1406|3278->1412|3342->1454|3447->1523|3462->1529|3520->1565|3611->1620|3642->1642|3657->1648|3668->1663|3707->1693|3747->1695|3867->1779|3882->1785|3938->1818|3985->1828|4012->1832|4053->1836|4101->1861|4243->1997|4266->2011|4292->2024|4403->2099|4435->2109|4683->2321|4715->2344|4754->2345|4861->2416|4894->2427|4930->2444|4943->2448|4982->2449|5143->2578|8557->5955|8608->5983|8963->6301|8979->6307|9021->6326|9706->6974|9738->6982|9958->7173|9988->7174|10116->7274|10145->7275|10192->7290
                    LINES: 20->1|27->7|27->7|28->1|30->6|31->7|33->9|33->9|33->9|36->12|36->12|39->15|39->15|41->17|41->17|43->19|43->19|45->21|45->21|47->23|47->23|49->25|49->25|51->27|51->27|53->29|53->29|55->31|55->31|58->34|58->34|58->34|59->35|59->35|59->35|62->38|62->38|63->39|63->39|64->40|64->40|66->42|66->42|66->42|67->43|67->43|67->43|69->45|69->45|69->45|69->46|69->46|69->46|70->47|70->47|70->47|70->47|70->47|70->47|70->47|72->50|72->50|72->51|75->54|75->54|82->61|82->61|82->61|83->62|83->62|84->63|84->63|84->63|86->65|146->125|146->125|153->132|153->132|153->132|160->139|160->139|166->145|166->145|169->148|169->148|172->151
                    -- GENERATED --
                */
            