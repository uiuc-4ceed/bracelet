
package views.html.folders

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listitem extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Folder,UUID,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(folder: Folder, parentDataset: UUID)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.75*/("""

"""),format.raw/*4.1*/("""<div class="panel panel-default folder-panel" id=""""),_display_(Seq[Any](/*4.52*/folder/*4.58*/.id)),format.raw/*4.61*/("""-listitem">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2 col-sm-2 col-lg-2">

                    <a href="javascript:updatePageAndFolder(0, '"""),_display_(Seq[Any](/*9.66*/folder/*9.72*/.id.stringify)),format.raw/*9.85*/("""')">
                        <span class="bigicon glyphicon glyphicon-folder-close"></span>
                    </a>

            </div>
            <div class="col-md-10 col-sm-10 col-lg-10">
                <div id="folder-title-"""),_display_(Seq[Any](/*15.40*/folder/*15.46*/.id)),format.raw/*15.49*/("""">
                    <h3 id =""""),_display_(Seq[Any](/*16.31*/folder/*16.37*/.id)),format.raw/*16.40*/("""-name" class="inline">
                        <a href="javascript:updatePageAndFolder(0, '"""),_display_(Seq[Any](/*17.70*/folder/*17.76*/.id.stringify)),format.raw/*17.89*/("""')">"""),_display_(Seq[Any](/*17.94*/folder/*17.100*/.displayName)),format.raw/*17.112*/("""</a>
                    </h3>
                    """),_display_(Seq[Any](/*19.22*/if(Permission.checkPermission(Permission.AddResourceToDataset, ResourceRef(ResourceRef.dataset, parentDataset)))/*19.134*/ {_display_(Seq[Any](format.raw/*19.136*/("""
                        <h3 id="h-edit-"""),_display_(Seq[Any](/*20.41*/folder/*20.47*/.id)),format.raw/*20.50*/("""" class="hiddencomplete">
                            <a id="edit-"""),_display_(Seq[Any](/*21.42*/folder)),format.raw/*21.48*/("""-id" href="javascript:updateFolderName('"""),_display_(Seq[Any](/*21.89*/parentDataset)),format.raw/*21.102*/("""', '"""),_display_(Seq[Any](/*21.107*/folder/*21.113*/.id)),format.raw/*21.116*/("""')" title="Click to edit folder name">
                                <span class ="glyphicon glyphicon-edit" aria-hidden ="true"></span>
                            </a>
                        </h3>
                    """)))})),format.raw/*25.22*/("""
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <ul class="list-unstyled">
                            <li>
                                <span class="glyphicon glyphicon-folder-close"></span> """),_display_(Seq[Any](/*32.89*/folder/*32.95*/.folders.length)),format.raw/*32.110*/("""
                                <span class="glyphicon glyphicon-file"></span> """),_display_(Seq[Any](/*33.81*/folder/*33.87*/.files.length)),format.raw/*33.100*/("""
                                """),_display_(Seq[Any](/*34.34*/if(user.isDefined)/*34.52*/ {_display_(Seq[Any](format.raw/*34.54*/("""
                                    """),_display_(Seq[Any](/*35.38*/if(Permission.checkPermission(Permission.RemoveResourceFromDataset, ResourceRef(ResourceRef.dataset, parentDataset)))/*35.155*/{_display_(Seq[Any](format.raw/*35.156*/("""
                                        <button onclick="confirmDeleteResource('folder','folder','"""),_display_(Seq[Any](/*36.100*/(folder.id))),format.raw/*36.111*/("""','"""),_display_(Seq[Any](/*36.115*/(folder.displayName.replace("'","&#39;")))),format.raw/*36.156*/("""', '"""),_display_(Seq[Any](/*36.161*/parentDataset)),format.raw/*36.174*/("""', '/')" class="btn btn-link" title="Delete folder">
                                            <span class="glyphicon glyphicon-trash"></span></button>
                                    """)))}/*38.39*/else/*38.44*/{_display_(Seq[Any](format.raw/*38.45*/("""
                                        <div class="inline" title="No permission to delete the folder">
                                            <button disabled class="btn btn-link"><span class="glyphicon glyphicon-trash"></span></button>
                                        </div>
                                    """)))})),format.raw/*42.38*/("""
                                """)))})),format.raw/*43.34*/("""
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>

        $(document).ready(function()"""),format.raw/*54.37*/("""{"""),format.raw/*54.38*/("""

            """),_display_(Seq[Any](/*56.14*/if(user.isDefined)/*56.32*/ {_display_(Seq[Any](format.raw/*56.34*/("""
                if("""),_display_(Seq[Any](/*57.21*/Permission/*57.31*/.checkPermission(Permission.AddResourceToDataset, ResourceRef(ResourceRef.dataset, parentDataset)))),format.raw/*57.129*/(""") """),format.raw/*57.131*/("""{"""),format.raw/*57.132*/("""
                     $(document).on('mouseenter', '#folder-title-"""),_display_(Seq[Any](/*58.67*/folder/*58.73*/.id)),format.raw/*58.76*/("""', function() """),format.raw/*58.90*/("""{"""),format.raw/*58.91*/("""
                        $('#h-edit-"""),_display_(Seq[Any](/*59.37*/folder/*59.43*/.id)),format.raw/*59.46*/("""').removeClass("hiddencomplete");
                        $('#h-edit-"""),_display_(Seq[Any](/*60.37*/folder/*60.43*/.id)),format.raw/*60.46*/("""').addClass("inline");
                     """),format.raw/*61.22*/("""}"""),format.raw/*61.23*/(""").on('mouseleave', '#folder-title-"""),_display_(Seq[Any](/*61.58*/folder/*61.64*/.id)),format.raw/*61.67*/("""', function() """),format.raw/*61.81*/("""{"""),format.raw/*61.82*/("""
                        $('#h-edit-"""),_display_(Seq[Any](/*62.37*/folder/*62.43*/.id)),format.raw/*62.46*/("""').removeClass("inline");
                        $('#h-edit-"""),_display_(Seq[Any](/*63.37*/folder/*63.43*/.id)),format.raw/*63.46*/("""').addClass("hiddencomplete");
                     """),format.raw/*64.22*/("""}"""),format.raw/*64.23*/(""");
                """),format.raw/*65.17*/("""}"""),format.raw/*65.18*/("""
            """)))})),format.raw/*66.14*/("""
        """),format.raw/*67.9*/("""}"""),format.raw/*67.10*/(""");
    </script>"""))}
    }
    
    def render(folder:Folder,parentDataset:UUID,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(folder,parentDataset)(user)
    
    def f:((Folder,UUID) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (folder,parentDataset) => (user) => apply(folder,parentDataset)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/folders/listitem.scala.html
                    HASH: 7046054f8514713c404f91de8b6ca235cc73b3a4
                    MATRIX: 623->1|812->74|840->99|926->150|940->156|964->159|1185->345|1199->351|1233->364|1501->596|1516->602|1541->605|1610->638|1625->644|1650->647|1778->739|1793->745|1828->758|1869->763|1885->769|1920->781|2008->833|2130->945|2171->947|2248->988|2263->994|2288->997|2391->1064|2419->1070|2496->1111|2532->1124|2574->1129|2590->1135|2616->1138|2871->1361|3202->1656|3217->1662|3255->1677|3372->1758|3387->1764|3423->1777|3493->1811|3520->1829|3560->1831|3634->1869|3761->1986|3801->1987|3938->2087|3972->2098|4013->2102|4077->2143|4119->2148|4155->2161|4365->2353|4378->2358|4417->2359|4777->2687|4843->2721|5088->2938|5117->2939|5168->2954|5195->2972|5235->2974|5292->2995|5311->3005|5432->3103|5463->3105|5493->3106|5596->3173|5611->3179|5636->3182|5678->3196|5707->3197|5780->3234|5795->3240|5820->3243|5926->3313|5941->3319|5966->3322|6038->3366|6067->3367|6138->3402|6153->3408|6178->3411|6220->3425|6249->3426|6322->3463|6337->3469|6362->3472|6460->3534|6475->3540|6500->3543|6580->3595|6609->3596|6656->3615|6685->3616|6731->3630|6767->3639|6796->3640
                    LINES: 20->1|24->1|26->4|26->4|26->4|26->4|31->9|31->9|31->9|37->15|37->15|37->15|38->16|38->16|38->16|39->17|39->17|39->17|39->17|39->17|39->17|41->19|41->19|41->19|42->20|42->20|42->20|43->21|43->21|43->21|43->21|43->21|43->21|43->21|47->25|54->32|54->32|54->32|55->33|55->33|55->33|56->34|56->34|56->34|57->35|57->35|57->35|58->36|58->36|58->36|58->36|58->36|58->36|60->38|60->38|60->38|64->42|65->43|76->54|76->54|78->56|78->56|78->56|79->57|79->57|79->57|79->57|79->57|80->58|80->58|80->58|80->58|80->58|81->59|81->59|81->59|82->60|82->60|82->60|83->61|83->61|83->61|83->61|83->61|83->61|83->61|84->62|84->62|84->62|85->63|85->63|85->63|86->64|86->64|87->65|87->65|88->66|89->67|89->67
                    -- GENERATED --
                */
            