
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object home2 extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template17[String,List[models.Event],User,List[Dataset],Map[UUID, Int],List[Collection],List[ProjectSpace],Boolean,List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],Option[Boolean],List[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*2.2*/(displayedName: String,  newsfeed: List[models.Event], profile:User, datasetsList: List[Dataset],  dscomments: Map[UUID, Int], collectionsList:List[Collection], spacesList: List[ProjectSpace],
        deletePermission: Boolean,  followers: List[(UUID, String, String, String)], followedUsers: List[(UUID, String, String, String)], followedFiles: List[(UUID, String, String)],
        followedDatasets: List[(UUID, String, String)], followedCollections: List[(UUID, String, String)],
        followedSpaces: List[(UUID, String, String)], ownProfile: Option[Boolean], userSelections: List[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters


Seq[Any](format.raw/*5.149*/("""

"""),format.raw/*8.1*/("""
"""),_display_(Seq[Any](/*9.2*/main(displayedName)/*9.21*/ {_display_(Seq[Any](format.raw/*9.23*/("""
    <script src=""""),_display_(Seq[Any](/*10.19*/routes/*10.25*/.Assets.at("javascripts/collectionListProcess.js"))),format.raw/*10.75*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*11.19*/routes/*11.25*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*11.72*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*12.19*/routes/*12.25*/.Assets.at("javascripts/spaceListProcess.js"))),format.raw/*12.70*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*13.19*/routes/*13.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*13.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*14.19*/routes/*14.25*/.Assets.at("javascripts/stickytabs/jquery.stickytabs.js"))),format.raw/*14.82*/("""" type="text/javascript"></script>

    <div class="alert alert-primary" role="alert">
    * The 4CeeD File tree and other advanced features are not supported on this machine.
    </div>    

    <div class="row">

<!--         <h2>"""),_display_(Seq[Any](/*22.19*/profile/*22.26*/.fullName)),format.raw/*22.35*/("""</h2>
        <div class="button-margins">
            <a href=""""),_display_(Seq[Any](/*24.23*/routes/*24.29*/.Profile.viewProfileUUID(profile.id))),format.raw/*24.65*/("""" class="btn btn-link" title="Show profile">
                <span class="glyphicon glyphicon-user"></span> Profile</a>
            <a href=""""),_display_(Seq[Any](/*26.23*/routes/*26.29*/.Spaces.newSpace())),format.raw/*26.47*/("""" class="btn btn-link" title="Create a new """),_display_(Seq[Any](/*26.91*/Messages("space.title"))),format.raw/*26.114*/("""">
                <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*27.63*/Messages("create.title", Messages("space.title")))),format.raw/*27.112*/("""</a>
            <a href=""""),_display_(Seq[Any](/*28.23*/routes/*28.29*/.Datasets.newDataset(None, None))),format.raw/*28.61*/("""" class="btn btn-link" title="Create a new dataset">
                <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*29.63*/Messages("create.title", Messages("dataset.title")))),format.raw/*29.114*/("""</a>
            <a href=""""),_display_(Seq[Any](/*30.23*/routes/*30.29*/.Collections.newCollection(None))),format.raw/*30.61*/("""" class="btn btn-link" title="Create a new collection">
                <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*31.63*/Messages("create.title", Messages("collection.title")))),format.raw/*31.117*/("""</a>
        </div> -->
    <div class="row">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
<!--             <li role="presentation" class="active"><a href="#events" aria-controls="events" role="tab" data-toggle="tab"><b>Activity</b></a></li>
 -->            <li role="presentation" ><a href="#spaces" aria-controls="spaces" role="tab" data-toggle="tab"><b>"""),_display_(Seq[Any](/*37.116*/play/*37.120*/.api.i18n.Messages("home.title", play.api.i18n.Messages("spaces.title")))),format.raw/*37.192*/("""</b></a></li>
            <li role="presentation" class="active"><a href="#collections" aria-controls="collections" role="tab" data-toggle="tab"><b>"""),_display_(Seq[Any](/*38.136*/play/*38.140*/.api.i18n.Messages("home.title", play.api.i18n.Messages("collections.title")))),format.raw/*38.217*/("""</b></a></li>
           
            <li role="presentation"><a href="#datasets" aria-controls="datasets" role="tab" data-toggle="tab"><b>"""),_display_(Seq[Any](/*40.115*/play/*40.119*/.api.i18n.Messages("home.title", play.api.i18n.Messages("datasets.title")))),format.raw/*40.193*/("""</b></a></li>
<!--             <li role="presentation"><a href="#followers" aria-controls="followers" role="tab" data-toggle="tab"><b>Followers</b></a></li>
 -->        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane" id="events">
                <div class="top-padding">
                    """),_display_(Seq[Any](/*47.22*/eventsList(newsfeed))),format.raw/*47.42*/("""
                    <div id="moreevent"></div>
                    <div class="text-center more-events">
                        <a  id="moreeventbutton" href="#/" onClick="moreEvents()" class="btn btn-link">
                            <span class="glyphicon glyphicon-refresh"> </span>
                            View More Events
                        </a>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="spaces">
                """),format.raw/*58.29*/("""
                <div class="row top-padding">
                    <div class="col-xs-12">
                        <p>"""),_display_(Seq[Any](/*61.29*/Messages("space.list.message", Messages("spaces.title")))),format.raw/*61.85*/("""</p>
                    </div>
                    <div class="col-xs-6">
                    </div>
                    <div class="col-xs-6">
                        <span class="pull-right">
                            <a href=""""),_display_(Seq[Any](/*67.39*/routes/*67.45*/.Spaces.list("","",12, "", Some(profile.id.stringify)))),format.raw/*67.99*/("""">See More</a>
                        </span>
                    </div>
                </div>
                <div class="row" id="masonry-spaces">
                """),_display_(Seq[Any](/*72.18*/spacesList/*72.28*/.map/*72.32*/ { space =>_display_(Seq[Any](format.raw/*72.43*/("""
                    """),_display_(Seq[Any](/*73.22*/spaces/*73.28*/.tile(space, "col-xs-3", routes.Application.index(), false))),format.raw/*73.87*/("""
                """)))})),format.raw/*74.18*/("""
                </div>
                """),_display_(Seq[Any](/*76.18*/if(spacesList.size < 1)/*76.41*/ {_display_(Seq[Any](format.raw/*76.43*/("""
                    <div class="text-center">
                        <div>"""),_display_(Seq[Any](/*78.31*/Messages("home.empty.message", Messages("spaces.title")))),format.raw/*78.87*/(""" </div>

                        <div> <a class="btn-link" href=""""),_display_(Seq[Any](/*80.58*/routes/*80.64*/.Spaces.newSpace())),format.raw/*80.82*/("""" title="Create a new """),_display_(Seq[Any](/*80.105*/Messages("space.title"))),format.raw/*80.128*/("""">"""),_display_(Seq[Any](/*80.131*/Messages("create.title", Messages("space.title")))),format.raw/*80.180*/("""</a></div>
                    </div>
                """)))})),format.raw/*82.18*/("""
            </div>
            <div role="tabpanel" class="tab-pane" id="datasets">
                """),format.raw/*85.31*/("""
                <div class="row top-padding">
                    <div class="col-xs-12">
                        <p>"""),_display_(Seq[Any](/*88.29*/Messages("dataset.list.message", Messages("datasets.title").toLowerCase, Messages("dataset.title").toLowerCase))),format.raw/*88.140*/("""  </p>
                    </div>
                    <div class="col-xs-6">
                    </div>
                    <div class="col-xs-6">
                        <span class="pull-right"><a href=""""),_display_(Seq[Any](/*93.60*/routes/*93.66*/.Datasets.list("","",12, None, None, "", Some(profile.id.stringify)))),format.raw/*93.134*/("""">See More</a></span>
                    </div>
                </div>
                <div class="row" id="masonry-datasets">
                """),_display_(Seq[Any](/*97.18*/datasetsList/*97.30*/.map/*97.34*/ { dataset =>_display_(Seq[Any](format.raw/*97.47*/("""
                    """),_display_(Seq[Any](/*98.22*/if(userSelections.indexOf(dataset.id.toString()) != -1)/*98.77*/ {_display_(Seq[Any](format.raw/*98.79*/("""
                        """),_display_(Seq[Any](/*99.26*/datasets/*99.34*/.tile(dataset, None, dscomments, "col-xs-3", false, false, routes.Application.index(), true))),format.raw/*99.126*/("""
                    """)))}/*100.23*/else/*100.28*/{_display_(Seq[Any](format.raw/*100.29*/("""
                        """),_display_(Seq[Any](/*101.26*/datasets/*101.34*/.tile(dataset, None, dscomments, "col-xs-3", false, false, routes.Application.index(), false))),format.raw/*101.127*/("""
                    """)))})),format.raw/*102.22*/("""

                """)))})),format.raw/*104.18*/("""
                </div>
                """),_display_(Seq[Any](/*106.18*/if(datasetsList.size < 1)/*106.43*/ {_display_(Seq[Any](format.raw/*106.45*/("""
                    <div class="text-center">
                        <div>"""),_display_(Seq[Any](/*108.31*/Messages("home.empty.message", Messages("datasets.title").toLowerCase))),format.raw/*108.101*/(""" </div>
                        <div> <a class="btn-link" href=""""),_display_(Seq[Any](/*109.58*/routes/*109.64*/.Datasets.newDataset(None, None))),format.raw/*109.96*/("""" title="Create a new """),_display_(Seq[Any](/*109.119*/Messages("dataset.title"))),format.raw/*109.144*/("""">"""),_display_(Seq[Any](/*109.147*/Messages("create.title", Messages("dataset.title")))),format.raw/*109.198*/("""</a></div>
                    </div>
                """)))})),format.raw/*111.18*/("""
            </div>
            <div role="tabpanel" class="tab-pane active" id="collections">
                """),format.raw/*114.34*/("""
                <div class="row top-padding">
                    <div class="col-xs-12">
                        <p>"""),_display_(Seq[Any](/*117.29*/Messages("collection.list.message", Messages("collections.title").toLowerCase,  Messages("datasets.title").toLowerCase ))),format.raw/*117.149*/("""</p>
                    </div>
                    <div class="col-xs-6">
                    </div>
                    <div class="col-xs-6">
                        <span class="pull-right">
                            <a href=""""),_display_(Seq[Any](/*123.39*/routes/*123.45*/.Collections.list("","",12, None, "", Some(profile.id.stringify)))),format.raw/*123.110*/("""">See More</a>
                        </span>
                    </div>
                </div>
                <div class="row" id ="masonry-collections">
                """),_display_(Seq[Any](/*128.18*/collectionsList/*128.33*/.map/*128.37*/ { collection =>_display_(Seq[Any](format.raw/*128.53*/("""
                    """),_display_(Seq[Any](/*129.22*/collections/*129.33*/.tile(collection, routes.Application.index(), None, "col-xs-3", false))),format.raw/*129.103*/("""
                """)))})),format.raw/*130.18*/("""
                </div>
                """),_display_(Seq[Any](/*132.18*/if(collectionsList.size < 1)/*132.46*/ {_display_(Seq[Any](format.raw/*132.48*/("""
                    <div class="text-center">
                        <div>"""),_display_(Seq[Any](/*134.31*/Messages("home.empty.message", Messages("collections.title").toLowerCase))),format.raw/*134.104*/(""" </div>

                        <div> <a class="btn-link" href=""""),_display_(Seq[Any](/*136.58*/routes/*136.64*/.Collections.newCollection(None))),format.raw/*136.96*/("""" title="Create a new """),_display_(Seq[Any](/*136.119*/Messages("collection.title"))),format.raw/*136.147*/("""">"""),_display_(Seq[Any](/*136.150*/Messages("create.title", Messages("collection.title")))),format.raw/*136.204*/("""</a></div>
                    </div>
                """)))})),format.raw/*138.18*/("""
            </div>
            <div role="tabpanel" class="tab-pane" id="followers">
                <div class="row top-padding">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <h2> Followers</h2>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*146.61*/routes/*146.67*/.Users.getFollowers())),format.raw/*146.88*/("""">See More</a></span>
                    </div>
                </div>
                <div class="row">

                    """),_display_(Seq[Any](/*151.22*/for(row <- followers) yield /*151.43*/ {_display_(Seq[Any](format.raw/*151.45*/("""
                        """),_display_(Seq[Any](/*152.26*/users/*152.31*/.tile(row, "col-xs-2", false))),format.raw/*152.60*/("""
                    """)))})),format.raw/*153.22*/("""
                    """),_display_(Seq[Any](/*154.22*/if(followers.size < 1)/*154.44*/ {_display_(Seq[Any](format.raw/*154.46*/("""
                        <div class="text-center col-xs-12">
                            This area will show users that follow you. Ask other users to follow you <a class="btn-link" href=""""),_display_(Seq[Any](/*156.129*/routes/*156.135*/.Profile.viewProfileUUID(profile.id))),format.raw/*156.171*/("""">here</a>
                        </div>
                    """)))})),format.raw/*158.22*/("""
                </div>
                <h2>Following</h2>
                <div class="panel-group" id="accordion" style="margin-top: 15px">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Users</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*171.77*/routes/*171.83*/.Users.getFollowing())),format.raw/*171.104*/("""">See More</a></span>
                                    </div>
                                </div>

                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body caption break-word">
                                <div class="row" style="margin-top : 15px;">
                                """),_display_(Seq[Any](/*180.34*/for(userInfo <- followedUsers) yield /*180.64*/ {_display_(Seq[Any](format.raw/*180.66*/("""
                                    """),_display_(Seq[Any](/*181.38*/users/*181.43*/.tile(userInfo, "col-xs-2", true))),format.raw/*181.76*/("""
                                """)))})),format.raw/*182.34*/("""
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSpaces">"""),_display_(Seq[Any](/*193.116*/Messages("spaces.title"))),format.raw/*193.140*/("""</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*196.77*/routes/*196.83*/.Spaces.followingSpaces(0, 12, ""))),format.raw/*196.117*/("""">See More</a></span>
                                    </div>
                                </div>

                            </h4>
                        </div>
                        <div id="collapseSpaces" class="panel-collapse collapse">
                            <div class="panel-body">
                            """),_display_(Seq[Any](/*204.30*/for(info <- followedSpaces) yield /*204.57*/ {_display_(Seq[Any](format.raw/*204.59*/("""
                                <div class="col-xs-3" style="margin-top : 30 px" id=""""),_display_(Seq[Any](/*205.87*/info/*205.91*/._1)),format.raw/*205.94*/("""-tile">
                                    <div class="panel panel-default space-panel">
                                        <div class="pull-left">
                                            <span class="glyphicon glyphicon-hdd"></span>
                                        </div>
                                        <div class="panel-body caption break-word">
                                            <h4><a href=""""),_display_(Seq[Any](/*211.59*/routes/*211.65*/.Spaces.getSpace(info._1))),format.raw/*211.90*/("""">"""),_display_(Seq[Any](/*211.93*/info/*211.97*/._2.toString)),format.raw/*211.109*/("""</a></h4>
                                        """),_display_(Seq[Any](/*212.42*/info/*212.46*/._3)),format.raw/*212.49*/("""
                                        </div>
                                        <ul class="list-group space-panel-footer">
                                        """),_display_(Seq[Any](/*215.42*/user/*215.46*/ match/*215.52*/ {/*216.45*/case Some(viewer) =>/*216.65*/ {_display_(Seq[Any](format.raw/*216.67*/("""
                                                """),_display_(Seq[Any](/*217.50*/ownProfile/*217.60*/ match/*217.66*/ {/*218.53*/case Some(sameProfile) =>/*218.78*/ {_display_(Seq[Any](format.raw/*218.80*/("""

                                                            <button
                                                            id="followButton"
                                                            type="button"
                                                            class="
                                                                """),_display_(Seq[Any](/*224.66*/if(viewer.followedEntities.filter(x => (x.id == info._1)).nonEmpty)/*224.133*/ {_display_(Seq[Any](format.raw/*224.135*/("""
                                                                    btn btn-link
                                                                """)))}/*226.67*/else/*226.72*/{_display_(Seq[Any](format.raw/*226.73*/("""
                                                                    btn btn-link
                                                                """)))})),format.raw/*228.66*/("""
                                                            "
                                                            data-toggle="button"
                                                            aria-pressed="
                                                                """),_display_(Seq[Any](/*232.66*/if(viewer.followedEntities.filter(x => (x.id == info._1)).nonEmpty)/*232.133*/ {_display_(Seq[Any](format.raw/*232.135*/("""
                                                                    true
                                                                """)))}/*234.67*/else/*234.72*/{_display_(Seq[Any](format.raw/*234.73*/("""
                                                                    false
                                                                """)))})),format.raw/*236.66*/("""
                                                            "
                                                            autocomplete="off"
                                                            objectType="space"
                                                            objectId=""""),_display_(Seq[Any](/*240.72*/info/*240.76*/._1.stringify)),format.raw/*240.89*/(""""
                                                            >
                                                            """),_display_(Seq[Any](/*242.62*/if(viewer.followedEntities.filter(x => (x.id == info._1)).nonEmpty)/*242.129*/ {_display_(Seq[Any](format.raw/*242.131*/("""
                                                                <span class="glyphicon glyphicon-star-empty"></span> Unfollow
                                                            """)))}/*244.63*/else/*244.68*/{_display_(Seq[Any](format.raw/*244.69*/("""
                                                                <span class="glyphicon glyphicon-star"></span> Follow
                                                            """)))})),format.raw/*246.62*/("""
                                                            </button>

                                                    """)))}/*250.53*/case None =>/*250.65*/ {_display_(Seq[Any](format.raw/*250.67*/("""
                                                    """)))}})),format.raw/*252.50*/("""
                                            """)))}/*254.45*/case None =>/*254.57*/ {}})),format.raw/*255.42*/("""
                                        </ul>
                                    </div>
                                </div>
                            """)))})),format.raw/*259.30*/("""
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Files</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*272.77*/routes/*272.83*/.Files.followingFiles(0, 12, ""))),format.raw/*272.115*/("""">See More</a></span>
                                    </div>
                                </div>

                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                            """),_display_(Seq[Any](/*280.30*/for(fileInfo <- followedFiles) yield /*280.60*/ {_display_(Seq[Any](format.raw/*280.62*/("""
                                <div class="col-xs-3" style="margin-top: 30px" id=""""),_display_(Seq[Any](/*281.85*/fileInfo/*281.93*/._1)),format.raw/*281.96*/("""-tile">
                                    <div class="panel panel-default file-panel">
                                        <div class="pull-left">
                                            <span class="glyphicon glyphicon-file"></span>
                                        </div>
                                        <div class="panel-body caption break-word">
                                            <h4><a href=""""),_display_(Seq[Any](/*287.59*/routes/*287.65*/.Files.file(fileInfo._1))),format.raw/*287.89*/("""">"""),_display_(Seq[Any](/*287.92*/fileInfo/*287.100*/._2.toString)),format.raw/*287.112*/("""</a></h4>
                                        """),_display_(Seq[Any](/*288.42*/fileInfo/*288.50*/._3)),format.raw/*288.53*/("""
                                        </div>
                                        <ul class="list-group file-panel-footer">
                                        """),_display_(Seq[Any](/*291.42*/user/*291.46*/ match/*291.52*/ {/*292.45*/case Some(viewer) =>/*292.65*/ {_display_(Seq[Any](format.raw/*292.67*/("""
                                                """),_display_(Seq[Any](/*293.50*/ownProfile/*293.60*/ match/*293.66*/ {/*294.53*/case Some(sameProfile) =>/*294.78*/ {_display_(Seq[Any](format.raw/*294.80*/("""

                                                            <button
                                                            id="followButton"
                                                            type="button"
                                                            class="
                                                                """),_display_(Seq[Any](/*300.66*/if(viewer.followedEntities.filter(x => (x.id == fileInfo._1)).nonEmpty)/*300.137*/ {_display_(Seq[Any](format.raw/*300.139*/("""
                                                                    btn btn-link
                                                                """)))}/*302.66*/else/*302.71*/{_display_(Seq[Any](format.raw/*302.72*/("""
                                                                    btn btn-link
                                                                """)))})),format.raw/*304.66*/("""
                                                            "
                                                            data-toggle="button"
                                                            aria-pressed="
                                                                """),_display_(Seq[Any](/*308.66*/if(viewer.followedEntities.filter(x => (x.id == fileInfo._1)).nonEmpty)/*308.137*/ {_display_(Seq[Any](format.raw/*308.139*/("""
                                                                    true
                                                                """)))}/*310.66*/else/*310.71*/{_display_(Seq[Any](format.raw/*310.72*/("""
                                                                    false
                                                                """)))})),format.raw/*312.66*/("""
                                                            "
                                                            autocomplete="off"
                                                            objectId = """"),_display_(Seq[Any](/*315.74*/fileInfo/*315.82*/._1.stringify)),format.raw/*315.95*/(""""
                                                            objectType = "file"
                                                            >
                                                            """),_display_(Seq[Any](/*318.62*/if(viewer.followedEntities.filter(x => (x.id == fileInfo._1)).nonEmpty)/*318.133*/ {_display_(Seq[Any](format.raw/*318.135*/("""
                                                                <span class="glyphicon glyphicon-star-empty"></span> Unfollow
                                                            """)))}/*320.63*/else/*320.68*/{_display_(Seq[Any](format.raw/*320.69*/("""
                                                                <span class="glyphicon glyphicon-star"></span> Follow
                                                            """)))})),format.raw/*322.62*/("""
                                                            </button>

                                                    """)))}/*326.53*/case None =>/*326.65*/ {_display_(Seq[Any](format.raw/*326.67*/("""
                                                    """)))}})),format.raw/*328.50*/("""
                                            """)))}/*330.45*/case None =>/*330.57*/ {}})),format.raw/*331.42*/("""
                                        </ul>
                                    </div>
                                </div>
                            """)))})),format.raw/*335.30*/("""
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Datasets</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*347.77*/routes/*347.83*/.Datasets.followingDatasets(0, 12, ""))),format.raw/*347.121*/("""">See More</a></span>
                                    </div>
                                </div>

                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                            """),_display_(Seq[Any](/*355.30*/for(datasetInfo <- followedDatasets) yield /*355.66*/ {_display_(Seq[Any](format.raw/*355.68*/("""
                                <div class="col-xs-3" style="margin-top: 30px" id=""""),_display_(Seq[Any](/*356.85*/datasetInfo/*356.96*/._1)),format.raw/*356.99*/("""-tile">
                                    <div class="panel panel-default dataset-panel">
                                        <div class="pull-left">
                                            <span class="glyphicon glyphicon-briefcase"></span>
                                        </div>
                                        <div class="panel-body caption break-word">
                                            <h4> <a href=""""),_display_(Seq[Any](/*362.60*/routes/*362.66*/.Datasets.dataset(datasetInfo._1))),format.raw/*362.99*/("""">"""),_display_(Seq[Any](/*362.102*/datasetInfo/*362.113*/._2.toString)),format.raw/*362.125*/("""</a></h4>
                                        """),_display_(Seq[Any](/*363.42*/datasetInfo/*363.53*/._3)),format.raw/*363.56*/("""
                                        </div>
                                        <ul class="list-group dataset-panel-footer">
                                        """),_display_(Seq[Any](/*366.42*/user/*366.46*/ match/*366.52*/ {/*367.45*/case Some(viewer) =>/*367.65*/ {_display_(Seq[Any](format.raw/*367.67*/("""
                                                """),_display_(Seq[Any](/*368.50*/ownProfile/*368.60*/ match/*368.66*/ {/*369.53*/case Some(sameProfile) =>/*369.78*/ {_display_(Seq[Any](format.raw/*369.80*/("""

                                                            <button
                                                            id="followButton"
                                                            type="button"
                                                            class="
                                                                """),_display_(Seq[Any](/*375.66*/if(viewer.followedEntities.filter(x => (x.id == datasetInfo._1)).nonEmpty)/*375.140*/ {_display_(Seq[Any](format.raw/*375.142*/("""
                                                                    btn btn-link
                                                                """)))}/*377.66*/else/*377.71*/{_display_(Seq[Any](format.raw/*377.72*/("""
                                                                    btn btn-link
                                                                """)))})),format.raw/*379.66*/("""
                                                            "
                                                            data-toggle="button"
                                                            aria-pressed="
                                                                """),_display_(Seq[Any](/*383.66*/if(viewer.followedEntities.filter(x => (x.id == datasetInfo._1)).nonEmpty)/*383.140*/ {_display_(Seq[Any](format.raw/*383.142*/("""
                                                                    true
                                                                """)))}/*385.66*/else/*385.71*/{_display_(Seq[Any](format.raw/*385.72*/("""
                                                                    false
                                                                """)))})),format.raw/*387.66*/("""
                                                            "
                                                            autocomplete="off"
                                                            objectType="dataset"
                                                            objectId=""""),_display_(Seq[Any](/*391.72*/datasetInfo/*391.83*/._1.stringify)),format.raw/*391.96*/(""""
                                                            >
                                                            """),_display_(Seq[Any](/*393.62*/if(viewer.followedEntities.filter(x => (x.id == datasetInfo._1)).nonEmpty)/*393.136*/ {_display_(Seq[Any](format.raw/*393.138*/("""
                                                                <span class="glyphicon glyphicon-star-empty"></span> Unfollow
                                                            """)))}/*395.63*/else/*395.68*/{_display_(Seq[Any](format.raw/*395.69*/("""
                                                                <span class="glyphicon glyphicon-star"></span> Follow
                                                            """)))})),format.raw/*397.62*/("""
                                                            </button>

                                                    """)))}/*401.53*/case None =>/*401.65*/ {_display_(Seq[Any](format.raw/*401.67*/("""
                                                    """)))}})),format.raw/*403.50*/("""
                                            """)))}/*405.45*/case None =>/*405.57*/ {}})),format.raw/*406.42*/("""
                                        </ul>
                                    </div>
                                </div>
                            """)))})),format.raw/*410.30*/("""
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Collections</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <span class="pull-right"> <a href=""""),_display_(Seq[Any](/*422.77*/routes/*422.83*/.Collections.followingCollections(0, 12, ""))),format.raw/*422.127*/("""">See More</a></span>
                                    </div>
                                </div>

                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                            """),_display_(Seq[Any](/*430.30*/for(collectionInfo <- followedCollections) yield /*430.72*/ {_display_(Seq[Any](format.raw/*430.74*/("""
                                <div class="col-xs-3" style="margin-top: 30px" id=""""),_display_(Seq[Any](/*431.85*/collectionInfo/*431.99*/._1)),format.raw/*431.102*/("""-tile">
                                    <div class="panel panel-default collection-panel">
                                        <div class="pull-left">
                                            <span class="glyphicon glyphicon-th-large"></span>
                                        </div>
                                        <div class="panel-body caption break-word">
                                            <h4><a href=""""),_display_(Seq[Any](/*437.59*/routes/*437.65*/.Collections.collection(collectionInfo._1))),format.raw/*437.107*/("""">"""),_display_(Seq[Any](/*437.110*/collectionInfo/*437.124*/._2.toString)),format.raw/*437.136*/("""</a></h4>
                                        """),_display_(Seq[Any](/*438.42*/collectionInfo/*438.56*/._3)),format.raw/*438.59*/("""
                                        </div>
                                        <ul class="list-group collection-panel-footer">
                                        """),_display_(Seq[Any](/*441.42*/user/*441.46*/ match/*441.52*/ {/*442.45*/case Some(viewer) =>/*442.65*/ {_display_(Seq[Any](format.raw/*442.67*/("""
                                                """),_display_(Seq[Any](/*443.50*/ownProfile/*443.60*/ match/*443.66*/ {/*444.53*/case Some(sameProfile) =>/*444.78*/ {_display_(Seq[Any](format.raw/*444.80*/("""

                                                            <button
                                                            id="followButton"
                                                            type="button"
                                                            class="
                                                                """),_display_(Seq[Any](/*450.66*/if(viewer.followedEntities.filter(x => (x.id == collectionInfo._1)).nonEmpty)/*450.143*/ {_display_(Seq[Any](format.raw/*450.145*/("""
                                                                    btn btn-link
                                                                """)))}/*452.66*/else/*452.71*/{_display_(Seq[Any](format.raw/*452.72*/("""
                                                                    btn btn-link
                                                                """)))})),format.raw/*454.66*/("""
                                                            "
                                                            data-toggle="button"
                                                            aria-pressed="
                                                                """),_display_(Seq[Any](/*458.66*/if(viewer.followedEntities.filter(x => (x.id == collectionInfo._1)).nonEmpty)/*458.143*/ {_display_(Seq[Any](format.raw/*458.145*/("""
                                                                    true
                                                                """)))}/*460.66*/else/*460.71*/{_display_(Seq[Any](format.raw/*460.72*/("""
                                                                    false
                                                                """)))})),format.raw/*462.66*/("""
                                                            "
                                                            autocomplete="off"
                                                            objectType="collection"
                                                            objectId=""""),_display_(Seq[Any](/*466.72*/collectionInfo/*466.86*/._1.stringify)),format.raw/*466.99*/(""""
                                                            >
                                                            """),_display_(Seq[Any](/*468.62*/if(viewer.followedEntities.filter(x => (x.id == collectionInfo._1)).nonEmpty)/*468.139*/ {_display_(Seq[Any](format.raw/*468.141*/("""
                                                                <span class="glyphicon glyphicon-star-empty"></span> Unfollow
                                                            """)))}/*470.63*/else/*470.68*/{_display_(Seq[Any](format.raw/*470.69*/("""
                                                                <span class="glyphicon glyphicon-star"></span> Follow
                                                            """)))})),format.raw/*472.62*/("""
                                                            </button>

                                                    """)))}/*476.53*/case None =>/*476.65*/ {_display_(Seq[Any](format.raw/*476.67*/("""
                                                    """)))}})),format.raw/*478.50*/("""
                                            """)))}/*480.45*/case None =>/*480.57*/ {}})),format.raw/*481.42*/("""
                                        </ul>
                                    </div>
                                </div>
                            """)))})),format.raw/*485.30*/("""

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script src=""""),_display_(Seq[Any](/*496.19*/routes/*496.25*/.Assets.at("javascripts/lib/masonry.pkgd.min.js"))),format.raw/*496.74*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*497.19*/routes/*497.25*/.Assets.at("javascripts/lib/imagesloaded.pkgd.min.js"))),format.raw/*497.79*/("""" type="text/javascript"></script>

    <script>
        var removeIndicator=true;
        function activateOne(id) """),format.raw/*501.34*/("""{"""),format.raw/*501.35*/("""
            // initialize Masonry
            var $container = $('#'+id).masonry();
            // layout Masonry again after all images have loaded
            imagesLoaded( '#masonry', function() """),format.raw/*505.50*/("""{"""),format.raw/*505.51*/("""
                $container.masonry("""),format.raw/*506.36*/("""{"""),format.raw/*506.37*/("""
                itemSelector: '.post-box',
                columnWidth: '.post-box',
                transitionDuration: 4
                """),format.raw/*510.17*/("""}"""),format.raw/*510.18*/(""");
            """),format.raw/*511.13*/("""}"""),format.raw/*511.14*/(""");
        """),format.raw/*512.9*/("""}"""),format.raw/*512.10*/("""

        function activate()"""),format.raw/*514.28*/("""{"""),format.raw/*514.29*/("""
            activateOne("masonry-datasets");
            activateOne("masonry-collections");
            activateOne("masonry-spaces");
        """),format.raw/*518.9*/("""}"""),format.raw/*518.10*/("""

        $(document).ready(function() """),format.raw/*520.38*/("""{"""),format.raw/*520.39*/("""
            activate();
            $('.nav-tabs').stickyTabs();
        """),format.raw/*523.9*/("""}"""),format.raw/*523.10*/(""");

        // fire when showing from tab
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) """),format.raw/*526.67*/("""{"""),format.raw/*526.68*/("""
            activate();
        """),format.raw/*528.9*/("""}"""),format.raw/*528.10*/(""")

        var eventCount = 3;
        function moreEvents()"""),format.raw/*531.30*/("""{"""),format.raw/*531.31*/("""
            var request = jsRoutes.controllers.Events.getEvents(eventCount).ajax("""),format.raw/*532.82*/("""{"""),format.raw/*532.83*/("""
                type: 'GET'
            """),format.raw/*534.13*/("""}"""),format.raw/*534.14*/(""");

            request.done(function (response, textStatus, jqXHR) """),format.raw/*536.65*/("""{"""),format.raw/*536.66*/("""
                eventCount = eventCount + 1;
                $("#moreevent").append(response);
            """),format.raw/*539.13*/("""}"""),format.raw/*539.14*/(""");

            request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*541.67*/("""{"""),format.raw/*541.68*/("""
                console.error("The following error occured: " + textStatus, errorThrown);
                notify("Could not get moe events: " + errorThrown, "error");
            """),format.raw/*544.13*/("""}"""),format.raw/*544.14*/(""");
        """),format.raw/*545.9*/("""}"""),format.raw/*545.10*/("""
    </script>
    <script src=""""),_display_(Seq[Any](/*547.19*/routes/*547.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*547.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*548.19*/routes/*548.25*/.Assets.at("javascripts/select.js"))),format.raw/*548.60*/("""" type="text/javascript"></script>
    """),format.raw/*553.14*/("""
""")))})),format.raw/*554.2*/("""
"""))}
    }
    
    def render(displayedName:String,newsfeed:List[models.Event],profile:User,datasetsList:List[Dataset],dscomments:Map[UUID, Int],collectionsList:List[Collection],spacesList:List[ProjectSpace],deletePermission:Boolean,followers:List[scala.Tuple4[UUID, String, String, String]],followedUsers:List[scala.Tuple4[UUID, String, String, String]],followedFiles:List[scala.Tuple3[UUID, String, String]],followedDatasets:List[scala.Tuple3[UUID, String, String]],followedCollections:List[scala.Tuple3[UUID, String, String]],followedSpaces:List[scala.Tuple3[UUID, String, String]],ownProfile:Option[Boolean],userSelections:List[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(displayedName,newsfeed,profile,datasetsList,dscomments,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections)(user)
    
    def f:((String,List[models.Event],User,List[Dataset],Map[UUID, Int],List[Collection],List[ProjectSpace],Boolean,List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple4[UUID, String, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],List[scala.Tuple3[UUID, String, String]],Option[Boolean],List[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (displayedName,newsfeed,profile,datasetsList,dscomments,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections) => (user) => apply(displayedName,newsfeed,profile,datasetsList,dscomments,collectionsList,spacesList,deletePermission,followers,followedUsers,followedFiles,followedDatasets,followedCollections,followedSpaces,ownProfile,userSelections)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/home2.scala.html
                    HASH: 01652fdc2829fb5f868751f90ffa895836868e52
                    MATRIX: 996->2|1752->633|1780->666|1816->668|1843->687|1882->689|1937->708|1952->714|2024->764|2113->817|2128->823|2197->870|2286->923|2301->929|2368->974|2457->1027|2472->1033|2534->1073|2623->1126|2638->1132|2717->1189|2986->1422|3002->1429|3033->1438|3134->1503|3149->1509|3207->1545|3385->1687|3400->1693|3440->1711|3520->1755|3566->1778|3667->1843|3739->1892|3802->1919|3817->1925|3871->1957|4022->2072|4096->2123|4159->2150|4174->2156|4228->2188|4382->2306|4459->2360|4883->2747|4897->2751|4992->2823|5178->2972|5192->2976|5292->3053|5469->3193|5483->3197|5580->3271|5978->3633|6020->3653|6559->4176|6714->4295|6792->4351|7061->4584|7076->4590|7152->4644|7356->4812|7375->4822|7388->4826|7437->4837|7495->4859|7510->4865|7591->4924|7641->4942|7718->4983|7750->5006|7790->5008|7903->5085|7981->5141|8083->5207|8098->5213|8138->5231|8198->5254|8244->5277|8284->5280|8356->5329|8443->5384|8572->5499|8727->5618|8861->5729|9103->5935|9118->5941|9209->6009|9390->6154|9411->6166|9424->6170|9475->6183|9533->6205|9597->6260|9637->6262|9699->6288|9716->6296|9831->6388|9873->6411|9887->6416|9927->6417|9990->6443|10008->6451|10125->6544|10180->6566|10232->6585|10310->6626|10345->6651|10386->6653|10500->6730|10594->6800|10696->6865|10712->6871|10767->6903|10828->6926|10877->6951|10918->6954|10993->7005|11081->7060|11221->7188|11377->7307|11521->7427|11791->7660|11807->7666|11896->7731|12107->7905|12132->7920|12146->7924|12201->7940|12260->7962|12281->7973|12375->8043|12426->8061|12504->8102|12542->8130|12583->8132|12697->8209|12794->8282|12897->8348|12913->8354|12968->8386|13029->8409|13081->8437|13122->8440|13200->8494|13288->8549|13728->8952|13744->8958|13788->8979|13953->9107|13991->9128|14032->9130|14095->9156|14110->9161|14162->9190|14217->9212|14276->9234|14308->9256|14349->9258|14576->9447|14593->9453|14653->9489|14749->9552|15495->10261|15511->10267|15556->10288|16024->10719|16071->10749|16112->10751|16187->10789|16202->10794|16258->10827|16325->10861|16880->11378|16928->11402|17148->11585|17164->11591|17222->11625|17593->11959|17637->11986|17678->11988|17802->12075|17816->12079|17842->12082|18312->12515|18328->12521|18376->12546|18416->12549|18430->12553|18466->12565|18554->12616|18568->12620|18594->12623|18803->12795|18817->12799|18833->12805|18845->12852|18875->12872|18916->12874|19003->12924|19023->12934|19039->12940|19051->12995|19086->13020|19127->13022|19519->13377|19597->13444|19639->13446|19806->13594|19820->13599|19860->13600|20040->13747|20361->14031|20439->14098|20481->14100|20640->14240|20654->14245|20694->14246|20867->14386|21196->14678|21210->14682|21246->14695|21408->14820|21486->14887|21528->14889|21736->15078|21750->15083|21790->15084|22003->15264|22148->15442|22170->15454|22211->15456|22299->15560|22365->15651|22387->15663|22414->15708|22605->15866|23304->16528|23320->16534|23376->16566|23744->16897|23791->16927|23832->16929|23954->17014|23972->17022|23998->17025|24468->17458|24484->17464|24531->17488|24571->17491|24590->17499|24626->17511|24714->17562|24732->17570|24758->17573|24966->17744|24980->17748|24996->17754|25008->17801|25038->17821|25079->17823|25166->17873|25186->17883|25202->17889|25214->17944|25249->17969|25290->17971|25682->18326|25764->18397|25806->18399|25973->18546|25987->18551|26027->18552|26207->18699|26528->18983|26610->19054|26652->19056|26811->19195|26825->19200|26865->19201|27038->19341|27290->19556|27308->19564|27344->19577|27586->19782|27668->19853|27710->19855|27918->20044|27932->20049|27972->20050|28185->20230|28330->20408|28352->20420|28393->20422|28481->20526|28547->20617|28569->20629|28596->20674|28787->20832|29490->21498|29506->21504|29568->21542|29938->21875|29991->21911|30032->21913|30154->21998|30175->22009|30201->22012|30680->22454|30696->22460|30752->22493|30793->22496|30815->22507|30851->22519|30939->22570|30960->22581|30986->22584|31197->22758|31211->22762|31227->22768|31239->22815|31269->22835|31310->22837|31397->22887|31417->22897|31433->22903|31445->22958|31480->22983|31521->22985|31913->23340|31998->23414|32040->23416|32207->23563|32221->23568|32261->23569|32441->23716|32762->24000|32847->24074|32889->24076|33048->24215|33062->24220|33102->24221|33275->24361|33606->24655|33627->24666|33663->24679|33825->24804|33910->24878|33952->24880|34160->25069|34174->25074|34214->25075|34427->25255|34572->25433|34594->25445|34635->25447|34723->25551|34789->25642|34811->25654|34838->25699|35029->25857|35734->26525|35750->26531|35818->26575|36187->26907|36246->26949|36287->26951|36409->27036|36433->27050|36460->27053|36940->27496|36956->27502|37022->27544|37063->27547|37088->27561|37124->27573|37212->27624|37236->27638|37262->27641|37476->27818|37490->27822|37506->27828|37518->27875|37548->27895|37589->27897|37676->27947|37696->27957|37712->27963|37724->28018|37759->28043|37800->28045|38192->28400|38280->28477|38322->28479|38489->28626|38503->28631|38543->28632|38723->28779|39044->29063|39132->29140|39174->29142|39333->29281|39347->29286|39387->29287|39560->29427|39894->29724|39918->29738|39954->29751|40116->29876|40204->29953|40246->29955|40454->30144|40468->30149|40508->30150|40721->30330|40866->30508|40888->30520|40929->30522|41017->30626|41083->30717|41105->30729|41132->30774|41323->30932|41543->31115|41559->31121|41631->31170|41721->31223|41737->31229|41814->31283|41959->31399|41989->31400|42217->31599|42247->31600|42312->31636|42342->31637|42511->31777|42541->31778|42585->31793|42615->31794|42654->31805|42684->31806|42742->31835|42772->31836|42945->31981|42975->31982|43043->32021|43073->32022|43175->32096|43205->32097|43342->32205|43372->32206|43433->32239|43463->32240|43552->32300|43582->32301|43693->32383|43723->32384|43793->32425|43823->32426|43920->32494|43950->32495|44087->32603|44117->32604|44216->32674|44246->32675|44455->32855|44485->32856|44524->32867|44554->32868|44624->32901|44640->32907|44705->32949|44795->33002|44811->33008|44869->33043|44937->33163|44971->33165
                    LINES: 20->2|27->5|29->8|30->9|30->9|30->9|31->10|31->10|31->10|32->11|32->11|32->11|33->12|33->12|33->12|34->13|34->13|34->13|35->14|35->14|35->14|43->22|43->22|43->22|45->24|45->24|45->24|47->26|47->26|47->26|47->26|47->26|48->27|48->27|49->28|49->28|49->28|50->29|50->29|51->30|51->30|51->30|52->31|52->31|58->37|58->37|58->37|59->38|59->38|59->38|61->40|61->40|61->40|68->47|68->47|79->58|82->61|82->61|88->67|88->67|88->67|93->72|93->72|93->72|93->72|94->73|94->73|94->73|95->74|97->76|97->76|97->76|99->78|99->78|101->80|101->80|101->80|101->80|101->80|101->80|101->80|103->82|106->85|109->88|109->88|114->93|114->93|114->93|118->97|118->97|118->97|118->97|119->98|119->98|119->98|120->99|120->99|120->99|121->100|121->100|121->100|122->101|122->101|122->101|123->102|125->104|127->106|127->106|127->106|129->108|129->108|130->109|130->109|130->109|130->109|130->109|130->109|130->109|132->111|135->114|138->117|138->117|144->123|144->123|144->123|149->128|149->128|149->128|149->128|150->129|150->129|150->129|151->130|153->132|153->132|153->132|155->134|155->134|157->136|157->136|157->136|157->136|157->136|157->136|157->136|159->138|167->146|167->146|167->146|172->151|172->151|172->151|173->152|173->152|173->152|174->153|175->154|175->154|175->154|177->156|177->156|177->156|179->158|192->171|192->171|192->171|201->180|201->180|201->180|202->181|202->181|202->181|203->182|214->193|214->193|217->196|217->196|217->196|225->204|225->204|225->204|226->205|226->205|226->205|232->211|232->211|232->211|232->211|232->211|232->211|233->212|233->212|233->212|236->215|236->215|236->215|236->216|236->216|236->216|237->217|237->217|237->217|237->218|237->218|237->218|243->224|243->224|243->224|245->226|245->226|245->226|247->228|251->232|251->232|251->232|253->234|253->234|253->234|255->236|259->240|259->240|259->240|261->242|261->242|261->242|263->244|263->244|263->244|265->246|268->250|268->250|268->250|269->252|270->254|270->254|270->255|274->259|287->272|287->272|287->272|295->280|295->280|295->280|296->281|296->281|296->281|302->287|302->287|302->287|302->287|302->287|302->287|303->288|303->288|303->288|306->291|306->291|306->291|306->292|306->292|306->292|307->293|307->293|307->293|307->294|307->294|307->294|313->300|313->300|313->300|315->302|315->302|315->302|317->304|321->308|321->308|321->308|323->310|323->310|323->310|325->312|328->315|328->315|328->315|331->318|331->318|331->318|333->320|333->320|333->320|335->322|338->326|338->326|338->326|339->328|340->330|340->330|340->331|344->335|356->347|356->347|356->347|364->355|364->355|364->355|365->356|365->356|365->356|371->362|371->362|371->362|371->362|371->362|371->362|372->363|372->363|372->363|375->366|375->366|375->366|375->367|375->367|375->367|376->368|376->368|376->368|376->369|376->369|376->369|382->375|382->375|382->375|384->377|384->377|384->377|386->379|390->383|390->383|390->383|392->385|392->385|392->385|394->387|398->391|398->391|398->391|400->393|400->393|400->393|402->395|402->395|402->395|404->397|407->401|407->401|407->401|408->403|409->405|409->405|409->406|413->410|425->422|425->422|425->422|433->430|433->430|433->430|434->431|434->431|434->431|440->437|440->437|440->437|440->437|440->437|440->437|441->438|441->438|441->438|444->441|444->441|444->441|444->442|444->442|444->442|445->443|445->443|445->443|445->444|445->444|445->444|451->450|451->450|451->450|453->452|453->452|453->452|455->454|459->458|459->458|459->458|461->460|461->460|461->460|463->462|467->466|467->466|467->466|469->468|469->468|469->468|471->470|471->470|471->470|473->472|476->476|476->476|476->476|477->478|478->480|478->480|478->481|482->485|493->496|493->496|493->496|494->497|494->497|494->497|498->501|498->501|502->505|502->505|503->506|503->506|507->510|507->510|508->511|508->511|509->512|509->512|511->514|511->514|515->518|515->518|517->520|517->520|520->523|520->523|523->526|523->526|525->528|525->528|528->531|528->531|529->532|529->532|531->534|531->534|533->536|533->536|536->539|536->539|538->541|538->541|541->544|541->544|542->545|542->545|544->547|544->547|544->547|545->548|545->548|545->548|546->553|547->554
                    -- GENERATED --
                */
            