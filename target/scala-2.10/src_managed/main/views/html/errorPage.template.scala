
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object errorPage extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[RequestHeader,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(re: RequestHeader, ex: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.69*/("""
"""),_display_(Seq[Any](/*2.2*/main("Error")/*2.15*/ {_display_(Seq[Any](format.raw/*2.17*/("""
  <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 text-center">
      <h2>
        Sorry, there was an internal error rendering this page!
      </h2>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 text-center">
        <button id="show-button" class="btn btn-default"><span class="glyphicon glyphicon-envelope"></span> Show Exception</button>
        <button id="error-button" class="btn btn-default"><span class="glyphicon glyphicon-envelope"></span> Submit Exception</button>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12" id="exception" hidden>
        <h3>"""),_display_(Seq[Any](/*14.14*/re/*14.16*/.toString())),format.raw/*14.27*/("""</h3>
        <pre>"""),_display_(Seq[Any](/*15.15*/ex)),format.raw/*15.17*/("""</pre>
    </div>
  </div>
  <script>
          $('#show-button').on('click', function() """),format.raw/*19.52*/("""{"""),format.raw/*19.53*/("""
              $("#exception").toggle();
          """),format.raw/*21.11*/("""}"""),format.raw/*21.12*/(""");
          $('#error-button').on('click', function()"""),format.raw/*22.52*/("""{"""),format.raw/*22.53*/("""
                var jsonData = JSON.stringify("""),format.raw/*23.47*/("""{"""),format.raw/*23.48*/(""""badRequest": '"""),_display_(Seq[Any](/*23.64*/re/*23.66*/.toString)),format.raw/*23.75*/("""', "exceptions":'"""),_display_(Seq[Any](/*23.93*/ex/*23.95*/.replace("\n", "\\n"))),format.raw/*23.116*/("""'"""),format.raw/*23.117*/("""}"""),format.raw/*23.118*/(""");
                var request = jsRoutes.api.Events.sendExceptionEmail().ajax("""),format.raw/*24.77*/("""{"""),format.raw/*24.78*/("""
                    data: jsonData,
                    type: 'POST',
                    contentType: "application/json"
                """),format.raw/*28.17*/("""}"""),format.raw/*28.18*/(""");

                request.done(function (response, textStatus, jqXHR) """),format.raw/*30.69*/("""{"""),format.raw/*30.70*/("""
                  $("#success").show();
                """),format.raw/*32.17*/("""}"""),format.raw/*32.18*/(""");

                request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*34.71*/("""{"""),format.raw/*34.72*/("""
                    console.error("The following error occured: " + textStatus, errorThrown);
                """),format.raw/*36.17*/("""}"""),format.raw/*36.18*/(""");
            """),format.raw/*37.13*/("""}"""),format.raw/*37.14*/(""");
  </script>
""")))})))}
    }
    
    def render(re:RequestHeader,ex:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(re,ex)(user)
    
    def f:((RequestHeader,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (re,ex) => (user) => apply(re,ex)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/errorPage.scala.html
                    HASH: 217deb775cf4b07a8a22f1b1de32913ef0b4b0a9
                    MATRIX: 625->1|786->68|822->70|843->83|882->85|1518->685|1529->687|1562->698|1618->718|1642->720|1759->809|1788->810|1867->861|1896->862|1978->916|2007->917|2082->964|2111->965|2163->981|2174->983|2205->992|2259->1010|2270->1012|2314->1033|2344->1034|2374->1035|2481->1114|2510->1115|2677->1254|2706->1255|2806->1327|2835->1328|2920->1385|2949->1386|3051->1460|3080->1461|3219->1572|3248->1573|3291->1588|3320->1589
                    LINES: 20->1|23->1|24->2|24->2|24->2|36->14|36->14|36->14|37->15|37->15|41->19|41->19|43->21|43->21|44->22|44->22|45->23|45->23|45->23|45->23|45->23|45->23|45->23|45->23|45->23|45->23|46->24|46->24|50->28|50->28|52->30|52->30|54->32|54->32|56->34|56->34|58->36|58->36|59->37|59->37
                    -- GENERATED --
                */
            