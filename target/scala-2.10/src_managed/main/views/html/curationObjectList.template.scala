
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object curationObjectList extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template8[List[models.CurationObject],String,String,Int,Option[String],Option[String],Option[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObjectsList: List[models.CurationObject], prev: String, next: String, limit: Int, mode: Option[String], space: Option[String], title: Option[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.196*/("""

"""),_display_(Seq[Any](/*4.2*/main(Messages("datasets.title"))/*4.34*/ {_display_(Seq[Any](format.raw/*4.36*/("""
    """),_display_(Seq[Any](/*5.6*/util/*5.10*/.masonry())),format.raw/*5.20*/("""
    <script src=""""),_display_(Seq[Any](/*6.19*/routes/*6.25*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*6.67*/("""" type="text/javascript"></script>

    <div class="row">
        <div class="col-md-12">
            <h1>Datasets published from """),_display_(Seq[Any](/*10.42*/Messages("space.title")/*10.65*/.toLowerCase)),format.raw/*10.77*/(""" <a href=""""),_display_(Seq[Any](/*10.88*/routes/*10.94*/.Spaces.getSpace(UUID(space.get)))),format.raw/*10.127*/("""">"""),_display_(Seq[Any](/*10.130*/Html(title.getOrElse("Published datasets")))),format.raw/*10.173*/("""</a> </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-11"></div>
        <div class="col-md-1">
            <div class="btn-group btn-group-sm pull-right">
                <button type="button" class="btn btn-default active" href="#tile-view" id="tile-view-btn"><span class="glyphicon glyphicon-th-large"></span></button>
                <button type="button" class="btn btn-default" href="#list-view" id="list-view-btn"><span class="glyphicon glyphicon-th-list"></span></button>
            </div>

            <script type="text/javascript" language="javascript">
                    var removeIndicator = false;
                    var viewMode = '"""),_display_(Seq[Any](/*24.38*/mode/*24.42*/.getOrElse("tile"))),format.raw/*24.60*/("""';
                    $.cookie.raw = true;
                    $.cookie.json = true;
                    $(function() """),format.raw/*27.34*/("""{"""),format.raw/*27.35*/("""
                        $('#tile-view-btn').click(function() """),format.raw/*28.62*/("""{"""),format.raw/*28.63*/("""
                            $('#tile-view').removeClass('hidden');
                            $('#list-view').addClass('hidden');
                            $('#tile-view-btn').addClass('active');
                            $('#list-view-btn').removeClass('active');
                            viewMode = "tile";
                            updatePage();
                            $.cookie('view-mode', 'tile', """),format.raw/*35.59*/("""{"""),format.raw/*35.60*/(""" path: '/' """),format.raw/*35.71*/("""}"""),format.raw/*35.72*/(""");
                            $('#masonry').masonry().masonry("""),format.raw/*36.61*/("""{"""),format.raw/*36.62*/("""
                                itemSelector: '.post-box',
                                columnWidth: '.post-box',
                                transitionDuration: 4
                            """),format.raw/*40.29*/("""}"""),format.raw/*40.30*/(""");
                        """),format.raw/*41.25*/("""}"""),format.raw/*41.26*/(""");
                        $('#list-view-btn').click(function() """),format.raw/*42.62*/("""{"""),format.raw/*42.63*/("""
                            $('#tile-view').addClass('hidden');
                            $('#list-view').removeClass('hidden');
                            $('#list-view-btn').addClass('active');
                            $('#tile-view-btn').removeClass('active');
                            viewMode = "list";
                            updatePage();
                            //Utilizing library from https://github.com/carhartl/jquery-cookie/tree/v1.4.1
                            $.cookie("view-mode", "list", """),format.raw/*50.59*/("""{"""),format.raw/*50.60*/(""" path: '/' """),format.raw/*50.71*/("""}"""),format.raw/*50.72*/(""");
                        """),format.raw/*51.25*/("""}"""),format.raw/*51.26*/(""");
                    """),format.raw/*52.21*/("""}"""),format.raw/*52.22*/(""");

                    $(document).ready(function() """),format.raw/*54.50*/("""{"""),format.raw/*54.51*/("""
                        //Set the cookie, for the case when it is passed in by the parameter
                        $.cookie("view-mode", viewMode, """),format.raw/*56.57*/("""{"""),format.raw/*56.58*/(""" path: '/' """),format.raw/*56.69*/("""}"""),format.raw/*56.70*/(""");
                        if (viewMode == "list") """),format.raw/*57.49*/("""{"""),format.raw/*57.50*/("""
                            $('#tile-view').addClass('hidden');
                            $('#list-view').removeClass('hidden');
                            $('#list-view-btn').addClass('active');
                            $('#tile-view-btn').removeClass('active');
                        """),format.raw/*62.25*/("""}"""),format.raw/*62.26*/("""
                        else """),format.raw/*63.30*/("""{"""),format.raw/*63.31*/("""
                            $('#tile-view').removeClass('hidden');
                            $('#list-view').addClass('hidden');
                            $('#tile-view-btn').addClass('active');
                            $('#list-view-btn').removeClass('active');
                        """),format.raw/*68.25*/("""}"""),format.raw/*68.26*/("""
                        updatePage();
                    """),format.raw/*70.21*/("""}"""),format.raw/*70.22*/(""");

                    //Function to unify the changing of the href for the next/previous links. Called on button activation for
                    //viewMode style, as well as on initial load of page.
                    function updatePage() """),format.raw/*74.43*/("""{"""),format.raw/*74.44*/("""
                        $('#nextlink').attr('href', """"),_display_(Seq[Any](/*75.55*/(routes.CurationObjects.list("a", next, limit, space)))),format.raw/*75.109*/("""");
                        $('#prevlink').attr('href', """"),_display_(Seq[Any](/*76.55*/(routes.CurationObjects.list("b", prev, limit, space)))),format.raw/*76.109*/("""");
                    """),format.raw/*77.21*/("""}"""),format.raw/*77.22*/("""
            </script>
        </div>
    </div>

    <div class="row hidden" id="tile-view">
        <div class="col-md-12">
            <div id="masonry">
            """),_display_(Seq[Any](/*85.14*/curationObjectsList/*85.33*/.map/*85.37*/ { curationObject =>_display_(Seq[Any](format.raw/*85.57*/("""
                """),_display_(Seq[Any](/*86.18*/curations/*86.27*/.tile(curationObject, space, "col-lg-4 col-md-4 col-sm-4"))),format.raw/*86.85*/("""
            """)))})),format.raw/*87.14*/("""
            </div>
        </div>
    </div>

    <div class="row hidden" id="list-view">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*94.14*/curationObjectsList/*94.33*/.map/*94.37*/ { curationObject =>_display_(Seq[Any](format.raw/*94.57*/("""
                """),_display_(Seq[Any](/*95.18*/curations/*95.27*/.listitem(curationObject, space))),format.raw/*95.59*/("""
            """)))})),format.raw/*96.14*/("""
        </div>
    </div>
    """),_display_(Seq[Any](/*99.6*/util/*99.10*/.masonry())),format.raw/*99.20*/("""

    <div class="row">
        <div class="col-md-12">
            <ul class="pager">
                """),_display_(Seq[Any](/*104.18*/if(prev != "")/*104.32*/ {_display_(Seq[Any](format.raw/*104.34*/("""
                    <li class="previous"><a class="btn btn-link" id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
                """)))})),format.raw/*106.18*/("""
                """),_display_(Seq[Any](/*107.18*/if(next != "")/*107.32*/ {_display_(Seq[Any](format.raw/*107.34*/("""
                    <li class ="next"><a class="btn btn-link" id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
                """)))})),format.raw/*109.18*/("""
            </ul>
        </div>
    </div>


""")))})),format.raw/*115.2*/("""
"""))}
    }
    
    def render(curationObjectsList:List[models.CurationObject],prev:String,next:String,limit:Int,mode:Option[String],space:Option[String],title:Option[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObjectsList,prev,next,limit,mode,space,title)(user)
    
    def f:((List[models.CurationObject],String,String,Int,Option[String],Option[String],Option[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObjectsList,prev,next,limit,mode,space,title) => (user) => apply(curationObjectsList,prev,next,limit,mode,space,title)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:28 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/curationObjectList.scala.html
                    HASH: 5e8ac39ac2ee9ac9fb794a238f8cdc280aad9922
                    MATRIX: 704->1|1015->195|1052->221|1092->253|1131->255|1171->261|1183->265|1214->275|1268->294|1282->300|1345->342|1512->473|1544->496|1578->508|1625->519|1640->525|1696->558|1736->561|1802->604|2523->1289|2536->1293|2576->1311|2723->1430|2752->1431|2842->1493|2871->1494|3317->1912|3346->1913|3385->1924|3414->1925|3505->1988|3534->1989|3762->2189|3791->2190|3846->2217|3875->2218|3967->2282|3996->2283|4549->2808|4578->2809|4617->2820|4646->2821|4701->2848|4730->2849|4781->2872|4810->2873|4891->2926|4920->2927|5098->3077|5127->3078|5166->3089|5195->3090|5274->3141|5303->3142|5626->3437|5655->3438|5713->3468|5742->3469|6065->3764|6094->3765|6181->3824|6210->3825|6484->4071|6513->4072|6604->4127|6681->4181|6775->4239|6852->4293|6904->4317|6933->4318|7139->4488|7167->4507|7180->4511|7238->4531|7292->4549|7310->4558|7390->4616|7436->4630|7608->4766|7636->4785|7649->4789|7707->4809|7761->4827|7779->4836|7833->4868|7879->4882|7946->4914|7959->4918|7991->4928|8132->5032|8156->5046|8197->5048|8432->5250|8487->5268|8511->5282|8552->5284|8780->5479|8860->5527
                    LINES: 20->1|24->1|26->4|26->4|26->4|27->5|27->5|27->5|28->6|28->6|28->6|32->10|32->10|32->10|32->10|32->10|32->10|32->10|32->10|46->24|46->24|46->24|49->27|49->27|50->28|50->28|57->35|57->35|57->35|57->35|58->36|58->36|62->40|62->40|63->41|63->41|64->42|64->42|72->50|72->50|72->50|72->50|73->51|73->51|74->52|74->52|76->54|76->54|78->56|78->56|78->56|78->56|79->57|79->57|84->62|84->62|85->63|85->63|90->68|90->68|92->70|92->70|96->74|96->74|97->75|97->75|98->76|98->76|99->77|99->77|107->85|107->85|107->85|107->85|108->86|108->86|108->86|109->87|116->94|116->94|116->94|116->94|117->95|117->95|117->95|118->96|121->99|121->99|121->99|126->104|126->104|126->104|128->106|129->107|129->107|129->107|131->109|137->115
                    -- GENERATED --
                */
            