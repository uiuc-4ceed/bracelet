
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object multimediasearch extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

implicit def /*3.2*/implicitFieldConstructor/*3.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.40*/("""
"""),format.raw/*3.75*/("""

"""),_display_(Seq[Any](/*5.2*/main("Clowder")/*5.17*/ {_display_(Seq[Any](format.raw/*5.19*/("""

<script>
var Configuration = """),format.raw/*8.21*/("""{"""),format.raw/*8.22*/("""}"""),format.raw/*8.23*/(""";
Configuration.hostIp = """"),_display_(Seq[Any](/*9.26*/play/*9.30*/.Play.application().configuration().getString("hostIp"))),format.raw/*9.85*/("""";
</script>

<link rel="stylesheet" media="screen" href=""""),_display_(Seq[Any](/*12.46*/routes/*12.52*/.Assets.at("stylesheets/dragdrop.css"))),format.raw/*12.90*/("""">

<div class="page-header">
    <h1>Multimedia Search</h1>
  </div>
<div class="row">
	<div class="col-md-12">		
	
<br>
<br>

 """),_display_(Seq[Any](/*23.3*/form(action = routes.Files.uploadSelectQuery, 'id->"formOne", 'enctype -> "multipart/form-data", 'class -> "form-horizontal"  )/*23.130*/ {_display_(Seq[Any](format.raw/*23.132*/("""
	    <!-- placeholder for validation -->
		 <div id="validation"></div>
		 
	      <legend>Select an Image and Search</legend>
	     	<input type="file" id="query" name="File">	     		
	     	<br>
	     	<input type="radio" name="indexType" value="images"   		onchange="toggleCheckbox()" checked >	Search within whole files
	     	<br>
			<input type="radio" name="indexType" value="sectionsAll" 	onchange="toggleCheckbox()" >			Search within ALL sections
			<br>
			<input type="radio" name="indexType" value="sectionsSome" 	onchange="toggleCheckbox()" id="someSections" >	Search within selected sections
			<br>
			
			<!-- placeholder for checkboxes with sections  -->
			<div id="checkboxes" style="padding-left:15px">	</div>
			
			<input type="radio" name="indexType" value="both" 			onchange="toggleCheckbox()">			Search within everything (whole files and sections)
	    </fieldset>
        <div class="form-actions">
			<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
        </div>
	  """)))})),format.raw/*45.5*/("""
			
		<script language="javascript">
		
		//var to store all available sections
		var sectionsList =[];
		
		//============================================================================
		// on page load
		//============================================================================
		$(document).ready(function ()"""),format.raw/*55.32*/("""{"""),format.raw/*55.33*/("""
			console.log("document is ready")	
			//this will set the sectionsList var
			getAllSections();
			
			var someSectionsButton = document.getElementById('someSections');
			if(someSectionsButton.checked)
      		"""),format.raw/*62.9*/("""{"""),format.raw/*62.10*/("""
      			console.log("radio button is checked. will display sections.");
      			displayAvailableSections();
      		"""),format.raw/*65.9*/("""}"""),format.raw/*65.10*/(""" else """),format.raw/*65.16*/("""{"""),format.raw/*65.17*/(""" console.log("radio button not checked");"""),format.raw/*65.58*/("""}"""),format.raw/*65.59*/("""
		"""),format.raw/*66.3*/("""}"""),format.raw/*66.4*/(""");
		
		
		//============================================================================
		//validate input on submit
		//============================================================================		
			document.getElementById('formOne').onsubmit =function()"""),format.raw/*72.59*/("""{"""),format.raw/*72.60*/("""			
			//alert("top of validation");
			var errors = "";
			//reset errors display
			document.getElementById("validation").innerHTML = "";

			//if someSections radio button selected - must select at least one section
			var someSectionsRB = document.getElementById('someSections');				
			if(someSectionsRB.checked)
      		"""),format.raw/*81.9*/("""{"""),format.raw/*81.10*/("""				
				var atLeastOneChecked = false;
				var checkboxElem;
				for (var i=0; i<sectionsList.length; i++)"""),format.raw/*84.46*/("""{"""),format.raw/*84.47*/("""
					//element ids of checkboxes are set to sectionsList
					//so iterating returns all checkboxes
					checkboxElem = document.getElementById(sectionsList[i]);
					atLeastOneChecked = atLeastOneChecked || checkboxElem.checked;
				"""),format.raw/*89.5*/("""}"""),format.raw/*89.6*/("""	
				if (!atLeastOneChecked)"""),format.raw/*90.28*/("""{"""),format.raw/*90.29*/("""
					errors += "<li>Please select at least one section </li>";
				"""),format.raw/*92.5*/("""}"""),format.raw/*92.6*/("""
      		"""),format.raw/*93.9*/("""}"""),format.raw/*93.10*/(""" 
			//make sure file is attached
			var f = document.getElementById("query").value;
			if (f == null || f == "")"""),format.raw/*96.29*/("""{"""),format.raw/*96.30*/("""
				errors += "<li>Please choose a file </li>";
			"""),format.raw/*98.4*/("""}"""),format.raw/*98.5*/(""" 			
			if (errors.length > 0) """),format.raw/*99.27*/("""{"""),format.raw/*99.28*/("""
		        document.getElementById("validation").innerHTML = "Please correct the following errors:<ul>" + errors + "</ul>";
		        document.getElementById("validation").style.backgroundColor = "#FFCCCC";
		        return false;
		    """),format.raw/*103.7*/("""}"""),format.raw/*103.8*/(""" else """),format.raw/*103.14*/("""{"""),format.raw/*103.15*/("""
		    	//alert("input OK");
		    	return true;
		    """),format.raw/*106.7*/("""}"""),format.raw/*106.8*/("""
		"""),format.raw/*107.3*/("""}"""),format.raw/*107.4*/("""
		
		//============================================================================
		//If radio button is checked for selecting some sections, displays all available sections as checkboxes
		// if a different radio button is checked, hides all the checkboxes
		//============================================================================
		function toggleCheckbox()"""),format.raw/*113.28*/("""{"""),format.raw/*113.29*/("""						
			var someSectionsButton = document.getElementById('someSections');				
			if(someSectionsButton.checked)
      		"""),format.raw/*116.9*/("""{"""),format.raw/*116.10*/("""
      			console.log("radio button is checked");
				displayAvailableSections();
			"""),format.raw/*119.4*/("""}"""),format.raw/*119.5*/(""" else """),format.raw/*119.11*/("""{"""),format.raw/*119.12*/("""
				console.log("radio button is UNchecked");	
				//remove checkboxes with sections
				var checkboxes =document.getElementById("checkboxes");
				while (checkboxes.firstChild) checkboxes.removeChild(checkboxes.firstChild);						
			"""),format.raw/*124.4*/("""}"""),format.raw/*124.5*/("""			
		"""),format.raw/*125.3*/("""}"""),format.raw/*125.4*/("""			
		
		//============================================================================
		// display all available sections as checkboxes
		//============================================================================
		function displayAvailableSections()"""),format.raw/*130.38*/("""{"""),format.raw/*130.39*/("""
			console.log("displayAvailableSections top + sectionsList.length = " + sectionsList.length);	
			var checkboxes = document.getElementById('checkboxes');			
			for (var i=0; i<sectionsList.length; i++)"""),format.raw/*133.45*/("""{"""),format.raw/*133.46*/("""
				console.log("sections list[ " + i + "] = " + sectionsList[i]);
				var label= document.createElement("label");
				var description = document.createTextNode(sectionsList[i]);
				
				var checkbox = document.createElement("input");				
				checkbox.type = "checkbox";    // make the element a checkbox
				checkbox.name = "sections";      // give it a name we can check on the server side
				checkbox.value = sectionsList[i];         // set value
				checkbox.id = sectionsList[i]; 			//set id
				
				label.appendChild(checkbox);   // add the box to the element
				label.appendChild(description);// add the description to the element				
				
				checkboxes.appendChild(label);
				checkboxes.appendChild(document.createElement("br"));
			"""),format.raw/*149.4*/("""}"""),format.raw/*149.5*/("""				
		"""),format.raw/*150.3*/("""}"""),format.raw/*150.4*/("""		
			
		//============================================================================
		// ajax call to get all available sections, runs on page load
		//============================================================================
			function getAllSections() """),format.raw/*155.30*/("""{"""),format.raw/*155.31*/("""
				$.ajax("""),format.raw/*156.12*/("""{"""),format.raw/*156.13*/("""
				          type: "GET",
				          url : """"),_display_(Seq[Any](/*158.23*/routes/*158.29*/.Admin.getSections())),format.raw/*158.49*/("""",
				          dataType : "json",
				          
				          // code to run if the request succeeds
				          success : function(json) """),format.raw/*162.40*/("""{"""),format.raw/*162.41*/("""   
				        	console.log('getAllSections - Make call succeeded')				          	
							console.log("text received: " + json + ", len = "+ json.length)
							for(var i=0; i<json.length; i++)"""),format.raw/*165.40*/("""{"""),format.raw/*165.41*/("""
								console.log("json = " + json[i])
								sectionsList.push(json[i])
							"""),format.raw/*168.8*/("""}"""),format.raw/*168.9*/("""							
				          """),format.raw/*169.15*/("""}"""),format.raw/*169.16*/(""",
				          
				          error : function(xhr, textStatus, errorThrown) """),format.raw/*171.62*/("""{"""),format.raw/*171.63*/("""
				              console.log('getAllSections - Make call failed ' +  errorThrown);
				              console.log( 'Status: ' + status );
				          """),format.raw/*174.15*/("""}"""),format.raw/*174.16*/(""",
				          
				          // code to run regardless of success or failure
				          complete: function( xhr, status ) """),format.raw/*177.49*/("""{"""),format.raw/*177.50*/("""
				              console.log( "getAllSections - The request is complete!" );
				          """),format.raw/*179.15*/("""}"""),format.raw/*179.16*/("""
				      """),format.raw/*180.11*/("""}"""),format.raw/*180.12*/(""");
				"""),format.raw/*181.5*/("""}"""),format.raw/*181.6*/("""					
							
			</script>
			
	   
	<p><p><br>
	
	      <legend> Drop an Image and Search </legend>
	      <br>
	      <div id="filedrag"> Drop Box</div>
	      <output id="list"></output>
	 
	  <div class="form-actions">
		  <button type="submit" class="btn btn-default" id="submit-button-id"><span class="glyphicon glyphicon-search"></span> Search</button>
        </div>
	 
	  
   """),_display_(Seq[Any](/*198.5*/form(action = routes.Search.searchbyURL(""), 'enctype -> "multipart/form-data", 'class -> "form-horizontal")/*198.113*/ {_display_(Seq[Any](format.raw/*198.115*/("""
	    <fieldset>
	      <br>
	 	    <label><h4>Paste your query image URL</h4></label>
    	<input type="text" placeholder="Paste your URL here" name="query">
	    <br>
	    <br>	
		</fieldset>	
		<br>
		<br>
	   <div style="text-align: center;">
		   <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
       </div>
       <br>
       <br>       
	  """)))})),format.raw/*213.5*/("""
	  	 
	
	</div>
   
        
	</div>		
   <script src=""""),_display_(Seq[Any](/*220.18*/routes/*220.24*/.Assets.at("javascripts/file-browser/views/dragdrop.js"))),format.raw/*220.80*/("""" type="text/javascript"></script>
    
  """)))})),format.raw/*222.4*/("""
"""))}
    }
    
    def render(user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply()(user)
    
    def f:(() => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = () => (user) => apply()(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/multimediasearch.scala.html
                    HASH: 2665463acc5e9995dbc3f5520685079972a47cca
                    MATRIX: 611->1|751->58|783->82|862->39|890->131|927->134|950->149|989->151|1047->182|1075->183|1103->184|1165->211|1177->215|1253->270|1348->329|1363->335|1423->373|1588->503|1725->630|1766->632|2856->1691|3203->2010|3232->2011|3473->2225|3502->2226|3648->2345|3677->2346|3711->2352|3740->2353|3809->2394|3838->2395|3868->2398|3896->2399|4184->2659|4213->2660|4566->2986|4595->2987|4730->3094|4759->3095|5021->3330|5049->3331|5106->3360|5135->3361|5230->3429|5258->3430|5294->3439|5323->3440|5464->3553|5493->3554|5572->3606|5600->3607|5659->3638|5688->3639|5953->3876|5982->3877|6017->3883|6047->3884|6130->3939|6159->3940|6190->3943|6219->3944|6617->4313|6647->4314|6797->4436|6827->4437|6940->4522|6969->4523|7004->4529|7034->4530|7297->4765|7326->4766|7360->4772|7389->4773|7674->5029|7704->5030|7936->5233|7966->5234|8742->5982|8771->5983|8806->5990|8835->5991|9126->6253|9156->6254|9197->6266|9227->6267|9314->6317|9330->6323|9373->6343|9545->6486|9575->6487|9798->6681|9828->6682|9940->6766|9969->6767|10020->6789|10050->6790|10157->6868|10187->6869|10369->7022|10399->7023|10555->7150|10585->7151|10707->7244|10737->7245|10777->7256|10807->7257|10842->7264|10871->7265|11293->7651|11412->7759|11454->7761|11898->8173|11992->8230|12008->8236|12087->8292|12162->8335
                    LINES: 20->1|23->3|23->3|24->1|25->3|27->5|27->5|27->5|30->8|30->8|30->8|31->9|31->9|31->9|34->12|34->12|34->12|45->23|45->23|45->23|67->45|77->55|77->55|84->62|84->62|87->65|87->65|87->65|87->65|87->65|87->65|88->66|88->66|94->72|94->72|103->81|103->81|106->84|106->84|111->89|111->89|112->90|112->90|114->92|114->92|115->93|115->93|118->96|118->96|120->98|120->98|121->99|121->99|125->103|125->103|125->103|125->103|128->106|128->106|129->107|129->107|135->113|135->113|138->116|138->116|141->119|141->119|141->119|141->119|146->124|146->124|147->125|147->125|152->130|152->130|155->133|155->133|171->149|171->149|172->150|172->150|177->155|177->155|178->156|178->156|180->158|180->158|180->158|184->162|184->162|187->165|187->165|190->168|190->168|191->169|191->169|193->171|193->171|196->174|196->174|199->177|199->177|201->179|201->179|202->180|202->180|203->181|203->181|220->198|220->198|220->198|235->213|242->220|242->220|242->220|244->222
                    -- GENERATED --
                */
            