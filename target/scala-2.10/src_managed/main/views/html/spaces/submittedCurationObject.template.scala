
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object submittedCurationObject extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[models.CurationObject,List[CurationFile],List[models.Metadata],Int,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObject: models.CurationObject, files :List[CurationFile], m: List[models.Metadata], limit:Int, spaceName: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import helper._

import _root_.util.Formatters._

implicit def /*6.4*/implicitFieldConstructor/*6.28*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.160*/("""
"""),format.raw/*5.1*/("""
  """),format.raw/*6.77*/("""

  """),_display_(Seq[Any](/*8.4*/main(curationObject.name)/*8.29*/ {_display_(Seq[Any](format.raw/*8.31*/("""
    <div class="row">
        <ol class="breadcrumb">
            <li> <a href=""""),_display_(Seq[Any](/*11.28*/routes/*11.34*/.Spaces.getSpace(curationObject.space))),format.raw/*11.72*/("""" title=""""),_display_(Seq[Any](/*11.82*/spaceName)),format.raw/*11.91*/("""">"""),_display_(Seq[Any](/*11.94*/Html(ellipsize(spaceName, 18)))),format.raw/*11.124*/("""</a></li>
            <li> <a href=""""),_display_(Seq[Any](/*12.28*/routes/*12.34*/.Spaces.stagingArea(curationObject.space))),format.raw/*12.75*/(""""> Staging Area</a></li>
            <li> Submitted """),_display_(Seq[Any](/*13.29*/Messages("curationobject.label"))),format.raw/*13.61*/("""</li>
        </ol>
    </div>

    <div class="container row">
      <div class="col-md-6">
      <div>
        <h2>"""),_display_(Seq[Any](/*20.14*/curationObject/*20.28*/.name)),format.raw/*20.33*/("""</h2>
        <p class= "inline"><b>"""),_display_(Seq[Any](/*21.32*/Messages("publicationrequest.description"))),format.raw/*21.74*/(""":</b></p>
        <p class= "inline abstract
        """),_display_(Seq[Any](/*23.10*/if(curationObject.description.contains("\n"))/*23.55*/ {_display_(Seq[Any](format.raw/*23.57*/("""
        	  abstract-panel
        	""")))})),format.raw/*25.11*/("""
        "><br/>
        """),_display_(Seq[Any](/*27.10*/Html(curationObject.description.replace("\n","<br>")))),format.raw/*27.63*/("""</p>
        <br/><br/>
        <span><b>Creator(s)</b>: 
          	"""),_display_(Seq[Any](/*30.13*/if(curationObject.creators.length !=0)/*30.51*/ {_display_(Seq[Any](format.raw/*30.53*/("""
       			"""),_display_(Seq[Any](/*31.12*/for(i <- 0 to (curationObject.creators.length  - 2) ) yield /*31.65*/ {_display_(Seq[Any](format.raw/*31.67*/("""
           			<span class="creator" >
           				<span class="authname person  break-word" data-creator=""""),_display_(Seq[Any](/*33.73*/curationObject/*33.87*/.creators(i).trim)),format.raw/*33.104*/("""">"""),_display_(Seq[Any](/*33.107*/curationObject/*33.121*/.creators(i).trim)),format.raw/*33.138*/("""</span>
           				<span class="glyphicon-remove creator-delete hiddencomplete"></span>
          				<span>, </span>
           			</span>
       			""")))})),format.raw/*37.12*/("""
           		<span class="creator" >
              		<span class="authname person  break-word" data-creator=""""),_display_(Seq[Any](/*39.74*/curationObject/*39.88*/.creators(curationObject.creators.length-1).trim)),format.raw/*39.136*/("""">"""),_display_(Seq[Any](/*39.139*/curationObject/*39.153*/.creators(curationObject.creators.length-1).trim)),format.raw/*39.201*/("""</span>
              		<span class="glyphicon-remove creator-delete hiddencomplete"></span>
   	        	</span>
           	""")))})),format.raw/*42.14*/("""	
        </span><br/>
        
        <b>"""),_display_(Seq[Any](/*45.13*/Messages("owner.label"))),format.raw/*45.36*/("""</b>: """),_display_(Seq[Any](/*45.43*/curationObject/*45.57*/.author.fullName)),format.raw/*45.73*/(""" <br/>
        <b>Created</b>: """),_display_(Seq[Any](/*46.26*/curationObject/*46.40*/.created.date.format("MMM dd, yyyy"))),format.raw/*46.76*/(""" <br/>
        <b>Repository</b>: """),_display_(Seq[Any](/*47.29*/curationObject/*47.43*/.repository.getOrElse("Not selected."))),format.raw/*47.81*/(""" <br/>
        <b>Submitted</b>: """),_display_(Seq[Any](/*48.28*/{curationObject.submittedDate match {
        case Some(d) => d.format("MMM dd, yyyy")
        case None => "Not submitted"
      }
      })),format.raw/*52.8*/("""  <br/>
        <b>Published</b>: """),_display_(Seq[Any](/*53.28*/{curationObject.publishedDate match {
        case Some(d) => d.format("MMM dd, yyyy")
        case None => "Not published."
      }
      })),format.raw/*57.8*/("""  <br/>

        <b>External Identifier</b>: """),_display_(Seq[Any](/*59.38*/curationObject/*59.52*/.externalIdentifier/*59.71*/ match/*59.77*/ {/*60.9*/case Some(e) =>/*60.24*/ {_display_(Seq[Any](format.raw/*60.26*/("""
          """),_display_(Seq[Any](/*61.12*/if(e.toString.startsWith("http://") || e.toString.startsWith("https://"))/*61.85*/ {_display_(Seq[Any](format.raw/*61.87*/("""
          <a href="""),_display_(Seq[Any](/*62.20*/e)),format.raw/*62.21*/(""">"""),_display_(Seq[Any](/*62.23*/e)),format.raw/*62.24*/("""</a>
             """)))}/*63.16*/else/*63.21*/{_display_(Seq[Any](format.raw/*63.22*/("""
           """),_display_(Seq[Any](/*64.13*/e)),format.raw/*64.14*/("""
          """)))})),format.raw/*65.12*/("""
        """)))}/*67.9*/case None =>/*67.21*/ {_display_(Seq[Any](format.raw/*67.23*/("""Not Set""")))}})),format.raw/*68.8*/("""

        <br/>
        <h4> """),_display_(Seq[Any](/*71.15*/Messages("dataset.title"))),format.raw/*71.40*/(""" <a href =""""),_display_(Seq[Any](/*71.52*/routes/*71.58*/.Datasets.dataset(curationObject.datasets(0).id))),format.raw/*71.106*/("""">"""),_display_(Seq[Any](/*71.109*/curationObject/*71.123*/.datasets(0).name)),format.raw/*71.140*/("""</a></h4>
        Note: The links in this page redirect to the live objects.<br/>
        <b>Size</b>: """),_display_(Seq[Any](/*73.23*/{files.map(_.length).sum /1024})),format.raw/*73.54*/(""" KB <br/>
        <b>File Formats</b>: """),_display_(Seq[Any](/*74.31*/files/*74.36*/.map(_.contentType).toSet.mkString(", "))),format.raw/*74.76*/(""" <br/>
        <b>"""),_display_(Seq[Any](/*75.13*/Messages("owner.label"))),format.raw/*75.36*/("""</b>: """),_display_(Seq[Any](/*75.43*/curationObject/*75.57*/.datasets(0).author.fullName)),format.raw/*75.85*/(""" <br/>


        """),format.raw/*78.130*/("""
        <div class="row">
          <div class="col-md-12 box-white-space">
            """),_display_(Seq[Any](/*81.14*/if(m.filter(_.attachedTo.resourceType == ResourceRef.curationObject).size > 0)/*81.92*/ {_display_(Seq[Any](format.raw/*81.94*/("""
           	  <h4>Metadata</h4>
            """)))})),format.raw/*83.14*/("""
            <div  class='usr_md_'>
            """),_display_(Seq[Any](/*85.14*/metadatald/*85.24*/.view(m.filter(_.attachedTo.resourceType == ResourceRef.curationObject), false))),format.raw/*85.103*/("""
            </div>
          </div>
        </div>
        """),format.raw/*89.14*/("""
      </div>
          <div id="files"></div>


    </div>

    <div class="col-md-6">
    <b>Submission Status in repository</b><button id="status" class ="btn btn-link btn-xs"><span class="glyphicon glyphicon-send"></span> Update</button>
    <br/><br/>
    <div id="showStatus"></div>
    </div>
    </div>
    <script src=""""),_display_(Seq[Any](/*102.19*/routes/*102.25*/.Assets.at("javascripts/curationProcess.js"))),format.raw/*102.69*/("""" type="text/javascript"></script>
    <script>
            var removeIndicator = false;
            var parentId = """"),_display_(Seq[Any](/*105.30*/curationObject/*105.44*/.id)),format.raw/*105.47*/("""";
            var parentType = "dataset";
            var pageIndex = 0;
            var folderId;

            $(document).ready(function()"""),format.raw/*110.41*/("""{"""),format.raw/*110.42*/("""
                getUpdatedFilesAndFolders('"""),_display_(Seq[Any](/*111.45*/curationObject/*111.59*/.id)),format.raw/*111.62*/("""', """),_display_(Seq[Any](/*111.66*/limit)),format.raw/*111.71*/(""");
            """),format.raw/*112.13*/("""}"""),format.raw/*112.14*/(""");
            $(window).on('fileDelete hashchange', function() """),format.raw/*113.62*/("""{"""),format.raw/*113.63*/("""
                getUpdatedFilesAndFolders('"""),_display_(Seq[Any](/*114.45*/curationObject/*114.59*/.id)),format.raw/*114.62*/("""', """),_display_(Seq[Any](/*114.66*/limit)),format.raw/*114.71*/(""");
            """),format.raw/*115.13*/("""}"""),format.raw/*115.14*/(""");
      $(document).ready(getStatus('"""),_display_(Seq[Any](/*116.37*/curationObject/*116.51*/.id)),format.raw/*116.54*/("""'));

      $('#status').click(function()"""),format.raw/*118.36*/("""{"""),format.raw/*118.37*/("""
        getStatus('"""),_display_(Seq[Any](/*119.21*/curationObject/*119.35*/.id)),format.raw/*119.38*/("""');
      """),format.raw/*120.7*/("""}"""),format.raw/*120.8*/(""");

      function getStatus(id)"""),format.raw/*122.29*/("""{"""),format.raw/*122.30*/("""
        var request = jsRoutes.controllers.CurationObjects.getStatusFromRepository(id).ajax("""),format.raw/*123.93*/("""{"""),format.raw/*123.94*/("""
          type : 'GET',
          contentType : "application/json"
         """),format.raw/*126.10*/("""}"""),format.raw/*126.11*/(""");
        request.done ( function ( response, textStatus, jqXHR) """),format.raw/*127.64*/("""{"""),format.raw/*127.65*/("""
        console.log("get status: "+ jqXHR.responseText );

        var o =response.Status;
        var table = document.createElement('table');
        table.classList.add('table');
        table.classList.add('table-bordered');
        table.id ='showStatus';

        var thead = document.createElement('thead');
        """),format.raw/*137.9*/("""{"""),format.raw/*137.10*/("""
          var tr = document.createElement('tr');
          var th1 = document.createElement('th');
          var th2 = document.createElement('th');
          var th3 = document.createElement('th');
          var th4 = document.createElement('th');

          var text1 = document.createTextNode("Date");
          var text2 = document.createTextNode("Reporter");
          var text3 = document.createTextNode("Stage");
          var text4 = document.createTextNode("Message");

          th1.appendChild(text1);
          th2.appendChild(text2);
          th3.appendChild(text3);
          th4.appendChild(text4);
          tr.appendChild(th1);
          tr.appendChild(th2);
          tr.appendChild(th3);
          tr.appendChild(th4);

          thead.appendChild(tr);
        """),format.raw/*159.9*/("""}"""),format.raw/*159.10*/("""
        var tbody = document.createElement('tbody');
        for ( var i = 0, l = o.length; i < l; i++)"""),format.raw/*161.51*/("""{"""),format.raw/*161.52*/("""
         var tr = document.createElement('tr');

          var td1 = document.createElement('td');
          var td2 = document.createElement('td');
          var td3 = document.createElement('td');
          var td4 = document.createElement('td');

          var text1 = document.createTextNode(o[i].date);
          var text2 = document.createTextNode(o[i].reporter);
          var text3 = document.createTextNode(o[i].stage);
          var text4 = document.createTextNode(o[i].message);

          td1.appendChild(text1);
          td2.appendChild(text2);
          td3.appendChild(text3);
          td4.appendChild(text4);

          tr.appendChild(td1);
          tr.appendChild(td2);
          tr.appendChild(td3);
          tr.appendChild(td4);

          tbody.appendChild(tr);
        """),format.raw/*185.9*/("""}"""),format.raw/*185.10*/("""
        table.classList.add("fixedtable");
        table.setAttribute('width','100%');
        table.appendChild(thead);
        table.appendChild(tbody);
        $('#showStatus').replaceWith(table);
      """),format.raw/*191.7*/("""}"""),format.raw/*191.8*/(""");
      request.fail(function(jqXHR) """),format.raw/*192.36*/("""{"""),format.raw/*192.37*/("""
          console.error("Error getting status from repository");
          $('#showStatus').replaceWith("<p id='showStatus'>No status available. Try again later.</p>");
      """),format.raw/*195.7*/("""}"""),format.raw/*195.8*/(""");
    """),format.raw/*196.5*/("""}"""),format.raw/*196.6*/("""

    </script>
  """)))})))}
    }
    
    def render(curationObject:models.CurationObject,files:List[CurationFile],m:List[models.Metadata],limit:Int,spaceName:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObject,files,m,limit,spaceName)(user)
    
    def f:((models.CurationObject,List[CurationFile],List[models.Metadata],Int,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObject,files,m,limit,spaceName) => (user) => apply(curationObject,files,m,limit,spaceName)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/submittedCurationObject.scala.html
                    HASH: e2a0c4702cf24374b5ae2774280179fcbf4fd181
                    MATRIX: 699->1|1023->245|1055->269|1135->159|1162->241|1192->318|1231->323|1264->348|1303->350|1421->432|1436->438|1496->476|1542->486|1573->495|1612->498|1665->528|1738->565|1753->571|1816->612|1905->665|1959->697|2113->815|2136->829|2163->834|2236->871|2300->913|2390->967|2444->1012|2484->1014|2553->1051|2615->1077|2690->1130|2796->1200|2843->1238|2883->1240|2931->1252|3000->1305|3040->1307|3187->1418|3210->1432|3250->1449|3290->1452|3314->1466|3354->1483|3541->1638|3688->1749|3711->1763|3782->1811|3822->1814|3846->1828|3917->1876|4076->2003|4156->2047|4201->2070|4244->2077|4267->2091|4305->2107|4373->2139|4396->2153|4454->2189|4525->2224|4548->2238|4608->2276|4678->2310|4838->2449|4909->2484|5070->2624|5152->2670|5175->2684|5203->2703|5218->2709|5228->2720|5252->2735|5292->2737|5340->2749|5422->2822|5462->2824|5518->2844|5541->2845|5579->2847|5602->2848|5640->2868|5653->2873|5692->2874|5741->2887|5764->2888|5808->2900|5836->2919|5857->2931|5897->2933|5937->2949|6003->2979|6050->3004|6098->3016|6113->3022|6184->3070|6224->3073|6248->3087|6288->3104|6428->3208|6481->3239|6557->3279|6571->3284|6633->3324|6688->3343|6733->3366|6776->3373|6799->3387|6849->3415|6895->3553|7021->3643|7108->3721|7148->3723|7226->3769|7311->3818|7330->3828|7432->3907|7520->3972|7886->4301|7902->4307|7969->4351|8124->4469|8148->4483|8174->4486|8344->4627|8374->4628|8456->4673|8480->4687|8506->4690|8547->4694|8575->4699|8619->4714|8649->4715|8742->4779|8772->4780|8854->4825|8878->4839|8904->4842|8945->4846|8973->4851|9017->4866|9047->4867|9123->4906|9147->4920|9173->4923|9243->4964|9273->4965|9331->4986|9355->5000|9381->5003|9419->5013|9448->5014|9509->5046|9539->5047|9661->5140|9691->5141|9797->5218|9827->5219|9922->5285|9952->5286|10304->5610|10334->5611|11144->6393|11174->6394|11307->6498|11337->6499|12160->7294|12190->7295|12425->7502|12454->7503|12521->7541|12551->7542|12755->7718|12784->7719|12819->7726|12848->7727
                    LINES: 20->1|27->6|27->6|28->1|29->5|30->6|32->8|32->8|32->8|35->11|35->11|35->11|35->11|35->11|35->11|35->11|36->12|36->12|36->12|37->13|37->13|44->20|44->20|44->20|45->21|45->21|47->23|47->23|47->23|49->25|51->27|51->27|54->30|54->30|54->30|55->31|55->31|55->31|57->33|57->33|57->33|57->33|57->33|57->33|61->37|63->39|63->39|63->39|63->39|63->39|63->39|66->42|69->45|69->45|69->45|69->45|69->45|70->46|70->46|70->46|71->47|71->47|71->47|72->48|76->52|77->53|81->57|83->59|83->59|83->59|83->59|83->60|83->60|83->60|84->61|84->61|84->61|85->62|85->62|85->62|85->62|86->63|86->63|86->63|87->64|87->64|88->65|89->67|89->67|89->67|89->68|92->71|92->71|92->71|92->71|92->71|92->71|92->71|92->71|94->73|94->73|95->74|95->74|95->74|96->75|96->75|96->75|96->75|96->75|99->78|102->81|102->81|102->81|104->83|106->85|106->85|106->85|110->89|123->102|123->102|123->102|126->105|126->105|126->105|131->110|131->110|132->111|132->111|132->111|132->111|132->111|133->112|133->112|134->113|134->113|135->114|135->114|135->114|135->114|135->114|136->115|136->115|137->116|137->116|137->116|139->118|139->118|140->119|140->119|140->119|141->120|141->120|143->122|143->122|144->123|144->123|147->126|147->126|148->127|148->127|158->137|158->137|180->159|180->159|182->161|182->161|206->185|206->185|212->191|212->191|213->192|213->192|216->195|216->195|217->196|217->196
                    -- GENERATED --
                */
            