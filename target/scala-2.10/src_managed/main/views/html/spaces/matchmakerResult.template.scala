
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object matchmakerResult extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[models.CurationObject,Map[String, List[String]],Map[String, List[String]],List[models.MatchMakerResponse],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObject: models.CurationObject, properties: Map[String, List[String]], userPreferences:  Map[String,List[String]], mmResponse: List[models.MatchMakerResponse])(implicit user: Option[models.User] ):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.206*/("""
"""),_display_(Seq[Any](/*2.2*/main("Matchmaker")/*2.20*/ {_display_(Seq[Any](format.raw/*2.22*/("""
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a  href =""""),_display_(Seq[Any](/*7.33*/routes/*7.39*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*7.92*/("""">Edit Metadata</a> >
                    <u><strong>Select Repository</strong></u> >
                    Submit to Repository
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <a class="btn btn-default" href =""""),_display_(Seq[Any](/*16.48*/routes/*16.54*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*16.107*/(""""><span class="glyphicon glyphicon-chevron-left"></span> Edit Metadata</a>
        </div>
        <div class="col-md-8 text-center">
            <h1>Select Repository</h1>
        </div>
        <div class="col-md-2">
            <button  class="btn btn-primary pull-right selectRepository">Submit to Repository <span class="glyphicon glyphicon-chevron-right"></span></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            """),_display_(Seq[Any](/*27.14*/spaces/*27.20*/.curationSummary( curationObject, false))),format.raw/*27.60*/("""
            <div class="panel-body">
                <h4>Preferences</h4>
                """),_display_(Seq[Any](/*30.18*/for((prop, value) <- properties) yield /*30.50*/ {_display_(Seq[Any](format.raw/*30.52*/("""
                    <h5>"""),_display_(Seq[Any](/*31.26*/prop)),format.raw/*31.30*/("""</h5>
                    """),_display_(Seq[Any](/*32.22*/if(prop.equalsIgnoreCase("Purpose"))/*32.58*/ {_display_(Seq[Any](format.raw/*32.60*/("""
                        <select id=""""),_display_(Seq[Any](/*33.38*/prop)),format.raw/*33.42*/("""" class="form-control">
                        """),_display_(Seq[Any](/*34.26*/for(cur<-value) yield /*34.41*/ {_display_(Seq[Any](format.raw/*34.43*/("""
                            """),_display_(Seq[Any](/*35.30*/if((userPreferences.contains(prop.replace(" ", "_")) && userPreferences(prop.replace(" ", "_")).contains(cur)))/*35.141*/ {_display_(Seq[Any](format.raw/*35.143*/("""
                                <option value=""""),_display_(Seq[Any](/*36.49*/cur)),format.raw/*36.52*/("""" selected>"""),_display_(Seq[Any](/*36.64*/cur)),format.raw/*36.67*/("""</option>
                            """)))}/*37.31*/else/*37.36*/{_display_(Seq[Any](format.raw/*37.37*/("""
                                <option value=""""),_display_(Seq[Any](/*38.49*/cur)),format.raw/*38.52*/("""">"""),_display_(Seq[Any](/*38.55*/cur)),format.raw/*38.58*/("""</option>
                            """)))})),format.raw/*39.30*/("""

                        """)))})),format.raw/*41.26*/("""
                        </select>
                    """)))}/*43.23*/else/*43.28*/{_display_(Seq[Any](format.raw/*43.29*/("""
                        """),_display_(Seq[Any](/*44.26*/for(cur<-value) yield /*44.41*/ {_display_(Seq[Any](format.raw/*44.43*/("""
                            """),_display_(Seq[Any](/*45.30*/if((userPreferences.contains(prop.replace(" ", "_")) && userPreferences(prop.replace(" ", "_")).contains(cur)))/*45.141*/ {_display_(Seq[Any](format.raw/*45.143*/("""
                                <input type="checkbox" name=""""),_display_(Seq[Any](/*46.63*/prop)),format.raw/*46.67*/("""" value=""""),_display_(Seq[Any](/*46.77*/cur)),format.raw/*46.80*/("""" checked> """),_display_(Seq[Any](/*46.92*/cur)),format.raw/*46.95*/(""" <br/>

                            """)))}/*48.31*/else/*48.36*/{_display_(Seq[Any](format.raw/*48.37*/("""
                                <input type="checkbox" name=""""),_display_(Seq[Any](/*49.63*/prop)),format.raw/*49.67*/("""" value=""""),_display_(Seq[Any](/*49.77*/cur)),format.raw/*49.80*/(""""> """),_display_(Seq[Any](/*49.84*/cur)),format.raw/*49.87*/(""" <br/>
                            """)))})),format.raw/*50.30*/("""

                        """)))})),format.raw/*52.26*/("""
                    """)))})),format.raw/*53.22*/("""
                """)))})),format.raw/*54.18*/("""

                <div class="center-margin">
                    <button class="btn btn-sm btn-link" id="updateResults"><span class="glyphicon glyphicon-send"></span> Update Results</button>
                </div>

            </div>
        </div>
        <div class="col-md-9">
                <h3 id="candidate">Candidate Repositories</h3>
            """),_display_(Seq[Any](/*64.14*/if(mmResponse.size >0 )/*64.37*/ {_display_(Seq[Any](format.raw/*64.39*/("""
                The results below are based on an analysis of the dataset's properties and metadata and the preferences you specified. <br/>
            """)))}/*66.15*/else/*66.20*/{_display_(Seq[Any](format.raw/*66.21*/("""
                There was an error calling Matchmaker or there were no match's returned <br/>
            """)))})),format.raw/*68.14*/("""

            <span class="error hiddencomplete" id="selecterror"> Please select a repository</span><br>
            <div id="mmresults"></div>

        </div>

    </div>
   <div class="row bottom-padding">
        <div class="col-md-12">
            <a class="btn btn-default" href =""""),_display_(Seq[Any](/*78.48*/routes/*78.54*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*78.107*/(""""><span class="glyphicon glyphicon-chevron-left"></span> Edit Metadata</a>
            <button class="btn btn-primary pull-right selectRepository">Submit to Repository<span class="glyphicon glyphicon-chevron-right"></span></button>
        </div>
    </div>
    <form id="selectRepository" action='"""),_display_(Seq[Any](/*82.42*/routes/*82.48*/.CurationObjects.submitRepositorySelection(curationObject.id))),format.raw/*82.109*/("""' method="POST" enctype="multipart/form-data">
            <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript>Javascript is required in order to use the uploader to create a new collection.</noscript>

        <input type="hidden" name="repository" id="hiddenRepository" value="not set">
        <input type="hidden" name="purpose" id="hiddenPurpose" value="not set">
    </form>
    <script src=""""),_display_(Seq[Any](/*89.19*/routes/*89.25*/.Assets.at("javascripts/handlebars-v1.3.0.js"))),format.raw/*89.71*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*90.19*/routes/*90.25*/.Assets.at("javascripts/handlebars-loader.js"))),format.raw/*90.71*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*91.19*/routes/*91.25*/.Assets.at("javascripts/people.js"))),format.raw/*91.60*/("""" type="text/javascript"></script>
    <script>

        Handlebars.registerHelper('nomorethan', function(lvalue, rvalue, options) """),format.raw/*94.83*/("""{"""),format.raw/*94.84*/("""
            if (arguments.length < 3)
                throw new Error("Handlebars Helper equal needs 2 parameters");
            if( lvalue>rvalue ) """),format.raw/*97.33*/("""{"""),format.raw/*97.34*/("""
                return options.inverse(this);
            """),format.raw/*99.13*/("""}"""),format.raw/*99.14*/(""" else """),format.raw/*99.20*/("""{"""),format.raw/*99.21*/("""
                return options.fn(this);
            """),format.raw/*101.13*/("""}"""),format.raw/*101.14*/("""
        """),format.raw/*102.9*/("""}"""),format.raw/*102.10*/(""");
        Handlebars.registerHelper('ifCond', function(v1, v2, options) """),format.raw/*103.71*/("""{"""),format.raw/*103.72*/("""
            if(v1 == v2) """),format.raw/*104.26*/("""{"""),format.raw/*104.27*/("""
                return options.fn(this);
            """),format.raw/*106.13*/("""}"""),format.raw/*106.14*/("""
            return options.inverse(this);
        """),format.raw/*108.9*/("""}"""),format.raw/*108.10*/(""");

        Handlebars.registerHelper('contains', function(string, sub_string, options)"""),format.raw/*110.84*/("""{"""),format.raw/*110.85*/("""
            if(string.toLowerCase().indexOf(sub_string.toLowerCase()) >=0) """),format.raw/*111.76*/("""{"""),format.raw/*111.77*/("""
                return options.fn(this);
            """),format.raw/*113.13*/("""}"""),format.raw/*113.14*/(""" else """),format.raw/*113.20*/("""{"""),format.raw/*113.21*/("""
                return options.inverse(this);
            """),format.raw/*115.13*/("""}"""),format.raw/*115.14*/("""

        """),format.raw/*117.9*/("""}"""),format.raw/*117.10*/(""");

        var repositoryTemplate = Handlebars.getTemplate('"""),_display_(Seq[Any](/*119.59*/routes/*119.65*/.Assets.at("templates/spaces/mmrepository"))),format.raw/*119.108*/("""');
        """),_display_(Seq[Any](/*120.10*/for(curRepository <- mmResponse) yield /*120.42*/ {_display_(Seq[Any](format.raw/*120.44*/("""
            var rules = [];
            var match = true;
            """),_display_(Seq[Any](/*123.14*/for(rule <- curRepository.per_rule_score) yield /*123.55*/ {_display_(Seq[Any](format.raw/*123.57*/("""
                rules.push("""),format.raw/*124.28*/("""{"""),format.raw/*124.29*/("""rule_name: """"),_display_(Seq[Any](/*124.42*/rule/*124.46*/.rule_name)),format.raw/*124.56*/("""", score: """"),_display_(Seq[Any](/*124.68*/rule/*124.72*/.Score)),format.raw/*124.78*/("""", message: """"),_display_(Seq[Any](/*124.92*/rule/*124.96*/.Message)),format.raw/*124.104*/("""""""),format.raw/*124.105*/("""}"""),format.raw/*124.106*/(""");
                """),_display_(Seq[Any](/*125.18*/if(rule.Score < 0)/*125.36*/ {_display_(Seq[Any](format.raw/*125.38*/("""
                 match = false;
                """)))})),format.raw/*127.18*/("""
            """)))})),format.raw/*128.14*/("""
            console.log(match);
            """),_display_(Seq[Any](/*130.14*/if(curationObject.repository.isDefined && curationObject.repository.get.compareTo(curRepository.orgidentifier) == 0)/*130.130*/ {_display_(Seq[Any](format.raw/*130.132*/("""
                var html =repositoryTemplate("""),format.raw/*131.46*/("""{"""),format.raw/*131.47*/("""href_metadata: """"),_display_(Seq[Any](/*131.64*/routes/*131.70*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*131.123*/("""", href_profile: """"),_display_(Seq[Any](/*131.142*/routes/*131.148*/.Profile.viewProfileUUID(curationObject.author.id))),format.raw/*131.198*/("""", repositoryName: new Handlebars.SafeString(""""),_display_(Seq[Any](/*131.245*/curRepository/*131.258*/.repositoryName)),format.raw/*131.273*/(""""), is_match:match, orgidentifier: """"),_display_(Seq[Any](/*131.310*/curRepository/*131.323*/.orgidentifier)),format.raw/*131.337*/("""", per_rule_score: rules,  selected: true"""),format.raw/*131.378*/("""}"""),format.raw/*131.379*/(""");
                $('#mmresults').append(html);
            """)))}/*133.15*/else/*133.20*/{_display_(Seq[Any](format.raw/*133.21*/("""

                var html =repositoryTemplate("""),format.raw/*135.46*/("""{"""),format.raw/*135.47*/("""href_metadata: """"),_display_(Seq[Any](/*135.64*/routes/*135.70*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*135.123*/("""", href_profile: """"),_display_(Seq[Any](/*135.142*/routes/*135.148*/.Profile.viewProfileUUID(curationObject.author.id))),format.raw/*135.198*/("""", repositoryName: new Handlebars.SafeString(""""),_display_(Seq[Any](/*135.245*/curRepository/*135.258*/.repositoryName)),format.raw/*135.273*/(""""), is_match:match, orgidentifier: """"),_display_(Seq[Any](/*135.310*/curRepository/*135.323*/.orgidentifier)),format.raw/*135.337*/("""", per_rule_score: rules, selected: false"""),format.raw/*135.378*/("""}"""),format.raw/*135.379*/(""");
                $('#mmresults').append(html);
            """)))})),format.raw/*137.14*/("""
        """)))})),format.raw/*138.10*/("""
        $('.matchmaker_link').on('click', function() """),format.raw/*139.54*/("""{"""),format.raw/*139.55*/("""
            if($(this).find('span').attr("class").indexOf("glyphicon-plus") > 0) """),format.raw/*140.82*/("""{"""),format.raw/*140.83*/("""
                $(this).find('span').removeClass("glyphicon-plus");
                $(this).find('span').addClass("glyphicon-minus");
            """),format.raw/*143.13*/("""}"""),format.raw/*143.14*/(""" else """),format.raw/*143.20*/("""{"""),format.raw/*143.21*/("""
                $(this).find('span').removeClass("glyphicon-minus");
                $(this).find('span').addClass("glyphicon-plus");
            """),format.raw/*146.13*/("""}"""),format.raw/*146.14*/("""
        """),format.raw/*147.9*/("""}"""),format.raw/*147.10*/(""");

        $('#updateResults').on('click', function() """),format.raw/*149.52*/("""{"""),format.raw/*149.53*/("""

            var dataMap = buildDataMap();

            var request = jsRoutes.api.CurationObjects.findMatchmakingRepositories('"""),_display_(Seq[Any](/*153.86*/curationObject/*153.100*/.id)),format.raw/*153.103*/("""').ajax("""),format.raw/*153.111*/("""{"""),format.raw/*153.112*/("""
                type: 'POST',
                data: JSON.stringify("""),format.raw/*155.38*/("""{"""),format.raw/*155.39*/(""""data": dataMap"""),format.raw/*155.54*/("""}"""),format.raw/*155.55*/("""),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8'
            """),format.raw/*158.13*/("""}"""),format.raw/*158.14*/(""");

            request.done(function(response, textStatus, jqXHR) """),format.raw/*160.64*/("""{"""),format.raw/*160.65*/("""
                console.log("Updated Data in Preferences");

                var repository = $("input[type='radio'][name='repository']:checked").val();
                $('#mmresults').html("");

                for(i in response) """),format.raw/*166.36*/("""{"""),format.raw/*166.37*/("""
                    var sel = repository == response[i].orgidentifier;
                    var match = true;
                    for(var j=0; j<response[i].per_rule_score.length; j++) """),format.raw/*169.76*/("""{"""),format.raw/*169.77*/("""
                        if(response[i].per_rule_score[j].score < 0) """),format.raw/*170.69*/("""{"""),format.raw/*170.70*/("""
                            match = false;
                        """),format.raw/*172.25*/("""}"""),format.raw/*172.26*/("""

                    """),format.raw/*174.21*/("""}"""),format.raw/*174.22*/("""

                    console.log(match);
                    var html  = repositoryTemplate("""),format.raw/*177.52*/("""{"""),format.raw/*177.53*/("""href_metadata: """"),_display_(Seq[Any](/*177.70*/routes/*177.76*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*177.129*/("""", repositoryName:response[i].repositoryName, is_match: match, orgidentifier: response[i].orgidentifier, per_rule_score: response[i].per_rule_score, selected: repository == response[i].orgidentifier"""),format.raw/*177.327*/("""}"""),format.raw/*177.328*/(""");
                    $('#mmresults').append(html);

                """),format.raw/*180.17*/("""}"""),format.raw/*180.18*/("""

            """),format.raw/*182.13*/("""}"""),format.raw/*182.14*/(""");
            request.fail(function( jqXHR, textStatus, errorThrown) """),format.raw/*183.68*/("""{"""),format.raw/*183.69*/("""
                console.error("The following error occurred: " + textStatus, errorThrown);
                notify("Preferences were not updated due to:"+ jqXHR.responseText, "error");
            """),format.raw/*186.13*/("""}"""),format.raw/*186.14*/(""");
        """),format.raw/*187.9*/("""}"""),format.raw/*187.10*/(""");

        function buildDataMap()"""),format.raw/*189.32*/("""{"""),format.raw/*189.33*/("""
            var purpose = $('#Purpose').val();

            var dataMap="""),format.raw/*192.25*/("""{"""),format.raw/*192.26*/("""}"""),format.raw/*192.27*/(""";
            if(purpose !="")
                dataMap ["Purpose"]= purpose.toString();

            return dataMap;

        """),format.raw/*198.9*/("""}"""),format.raw/*198.10*/("""

        $('.selectRepository').on('click', function() """),format.raw/*200.55*/("""{"""),format.raw/*200.56*/("""
            var radios = document.getElementsByName('repository');
            var repository = "";
            for (var i = 0, length = radios.length; i < length; i++) """),format.raw/*203.70*/("""{"""),format.raw/*203.71*/("""
                 if (radios[i].checked) """),format.raw/*204.41*/("""{"""),format.raw/*204.42*/("""
                    repository = radios[i].value;
                 break;
                """),format.raw/*207.17*/("""}"""),format.raw/*207.18*/("""
            """),format.raw/*208.13*/("""}"""),format.raw/*208.14*/("""

            if(repository == "")"""),format.raw/*210.33*/("""{"""),format.raw/*210.34*/("""
                $('#selecterror').show();

                $('html, body').animate("""),format.raw/*213.41*/("""{"""),format.raw/*213.42*/("""
                    scrollTop: $('#candidate').offset().top + 'px'
                """),format.raw/*215.17*/("""}"""),format.raw/*215.18*/(""", 'fast');

            """),format.raw/*217.13*/("""}"""),format.raw/*217.14*/(""" else"""),format.raw/*217.19*/("""{"""),format.raw/*217.20*/("""
                var dataMap = buildDataMap();
                $('#hiddenRepository').val(repository);
                $('#hiddenPurpose').val(dataMap["Purpose"]);
                $('#selectRepository').submit();

            """),format.raw/*223.13*/("""}"""),format.raw/*223.14*/("""
        """),format.raw/*224.9*/("""}"""),format.raw/*224.10*/(""");

        $(document).ready(function()"""),format.raw/*226.37*/("""{"""),format.raw/*226.38*/("""
            $("#Purpose").select2("""),format.raw/*227.35*/("""{"""),format.raw/*227.36*/(""" minimumResultsForSearch: -1"""),format.raw/*227.64*/("""}"""),format.raw/*227.65*/(""");

        """),format.raw/*229.9*/("""}"""),format.raw/*229.10*/(""");

    </script>
""")))})))}
    }
    
    def render(curationObject:models.CurationObject,properties:Map[String, List[String]],userPreferences:Map[String, List[String]],mmResponse:List[models.MatchMakerResponse],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObject,properties,userPreferences,mmResponse)(user)
    
    def f:((models.CurationObject,Map[String, List[String]],Map[String, List[String]],List[models.MatchMakerResponse]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObject,properties,userPreferences,mmResponse) => (user) => apply(curationObject,properties,userPreferences,mmResponse)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/matchmakerResult.scala.html
                    HASH: 480905dd251003dd3428ea90f0c3d6876e656dd7
                    MATRIX: 724->1|1023->205|1059->207|1085->225|1124->227|1333->401|1347->407|1421->460|1752->755|1767->761|1843->814|2348->1283|2363->1289|2425->1329|2553->1421|2601->1453|2641->1455|2703->1481|2729->1485|2792->1512|2837->1548|2877->1550|2951->1588|2977->1592|3062->1641|3093->1656|3133->1658|3199->1688|3320->1799|3361->1801|3446->1850|3471->1853|3519->1865|3544->1868|3602->1908|3615->1913|3654->1914|3739->1963|3764->1966|3803->1969|3828->1972|3899->2011|3958->2038|4033->2095|4046->2100|4085->2101|4147->2127|4178->2142|4218->2144|4284->2174|4405->2285|4446->2287|4545->2350|4571->2354|4617->2364|4642->2367|4690->2379|4715->2382|4771->2420|4784->2425|4823->2426|4922->2489|4948->2493|4994->2503|5019->2506|5059->2510|5084->2513|5152->2549|5211->2576|5265->2598|5315->2616|5708->2973|5740->2996|5780->2998|5954->3154|5967->3159|6006->3160|6146->3268|6469->3555|6484->3561|6560->3614|6895->3913|6910->3919|6994->3980|7466->4416|7481->4422|7549->4468|7638->4521|7653->4527|7721->4573|7810->4626|7825->4632|7882->4667|8041->4798|8070->4799|8248->4949|8277->4950|8364->5009|8393->5010|8427->5016|8456->5017|8539->5071|8569->5072|8606->5081|8636->5082|8738->5155|8768->5156|8823->5182|8853->5183|8936->5237|8966->5238|9045->5289|9075->5290|9191->5377|9221->5378|9326->5454|9356->5455|9439->5509|9469->5510|9504->5516|9534->5517|9622->5576|9652->5577|9690->5587|9720->5588|9819->5650|9835->5656|9902->5699|9952->5712|10001->5744|10042->5746|10151->5818|10209->5859|10250->5861|10307->5889|10337->5890|10387->5903|10401->5907|10434->5917|10483->5929|10497->5933|10526->5939|10577->5953|10591->5957|10623->5965|10654->5966|10685->5967|10742->5987|10770->6005|10811->6007|10894->6057|10941->6071|11024->6117|11151->6233|11193->6235|11268->6281|11298->6282|11352->6299|11368->6305|11445->6358|11502->6377|11519->6383|11593->6433|11678->6480|11702->6493|11741->6508|11816->6545|11840->6558|11878->6572|11949->6613|11980->6614|12062->6677|12076->6682|12116->6683|12192->6730|12222->6731|12276->6748|12292->6754|12369->6807|12426->6826|12443->6832|12517->6882|12602->6929|12626->6942|12665->6957|12740->6994|12764->7007|12802->7021|12873->7062|12904->7063|12999->7125|13042->7135|13125->7189|13155->7190|13266->7272|13296->7273|13472->7420|13502->7421|13537->7427|13567->7428|13743->7575|13773->7576|13810->7585|13840->7586|13924->7641|13954->7642|14121->7772|14146->7786|14173->7789|14211->7797|14242->7798|14339->7866|14369->7867|14413->7882|14443->7883|14584->7995|14614->7996|14710->8063|14740->8064|15001->8296|15031->8297|15245->8482|15275->8483|15373->8552|15403->8553|15500->8621|15530->8622|15581->8644|15611->8645|15733->8738|15763->8739|15817->8756|15833->8762|15910->8815|16138->9013|16169->9014|16268->9084|16298->9085|16341->9099|16371->9100|16470->9170|16500->9171|16726->9368|16756->9369|16795->9380|16825->9381|16889->9416|16919->9417|17021->9490|17051->9491|17081->9492|17235->9618|17265->9619|17350->9675|17380->9676|17579->9846|17609->9847|17679->9888|17709->9889|17829->9980|17859->9981|17901->9994|17931->9995|17994->10029|18024->10030|18137->10114|18167->10115|18280->10199|18310->10200|18363->10224|18393->10225|18427->10230|18457->10231|18712->10457|18742->10458|18779->10467|18809->10468|18878->10508|18908->10509|18972->10544|19002->10545|19059->10573|19089->10574|19129->10586|19159->10587
                    LINES: 20->1|23->1|24->2|24->2|24->2|29->7|29->7|29->7|38->16|38->16|38->16|49->27|49->27|49->27|52->30|52->30|52->30|53->31|53->31|54->32|54->32|54->32|55->33|55->33|56->34|56->34|56->34|57->35|57->35|57->35|58->36|58->36|58->36|58->36|59->37|59->37|59->37|60->38|60->38|60->38|60->38|61->39|63->41|65->43|65->43|65->43|66->44|66->44|66->44|67->45|67->45|67->45|68->46|68->46|68->46|68->46|68->46|68->46|70->48|70->48|70->48|71->49|71->49|71->49|71->49|71->49|71->49|72->50|74->52|75->53|76->54|86->64|86->64|86->64|88->66|88->66|88->66|90->68|100->78|100->78|100->78|104->82|104->82|104->82|111->89|111->89|111->89|112->90|112->90|112->90|113->91|113->91|113->91|116->94|116->94|119->97|119->97|121->99|121->99|121->99|121->99|123->101|123->101|124->102|124->102|125->103|125->103|126->104|126->104|128->106|128->106|130->108|130->108|132->110|132->110|133->111|133->111|135->113|135->113|135->113|135->113|137->115|137->115|139->117|139->117|141->119|141->119|141->119|142->120|142->120|142->120|145->123|145->123|145->123|146->124|146->124|146->124|146->124|146->124|146->124|146->124|146->124|146->124|146->124|146->124|146->124|146->124|147->125|147->125|147->125|149->127|150->128|152->130|152->130|152->130|153->131|153->131|153->131|153->131|153->131|153->131|153->131|153->131|153->131|153->131|153->131|153->131|153->131|153->131|153->131|153->131|155->133|155->133|155->133|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|159->137|160->138|161->139|161->139|162->140|162->140|165->143|165->143|165->143|165->143|168->146|168->146|169->147|169->147|171->149|171->149|175->153|175->153|175->153|175->153|175->153|177->155|177->155|177->155|177->155|180->158|180->158|182->160|182->160|188->166|188->166|191->169|191->169|192->170|192->170|194->172|194->172|196->174|196->174|199->177|199->177|199->177|199->177|199->177|199->177|199->177|202->180|202->180|204->182|204->182|205->183|205->183|208->186|208->186|209->187|209->187|211->189|211->189|214->192|214->192|214->192|220->198|220->198|222->200|222->200|225->203|225->203|226->204|226->204|229->207|229->207|230->208|230->208|232->210|232->210|235->213|235->213|237->215|237->215|239->217|239->217|239->217|239->217|245->223|245->223|246->224|246->224|248->226|248->226|249->227|249->227|249->227|249->227|251->229|251->229
                    -- GENERATED --
                */
            