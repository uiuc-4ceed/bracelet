
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object tile extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[models.ProjectSpace,String,Call,Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: models.ProjectSpace, classes: String, route: Call, showFollow: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters.ellipsize

import api.Permission

import play.api.i18n.Messages


Seq[Any](format.raw/*1.117*/("""
"""),format.raw/*5.1*/("""
<div class="post-box """),_display_(Seq[Any](/*6.23*/classes)),format.raw/*6.30*/("""" id=""""),_display_(Seq[Any](/*6.37*/space/*6.42*/.id)),format.raw/*6.45*/("""-tile">
    <div class="panel panel-default space-panel">
        <div class="pull-left">
            <span class="glyphicon glyphicon-hdd"></span>
        </div>
        <div class="panel-body">
            """),_display_(Seq[Any](/*12.14*/if(!space.logoURL.isEmpty)/*12.40*/ {_display_(Seq[Any](format.raw/*12.42*/("""
                <a href=""""),_display_(Seq[Any](/*13.27*/routes/*13.33*/.Spaces.getSpace(space.id))),format.raw/*13.59*/("""">
                    <img class="img-responsive" src=""""),_display_(Seq[Any](/*14.55*/(space.logoURL))),format.raw/*14.70*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*14.91*/(space.name))),format.raw/*14.103*/("""">
                </a>
            """)))})),format.raw/*16.14*/("""
        <div class="caption break-word">
            <h4 class="no-overflow oneline"><a href=""""),_display_(Seq[Any](/*18.55*/routes/*18.61*/.Spaces.getSpace(space.id))),format.raw/*18.87*/("""">"""),_display_(Seq[Any](/*18.90*/space/*18.95*/.name)),format.raw/*18.100*/("""</a></h4>
            <p class = 'abstractsummary'>"""),_display_(Seq[Any](/*19.43*/Html(space.description.replace("\n","<br>")))),format.raw/*19.87*/("""</p>
        </div>
        </div>
            <!-- Space Info -->
        <ul class="list-group">
            <li class="list-group-item space-panel-footer">
                <span class="glyphicon glyphicon-briefcase" title=""""),_display_(Seq[Any](/*25.69*/space/*25.74*/.datasetCount)),format.raw/*25.87*/(""" datasets"></span> """),_display_(Seq[Any](/*25.107*/space/*25.112*/.datasetCount)),format.raw/*25.125*/("""
                <span class="glyphicon glyphicon-th-large" title=""""),_display_(Seq[Any](/*26.68*/space/*26.73*/.collectionCount)),format.raw/*26.89*/(""" collections"></span> """),_display_(Seq[Any](/*26.112*/space/*26.117*/.collectionCount)),format.raw/*26.133*/("""
                <span class="glyphicon glyphicon-user" title=""""),_display_(Seq[Any](/*27.64*/space/*27.69*/.userCount)),format.raw/*27.79*/(""" users"></span> """),_display_(Seq[Any](/*27.96*/space/*27.101*/.userCount)),format.raw/*27.111*/("""
                """),_display_(Seq[Any](/*28.18*/if(user.isDefined)/*28.36*/ {_display_(Seq[Any](format.raw/*28.38*/("""

                    <!-- If user can delete, the button is enabled, otherwise the button is present but disabled to provide consistent UE. -->
                    """),_display_(Seq[Any](/*31.22*/if(Permission.checkPermission(Permission.DeleteSpace, ResourceRef(ResourceRef.space, space.id)) || user.get.identityId.userId.equals(space.creator))/*31.170*/{_display_(Seq[Any](format.raw/*31.171*/("""
                        <button onclick="confirmDeleteResource('space','"""),_display_(Seq[Any](/*32.74*/Messages("space.title"))),format.raw/*32.97*/("""','"""),_display_(Seq[Any](/*32.101*/(space.id))),format.raw/*32.111*/("""','"""),_display_(Seq[Any](/*32.115*/(space.name.replace("'","&#39;")))),format.raw/*32.148*/("""',false, '"""),_display_(Seq[Any](/*32.159*/route)),format.raw/*32.164*/("""')" class="btn btn-link" title="Delete the """),_display_(Seq[Any](/*32.208*/Messages("space.title"))),format.raw/*32.231*/(""" but not its contents">
<!--                             <span class="glyphicon glyphicon-trash"></span>
 -->                        </button>
                    """)))}/*35.23*/else/*35.28*/{_display_(Seq[Any](format.raw/*35.29*/("""
                        <div class="inline" title="Not enough permission to delete the """),_display_(Seq[Any](/*36.89*/Messages("space.title"))),format.raw/*36.112*/("""">
<!--                             <button disabled class="btn btn-link"><span class="glyphicon glyphicon-trash"></span></button>
 -->                        </div>
                    """)))})),format.raw/*39.22*/("""

                """)))})),format.raw/*41.18*/("""
            </li>
        </ul>
        """),_display_(Seq[Any](/*44.10*/if(showFollow)/*44.24*/ {_display_(Seq[Any](format.raw/*44.26*/("""
            """),_display_(Seq[Any](/*45.14*/user/*45.18*/ match/*45.24*/ {/*46.17*/case Some(viewer) =>/*46.37*/ {_display_(Seq[Any](format.raw/*46.39*/("""
                    <ul class="list-group center-margin">
                        <button id="followButton" type="button" class="btn-link" data-toggle="button" autocomplete="off" objectType="space" objectId=""""),_display_(Seq[Any](/*48.152*/space/*48.157*/.id.stringify)),format.raw/*48.170*/("""">
                        """),_display_(Seq[Any](/*49.26*/if(viewer.followedEntities.filter(x => (x.id == space.id)).nonEmpty)/*49.94*/ {_display_(Seq[Any](format.raw/*49.96*/("""
<!--                             <span class='glyphicon glyphicon-star-empty'></span> Unfollow
 -->                        """)))}/*51.31*/else/*51.36*/{_display_(Seq[Any](format.raw/*51.37*/("""
<!--                             <span class='glyphicon glyphicon-star'></span> Follow
 -->                        """)))})),format.raw/*53.30*/("""
                        </button>
                    </ul>
                """)))}/*57.17*/case None =>/*57.29*/ {}})),format.raw/*58.14*/("""

        """)))})),format.raw/*60.10*/("""
    </div>
</div>"""))}
    }
    
    def render(space:models.ProjectSpace,classes:String,route:Call,showFollow:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space,classes,route,showFollow)(user)
    
    def f:((models.ProjectSpace,String,Call,Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space,classes,route,showFollow) => (user) => apply(space,classes,route,showFollow)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/tile.scala.html
                    HASH: f981cb0f61c51be2fbb9613f4aac52c9f46e12d8
                    MATRIX: 646->1|950->116|977->212|1035->235|1063->242|1105->249|1118->254|1142->257|1387->466|1422->492|1462->494|1525->521|1540->527|1588->553|1681->610|1718->625|1775->646|1810->658|1879->695|2011->791|2026->797|2074->823|2113->826|2127->831|2155->836|2243->888|2309->932|2572->1159|2586->1164|2621->1177|2678->1197|2693->1202|2729->1215|2833->1283|2847->1288|2885->1304|2945->1327|2960->1332|2999->1348|3099->1412|3113->1417|3145->1427|3198->1444|3213->1449|3246->1459|3300->1477|3327->1495|3367->1497|3569->1663|3727->1811|3767->1812|3877->1886|3922->1909|3963->1913|3996->1923|4037->1927|4093->1960|4141->1971|4169->1976|4250->2020|4296->2043|4479->2208|4492->2213|4531->2214|4656->2303|4702->2326|4921->2513|4972->2532|5050->2574|5073->2588|5113->2590|5163->2604|5176->2608|5191->2614|5202->2633|5231->2653|5271->2655|5518->2865|5533->2870|5569->2883|5633->2911|5710->2979|5750->2981|5894->3107|5907->3112|5946->3113|6095->3230|6192->3325|6213->3337|6239->3354|6282->3365
                    LINES: 20->1|28->1|29->5|30->6|30->6|30->6|30->6|30->6|36->12|36->12|36->12|37->13|37->13|37->13|38->14|38->14|38->14|38->14|40->16|42->18|42->18|42->18|42->18|42->18|42->18|43->19|43->19|49->25|49->25|49->25|49->25|49->25|49->25|50->26|50->26|50->26|50->26|50->26|50->26|51->27|51->27|51->27|51->27|51->27|51->27|52->28|52->28|52->28|55->31|55->31|55->31|56->32|56->32|56->32|56->32|56->32|56->32|56->32|56->32|56->32|56->32|59->35|59->35|59->35|60->36|60->36|63->39|65->41|68->44|68->44|68->44|69->45|69->45|69->45|69->46|69->46|69->46|71->48|71->48|71->48|72->49|72->49|72->49|74->51|74->51|74->51|76->53|79->57|79->57|79->58|81->60
                    -- GENERATED --
                */
            