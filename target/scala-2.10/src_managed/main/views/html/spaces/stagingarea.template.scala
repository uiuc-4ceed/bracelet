
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object stagingarea extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[models.ProjectSpace,List[models.CurationObject],Int,Int,Int,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: models.ProjectSpace, curationObjects: List[models.CurationObject], prev: Int, next: Int, limit: Int)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import _root_.util.Formatters._


Seq[Any](format.raw/*1.146*/("""
"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Staging Area")/*5.22*/ {_display_(Seq[Any](format.raw/*5.24*/("""
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><span class="glyphicon glyphicon-hdd"></span>  <a href=""""),_display_(Seq[Any](/*9.78*/routes/*9.84*/.Spaces.getSpace(space.id))),format.raw/*9.110*/("""" title=""""),_display_(Seq[Any](/*9.120*/space/*9.125*/.name)),format.raw/*9.130*/(""""> """),_display_(Seq[Any](/*9.134*/Html(ellipsize(space.name,18)))),format.raw/*9.164*/("""</a></li>
                <li><span class="glyphicon glyphicon-book"></span> Staging Area</li>

            </ol>
            <h1>Staging Area</h1>
        </div>
    </div>
    """),_display_(Seq[Any](/*16.6*/util/*16.10*/.masonry())),format.raw/*16.20*/("""
    <div class="row" id="tile-view">
        <div class="col-md-12">
            <div id="masonry">
                """),_display_(Seq[Any](/*20.18*/curationObjects/*20.33*/.map/*20.37*/ { curObject =>_display_(Seq[Any](format.raw/*20.52*/("""
                    <div class="post-box col-lg-3 col-md-3 coml-sm-3 col-xs-12">
                        <div class = "panel panel-default" id=""""),_display_(Seq[Any](/*22.65*/curObject/*22.74*/.id)),format.raw/*22.77*/("""-tile">
                            """),_display_(Seq[Any](/*23.30*/if(curObject.status == "Submitted")/*23.65*/ {_display_(Seq[Any](format.raw/*23.67*/("""
                                <div id=""""),_display_(Seq[Any](/*24.43*/curObject/*24.52*/.id)),format.raw/*24.55*/("""" class="panel-heading text-center submitted">
                            """)))}/*25.31*/else/*25.36*/{_display_(Seq[Any](format.raw/*25.37*/("""
                                """),_display_(Seq[Any](/*26.34*/if(curObject.status == "In Preparation")/*26.74*/ {_display_(Seq[Any](format.raw/*26.76*/("""
                                <div id=""""),_display_(Seq[Any](/*27.43*/curObject/*27.52*/.id)),format.raw/*27.55*/("""" class="panel-heading text-center in-curation">
                                    """)))}/*28.39*/else/*28.44*/{_display_(Seq[Any](format.raw/*28.45*/("""
                                <div id=""""),_display_(Seq[Any](/*29.43*/curObject/*29.52*/.id)),format.raw/*29.55*/("""" class="panel-heading text-center publish">
                                    """)))})),format.raw/*30.38*/("""

                            """)))})),format.raw/*32.30*/("""

                               """),_display_(Seq[Any](/*34.33*/curObject/*34.42*/.status)),format.raw/*34.49*/("""
                            </div>
                            <div class = "panel-body">
                                <div class="caption break-word">
                                    <h4> <a href=""""),_display_(Seq[Any](/*38.52*/routes/*38.58*/.CurationObjects.getCurationObject( curObject.id))),format.raw/*38.107*/("""">"""),_display_(Seq[Any](/*38.110*/curObject/*38.119*/.name)),format.raw/*38.124*/("""</a></h4>

                                    """),_display_(Seq[Any](/*40.38*/Messages("owner.label"))),format.raw/*40.61*/(""": """),_display_(Seq[Any](/*40.64*/curObject/*40.73*/.author.fullName)),format.raw/*40.89*/("""
                                    <p>Created: """),_display_(Seq[Any](/*41.50*/curObject/*41.59*/.created.format("MMM dd, yyyy"))),format.raw/*41.90*/("""</p>
                                    <p class="abstractsummary">"""),_display_(Seq[Any](/*42.65*/Html(curObject.description.replace("\n","<br>")))),format.raw/*42.113*/("""</p>
                                </div>
                            </div>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <span class="glyphicon glyphicon-folder-close" title=""""),_display_(Seq[Any](/*47.92*/curObject/*47.101*/.folders.size)),format.raw/*47.114*/(""" folders"></span> """),_display_(Seq[Any](/*47.133*/curObject/*47.142*/.folders.size)),format.raw/*47.155*/("""
                                    <span class="glyphicon glyphicon-file" title=""""),_display_(Seq[Any](/*48.84*/curObject/*48.93*/.files.size)),format.raw/*48.104*/(""" files"></span> """),_display_(Seq[Any](/*48.121*/curObject/*48.130*/.files.size)),format.raw/*48.141*/("""
                                    <span class="glyphicon glyphicon-list" title=""""),_display_(Seq[Any](/*49.84*/(curObject.metadataCount))),format.raw/*49.109*/(""" metadata fields"></span> """),_display_(Seq[Any](/*49.136*/(curObject.metadataCount))),format.raw/*49.161*/("""
                                    <button onclick="confirmDeleteResource('curation object','curation object','"""),_display_(Seq[Any](/*50.114*/(curObject.id))),format.raw/*50.128*/("""','"""),_display_(Seq[Any](/*50.132*/(curObject.name.replace("'","&#39;")))),format.raw/*50.169*/("""',false, '/')"  type="button" class="btn btn-link">
                                        <span class="glyphicon glyphicon-trash" title=""""),_display_(Seq[Any](/*51.89*/curObject/*51.98*/.collections.size)),format.raw/*51.115*/(""" collections"></span></button>
                                </li>
                            </ul>
                            """),_display_(Seq[Any](/*54.30*/if(curObject.status == "Submitted")/*54.65*/ {_display_(Seq[Any](format.raw/*54.67*/("""
                                <ul id="ul_"""),_display_(Seq[Any](/*55.45*/curObject/*55.54*/.id)),format.raw/*55.57*/("""" class="list-group">
                                <li class="list-group-item"><button class="btn btn-sm btn-default text-center" onclick="retractCuration('"""),_display_(Seq[Any](/*56.139*/curObject/*56.148*/.id)),format.raw/*56.151*/("""')"><span class="glyphicon glyphicon-fire"></span> Retract Submission</button></li>
                                </ul>
                            """)))})),format.raw/*58.30*/("""
                        </div>
                    </div>
                """)))})),format.raw/*61.18*/("""
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="pager">
                    <!-- The following items have to be links due to the way the list items render them. Ideally, they should be buttons. -->
                """),_display_(Seq[Any](/*69.18*/if(prev >= 0)/*69.31*/ {_display_(Seq[Any](format.raw/*69.33*/("""
                    <li class="previous"><a id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span>Previous</a></li>
                """)))})),format.raw/*71.18*/("""
                """),_display_(Seq[Any](/*72.18*/if(next >= 0)/*72.31*/ {_display_(Seq[Any](format.raw/*72.33*/("""
                    <li class ="next"><a id="nextlink" title="Page forwards" href="#">Next<span class="glyphicon glyphicon-chevron-right"></span></a></li>
                """)))})),format.raw/*74.18*/("""
            </ul>
        </div>
    </div>
    <script src=""""),_display_(Seq[Any](/*78.19*/routes/*78.25*/.Assets.at("javascripts/curationProcess.js"))),format.raw/*78.69*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*79.19*/routes/*79.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*79.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*80.19*/routes/*80.25*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*80.70*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*81.19*/routes/*81.25*/.Assets.at("javascripts/descriptionSummary.js"))),format.raw/*81.72*/("""" type="text/javascript"></script>
    
    <script>
        $(document).ready(function()"""),format.raw/*84.37*/("""{"""),format.raw/*84.38*/("""
            updatePage();
        """),format.raw/*86.9*/("""}"""),format.raw/*86.10*/(""");
        function updatePage() """),format.raw/*87.31*/("""{"""),format.raw/*87.32*/("""
            $('#nextlink').attr('href', """"),_display_(Seq[Any](/*88.43*/(routes.Spaces.stagingArea(space.id, next, limit)))),format.raw/*88.93*/("""");
            $('#prevlink').attr('href', """"),_display_(Seq[Any](/*89.43*/(routes.Spaces.stagingArea(space.id, prev, limit)))),format.raw/*89.93*/("""");
        """),format.raw/*90.9*/("""}"""),format.raw/*90.10*/("""
    </script>
""")))})))}
    }
    
    def render(space:models.ProjectSpace,curationObjects:List[models.CurationObject],prev:Int,next:Int,limit:Int,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space,curationObjects,prev,next,limit)(user)
    
    def f:((models.ProjectSpace,List[models.CurationObject],Int,Int,Int) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space,curationObjects,prev,next,limit) => (user) => apply(space,curationObjects,prev,next,limit)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/stagingarea.scala.html
                    HASH: 91c20faa4307a9466c22e2291244faf699cc814c
                    MATRIX: 673->1|975->145|1002->210|1038->212|1066->232|1105->234|1308->402|1322->408|1370->434|1416->444|1430->449|1457->454|1497->458|1549->488|1763->667|1776->671|1808->681|1962->799|1986->814|1999->818|2052->833|2234->979|2252->988|2277->991|2350->1028|2394->1063|2434->1065|2513->1108|2531->1117|2556->1120|2651->1197|2664->1202|2703->1203|2773->1237|2822->1277|2862->1279|2941->1322|2959->1331|2984->1334|3089->1421|3102->1426|3141->1427|3220->1470|3238->1479|3263->1482|3377->1564|3440->1595|3510->1629|3528->1638|3557->1645|3800->1852|3815->1858|3887->1907|3927->1910|3946->1919|3974->1924|4058->1972|4103->1995|4142->1998|4160->2007|4198->2023|4284->2073|4302->2082|4355->2113|4460->2182|4531->2230|4850->2513|4869->2522|4905->2535|4961->2554|4980->2563|5016->2576|5136->2660|5154->2669|5188->2680|5242->2697|5261->2706|5295->2717|5415->2801|5463->2826|5527->2853|5575->2878|5726->2992|5763->3006|5804->3010|5864->3047|6040->3187|6058->3196|6098->3213|6266->3345|6310->3380|6350->3382|6431->3427|6449->3436|6474->3439|6671->3599|6690->3608|6716->3611|6899->3762|7007->3838|7333->4128|7355->4141|7395->4143|7607->4323|7661->4341|7683->4354|7723->4356|7928->4529|8027->4592|8042->4598|8108->4642|8197->4695|8212->4701|8274->4741|8363->4794|8378->4800|8445->4845|8534->4898|8549->4904|8618->4951|8735->5040|8764->5041|8826->5076|8855->5077|8916->5110|8945->5111|9024->5154|9096->5204|9178->5250|9250->5300|9289->5312|9318->5313
                    LINES: 20->1|26->1|27->4|28->5|28->5|28->5|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|39->16|39->16|39->16|43->20|43->20|43->20|43->20|45->22|45->22|45->22|46->23|46->23|46->23|47->24|47->24|47->24|48->25|48->25|48->25|49->26|49->26|49->26|50->27|50->27|50->27|51->28|51->28|51->28|52->29|52->29|52->29|53->30|55->32|57->34|57->34|57->34|61->38|61->38|61->38|61->38|61->38|61->38|63->40|63->40|63->40|63->40|63->40|64->41|64->41|64->41|65->42|65->42|70->47|70->47|70->47|70->47|70->47|70->47|71->48|71->48|71->48|71->48|71->48|71->48|72->49|72->49|72->49|72->49|73->50|73->50|73->50|73->50|74->51|74->51|74->51|77->54|77->54|77->54|78->55|78->55|78->55|79->56|79->56|79->56|81->58|84->61|92->69|92->69|92->69|94->71|95->72|95->72|95->72|97->74|101->78|101->78|101->78|102->79|102->79|102->79|103->80|103->80|103->80|104->81|104->81|104->81|107->84|107->84|109->86|109->86|110->87|110->87|111->88|111->88|112->89|112->89|113->90|113->90
                    -- GENERATED --
                */
            