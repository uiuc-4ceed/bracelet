
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object externalLinks extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[List[java.net.URL],models.ProjectSpace,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(links: List[java.net.URL], space:models.ProjectSpace, classes: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import api.Permission


Seq[Any](format.raw/*1.109*/("""
"""),format.raw/*4.1*/("""
<div class=""""),_display_(Seq[Any](/*5.14*/classes)),format.raw/*5.21*/("""">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <h3>External Links</h3>
        """),_display_(Seq[Any](/*8.10*/if(links.size == 0)/*8.29*/ {_display_(Seq[Any](format.raw/*8.31*/("""
            """),_display_(Seq[Any](/*9.14*/if(Permission.checkPermission(Permission.EditSpace, ResourceRef(ResourceRef.space, space.id)))/*9.108*/ {_display_(Seq[Any](format.raw/*9.110*/("""
                <p>Edit """),_display_(Seq[Any](/*10.26*/Messages("space.title"))),format.raw/*10.49*/(""" to add links</p>
            """)))}/*11.15*/else/*11.20*/{_display_(Seq[Any](format.raw/*11.21*/("""
                <p>No External Links</p>
            """)))})),format.raw/*13.14*/("""
        """)))}/*14.11*/else/*14.16*/{_display_(Seq[Any](format.raw/*14.17*/("""
            <ul class="list-unstyled">
            """),_display_(Seq[Any](/*16.14*/for(link <- links) yield /*16.32*/ {_display_(Seq[Any](format.raw/*16.34*/("""
                <li><a href=""""),_display_(Seq[Any](/*17.31*/link)),format.raw/*17.35*/("""">"""),_display_(Seq[Any](/*17.38*/link)),format.raw/*17.42*/("""</a></li>
            """)))})),format.raw/*18.14*/("""
            </ul>
        """)))})),format.raw/*20.10*/("""
    </div>
</div>"""))}
    }
    
    def render(links:List[java.net.URL],space:models.ProjectSpace,classes:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(links,space,classes)(user)
    
    def f:((List[java.net.URL],models.ProjectSpace,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (links,space,classes) => (user) => apply(links,space,classes)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/externalLinks.scala.html
                    HASH: 2955ea11a4889d44114d845fcb9451fd8350b22b
                    MATRIX: 661->1|916->108|943->163|992->177|1020->184|1147->276|1174->295|1213->297|1262->311|1365->405|1405->407|1467->433|1512->456|1562->488|1575->493|1614->494|1701->549|1730->560|1743->565|1782->566|1871->619|1905->637|1945->639|2012->670|2038->674|2077->677|2103->681|2158->704|2218->732
                    LINES: 20->1|26->1|27->4|28->5|28->5|31->8|31->8|31->8|32->9|32->9|32->9|33->10|33->10|34->11|34->11|34->11|36->13|37->14|37->14|37->14|39->16|39->16|39->16|40->17|40->17|40->17|40->17|41->18|43->20
                    -- GENERATED --
                */
            