
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object copySpace extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[Form[controllers.spaceFormData],UUID,play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[controllers.spaceFormData], originalUUID : UUID)(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages


Seq[Any](format.raw/*1.127*/("""
"""),_display_(Seq[Any](/*3.2*/newCopyTemplate(myForm, title = Messages("create.header", Messages("space.title")), submitButton = createButton(), None,originalUUID))))}
    }
    
    def render(myForm:Form[controllers.spaceFormData],originalUUID:UUID,flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm,originalUUID)(flash,user)
    
    def f:((Form[controllers.spaceFormData],UUID) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm,originalUUID) => (flash,user) => apply(myForm,originalUUID)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/copySpace.scala.html
                    HASH: 7f5ff50d915a93162528ee92b3dbf93b2c7c5000
                    MATRIX: 667->1|917->126|953->159
                    LINES: 20->1|24->1|25->3
                    -- GENERATED --
                */
            