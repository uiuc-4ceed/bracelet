
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object newEditTemplate extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[Form[controllers.spaceFormData],String,Html,Option[UUID],Option[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[controllers.spaceFormData], title:String, submitButton: Html, spaceId: Option[UUID], spaceName: Option[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import helper._

import play.api.Play.current

import models.SpaceStatus

import api.Permission

import _root_.util.Formatters.ellipsize

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f)}};
Seq[Any](format.raw/*1.190*/("""
"""),format.raw/*4.74*/("""
"""),format.raw/*9.1*/("""
"""),_display_(Seq[Any](/*10.2*/main(title)/*10.13*/ {_display_(Seq[Any](format.raw/*10.15*/("""
  <div class="page-header">
    <ol class="breadcrumb">
    """),_display_(Seq[Any](/*13.6*/(spaceId, spaceName)/*13.26*/ match/*13.32*/ {/*14.7*/case (Some(s), Some(name)) =>/*14.36*/ {_display_(Seq[Any](format.raw/*14.38*/("""
        <li> <span class="glyphicon glyphicon-hdd"></span>  <a href=""""),_display_(Seq[Any](/*15.71*/routes/*15.77*/.Spaces.getSpace(s))),format.raw/*15.96*/("""" title=""""),_display_(Seq[Any](/*15.106*/name)),format.raw/*15.110*/(""""> """),_display_(Seq[Any](/*15.114*/Html(ellipsize(name, 18)))),format.raw/*15.139*/("""</a></li>
        <li> <span class="glyphicon glyphicon-edit"></span> """),_display_(Seq[Any](/*16.62*/Html(title))),format.raw/*16.73*/("""</li>
      """)))}/*18.7*/case (_,_) =>/*18.20*/ {_display_(Seq[Any](format.raw/*18.22*/("""
        """),_display_(Seq[Any](/*19.10*/user/*19.14*/ match/*19.20*/ {/*20.11*/case Some(u) =>/*20.26*/ {_display_(Seq[Any](format.raw/*20.28*/("""
            <li><span class="glyphicon glyphicon-user"></span> <a href= """"),_display_(Seq[Any](/*21.75*/routes/*21.81*/.Profile.viewProfileUUID(u.id))),format.raw/*21.111*/(""""> """),_display_(Seq[Any](/*21.115*/Html(u.fullName))),format.raw/*21.131*/(""" </a> </li>
            <li><span class="glyphicon glyphicon-hdd"></span> """),_display_(Seq[Any](/*22.64*/Html(title))),format.raw/*22.75*/(""" </li>
          """)))}/*24.11*/case None =>/*24.23*/ {}})),format.raw/*25.10*/("""
      """)))}})),format.raw/*27.6*/("""
    </ol>

    <h1>"""),_display_(Seq[Any](/*30.10*/title)),format.raw/*30.15*/("""</h1>
    <p>"""),_display_(Seq[Any](/*31.9*/Messages("space.create.message", Messages("spaces.title"), Messages("datasets.title")))),format.raw/*31.95*/("""</p>
  </div>
  <div class="row">
    <div class="col-md-12">
      """),_display_(Seq[Any](/*35.8*/if(myForm.hasErrors)/*35.28*/ {_display_(Seq[Any](format.raw/*35.30*/("""
        <div class="alert alert-error error">
          <p>Please fix all errors</p>
        </div>
      """)))})),format.raw/*39.8*/("""
      """),_display_(Seq[Any](/*40.8*/flash/*40.13*/.get("error").map/*40.30*/ { message =>_display_(Seq[Any](format.raw/*40.43*/("""
        <div class="alert alert-error">
          <p>"""),_display_(Seq[Any](/*42.15*/message)),format.raw/*42.22*/("""</p>
        </div>
      """)))})),format.raw/*44.8*/("""

      """),_display_(Seq[Any](/*46.8*/form(action = routes.Spaces.submit, 'enctype -> "multipart/form-data", 'class -> "form-horizontal", 'id -> "spaceForm")/*46.127*/ {_display_(Seq[Any](format.raw/*46.129*/("""
        <div class="jumbotron">
            <!--         <fieldset  id="nameDescrFieldSet">
 -->          """),_display_(Seq[Any](/*49.16*/inputText(myForm("name"), 'class -> "form-control", '_label -> "Name"))),format.raw/*49.86*/("""
          """),_display_(Seq[Any](/*50.12*/textarea(myForm("description"), 'class -> "form-control", '_label -> "Description"))),format.raw/*50.95*/("""
            <!--           <div class ="control-group"><label class="control-label" for="homePages">External Links</label></div>
          <div class = "homePages">
            """),_display_(Seq[Any](/*53.14*/repeat(myForm("homePages"), min = 1)/*53.50*/ {externalLink =>_display_(Seq[Any](format.raw/*53.67*/("""
          """),_display_(Seq[Any](/*54.12*/inputText(externalLink, 'class -> "form-control", '_label -> ""))),format.raw/*54.76*/("""
          """),format.raw/*59.18*/("""
        """)))})),format.raw/*60.10*/("""
          <div class="external-link-add"><a href="#">add</a></div>
        </div>
        """),_display_(Seq[Any](/*63.10*/inputText(myForm("logoUrl"),'class -> "form-control", '_label-> "Logo URL"))),format.raw/*63.85*/("""
        Recommended image size is 100 x 100
        """),_display_(Seq[Any](/*65.10*/inputText(myForm("bannerUrl"),'class -> "form-control", '_label-> "Banner URL"))),format.raw/*65.89*/("""
        Recommended image size is 1170 x 200
        -->
        """),_display_(Seq[Any](/*68.10*/helper/*68.16*/.inputRadioGroup(myForm("expiration"), options("expiretrue"->"On", "expirefalse"->"Off"), '_label -> "Expiration"))),format.raw/*68.130*/("""
        """),_display_(Seq[Any](/*69.10*/defining(myForm("isTimeToLiveEnabled"))/*69.49*/ { isTimeToLiveEnabled =>_display_(Seq[Any](format.raw/*69.74*/("""
          <input type="hidden" name="isTimeToLiveEnabled" id="isTimeToLiveEnabled", value = """),_display_(Seq[Any](/*70.94*/isTimeToLiveEnabled/*70.113*/.value)),format.raw/*70.119*/(""">
        """)))})),format.raw/*71.10*/("""
        """),_display_(Seq[Any](/*72.10*/helper/*72.16*/.inputText(myForm("editTime"), 'class -> "form-control", '_label -> "Time to live (hours):"))),format.raw/*72.108*/("""
        """),format.raw/*73.66*/("""

        """),_display_(Seq[Any](/*75.10*/helper/*75.16*/.inputRadioGroup(myForm("access"),
          options = options(SpaceStatus.PRIVATE.toString->SpaceStatus.PRIVATE.toString,SpaceStatus.PUBLIC.toString->SpaceStatus.PUBLIC.toString),
          '_label -> "Access"))),format.raw/*77.31*/("""
        <hr />
        """),_display_(Seq[Any](/*79.10*/Messages("crate.space.access.message", Messages("collections.title").toLowerCase(), Messages("datasets.title").toLowerCase, Messages("space.title").toLowerCase, Messages("dataset.title").toLowerCase))),format.raw/*79.209*/("""
        <hr />
        """),_display_(Seq[Any](/*81.10*/helper/*81.16*/.input(myForm("space_id"), 'class -> "form-control", '_label ->"")/*81.82*/ {(id, name, value, args) =>_display_(Seq[Any](format.raw/*81.110*/("""
          <input type="hidden" name=""""),_display_(Seq[Any](/*82.39*/id)),format.raw/*82.41*/("""" value=""""),_display_(Seq[Any](/*82.51*/value)),format.raw/*82.56*/("""">
        """)))})),format.raw/*83.10*/("""

          <!--         </fieldset>
 -->        """),_display_(Seq[Any](/*86.14*/submitButton)),format.raw/*86.26*/("""
      </div>
    """)))})),format.raw/*88.6*/("""
  </div>
  </div>

  <script language="javascript">
  $(document).ready(function() """),format.raw/*93.32*/("""{"""),format.raw/*93.33*/("""
    var verified = """),_display_(Seq[Any](/*94.21*/play/*94.25*/.Play.application().configuration().getBoolean("verifySpaces"))),format.raw/*94.87*/(""";
    var publicEnabled = """),_display_(Seq[Any](/*95.26*/play/*95.30*/.Play.application().configuration().getBoolean("enablePublic"))),format.raw/*95.92*/(""";
    if( publicEnabled) """),format.raw/*96.24*/("""{"""),format.raw/*96.25*/("""

      if(verified)"""),format.raw/*98.19*/("""{"""),format.raw/*98.20*/("""
        //enableVerified == true, create a space or still unverified space
        if(typeof $("input[name='access']:checked").val() === "undefined" || $("input[name='access']:checked").val() === """"),_display_(Seq[Any](/*100.124*/SpaceStatus/*100.135*/.TRIAL.toString)),format.raw/*100.150*/("""")"""),format.raw/*100.152*/("""{"""),format.raw/*100.153*/("""

          $('#access').closest(".control-group").hide();
          $('#help_access').hide();
          $('#access').append('<input type="radio" id="access_trial" name="access" value=""""),_display_(Seq[Any](/*104.92*/SpaceStatus/*104.103*/.TRIAL.toString)),format.raw/*104.118*/("""" checked>')
        """),format.raw/*105.9*/("""}"""),format.raw/*105.10*/("""
      """),format.raw/*106.7*/("""}"""),format.raw/*106.8*/(""" else"""),format.raw/*106.13*/("""{"""),format.raw/*106.14*/("""
        // enablePublic ==true  && enableVerified == false && update
        """),_display_(Seq[Any](/*108.10*/if(spaceId.isDefined && !Permission.checkPermission(Permission.PublicSpace, ResourceRef(ResourceRef.space, spaceId.get)))/*108.131*/{_display_(Seq[Any](format.raw/*108.132*/("""
        $('#access').closest(".control-group").hide();
        $('#help_access').hide();
        """)))})),format.raw/*111.10*/("""
        // enablePublic ==true  && enableVerified == false && create
        if(typeof $("input[name='access']:checked").val() === "undefined" )"""),format.raw/*113.76*/("""{"""),format.raw/*113.77*/("""
          document.getElementById('access_"""),_display_(Seq[Any](/*114.44*/SpaceStatus/*114.55*/.PRIVATE.toString)),format.raw/*114.72*/("""').checked= true;
        """),format.raw/*115.9*/("""}"""),format.raw/*115.10*/("""
      """),format.raw/*116.7*/("""}"""),format.raw/*116.8*/("""
    """),format.raw/*117.5*/("""}"""),format.raw/*117.6*/(""" else """),format.raw/*117.12*/("""{"""),format.raw/*117.13*/("""
      // enablePublic == false
      $('#access').closest(".control-group").hide();
      $('#help_access').hide();
      document.getElementById('access_PRIVATE').checked= true;
    """),format.raw/*122.5*/("""}"""),format.raw/*122.6*/("""
  """),format.raw/*123.3*/("""}"""),format.raw/*123.4*/(""");
  // as fields are added, they get the same default homePages[0] name For automatic binding the names need to
  // be homePages[0], homePages[1],.. etc. This function renumbers all the fields appropriately even if a field in the
  // middle of the list is deleted.
  var renumberHomePageMap = function() """),format.raw/*127.40*/("""{"""),format.raw/*127.41*/("""
    Array.prototype.reduce.call($('.homePages .control-group'), function (i, next) """),format.raw/*128.84*/("""{"""),format.raw/*128.85*/("""
      var label = $(next).children('label');
      label.attr('for', label.attr('for').replace(/_\d+_/, '_' + i + '_'));

      var input = $(next).find('input');
      input.attr('id', input.attr('id').replace(/_\d+_/, '_' + i + '_'));
      input.attr('name', input.attr('name').replace(/\[\d+\]/, '[' + i + ']'));

      if (input.attr('id').match(/homePages_\d+_/)) """),format.raw/*136.53*/("""{"""),format.raw/*136.54*/("""
        return i + 1;
      """),format.raw/*138.7*/("""}"""),format.raw/*138.8*/("""

      return i;
    """),format.raw/*141.5*/("""}"""),format.raw/*141.6*/(""", 0);
  """),format.raw/*142.3*/("""}"""),format.raw/*142.4*/("""

  var addDeleteInCaseOfError = function() """),format.raw/*144.43*/("""{"""),format.raw/*144.44*/("""
    vals = $('.homePages .control-group')
    for(var i = 0; i < vals.length - 1 ; i++)
    """),format.raw/*147.5*/("""{"""),format.raw/*147.6*/("""
      $(vals[i]).after($('<div class="external-link-delete"><a href="#">delete</a></div>'));
    """),format.raw/*149.5*/("""}"""),format.raw/*149.6*/("""
  """),format.raw/*150.3*/("""}"""),format.raw/*150.4*/(""";

  addDeleteInCaseOfError();


  var title = $('.page-header h1').text();
  if(title.indexOf("Edit ") >= 0)
  """),format.raw/*157.3*/("""{"""),format.raw/*157.4*/("""
    if(!$('.help-inline').text())
    """),format.raw/*159.5*/("""{"""),format.raw/*159.6*/("""
      var timeToLive = $('#editTime').val();
      timeToLive = timeToLive/3600000;
      $('#editTime').val(timeToLive);
    """),format.raw/*163.5*/("""}"""),format.raw/*163.6*/("""
  """),format.raw/*164.3*/("""}"""),format.raw/*164.4*/("""
  else
  """),format.raw/*166.3*/("""{"""),format.raw/*166.4*/("""
    $('#editTime').val(720);
  """),format.raw/*168.3*/("""}"""),format.raw/*168.4*/("""


  var $radios = $('input:radio[name=expiration]');
  var isEnabled =  $('#isTimeToLiveEnabled').val();

  if (isEnabled === "true") """),format.raw/*174.29*/("""{"""),format.raw/*174.30*/("""
    $radios.filter('[value=expiretrue]').prop('checked', true);
    //$('input:radio[name=radiogroup]')[0].checked = true;
    $('#expireenabled').html("On");
  """),format.raw/*178.3*/("""}"""),format.raw/*178.4*/("""
  else """),format.raw/*179.8*/("""{"""),format.raw/*179.9*/("""
    $radios.filter('[value=expirefalse]').prop('checked', true);
    //$('input:radio[name=radiogroup]')[1].checked = true;
    $('#expireenabled').html("Off");
  """),format.raw/*183.3*/("""}"""),format.raw/*183.4*/("""
  $('.homePages').on('click', '.external-link-delete a', function () """),format.raw/*184.70*/("""{"""),format.raw/*184.71*/("""
    $(this).parent().prev().remove();
    $(this).parent().remove();
    renumberHomePageMap();
  """),format.raw/*188.3*/("""}"""),format.raw/*188.4*/(""");
  $('.control-label').addClass('h3');
  $('.external-link-add').click(function () """),format.raw/*190.45*/("""{"""),format.raw/*190.46*/("""
    var value = $(this).prev().clone();

    $(this).prev().after($('<div class="external-link-delete"><a href="#">delete</a></div>'));
    value.find('input').val('');
    $(this).before(value);
    renumberHomePageMap();
  """),format.raw/*197.3*/("""}"""),format.raw/*197.4*/(""");

  //remove blank lines before submitting
  $('form').submit(function () """),format.raw/*200.32*/("""{"""),format.raw/*200.33*/("""
    var expireEnabled = $('#expiration_expiretrue').prop('checked');
    $('#isTimeToLiveEnabled').val(expireEnabled);
    $('.homePages .control-group').each(function () """),format.raw/*203.53*/("""{"""),format.raw/*203.54*/("""
      var valueInput = $(this).find('input');

      if (valueInput.val() === '') """),format.raw/*206.36*/("""{"""),format.raw/*206.37*/("""
        $(this).next().remove();
        $(this).remove();
        renumberHomePageMap();
      """),format.raw/*210.7*/("""}"""),format.raw/*210.8*/("""

    """),format.raw/*212.5*/("""}"""),format.raw/*212.6*/(""");
  """),format.raw/*213.3*/("""}"""),format.raw/*213.4*/(""");

  $('#cancel_space').click(function() """),format.raw/*215.39*/("""{"""),format.raw/*215.40*/("""
    if(!"""),_display_(Seq[Any](/*216.10*/spaceId/*216.17*/.isEmpty)),format.raw/*216.25*/(""") """),format.raw/*216.27*/("""{"""),format.raw/*216.28*/("""
      window.location.replace(""""),_display_(Seq[Any](/*217.33*/routes/*217.39*/.Spaces.getSpace(spaceId.getOrElse(new UUID(""))))),format.raw/*217.88*/("""");
    """),format.raw/*218.5*/("""}"""),format.raw/*218.6*/(""" else """),format.raw/*218.12*/("""{"""),format.raw/*218.13*/("""
      window.location.replace(""""),_display_(Seq[Any](/*219.33*/routes/*219.39*/.Spaces.list())),format.raw/*219.53*/("""");
    """),format.raw/*220.5*/("""}"""),format.raw/*220.6*/("""

  """),format.raw/*222.3*/("""}"""),format.raw/*222.4*/(""");
  """),_display_(Seq[Any](/*223.4*/if(!play.api.Play.configuration.getBoolean("enable_expiration").getOrElse(false))/*223.85*/ {_display_(Seq[Any](format.raw/*223.87*/("""
  $("#expiration").closest(".control-group").hide();
  $("#editTime").closest(".control-group").hide();
  """)))})),format.raw/*226.4*/("""

  </script>
""")))})),format.raw/*229.2*/("""
"""))}
    }
    
    def render(myForm:Form[controllers.spaceFormData],title:String,submitButton:Html,spaceId:Option[UUID],spaceName:Option[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm,title,submitButton,spaceId,spaceName)(flash,user)
    
    def f:((Form[controllers.spaceFormData],String,Html,Option[UUID],Option[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm,title,submitButton,spaceId,spaceName) => (flash,user) => apply(myForm,title,submitButton,spaceId,spaceName)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/newEditTemplate.scala.html
                    HASH: aaa6ef9217d2fcd4ba463ad9b4ce0265ac79babf
                    MATRIX: 708->1|1150->239|1182->263|1261->189|1289->311|1316->433|1353->435|1373->446|1413->448|1510->510|1539->530|1554->536|1564->545|1602->574|1642->576|1749->647|1764->653|1805->672|1852->682|1879->686|1920->690|1968->715|2075->786|2108->797|2139->817|2161->830|2201->832|2247->842|2260->846|2275->852|2286->865|2310->880|2350->882|2461->957|2476->963|2529->993|2570->997|2609->1013|2720->1088|2753->1099|2790->1128|2811->1140|2837->1153|2877->1167|2934->1188|2961->1193|3010->1207|3118->1293|3222->1362|3251->1382|3291->1384|3430->1492|3473->1500|3487->1505|3513->1522|3564->1535|3655->1590|3684->1597|3742->1624|3786->1633|3915->1752|3956->1754|4100->1862|4192->1932|4240->1944|4345->2027|4560->2206|4605->2242|4660->2259|4708->2271|4794->2335|4833->2556|4875->2566|5003->2658|5100->2733|5190->2787|5291->2866|5394->2933|5409->2939|5546->3053|5592->3063|5640->3102|5703->3127|5833->3221|5862->3240|5891->3246|5934->3257|5980->3267|5995->3273|6110->3365|6147->3431|6194->3442|6209->3448|6442->3659|6503->3684|6725->3883|6786->3908|6801->3914|6876->3980|6943->4008|7018->4047|7042->4049|7088->4059|7115->4064|7159->4076|7245->4126|7279->4138|7329->4157|7441->4241|7470->4242|7527->4263|7540->4267|7624->4329|7687->4356|7700->4360|7784->4422|7837->4447|7866->4448|7914->4468|7943->4469|8180->4668|8202->4679|8241->4694|8273->4696|8304->4697|8527->4883|8549->4894|8588->4909|8637->4930|8667->4931|8702->4938|8731->4939|8765->4944|8795->4945|8911->5024|9043->5145|9084->5146|9216->5245|9390->5390|9420->5391|9501->5435|9522->5446|9562->5463|9616->5489|9646->5490|9681->5497|9710->5498|9743->5503|9772->5504|9807->5510|9837->5511|10049->5695|10078->5696|10109->5699|10138->5700|10474->6007|10504->6008|10617->6092|10647->6093|11047->6464|11077->6465|11134->6494|11163->6495|11213->6517|11242->6518|11278->6526|11307->6527|11380->6571|11410->6572|11531->6665|11560->6666|11686->6764|11715->6765|11746->6768|11775->6769|11915->6881|11944->6882|12011->6921|12040->6922|12195->7049|12224->7050|12255->7053|12284->7054|12322->7064|12351->7065|12411->7097|12440->7098|12604->7233|12634->7234|12824->7396|12853->7397|12889->7405|12918->7406|13110->7570|13139->7571|13238->7641|13268->7642|13395->7741|13424->7742|13538->7827|13568->7828|13822->8054|13851->8055|13956->8131|13986->8132|14187->8304|14217->8305|14329->8388|14359->8389|14484->8486|14513->8487|14547->8493|14576->8494|14609->8499|14638->8500|14709->8542|14739->8543|14786->8553|14803->8560|14834->8568|14865->8570|14895->8571|14965->8604|14981->8610|15053->8659|15089->8667|15118->8668|15153->8674|15183->8675|15253->8708|15269->8714|15306->8728|15342->8736|15371->8737|15403->8741|15432->8742|15474->8748|15565->8829|15606->8831|15746->8939|15793->8954
                    LINES: 20->1|33->4|33->4|34->1|35->4|36->9|37->10|37->10|37->10|40->13|40->13|40->13|40->14|40->14|40->14|41->15|41->15|41->15|41->15|41->15|41->15|41->15|42->16|42->16|43->18|43->18|43->18|44->19|44->19|44->19|44->20|44->20|44->20|45->21|45->21|45->21|45->21|45->21|46->22|46->22|47->24|47->24|47->25|48->27|51->30|51->30|52->31|52->31|56->35|56->35|56->35|60->39|61->40|61->40|61->40|61->40|63->42|63->42|65->44|67->46|67->46|67->46|70->49|70->49|71->50|71->50|74->53|74->53|74->53|75->54|75->54|76->59|77->60|80->63|80->63|82->65|82->65|85->68|85->68|85->68|86->69|86->69|86->69|87->70|87->70|87->70|88->71|89->72|89->72|89->72|90->73|92->75|92->75|94->77|96->79|96->79|98->81|98->81|98->81|98->81|99->82|99->82|99->82|99->82|100->83|103->86|103->86|105->88|110->93|110->93|111->94|111->94|111->94|112->95|112->95|112->95|113->96|113->96|115->98|115->98|117->100|117->100|117->100|117->100|117->100|121->104|121->104|121->104|122->105|122->105|123->106|123->106|123->106|123->106|125->108|125->108|125->108|128->111|130->113|130->113|131->114|131->114|131->114|132->115|132->115|133->116|133->116|134->117|134->117|134->117|134->117|139->122|139->122|140->123|140->123|144->127|144->127|145->128|145->128|153->136|153->136|155->138|155->138|158->141|158->141|159->142|159->142|161->144|161->144|164->147|164->147|166->149|166->149|167->150|167->150|174->157|174->157|176->159|176->159|180->163|180->163|181->164|181->164|183->166|183->166|185->168|185->168|191->174|191->174|195->178|195->178|196->179|196->179|200->183|200->183|201->184|201->184|205->188|205->188|207->190|207->190|214->197|214->197|217->200|217->200|220->203|220->203|223->206|223->206|227->210|227->210|229->212|229->212|230->213|230->213|232->215|232->215|233->216|233->216|233->216|233->216|233->216|234->217|234->217|234->217|235->218|235->218|235->218|235->218|236->219|236->219|236->219|237->220|237->220|239->222|239->222|240->223|240->223|240->223|243->226|246->229
                    -- GENERATED --
                */
            