
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object editUsers extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[ProjectSpace,Option[User],Map[User, String],List[User],List[String],Map[String, String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: ProjectSpace, creator: Option[User], userRoleMap: Map[User, String], externalUsers: List[User], roleList: List[String], roleDescription:Map[String, String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.libs.json._

import play.api.Play.current


Seq[Any](format.raw/*1.202*/("""

"""),format.raw/*6.1*/("""

<div class="row">
    <div class="col-md-12">
        """),_display_(Seq[Any](/*10.10*/if(creator.isDefined)/*10.31*/ {_display_(Seq[Any](format.raw/*10.33*/("""
            <h3>
                """),_display_(Seq[Any](/*12.18*/Messages("owner.label"))),format.raw/*12.41*/(""": <a href= """"),_display_(Seq[Any](/*12.54*/routes/*12.60*/.Profile.viewProfileUUID(space.creator))),format.raw/*12.99*/("""" title=""""),_display_(Seq[Any](/*12.109*/creator/*12.116*/.get.email)),format.raw/*12.126*/(""""> """),_display_(Seq[Any](/*12.130*/creator/*12.137*/.get.fullName)),format.raw/*12.150*/("""</a>
            </h3>

            <br />
        """)))})),format.raw/*16.10*/("""

        <br />

        <div id="user-list">
            """),_display_(Seq[Any](/*21.14*/for(roleLevel <- roleList) yield /*21.40*/{_display_(Seq[Any](format.raw/*21.41*/("""
                <h4>"""),_display_(Seq[Any](/*22.22*/roleLevel)),format.raw/*22.31*/("""</h4>
                <p>"""),_display_(Seq[Any](/*23.21*/roleDescription(roleLevel))),format.raw/*23.47*/("""</p>
                """),_display_(Seq[Any](/*24.18*/defining(roleLevel.replaceAll("\\s+", ""))/*24.60*/ { currRole =>_display_(Seq[Any](format.raw/*24.74*/("""

                    <ul id='"""),_display_(Seq[Any](/*26.30*/currRole/*26.38*/.concat("-current"))),format.raw/*26.57*/("""'>
                        """),_display_(Seq[Any](/*27.26*/for((k,v) <- userRoleMap) yield /*27.51*/ {_display_(Seq[Any](format.raw/*27.53*/("""
                            """),_display_(Seq[Any](/*28.30*/if(roleLevel == v)/*28.48*/ {_display_(Seq[Any](format.raw/*28.50*/("""
                                <li>
                                    <a href= """"),_display_(Seq[Any](/*30.48*/routes/*30.54*/.Profile.viewProfileUUID(k.id))),format.raw/*30.84*/("""" id=""""),_display_(Seq[Any](/*30.91*/k/*30.92*/.id)),format.raw/*30.95*/("""" title=""""),_display_(Seq[Any](/*30.105*/k/*30.106*/.email)),format.raw/*30.112*/("""">
                                        """),_display_(Seq[Any](/*31.42*/k/*31.43*/.format(true))),format.raw/*31.56*/("""</a>
                                    <a class='remove-user' id=""""),_display_(Seq[Any](/*32.65*/k/*32.66*/.id)),format.raw/*32.69*/(""""><span class="glyphicon glyphicon-remove"></span></a>
                                </li>
                            """)))})),format.raw/*34.30*/("""
                        """)))})),format.raw/*35.26*/("""
                    </ul>

                    <ul>
                        <li style="list-style: none">
                            <select class="chosen-select" multiple id='"""),_display_(Seq[Any](/*40.73*/currRole)),format.raw/*40.81*/("""'>
                            """),_display_(Seq[Any](/*41.30*/for(extUser <- externalUsers) yield /*41.59*/ {_display_(Seq[Any](format.raw/*41.61*/("""
                                <option value=""""),_display_(Seq[Any](/*42.49*/extUser/*42.56*/.id)),format.raw/*42.59*/("""">"""),_display_(Seq[Any](/*42.62*/extUser/*42.69*/.format(true))),format.raw/*42.82*/("""</option>
                            """)))})),format.raw/*43.30*/("""
                            </select>
                        </li>
                    </ul>
                    <br />

                """)))})),format.raw/*49.18*/("""
            """)))})),format.raw/*50.14*/("""
            <br />
        </div>

        """),_display_(Seq[Any](/*54.10*/if(Permission.checkPermission(Permission.EditSpace, ResourceRef(ResourceRef.space, space.id)))/*54.104*/ {_display_(Seq[Any](format.raw/*54.106*/("""
            <button class="btn btn-primary" title="Update" onclick="return updateUsersInSpace('"""),_display_(Seq[Any](/*55.97*/space/*55.102*/.id)),format.raw/*55.105*/("""')"><span class="glyphicon glyphicon-saved"></span> Submit</button>
            <button class="btn btn-default" title="Cancel" type="button" id="cancel-button" onClick="window.location.reload();"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
        """)))})),format.raw/*57.10*/("""
    </div>
</div>

<link rel="stylesheet" href=""""),_display_(Seq[Any](/*61.31*/routes/*61.37*/.Assets.at("stylesheets/chosen.css"))),format.raw/*61.73*/("""">
<script src=""""),_display_(Seq[Any](/*62.15*/routes/*62.21*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*62.63*/("""" type="text/javascript"></script>

<script language="javascript">
  $(".chosen-select").chosen("""),format.raw/*65.30*/("""{"""),format.raw/*65.31*/("""
    width: "500px",
    placeholder_text_multiple: "Select Users for this Level...",
    no_results_text: "Sorry, no results match""""),format.raw/*68.47*/("""}"""),format.raw/*68.48*/(""");
</script>

<script language="javascript">
    var idNameObj = """),format.raw/*72.21*/("""{"""),format.raw/*72.22*/("""}"""),format.raw/*72.23*/(""";
    var idEmailObj = """),format.raw/*73.22*/("""{"""),format.raw/*73.23*/("""}"""),format.raw/*73.24*/(""";

    function isUndefined(value) """),format.raw/*75.33*/("""{"""),format.raw/*75.34*/("""
        return typeof value === 'undefined';
    """),format.raw/*77.5*/("""}"""),format.raw/*77.6*/("""

    function getName(id) """),format.raw/*79.26*/("""{"""),format.raw/*79.27*/("""
        return idNameObj[id];
    """),format.raw/*81.5*/("""}"""),format.raw/*81.6*/("""

    function getEmail(id) """),format.raw/*83.27*/("""{"""),format.raw/*83.28*/("""
        return idEmailObj[id];
    """),format.raw/*85.5*/("""}"""),format.raw/*85.6*/("""

    //Define maps to easily look up names and emails based on the ID that is added or removed
    $(document).ready(function() """),format.raw/*88.34*/("""{"""),format.raw/*88.35*/("""
        """),_display_(Seq[Any](/*89.10*/for(extUser <- externalUsers) yield /*89.39*/ {_display_(Seq[Any](format.raw/*89.41*/("""
            idNameObj[""""),_display_(Seq[Any](/*90.25*/extUser/*90.32*/.id)),format.raw/*90.35*/(""""] = """"),_display_(Seq[Any](/*90.42*/extUser/*90.49*/.fullName)),format.raw/*90.58*/("""";
            idEmailObj[""""),_display_(Seq[Any](/*91.26*/extUser/*91.33*/.id)),format.raw/*91.36*/(""""] = """"),_display_(Seq[Any](/*91.43*/extUser/*91.50*/.email)),format.raw/*91.56*/("""";
        """)))})),format.raw/*92.10*/("""

        """),_display_(Seq[Any](/*94.10*/for((k,v) <- userRoleMap) yield /*94.35*/ {_display_(Seq[Any](format.raw/*94.37*/("""
            idNameObj[""""),_display_(Seq[Any](/*95.25*/k/*95.26*/.id)),format.raw/*95.29*/(""""] = """"),_display_(Seq[Any](/*95.36*/k/*95.37*/.fullName)),format.raw/*95.46*/("""";
            idEmailObj[""""),_display_(Seq[Any](/*96.26*/k/*96.27*/.id)),format.raw/*96.30*/(""""] = """"),_display_(Seq[Any](/*96.37*/k/*96.38*/.email)),format.raw/*96.44*/("""";
        """)))})),format.raw/*97.10*/("""
        $(".chosen-choices").addClass("form-control");
    """),format.raw/*99.5*/("""}"""),format.raw/*99.6*/(""");

    //Handle the change events when a user is added or removed from the selection lists
    $(".chosen-select").chosen().change(function (event, params) """),format.raw/*102.66*/("""{"""),format.raw/*102.67*/("""
        var targetId = this.getAttribute("id");
        if (!isUndefined(params.selected)) """),format.raw/*104.44*/("""{"""),format.raw/*104.45*/("""
            //Remove the newly selected ID from the options lists
            var addedId = params.selected;
            """),_display_(Seq[Any](/*107.14*/for(roleLevel <- roleList) yield /*107.40*/ {_display_(Seq[Any](format.raw/*107.42*/("""
                var currentId = '"""),_display_(Seq[Any](/*108.35*/roleLevel/*108.44*/.replaceAll("\\s+", ""))),format.raw/*108.67*/("""';
                //Check to see if we are operating on the original target of the event. If so, don't do anything, it handles
                //it on its own.
                if (currentId != targetId) """),format.raw/*111.44*/("""{"""),format.raw/*111.45*/("""
                    $('#"""),_display_(Seq[Any](/*112.26*/roleLevel/*112.35*/.replaceAll("\\s+", ""))),format.raw/*112.58*/(""" option[value=' + addedId + ']').remove();
                    $('#"""),_display_(Seq[Any](/*113.26*/roleLevel/*113.35*/.replaceAll("\\s+", ""))),format.raw/*113.58*/("""').trigger("chosen:updated");
                """),format.raw/*114.17*/("""}"""),format.raw/*114.18*/("""
            """)))})),format.raw/*115.14*/("""
        """),format.raw/*116.9*/("""}"""),format.raw/*116.10*/("""
        else if (!isUndefined(params.deselected)) """),format.raw/*117.51*/("""{"""),format.raw/*117.52*/("""
            //Add the newly deselected ID to the options lists
            var removedId = params.deselected;
            """),_display_(Seq[Any](/*120.14*/for(roleLevel <- roleList) yield /*120.40*/ {_display_(Seq[Any](format.raw/*120.42*/("""
                var currentId = '"""),_display_(Seq[Any](/*121.35*/roleLevel/*121.44*/.replaceAll("\\s+", ""))),format.raw/*121.67*/("""';
                //Check to see if we are operating on the original target of the event. If so, don't do anything, it handles
                //it on its own.
                if (currentId != targetId) """),format.raw/*124.44*/("""{"""),format.raw/*124.45*/("""
                    $('#"""),_display_(Seq[Any](/*125.26*/roleLevel/*125.35*/.replaceAll("\\s+", ""))),format.raw/*125.58*/("""').prepend($("<option></option>").attr("value", params.deselected).text(getName(params.deselected)
                        + " (" + getEmail(params.deselected) + ")"));
                    $('#"""),_display_(Seq[Any](/*127.26*/roleLevel/*127.35*/.replaceAll("\\s+", ""))),format.raw/*127.58*/("""').trigger("chosen:updated");
                """),format.raw/*128.17*/("""}"""),format.raw/*128.18*/("""
            """)))})),format.raw/*129.14*/("""
        """),format.raw/*130.9*/("""}"""),format.raw/*130.10*/("""
    """),format.raw/*131.5*/("""}"""),format.raw/*131.6*/(""");


    $(document).on("click",".remove-user", function()"""),format.raw/*134.54*/("""{"""),format.raw/*134.55*/("""
        $( this ).closest( "LI" ).remove();
        var targetId = this.getAttribute("id");

        """),_display_(Seq[Any](/*138.10*/for(roleLevel <- roleList) yield /*138.36*/ {_display_(Seq[Any](format.raw/*138.38*/("""
                var currentId = '"""),_display_(Seq[Any](/*139.35*/roleLevel/*139.44*/.replaceAll("\\s+", ""))),format.raw/*139.67*/("""';
                //Check to see if we are operating on the original target of the event. If so, don't do anything, it handles
                //it on its own.

                 $('#"""),_display_(Seq[Any](/*143.23*/roleLevel/*143.32*/.replaceAll("\\s+", ""))),format.raw/*143.55*/("""').prepend($("<option></option>").attr("value", targetId).text(getName(targetId)
                        + " (" + getEmail(targetId) + ")"));
                 $('#"""),_display_(Seq[Any](/*145.23*/roleLevel/*145.32*/.replaceAll("\\s+", ""))),format.raw/*145.55*/("""').trigger("chosen:updated");

            """)))})),format.raw/*147.14*/("""
        $( this ).closest( "UL" ).trigger('contentchange');


    var request = jsRoutes.api.Spaces.removeUser('"""),_display_(Seq[Any](/*151.52*/space/*151.57*/.id)),format.raw/*151.60*/("""', this.id).ajax("""),format.raw/*151.77*/("""{"""),format.raw/*151.78*/("""
        type : 'POST'
    """),format.raw/*153.5*/("""}"""),format.raw/*153.6*/(""");
    request.done ( function ( response, textStatus, jqXHR ) """),format.raw/*154.61*/("""{"""),format.raw/*154.62*/("""
        console.log("Successful remove user " + this.id);
    """),format.raw/*156.5*/("""}"""),format.raw/*156.6*/(""");
    request.fail(function(jqXHR) """),format.raw/*157.34*/("""{"""),format.raw/*157.35*/("""
        console.error("ERROR occurs when deleting user "+ this.id);
    """),format.raw/*159.5*/("""}"""),format.raw/*159.6*/(""");
    """),format.raw/*160.5*/("""}"""),format.raw/*160.6*/(""");
</script>

<script type="text/javascript">
    var roleArray = """),_display_(Seq[Any](/*164.22*/Html(Json.stringify(Json.toJson(roleList))))),format.raw/*164.65*/("""
</script>
<script src=""""),_display_(Seq[Any](/*166.15*/routes/*166.21*/.Assets.at("javascripts/spaceModify.js"))),format.raw/*166.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*167.15*/routes/*167.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*167.63*/("""" language="javascript"></script>

"""))}
    }
    
    def render(space:ProjectSpace,creator:Option[User],userRoleMap:Map[User, String],externalUsers:List[User],roleList:List[String],roleDescription:Map[String, String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space,creator,userRoleMap,externalUsers,roleList,roleDescription)(user)
    
    def f:((ProjectSpace,Option[User],Map[User, String],List[User],List[String],Map[String, String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space,creator,userRoleMap,externalUsers,roleList,roleDescription) => (user) => apply(space,creator,userRoleMap,externalUsers,roleList,roleDescription)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/editUsers.scala.html
                    HASH: fc49d9be0d758427d20d8811648044510283575f
                    MATRIX: 699->1|1075->201|1103->285|1196->342|1226->363|1266->365|1337->400|1382->423|1431->436|1446->442|1507->481|1554->491|1571->498|1604->508|1645->512|1662->519|1698->532|1782->584|1878->644|1920->670|1959->671|2017->693|2048->702|2110->728|2158->754|2216->776|2267->818|2319->832|2386->863|2403->871|2444->890|2508->918|2549->943|2589->945|2655->975|2682->993|2722->995|2843->1080|2858->1086|2910->1116|2953->1123|2963->1124|2988->1127|3035->1137|3046->1138|3075->1144|3155->1188|3165->1189|3200->1202|3305->1271|3315->1272|3340->1275|3494->1397|3552->1423|3767->1602|3797->1610|3865->1642|3910->1671|3950->1673|4035->1722|4051->1729|4076->1732|4115->1735|4131->1742|4166->1755|4237->1794|4409->1934|4455->1948|4536->1993|4640->2087|4681->2089|4814->2186|4829->2191|4855->2194|5157->2464|5243->2514|5258->2520|5316->2556|5369->2573|5384->2579|5448->2621|5572->2717|5601->2718|5761->2850|5790->2851|5883->2916|5912->2917|5941->2918|5992->2941|6021->2942|6050->2943|6113->2978|6142->2979|6219->3029|6247->3030|6302->3057|6331->3058|6393->3093|6421->3094|6477->3122|6506->3123|6569->3159|6597->3160|6754->3289|6783->3290|6829->3300|6874->3329|6914->3331|6975->3356|6991->3363|7016->3366|7059->3373|7075->3380|7106->3389|7170->3417|7186->3424|7211->3427|7254->3434|7270->3441|7298->3447|7342->3459|7389->3470|7430->3495|7470->3497|7531->3522|7541->3523|7566->3526|7609->3533|7619->3534|7650->3543|7714->3571|7724->3572|7749->3575|7792->3582|7802->3583|7830->3589|7874->3601|7961->3661|7989->3662|8175->3819|8205->3820|8326->3912|8356->3913|8516->4036|8559->4062|8600->4064|8672->4099|8691->4108|8737->4131|8970->4335|9000->4336|9063->4362|9082->4371|9128->4394|9233->4462|9252->4471|9298->4494|9373->4540|9403->4541|9450->4555|9487->4564|9517->4565|9597->4616|9627->4617|9788->4741|9831->4767|9872->4769|9944->4804|9963->4813|10009->4836|10242->5040|10272->5041|10335->5067|10354->5076|10400->5099|10631->5293|10650->5302|10696->5325|10771->5371|10801->5372|10848->5386|10885->5395|10915->5396|10948->5401|10977->5402|11064->5460|11094->5461|11234->5564|11277->5590|11318->5592|11390->5627|11409->5636|11455->5659|11676->5843|11695->5852|11741->5875|11942->6039|11961->6048|12007->6071|12084->6115|12235->6229|12250->6234|12276->6237|12322->6254|12352->6255|12407->6282|12436->6283|12528->6346|12558->6347|12649->6410|12678->6411|12743->6447|12773->6448|12874->6521|12903->6522|12938->6529|12967->6530|13071->6597|13137->6640|13199->6665|13215->6671|13278->6711|13364->6760|13380->6766|13445->6808
                    LINES: 20->1|28->1|30->6|34->10|34->10|34->10|36->12|36->12|36->12|36->12|36->12|36->12|36->12|36->12|36->12|36->12|36->12|40->16|45->21|45->21|45->21|46->22|46->22|47->23|47->23|48->24|48->24|48->24|50->26|50->26|50->26|51->27|51->27|51->27|52->28|52->28|52->28|54->30|54->30|54->30|54->30|54->30|54->30|54->30|54->30|54->30|55->31|55->31|55->31|56->32|56->32|56->32|58->34|59->35|64->40|64->40|65->41|65->41|65->41|66->42|66->42|66->42|66->42|66->42|66->42|67->43|73->49|74->50|78->54|78->54|78->54|79->55|79->55|79->55|81->57|85->61|85->61|85->61|86->62|86->62|86->62|89->65|89->65|92->68|92->68|96->72|96->72|96->72|97->73|97->73|97->73|99->75|99->75|101->77|101->77|103->79|103->79|105->81|105->81|107->83|107->83|109->85|109->85|112->88|112->88|113->89|113->89|113->89|114->90|114->90|114->90|114->90|114->90|114->90|115->91|115->91|115->91|115->91|115->91|115->91|116->92|118->94|118->94|118->94|119->95|119->95|119->95|119->95|119->95|119->95|120->96|120->96|120->96|120->96|120->96|120->96|121->97|123->99|123->99|126->102|126->102|128->104|128->104|131->107|131->107|131->107|132->108|132->108|132->108|135->111|135->111|136->112|136->112|136->112|137->113|137->113|137->113|138->114|138->114|139->115|140->116|140->116|141->117|141->117|144->120|144->120|144->120|145->121|145->121|145->121|148->124|148->124|149->125|149->125|149->125|151->127|151->127|151->127|152->128|152->128|153->129|154->130|154->130|155->131|155->131|158->134|158->134|162->138|162->138|162->138|163->139|163->139|163->139|167->143|167->143|167->143|169->145|169->145|169->145|171->147|175->151|175->151|175->151|175->151|175->151|177->153|177->153|178->154|178->154|180->156|180->156|181->157|181->157|183->159|183->159|184->160|184->160|188->164|188->164|190->166|190->166|190->166|191->167|191->167|191->167
                    -- GENERATED --
                */
            