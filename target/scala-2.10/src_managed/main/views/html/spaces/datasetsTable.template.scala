
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object datasetsTable extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[Dataset],String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(datasets: List[Dataset], space: String):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.42*/("""

<p>
    Most recent datasets:
    <a href=""""),_display_(Seq[Any](/*5.15*/(routes.Datasets.list("", "", 12, Some(space))))),format.raw/*5.62*/("""" class="pull-right btn btn-default btn-xs">
        <span class="glyphicon glyphicon-hand-right"></span>
        All Datasets
    </a>
</p>

<table id='spacesDatasetsTable' class="table table-bordered table-hover fixedtable">
    <thead>
        <tr>
            <th style="width: 27%">Name</th>
            <th style="width: 17%">Created</th>
            <th style="width: 36%">Description</th>
            <th style="width: 20%"></th>
        </tr>
    </thead>
    <tbody>
    """),_display_(Seq[Any](/*21.6*/datasets/*21.14*/.map/*21.18*/ { dataset =>_display_(Seq[Any](format.raw/*21.31*/("""
        <tr data-datasetId=""""),_display_(Seq[Any](/*22.30*/(dataset.id.toString))),format.raw/*22.51*/("""">
            <td><a href=""""),_display_(Seq[Any](/*23.27*/(routes.Datasets.dataset(dataset.id, Some(space))))),format.raw/*23.77*/("""">"""),_display_(Seq[Any](/*23.80*/dataset/*23.87*/.name)),format.raw/*23.92*/("""</a></td>
            <td>"""),_display_(Seq[Any](/*24.18*/dataset/*24.25*/.created.format("MMM dd, yyyy"))),format.raw/*24.56*/("""</td>
            <td style="white-space:pre-line;">"""),_display_(Seq[Any](/*25.48*/dataset/*25.55*/.description)),format.raw/*25.67*/("""</td>
            <td>
                """),_display_(Seq[Any](/*27.18*/if(!dataset.thumbnail_id.isEmpty)/*27.51*/{_display_(Seq[Any](format.raw/*27.52*/("""
                    <div class="fit-in-space">
                        <a href=""""),_display_(Seq[Any](/*29.35*/(routes.Datasets.dataset(dataset.id)))),format.raw/*29.72*/("""">
                            <img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*30.76*/(routes.Files.thumbnail(UUID(dataset.thumbnail_id.toString().substring(5,dataset.thumbnail_id.toString().length-1)))))),format.raw/*30.193*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*30.214*/Html(dataset.name))),format.raw/*30.232*/("""">
                        </a>
                    </div>
                """)))})),format.raw/*33.18*/("""
                """),_display_(Seq[Any](/*34.18*/if(dataset.thumbnail_id.isEmpty)/*34.50*/{_display_(Seq[Any](format.raw/*34.51*/("""No thumbnail available""")))})),format.raw/*34.74*/("""
            </td>
        </tr>
    """)))})),format.raw/*37.6*/("""
    </tbody>
</table>
"""))}
    }
    
    def render(datasets:List[Dataset],space:String): play.api.templates.HtmlFormat.Appendable = apply(datasets,space)
    
    def f:((List[Dataset],String) => play.api.templates.HtmlFormat.Appendable) = (datasets,space) => apply(datasets,space)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/datasetsTable.scala.html
                    HASH: 6e21f05f2f0dbbd7243fc9ce2699164801b6610a
                    MATRIX: 616->1|750->41|831->87|899->134|1416->616|1433->624|1446->628|1497->641|1563->671|1606->692|1671->721|1743->771|1782->774|1798->781|1825->786|1888->813|1904->820|1957->851|2046->904|2062->911|2096->923|2172->963|2214->996|2253->997|2371->1079|2430->1116|2544->1194|2684->1311|2742->1332|2783->1350|2891->1426|2945->1444|2986->1476|3025->1477|3080->1500|3149->1538
                    LINES: 20->1|23->1|27->5|27->5|43->21|43->21|43->21|43->21|44->22|44->22|45->23|45->23|45->23|45->23|45->23|46->24|46->24|46->24|47->25|47->25|47->25|49->27|49->27|49->27|51->29|51->29|52->30|52->30|52->30|52->30|55->33|56->34|56->34|56->34|56->34|59->37
                    -- GENERATED --
                */
            