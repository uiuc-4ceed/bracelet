
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object verifySpaceEmail extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[String,String,String,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(spaceid: String, spaceName: String, member: String)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider

import play.api.i18n.Messages


Seq[Any](format.raw/*1.87*/("""
"""),format.raw/*4.1*/("""<html>
  <body>

    <p>Hello """),_display_(Seq[Any](/*7.15*/member)),format.raw/*7.21*/(""",</p>

    <p>Congratulations, your """),_display_(Seq[Any](/*9.31*/services/*9.39*/.AppConfiguration.getDisplayName)),format.raw/*9.71*/(""" """),_display_(Seq[Any](/*9.73*/Messages("space.title"))),format.raw/*9.96*/(""" <a href=""""),_display_(Seq[Any](/*9.107*/routes/*9.113*/.Spaces.getSpace(UUID(spaceid)).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.185*/("""">"""),_display_(Seq[Any](/*9.188*/spaceName)),format.raw/*9.197*/("""</a> has been verified!
      """),_display_(Seq[Any](/*10.8*/play/*10.12*/.api.i18n.Messages("verify.email.content"))),format.raw/*10.54*/("""
    </p>


    <p>Please contact <a style="text-decoration:none;" href=""""),_display_(Seq[Any](/*14.63*/play/*14.67*/.api.i18n.Messages("verify.email.contact.link"))),format.raw/*14.114*/(""""> """),_display_(Seq[Any](/*14.118*/services/*14.126*/.AppConfiguration.getDisplayName)),format.raw/*14.158*/(""" </a> if you have questions or feedback.</p>
    """),_display_(Seq[Any](/*15.6*/emails/*15.12*/.footer())),format.raw/*15.21*/("""
  </body>
</html>"""))}
    }
    
    def render(spaceid:String,spaceName:String,member:String,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(spaceid,spaceName,member)(request)
    
    def f:((String,String,String) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (spaceid,spaceName,member) => (request) => apply(spaceid,spaceName,member)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/verifySpaceEmail.scala.html
                    HASH: 90efcf9a0949d815a2a7a23aa444d3b88a6539aa
                    MATRIX: 633->1|885->86|912->161|978->192|1005->198|1077->235|1093->243|1146->275|1183->277|1227->300|1274->311|1289->317|1383->389|1422->392|1453->401|1519->432|1532->436|1596->478|1706->552|1719->556|1789->603|1830->607|1848->615|1903->647|1988->697|2003->703|2034->712
                    LINES: 20->1|26->1|27->4|30->7|30->7|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|33->10|33->10|33->10|37->14|37->14|37->14|37->14|37->14|37->14|38->15|38->15|38->15
                    -- GENERATED --
                */
            