
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object spaceConfiguration extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[ProjectSpace,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: ProjectSpace)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.59*/("""

<script src=""""),_display_(Seq[Any](/*3.15*/routes/*3.21*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*3.66*/("""" language="javascript"></script>
<script src=""""),_display_(Seq[Any](/*4.15*/routes/*4.21*/.Assets.at("javascripts/spaceconfiguration.js"))),format.raw/*4.68*/("""" language="javascript"></script>
<script src=""""),_display_(Seq[Any](/*5.15*/routes/*5.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*5.63*/("""" language="javascript"></script>

<div class="row ds-section-sm border-top">
	 <!-- Advanced space elements -->
	 <h3>Advanced Configuration</h3>
	 <table>
		 <tr>
			 <td><b>Expiration Enabled: </b></td>
			 <td id="expireenabled" style="padding-left:10px;">empty</td>
		 </tr>
	     <tr>
	         <td><b>Time To Live (hours): </b></td>
	         <td id="timetolive" style="padding-left:10px;">empty</td>
	     </tr>

	 </table>
	 <p>

	
	 <script type="text/javascript" language="javascript">
		 $(document).ready(function() """),format.raw/*25.33*/("""{"""),format.raw/*25.34*/("""    
		     var timeToLive = """),_display_(Seq[Any](/*26.26*/space/*26.31*/.resourceTimeToLive)),format.raw/*26.50*/(""";
		     timeToLive = timeToLive/3600000;
		     $('#timetolive').html(timeToLive);
		     
		     var $radios = $('input:radio[name=radiogroup]');
		     var isEnabled = """),_display_(Seq[Any](/*31.25*/space/*31.30*/.isTimeToLiveEnabled)),format.raw/*31.50*/(""";
		     
		     if (isEnabled) """),format.raw/*33.23*/("""{"""),format.raw/*33.24*/("""
		    	 $radios.filter('[value=expiretrue]').prop('checked', true);
		    	 //$('input:radio[name=radiogroup]')[0].checked = true;
		         $('#expireenabled').html("On");
		     """),format.raw/*37.8*/("""}"""),format.raw/*37.9*/("""
		     else """),format.raw/*38.13*/("""{"""),format.raw/*38.14*/("""
		    	 $radios.filter('[value=expirefalse]').prop('checked', true);
		    	 //$('input:radio[name=radiogroup]')[1].checked = true;
		         $('#expireenabled').html("Off");
		     """),format.raw/*42.8*/("""}"""),format.raw/*42.9*/("""

		"""),format.raw/*44.3*/("""}"""),format.raw/*44.4*/(""");
	 </script>
<!-- End License elements -->
</div>"""))}
    }
    
    def render(space:ProjectSpace,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space)(user)
    
    def f:((ProjectSpace) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space) => (user) => apply(space)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/spaceConfiguration.scala.html
                    HASH: 7817a795cc73554e0d3fbd676b10f59f057b299e
                    MATRIX: 633->1|784->58|835->74|849->80|915->125|998->173|1012->179|1080->226|1163->274|1177->280|1240->322|1797->851|1826->852|1892->882|1906->887|1947->906|2155->1078|2169->1083|2211->1103|2271->1135|2300->1136|2509->1318|2537->1319|2578->1332|2607->1333|2818->1517|2846->1518|2877->1522|2905->1523
                    LINES: 20->1|23->1|25->3|25->3|25->3|26->4|26->4|26->4|27->5|27->5|27->5|47->25|47->25|48->26|48->26|48->26|53->31|53->31|53->31|55->33|55->33|59->37|59->37|60->38|60->38|64->42|64->42|66->44|66->44
                    -- GENERATED --
                */
            