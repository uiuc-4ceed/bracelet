
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object users extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template10[Form[controllers.spaceInviteData],ProjectSpace,Option[User],Map[User, String],List[User],List[String],List[SpaceInvite],Map[String, String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[controllers.spaceInviteData], space: ProjectSpace, creator: Option[User], userRoleMap: Map[User, String], externalUsers: List[User], roleList: List[String], invite:List[SpaceInvite], roleDescription:Map[String, String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import api.Permission

import _root_.util.Formatters._


Seq[Any](format.raw/*1.298*/("""
"""),format.raw/*5.1*/("""
"""),_display_(Seq[Any](/*6.2*/main(space.name)/*6.18*/ {_display_(Seq[Any](format.raw/*6.20*/("""


    <div class="row">
        <div class="col-md-12">

            <ol class="breadcrumb">
                <li><span class="glyphicon glyphicon-hdd"></span><a href= """"),_display_(Seq[Any](/*13.77*/routes/*13.83*/.Spaces.getSpace(space.id))),format.raw/*13.109*/("""" title=""""),_display_(Seq[Any](/*13.119*/space/*13.124*/.name)),format.raw/*13.129*/(""""> """),_display_(Seq[Any](/*13.133*/Html(ellipsize(space.name, 18)))),format.raw/*13.164*/("""</a></li>
                <li><span class="glyphicon glyphicon-user"></span> Manage Users</li>
            </ol>

            <div class="row">
                <div class="col-md-12" id="s-title">
                    <h1 id="space-title" class="inline">Manage Users of """),_display_(Seq[Any](/*19.74*/Messages("space.title"))),format.raw/*19.97*/("""
                        <a href= """"),_display_(Seq[Any](/*20.36*/routes/*20.42*/.Spaces.getSpace(space.id))),format.raw/*20.68*/("""" title=""""),_display_(Seq[Any](/*20.78*/space/*20.83*/.name)),format.raw/*20.88*/(""""> """),_display_(Seq[Any](/*20.92*/space/*20.97*/.name)),format.raw/*20.102*/("""</a>
                    </h1>
                </div>
            </div>

            """),_display_(Seq[Any](/*25.14*/if(Permission.checkPermission(Permission.EditSpace, ResourceRef(ResourceRef.space, space.id)))/*25.108*/ {_display_(Seq[Any](format.raw/*25.110*/("""

                    <div class="col-md-12 box-white-space">
                        <div class="tabbable" id="manage-access-tabbable"> <!-- Only required for left/right tabs -->
                            <!-- Display the tabs with their titles, etc. -->
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="active">
                                    <a href="#tab-home" role="tab" data-toggle="tab"> Users</a>
                                </li>
                                <li role="presentation">
                                    <a href="#tab-invite" role="tab" data-toggle="tab"> Invites</a>
                                </li>
                                <li role="presentation">
                                    <a id="request-counter" href="#tab-outstanding" role="tab" data-toggle="tab"> Requests ("""),_display_(Seq[Any](/*38.126*/space/*38.131*/.requests.length)),format.raw/*38.147*/(""")</a>
                                </li>
                            </ul>
                            <br /> <br />
                            <!-- Define the tabs' contents. -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="tab-home">
                                    """),_display_(Seq[Any](/*45.38*/spaces/*45.44*/.editUsers(space, creator, userRoleMap, externalUsers, roleList, roleDescription))),format.raw/*45.125*/("""
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab-invite">
                                    """),_display_(Seq[Any](/*48.38*/spaces/*48.44*/.invite(myForm, space, roleList, invite))),format.raw/*48.84*/("""
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab-outstanding">
                                    """),_display_(Seq[Any](/*51.38*/spaces/*51.44*/.outstandingRequests(space, creator, userRoleMap, externalUsers, roleList))),format.raw/*51.118*/("""
                                </div>
                            </div>
                        </div>
                    </div>

            """)))})),format.raw/*57.14*/("""
        </div>
    </div>
""")))})))}
    }
    
    def render(myForm:Form[controllers.spaceInviteData],space:ProjectSpace,creator:Option[User],userRoleMap:Map[User, String],externalUsers:List[User],roleList:List[String],invite:List[SpaceInvite],roleDescription:Map[String, String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm,space,creator,userRoleMap,externalUsers,roleList,invite,roleDescription)(flash,user)
    
    def f:((Form[controllers.spaceInviteData],ProjectSpace,Option[User],Map[User, String],List[User],List[String],List[SpaceInvite],Map[String, String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm,space,creator,userRoleMap,externalUsers,roleList,invite,roleDescription) => (flash,user) => apply(myForm,space,creator,userRoleMap,externalUsers,roleList,invite,roleDescription)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:49 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/users.scala.html
                    HASH: 3d7a268a62b75ce1f0c5839930e549c331d6caf9
                    MATRIX: 767->1|1244->297|1271->385|1307->387|1331->403|1370->405|1576->575|1591->581|1640->607|1687->617|1702->622|1730->627|1771->631|1825->662|2131->932|2176->955|2248->991|2263->997|2311->1023|2357->1033|2371->1038|2398->1043|2438->1047|2452->1052|2480->1057|2603->1144|2707->1238|2748->1240|3680->2135|3695->2140|3734->2156|4145->2531|4160->2537|4264->2618|4464->2782|4479->2788|4541->2828|4746->2997|4761->3003|4858->3077|5037->3224
                    LINES: 20->1|28->1|29->5|30->6|30->6|30->6|37->13|37->13|37->13|37->13|37->13|37->13|37->13|37->13|43->19|43->19|44->20|44->20|44->20|44->20|44->20|44->20|44->20|44->20|44->20|49->25|49->25|49->25|62->38|62->38|62->38|69->45|69->45|69->45|72->48|72->48|72->48|75->51|75->51|75->51|81->57
                    -- GENERATED --
                */
            