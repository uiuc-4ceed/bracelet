
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object collectionsGrid extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[List[Collection],ProjectSpace,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(collectionsList: List[Collection], space: ProjectSpace)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import api.Permission


Seq[Any](format.raw/*1.94*/("""
"""),format.raw/*4.1*/("""<p>
    <span class="small">"""),_display_(Seq[Any](/*5.26*/Messages("recent.message", Messages("collections.title").toLowerCase))),format.raw/*5.95*/("""</span>
    <span class="pull-right">
    """),_display_(Seq[Any](/*7.6*/if(play.Play.application().configuration().getBoolean("sortInMemory"))/*7.76*/ {_display_(Seq[Any](format.raw/*7.78*/("""
        <a href=""""),_display_(Seq[Any](/*8.19*/(routes.Collections.sortedListInSpace(space.id.stringify, 0, 12)))),format.raw/*8.84*/("""" class="btn btn-link btn-xs">
    """)))}/*9.7*/else/*9.12*/{_display_(Seq[Any](format.raw/*9.13*/("""
        <a href=""""),_display_(Seq[Any](/*10.19*/(routes.Collections.list("", "", 12, Some(space.id.stringify))))),format.raw/*10.82*/("""" class="btn btn-link btn-xs">
    """)))})),format.raw/*11.6*/("""
            <span class="glyphicon glyphicon-th-large"></span>
             """),_display_(Seq[Any](/*13.15*/Messages("view.all", Messages("collections.title")))),format.raw/*13.66*/("""
        </a>
    </span>

</p>

"""),_display_(Seq[Any](/*19.2*/util/*19.6*/.masonry())),format.raw/*19.16*/("""

<div class="row" id="tile-view">
    <div class="col-md-12">
        <div id="masonry-collections">
        """),_display_(Seq[Any](/*24.10*/if(user.isDefined || space.isPublic)/*24.46*/ {_display_(Seq[Any](format.raw/*24.48*/("""
        """),_display_(Seq[Any](/*25.10*/collectionsList/*25.25*/.map/*25.29*/ { collection =>_display_(Seq[Any](format.raw/*25.45*/("""
                """),_display_(Seq[Any](/*26.18*/collections/*26.29*/.tile(collection, routes.Spaces.getSpace(UUID(space.id.stringify)), Some(space.id.stringify), "col-lg-4 col-md-4 col-sm-4", false))),format.raw/*26.159*/("""
            """)))})),format.raw/*27.14*/("""
        """)))})),format.raw/*28.10*/("""
        </div>
    </div>
</div>

"""),_display_(Seq[Any](/*33.2*/if(!user.isDefined)/*33.21*/ {_display_(Seq[Any](format.raw/*33.23*/("""
    <script type="text/javascript">
        $('#create-collection').addClass('disabled');
    </script>
""")))})),format.raw/*37.2*/("""
"""))}
    }
    
    def render(collectionsList:List[Collection],space:ProjectSpace,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(collectionsList,space)(user)
    
    def f:((List[Collection],ProjectSpace) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (collectionsList,space) => (user) => apply(collectionsList,space)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/collectionsGrid.scala.html
                    HASH: 311097e57240c62dea6419d5243532ef1399555d
                    MATRIX: 647->1|886->93|913->148|977->177|1067->246|1144->289|1222->359|1261->361|1315->380|1401->445|1454->482|1466->487|1504->488|1559->507|1644->570|1711->606|1825->684|1898->735|1967->769|1979->773|2011->783|2158->894|2203->930|2243->932|2289->942|2313->957|2326->961|2380->977|2434->995|2454->1006|2607->1136|2653->1150|2695->1160|2766->1196|2794->1215|2834->1217|2971->1323
                    LINES: 20->1|26->1|27->4|28->5|28->5|30->7|30->7|30->7|31->8|31->8|32->9|32->9|32->9|33->10|33->10|34->11|36->13|36->13|42->19|42->19|42->19|47->24|47->24|47->24|48->25|48->25|48->25|48->25|49->26|49->26|49->26|50->27|51->28|56->33|56->33|56->33|60->37
                    -- GENERATED --
                */
            