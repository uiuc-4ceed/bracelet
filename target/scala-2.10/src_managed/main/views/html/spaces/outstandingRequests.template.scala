
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object outstandingRequests extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[ProjectSpace,Option[User],Map[User, String],List[User],List[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: ProjectSpace, creator: Option[User], userRoleMap: Map[User, String], externalUsers: List[User], roleList: List[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.libs.json._

import play.api.Play.current


Seq[Any](format.raw/*1.165*/("""

"""),format.raw/*6.1*/("""
<div class="row">

            """),_display_(Seq[Any](/*9.14*/if(space.requests.isEmpty)/*9.40*/ {_display_(Seq[Any](format.raw/*9.42*/("""
                <ul class="list-unstyled"><li>No outstanding requests</li></ul>
            """)))}/*11.15*/else/*11.20*/{_display_(Seq[Any](format.raw/*11.21*/("""
                <div class="col-md-6">
                <table id="outstanding-requests" class="table borderless">
                    <tbody >
                        """),_display_(Seq[Any](/*15.26*/for(user <- space.requests) yield /*15.53*/ {_display_(Seq[Any](format.raw/*15.55*/("""
                            <tr id="request-tr-"""),_display_(Seq[Any](/*16.49*/user/*16.53*/.id.stringify)),format.raw/*16.66*/("""">
                                <td>
                                    <a href=""""),_display_(Seq[Any](/*18.47*/routes/*18.53*/.Profile.viewProfileUUID(user.id))),format.raw/*18.86*/("""">"""),_display_(Seq[Any](/*18.89*/user/*18.93*/.name)),format.raw/*18.98*/("""</a>
                                </td>

                                <td>
                                    <select id="roleSelect-"""),_display_(Seq[Any](/*22.61*/user/*22.65*/.id.stringify)),format.raw/*22.78*/("""" class="chosen-select btn btn-success btn-xs">
                                        """),_display_(Seq[Any](/*23.42*/for(aRole <- roleList) yield /*23.64*/ {_display_(Seq[Any](format.raw/*23.66*/("""
                                            <h5>"""),_display_(Seq[Any](/*24.50*/aRole)),format.raw/*24.55*/("""</h5>
                                            <option value=""""),_display_(Seq[Any](/*25.61*/aRole)),format.raw/*25.66*/("""">"""),_display_(Seq[Any](/*25.69*/aRole)),format.raw/*25.74*/("""</option>
                                        """)))})),format.raw/*26.42*/("""
                                    </select>

                                    <button class="btn btn-link" id="request-accept-"""),_display_(Seq[Any](/*29.86*/user/*29.90*/.id.stringify)),format.raw/*29.103*/("""" title="Accept request" onclick="return acceptSpaceRequest('"""),_display_(Seq[Any](/*29.165*/space/*29.170*/.id)),format.raw/*29.173*/("""', '"""),_display_(Seq[Any](/*29.178*/user/*29.182*/.id.toString)),format.raw/*29.194*/("""', '"""),_display_(Seq[Any](/*29.199*/user/*29.203*/.name)),format.raw/*29.208*/("""')">
                                        <span class="glyphicon glyphicon-thumbs-up"></span> Accept
                                    </button>&nbsp;
                                    <button class="btn btn-link" id="request-reject-"""),_display_(Seq[Any](/*32.86*/user/*32.90*/.id.stringify)),format.raw/*32.103*/("""" title="Reject request" onclick= "return rejectSpaceRequest('"""),_display_(Seq[Any](/*32.166*/space/*32.171*/.id)),format.raw/*32.174*/("""', '"""),_display_(Seq[Any](/*32.179*/user/*32.183*/.id.toString)),format.raw/*32.195*/("""')">
                                        <span class="glyphicon glyphicon-thumbs-down"></span> Reject
                                    </button>
                                </td>
                            </tr>
                        """)))})),format.raw/*37.26*/("""
                    </tbody>
                </table>

                </div>
               """)))})),format.raw/*42.17*/("""

</div>

<script src=""""),_display_(Seq[Any](/*46.15*/routes/*46.21*/.Assets.at("javascripts/spaceModify.js"))),format.raw/*46.61*/("""" type="text/javascript"></script>

"""))}
    }
    
    def render(space:ProjectSpace,creator:Option[User],userRoleMap:Map[User, String],externalUsers:List[User],roleList:List[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space,creator,userRoleMap,externalUsers,roleList)(user)
    
    def f:((ProjectSpace,Option[User],Map[User, String],List[User],List[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space,creator,userRoleMap,externalUsers,roleList) => (user) => apply(space,creator,userRoleMap,externalUsers,roleList)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/outstandingRequests.scala.html
                    HASH: 8d3df48ead40c01c8783615c2ef01171bf3a51ae
                    MATRIX: 689->1|1028->164|1056->248|1124->281|1158->307|1197->309|1310->404|1323->409|1362->410|1567->579|1610->606|1650->608|1735->657|1748->661|1783->674|1905->760|1920->766|1975->799|2014->802|2027->806|2054->811|2231->952|2244->956|2279->969|2404->1058|2442->1080|2482->1082|2568->1132|2595->1137|2697->1203|2724->1208|2763->1211|2790->1216|2873->1267|3042->1400|3055->1404|3091->1417|3190->1479|3205->1484|3231->1487|3273->1492|3287->1496|3322->1508|3364->1513|3378->1517|3406->1522|3683->1763|3696->1767|3732->1780|3832->1843|3847->1848|3873->1851|3915->1856|3929->1860|3964->1872|4245->2121|4372->2216|4432->2240|4447->2246|4509->2286
                    LINES: 20->1|28->1|30->6|33->9|33->9|33->9|35->11|35->11|35->11|39->15|39->15|39->15|40->16|40->16|40->16|42->18|42->18|42->18|42->18|42->18|42->18|46->22|46->22|46->22|47->23|47->23|47->23|48->24|48->24|49->25|49->25|49->25|49->25|50->26|53->29|53->29|53->29|53->29|53->29|53->29|53->29|53->29|53->29|53->29|53->29|53->29|56->32|56->32|56->32|56->32|56->32|56->32|56->32|56->32|56->32|61->37|66->42|70->46|70->46|70->46
                    -- GENERATED --
                */
            