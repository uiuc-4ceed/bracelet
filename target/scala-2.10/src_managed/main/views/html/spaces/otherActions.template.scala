
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object otherActions extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[ProjectSpace,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: ProjectSpace)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.i18n.Messages


Seq[Any](format.raw/*1.59*/("""
"""),format.raw/*4.1*/("""<div class="row ds-section-sm space-col-right">
    <ul class="list-unstyled">
        """),_display_(Seq[Any](/*6.10*/if(Permission.checkPermission(Permission.EditSpace, ResourceRef(ResourceRef.space, space.id)))/*6.104*/ {_display_(Seq[Any](format.raw/*6.106*/("""
<!--             <li><a href=""""),_display_(Seq[Any](/*7.32*/routes/*7.38*/.Spaces.manageUsers(space.id))),format.raw/*7.67*/("""" class="btn btn-link btn-xs btn-margins">
                <span class = "glyphicon glyphicon-user" aria-hidden="true"></span> Manage Users
            </a></li>
            <li><a href=""""),_display_(Seq[Any](/*10.27*/routes/*10.33*/.Spaces.updateSpace(space.id))),format.raw/*10.62*/("""" class="btn btn-link btn-xs btn-margins">
                <span class = "glyphicon glyphicon-edit" aria-hidden="true"></span> Edit """),_display_(Seq[Any](/*11.91*/Messages("space.title"))),format.raw/*11.114*/("""
            </a></li>
 --><!--             <li><a href=""""),_display_(Seq[Any](/*13.36*/routes/*13.42*/.Metadata.getMetadataBySpace(space.id))),format.raw/*13.80*/("""" class="btn btn-link btn-xs btn-margins">
                <span class = "glyphicon glyphicon-list" aria-hidden="true"></span> Manage Metadata Terms & Definitions
            </a></li>
            <li><a href=""""),_display_(Seq[Any](/*16.27*/routes/*16.33*/.Spaces.selectExtractors(space.id))),format.raw/*16.67*/("""" class="btn btn-link btn-xs btn-margins">
                <span class = "glyphicon glyphicon-fullscreen" aria-hidden="true"></span> Extractors
            </a></li>
 -->      
   """)))}/*20.6*/else/*20.11*/{_display_(Seq[Any](format.raw/*20.12*/("""
<!--             <li><a class="btn btn-link btn-xs disabled btn-margins">
                <span class = "glyphicon glyphicon-user" aria-hidden="true"></span> Manage Users
            </a></li>
            <li><a class="btn btn-link btn-xs disabled btn-margins">
                <span class = "glyphicon glyphicon-edit" aria-hidden="true"></span> Edit """),_display_(Seq[Any](/*25.91*/Messages("space.title"))),format.raw/*25.114*/("""
            </a></li>
 --><!--             <li><a class="btn btn-link btn-xs disabled btn-margins">
                <span class = "glyphicon glyphicon-list" aria-hidden="true"></span> Manage Metadata Terms & Definitions
            </a></li>
            <li><a class="btn btn-link btn-xs disabled btn-margins">
                <span class = "glyphicon glyphicon-fullscreen" aria-hidden="true"></span> Extractors
            </a></li>
 -->        """)))})),format.raw/*33.14*/("""
        """),_display_(Seq[Any](/*34.10*/if(play.api.Play.current.plugin[services.StagingAreaPlugin].isDefined && Permission.checkPermission(Permission.EditStagingArea, ResourceRef(ResourceRef.space, space.id)))/*34.180*/ {_display_(Seq[Any](format.raw/*34.182*/("""
<!--             <li><a class = "btn btn-xs btn-link btn-margins" href=""""),_display_(Seq[Any](/*35.74*/routes/*35.80*/.Spaces.stagingArea(space.id))),format.raw/*35.109*/("""" title="Staging Area"><span class="glyphicon glyphicon-book"></span> Staging Area</a></li>
 -->        """)))})),format.raw/*36.14*/("""
    </ul>
</div>
"""))}
    }
    
    def render(space:ProjectSpace,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space)(user)
    
    def f:((ProjectSpace) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space) => (user) => apply(space)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/otherActions.scala.html
                    HASH: a84c9f6cbd31ffbe83800235486a1ac091829193
                    MATRIX: 627->1|831->58|858->113|981->201|1084->295|1124->297|1191->329|1205->335|1255->364|1479->552|1494->558|1545->587|1714->720|1760->743|1854->801|1869->807|1929->845|2176->1056|2191->1062|2247->1096|2446->1278|2459->1283|2498->1284|2887->1637|2933->1660|3413->2108|3459->2118|3639->2288|3680->2290|3790->2364|3805->2370|3857->2399|3994->2504
                    LINES: 20->1|26->1|27->4|29->6|29->6|29->6|30->7|30->7|30->7|33->10|33->10|33->10|34->11|34->11|36->13|36->13|36->13|39->16|39->16|39->16|43->20|43->20|43->20|48->25|48->25|56->33|57->34|57->34|57->34|58->35|58->35|58->35|59->36
                    -- GENERATED --
                */
            