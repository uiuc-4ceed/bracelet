
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listItem extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[models.ProjectSpace,Call,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: models.ProjectSpace, redirect: Call)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.i18n.Messages


Seq[Any](format.raw/*1.82*/("""
"""),format.raw/*4.1*/("""<div class="panel panel-default space-panel" id=""""),_display_(Seq[Any](/*4.51*/space/*4.56*/.id)),format.raw/*4.59*/("""-listitem">
    """),_display_(Seq[Any](/*5.6*/if(!space.logoURL.isEmpty)/*5.32*/ {_display_(Seq[Any](format.raw/*5.34*/("""
        <div class="row">
            <div class="pull-left col-xs-12">
                <span class="glyphicon glyphicon-hdd"></span>
            </div>
        </div>

    """)))})),format.raw/*12.6*/("""
    <div class="panel-body">
        <div class = "row">
            <div class="col-xs-2">
                """),_display_(Seq[Any](/*16.18*/if(!space.logoURL.isEmpty)/*16.44*/ {_display_(Seq[Any](format.raw/*16.46*/("""
                    <a href=""""),_display_(Seq[Any](/*17.31*/routes/*17.37*/.Spaces.getSpace(space.id))),format.raw/*17.63*/(""""> <img class="fit-in-space" src=""""),_display_(Seq[Any](/*17.98*/(space.logoURL))),format.raw/*17.113*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*17.134*/(space.name))),format.raw/*17.146*/(""""></a>
                """)))}/*18.19*/else/*18.24*/{_display_(Seq[Any](format.raw/*18.25*/("""
                    <a href=""""),_display_(Seq[Any](/*19.31*/routes/*19.37*/.Spaces.getSpace(space.id))),format.raw/*19.63*/("""">
                        <span class="bigicon glyphicon glyphicon-hdd"></span>
                    </a>
                """)))})),format.raw/*22.18*/("""
            </div>
            <div class="col-xs-10 ">
                <div class="row">
                    <div class="col-xs-12">
                        <h3><a href=""""),_display_(Seq[Any](/*27.39*/routes/*27.45*/.Spaces.getSpace(space.id))),format.raw/*27.71*/("""">"""),_display_(Seq[Any](/*27.74*/space/*27.79*/.name)),format.raw/*27.84*/("""</a></h3>
                    </div>
                </div>

                <div class= "row">
                    <div class="col-md-6 col-sm-6 col-lg-6">
                        <div class = 'abstractsummary'>"""),_display_(Seq[Any](/*33.57*/Html(space.description.replace("\n","<br>")))),format.raw/*33.101*/("""</div>
                        <div>"""),_display_(Seq[Any](/*34.31*/space/*34.36*/.created.format("MMM dd, yyyy"))),format.raw/*34.67*/("""</div>
                        <div>
                            <span class="glyphicon glyphicon-briefcase" title=""""),_display_(Seq[Any](/*36.81*/space/*36.86*/.datasetCount)),format.raw/*36.99*/(""" datasets"></span> """),_display_(Seq[Any](/*36.119*/space/*36.124*/.datasetCount)),format.raw/*36.137*/("""
                            <span class="glyphicon glyphicon-th-large" title=""""),_display_(Seq[Any](/*37.80*/space/*37.85*/.collectionCount)),format.raw/*37.101*/(""" collections"></span> """),_display_(Seq[Any](/*37.124*/space/*37.129*/.collectionCount)),format.raw/*37.145*/("""
                            <span class="glyphicon glyphicon-user" title=""""),_display_(Seq[Any](/*38.76*/space/*38.81*/.userCount)),format.raw/*38.91*/(""" users"></span> """),_display_(Seq[Any](/*38.108*/space/*38.113*/.userCount)),format.raw/*38.123*/("""
                            """),_display_(Seq[Any](/*39.30*/if(user.isDefined)/*39.48*/ {_display_(Seq[Any](format.raw/*39.50*/("""

                                    <!-- If user can delete, the button is enabled, otherwise the button is present but disabled to provide consistent UE. -->
<!--                                     """),_display_(Seq[Any](/*42.43*/if(Permission.checkPermission(Permission.DeleteSpace, ResourceRef(ResourceRef.space, space.id)) || user.get.identityId.userId.equals(space.creator))/*42.191*/{_display_(Seq[Any](format.raw/*42.192*/("""
                                        <button onclick="confirmDeleteResource('space','"""),_display_(Seq[Any](/*43.90*/Messages("space.title"))),format.raw/*43.113*/("""','"""),_display_(Seq[Any](/*43.117*/(space.id))),format.raw/*43.127*/("""','"""),_display_(Seq[Any](/*43.131*/(space.name.replace("'","&#39;")))),format.raw/*43.164*/("""',false, '"""),_display_(Seq[Any](/*43.175*/redirect)),format.raw/*43.183*/("""')" class="btn btn-link" title="Delete the """),_display_(Seq[Any](/*43.227*/Messages("space.title"))),format.raw/*43.250*/(""" but not its contents">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                    """)))}/*46.39*/else/*46.44*/{_display_(Seq[Any](format.raw/*46.45*/("""
                                        <div class="inline" title="Not enough permission to delete the """),_display_(Seq[Any](/*47.105*/Messages("space.title"))),format.raw/*47.128*/("""">
                                            <button disabled class="btn btn-link"><span class="glyphicon glyphicon-trash"></span></button>
                                        </div>
                                    """)))})),format.raw/*50.38*/("""
 -->
                            """)))})),format.raw/*52.30*/("""
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3">
                        <ul class="list-unstyled">
                            <li><a href=""""),_display_(Seq[Any](/*57.43*/(routes.Collections.list("", "", 12, Some(space.id.stringify))))),format.raw/*57.106*/("""" class="btn btn-link">
                                <span class="glyphicon glyphicon-hand-right"></span>
                                Collections in """),_display_(Seq[Any](/*59.49*/Messages("space.title"))),format.raw/*59.72*/("""
                            </a></li>
                            <li><a href=""""),_display_(Seq[Any](/*61.43*/(routes.Datasets.list("", "", 12, Some(space.id.stringify))))),format.raw/*61.103*/("""" class="btn btn-link">
                                <span class="glyphicon glyphicon-hand-right"></span>
                                Datasets in """),_display_(Seq[Any](/*63.46*/Messages("space.title"))),format.raw/*63.69*/("""
                            </a></li>
                            <li>
                            """),_display_(Seq[Any](/*66.30*/if(user.isDefined)/*66.48*/ {_display_(Seq[Any](format.raw/*66.50*/("""
                                    <!-- Fix for CATS-163 An exception is thrown if user is not defined -->
                                """),_display_(Seq[Any](/*68.34*/if(user.isEmpty || !space.followers.contains(user.get.id))/*68.92*/ {_display_(Seq[Any](format.raw/*68.94*/("""
<!--                                     <button id="followButton" class="btn btn-link" objectId=""""),_display_(Seq[Any](/*69.100*/space/*69.105*/.id)),format.raw/*69.108*/("""" objectName=""""),_display_(Seq[Any](/*69.123*/space/*69.128*/.name)),format.raw/*69.133*/("""" objectType="space">
                                        <span class='glyphicon glyphicon-star'></span>
                                        Follow
                                    </button>
 -->                                """)))}/*73.39*/else/*73.44*/{_display_(Seq[Any](format.raw/*73.45*/("""
<!--                                     <button id="followButton" class="btn btn-link" objectId=""""),_display_(Seq[Any](/*74.100*/space/*74.105*/.id)),format.raw/*74.108*/("""" objectName=""""),_display_(Seq[Any](/*74.123*/space/*74.128*/.name)),format.raw/*74.133*/("""" objectType="space">
                                        <span class='glyphicon glyphicon-star-empty'></span>
                                        Unfollow
                                    </button>
 -->                                """)))})),format.raw/*78.38*/("""

                                <script src=""""),_display_(Seq[Any](/*80.47*/routes/*80.53*/.Assets.at("javascripts/recommendation.js"))),format.raw/*80.96*/("""" type="text/javascript"></script>
                                <script type="text/javascript" language="javascript">
                                                            $(document).ready(function() """),format.raw/*82.90*/("""{"""),format.raw/*82.91*/("""
                                                                $(document).on('click', '.followButton', function() """),format.raw/*83.117*/("""{"""),format.raw/*83.118*/("""
                                                                    var id = $(this).attr('objectId');
                                                                    var name = $(this).attr('objectName');
                                                                    var type = $(this).attr('objectType');
                                                                    followHandler.call(this, jsRoutes, id, name, type, undefined, undefined);
                                                                """),format.raw/*88.65*/("""}"""),format.raw/*88.66*/(""");
                                                            """),format.raw/*89.61*/("""}"""),format.raw/*89.62*/(""");
                                                        </script>
                            """)))})),format.raw/*91.30*/("""
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-6 col-lg-3">
<!--                         <ul class="list-unstyled">
                            """),_display_(Seq[Any](/*97.30*/if(Permission.checkPermission(Permission.EditSpace, ResourceRef(ResourceRef.space, space.id)))/*97.124*/ {_display_(Seq[Any](format.raw/*97.126*/("""
                                <li><a href=""""),_display_(Seq[Any](/*98.47*/routes/*98.53*/.Spaces.manageUsers(space.id))),format.raw/*98.82*/("""" class="btn btn-link">
                                    <span class = "glyphicon glyphicon-user" aria-hidden="true"></span> Manage Users
                                </a></li>
                                <li><a href=""""),_display_(Seq[Any](/*101.47*/routes/*101.53*/.Spaces.updateSpace(space.id))),format.raw/*101.82*/("""" class="btn btn-link">
                                    <span class = "glyphicon glyphicon-edit" aria-hidden="true"></span> Edit """),_display_(Seq[Any](/*102.111*/Messages("space.title"))),format.raw/*102.134*/("""
                                </a></li>
                                <li><a href=""""),_display_(Seq[Any](/*104.47*/routes/*104.53*/.Spaces.selectExtractors(space.id))),format.raw/*104.87*/("""" class="btn btn-link">
                                    <span class = "glyphicon glyphicon-edit" aria-hidden="true"></span> Extractors
                                </a></li>
                            """)))})),format.raw/*107.30*/("""
                            """),_display_(Seq[Any](/*108.30*/if(play.api.Play.current.plugin[services.StagingAreaPlugin].isDefined && Permission.checkPermission(Permission.EditStagingArea, ResourceRef(ResourceRef.space, space.id)))/*108.200*/ {_display_(Seq[Any](format.raw/*108.202*/("""
                                <li><a class = "btn btn-link" href=""""),_display_(Seq[Any](/*109.70*/routes/*109.76*/.Spaces.stagingArea(space.id))),format.raw/*109.105*/("""" title="Staging Area"><span class="glyphicon glyphicon-briefcase"></span> Staging Area</a></li>
                            """)))})),format.raw/*110.30*/("""
                        </ul>
 -->
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>
"""))}
    }
    
    def render(space:models.ProjectSpace,redirect:Call,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space,redirect)(user)
    
    def f:((models.ProjectSpace,Call) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space,redirect) => (user) => apply(space,redirect)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/listItem.scala.html
                    HASH: 5e0036163638310780c06c4adf60767ead1b532d
                    MATRIX: 635->1|862->81|889->136|974->186|987->191|1011->194|1062->211|1096->237|1135->239|1341->414|1487->524|1522->550|1562->552|1629->583|1644->589|1692->615|1763->650|1801->665|1859->686|1894->698|1937->723|1950->728|1989->729|2056->760|2071->766|2119->792|2274->915|2483->1088|2498->1094|2546->1120|2585->1123|2599->1128|2626->1133|2875->1346|2942->1390|3015->1427|3029->1432|3082->1463|3235->1580|3249->1585|3284->1598|3341->1618|3356->1623|3392->1636|3508->1716|3522->1721|3561->1737|3621->1760|3636->1765|3675->1781|3787->1857|3801->1862|3833->1872|3887->1889|3902->1894|3935->1904|4001->1934|4028->1952|4068->1954|4307->2157|4465->2305|4505->2306|4631->2396|4677->2419|4718->2423|4751->2433|4792->2437|4848->2470|4896->2481|4927->2489|5008->2533|5054->2556|5276->2760|5289->2765|5328->2766|5470->2871|5516->2894|5774->3120|5841->3155|6090->3368|6176->3431|6369->3588|6414->3611|6531->3692|6614->3752|6804->3906|6849->3929|6986->4030|7013->4048|7053->4050|7231->4192|7298->4250|7338->4252|7475->4352|7490->4357|7516->4360|7568->4375|7583->4380|7611->4385|7869->4625|7882->4630|7921->4631|8058->4731|8073->4736|8099->4739|8151->4754|8166->4759|8194->4764|8473->5011|8557->5059|8572->5065|8637->5108|8875->5318|8904->5319|9050->5436|9080->5437|9632->5961|9661->5962|9752->6025|9781->6026|9911->6124|10185->6362|10289->6456|10330->6458|10413->6505|10428->6511|10479->6540|10745->6769|10761->6775|10813->6804|10985->6938|11032->6961|11158->7050|11174->7056|11231->7090|11474->7300|11541->7330|11722->7500|11764->7502|11871->7572|11887->7578|11940->7607|12099->7733
                    LINES: 20->1|26->1|27->4|27->4|27->4|27->4|28->5|28->5|28->5|35->12|39->16|39->16|39->16|40->17|40->17|40->17|40->17|40->17|40->17|40->17|41->18|41->18|41->18|42->19|42->19|42->19|45->22|50->27|50->27|50->27|50->27|50->27|50->27|56->33|56->33|57->34|57->34|57->34|59->36|59->36|59->36|59->36|59->36|59->36|60->37|60->37|60->37|60->37|60->37|60->37|61->38|61->38|61->38|61->38|61->38|61->38|62->39|62->39|62->39|65->42|65->42|65->42|66->43|66->43|66->43|66->43|66->43|66->43|66->43|66->43|66->43|66->43|69->46|69->46|69->46|70->47|70->47|73->50|75->52|80->57|80->57|82->59|82->59|84->61|84->61|86->63|86->63|89->66|89->66|89->66|91->68|91->68|91->68|92->69|92->69|92->69|92->69|92->69|92->69|96->73|96->73|96->73|97->74|97->74|97->74|97->74|97->74|97->74|101->78|103->80|103->80|103->80|105->82|105->82|106->83|106->83|111->88|111->88|112->89|112->89|114->91|120->97|120->97|120->97|121->98|121->98|121->98|124->101|124->101|124->101|125->102|125->102|127->104|127->104|127->104|130->107|131->108|131->108|131->108|132->109|132->109|132->109|133->110
                    -- GENERATED --
                */
            