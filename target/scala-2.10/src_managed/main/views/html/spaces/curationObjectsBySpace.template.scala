
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object curationObjectsBySpace extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[List[models.CurationObject],ProjectSpace,Option[Boolean],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObjects: List[models.CurationObject], space: ProjectSpace, isPublic: Option[Boolean])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import api.Permission


Seq[Any](format.raw/*1.132*/("""
"""),format.raw/*4.1*/("""<h3>Published Datasets</h3>
"""),_display_(Seq[Any](/*5.2*/if(curationObjects.size == 0)/*5.31*/ {_display_(Seq[Any](format.raw/*5.33*/("""
    <p> There are no published datasets associated with this """),_display_(Seq[Any](/*6.63*/Messages("space.title"))),format.raw/*6.86*/(""".</p>
""")))}/*7.3*/else/*7.8*/{_display_(Seq[Any](format.raw/*7.9*/("""
    """),_display_(Seq[Any](/*8.6*/spaces/*8.12*/.curationObjectsGrid(curationObjects, space, isPublic))),format.raw/*8.66*/("""
""")))})))}
    }
    
    def render(curationObjects:List[models.CurationObject],space:ProjectSpace,isPublic:Option[Boolean],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObjects,space,isPublic)(user)
    
    def f:((List[models.CurationObject],ProjectSpace,Option[Boolean]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObjects,space,isPublic) => (user) => apply(curationObjects,space,isPublic)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/curationObjectsBySpace.scala.html
                    HASH: 1deecfa0d7f39b4fea819c14751562db84a0ffdc
                    MATRIX: 681->1|959->131|986->186|1049->215|1086->244|1125->246|1223->309|1267->332|1291->340|1302->345|1339->346|1379->352|1393->358|1468->412
                    LINES: 20->1|26->1|27->4|28->5|28->5|28->5|29->6|29->6|30->7|30->7|30->7|31->8|31->8|31->8
                    -- GENERATED --
                */
            