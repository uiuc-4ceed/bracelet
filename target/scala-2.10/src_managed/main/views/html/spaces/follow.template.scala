
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object follow extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[ProjectSpace,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: ProjectSpace)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.59*/("""
"""),_display_(Seq[Any](/*2.2*/if(user.isDefined)/*2.20*/ {_display_(Seq[Any](format.raw/*2.22*/("""
    <!-- Fix for CATS-163 An exception is thrown if user is not defined -->
    """),_display_(Seq[Any](/*4.6*/if(user.isEmpty || !space.followers.contains(user.get.id))/*4.64*/ {_display_(Seq[Any](format.raw/*4.66*/("""
        <button id="followButton" class="btn btn-link followButton btn-xs btn-margins" objectId=""""),_display_(Seq[Any](/*5.99*/space/*5.104*/.id)),format.raw/*5.107*/("""" objectName=""""),_display_(Seq[Any](/*5.122*/space/*5.127*/.name)),format.raw/*5.132*/("""" objectType="space">
            <span class="glyphicon glyphicon-star"></span> Follow
        </button>
    """)))}/*8.7*/else/*8.12*/{_display_(Seq[Any](format.raw/*8.13*/("""
        <button id="followButton" class="btn btn-link followButton btn-xs btn-margins" objectId=""""),_display_(Seq[Any](/*9.99*/space/*9.104*/.id)),format.raw/*9.107*/("""" objectName=""""),_display_(Seq[Any](/*9.122*/space/*9.127*/.name)),format.raw/*9.132*/("""" objectType="space">
            <span class="glyphicon glyphicon-star-empty"></span> Unfollow
        </button>
    """)))})),format.raw/*12.6*/("""

    <script src=""""),_display_(Seq[Any](/*14.19*/routes/*14.25*/.Assets.at("javascripts/recommendation.js"))),format.raw/*14.68*/("""" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
    $(document).ready(function() """),format.raw/*16.34*/("""{"""),format.raw/*16.35*/("""
        $(document).on('click', '.followButton', function() """),format.raw/*17.61*/("""{"""),format.raw/*17.62*/("""
            var id = $(this).attr('objectId');
            var name = $(this).attr('objectName');
            var type = $(this).attr('objectType');
            followHandler.call(this, jsRoutes, id, name, type, undefined, undefined);
        """),format.raw/*22.9*/("""}"""),format.raw/*22.10*/(""");
    """),format.raw/*23.5*/("""}"""),format.raw/*23.6*/(""");
    </script>
""")))})))}
    }
    
    def render(space:ProjectSpace,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space)(user)
    
    def f:((ProjectSpace) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space) => (user) => apply(space)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/follow.scala.html
                    HASH: d3cd9deb23ac62f8465940c4da1a60b5c5f4ace2
                    MATRIX: 621->1|772->58|808->60|834->78|873->80|989->162|1055->220|1094->222|1228->321|1242->326|1267->329|1318->344|1332->349|1359->354|1487->466|1499->471|1537->472|1671->571|1685->576|1710->579|1761->594|1775->599|1802->604|1952->723|2008->743|2023->749|2088->792|2242->918|2271->919|2360->980|2389->981|2660->1225|2689->1226|2723->1233|2751->1234
                    LINES: 20->1|23->1|24->2|24->2|24->2|26->4|26->4|26->4|27->5|27->5|27->5|27->5|27->5|27->5|30->8|30->8|30->8|31->9|31->9|31->9|31->9|31->9|31->9|34->12|36->14|36->14|36->14|38->16|38->16|39->17|39->17|44->22|44->22|45->23|45->23
                    -- GENERATED --
                */
            