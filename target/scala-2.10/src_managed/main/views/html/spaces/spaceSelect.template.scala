
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object spaceSelect extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[UUID,Symbol,String,String,String,String,String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(resourceId: UUID, resourceType: Symbol, selectId: String, onClick:String, buttonText: String, buttonTitle: String, divId: String):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages


Seq[Any](format.raw/*1.132*/("""
"""),format.raw/*3.1*/("""<div class="row bottom-padding" id=""""),_display_(Seq[Any](/*3.38*/divId)),format.raw/*3.43*/("""">
    <div class="col-xs-12">
        <div class="form-inline">
            <div class="input-group input-group-sm col-md-8">

                <select id=""""),_display_(Seq[Any](/*8.30*/selectId)),format.raw/*8.38*/("""" class="form-control add-resource">
                </select>

                <span class="input-group-btn">
                    <a href="#" class="btn btn-default btn-large" id="addSpaceBtn" title=""""),_display_(Seq[Any](/*12.92*/buttonTitle)),format.raw/*12.103*/("""" onclick=""""),_display_(Seq[Any](/*12.115*/onClick)),format.raw/*12.122*/("""">
                        <span class="glyphicon glyphicon-plus"></span> """),_display_(Seq[Any](/*13.73*/buttonText)),format.raw/*13.83*/("""
                    </a>
                </span>

            </div>
        </div>
    </div>
</div>


<script language="javascript">
        $("#"""),_display_(Seq[Any](/*24.14*/selectId)),format.raw/*24.22*/("""").select2("""),format.raw/*24.33*/("""{"""),format.raw/*24.34*/("""
            theme: "bootstrap",
            placeholder: "Select a """),_display_(Seq[Any](/*26.37*/Messages("space.title"))),format.raw/*26.60*/("""",
            allowClear: true,
            ajax: """),format.raw/*28.19*/("""{"""),format.raw/*28.20*/("""
                url: function(params) """),format.raw/*29.39*/("""{"""),format.raw/*29.40*/("""
                    return jsRoutes.api.Spaces.listCanEdit(params.term, null, 5).url;
                """),format.raw/*31.17*/("""}"""),format.raw/*31.18*/(""",
                data: function(params) """),format.raw/*32.40*/("""{"""),format.raw/*32.41*/("""
                    return """),format.raw/*33.28*/("""{"""),format.raw/*33.29*/(""" title: params.term """),format.raw/*33.49*/("""}"""),format.raw/*33.50*/(""";
                """),format.raw/*34.17*/("""}"""),format.raw/*34.18*/(""",
                processResults: function(data, page) """),format.raw/*35.54*/("""{"""),format.raw/*35.55*/("""
                    return """),format.raw/*36.28*/("""{"""),format.raw/*36.29*/("""results: data.filter(function(x) """),format.raw/*36.62*/("""{"""),format.raw/*36.63*/("""
                        var ids = $('.space').map(function() """),format.raw/*37.62*/("""{"""),format.raw/*37.63*/("""
                            return $(this).attr('id');
                        """),format.raw/*39.25*/("""}"""),format.raw/*39.26*/(""");
                        return $.inArray(x["id"], ids) == -1;
                    """),format.raw/*41.21*/("""}"""),format.raw/*41.22*/(""").map(function(x) """),format.raw/*41.40*/("""{"""),format.raw/*41.41*/("""
                        return """),format.raw/*42.32*/("""{"""),format.raw/*42.33*/("""
                            text: x["name"],
                            id: x["id"]
                        """),format.raw/*45.25*/("""}"""),format.raw/*45.26*/("""
                    """),format.raw/*46.21*/("""}"""),format.raw/*46.22*/(""")"""),format.raw/*46.23*/("""}"""),format.raw/*46.24*/(""";
                """),format.raw/*47.17*/("""}"""),format.raw/*47.18*/("""
            """),format.raw/*48.13*/("""}"""),format.raw/*48.14*/("""
        """),format.raw/*49.9*/("""}"""),format.raw/*49.10*/(""");

</script>"""))}
    }
    
    def render(resourceId:UUID,resourceType:Symbol,selectId:String,onClick:String,buttonText:String,buttonTitle:String,divId:String): play.api.templates.HtmlFormat.Appendable = apply(resourceId,resourceType,selectId,onClick,buttonText,buttonTitle,divId)
    
    def f:((UUID,Symbol,String,String,String,String,String) => play.api.templates.HtmlFormat.Appendable) = (resourceId,resourceType,selectId,onClick,buttonText,buttonTitle,divId) => apply(resourceId,resourceType,selectId,onClick,buttonText,buttonTitle,divId)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/spaceSelect.scala.html
                    HASH: 40d3a07c7bda42722eec3efdd87c04651e7035a7
                    MATRIX: 640->1|895->131|922->163|994->200|1020->205|1212->362|1241->370|1479->572|1513->583|1562->595|1592->602|1703->677|1735->687|1920->836|1950->844|1989->855|2018->856|2123->925|2168->948|2247->999|2276->1000|2343->1039|2372->1040|2503->1143|2532->1144|2601->1185|2630->1186|2686->1214|2715->1215|2763->1235|2792->1236|2838->1254|2867->1255|2950->1310|2979->1311|3035->1339|3064->1340|3125->1373|3154->1374|3244->1436|3273->1437|3381->1517|3410->1518|3523->1603|3552->1604|3598->1622|3627->1623|3687->1655|3716->1656|3854->1766|3883->1767|3932->1788|3961->1789|3990->1790|4019->1791|4065->1809|4094->1810|4135->1823|4164->1824|4200->1833|4229->1834
                    LINES: 20->1|24->1|25->3|25->3|25->3|30->8|30->8|34->12|34->12|34->12|34->12|35->13|35->13|46->24|46->24|46->24|46->24|48->26|48->26|50->28|50->28|51->29|51->29|53->31|53->31|54->32|54->32|55->33|55->33|55->33|55->33|56->34|56->34|57->35|57->35|58->36|58->36|58->36|58->36|59->37|59->37|61->39|61->39|63->41|63->41|63->41|63->41|64->42|64->42|67->45|67->45|68->46|68->46|68->46|68->46|69->47|69->47|70->48|70->48|71->49|71->49
                    -- GENERATED --
                */
            