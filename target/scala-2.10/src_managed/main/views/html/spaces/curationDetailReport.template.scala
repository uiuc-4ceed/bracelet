
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object curationDetailReport extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[models.CurationObject,models.MatchMakerResponse,String,Map[String, List[String]],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObject: models.CurationObject, mmResponse: models.MatchMakerResponse, repository: String, userPreferences:  Map[String,List[String]])(implicit user: Option[models.User] ):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.181*/("""
"""),_display_(Seq[Any](/*2.2*/main("Submit to Repository")/*2.30*/ {_display_(Seq[Any](format.raw/*2.32*/("""
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a  href =""""),_display_(Seq[Any](/*7.33*/routes/*7.39*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*7.92*/(""""> Edit Metadata</a> >
                    <a  href=""""),_display_(Seq[Any](/*8.32*/routes/*8.38*/.CurationObjects.findMatchingRepositories(curationObject.id))),format.raw/*8.98*/("""">Select Repository</a> >
                    <u><strong>Submit to Repository</strong></u>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <a class="btn btn-default" href=""""),_display_(Seq[Any](/*16.47*/routes/*16.53*/.CurationObjects.findMatchingRepositories(curationObject.id))),format.raw/*16.113*/(""""><span class="glyphicon glyphicon-chevron-left"></span> Select Repository</a>
        </div>
        <div class="col-md-8 text-center">
            <h1>Submit to Repository</h1>
        </div>
        <div class="col-md-2">
            <a class="btn btn-primary pull-right" href=""""),_display_(Seq[Any](/*22.58*/routes/*22.64*/.CurationObjects.sendToRepository(curationObject.id))),format.raw/*22.116*/(""""><span class="glyphicon glyphicon-saved"></span> Submit</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            """),_display_(Seq[Any](/*27.14*/spaces/*27.20*/.curationSummary(curationObject, false))),format.raw/*27.59*/("""
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>Preferences</h4>
                    """),_display_(Seq[Any](/*31.22*/for((prop, value) <- userPreferences) yield /*31.59*/ {_display_(Seq[Any](format.raw/*31.61*/("""
                        <h5>"""),_display_(Seq[Any](/*32.30*/prop)),format.raw/*32.34*/("""</h5>
                        """),_display_(Seq[Any](/*33.26*/for(cur<-value) yield /*33.41*/ {_display_(Seq[Any](format.raw/*33.43*/("""
                            """),_display_(Seq[Any](/*34.30*/if((userPreferences.contains(prop.replace(" ", "_")) && userPreferences(prop.replace(" ", "_")).contains(cur)))/*34.141*/ {_display_(Seq[Any](format.raw/*34.143*/("""
                                """),_display_(Seq[Any](/*35.34*/cur)),format.raw/*35.37*/("""
                            """)))})),format.raw/*36.30*/("""
                        """)))})),format.raw/*37.26*/("""
                    """)))})),format.raw/*38.22*/("""
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <h3>"""),_display_(Seq[Any](/*43.18*/mmResponse/*43.28*/.repositoryName)),format.raw/*43.43*/("""</h3>
            <h4>Matchmaker Summary</h4>
            <div id="mmresults">
                <ul class="indent_20 list-unstyled">
                    """),_display_(Seq[Any](/*47.22*/for(rule <- mmResponse.per_rule_score) yield /*47.60*/ {_display_(Seq[Any](format.raw/*47.62*/("""

                        """),_display_(Seq[Any](/*49.26*/if(rule.Score < 0)/*49.44*/ {_display_(Seq[Any](format.raw/*49.46*/("""
                            <li> <span class="glyphicon glyphicon-remove red" ></span> """),_display_(Seq[Any](/*50.89*/rule/*50.93*/.rule_name)),format.raw/*50.103*/(""" <span class="red">"""),_display_(Seq[Any](/*50.123*/rule/*50.127*/.Message)),format.raw/*50.135*/("""</span>
                                """),_display_(Seq[Any](/*51.34*/if(rule.rule_name.toLowerCase().contains("metadata".toLowerCase()))/*51.101*/ {_display_(Seq[Any](format.raw/*51.103*/("""
                                    <a href=""""),_display_(Seq[Any](/*52.47*/routes/*52.53*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*52.106*/("""">
                                        Edit metadata</a> <br/>
                                """)))}/*54.35*/else/*54.40*/{_display_(Seq[Any](format.raw/*54.41*/(""" """),_display_(Seq[Any](/*54.43*/if(rule.rule_name.toLowerCase().contains("organization".toLowerCase()) || rule.rule_name.toLowerCase().contains("rights holder".toLowerCase()))/*54.186*/{_display_(Seq[Any](format.raw/*54.187*/("""

                                    <a href=""""),_display_(Seq[Any](/*56.47*/routes/*56.53*/.Profile.viewProfileUUID(curationObject.author.id))),format.raw/*56.103*/("""">
                                        Edit profile</a> <br/>
                                """)))}/*58.35*/else/*58.40*/{_display_(Seq[Any](format.raw/*58.41*/(""" """),_display_(Seq[Any](/*58.43*/if(rule.rule_name.toLowerCase().contains("data types".toLowerCase()))/*58.112*/{_display_(Seq[Any](format.raw/*58.113*/("""

                                    <a href=""""),_display_(Seq[Any](/*60.47*/routes/*60.53*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*60.106*/("""">
                                        Delete unacceptable files</a> <br/>
                                """)))}/*62.35*/else/*62.40*/{_display_(Seq[Any](format.raw/*62.41*/(""" """),_display_(Seq[Any](/*62.43*/if(rule.rule_name.toLowerCase().contains("collection depth".toLowerCase()))/*62.118*/{_display_(Seq[Any](format.raw/*62.119*/("""

                                    <a href=""""),_display_(Seq[Any](/*64.47*/routes/*64.53*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*64.106*/("""">
                                        Delete folders</a> <br/>
                                """)))}/*66.35*/else/*66.40*/{_display_(Seq[Any](format.raw/*66.41*/(""" """),_display_(Seq[Any](/*66.43*/if(rule.rule_name.toLowerCase().contains("size".toLowerCase()))/*66.106*/{_display_(Seq[Any](format.raw/*66.107*/("""

                                    <a href=""""),_display_(Seq[Any](/*68.47*/routes/*68.53*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*68.106*/("""">
                                        Delete files</a> <br/>
                                """)))})),format.raw/*70.34*/("""


                                """)))}))))}))))}))))})),format.raw/*73.37*/("""
                            </li>
                        """)))}/*75.27*/else/*75.32*/{_display_(Seq[Any](format.raw/*75.33*/("""
                            """),_display_(Seq[Any](/*76.30*/if(rule.Score == 0)/*76.49*/ {_display_(Seq[Any](format.raw/*76.51*/("""
                                <li><span class="glyphicon glyphicon-ok yellow"> </span> """),_display_(Seq[Any](/*77.91*/rule/*77.95*/.rule_name)),format.raw/*77.105*/(""" <span class="yellow">This info is not required </span></li>
                            """)))}/*78.31*/else/*78.36*/{_display_(Seq[Any](format.raw/*78.37*/("""
                                <li><span class="glyphicon glyphicon-ok green"> </span> """),_display_(Seq[Any](/*79.90*/rule/*79.94*/.rule_name)),format.raw/*79.104*/(""" <span class="green">All Requirements are satisfied.</span></li>
                            """)))})),format.raw/*80.30*/("""
                        """)))})),format.raw/*81.26*/("""
                    """)))})),format.raw/*82.22*/("""
                </ul>
            </div>
        </div>
    </div>
    <div class="row bottom-padding">
        <div class="col-md-12">
            <a class="btn btn-default" href=""""),_display_(Seq[Any](/*89.47*/routes/*89.53*/.CurationObjects.findMatchingRepositories( curationObject.id))),format.raw/*89.114*/(""""><span class="glyphicon glyphicon-chevron-left"></span> Select Repository</a>
            <a class = "btn btn-primary pull-right" href =""""),_display_(Seq[Any](/*90.61*/routes/*90.67*/.CurationObjects.sendToRepository(curationObject.id))),format.raw/*90.119*/(""""><span class="glyphicon glyphicon-saved"></span> Submit</a>
        </div>
    </div>


""")))})))}
    }
    
    def render(curationObject:models.CurationObject,mmResponse:models.MatchMakerResponse,repository:String,userPreferences:Map[String, List[String]],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObject,mmResponse,repository,userPreferences)(user)
    
    def f:((models.CurationObject,models.MatchMakerResponse,String,Map[String, List[String]]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObject,mmResponse,repository,userPreferences) => (user) => apply(curationObject,mmResponse,repository,userPreferences)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/curationDetailReport.scala.html
                    HASH: 30fca8636043430de66cbecdaf309d71232fff7d
                    MATRIX: 703->1|977->180|1013->182|1049->210|1088->212|1297->386|1311->392|1385->445|1474->499|1488->505|1569->565|1863->823|1878->829|1961->889|2279->1171|2294->1177|2369->1229|2558->1382|2573->1388|2634->1427|2820->1577|2873->1614|2913->1616|2979->1646|3005->1650|3072->1681|3103->1696|3143->1698|3209->1728|3330->1839|3371->1841|3441->1875|3466->1878|3528->1908|3586->1934|3640->1956|3782->2062|3801->2072|3838->2087|4027->2240|4081->2278|4121->2280|4184->2307|4211->2325|4251->2327|4376->2416|4389->2420|4422->2430|4479->2450|4493->2454|4524->2462|4601->2503|4678->2570|4719->2572|4802->2619|4817->2625|4893->2678|5012->2779|5025->2784|5064->2785|5102->2787|5255->2930|5295->2931|5379->2979|5394->2985|5467->3035|5585->3135|5598->3140|5637->3141|5675->3143|5754->3212|5794->3213|5878->3261|5893->3267|5969->3320|6100->3433|6113->3438|6152->3439|6190->3441|6275->3516|6315->3517|6399->3565|6414->3571|6490->3624|6610->3726|6623->3731|6662->3732|6700->3734|6773->3797|6813->3798|6897->3846|6912->3852|6988->3905|7119->4004|7202->4043|7281->4104|7294->4109|7333->4110|7399->4140|7427->4159|7467->4161|7594->4252|7607->4256|7640->4266|7749->4357|7762->4362|7801->4363|7927->4453|7940->4457|7973->4467|8099->4561|8157->4587|8211->4609|8430->4792|8445->4798|8529->4859|8704->4998|8719->5004|8794->5056
                    LINES: 20->1|23->1|24->2|24->2|24->2|29->7|29->7|29->7|30->8|30->8|30->8|38->16|38->16|38->16|44->22|44->22|44->22|49->27|49->27|49->27|53->31|53->31|53->31|54->32|54->32|55->33|55->33|55->33|56->34|56->34|56->34|57->35|57->35|58->36|59->37|60->38|65->43|65->43|65->43|69->47|69->47|69->47|71->49|71->49|71->49|72->50|72->50|72->50|72->50|72->50|72->50|73->51|73->51|73->51|74->52|74->52|74->52|76->54|76->54|76->54|76->54|76->54|76->54|78->56|78->56|78->56|80->58|80->58|80->58|80->58|80->58|80->58|82->60|82->60|82->60|84->62|84->62|84->62|84->62|84->62|84->62|86->64|86->64|86->64|88->66|88->66|88->66|88->66|88->66|88->66|90->68|90->68|90->68|92->70|95->73|97->75|97->75|97->75|98->76|98->76|98->76|99->77|99->77|99->77|100->78|100->78|100->78|101->79|101->79|101->79|102->80|103->81|104->82|111->89|111->89|111->89|112->90|112->90|112->90
                    -- GENERATED --
                */
            