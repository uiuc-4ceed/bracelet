
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object curationObject extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[models.CurationObject,List[models.Metadata],Boolean,Int,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObject: models.CurationObject,m: List[models.Metadata], rdfExported: Boolean, limit: Int)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import helper._

import collection.JavaConverters._

import play.api.Play.current

implicit def /*6.2*/implicitFieldConstructor/*6.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.136*/("""
"""),format.raw/*6.75*/("""

"""),_display_(Seq[Any](/*8.2*/main(curationObject.name)/*8.27*/ {_display_(Seq[Any](format.raw/*8.29*/("""
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <u><strong>Edit Metadata</strong></u> >
                    <a  href=""""),_display_(Seq[Any](/*15.32*/routes/*15.38*/.CurationObjects.findMatchingRepositories(curationObject.id))),format.raw/*15.98*/("""">Select Repository</a> >
                    Submit to Repository
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8 text-center">
            <h1>Edit Metadata</h1>
        </div>
        <div class="col-md-2">
            <a class="btn btn-primary pull-right" href=""""),_display_(Seq[Any](/*28.58*/routes/*28.64*/.CurationObjects.findMatchingRepositories(curationObject.id))),format.raw/*28.124*/(""""> Select Repository <span class="glyphicon glyphicon-chevron-right"></span></a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            """),_display_(Seq[Any](/*33.14*/spaces/*33.20*/.curationSummary( curationObject,  true))),format.raw/*33.60*/("""
        </div>
        <div class="col-md-9">
        <div>
            <div>Note: The links in this page redirect to the live objects.</div>
            """),_display_(Seq[Any](/*38.14*/if(Permission.checkPermission(Permission.AddMetadata, ResourceRef(ResourceRef.curationObject, curationObject.id)))/*38.128*/ {_display_(Seq[Any](format.raw/*38.130*/("""
                 <div class="row">
                    <div class="col-md-12">
                    """),_display_(Seq[Any](/*41.22*/metadatald/*41.32*/.addMetadata("curationObject", curationObject.id.toString, "metadata-content-ds"))),format.raw/*41.113*/("""
                    </div>
                 </div>
            """)))})),format.raw/*44.14*/("""
            <div class="row">
                <div class="col-md-12" id="metadata-content-ds">
                """),_display_(Seq[Any](/*47.18*/metadatald/*47.28*/.view(m.filter(_.attachedTo.resourceType == ResourceRef.curationObject), true))),format.raw/*47.106*/("""
                </div>
            </div>

            <div id="files"></div>

        </div>



        <script src=""""),_display_(Seq[Any](/*57.23*/routes/*57.29*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*57.69*/("""" type="text/javascript"></script>
        <script src=""""),_display_(Seq[Any](/*58.23*/routes/*58.29*/.Assets.at("javascripts/people.js"))),format.raw/*58.64*/("""" type="text/javascript"></script>
        <script type="text/javascript">
            var removeIndicator = false;
            var parentId = """"),_display_(Seq[Any](/*61.30*/curationObject/*61.44*/.id)),format.raw/*61.47*/("""";
            var parentType = "dataset";
            var pageIndex = 0;
            var folderId;

            $(document).ready(function()"""),format.raw/*66.41*/("""{"""),format.raw/*66.42*/("""
                getUpdatedFilesAndFolders('"""),_display_(Seq[Any](/*67.45*/curationObject/*67.59*/.id)),format.raw/*67.62*/("""', """),_display_(Seq[Any](/*67.66*/limit)),format.raw/*67.71*/(""");
            """),format.raw/*68.13*/("""}"""),format.raw/*68.14*/(""");
            $(window).on('fileDelete hashchange', function() """),format.raw/*69.62*/("""{"""),format.raw/*69.63*/("""
                getUpdatedFilesAndFolders('"""),_display_(Seq[Any](/*70.45*/curationObject/*70.59*/.id)),format.raw/*70.62*/("""', """),_display_(Seq[Any](/*70.66*/limit)),format.raw/*70.71*/(""");
            """),format.raw/*71.13*/("""}"""),format.raw/*71.14*/(""");
                </script>

        </div>
        </div>
    <div class="row bottom-padding">
        <div class="col-md-12">
            <a class="btn btn-primary pull-right" href=""""),_display_(Seq[Any](/*78.58*/routes/*78.64*/.CurationObjects.findMatchingRepositories(curationObject.id))),format.raw/*78.124*/("""">Select Repository <span class="glyphicon glyphicon-chevron-right"></span></a>
        </div>
    </div>


""")))})))}
    }
    
    def render(curationObject:models.CurationObject,m:List[models.Metadata],rdfExported:Boolean,limit:Int,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObject,m,rdfExported,limit)(user)
    
    def f:((models.CurationObject,List[models.Metadata],Boolean,Int) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObject,m,rdfExported,limit) => (user) => apply(curationObject,m,rdfExported,limit)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/curationObject.scala.html
                    HASH: 9b834dc178674c115b10cd8cff85620fa90a6ba9
                    MATRIX: 672->1|997->243|1029->267|1109->135|1137->316|1174->319|1207->344|1246->346|1516->580|1531->586|1613->646|2033->1030|2048->1036|2131->1096|2340->1269|2355->1275|2417->1315|2609->1471|2733->1585|2774->1587|2911->1688|2930->1698|3034->1779|3131->1844|3280->1957|3299->1967|3400->2045|3556->2165|3571->2171|3633->2211|3726->2268|3741->2274|3798->2309|3979->2454|4002->2468|4027->2471|4196->2612|4225->2613|4306->2658|4329->2672|4354->2675|4394->2679|4421->2684|4464->2699|4493->2700|4585->2764|4614->2765|4695->2810|4718->2824|4743->2827|4783->2831|4810->2836|4853->2851|4882->2852|5104->3038|5119->3044|5202->3104
                    LINES: 20->1|29->6|29->6|30->1|31->6|33->8|33->8|33->8|40->15|40->15|40->15|53->28|53->28|53->28|58->33|58->33|58->33|63->38|63->38|63->38|66->41|66->41|66->41|69->44|72->47|72->47|72->47|82->57|82->57|82->57|83->58|83->58|83->58|86->61|86->61|86->61|91->66|91->66|92->67|92->67|92->67|92->67|92->67|93->68|93->68|94->69|94->69|95->70|95->70|95->70|95->70|95->70|96->71|96->71|103->78|103->78|103->78
                    -- GENERATED --
                */
            