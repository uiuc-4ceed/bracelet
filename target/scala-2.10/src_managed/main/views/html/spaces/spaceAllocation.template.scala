
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object spaceAllocation extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[UUID,Symbol,Map[ProjectSpace, Boolean],Option[Collection],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(resourceId: UUID, resourceType: Symbol, resourceSpaces_canRemove : Map[ProjectSpace,Boolean], currentCollection: Option[Collection])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.libs.json.Json

import play.api.Play.current

import play.api.i18n.Messages


Seq[Any](format.raw/*1.171*/("""
"""),format.raw/*6.1*/("""    <div class="row">
        <div class="col-md-12">
        """),_display_(Seq[Any](/*8.10*/if(resourceType == ResourceRef.collection)/*8.52*/ {_display_(Seq[Any](format.raw/*8.54*/("""
            """),_display_(Seq[Any](/*9.14*/if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*9.85*/ {_display_(Seq[Any](format.raw/*9.87*/("""
                <h4> """),_display_(Seq[Any](/*10.23*/Messages("a.contains.b", Messages("space.title"), Messages("collection.title")))),format.raw/*10.102*/("""</h4>
            """)))}/*11.15*/else/*11.20*/{_display_(Seq[Any](format.raw/*11.21*/("""
                <h4>"""),_display_(Seq[Any](/*12.22*/Messages("a.contains.b", Messages("spaces.title"), Messages("collection.title")))),format.raw/*12.102*/("""</h4>
            """)))})),format.raw/*13.14*/("""

        """)))}/*15.11*/else/*15.16*/{_display_(Seq[Any](format.raw/*15.17*/("""
            """),_display_(Seq[Any](/*16.14*/if(play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)/*16.85*/ {_display_(Seq[Any](format.raw/*16.87*/("""
                <h4>"""),_display_(Seq[Any](/*17.22*/Messages("a.contains.b", Messages("space.title"), Messages("dataset.title")))),format.raw/*17.98*/("""</h4>
            """)))}/*18.15*/else/*18.20*/{_display_(Seq[Any](format.raw/*18.21*/("""
                <h4>"""),_display_(Seq[Any](/*19.22*/Messages("a.contains.b", Messages("spaces.title"), Messages("dataset.title")))),format.raw/*19.99*/("""</h4>
            """)))})),format.raw/*20.14*/("""
        """)))})),format.raw/*21.10*/("""
        </div>
    </div>

    <div class="row bottom-padding hideTree">
        <div id="spacesList" class="col-md-12">
        """),_display_(Seq[Any](/*27.10*/resourceSpaces_canRemove/*27.34*/.map/*27.38*/ {case (s, v) =>_display_(Seq[Any](format.raw/*27.54*/("""
            <div id="col_"""),_display_(Seq[Any](/*28.27*/s/*28.28*/.id)),format.raw/*28.31*/("""" class="row bottom-padding">
                <div class="col-md-2">
                    """),_display_(Seq[Any](/*30.22*/if(s.logoURL.isDefined)/*30.45*/ {_display_(Seq[Any](format.raw/*30.47*/("""
                        <img src=""""),_display_(Seq[Any](/*31.36*/s/*31.37*/.logoURL.get)),format.raw/*31.49*/("""" class="smallthumbnail">
                    """)))}/*32.23*/else/*32.28*/{_display_(Seq[Any](format.raw/*32.29*/("""
                        <a href=""""),_display_(Seq[Any](/*33.35*/routes/*33.41*/.Spaces.getSpace(s.id))),format.raw/*33.63*/("""">
                            <span class="smallicon glyphicon glyphicon-hdd"></span>
                        </a>
                    """)))})),format.raw/*36.22*/("""
                </div>
                <div class="col-md-10">
                    <div>
                        <a href=""""),_display_(Seq[Any](/*40.35*/routes/*40.41*/.Spaces.getSpace(s.id))),format.raw/*40.63*/("""" id='"""),_display_(Seq[Any](/*40.70*/s/*40.71*/.id)),format.raw/*40.74*/("""' class ="space">"""),_display_(Seq[Any](/*40.92*/s/*40.93*/.name)),format.raw/*40.98*/("""</a>
                    </div>
                    <div>
                        """),_display_(Seq[Any](/*43.26*/if(resourceType == ResourceRef.dataset)/*43.65*/ {_display_(Seq[Any](format.raw/*43.67*/("""
                            """),_display_(Seq[Any](/*44.30*/if(s.datasetCount ==1)/*44.52*/{_display_(Seq[Any](format.raw/*44.53*/("""
                                1 dataset
                            """)))}/*46.31*/else/*46.36*/{_display_(Seq[Any](format.raw/*46.37*/("""
                                """),_display_(Seq[Any](/*47.34*/s/*47.35*/.datasetCount)),format.raw/*47.48*/(""" datasets
                            """)))})),format.raw/*48.30*/("""
                                """),_display_(Seq[Any](/*49.34*/if(Permission.checkOwner(user, ResourceRef(ResourceRef.dataset, resourceId)) || Permission.checkPermission(Permission.RemoveResourceFromSpace, ResourceRef(ResourceRef.space, s.id)))/*49.215*/ {_display_(Seq[Any](format.raw/*49.217*/("""
                                    | <button onclick="confirmRemoveResourceFromResourceEvent('space','"""),_display_(Seq[Any](/*50.105*/Messages("space.title"))),format.raw/*50.128*/("""','"""),_display_(Seq[Any](/*50.132*/(s.id))),format.raw/*50.138*/("""','dataset','"""),_display_(Seq[Any](/*50.152*/(resourceId))),format.raw/*50.164*/("""',event)" class="btn btn-link btn-xs" title="Remove the dataset from the """),_display_(Seq[Any](/*50.238*/Messages("space.title"))),format.raw/*50.261*/("""">
                                        <span class="glyphicon glyphicon-remove"></span> Remove</button>
                                """)))}/*52.35*/else/*52.40*/{_display_(Seq[Any](format.raw/*52.41*/("""
                                    |
                                    <div class="inline" title="No permission to remove from the """),_display_(Seq[Any](/*54.98*/Messages("space.title"))),format.raw/*54.121*/("""">
                                        <button class="btn btn-link btn-xs disabled"><span class="glyphicon glyphicon-remove"></span> Remove</button>
                                    </div>
                                """)))})),format.raw/*57.34*/("""

                        """)))})),format.raw/*59.26*/("""
                        """),_display_(Seq[Any](/*60.26*/if(resourceType == ResourceRef.collection)/*60.68*/ {_display_(Seq[Any](format.raw/*60.70*/("""
                            """),_display_(Seq[Any](/*61.30*/if(s.collectionCount == 1)/*61.56*/{_display_(Seq[Any](format.raw/*61.57*/("""
                                1 collection
                            """)))}/*63.31*/else/*63.36*/{_display_(Seq[Any](format.raw/*63.37*/("""
                                """),_display_(Seq[Any](/*64.34*/s/*64.35*/.collectionCount)),format.raw/*64.51*/(""" collections
                            """)))})),format.raw/*65.30*/("""
                            """),format.raw/*69.32*/("""
                            """),_display_(Seq[Any](/*70.30*/if(v == true)/*70.43*/{_display_(Seq[Any](format.raw/*70.44*/("""

                                    """),_display_(Seq[Any](/*72.38*/if(Permission.checkOwner(user, ResourceRef(ResourceRef.collection, resourceId)) || Permission.checkPermission(Permission.RemoveResourceFromSpace, ResourceRef(ResourceRef.space, s.id)))/*72.222*/ {_display_(Seq[Any](format.raw/*72.224*/("""
                                        | <button onclick="confirmRemoveResourceFromResourceEvent('space','"""),_display_(Seq[Any](/*73.109*/Messages("space.title"))),format.raw/*73.132*/("""','"""),_display_(Seq[Any](/*73.136*/(s.id))),format.raw/*73.142*/("""','collection','"""),_display_(Seq[Any](/*73.159*/(resourceId))),format.raw/*73.171*/("""',event)" class="btn btn-link btn-xs" title="Remove the collection from the """),_display_(Seq[Any](/*73.248*/Messages("space.title"))),format.raw/*73.271*/("""">
                                            <span class="glyphicon glyphicon-remove"></span> Remove</button>
                                    """)))}/*75.39*/else/*75.44*/{_display_(Seq[Any](format.raw/*75.45*/("""
                                        |
                                        <div class="inline" title="No permission to remove from the """),_display_(Seq[Any](/*77.102*/Messages("space.title"))),format.raw/*77.125*/("""">
                                            <button class="btn btn-link btn-xs disabled"><span class="glyphicon glyphicon-remove"></span> Remove</button>
                                        </div>
                                    """)))})),format.raw/*80.38*/("""

                            """)))}/*82.31*/else/*82.36*/{_display_(Seq[Any](format.raw/*82.37*/("""
                                |
                                <div class="inline" title="Child collection, only the parent can be removed">
                                    <button class="btn btn-link btn-xs disabled"><span class="glyphicon glyphicon-remove"></span>
                                        Remove</button>
                                </div>
                            """)))})),format.raw/*88.30*/("""
                        """)))})),format.raw/*89.26*/("""
                    </div>
                </div>
            </div>
        """)))})),format.raw/*93.10*/("""
        </div>
    </div>

    <!-- If the user can edit the dataset, the elements are enabled, otherwise they are present but disabled to provide consistent UE. -->
    """),_display_(Seq[Any](/*98.6*/if(Permission.checkOwner(user, ResourceRef(resourceType, resourceId)))/*98.76*/ {_display_(Seq[Any](format.raw/*98.78*/("""

        """),_display_(Seq[Any](/*100.10*/if(resourceType == ResourceRef.collection)/*100.52*/ {_display_(Seq[Any](format.raw/*100.54*/("""
            """),_display_(Seq[Any](/*101.14*/spaces/*101.20*/.spaceSelect(resourceId, resourceType, "spaceAddSelect", "addCollectionToSpace('" + resourceId + "')", "Add", "Add collection to " + Messages("space.title"), "add-to-space-widget"))),format.raw/*101.200*/("""
        """)))}/*102.11*/else/*102.16*/{_display_(Seq[Any](format.raw/*102.17*/("""
            """),_display_(Seq[Any](/*103.14*/spaces/*103.20*/.spaceSelect(resourceId, resourceType, "spaceAddSelect", "addDatasetToSpace('" + resourceId + "')", "Add", "Add dataset to " + Messages("space.title"), "add-to-space-widget"))),format.raw/*103.194*/("""
        """)))})),format.raw/*104.10*/("""
    """)))})),format.raw/*105.6*/("""
    """),_display_(Seq[Any](/*106.6*/if(Permission.checkPermission(user, Permission.DownloadFiles, ResourceRef(resourceType, resourceId)) &&
        Permission.checkPermission(user, Permission.ViewDataset, ResourceRef(resourceType, resourceId)))/*107.105*/ {_display_(Seq[Any](format.raw/*107.107*/("""
        """),_display_(Seq[Any](/*108.10*/if(current.plugin[services.SpaceCopyPlugin].isDefined)/*108.64*/ {_display_(Seq[Any](format.raw/*108.66*/("""
            """),_display_(Seq[Any](/*109.14*/if(resourceType == ResourceRef.dataset)/*109.53*/ {_display_(Seq[Any](format.raw/*109.55*/("""
                <h4 class="bottom-padding">Copy Dataset to """),_display_(Seq[Any](/*110.61*/Messages("spaces.title"))),format.raw/*110.85*/("""</h4>
                """),_display_(Seq[Any](/*111.18*/spaces/*111.24*/.spaceSelect(resourceId, resourceType, "spaceCopySelect", "copyDatasetToSpace('" + resourceId + "')", "Copy", "Copy dataset to " + Messages("space.title"), "copy-to-space-widget"))),format.raw/*111.203*/("""
            """)))})),format.raw/*112.14*/(""" """),format.raw/*116.18*/("""
        """)))})),format.raw/*117.10*/("""

    """)))})),format.raw/*119.6*/("""



<link rel="stylesheet" href=""""),_display_(Seq[Any](/*123.31*/routes/*123.37*/.Assets.at("stylesheets/chosen.css"))),format.raw/*123.73*/("""">
<script src=""""),_display_(Seq[Any](/*124.15*/routes/*124.21*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*124.63*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*125.15*/routes/*125.21*/.Assets.at("javascripts/spaceModify.js"))),format.raw/*125.61*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*126.15*/routes/*126.21*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*126.61*/("""" type="text/javascript"></script>


<script language = "javascript">
    var isSharingEnabled= """),_display_(Seq[Any](/*130.28*/play/*130.32*/.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined)),format.raw/*130.95*/(""";
    """),_display_(Seq[Any](/*131.6*/if(!play.api.Play.current.plugin[services.SpaceSharingPlugin].isDefined && resourceSpaces_canRemove.size > 0 )/*131.116*/ {_display_(Seq[Any](format.raw/*131.118*/("""
    $("#add-to-space-widget").addClass("hidden");
    """)))})),format.raw/*133.6*/("""
    function getCollection(id) """),format.raw/*134.32*/("""{"""),format.raw/*134.33*/("""
        var request = jsRoutes.api.Collections.getCollection(id).ajax("""),format.raw/*135.71*/("""{"""),format.raw/*135.72*/("""
            type: 'GET'
        """),format.raw/*137.9*/("""}"""),format.raw/*137.10*/(""");

        request.done(function (response, textStatus, jqXHR) """),format.raw/*139.61*/("""{"""),format.raw/*139.62*/("""
            var o =$.parseJSON(jqXHR.responseText);
            return o
        """),format.raw/*142.9*/("""}"""),format.raw/*142.10*/(""");

        request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*144.63*/("""{"""),format.raw/*144.64*/("""
            console.error("The following error occured: " + textStatus, errorThrown);
            var errMsg = "Error message.";
            if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*147.56*/("""{"""),format.raw/*147.57*/("""
                notify("Error : " + errorThrown, "error");
            """),format.raw/*149.13*/("""}"""),format.raw/*149.14*/("""
        """),format.raw/*150.9*/("""}"""),format.raw/*150.10*/(""");
        return false;
    """),format.raw/*152.5*/("""}"""),format.raw/*152.6*/("""

</script>
"""))}
    }
    
    def render(resourceId:UUID,resourceType:Symbol,resourceSpaces_canRemove:Map[ProjectSpace, Boolean],currentCollection:Option[Collection],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(resourceId,resourceType,resourceSpaces_canRemove,currentCollection)(user)
    
    def f:((UUID,Symbol,Map[ProjectSpace, Boolean],Option[Collection]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (resourceId,resourceType,resourceSpaces_canRemove,currentCollection) => (user) => apply(resourceId,resourceType,resourceSpaces_canRemove,currentCollection)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:49 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/spaceAllocation.scala.html
                    HASH: 00641cbd4fdd2deee8899d7d45fd0bee0109cb4d
                    MATRIX: 675->1|1054->170|1081->287|1179->350|1229->392|1268->394|1317->408|1396->479|1435->481|1494->504|1596->583|1634->603|1647->608|1686->609|1744->631|1847->711|1898->730|1928->742|1941->747|1980->748|2030->762|2110->833|2150->835|2208->857|2306->933|2344->953|2357->958|2396->959|2454->981|2553->1058|2604->1077|2646->1087|2813->1218|2846->1242|2859->1246|2913->1262|2976->1289|2986->1290|3011->1293|3137->1383|3169->1406|3209->1408|3281->1444|3291->1445|3325->1457|3391->1505|3404->1510|3443->1511|3514->1546|3529->1552|3573->1574|3742->1711|3902->1835|3917->1841|3961->1863|4004->1870|4014->1871|4039->1874|4093->1892|4103->1893|4130->1898|4249->1981|4297->2020|4337->2022|4403->2052|4434->2074|4473->2075|4564->2148|4577->2153|4616->2154|4686->2188|4696->2189|4731->2202|4802->2241|4872->2275|5063->2456|5104->2458|5246->2563|5292->2586|5333->2590|5362->2596|5413->2610|5448->2622|5559->2696|5605->2719|5765->2861|5778->2866|5817->2867|5989->3003|6035->3026|6296->3255|6355->3282|6417->3308|6468->3350|6508->3352|6574->3382|6609->3408|6648->3409|6742->3485|6755->3490|6794->3491|6864->3525|6874->3526|6912->3542|6986->3584|7043->3853|7109->3883|7131->3896|7170->3897|7245->3936|7439->4120|7480->4122|7626->4231|7672->4254|7713->4258|7742->4264|7796->4281|7831->4293|7945->4370|7991->4393|8159->4543|8172->4548|8211->4549|8392->4693|8438->4716|8711->4957|8761->4989|8774->4994|8813->4995|9244->5394|9302->5420|9413->5499|9620->5671|9699->5741|9739->5743|9787->5754|9839->5796|9880->5798|9931->5812|9947->5818|10151->5998|10181->6009|10195->6014|10235->6015|10286->6029|10302->6035|10500->6209|10543->6219|10581->6225|10623->6231|10842->6439|10884->6441|10931->6451|10995->6505|11036->6507|11087->6521|11136->6560|11177->6562|11275->6623|11322->6647|11382->6670|11398->6676|11601->6855|11648->6869|11678->7128|11721->7138|11760->7145|11831->7179|11847->7185|11906->7221|11960->7238|11976->7244|12041->7286|12127->7335|12143->7341|12206->7381|12292->7430|12308->7436|12371->7476|12505->7573|12519->7577|12605->7640|12648->7647|12769->7757|12811->7759|12899->7815|12960->7847|12990->7848|13090->7919|13120->7920|13181->7953|13211->7954|13304->8018|13334->8019|13444->8101|13474->8102|13569->8168|13599->8169|13813->8354|13843->8355|13944->8427|13974->8428|14011->8437|14041->8438|14098->8467|14127->8468
                    LINES: 20->1|30->1|31->6|33->8|33->8|33->8|34->9|34->9|34->9|35->10|35->10|36->11|36->11|36->11|37->12|37->12|38->13|40->15|40->15|40->15|41->16|41->16|41->16|42->17|42->17|43->18|43->18|43->18|44->19|44->19|45->20|46->21|52->27|52->27|52->27|52->27|53->28|53->28|53->28|55->30|55->30|55->30|56->31|56->31|56->31|57->32|57->32|57->32|58->33|58->33|58->33|61->36|65->40|65->40|65->40|65->40|65->40|65->40|65->40|65->40|65->40|68->43|68->43|68->43|69->44|69->44|69->44|71->46|71->46|71->46|72->47|72->47|72->47|73->48|74->49|74->49|74->49|75->50|75->50|75->50|75->50|75->50|75->50|75->50|75->50|77->52|77->52|77->52|79->54|79->54|82->57|84->59|85->60|85->60|85->60|86->61|86->61|86->61|88->63|88->63|88->63|89->64|89->64|89->64|90->65|91->69|92->70|92->70|92->70|94->72|94->72|94->72|95->73|95->73|95->73|95->73|95->73|95->73|95->73|95->73|97->75|97->75|97->75|99->77|99->77|102->80|104->82|104->82|104->82|110->88|111->89|115->93|120->98|120->98|120->98|122->100|122->100|122->100|123->101|123->101|123->101|124->102|124->102|124->102|125->103|125->103|125->103|126->104|127->105|128->106|129->107|129->107|130->108|130->108|130->108|131->109|131->109|131->109|132->110|132->110|133->111|133->111|133->111|134->112|134->116|135->117|137->119|141->123|141->123|141->123|142->124|142->124|142->124|143->125|143->125|143->125|144->126|144->126|144->126|148->130|148->130|148->130|149->131|149->131|149->131|151->133|152->134|152->134|153->135|153->135|155->137|155->137|157->139|157->139|160->142|160->142|162->144|162->144|165->147|165->147|167->149|167->149|168->150|168->150|170->152|170->152
                    -- GENERATED --
                */
            