
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object updateExtractors extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[List[String],List[String],UUID,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(runningExtractors: List[String], selectedExtractors: List[String], space: UUID, spaceName: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters._

import helper._


Seq[Any](format.raw/*1.137*/("""
"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Space")/*5.15*/ {_display_(Seq[Any](format.raw/*5.17*/("""
	<div>
		<ol class="breadcrumb">
			<li><span class="glyphicon glyphicon-hdd"></span>  <a href=""""),_display_(Seq[Any](/*8.65*/routes/*8.71*/.Spaces.getSpace(space))),format.raw/*8.94*/("""" title=""""),_display_(Seq[Any](/*8.104*/spaceName)),format.raw/*8.113*/(""""> """),_display_(Seq[Any](/*8.117*/Html(ellipsize(spaceName,18)))),format.raw/*8.146*/("""</a></li>
			<li><span class="glyphicon glyphicon-fullscreen"></span> Extractors</li>

		</ol>
	</div>
 """),_display_(Seq[Any](/*13.3*/if(runningExtractors.size == 0)/*13.34*/ {_display_(Seq[Any](format.raw/*13.36*/("""
	<div class="page-header">
        <h1> No extractors running</h1>
  </div>
 """)))}/*17.4*/else/*17.9*/{_display_(Seq[Any](format.raw/*17.10*/("""     
     <div class="page-header">
        <h1> Update Extractors </h1>
    </div>
    
	"""),_display_(Seq[Any](/*22.3*/form(action = routes.Spaces.updateExtractors(space), 'id->"formOne", 'enctype -> "multipart/form-data", 'class -> "form-horizontal"  )/*22.137*/{_display_(Seq[Any](format.raw/*22.138*/("""
	   	<!--  Pass the space id to the controller -->                        
  		<input type="hidden" name="space_id" value=""""),_display_(Seq[Any](/*24.50*/space)),format.raw/*24.55*/("""">
  	
   		<!-- Display all running extractors. If extractor already in this space, check the box. -->
     	"""),_display_(Seq[Any](/*27.8*/runningExtractors/*27.25*/.map/*27.29*/ { extractor =>_display_(Seq[Any](format.raw/*27.44*/("""
     		<input type="checkbox" name="extractors" value=""""),_display_(Seq[Any](/*28.57*/extractor)),format.raw/*28.66*/(""""	 
     			"""),_display_(Seq[Any](/*29.10*/if(selectedExtractors.contains(extractor))/*29.52*/ {_display_(Seq[Any](format.raw/*29.54*/("""checked""")))}/*29.63*/else/*29.68*/{_display_(Seq[Any](format.raw/*29.69*/("""unchecked""")))})),format.raw/*29.79*/(""" >
	 		"""),_display_(Seq[Any](/*30.6*/extractor)),format.raw/*30.15*/("""	     	
	 		<br>
	 	""")))})),format.raw/*32.5*/("""
	 	<p> 	
	 	"""),_display_(Seq[Any](/*34.5*/updateButton())),format.raw/*34.19*/("""	 	 
	""")))})),format.raw/*35.3*/("""<!-- end of form -->
 """)))})),format.raw/*36.3*/("""  
""")))})),format.raw/*37.2*/("""    
"""))}
    }
    
    def render(runningExtractors:List[String],selectedExtractors:List[String],space:UUID,spaceName:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(runningExtractors,selectedExtractors,space,spaceName)(user)
    
    def f:((List[String],List[String],UUID,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (runningExtractors,selectedExtractors,space,spaceName) => (user) => apply(runningExtractors,selectedExtractors,space,spaceName)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/updateExtractors.scala.html
                    HASH: 18894e881520fa41b8760691c911e7392a6e6431
                    MATRIX: 656->1|935->136|962->187|998->189|1019->202|1058->204|1191->302|1205->308|1249->331|1295->341|1326->350|1366->354|1417->383|1557->488|1597->519|1637->521|1734->601|1746->606|1785->607|1912->699|2056->833|2096->834|2257->959|2284->964|2430->1075|2456->1092|2469->1096|2522->1111|2615->1168|2646->1177|2695->1190|2746->1232|2786->1234|2813->1243|2826->1248|2865->1249|2907->1259|2950->1267|2981->1276|3033->1297|3082->1311|3118->1325|3156->1332|3210->1355|3245->1359
                    LINES: 20->1|26->1|27->4|28->5|28->5|28->5|31->8|31->8|31->8|31->8|31->8|31->8|31->8|36->13|36->13|36->13|40->17|40->17|40->17|45->22|45->22|45->22|47->24|47->24|50->27|50->27|50->27|50->27|51->28|51->28|52->29|52->29|52->29|52->29|52->29|52->29|52->29|53->30|53->30|55->32|57->34|57->34|58->35|59->36|60->37
                    -- GENERATED --
                */
            