
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object requestresponseemail extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[models.User,String,String,String,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(user:models.User, spaceid: String, spacename:String, requestmessage: String)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider

import play.api.i18n.Messages


Seq[Any](format.raw/*1.112*/("""
"""),format.raw/*4.1*/("""<html>
  <body>
    <p>Hello,</p>

    <p>
      <a href=""""),_display_(Seq[Any](/*9.17*/routes/*9.23*/.Profile.viewProfileUUID(user.id).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.97*/("""">"""),_display_(Seq[Any](/*9.100*/user/*9.104*/.fullName)),format.raw/*9.113*/("""</a> """),_display_(Seq[Any](/*9.119*/{requestmessage})),format.raw/*9.135*/(""" """),_display_(Seq[Any](/*9.137*/Messages("space.title"))),format.raw/*9.160*/(""" <a href=""""),_display_(Seq[Any](/*9.171*/routes/*9.177*/.Spaces.getSpace(UUID(spaceid)).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.249*/("""">"""),_display_(Seq[Any](/*9.252*/spacename)),format.raw/*9.261*/("""</a>
      at <a href=""""),_display_(Seq[Any](/*10.20*/routes/*10.26*/.Application.index().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*10.87*/("""">"""),_display_(Seq[Any](/*10.90*/services/*10.98*/.AppConfiguration.getDisplayName)),format.raw/*10.130*/("""</a>.
    </p>
    """),_display_(Seq[Any](/*12.6*/if(requestmessage.contains("accept"))/*12.43*/ {_display_(Seq[Any](format.raw/*12.45*/("""
      <p>You can log in to view it.</p>
    """)))})),format.raw/*14.6*/("""
  </body>
</html>"""))}
    }
    
    def render(user:models.User,spaceid:String,spacename:String,requestmessage:String,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(user,spaceid,spacename,requestmessage)(request)
    
    def f:((models.User,String,String,String) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (user,spaceid,spacename,requestmessage) => (request) => apply(user,spaceid,spacename,requestmessage)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/requestresponseemail.scala.html
                    HASH: f101deca869c3587920d40f85020f62cd14ac4ea
                    MATRIX: 649->1|927->111|954->186|1048->245|1062->251|1157->325|1196->328|1209->332|1240->341|1282->347|1320->363|1358->365|1403->388|1450->399|1465->405|1559->477|1598->480|1629->489|1689->513|1704->519|1787->580|1826->583|1843->591|1898->623|1953->643|1999->680|2039->682|2116->728
                    LINES: 20->1|26->1|27->4|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|33->10|33->10|33->10|33->10|33->10|33->10|35->12|35->12|35->12|37->14
                    -- GENERATED --
                */
            