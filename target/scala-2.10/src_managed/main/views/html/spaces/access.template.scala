
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object access extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[models.ProjectSpace,Map[User, String],String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space:models.ProjectSpace, userRoleMap: Map[User, String], classes: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import api.Permission


Seq[Any](format.raw/*1.114*/("""
"""),format.raw/*4.1*/("""
<div class=""""),_display_(Seq[Any](/*5.14*/classes)),format.raw/*5.21*/("""">
    <div class="col-md-12 col-lg-12 col-sm-12 hideTree">
        <h3>Access</h3>
        <p><span class="label label-default">"""),_display_(Seq[Any](/*8.47*/space/*8.52*/.status.capitalize)),format.raw/*8.70*/("""</span></p>
        """),_display_(Seq[Any](/*9.10*/if(space.isTrial)/*9.27*/{_display_(Seq[Any](format.raw/*9.28*/("""
            """),_display_(Seq[Any](/*10.14*/if(play.Play.application().configuration().getBoolean("verifySpaces") &&
                    Permission.checkPermission(Permission.PublicSpace, ResourceRef(ResourceRef.space, space.id)))/*11.114*/ {_display_(Seq[Any](format.raw/*11.116*/("""
                <div id="verify">
                    <p>"""),_display_(Seq[Any](/*13.25*/play/*13.29*/.api.i18n.Messages("trial.space"))),format.raw/*13.62*/("""</p>
                    """),_display_(Seq[Any](/*14.22*/if(api.Permission.checkServerAdmin(user))/*14.63*/{_display_(Seq[Any](format.raw/*14.64*/("""
                        <button class="btn btn-link" title="Verify this space" >
                            <span class="glyphicon glyphicon-ok" ></span> Verify</button>
                    """)))})),format.raw/*17.22*/("""
                </div>
            """)))})),format.raw/*19.14*/("""
        """)))})),format.raw/*20.10*/("""
        <!-- user has submit request -->
        """),_display_(Seq[Any](/*22.10*/defining("You are not authorized to access this " + Messages("space.title"))/*22.86*/ { msg =>_display_(Seq[Any](format.raw/*22.95*/("""
            """),_display_(Seq[Any](/*23.14*/if(user.isDefined)/*23.32*/ {_display_(Seq[Any](format.raw/*23.34*/("""
                """),_display_(Seq[Any](/*24.18*/if(userRoleMap.contains(user.get))/*24.52*/ {_display_(Seq[Any](format.raw/*24.54*/("""
                    """),_display_(Seq[Any](/*25.22*/if(Permission.checkPermission(Permission.EditSpace, ResourceRef(ResourceRef.space, UUID(space.id.stringify))))/*25.132*/ {_display_(Seq[Any](format.raw/*25.134*/("""
                        <p>You are authorized to access this """),_display_(Seq[Any](/*26.63*/{Messages("space.title")})),format.raw/*26.88*/(""".</p>
                    """)))}/*27.23*/else/*27.28*/{_display_(Seq[Any](format.raw/*27.29*/("""
                        <p>You are authorized to partially access this """),_display_(Seq[Any](/*28.73*/{Messages("space.title")})),format.raw/*28.98*/(""".</p>
                    """)))})),format.raw/*29.22*/("""
                """)))}/*30.19*/else/*30.24*/{_display_(Seq[Any](format.raw/*30.25*/("""
                    """),_display_(Seq[Any](/*31.22*/if(space.requests.contains(RequestResource(user.get.id)))/*31.79*/ {_display_(Seq[Any](format.raw/*31.81*/("""
                        <p>"""),_display_(Seq[Any](/*32.29*/msg)),format.raw/*32.32*/(""" Authorization request is pending.</p>
                    """)))}/*33.23*/else/*33.28*/{_display_(Seq[Any](format.raw/*33.29*/("""
                        <div>
                            <p>"""),_display_(Seq[Any](/*35.33*/msg)),format.raw/*35.36*/(""" """),_display_(Seq[Any](/*35.38*/spaces/*35.44*/.requestAuthorization(space.id))),format.raw/*35.75*/("""</p>
                        </div>
                    """)))})),format.raw/*37.22*/("""
                """)))})),format.raw/*38.18*/("""
            """)))}/*39.15*/else/*39.20*/{_display_(Seq[Any](format.raw/*39.21*/("""
                """),_display_(Seq[Any](/*40.18*/if(!space.isPublic)/*40.37*/{_display_(Seq[Any](format.raw/*40.38*/("""
                    <div>
                        <p>"""),_display_(Seq[Any](/*42.29*/msg)),format.raw/*42.32*/(""" """),_display_(Seq[Any](/*42.34*/spaces/*42.40*/.requestAuthorization(space.id))),format.raw/*42.71*/("""</p>
                    </div>
                """)))})),format.raw/*44.18*/("""
            """)))})),format.raw/*45.14*/("""
        """)))})),format.raw/*46.10*/("""
    </div>
</div>
<script>
    $("#verify").find("button").click(function() """),format.raw/*50.50*/("""{"""),format.raw/*50.51*/("""
        var request = jsRoutes.api.Spaces.verifySpace(""""),_display_(Seq[Any](/*51.57*/space/*51.62*/.id)),format.raw/*51.65*/("""").ajax("""),format.raw/*51.73*/("""{"""),format.raw/*51.74*/("""
            type: 'PUT'
        """),format.raw/*53.9*/("""}"""),format.raw/*53.10*/(""");
        request.done(function(response, textStatus, jsXHR)"""),format.raw/*54.59*/("""{"""),format.raw/*54.60*/("""
            $("#verify").hide();
            notify(""""),_display_(Seq[Any](/*56.22*/space/*56.27*/.name)),format.raw/*56.32*/(""""+" is verified and set to private", "success", false, 2000);
        """),format.raw/*57.9*/("""}"""),format.raw/*57.10*/(""");
        request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*58.63*/("""{"""),format.raw/*58.64*/("""
            console.error("The following error occurred: " + textStatus, errorThrown);
            var errMsg = "You must be logged in to verify";
            if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*61.56*/("""{"""),format.raw/*61.57*/("""
                notify("Error in verifying due to : " + errorThrown, "error");
            """),format.raw/*63.13*/("""}"""),format.raw/*63.14*/("""
        """),format.raw/*64.9*/("""}"""),format.raw/*64.10*/(""");
    """),format.raw/*65.5*/("""}"""),format.raw/*65.6*/(""");
</script>
"""))}
    }
    
    def render(space:models.ProjectSpace,userRoleMap:Map[User, String],classes:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space,userRoleMap,classes)(user)
    
    def f:((models.ProjectSpace,Map[User, String],String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space,userRoleMap,classes) => (user) => apply(space,userRoleMap,classes)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 10 14:38:49 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/access.scala.html
                    HASH: 672df28658b57f95fde2552855c9e49e537f200c
                    MATRIX: 653->1|913->113|940->168|989->182|1017->189|1182->319|1195->324|1234->342|1290->363|1315->380|1353->381|1403->395|1599->581|1640->583|1735->642|1748->646|1803->679|1865->705|1915->746|1954->747|2179->940|2248->977|2290->987|2377->1038|2462->1114|2509->1123|2559->1137|2586->1155|2626->1157|2680->1175|2723->1209|2763->1211|2821->1233|2941->1343|2982->1345|3081->1408|3128->1433|3174->1461|3187->1466|3226->1467|3335->1540|3382->1565|3441->1592|3478->1611|3491->1616|3530->1617|3588->1639|3654->1696|3694->1698|3759->1727|3784->1730|3863->1791|3876->1796|3915->1797|4014->1860|4039->1863|4077->1865|4092->1871|4145->1902|4234->1959|4284->1977|4317->1992|4330->1997|4369->1998|4423->2016|4451->2035|4490->2036|4581->2091|4606->2094|4644->2096|4659->2102|4712->2133|4793->2182|4839->2196|4881->2206|4986->2283|5015->2284|5108->2341|5122->2346|5147->2349|5183->2357|5212->2358|5272->2391|5301->2392|5390->2453|5419->2454|5510->2509|5524->2514|5551->2519|5648->2589|5677->2590|5770->2655|5799->2656|6030->2859|6059->2860|6179->2952|6208->2953|6244->2962|6273->2963|6307->2970|6335->2971
                    LINES: 20->1|26->1|27->4|28->5|28->5|31->8|31->8|31->8|32->9|32->9|32->9|33->10|34->11|34->11|36->13|36->13|36->13|37->14|37->14|37->14|40->17|42->19|43->20|45->22|45->22|45->22|46->23|46->23|46->23|47->24|47->24|47->24|48->25|48->25|48->25|49->26|49->26|50->27|50->27|50->27|51->28|51->28|52->29|53->30|53->30|53->30|54->31|54->31|54->31|55->32|55->32|56->33|56->33|56->33|58->35|58->35|58->35|58->35|58->35|60->37|61->38|62->39|62->39|62->39|63->40|63->40|63->40|65->42|65->42|65->42|65->42|65->42|67->44|68->45|69->46|73->50|73->50|74->51|74->51|74->51|74->51|74->51|76->53|76->53|77->54|77->54|79->56|79->56|79->56|80->57|80->57|81->58|81->58|84->61|84->61|86->63|86->63|87->64|87->64|88->65|88->65
                    -- GENERATED --
                */
            