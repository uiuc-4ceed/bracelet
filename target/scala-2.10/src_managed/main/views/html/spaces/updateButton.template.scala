
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object updateButton extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply():play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.1*/("""<div class="form-actions">
  <button type="submit" class="btn btn-primary" name = "submitValue" value="Update"><span class='glyphicon glyphicon-send'></span> Update</button>
  <a class="btn btn-default" id="cancel_space"><span class="glyphicon glyphicon-remove" ></span> Cancel</a>
</div>"""))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/updateButton.scala.html
                    HASH: 1cadd163ec27ab609cff92cb9bfde4d54efbff76
                    MATRIX: 682->0
                    LINES: 23->1
                    -- GENERATED --
                */
            