
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object collectionsTable extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[Collection],String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(collections: List[Collection], space: String):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.48*/("""

<!-- Not used? -->

<p>
    Most recent collections:
    <a href=""""),_display_(Seq[Any](/*7.15*/(routes.Collections.list("", "", 12, Some(space))))),format.raw/*7.65*/("""" class="pull-right btn btn-link btn-xs">
        <span class="glyphicon glyphicon-hand-right"></span>
        All Collections
    </a>
</p>

<table id='spacesCollectionsTable' class="table table-bordered table-hover fixedtable">
<thead>
    <tr>
        <th style="width: 27%">Name</th>
        <th style="width: 17%">Created</th>
        <th style="width: 36%">Description</th>
        <th style="width: 20%"></th>
    </tr>
</thead>
<tbody>
"""),_display_(Seq[Any](/*23.2*/collections/*23.13*/.map/*23.17*/ { collection =>_display_(Seq[Any](format.raw/*23.33*/("""
    <tr data-collectionId=""""),_display_(Seq[Any](/*24.29*/(collection.id.toString))),format.raw/*24.53*/("""">
        <td><a href=""""),_display_(Seq[Any](/*25.23*/(routes.Collections.collection(collection.id)))),format.raw/*25.69*/("""">"""),_display_(Seq[Any](/*25.72*/collection/*25.82*/.name)),format.raw/*25.87*/("""</a></td>
        <td>"""),_display_(Seq[Any](/*26.14*/collection/*26.24*/.created.format("MMM dd, yyyy"))),format.raw/*26.55*/("""</td>
        <td style="white-space:pre-line;">"""),_display_(Seq[Any](/*27.44*/collection/*27.54*/.description)),format.raw/*27.66*/("""</td>
        <td>
            """),_display_(Seq[Any](/*29.14*/if(!collection.thumbnail_id.isEmpty)/*29.50*/{_display_(Seq[Any](format.raw/*29.51*/("""
                <a href=""""),_display_(Seq[Any](/*30.27*/(routes.Collections.collection(collection.id)))),format.raw/*30.73*/("""">
                    <img class="img-responsive fit-in-space" src=""""),_display_(Seq[Any](/*31.68*/(routes.Files.thumbnail(UUID(collection.thumbnail_id.toString().substring(5,collection.thumbnail_id.toString().length-1)))))),format.raw/*31.191*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*31.212*/Html(collection.name))),format.raw/*31.233*/("""">>
                </a>
            """)))})),format.raw/*33.14*/("""
            """),_display_(Seq[Any](/*34.14*/if(collection.thumbnail_id.isEmpty)/*34.49*/{_display_(Seq[Any](format.raw/*34.50*/("""No thumbnail available""")))})),format.raw/*34.73*/("""
        </td>
    </tr>
""")))})),format.raw/*37.2*/("""
</tbody>
</table>
"""))}
    }
    
    def render(collections:List[Collection],space:String): play.api.templates.HtmlFormat.Appendable = apply(collections,space)
    
    def f:((List[Collection],String) => play.api.templates.HtmlFormat.Appendable) = (collections,space) => apply(collections,space)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/collectionsTable.scala.html
                    HASH: 3594c4dde84d74a01a3aecf04af5c1744fa37f2f
                    MATRIX: 622->1|762->47|866->116|937->166|1417->611|1437->622|1450->626|1504->642|1569->671|1615->695|1676->720|1744->766|1783->769|1802->779|1829->784|1888->807|1907->817|1960->848|2045->897|2064->907|2098->919|2166->951|2211->987|2250->988|2313->1015|2381->1061|2487->1131|2633->1254|2691->1275|2735->1296|2805->1334|2855->1348|2899->1383|2938->1384|2993->1407|3050->1433
                    LINES: 20->1|23->1|29->7|29->7|45->23|45->23|45->23|45->23|46->24|46->24|47->25|47->25|47->25|47->25|47->25|48->26|48->26|48->26|49->27|49->27|49->27|51->29|51->29|51->29|52->30|52->30|53->31|53->31|53->31|53->31|55->33|56->34|56->34|56->34|56->34|59->37
                    -- GENERATED --
                */
            