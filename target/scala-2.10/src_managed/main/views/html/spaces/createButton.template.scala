
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object createButton extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply():play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.3*/("""<div class="form-actions">
      <button type="submit" class="btn btn-primary" name="submitValue" value="Create"><span class='glyphicon glyphicon-ok'></span> """),_display_(Seq[Any](/*2.133*/Messages("create.title", ""))),format.raw/*2.161*/("""</button>
  </div>"""))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/createButton.scala.html
                    HASH: 495e1349f5770e34fead52982de674265a26fd69
                    MATRIX: 682->2|877->161|927->189
                    LINES: 23->1|24->2|24->2
                    -- GENERATED --
                */
            