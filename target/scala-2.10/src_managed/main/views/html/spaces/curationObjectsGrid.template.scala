
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object curationObjectsGrid extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[List[CurationObject],ProjectSpace,Option[Boolean],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObjectsList: List[CurationObject], space: ProjectSpace, isPublic: Option[Boolean])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.i18n.Messages


Seq[Any](format.raw/*1.129*/("""

"""),_display_(Seq[Any](/*5.2*/util/*5.6*/.masonry())),format.raw/*5.16*/("""

<p>
    <span class="small">"""),_display_(Seq[Any](/*8.26*/Messages("recent.message", "publications"))),format.raw/*8.68*/("""</span>
    <span class="pull-right">
        <a href=""""),_display_(Seq[Any](/*10.19*/routes/*10.25*/.CurationObjects.list("", "", 4, Some(space.id.stringify)))),format.raw/*10.83*/("""" class="btn btn-link btn-xs">
            <span class="glyphicon glyphicon-briefcase"></span> """),_display_(Seq[Any](/*11.66*/Messages("view.all", "Published Datasets"))),format.raw/*11.108*/("""
        </a>
    </span>
</p>

<div class="row" id="tile-view">
    <div class="col-md-12">
        <div id="masonry-datasets">
            """),_display_(Seq[Any](/*19.14*/curationObjectsList/*19.33*/.map/*19.37*/ { curationObject =>_display_(Seq[Any](format.raw/*19.57*/("""
                """),_display_(Seq[Any](/*20.18*/curations/*20.27*/.tile(curationObject, Some(space.id.stringify), "col-lg-4 col-md-4 col-sm-4"))),format.raw/*20.104*/("""
            """)))})),format.raw/*21.14*/("""
        </div>
    </div>
</div>

"""),_display_(Seq[Any](/*26.2*/if(!user.isDefined)/*26.21*/ {_display_(Seq[Any](format.raw/*26.23*/("""
    <script type="text/javascript">
            $('#create-dataset').addClass('disabled');
    </script>
""")))})))}
    }
    
    def render(curationObjectsList:List[CurationObject],space:ProjectSpace,isPublic:Option[Boolean],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObjectsList,space,isPublic)(user)
    
    def f:((List[CurationObject],ProjectSpace,Option[Boolean]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObjectsList,space,isPublic) => (user) => apply(curationObjectsList,space,isPublic)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/curationObjectsGrid.scala.html
                    HASH: 3ff16d0e3d3d1231e162d5e014b0f7732b9a269d
                    MATRIX: 671->1|946->128|983->185|994->189|1025->199|1091->230|1154->272|1246->328|1261->334|1341->392|1473->488|1538->530|1716->672|1744->691|1757->695|1815->715|1869->733|1887->742|1987->819|2033->833|2104->869|2132->888|2172->890
                    LINES: 20->1|26->1|28->5|28->5|28->5|31->8|31->8|33->10|33->10|33->10|34->11|34->11|42->19|42->19|42->19|42->19|43->20|43->20|43->20|44->21|49->26|49->26|49->26
                    -- GENERATED --
                */
            