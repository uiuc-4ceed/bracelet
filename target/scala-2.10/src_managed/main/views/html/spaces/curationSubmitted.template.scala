
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object curationSubmitted extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[models.CurationObject,String,Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObject: models.CurationObject, repository: String, success: Boolean)(implicit user: Option[models.User] ):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages


Seq[Any](format.raw/*1.116*/("""
"""),format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*4.2*/main("Submitted")/*4.19*/{_display_(Seq[Any](format.raw/*4.20*/("""
    <div class="row">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*7.14*/if(success)/*7.25*/ {_display_(Seq[Any](format.raw/*7.27*/("""
                <h1>Submitted</h1>
                <div class="alert alert-success" role="alert">
                    <a href=""""),_display_(Seq[Any](/*10.31*/routes/*10.37*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*10.90*/("""">"""),_display_(Seq[Any](/*10.93*/curationObject/*10.107*/.name)),format.raw/*10.112*/("""</a>
                    has been successfully submitted to repository <strong>"""),_display_(Seq[Any](/*11.76*/repository)),format.raw/*11.86*/("""</strong>.
                </div>
            """)))}/*13.15*/else/*13.20*/{_display_(Seq[Any](format.raw/*13.21*/("""
                <h1>Error</h1>
                <div class="alert alert-danger" role="alert">
                    There was an error submitting
                        <a href=""""),_display_(Seq[Any](/*17.35*/routes/*17.41*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*17.94*/(""""> """),_display_(Seq[Any](/*17.98*/curationObject/*17.112*/.name)),format.raw/*17.117*/("""</a>.
                        Please try again later.
                </div>
            """)))})),format.raw/*20.14*/("""
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-default" href=""""),_display_(Seq[Any](/*26.47*/routes/*26.53*/.Spaces.getSpace(curationObject.space))),format.raw/*26.91*/(""""><span class="glyphicon glyphicon-circle-arrow-left"></span> Go Back to """),_display_(Seq[Any](/*26.165*/Messages("space.title"))),format.raw/*26.188*/(""" </a>
            <a class="btn btn-default" href=""""),_display_(Seq[Any](/*27.47*/routes/*27.53*/.Spaces.stagingArea(curationObject.space))),format.raw/*27.94*/(""""><span class="glyphicon glyphicon-circle-arrow-left"></span> Go Back to Staging Area </a>
            """),_display_(Seq[Any](/*28.14*/if(success)/*28.25*/ {_display_(Seq[Any](format.raw/*28.27*/("""
                <a id="retract-curation" class="btn btn-danger" onclick="retractCuration('"""),_display_(Seq[Any](/*29.92*/curationObject/*29.106*/.id)),format.raw/*29.109*/("""')"><span class="glyphicon glyphicon-fire"></span> Retract Submission</a>
            """)))})),format.raw/*30.14*/("""
        </div>
    </div>
    <script src=""""),_display_(Seq[Any](/*33.19*/routes/*33.25*/.Assets.at("javascripts/curationProcess.js"))),format.raw/*33.69*/("""" type="text/javascript"></script>
    <script>
        function retractCuration(curationId) """),format.raw/*35.46*/("""{"""),format.raw/*35.47*/("""
            console.log("Retracting curation Object: " + curationId);

            var request = jsRoutes.api.CurationObjects.retractCurationObject(curationId).ajax("""),format.raw/*38.95*/("""{"""),format.raw/*38.96*/("""
                type: 'Delete'
            """),format.raw/*40.13*/("""}"""),format.raw/*40.14*/(""");

            request.done(function(response, texStatus, jqXHR) """),format.raw/*42.63*/("""{"""),format.raw/*42.64*/("""
                $('#retract-curation').addClass('disabled');
                notify(""""),_display_(Seq[Any](/*44.26*/Messages("curationobject.label"))),format.raw/*44.58*/(""" retracted");
            """),format.raw/*45.13*/("""}"""),format.raw/*45.14*/(""");

            request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*47.67*/("""{"""),format.raw/*47.68*/("""
               console.error("The following error ocurred: " + textStatus, errorThrown);
                notify("The """),_display_(Seq[Any](/*49.30*/Messages("curationobject.label"))),format.raw/*49.62*/(""" could not be retracted due to: " + errorThrown, "Error" );
            """),format.raw/*50.13*/("""}"""),format.raw/*50.14*/(""");
        """),format.raw/*51.9*/("""}"""),format.raw/*51.10*/("""
    </script>
""")))})))}
    }
    
    def render(curationObject:models.CurationObject,repository:String,success:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObject,repository,success)(user)
    
    def f:((models.CurationObject,String,Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObject,repository,success) => (user) => apply(curationObject,repository,success)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/curationSubmitted.scala.html
                    HASH: a9dce02a185b42a145b9adc47d4ce806105bc84c
                    MATRIX: 656->1|895->115|922->147|958->149|983->166|1021->167|1124->235|1143->246|1182->248|1347->377|1362->383|1437->436|1476->439|1500->453|1528->458|1644->538|1676->548|1742->596|1755->601|1794->602|2008->780|2023->786|2098->839|2138->843|2162->857|2190->862|2312->952|2476->1080|2491->1086|2551->1124|2662->1198|2708->1221|2796->1273|2811->1279|2874->1320|3014->1424|3034->1435|3074->1437|3202->1529|3226->1543|3252->1546|3371->1633|3452->1678|3467->1684|3533->1728|3654->1821|3683->1822|3877->1988|3906->1989|3978->2033|4007->2034|4101->2100|4130->2101|4253->2188|4307->2220|4361->2246|4390->2247|4488->2317|4517->2318|4672->2437|4726->2469|4826->2541|4855->2542|4893->2553|4922->2554
                    LINES: 20->1|24->1|25->3|26->4|26->4|26->4|29->7|29->7|29->7|32->10|32->10|32->10|32->10|32->10|32->10|33->11|33->11|35->13|35->13|35->13|39->17|39->17|39->17|39->17|39->17|39->17|42->20|48->26|48->26|48->26|48->26|48->26|49->27|49->27|49->27|50->28|50->28|50->28|51->29|51->29|51->29|52->30|55->33|55->33|55->33|57->35|57->35|60->38|60->38|62->40|62->40|64->42|64->42|66->44|66->44|67->45|67->45|69->47|69->47|71->49|71->49|72->50|72->50|73->51|73->51
                    -- GENERATED --
                */
            