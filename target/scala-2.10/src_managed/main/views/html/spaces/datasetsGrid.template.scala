
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object datasetsGrid extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[List[Dataset],ProjectSpace,Option[Boolean],List[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(datasetsList: List[Dataset], space: ProjectSpace, isPublic: Option[Boolean], userSelections: List[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.i18n.Messages


Seq[Any](format.raw/*1.145*/("""

"""),_display_(Seq[Any](/*5.2*/util/*5.6*/.masonry())),format.raw/*5.16*/("""

<p>
    <span class="small">"""),_display_(Seq[Any](/*8.26*/Messages("recent.message", Messages("datasets.title").toLowerCase))),format.raw/*8.92*/("""</span>
    <span class="pull-right">
    
            """),_display_(Seq[Any](/*11.14*/isPublic/*11.22*/ match/*11.28*/ {/*12.13*/case Some(b) =>/*12.28*/ {_display_(Seq[Any](format.raw/*12.30*/("""
                """),_display_(Seq[Any](/*13.18*/if(play.Play.application().configuration().getBoolean("sortInMemory"))/*13.88*/ {_display_(Seq[Any](format.raw/*13.90*/("""
                    <a href=""""),_display_(Seq[Any](/*14.31*/(routes.Datasets.sortedListInSpace(space.id.stringify, 0, 12, isPublic)))),format.raw/*14.103*/("""" class="btn btn-link btn-xs">
                        <span class="glyphicon glyphicon-briefcase"></span> """),_display_(Seq[Any](/*15.78*/Messages("view.all", Messages("datasets.title")))),format.raw/*15.126*/("""
                    </a>
                """)))}/*17.19*/else/*17.24*/{_display_(Seq[Any](format.raw/*17.25*/("""
                    <a href=""""),_display_(Seq[Any](/*18.31*/(routes.Datasets.list("", "", 12, Some(space.id.stringify), Some("publicAll"))))),format.raw/*18.110*/("""" class="btn btn-link btn-xs">
                        <span class="glyphicon glyphicon-briefcase"></span> """),_display_(Seq[Any](/*19.78*/Messages("view.all", Messages("datasets.title")))),format.raw/*19.126*/("""
                    </a>
                """)))})),format.raw/*21.18*/("""
         	""")))}/*23.13*/case None =>/*23.25*/ {_display_(Seq[Any](format.raw/*23.27*/("""
                """),_display_(Seq[Any](/*24.18*/if(play.Play.application().configuration().getBoolean("sortInMemory"))/*24.88*/ {_display_(Seq[Any](format.raw/*24.90*/("""
                    <a href=""""),_display_(Seq[Any](/*25.31*/(routes.Datasets.sortedListInSpace(space.id.stringify, 0, 12)))),format.raw/*25.93*/("""" class="btn btn-link btn-xs">
                        <span class="glyphicon glyphicon-briefcase"></span> """),_display_(Seq[Any](/*26.78*/Messages("view.all", Messages("datasets.title")))),format.raw/*26.126*/("""
                    </a>
                """)))}/*28.19*/else/*28.24*/{_display_(Seq[Any](format.raw/*28.25*/("""
                    <a href=""""),_display_(Seq[Any](/*29.31*/(routes.Datasets.list("", "", 12, Some(space.id.stringify))))),format.raw/*29.91*/("""" class="btn btn-link btn-xs">
                        <span class="glyphicon glyphicon-briefcase"></span> """),_display_(Seq[Any](/*30.78*/Messages("view.all", Messages("datasets.title")))),format.raw/*30.126*/("""
                    </a>
                """)))})),format.raw/*32.18*/("""
            """)))}})),format.raw/*34.10*/("""
    </span>
</p>

<div class="row" id="tile-view">
    <div class="col-md-12">
        <div id="masonry-datasets">
        """),_display_(Seq[Any](/*41.10*/datasetsList/*41.22*/.map/*41.26*/ { dataset =>_display_(Seq[Any](format.raw/*41.39*/("""
            """),_display_(Seq[Any](/*42.14*/if(userSelections.indexOf(dataset.id.toString()) != -1)/*42.69*/ {_display_(Seq[Any](format.raw/*42.71*/("""
                """),_display_(Seq[Any](/*43.18*/datasets/*43.26*/.tile(dataset, Some(space.id.stringify), Map.empty[UUID, Int], "col-lg-4 col-md-4 col-sm-4", false, true, routes.Spaces.getSpace(UUID(space.id.stringify)), true))),format.raw/*43.187*/("""
            """)))}/*44.15*/else/*44.20*/{_display_(Seq[Any](format.raw/*44.21*/("""
                """),_display_(Seq[Any](/*45.18*/datasets/*45.26*/.tile(dataset, Some(space.id.stringify), Map.empty[UUID, Int], "col-lg-4 col-md-4 col-sm-4", false, true, routes.Spaces.getSpace(UUID(space.id.stringify)), false))),format.raw/*45.188*/("""
            """)))})),format.raw/*46.14*/("""
        """)))})),format.raw/*47.10*/("""
        </div>
    </div>
</div>

"""),_display_(Seq[Any](/*52.2*/if(!user.isDefined)/*52.21*/ {_display_(Seq[Any](format.raw/*52.23*/("""
    <script type="text/javascript">
        $('#create-dataset').addClass('disabled');
    </script>
""")))})),format.raw/*56.2*/("""
		<script src=""""),_display_(Seq[Any](/*57.17*/routes/*57.23*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*57.68*/("""" type="text/javascript"></script>
        <script src=""""),_display_(Seq[Any](/*58.23*/routes/*58.29*/.Assets.at("javascripts/descriptionSummary.js"))),format.raw/*58.76*/("""" type="text/javascript"></script>"""))}
    }
    
    def render(datasetsList:List[Dataset],space:ProjectSpace,isPublic:Option[Boolean],userSelections:List[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(datasetsList,space,isPublic,userSelections)(user)
    
    def f:((List[Dataset],ProjectSpace,Option[Boolean],List[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (datasetsList,space,isPublic,userSelections) => (user) => apply(datasetsList,space,isPublic,userSelections)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/datasetsGrid.scala.html
                    HASH: 83ce2ac014b01a02a5253b6b87a8de7dbf421426
                    MATRIX: 670->1|961->144|998->201|1009->205|1040->215|1106->246|1193->312|1285->368|1302->376|1317->382|1328->397|1352->412|1392->414|1446->432|1525->502|1565->504|1632->535|1727->607|1871->715|1942->763|2004->807|2017->812|2056->813|2123->844|2225->923|2369->1031|2440->1079|2515->1122|2546->1147|2567->1159|2607->1161|2661->1179|2740->1249|2780->1251|2847->1282|2931->1344|3075->1452|3146->1500|3208->1544|3221->1549|3260->1550|3327->1581|3409->1641|3553->1749|3624->1797|3699->1840|3746->1864|3907->1989|3928->2001|3941->2005|3992->2018|4042->2032|4106->2087|4146->2089|4200->2107|4217->2115|4401->2276|4434->2291|4447->2296|4486->2297|4540->2315|4557->2323|4742->2485|4788->2499|4830->2509|4901->2545|4929->2564|4969->2566|5103->2669|5156->2686|5171->2692|5238->2737|5331->2794|5346->2800|5415->2847
                    LINES: 20->1|26->1|28->5|28->5|28->5|31->8|31->8|34->11|34->11|34->11|34->12|34->12|34->12|35->13|35->13|35->13|36->14|36->14|37->15|37->15|39->17|39->17|39->17|40->18|40->18|41->19|41->19|43->21|44->23|44->23|44->23|45->24|45->24|45->24|46->25|46->25|47->26|47->26|49->28|49->28|49->28|50->29|50->29|51->30|51->30|53->32|54->34|61->41|61->41|61->41|61->41|62->42|62->42|62->42|63->43|63->43|63->43|64->44|64->44|64->44|65->45|65->45|65->45|66->46|67->47|72->52|72->52|72->52|76->56|77->57|77->57|77->57|78->58|78->58|78->58
                    -- GENERATED --
                */
            