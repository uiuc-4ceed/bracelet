
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object newSpace extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Form[controllers.spaceFormData],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[controllers.spaceFormData])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages


Seq[Any](format.raw/*1.106*/("""
"""),_display_(Seq[Any](/*3.2*/newEditTemplate(myForm, title = Messages("create.header", Messages("space.title")), submitButton = createButton(), None, None))))}
    }
    
    def render(myForm:Form[controllers.spaceFormData],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm)(flash,user)
    
    def f:((Form[controllers.spaceFormData]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm) => (flash,user) => apply(myForm)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/newSpace.scala.html
                    HASH: a657edab7f5ef07076cccd0e027cca52c403af38
                    MATRIX: 661->1|890->105|926->138
                    LINES: 20->1|24->1|25->3
                    -- GENERATED --
                */
            