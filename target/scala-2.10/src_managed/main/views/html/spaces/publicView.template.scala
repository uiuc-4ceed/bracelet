
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object publicView extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[ProjectSpace,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: ProjectSpace, msg: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.72*/("""

"""),_display_(Seq[Any](/*3.2*/main(space.name)/*3.18*/ {_display_(Seq[Any](format.raw/*3.20*/("""
  """),_display_(Seq[Any](/*4.4*/if( space.bannerURL.isDefined || space.logoURL.isDefined)/*4.61*/{_display_(Seq[Any](format.raw/*4.62*/("""
    <div class="row">
      """),_display_(Seq[Any](/*6.8*/if(space.bannerURL.isDefined)/*6.37*/ {_display_(Seq[Any](format.raw/*6.39*/("""
        <div class="space-banner nopadding col-md-12 col-lg-12 col-sm-12" style="background-image:url('"""),_display_(Seq[Any](/*7.105*/(space.bannerURL))),format.raw/*7.122*/("""');">
        """)))}/*8.11*/else/*8.16*/{_display_(Seq[Any](format.raw/*8.17*/("""
        <div class="col-md-12 col-lg-12 col-sm-12">
        """)))})),format.raw/*10.10*/("""
      """),_display_(Seq[Any](/*11.8*/if(space.logoURL.isDefined)/*11.35*/ {_display_(Seq[Any](format.raw/*11.37*/("""
        <img class="space-banner-logo pull-left" src=""""),_display_(Seq[Any](/*12.56*/(space.logoURL))),format.raw/*12.71*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*12.92*/(space.name))),format.raw/*12.104*/("""">
        """)))})),format.raw/*13.10*/("""
    </div>
    </div>
    """)))})),format.raw/*16.6*/("""

  <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
      <h1 id="spacenamedisplay">"""),_display_(Seq[Any](/*20.34*/space/*20.39*/.name)),format.raw/*20.44*/("""</h1>
      <p><span id="spacedescdisplay">"""),_display_(Seq[Any](/*21.39*/space/*21.44*/.description)),format.raw/*21.56*/("""</span></p>
    </div>
  </div>
  <hr>

  """),_display_(Seq[Any](/*26.4*/spaces/*26.10*/.externalLinks(space.homePage, space, "row"))),format.raw/*26.54*/("""
  <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">

      <h3>Project Space Stats</h3>
      <p><span class="glyphicon glyphicon-user"></span> Members: """),_display_(Seq[Any](/*31.67*/space/*31.72*/.userCount)),format.raw/*31.82*/("""</p>
      <p><span class="glyphicon glyphicon-th-list"></span> Collections: """),_display_(Seq[Any](/*32.74*/space/*32.79*/.collectionCount)),format.raw/*32.95*/("""</p>
      <p><span class="glyphicon glyphicon-briefcase"></span> Datasets: """),_display_(Seq[Any](/*33.73*/space/*33.78*/.datasetCount)),format.raw/*33.91*/("""</p>

    <p>"""),_display_(Seq[Any](/*35.9*/msg)),format.raw/*35.12*/("""</p>
        """),_display_(Seq[Any](/*36.10*/if(!msg.contains("pending"))/*36.38*/ {_display_(Seq[Any](format.raw/*36.40*/("""
          """),_display_(Seq[Any](/*37.12*/spaces/*37.18*/.requestAuthorization(space.id))),format.raw/*37.49*/("""
        """)))})),format.raw/*38.10*/("""

      </div>
  </div>
""")))})),format.raw/*42.2*/("""
"""))}
    }
    
    def render(space:ProjectSpace,msg:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space,msg)(user)
    
    def f:((ProjectSpace,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space,msg) => (user) => apply(space,msg)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/publicView.scala.html
                    HASH: d2373761cb1f473d74427b35c258a088c133c705
                    MATRIX: 632->1|796->71|833->74|857->90|896->92|934->96|999->153|1037->154|1101->184|1138->213|1177->215|1318->320|1357->337|1390->353|1402->358|1440->359|1534->421|1577->429|1613->456|1653->458|1745->514|1782->529|1839->550|1874->562|1918->574|1977->602|2116->705|2130->710|2157->715|2237->759|2251->764|2285->776|2363->819|2378->825|2444->869|2651->1040|2665->1045|2697->1055|2811->1133|2825->1138|2863->1154|2976->1231|2990->1236|3025->1249|3074->1263|3099->1266|3149->1280|3186->1308|3226->1310|3274->1322|3289->1328|3342->1359|3384->1369|3440->1394
                    LINES: 20->1|23->1|25->3|25->3|25->3|26->4|26->4|26->4|28->6|28->6|28->6|29->7|29->7|30->8|30->8|30->8|32->10|33->11|33->11|33->11|34->12|34->12|34->12|34->12|35->13|38->16|42->20|42->20|42->20|43->21|43->21|43->21|48->26|48->26|48->26|53->31|53->31|53->31|54->32|54->32|54->32|55->33|55->33|55->33|57->35|57->35|58->36|58->36|58->36|59->37|59->37|59->37|60->38|64->42
                    -- GENERATED --
                */
            