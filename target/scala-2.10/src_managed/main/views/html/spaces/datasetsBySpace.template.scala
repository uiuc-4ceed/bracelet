
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object datasetsBySpace extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[List[models.Dataset],ProjectSpace,Option[Boolean],List[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(datasets: List[models.Dataset], space: ProjectSpace, isPublic: Option[Boolean], userSelections: List[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import api.Permission


Seq[Any](format.raw/*1.148*/("""
"""),_display_(Seq[Any](/*4.2*/isPublic/*4.10*/ match/*4.16*/ {/*5.5*/case Some(b) =>/*5.20*/ {_display_(Seq[Any](format.raw/*5.22*/("""<h3>"""),_display_(Seq[Any](/*5.27*/Messages("datasets.title"))),format.raw/*5.53*/("""</h3>""")))}/*6.5*/case None =>/*6.17*/ {_display_(Seq[Any](format.raw/*6.19*/("""<h3>"""),_display_(Seq[Any](/*6.24*/Messages("a.in.b", Messages("datasets.title"), Messages("space.title")))),format.raw/*6.95*/("""</h3>""")))}})),format.raw/*7.2*/("""

"""),_display_(Seq[Any](/*9.2*/if(datasets.size == 0)/*9.24*/ {_display_(Seq[Any](format.raw/*9.26*/("""
    """),_display_(Seq[Any](/*10.6*/if(user.isDefined && !isPublic.isDefined && !Permission.checkPermission(Permission.EditSpace, ResourceRef(ResourceRef.space, UUID(space.id.stringify))))/*10.158*/ {_display_(Seq[Any](format.raw/*10.160*/("""
        <p> Please request access to add """),_display_(Seq[Any](/*11.43*/Messages("datasets.title")/*11.69*/.toLowerCase)),format.raw/*11.81*/(""" to this """),_display_(Seq[Any](/*11.91*/Messages("space.title"))),format.raw/*11.114*/("""</p>
    """)))}/*12.7*/else/*12.12*/{_display_(Seq[Any](format.raw/*12.13*/("""
        <p> There are no """),_display_(Seq[Any](/*13.27*/Messages("datasets.title")/*13.53*/.toLowerCase)),format.raw/*13.65*/(""" associated with this """),_display_(Seq[Any](/*13.88*/Messages("space.title"))),format.raw/*13.111*/("""</p>
    """)))})),format.raw/*14.6*/("""
""")))}/*15.3*/else/*15.8*/{_display_(Seq[Any](format.raw/*15.9*/("""
    """),_display_(Seq[Any](/*16.6*/spaces/*16.12*/.datasetsGrid(datasets, space, isPublic, userSelections))),format.raw/*16.68*/("""
""")))})))}
    }
    
    def render(datasets:List[models.Dataset],space:ProjectSpace,isPublic:Option[Boolean],userSelections:List[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(datasets,space,isPublic,userSelections)(user)
    
    def f:((List[models.Dataset],ProjectSpace,Option[Boolean],List[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (datasets,space,isPublic,userSelections) => (user) => apply(datasets,space,isPublic,userSelections)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/datasetsBySpace.scala.html
                    HASH: 285eb3f9b4330fd23fd31f7865f5ae9d4e9dd43b
                    MATRIX: 680->1|974->147|1010->203|1026->211|1040->217|1049->224|1072->239|1111->241|1151->246|1198->272|1221->283|1241->295|1280->297|1320->302|1412->373|1449->381|1486->384|1516->406|1555->408|1596->414|1758->566|1799->568|1878->611|1913->637|1947->649|1993->659|2039->682|2067->693|2080->698|2119->699|2182->726|2217->752|2251->764|2310->787|2356->810|2397->820|2417->823|2429->828|2467->829|2508->835|2523->841|2601->897
                    LINES: 20->1|26->1|27->4|27->4|27->4|27->5|27->5|27->5|27->5|27->5|27->6|27->6|27->6|27->6|27->6|27->7|29->9|29->9|29->9|30->10|30->10|30->10|31->11|31->11|31->11|31->11|31->11|32->12|32->12|32->12|33->13|33->13|33->13|33->13|33->13|34->14|35->15|35->15|35->15|36->16|36->16|36->16
                    -- GENERATED --
                */
            