
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object newCopyTemplate extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[Form[controllers.spaceFormData],String,Html,Option[UUID],UUID,play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[controllers.spaceFormData], title:String, submitButton: Html, spaceId: Option[UUID],originalSpaceId : UUID)(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import helper._

import play.api.Play.current

import models.SpaceStatus

import api.Permission

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f)}};
Seq[Any](format.raw/*1.186*/("""
"""),format.raw/*4.74*/("""
"""),format.raw/*8.1*/("""
"""),_display_(Seq[Any](/*9.2*/main(title)/*9.13*/ {_display_(Seq[Any](format.raw/*9.15*/("""
    <ul class="breadcrumb">
        <li><a href= """"),_display_(Seq[Any](/*11.24*/routes/*11.30*/.Spaces.list())),format.raw/*11.44*/(""""> < Spaces</a></li>
    </ul>
    <div class="page-header">
        <h1>"""),_display_(Seq[Any](/*14.14*/title)),format.raw/*14.19*/("""</h1>
        <p>"""),_display_(Seq[Any](/*15.13*/Messages("space.create.message", Messages("spaces.title"), Messages("datasets.title")))),format.raw/*15.99*/("""</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*19.14*/if(myForm.hasErrors)/*19.34*/ {_display_(Seq[Any](format.raw/*19.36*/("""
                <div class="alert alert-error error">
                    <p>Please fix all errors</p>
                </div>
            """)))})),format.raw/*23.14*/("""
            """),_display_(Seq[Any](/*24.14*/flash/*24.19*/.get("error").map/*24.36*/ { message =>_display_(Seq[Any](format.raw/*24.49*/("""
                <div class="alert alert-error">
                    <p>"""),_display_(Seq[Any](/*26.25*/message)),format.raw/*26.32*/("""</p>
                </div>
            """)))})),format.raw/*28.14*/("""

            """),_display_(Seq[Any](/*30.14*/form(action = routes.Spaces.submitCopy, 'enctype -> "multipart/form-data", 'class -> "form-horizontal", 'id -> "spaceForm")/*30.137*/ {_display_(Seq[Any](format.raw/*30.139*/("""
                <div class="jumbotron">
                        <!--         <fieldset  id="nameDescrFieldSet">
 -->          """),_display_(Seq[Any](/*33.16*/inputText(myForm("name"), 'class -> "form-control", '_label -> "Name"))),format.raw/*33.86*/("""
                    """),_display_(Seq[Any](/*34.22*/textarea(myForm("description"), 'class -> "form-control", '_label -> "Description"))),format.raw/*34.105*/("""
                        <!--           <div class ="control-group"><label class="control-label" for="homePages">External Links</label></div>
          <div class = "homePages">
            """),_display_(Seq[Any](/*37.14*/repeat(myForm("homePages"), min = 1)/*37.50*/ {externalLink =>_display_(Seq[Any](format.raw/*37.67*/("""
                    """),_display_(Seq[Any](/*38.22*/inputText(externalLink, 'class -> "form-control", '_label -> ""))),format.raw/*38.86*/("""
                    """),format.raw/*43.28*/("""
                """)))})),format.raw/*44.18*/("""
                    <div class="external-link-add"><a href="#">add</a></div>
                </div>
                """),_display_(Seq[Any](/*47.18*/inputText(myForm("logoUrl"),'class -> "form-control", '_label-> "Logo URL"))),format.raw/*47.93*/("""
                Recommended image size is 100 x 100
                """),_display_(Seq[Any](/*49.18*/inputText(myForm("bannerUrl"),'class -> "form-control", '_label-> "Banner URL"))),format.raw/*49.97*/("""
                Recommended image size is 1170 x 200
                -->
                """),_display_(Seq[Any](/*52.18*/helper/*52.24*/.inputRadioGroup(myForm("expiration"), options("expiretrue"->"On", "expirefalse"->"Off"), '_label -> "Expiration"))),format.raw/*52.138*/("""
                """),_display_(Seq[Any](/*53.18*/defining(myForm("isTimeToLiveEnabled"))/*53.57*/ { isTimeToLiveEnabled =>_display_(Seq[Any](format.raw/*53.82*/("""
                    <input type="hidden" name="isTimeToLiveEnabled" id="isTimeToLiveEnabled", value = """),_display_(Seq[Any](/*54.104*/isTimeToLiveEnabled/*54.123*/.value)),format.raw/*54.129*/(""">
                """)))})),format.raw/*55.18*/("""
                """),_display_(Seq[Any](/*56.18*/helper/*56.24*/.inputText(myForm("editTime"), 'class -> "form-control", '_label -> "Time to live (hours):"))),format.raw/*56.116*/("""
                """),format.raw/*57.74*/("""
                <input type="hidden" name="originalId" id="originalId", value = """),_display_(Seq[Any](/*58.82*/originalSpaceId)),format.raw/*58.97*/(""">
                """),_display_(Seq[Any](/*59.18*/helper/*59.24*/.inputRadioGroup(myForm("access"),
                    options = options(SpaceStatus.PRIVATE.toString->SpaceStatus.PRIVATE.toString,SpaceStatus.PUBLIC.toString->SpaceStatus.PUBLIC.toString),
                    '_label -> "Access"))),format.raw/*61.41*/("""
                <hr />
                """),_display_(Seq[Any](/*63.18*/Messages("crate.space.access.message", Messages("collections.title").toLowerCase(), Messages("datasets.title").toLowerCase, Messages("space.title").toLowerCase, Messages("dataset.title").toLowerCase))),format.raw/*63.217*/("""
                <hr />
                """),_display_(Seq[Any](/*65.18*/helper/*65.24*/.input(myForm("space_id"), 'class -> "form-control", '_label ->"")/*65.90*/ {(id, name, value, args) =>_display_(Seq[Any](format.raw/*65.118*/("""
                    <input type="hidden" name=""""),_display_(Seq[Any](/*66.49*/id)),format.raw/*66.51*/("""" value=""""),_display_(Seq[Any](/*66.61*/value)),format.raw/*66.66*/("""">
                """)))})),format.raw/*67.18*/("""

                    <!--         </fieldset>
 -->        """),_display_(Seq[Any](/*70.14*/submitButton)),format.raw/*70.26*/("""
            </div>
        """)))})),format.raw/*72.10*/("""
    </div>
    </div>

    <script language="javascript">
    $(document).ready(function() """),format.raw/*77.34*/("""{"""),format.raw/*77.35*/("""
        var verified = """),_display_(Seq[Any](/*78.25*/play/*78.29*/.Play.application().configuration().getBoolean("verifySpaces"))),format.raw/*78.91*/(""";
        var publicEnabled = """),_display_(Seq[Any](/*79.30*/play/*79.34*/.Play.application().configuration().getBoolean("enablePublic"))),format.raw/*79.96*/(""";
        if( publicEnabled) """),format.raw/*80.28*/("""{"""),format.raw/*80.29*/("""

            if(verified)"""),format.raw/*82.25*/("""{"""),format.raw/*82.26*/("""
                //enableVerified == true, create a space or still unverified space
                if(typeof $("input[name='access']:checked").val() === "undefined" || $("input[name='access']:checked").val() === """"),_display_(Seq[Any](/*84.132*/SpaceStatus/*84.143*/.TRIAL.toString)),format.raw/*84.158*/("""")"""),format.raw/*84.160*/("""{"""),format.raw/*84.161*/("""

                    $('#access').closest(".control-group").hide();
                    $('#help_access').hide();
                    $('#access').append('<input type="radio" id="access_trial" name="access" value=""""),_display_(Seq[Any](/*88.102*/SpaceStatus/*88.113*/.TRIAL.toString)),format.raw/*88.128*/("""" checked>')
                """),format.raw/*89.17*/("""}"""),format.raw/*89.18*/("""
            """),format.raw/*90.13*/("""}"""),format.raw/*90.14*/(""" else"""),format.raw/*90.19*/("""{"""),format.raw/*90.20*/("""
                // enablePublic ==true  && enableVerified == false && update
                """),_display_(Seq[Any](/*92.18*/if(spaceId.isDefined && !Permission.checkPermission(Permission.PublicSpace, ResourceRef(ResourceRef.space, spaceId.get)))/*92.139*/{_display_(Seq[Any](format.raw/*92.140*/("""
                $('#access').closest(".control-group").hide();
                $('#help_access').hide();
                """)))})),format.raw/*95.18*/("""
                // enablePublic ==true  && enableVerified == false && create
                if(typeof $("input[name='access']:checked").val() === "undefined" )"""),format.raw/*97.84*/("""{"""),format.raw/*97.85*/("""
                    document.getElementById('access_"""),_display_(Seq[Any](/*98.54*/SpaceStatus/*98.65*/.PRIVATE.toString)),format.raw/*98.82*/("""').checked= true;
                """),format.raw/*99.17*/("""}"""),format.raw/*99.18*/("""
            """),format.raw/*100.13*/("""}"""),format.raw/*100.14*/("""
        """),format.raw/*101.9*/("""}"""),format.raw/*101.10*/(""" else """),format.raw/*101.16*/("""{"""),format.raw/*101.17*/("""
            // enablePublic == false
            $('#access').closest(".control-group").hide();
            $('#help_access').hide();
            document.getElementById('access_PRIVATE').checked= true;
        """),format.raw/*106.9*/("""}"""),format.raw/*106.10*/("""
    """),format.raw/*107.5*/("""}"""),format.raw/*107.6*/(""");
    // as fields are added, they get the same default homePages[0] name For automatic binding the names need to
    // be homePages[0], homePages[1],.. etc. This function renumbers all the fields appropriately even if a field in the
    // middle of the list is deleted.
    var renumberHomePageMap = function() """),format.raw/*111.42*/("""{"""),format.raw/*111.43*/("""
        Array.prototype.reduce.call($('.homePages .control-group'), function (i, next) """),format.raw/*112.88*/("""{"""),format.raw/*112.89*/("""
            var label = $(next).children('label');
            label.attr('for', label.attr('for').replace(/_\d+_/, '_' + i + '_'));

            var input = $(next).find('input');
            input.attr('id', input.attr('id').replace(/_\d+_/, '_' + i + '_'));
            input.attr('name', input.attr('name').replace(/\[\d+\]/, '[' + i + ']'));

            if (input.attr('id').match(/homePages_\d+_/)) """),format.raw/*120.59*/("""{"""),format.raw/*120.60*/("""
                return i + 1;
            """),format.raw/*122.13*/("""}"""),format.raw/*122.14*/("""

            return i;
        """),format.raw/*125.9*/("""}"""),format.raw/*125.10*/(""", 0);
    """),format.raw/*126.5*/("""}"""),format.raw/*126.6*/("""

    var addDeleteInCaseOfError = function() """),format.raw/*128.45*/("""{"""),format.raw/*128.46*/("""
        vals = $('.homePages .control-group')
        for(var i = 0; i < vals.length - 1 ; i++)
        """),format.raw/*131.9*/("""{"""),format.raw/*131.10*/("""
            $(vals[i]).after($('<div class="external-link-delete"><a href="#">delete</a></div>'));
        """),format.raw/*133.9*/("""}"""),format.raw/*133.10*/("""
    """),format.raw/*134.5*/("""}"""),format.raw/*134.6*/(""";

    addDeleteInCaseOfError();


    var title = $('.page-header h1').text();
    if(title.indexOf("Edit ") >= 0)
    """),format.raw/*141.5*/("""{"""),format.raw/*141.6*/("""
        if(!$('.help-inline').text())
        """),format.raw/*143.9*/("""{"""),format.raw/*143.10*/("""
            var timeToLive = $('#editTime').val();
            timeToLive = timeToLive/3600000;
            $('#editTime').val(timeToLive);
        """),format.raw/*147.9*/("""}"""),format.raw/*147.10*/("""
    """),format.raw/*148.5*/("""}"""),format.raw/*148.6*/("""
    else
    """),format.raw/*150.5*/("""{"""),format.raw/*150.6*/("""
        $('#editTime').val(720);
    """),format.raw/*152.5*/("""}"""),format.raw/*152.6*/("""


    var $radios = $('input:radio[name=expiration]');
    var isEnabled =  $('#isTimeToLiveEnabled').val();

    if (isEnabled === "true") """),format.raw/*158.31*/("""{"""),format.raw/*158.32*/("""
        $radios.filter('[value=expiretrue]').prop('checked', true);
        //$('input:radio[name=radiogroup]')[0].checked = true;
        $('#expireenabled').html("On");
    """),format.raw/*162.5*/("""}"""),format.raw/*162.6*/("""
    else """),format.raw/*163.10*/("""{"""),format.raw/*163.11*/("""
        $radios.filter('[value=expirefalse]').prop('checked', true);
        //$('input:radio[name=radiogroup]')[1].checked = true;
        $('#expireenabled').html("Off");
    """),format.raw/*167.5*/("""}"""),format.raw/*167.6*/("""
    $('.homePages').on('click', '.external-link-delete a', function () """),format.raw/*168.72*/("""{"""),format.raw/*168.73*/("""
        $(this).parent().prev().remove();
        $(this).parent().remove();
        renumberHomePageMap();
    """),format.raw/*172.5*/("""}"""),format.raw/*172.6*/(""");
    $('.control-label').addClass('h3');
    $('.external-link-add').click(function () """),format.raw/*174.47*/("""{"""),format.raw/*174.48*/("""
        var value = $(this).prev().clone();

        $(this).prev().after($('<div class="external-link-delete"><a href="#">delete</a></div>'));
        value.find('input').val('');
        $(this).before(value);
        renumberHomePageMap();
    """),format.raw/*181.5*/("""}"""),format.raw/*181.6*/(""");

    //remove blank lines before submitting
    $('form').submit(function () """),format.raw/*184.34*/("""{"""),format.raw/*184.35*/("""
        var expireEnabled = $('#expiration_expiretrue').prop('checked');
        $('#isTimeToLiveEnabled').val(expireEnabled);
        $('.homePages .control-group').each(function () """),format.raw/*187.57*/("""{"""),format.raw/*187.58*/("""
            var valueInput = $(this).find('input');

            if (valueInput.val() === '') """),format.raw/*190.42*/("""{"""),format.raw/*190.43*/("""
                $(this).next().remove();
                $(this).remove();
                renumberHomePageMap();
            """),format.raw/*194.13*/("""}"""),format.raw/*194.14*/("""

        """),format.raw/*196.9*/("""}"""),format.raw/*196.10*/(""");
    """),format.raw/*197.5*/("""}"""),format.raw/*197.6*/(""");

    $('#cancel_space').click(function() """),format.raw/*199.41*/("""{"""),format.raw/*199.42*/("""
        if(!"""),_display_(Seq[Any](/*200.14*/spaceId/*200.21*/.isEmpty)),format.raw/*200.29*/(""") """),format.raw/*200.31*/("""{"""),format.raw/*200.32*/("""
            window.location.replace(""""),_display_(Seq[Any](/*201.39*/routes/*201.45*/.Spaces.getSpace(spaceId.getOrElse(new UUID(""))))),format.raw/*201.94*/("""");
        """),format.raw/*202.9*/("""}"""),format.raw/*202.10*/(""" else """),format.raw/*202.16*/("""{"""),format.raw/*202.17*/("""
            window.location.replace(""""),_display_(Seq[Any](/*203.39*/routes/*203.45*/.Spaces.list())),format.raw/*203.59*/("""");
        """),format.raw/*204.9*/("""}"""),format.raw/*204.10*/("""

    """),format.raw/*206.5*/("""}"""),format.raw/*206.6*/(""");
    """),_display_(Seq[Any](/*207.6*/if(!play.api.Play.configuration.getBoolean("enable_expiration").getOrElse(false))/*207.87*/ {_display_(Seq[Any](format.raw/*207.89*/("""
    $("#expiration").closest(".control-group").hide();
    $("#editTime").closest(".control-group").hide();
    """)))})),format.raw/*210.6*/("""

    </script>
""")))})))}
    }
    
    def render(myForm:Form[controllers.spaceFormData],title:String,submitButton:Html,spaceId:Option[UUID],originalSpaceId:UUID,flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm,title,submitButton,spaceId,originalSpaceId)(flash,user)
    
    def f:((Form[controllers.spaceFormData],String,Html,Option[UUID],UUID) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm,title,submitButton,spaceId,originalSpaceId) => (flash,user) => apply(myForm,title,submitButton,spaceId,originalSpaceId)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/newCopyTemplate.scala.html
                    HASH: ddc3696291d3806710c9ee0312730f0daf551a4a
                    MATRIX: 698->1|1095->235|1127->259|1206->185|1234->307|1261->388|1297->390|1316->401|1355->403|1443->455|1458->461|1494->475|1604->549|1631->554|1685->572|1793->658|1912->741|1941->761|1981->763|2153->903|2203->917|2217->922|2243->939|2294->952|2403->1025|2432->1032|2505->1073|2556->1088|2689->1211|2730->1213|2894->1341|2986->1411|3044->1433|3150->1516|3377->1707|3422->1743|3477->1760|3535->1782|3621->1846|3670->2117|3720->2135|3874->2253|3971->2328|4077->2398|4178->2477|4305->2568|4320->2574|4457->2688|4511->2706|4559->2745|4622->2770|4763->2874|4792->2893|4821->2899|4872->2918|4926->2936|4941->2942|5056->3034|5101->3108|5219->3190|5256->3205|5311->3224|5326->3230|5579->3461|5656->3502|5878->3701|5955->3742|5970->3748|6045->3814|6112->3842|6197->3891|6221->3893|6267->3903|6294->3908|6346->3928|6442->3988|6476->4000|6537->4029|6657->4121|6686->4122|6747->4147|6760->4151|6844->4213|6911->4244|6924->4248|7008->4310|7065->4339|7094->4340|7148->4366|7177->4367|7429->4582|7450->4593|7488->4608|7519->4610|7549->4611|7802->4827|7823->4838|7861->4853|7918->4882|7947->4883|7988->4896|8017->4897|8050->4902|8079->4903|8210->4998|8341->5119|8381->5120|8536->5243|8725->5404|8754->5405|8844->5459|8864->5470|8903->5487|8965->5521|8994->5522|9036->5535|9066->5536|9103->5545|9133->5546|9168->5552|9198->5553|9438->5765|9468->5766|9501->5771|9530->5772|9874->6087|9904->6088|10021->6176|10051->6177|10487->6584|10517->6585|10589->6628|10619->6629|10679->6661|10709->6662|10747->6672|10776->6673|10851->6719|10881->6720|11014->6825|11044->6826|11180->6934|11210->6935|11243->6940|11272->6941|11420->7061|11449->7062|11524->7109|11554->7110|11731->7259|11761->7260|11794->7265|11823->7266|11865->7280|11894->7281|11960->7319|11989->7320|12159->7461|12189->7462|12393->7638|12422->7639|12461->7649|12491->7650|12697->7828|12726->7829|12827->7901|12857->7902|12998->8015|13027->8016|13145->8105|13175->8106|13451->8354|13480->8355|13589->8435|13619->8436|13832->8620|13862->8621|13986->8716|14016->8717|14172->8844|14202->8845|14240->8855|14270->8856|14305->8863|14334->8864|14407->8908|14437->8909|14488->8923|14505->8930|14536->8938|14567->8940|14597->8941|14673->8980|14689->8986|14761->9035|14801->9047|14831->9048|14866->9054|14896->9055|14972->9094|14988->9100|15025->9114|15065->9126|15095->9127|15129->9133|15158->9134|15202->9142|15293->9223|15334->9225|15480->9339
                    LINES: 20->1|31->4|31->4|32->1|33->4|34->8|35->9|35->9|35->9|37->11|37->11|37->11|40->14|40->14|41->15|41->15|45->19|45->19|45->19|49->23|50->24|50->24|50->24|50->24|52->26|52->26|54->28|56->30|56->30|56->30|59->33|59->33|60->34|60->34|63->37|63->37|63->37|64->38|64->38|65->43|66->44|69->47|69->47|71->49|71->49|74->52|74->52|74->52|75->53|75->53|75->53|76->54|76->54|76->54|77->55|78->56|78->56|78->56|79->57|80->58|80->58|81->59|81->59|83->61|85->63|85->63|87->65|87->65|87->65|87->65|88->66|88->66|88->66|88->66|89->67|92->70|92->70|94->72|99->77|99->77|100->78|100->78|100->78|101->79|101->79|101->79|102->80|102->80|104->82|104->82|106->84|106->84|106->84|106->84|106->84|110->88|110->88|110->88|111->89|111->89|112->90|112->90|112->90|112->90|114->92|114->92|114->92|117->95|119->97|119->97|120->98|120->98|120->98|121->99|121->99|122->100|122->100|123->101|123->101|123->101|123->101|128->106|128->106|129->107|129->107|133->111|133->111|134->112|134->112|142->120|142->120|144->122|144->122|147->125|147->125|148->126|148->126|150->128|150->128|153->131|153->131|155->133|155->133|156->134|156->134|163->141|163->141|165->143|165->143|169->147|169->147|170->148|170->148|172->150|172->150|174->152|174->152|180->158|180->158|184->162|184->162|185->163|185->163|189->167|189->167|190->168|190->168|194->172|194->172|196->174|196->174|203->181|203->181|206->184|206->184|209->187|209->187|212->190|212->190|216->194|216->194|218->196|218->196|219->197|219->197|221->199|221->199|222->200|222->200|222->200|222->200|222->200|223->201|223->201|223->201|224->202|224->202|224->202|224->202|225->203|225->203|225->203|226->204|226->204|228->206|228->206|229->207|229->207|229->207|232->210
                    -- GENERATED --
                */
            