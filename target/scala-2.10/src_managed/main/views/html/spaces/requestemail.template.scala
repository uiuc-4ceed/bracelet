
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object requestemail extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[models.User,String,String,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(user:models.User, spaceid: String, spacename:String)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider

import play.api.i18n.Messages


Seq[Any](format.raw/*1.88*/("""
"""),format.raw/*4.1*/("""<html>
  <body>
    <p>Hello,</p>

    <p>
      <a href=""""),_display_(Seq[Any](/*9.17*/routes/*9.23*/.Profile.viewProfileUUID(user.id).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.97*/("""">"""),_display_(Seq[Any](/*9.100*/user/*9.104*/.fullName)),format.raw/*9.113*/("""</a> submitted an authorization request to your """),_display_(Seq[Any](/*9.162*/Messages("space.title"))),format.raw/*9.185*/(""" "<a href=""""),_display_(Seq[Any](/*9.197*/routes/*9.203*/.Spaces.getSpace(UUID(spaceid)).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.275*/("""">"""),_display_(Seq[Any](/*9.278*/spacename)),format.raw/*9.287*/("""</a>"
      at <a href=""""),_display_(Seq[Any](/*10.20*/routes/*10.26*/.Application.index().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*10.87*/("""">"""),_display_(Seq[Any](/*10.90*/services/*10.98*/.AppConfiguration.getDisplayName)),format.raw/*10.130*/("""</a>.
    </p>
  </body>
</html>"""))}
    }
    
    def render(user:models.User,spaceid:String,spacename:String,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(user,spaceid,spacename)(request)
    
    def f:((models.User,String,String) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (user,spaceid,spacename) => (request) => apply(user,spaceid,spacename)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/requestemail.scala.html
                    HASH: a226f5f0944a29c913797e3597fb62b9a8574e92
                    MATRIX: 634->1|887->87|914->162|1008->221|1022->227|1117->301|1156->304|1169->308|1200->317|1285->366|1330->389|1378->401|1393->407|1487->479|1526->482|1557->491|1618->516|1633->522|1716->583|1755->586|1772->594|1827->626
                    LINES: 20->1|26->1|27->4|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|32->9|33->10|33->10|33->10|33->10|33->10|33->10
                    -- GENERATED --
                */
            