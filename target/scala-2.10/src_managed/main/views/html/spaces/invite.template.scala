
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object invite extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[Form[controllers.spaceInviteData],ProjectSpace,List[String],List[SpaceInvite],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[controllers.spaceInviteData], space: ProjectSpace, roleList: List[String], invite:List[SpaceInvite])(implicit  flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import play.api.i18n.Messages


Seq[Any](format.raw/*1.180*/("""
    """),format.raw/*3.1*/("""    """),format.raw/*4.1*/("""    <div class="container row">
        <div class="col-md-8">

            <div class="tab-header">
                <h3>Invite new users</h3>
            </div>

            """),_display_(Seq[Any](/*11.14*/form(action = routes.Spaces.inviteToSpace(space.id), 'id -> "invitecreate", 'enctype -> "multipart/form-data", 'class -> "form-horizontal", 'autocomplete -> "off", 'method -> "POST")/*11.196*/ {_display_(Seq[Any](format.raw/*11.198*/("""
                <fieldset class="email-new-users">
                    """),_display_(Seq[Any](/*13.22*/inputText(myForm("addressList"),'class->"form-control", 'id -> "addressList", '_label -> "Email addresses (comma-separated)", 'type -> "Email", 'multiple -> "true"))),format.raw/*13.186*/("""
                    <span class="error hiddencomplete" id="addresserror"> Please enter at least one email address.<br></span>

                    """),_display_(Seq[Any](/*16.22*/select(myForm("role"), options(roleList), 'id -> "role_list", '_label -> "Role", 'placeholder -> "Select a default role for all recipients"))),format.raw/*16.162*/("""
                    """),_display_(Seq[Any](/*17.22*/textarea(myForm("message"), 'class->"form-control", '_label -> "Message"))),format.raw/*17.95*/("""


                </fieldset>
                <div type="hidden" class="form-actions">
                    <button class="btn btn-primary" id="submitbutton"><span class="glyphicon glyphicon-envelope"></span> Send</button>
                </div>
            """)))})),format.raw/*24.14*/("""
            <button class="btn btn-primary" id="invitebutton"><span class="glyphicon glyphicon-envelope"></span> Send</button>

        </div>
        <div class="col-md-4">

            <br />
            <h3>Invitations to """),_display_(Seq[Any](/*31.33*/Messages("space.title"))),format.raw/*31.56*/(""" """),_display_(Seq[Any](/*31.58*/space/*31.63*/.name)),format.raw/*31.68*/("""</h3>
            """),_display_(Seq[Any](/*32.14*/for(v <- invite) yield /*32.30*/{_display_(Seq[Any](format.raw/*32.31*/("""
                <li>
                    """),_display_(Seq[Any](/*34.22*/v/*34.23*/.email)),format.raw/*34.29*/(""" as """),_display_(Seq[Any](/*34.34*/v/*34.35*/.role)),format.raw/*34.40*/("""
                </li>
            """)))})),format.raw/*36.14*/("""
        </div>
    </div>
    <script src=""""),_display_(Seq[Any](/*39.19*/routes/*39.25*/.Assets.at("javascripts/chosen.jquery.js"))),format.raw/*39.67*/("""" type="text/javascript"></script>
    <script language="javascript">

$('#submitbutton').hide();

$('#invitebutton').click(function() """),format.raw/*44.37*/("""{"""),format.raw/*44.38*/("""
    $('#addresserror').hide();
    if(!$('#addressList').val())"""),format.raw/*46.33*/("""{"""),format.raw/*46.34*/("""
        $('#addresserror').show();
        console.log("no email");
    """),format.raw/*49.5*/("""}"""),format.raw/*49.6*/("""else"""),format.raw/*49.10*/("""{"""),format.raw/*49.11*/("""


    var addrList = $('#addressList').val().split(',');
    for(var i = 0; i < addrList.length; i++)"""),format.raw/*53.45*/("""{"""),format.raw/*53.46*/("""
        $('form fieldset').append('<input type="hidden" name="addresses['+i+']" value='+addrList[i]+' />');
    """),format.raw/*55.5*/("""}"""),format.raw/*55.6*/("""
   $('#submitbutton').trigger("click" );
    """),format.raw/*57.5*/("""}"""),format.raw/*57.6*/("""
"""),format.raw/*58.1*/("""}"""),format.raw/*58.2*/(""");



</script>

    <style type="text/css">
    textarea """),format.raw/*65.14*/("""{"""),format.raw/*65.15*/("""
    width: 100%;
    height: 100px;
    """),format.raw/*68.5*/("""}"""),format.raw/*68.6*/("""
    input#addressList"""),format.raw/*69.22*/("""{"""),format.raw/*69.23*/("""
    width: 100%
    """),format.raw/*71.5*/("""}"""),format.raw/*71.6*/("""
    </style>
"""))}
    }
    
    def render(myForm:Form[controllers.spaceInviteData],space:ProjectSpace,roleList:List[String],invite:List[SpaceInvite],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm,space,roleList,invite)(flash,user)
    
    def f:((Form[controllers.spaceInviteData],ProjectSpace,List[String],List[SpaceInvite]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm,space,roleList,invite) => (flash,user) => apply(myForm,space,roleList,invite)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/invite.scala.html
                    HASH: 591f41d0f5322a723c321c37ed2ea75ed73f2f52
                    MATRIX: 705->1|1025->179|1056->201|1086->236|1298->412|1490->594|1531->596|1640->669|1827->833|2012->982|2175->1122|2233->1144|2328->1217|2619->1476|2882->1703|2927->1726|2965->1728|2979->1733|3006->1738|3061->1757|3093->1773|3132->1774|3211->1817|3221->1818|3249->1824|3290->1829|3300->1830|3327->1835|3395->1871|3476->1916|3491->1922|3555->1964|3718->2099|3747->2100|3839->2164|3868->2165|3968->2238|3996->2239|4028->2243|4057->2244|4187->2346|4216->2347|4356->2460|4384->2461|4457->2507|4485->2508|4513->2509|4541->2510|4627->2568|4656->2569|4724->2610|4752->2611|4802->2633|4831->2634|4879->2655|4907->2656
                    LINES: 20->1|26->1|27->3|27->4|34->11|34->11|34->11|36->13|36->13|39->16|39->16|40->17|40->17|47->24|54->31|54->31|54->31|54->31|54->31|55->32|55->32|55->32|57->34|57->34|57->34|57->34|57->34|57->34|59->36|62->39|62->39|62->39|67->44|67->44|69->46|69->46|72->49|72->49|72->49|72->49|76->53|76->53|78->55|78->55|80->57|80->57|81->58|81->58|88->65|88->65|91->68|91->68|92->69|92->69|94->71|94->71
                    -- GENERATED --
                */
            