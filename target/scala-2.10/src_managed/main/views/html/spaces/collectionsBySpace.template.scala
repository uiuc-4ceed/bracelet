
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object collectionsBySpace extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[List[Collection],ProjectSpace,Option[Boolean],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(collections: List[Collection], space: ProjectSpace, isPublic: Option[Boolean])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.i18n.Messages


Seq[Any](format.raw/*1.117*/("""
"""),_display_(Seq[Any](/*4.2*/isPublic/*4.10*/ match/*4.16*/ {/*5.5*/case Some(b) =>/*5.20*/ {_display_(Seq[Any](format.raw/*5.22*/("""<h3>"""),_display_(Seq[Any](/*5.27*/Messages("collections.title"))),format.raw/*5.56*/("""</h3>""")))}/*6.5*/case None =>/*6.17*/ {_display_(Seq[Any](format.raw/*6.19*/("""<h3>"""),_display_(Seq[Any](/*6.24*/Messages("a.in.b", Messages("collections.title"), Messages("space.title")))),format.raw/*6.98*/("""</h3>""")))}})),format.raw/*7.2*/("""

"""),_display_(Seq[Any](/*9.2*/if(collections.size == 0)/*9.27*/ {_display_(Seq[Any](format.raw/*9.29*/("""
    """),_display_(Seq[Any](/*10.6*/if(user.isDefined && !Permission.checkPermission(Permission.EditSpace, ResourceRef(ResourceRef.space, UUID(space.id.stringify))))/*10.135*/ {_display_(Seq[Any](format.raw/*10.137*/("""
        <p>Please request access to add collections to this """),_display_(Seq[Any](/*11.62*/Messages("space.title"))),format.raw/*11.85*/("""</p>
    """)))}/*12.7*/else/*12.12*/{_display_(Seq[Any](format.raw/*12.13*/("""
        <p>There are no collections associated with this """),_display_(Seq[Any](/*13.59*/Messages("space.title"))),format.raw/*13.82*/(""". </p>
    """)))})),format.raw/*14.6*/("""
""")))}/*15.3*/else/*15.8*/{_display_(Seq[Any](format.raw/*15.9*/("""
    """),_display_(Seq[Any](/*16.6*/spaces/*16.12*/.collectionsGrid(collections, space))),format.raw/*16.48*/("""
""")))})))}
    }
    
    def render(collections:List[Collection],space:ProjectSpace,isPublic:Option[Boolean],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(collections,space,isPublic)(user)
    
    def f:((List[Collection],ProjectSpace,Option[Boolean]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (collections,space,isPublic) => (user) => apply(collections,space,isPublic)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/collectionsBySpace.scala.html
                    HASH: 52faf169d8529f2d972128781e53f8b01688976f
                    MATRIX: 666->1|929->116|965->172|981->180|995->186|1004->193|1027->208|1066->210|1106->215|1156->244|1179->255|1199->267|1238->269|1278->274|1373->348|1410->356|1447->359|1480->384|1519->386|1560->392|1699->521|1740->523|1838->585|1883->608|1911->619|1924->624|1963->625|2058->684|2103->707|2146->719|2166->722|2178->727|2216->728|2257->734|2272->740|2330->776
                    LINES: 20->1|26->1|27->4|27->4|27->4|27->5|27->5|27->5|27->5|27->5|27->6|27->6|27->6|27->6|27->6|27->7|29->9|29->9|29->9|30->10|30->10|30->10|31->11|31->11|32->12|32->12|32->12|33->13|33->13|34->14|35->15|35->15|35->15|36->16|36->16|36->16
                    -- GENERATED --
                */
            