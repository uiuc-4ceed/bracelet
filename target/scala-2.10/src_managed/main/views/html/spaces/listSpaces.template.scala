
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listSpaces extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template14[List[models.ProjectSpace],String,String,Int,Option[String],Option[String],Boolean,Option[String],String,String,Option[String],Boolean,Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(spacesList: List[models.ProjectSpace], when: String, date: String, limit: Int, owner: Option[String], ownerName: Option[String], showAll: Boolean, mode: Option[String], prev: String, next: String, title: Option[String], showPublic: Boolean, onlyTrial: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages


Seq[Any](format.raw/*1.299*/("""
"""),_display_(Seq[Any](/*3.2*/main(Messages("spaces.title"))/*3.32*/ {_display_(Seq[Any](format.raw/*3.34*/("""

        """),_display_(Seq[Any](/*5.10*/util/*5.14*/.masonry())),format.raw/*5.24*/("""

        <script src=""""),_display_(Seq[Any](/*7.23*/routes/*7.29*/.Assets.at("javascripts/spaceListProcess.js"))),format.raw/*7.74*/("""" type="text/javascript"></script>
        <script src=""""),_display_(Seq[Any](/*8.23*/routes/*8.29*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*8.69*/("""" type="text/javascript"></script>
        <script src=""""),_display_(Seq[Any](/*9.23*/routes/*9.29*/.Assets.at("javascripts/jquery.cookie.js"))),format.raw/*9.71*/("""" type="text/javascript"></script>
        <script src=""""),_display_(Seq[Any](/*10.23*/routes/*10.29*/.Assets.at("javascripts/follow-button.js"))),format.raw/*10.71*/("""" type="text/javascript"></script>
        <script src=""""),_display_(Seq[Any](/*11.23*/routes/*11.29*/.Assets.at("javascripts/displayPanels.js"))),format.raw/*11.71*/("""" type="text/javascript"></script>
       
    <div class="row">
        <ol class="breadcrumb">
            """),_display_(Seq[Any](/*15.14*/(owner, ownerName)/*15.32*/ match/*15.38*/ {/*16.17*/case (Some(o), Some(n)) =>/*16.43*/ {_display_(Seq[Any](format.raw/*16.45*/("""
                    <li> <span class="glyphicon glyphicon-user"></span> <a href=""""),_display_(Seq[Any](/*17.83*/routes/*17.89*/.Profile.viewProfileUUID(UUID(o)))),format.raw/*17.122*/(""""> """),_display_(Seq[Any](/*17.126*/n)),format.raw/*17.127*/("""</a></li>
                """)))}/*19.17*/case (_, _) =>/*19.31*/ {}})),format.raw/*20.14*/("""


            <li><span class="glyphicon glyphicon-hdd"> </span> """),_display_(Seq[Any](/*23.65*/Messages("spaces.title"))),format.raw/*23.89*/("""</li>
        </ol>
    </div>
        <div class="row">
            <div class="col-md-12">
                <h1>"""),_display_(Seq[Any](/*28.22*/title)),format.raw/*28.27*/("""</h1>
                <p>"""),_display_(Seq[Any](/*29.21*/Messages("space.list.message", Messages("spaces.title")))),format.raw/*29.77*/("""</p>
            </div>
        </div>


        <div class="row">
            <div class="btn-toolbar">
                <div class="btn-group btn-group-sm pull-right">
                    <button type="button" class="btn btn-default active" href="#tile-view" id="tile-view-btn"><span class="glyphicon glyphicon-th-large"></span></button>
                    <button type="button" class="btn btn-default" href="#list-view" id="list-view-btn"><span class="glyphicon glyphicon-th-list"></span></button>
                </div>
        	<div class="btn-group btn-group-sm pull-right" id="number-displayed-dropdown">
            		<select id="numPageItems" class="form-control" onchange="getValue()">
                		<option value="12">12</option>
                		<option value="24">24</option>
                		<option value="48">48</option>
                		<option value="96">96</option>
            		</select>
        	</div>

                """),_display_(Seq[Any](/*49.18*/(user, owner)/*49.31*/ match/*49.37*/ {/*50.21*/case (Some(u), Some(o)) =>/*50.47*/ {_display_(Seq[Any](format.raw/*50.49*/("""
                        """),_display_(Seq[Any](/*51.26*/if(o.equalsIgnoreCase(u.id.stringify))/*51.64*/ {_display_(Seq[Any](format.raw/*51.66*/("""
                            <a class="btn btn-primary btn-sm pull-right" id="create-space" href=""""),_display_(Seq[Any](/*52.99*/routes/*52.105*/.Spaces.newSpace)),format.raw/*52.121*/("""" title="Create a new """),_display_(Seq[Any](/*52.144*/Messages("space.title"))),format.raw/*52.167*/(""""><span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*52.215*/Messages("create.title", ""))),format.raw/*52.243*/("""</a>
                        """)))})),format.raw/*53.26*/("""
                    """)))}/*55.21*/case (Some(u), _) =>/*55.41*/ {_display_(Seq[Any](format.raw/*55.43*/("""

                        <a class="btn btn-primary btn-sm pull-right" id="create-space" href=""""),_display_(Seq[Any](/*57.95*/routes/*57.101*/.Spaces.newSpace)),format.raw/*57.117*/("""" title="Create a new """),_display_(Seq[Any](/*57.140*/Messages("space.title"))),format.raw/*57.163*/(""""><span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*57.211*/Messages("create.title", ""))),format.raw/*57.239*/("""</a>

                    """)))}/*60.21*/case (_,_) =>/*60.34*/ {}})),format.raw/*62.18*/("""
            </div>
        </div>
                <script>
                var removeIndicator = false;
                var viewMode = '"""),_display_(Seq[Any](/*67.34*/mode/*67.38*/.getOrElse("tile"))),format.raw/*67.56*/("""';
                $.cookie.raw = true;
                $.cookie.json = true;
                $(function() """),format.raw/*70.30*/("""{"""),format.raw/*70.31*/("""
                    $('#tile-view-btn').click(function() """),format.raw/*71.58*/("""{"""),format.raw/*71.59*/("""
                        $('#tile-view').removeClass ('hidden');
                        $('#list-view').addClass ('hidden');
                        $('#tile-view-btn').addClass('active');
                        $('#list-view-btn').removeClass ('active');
                        viewMode = "tile";
                        updatePage();
                        $.cookie('view-mode', 'tile', """),format.raw/*78.55*/("""{"""),format.raw/*78.56*/(""" path: '/' """),format.raw/*78.67*/("""}"""),format.raw/*78.68*/(""");
                        $('#masonry').masonry().masonry( """),format.raw/*79.58*/("""{"""),format.raw/*79.59*/("""
                            itemSelector : '.post-box',
                            columnWidth : '.post-box',
                            transitionDuration : 4
                        """),format.raw/*83.25*/("""}"""),format.raw/*83.26*/(""");
                    """),format.raw/*84.21*/("""}"""),format.raw/*84.22*/(""");
                    $('#list-view-btn').click(function() """),format.raw/*85.58*/("""{"""),format.raw/*85.59*/("""
                        $('#tile-view').addClass('hidden');
                        $('#list-view' ).removeClass('hidden');
                        $('#list-view-btn').addClass('active');
                        $('#tile-view-btn').removeClass('active');
                        viewMode = "list" ;
                        updatePage();
                        //Utilizing library from https://github.com/carhartl/jquery-cookie/tree/v1.4.1
                        $.cookie("view-mode", "list", """),format.raw/*93.55*/("""{"""),format.raw/*93.56*/(""" path: '/' """),format.raw/*93.67*/("""}"""),format.raw/*93.68*/(""");
                    """),format.raw/*94.21*/("""}"""),format.raw/*94.22*/(""");

                """),format.raw/*96.17*/("""}"""),format.raw/*96.18*/(""");

                $(document).ready (function () """),format.raw/*98.48*/("""{"""),format.raw/*98.49*/("""
                    //Set the cookie, for the case when it is passed in by the parameter
                    $.cookie("view-mode", viewMode, """),format.raw/*100.53*/("""{"""),format.raw/*100.54*/(""" path: '/' """),format.raw/*100.65*/("""}"""),format.raw/*100.66*/(""");
                    if (viewMode == "list") """),format.raw/*101.45*/("""{"""),format.raw/*101.46*/("""
                        $('#tile-view').addClass('hidden');
                        $('#list-view').removeClass('hidden');
                        $('#list-view-btn').addClass('active');
                        $('#tile-view-btn').removeClass('active');
                    """),format.raw/*106.21*/("""}"""),format.raw/*106.22*/("""
                    else """),format.raw/*107.26*/("""{"""),format.raw/*107.27*/("""
                        $('#tile-view').removeClass('hidden');
                        $('#list-view').addClass('hidden');
                        $('#tile-view-btn').addClass('active');
                        $('#list-view-btn').removeClass('active');
                    """),format.raw/*112.21*/("""}"""),format.raw/*112.22*/("""
                    updatePage();
                """),format.raw/*114.17*/("""}"""),format.raw/*114.18*/(""");

                //Function to unify the changing of the href for the next/previous links. Called on button activation for
                //viewMode style, as well as on initial load of page.
                function updatePage() """),format.raw/*118.39*/("""{"""),format.raw/*118.40*/("""
                    $('#nextlink').attr('href', """"),_display_(Seq[Any](/*119.51*/(routes.Spaces.list("a", next, limit, "", owner, showAll, showPublic, onlyTrial)))),format.raw/*119.132*/("""");
                    $('#prevlink').attr('href', """"),_display_(Seq[Any](/*120.51*/(routes.Spaces.list("b", prev, limit, "", owner, showAll, showPublic, onlyTrial)))),format.raw/*120.132*/("""");
                    doSummarizeAbstracts();
                """),format.raw/*122.17*/("""}"""),format.raw/*122.18*/("""
            </script>

        <div class="row hidden" id="tile-view">
            <div class="col-md-12">
                <div id="masonry">
                    """),_display_(Seq[Any](/*128.22*/spacesList/*128.32*/.map/*128.36*/ { space =>_display_(Seq[Any](format.raw/*128.47*/("""
                        """),_display_(Seq[Any](/*129.26*/spaces/*129.32*/.tile(space, "col-lg-3 col-md-3 col-sm-3 col-xs-12",  routes.Spaces.list(when, date, limit, "", owner, showAll, showPublic, onlyTrial), false))),format.raw/*129.174*/("""
                    """)))})),format.raw/*130.22*/("""
                </div>
                </div>
            </div>

            <div class="row hidden" id="list-view">
                <div  class="col-md-12">
                    """),_display_(Seq[Any](/*137.22*/spacesList/*137.32*/.map/*137.36*/ { space =>_display_(Seq[Any](format.raw/*137.47*/("""
                        """),_display_(Seq[Any](/*138.26*/spaces/*138.32*/.listItem(space, (routes.Spaces.list(when, date, limit, "", owner, showAll, showPublic, onlyTrial)) ))),format.raw/*138.133*/("""
                    """)))})),format.raw/*139.22*/("""
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <ul class="pager">
                        """),_display_(Seq[Any](/*146.26*/if(prev != "")/*146.40*/ {_display_(Seq[Any](format.raw/*146.42*/("""
                            <li class="previous"><a class="btn btn-link" id="prevlink" title="Page backwards" href="#"><span class="glyphicon glyphicon-chevron-left"></span> Previous</a></li>
                        """)))})),format.raw/*148.26*/("""
                        """),_display_(Seq[Any](/*149.26*/if(next != "")/*149.40*/ {_display_(Seq[Any](format.raw/*149.42*/("""
                            <li class ="next"><a class="btn btn-link" id="nextlink" title="Page forwards" href="#">Next <span class="glyphicon glyphicon-chevron-right"></span></a></li>
                        """)))})),format.raw/*151.26*/("""
                    </ul>
                </div>
            </div>
		<script src=""""),_display_(Seq[Any](/*155.17*/routes/*155.23*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*155.68*/("""" type="text/javascript"></script>
        <script src=""""),_display_(Seq[Any](/*156.23*/routes/*156.29*/.Assets.at("javascripts/descriptionSummary.js"))),format.raw/*156.76*/("""" type="text/javascript"></script>
""")))})),format.raw/*157.2*/("""
"""))}
    }
    
    def render(spacesList:List[models.ProjectSpace],when:String,date:String,limit:Int,owner:Option[String],ownerName:Option[String],showAll:Boolean,mode:Option[String],prev:String,next:String,title:Option[String],showPublic:Boolean,onlyTrial:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(spacesList,when,date,limit,owner,ownerName,showAll,mode,prev,next,title,showPublic,onlyTrial)(user)
    
    def f:((List[models.ProjectSpace],String,String,Int,Option[String],Option[String],Boolean,Option[String],String,String,Option[String],Boolean,Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (spacesList,when,date,limit,owner,ownerName,showAll,mode,prev,next,title,showPublic,onlyTrial) => (user) => apply(spacesList,when,date,limit,owner,ownerName,showAll,mode,prev,next,title,showPublic,onlyTrial)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/listSpaces.scala.html
                    HASH: 05f7f3825663b28f4832f11cfe5b9ef717171bcd
                    MATRIX: 755->1|1177->298|1213->331|1251->361|1290->363|1336->374|1348->378|1379->388|1438->412|1452->418|1518->463|1610->520|1624->526|1685->566|1777->623|1791->629|1854->671|1947->728|1962->734|2026->776|2119->833|2134->839|2198->881|2344->991|2371->1009|2386->1015|2397->1034|2432->1060|2472->1062|2591->1145|2606->1151|2662->1184|2703->1188|2727->1189|2773->1233|2796->1247|2822->1264|2925->1331|2971->1355|3121->1469|3148->1474|3210->1500|3288->1556|4274->2506|4296->2519|4311->2525|4322->2548|4357->2574|4397->2576|4459->2602|4506->2640|4546->2642|4681->2741|4697->2747|4736->2763|4796->2786|4842->2809|4927->2857|4978->2885|5040->2915|5081->2958|5110->2978|5150->2980|5282->3076|5298->3082|5337->3098|5397->3121|5443->3144|5528->3192|5579->3220|5625->3268|5647->3281|5673->3303|5847->3441|5860->3445|5900->3463|6035->3570|6064->3571|6150->3629|6179->3630|6600->4023|6629->4024|6668->4035|6697->4036|6785->4096|6814->4097|7029->4284|7058->4285|7109->4308|7138->4309|7226->4369|7255->4370|7778->4865|7807->4866|7846->4877|7875->4878|7926->4901|7955->4902|8003->4922|8032->4923|8111->4974|8140->4975|8311->5117|8341->5118|8381->5129|8411->5130|8487->5177|8517->5178|8821->5453|8851->5454|8906->5480|8936->5481|9240->5756|9270->5757|9350->5808|9380->5809|9643->6043|9673->6044|9761->6095|9866->6176|9957->6230|10062->6311|10155->6375|10185->6376|10386->6540|10406->6550|10420->6554|10470->6565|10533->6591|10549->6597|10715->6739|10770->6761|10988->6942|11008->6952|11022->6956|11072->6967|11135->6993|11151->6999|11276->7100|11331->7122|11546->7300|11570->7314|11611->7316|11862->7534|11925->7560|11949->7574|11990->7576|12234->7787|12356->7872|12372->7878|12440->7923|12534->7980|12550->7986|12620->8033|12688->8069
                    LINES: 20->1|24->1|25->3|25->3|25->3|27->5|27->5|27->5|29->7|29->7|29->7|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10|33->11|33->11|33->11|37->15|37->15|37->15|37->16|37->16|37->16|38->17|38->17|38->17|38->17|38->17|39->19|39->19|39->20|42->23|42->23|47->28|47->28|48->29|48->29|68->49|68->49|68->49|68->50|68->50|68->50|69->51|69->51|69->51|70->52|70->52|70->52|70->52|70->52|70->52|70->52|71->53|72->55|72->55|72->55|74->57|74->57|74->57|74->57|74->57|74->57|74->57|76->60|76->60|76->62|81->67|81->67|81->67|84->70|84->70|85->71|85->71|92->78|92->78|92->78|92->78|93->79|93->79|97->83|97->83|98->84|98->84|99->85|99->85|107->93|107->93|107->93|107->93|108->94|108->94|110->96|110->96|112->98|112->98|114->100|114->100|114->100|114->100|115->101|115->101|120->106|120->106|121->107|121->107|126->112|126->112|128->114|128->114|132->118|132->118|133->119|133->119|134->120|134->120|136->122|136->122|142->128|142->128|142->128|142->128|143->129|143->129|143->129|144->130|151->137|151->137|151->137|151->137|152->138|152->138|152->138|153->139|160->146|160->146|160->146|162->148|163->149|163->149|163->149|165->151|169->155|169->155|169->155|170->156|170->156|170->156|171->157
                    -- GENERATED --
                */
            