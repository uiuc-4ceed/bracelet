
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object requestAuthorization extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[UUID,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(id: UUID)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.48*/("""

    """),format.raw/*3.67*/("""
    <form action=""""),_display_(Seq[Any](/*4.20*/(routes.Spaces.addRequest(id)))),format.raw/*4.50*/("""" method="GET">
       <input type="submit" value="Request access" class="btn btn-link btn-xs"/>
    </form>"""))}
    }
    
    def render(id:UUID,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(id)(user)
    
    def f:((UUID) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (id) => (user) => apply(id)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/requestAuthorization.scala.html
                    HASH: 10ab3a0d4d0fae6b9170a0d12eb461afdf8824dd
                    MATRIX: 627->1|767->47|800->115|855->135|906->165
                    LINES: 20->1|23->1|25->3|26->4|26->4
                    -- GENERATED --
                */
            