
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object editSpace extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template5[Form[controllers.spaceFormData],Option[UUID],Option[String],play.api.mvc.Flash,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[controllers.spaceFormData], spaceId: Option[UUID], spaceName:Option[String])(implicit flash: play.api.mvc.Flash, user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages


Seq[Any](format.raw/*1.155*/("""
    """),format.raw/*3.1*/("""    """),_display_(Seq[Any](/*3.6*/newEditTemplate(myForm, title = "Edit " + Messages("space.title"), submitButton = updateButton(), spaceId, spaceName))))}
    }
    
    def render(myForm:Form[controllers.spaceFormData],spaceId:Option[UUID],spaceName:Option[String],flash:play.api.mvc.Flash,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm,spaceId,spaceName)(flash,user)
    
    def f:((Form[controllers.spaceFormData],Option[UUID],Option[String]) => (play.api.mvc.Flash,Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm,spaceId,spaceName) => (flash,user) => apply(myForm,spaceId,spaceName)(flash,user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/editSpace.scala.html
                    HASH: 903228f84786d54f5acd92e4c008f1e0da309d41
                    MATRIX: 690->1|968->154|999->190|1038->195
                    LINES: 20->1|24->1|25->3|25->3
                    -- GENERATED --
                */
            