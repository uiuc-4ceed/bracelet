
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object space extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template9[ProjectSpace,List[Collection],List[Dataset],List[Dataset],List[play.api.libs.json.JsValue],String,Map[User, String],List[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: ProjectSpace, collections: List[Collection], publicDatasets: List[Dataset], datasets: List[Dataset], publishedData: List[play.api.libs.json.JsValue], servicesUrl: String, userRoleMap: Map[User, String], userSelections: List[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.libs.json._

import play.api.Play.current

import api.Permission

import play.api.i18n.Messages


Seq[Any](format.raw/*1.278*/("""
"""),format.raw/*3.1*/("""
"""),_display_(Seq[Any](/*7.2*/main(space.name)/*7.18*/ {_display_(Seq[Any](format.raw/*7.20*/("""


    """),_display_(Seq[Any](/*10.6*/if( space.bannerURL.isDefined || space.logoURL.isDefined)/*10.63*/{_display_(Seq[Any](format.raw/*10.64*/("""
        <div class="row">
        """),_display_(Seq[Any](/*12.10*/if(space.bannerURL.isDefined)/*12.39*/ {_display_(Seq[Any](format.raw/*12.41*/("""
            <div class="space-banner nopadding col-md-12 col-lg-12 col-sm-12" style="background-image:url('"""),_display_(Seq[Any](/*13.109*/(space.bannerURL))),format.raw/*13.126*/("""');">
        """)))}/*14.11*/else/*14.16*/{_display_(Seq[Any](format.raw/*14.17*/("""
            <div class="col-md-12 col-lg-12 col-sm-12">
        """)))})),format.raw/*16.10*/("""
            """),_display_(Seq[Any](/*17.14*/if(space.logoURL.isDefined)/*17.41*/ {_display_(Seq[Any](format.raw/*17.43*/("""
                <img class="space-banner-logo pull-left" src=""""),_display_(Seq[Any](/*18.64*/(space.logoURL))),format.raw/*18.79*/("""" alt="Thumbnail of """),_display_(Seq[Any](/*18.100*/(space.name))),format.raw/*18.112*/("""">
            """)))})),format.raw/*19.14*/("""
            </div>
        </div>
    """)))})),format.raw/*22.6*/("""

    <div class="row vertical-align">

            <div class="col-md-12 col-lg-12 col-sm-12">
                <h1 id="spacenamedisplay" class="space-title"><span class="glyphicon glyphicon-hdd"></span> """),_display_(Seq[Any](/*27.110*/space/*27.115*/.name)),format.raw/*27.120*/("""</h1>
                
            <p><span id="spacedescdisplay" class='abstract'>"""),_display_(Seq[Any](/*29.62*/Html(space.description.replace("\n","<br>")))),format.raw/*29.106*/("""</span></p>
            """),_display_(Seq[Any](/*30.14*/if(user.isDefined)/*30.32*/ {_display_(Seq[Any](format.raw/*30.34*/("""
                <hr/>
                """),_display_(Seq[Any](/*32.18*/if(Permission.checkPermission(Permission.DeleteSpace, ResourceRef(ResourceRef.space, space.id)))/*32.114*/{_display_(Seq[Any](format.raw/*32.115*/("""
<!--                     <button id="deleteButton" onclick="confirmDeleteResource('space','"""),_display_(Seq[Any](/*33.93*/Messages("space.title"))),format.raw/*33.116*/("""','"""),_display_(Seq[Any](/*33.120*/(space.id))),format.raw/*33.130*/("""','"""),_display_(Seq[Any](/*33.134*/(space.name.replace("'","&#39;")))),format.raw/*33.167*/("""',true, '"""),_display_(Seq[Any](/*33.177*/(routes.Spaces.list("")))),format.raw/*33.201*/("""')"
                    class="btn btn-link" title="Delete the """),_display_(Seq[Any](/*34.61*/Messages("space.title"))),format.raw/*34.84*/(""" but not its contents">
                    <span class="glyphicon glyphicon-trash"></span> Delete</button>
 -->                """)))})),format.raw/*36.22*/("""
                """),_display_(Seq[Any](/*37.18*/if(Permission.checkPermission(Permission.CreateCollection, ResourceRef(ResourceRef.space, space.id)))/*37.119*/ {_display_(Seq[Any](format.raw/*37.121*/("""
                    <a id="create_collection" href=""""),_display_(Seq[Any](/*38.54*/routes/*38.60*/.Collections.newCollection(Some(space.id.toString())))),format.raw/*38.113*/("""" class="btn btn-link" title=""""),_display_(Seq[Any](/*38.144*/Messages("create.title", Messages("collections.title")))),format.raw/*38.199*/("""">
                        <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*39.71*/Messages("create.title", Messages("collections.title")))),format.raw/*39.126*/("""</a>
                """)))})),format.raw/*40.18*/("""                
                """),_display_(Seq[Any](/*41.18*/if(Permission.checkPermission(Permission.CreateDataset, ResourceRef(ResourceRef.space, space.id)))/*41.116*/ {_display_(Seq[Any](format.raw/*41.118*/("""
                    <a id="create_dataset" href=""""),_display_(Seq[Any](/*42.51*/routes/*42.57*/.Datasets.newDataset(Some(space.id.toString()), None))),format.raw/*42.110*/("""" class="btn btn-link" title=""""),_display_(Seq[Any](/*42.141*/Messages("create.title", Messages("dataset.title")))),format.raw/*42.192*/("""">
                        <span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*43.71*/Messages("create.title", Messages("dataset.title")))),format.raw/*43.122*/("""</a>
                """)))})),format.raw/*44.18*/("""

                """),_display_(Seq[Any](/*46.18*/if(play.Play.application().configuration().getBoolean("enablePublic") && (Permission.checkPermission(Permission.EditSpace, ResourceRef(ResourceRef.space, space.id)) || Permission.checkPermission(Permission.DeleteSpace, ResourceRef(ResourceRef.space, space.id))))/*46.280*/{_display_(Seq[Any](format.raw/*46.281*/("""
                    <hr/>
                """)))})),format.raw/*48.18*/("""
            """)))})),format.raw/*49.14*/("""
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-8">
            <ul class="nav nav-tabs margin-bottom-20 hideTree" >
                """),_display_(Seq[Any](/*55.18*/if(play.Play.application().configuration().getBoolean("enablePublic"))/*55.88*/{_display_(Seq[Any](format.raw/*55.89*/("""
                    """),_display_(Seq[Any](/*56.22*/if(user.isDefined)/*56.40*/ {_display_(Seq[Any](format.raw/*56.42*/("""
                        <li role="presentation" class="active"><a href="#tab-all" role="tab" data-toggle="tab">All Data</a></li>
<!--                         <li role="presentation hideTree"><a href="#tab-public" role="tab" data-toggle="tab">Public Data</a></li>
 -->                        """),_display_(Seq[Any](/*59.30*/if(play.api.Play.current.plugin[services.StagingAreaPlugin].isDefined)/*59.100*/{_display_(Seq[Any](format.raw/*59.101*/("""
<!--                             <li role="presentation"><a href="#tab-publish" role="tab" data-toggle="tab">Published Data</a></li>
 -->                        """)))})),format.raw/*61.30*/("""
                    """)))}/*62.23*/else/*62.28*/{_display_(Seq[Any](format.raw/*62.29*/("""
<!--                     	<li role="presentation" class="active hideTree"><a href="#tab-public" role="tab" data-toggle="tab">Public Data</a></li>
                        """),_display_(Seq[Any](/*64.26*/if(play.api.Play.current.plugin[services.StagingAreaPlugin].isDefined)/*64.96*/ {_display_(Seq[Any](format.raw/*64.98*/("""
                            <li role="presentation"><a href="#tab-publish" role="tab" data-toggle="tab">Published Data</a></li>
 -->                        """)))})),format.raw/*66.30*/("""
                    """)))})),format.raw/*67.22*/("""
                """)))})),format.raw/*68.18*/("""
            </ul>
            <div class="tab-content hideTree">
                """),_display_(Seq[Any](/*71.18*/if(user.isDefined)/*71.36*/{_display_(Seq[Any](format.raw/*71.37*/("""
                    <div role="tabpanel" class="tab-pane fade in active" id="tab-all">
                        """),_display_(Seq[Any](/*73.26*/spaces/*73.32*/.collectionsBySpace(collections, space, None))),format.raw/*73.77*/("""
                        """),_display_(Seq[Any](/*74.26*/spaces/*74.32*/.datasetsBySpace(datasets, space, None, userSelections))),format.raw/*74.87*/("""
                    </div>
                     
                    <div role="tabpanel" class="tab-pane fade" id="tab-public">
                        <p>The """),_display_(Seq[Any](/*78.33*/Messages("space.title"))),format.raw/*78.56*/(""" team has made the following """),_display_(Seq[Any](/*78.86*/Messages("datasets.title")/*78.112*/.toLowerCase)),format.raw/*78.124*/(""" and """),_display_(Seq[Any](/*78.130*/Messages("collections.title")/*78.159*/.toLowerCase)),format.raw/*78.171*/(""" publicly available.</p>
                        """),_display_(Seq[Any](/*79.26*/spaces/*79.32*/.datasetsBySpace(publicDatasets, space, Some(true), userSelections))),format.raw/*79.99*/("""
                            <!-- collections follow the space status, there is no public, private flag on a collection -->
                        """),_display_(Seq[Any](/*81.26*/if(space.isPublic)/*81.44*/ {_display_(Seq[Any](format.raw/*81.46*/("""
                            """),_display_(Seq[Any](/*82.30*/spaces/*82.36*/.collectionsBySpace(collections, space, Some(true)))),format.raw/*82.87*/("""
                        """)))}/*83.27*/else/*83.32*/{_display_(Seq[Any](format.raw/*83.33*/("""
                            <h3>"""),_display_(Seq[Any](/*84.34*/Messages("collections.title"))),format.raw/*84.63*/("""</h3>
                            <p>There are no public collections associated with this """),_display_(Seq[Any](/*85.86*/Messages("space.title"))),format.raw/*85.109*/(""". </p>
                        """)))})),format.raw/*86.26*/("""
                    </div>
                """)))}/*88.19*/else/*88.24*/{_display_(Seq[Any](format.raw/*88.25*/("""
                    <div role="tabpanel" class="tab-pane fade in active" id="tab-public">
                        <p>The """),_display_(Seq[Any](/*90.33*/Messages("space.title"))),format.raw/*90.56*/(""" team has made the following """),_display_(Seq[Any](/*90.86*/Messages("datasets.title")/*90.112*/.toLowerCase)),format.raw/*90.124*/(""" and """),_display_(Seq[Any](/*90.130*/Messages("collections.title")/*90.159*/.toLowerCase)),format.raw/*90.171*/(""" publicly available.
                            You must be a logged-in member of the """),_display_(Seq[Any](/*91.68*/Messages("space.title"))),format.raw/*91.91*/(""" to access all the """),_display_(Seq[Any](/*91.111*/Messages("datasets.title")/*91.137*/.toLowerCase)),format.raw/*91.149*/(""" and """),_display_(Seq[Any](/*91.155*/Messages("collections.title")/*91.184*/.toLowerCase)),format.raw/*91.196*/(""".
                        </p>
                        """),_display_(Seq[Any](/*93.26*/spaces/*93.32*/.datasetsBySpace(publicDatasets, space, Some(true), userSelections))),format.raw/*93.99*/("""
                        """),_display_(Seq[Any](/*94.26*/if(space.isPublic)/*94.44*/ {_display_(Seq[Any](format.raw/*94.46*/("""
                            """),_display_(Seq[Any](/*95.30*/spaces/*95.36*/.collectionsBySpace(collections, space, Some(true)))),format.raw/*95.87*/("""
                        """)))}/*96.27*/else/*96.32*/{_display_(Seq[Any](format.raw/*96.33*/("""
                            <h3>"""),_display_(Seq[Any](/*97.34*/Messages("collections.title"))),format.raw/*97.63*/("""</h3>
                            <p>There are no public collections associated with this """),_display_(Seq[Any](/*98.86*/Messages("space.title"))),format.raw/*98.109*/(""". </p>
                        """)))})),format.raw/*99.26*/("""
                    </div>
                """)))})),format.raw/*101.18*/("""
                <div role="tabpanel" class="tab-pane fade" id="tab-publish">
                <p>The following """),_display_(Seq[Any](/*103.35*/Messages("datasets.title")/*103.61*/.toLowerCase)),format.raw/*103.73*/(""" have been published through this """),_display_(Seq[Any](/*103.108*/Messages("space.title"))),format.raw/*103.131*/(""" and any affiliated """),_display_(Seq[Any](/*103.152*/Messages("space.title"))),format.raw/*103.175*/("""s.</p>
                    """),_display_(Seq[Any](/*104.22*/curations/*104.31*/.publishedGrid(publishedData, servicesUrl, None))),format.raw/*104.79*/("""
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4">
            """),_display_(Seq[Any](/*109.14*/if(user.isDefined)/*109.32*/ {_display_(Seq[Any](format.raw/*109.34*/("""
                """),_display_(Seq[Any](/*110.18*/spaces/*110.24*/.otherActions(space))),format.raw/*110.44*/("""
            """)))}/*111.15*/else/*111.20*/{_display_(Seq[Any](format.raw/*111.21*/("""
                """),_display_(Seq[Any](/*112.18*/spaces/*112.24*/.statistics(space, "row ds-section-sm space-col-right"))),format.raw/*112.79*/("""
            """)))})),format.raw/*113.14*/("""
            """),format.raw/*114.111*/("""
            """),_display_(Seq[Any](/*115.14*/if(play.Play.application().configuration().getBoolean("enablePublic"))/*115.84*/ {_display_(Seq[Any](format.raw/*115.86*/("""
                """),_display_(Seq[Any](/*116.18*/spaces/*116.24*/.access(space, userRoleMap, "row ds-section-sm break-word space-col-right"))),format.raw/*116.99*/("""
            """)))})),format.raw/*117.14*/("""
            """),_display_(Seq[Any](/*118.14*/if(play.api.Play.configuration.getBoolean("enable_expiration").getOrElse(false))/*118.94*/ {_display_(Seq[Any](format.raw/*118.96*/("""
                """),_display_(Seq[Any](/*119.18*/spaces/*119.24*/.spaceConfiguration(space))),format.raw/*119.50*/("""
            """)))})),format.raw/*120.14*/("""
        </div>
    </div>
    <script src=""""),_display_(Seq[Any](/*123.19*/routes/*123.25*/.Assets.at("javascripts/descriptionSummary.js"))),format.raw/*123.72*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*124.19*/routes/*124.25*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*124.70*/("""" type="text/javascript"></script>
    <script>
        function activateOne(id) """),format.raw/*126.34*/("""{"""),format.raw/*126.35*/("""
            // initialize Masonry
            var items = $("#" + id); 
            if(items.length) """),format.raw/*129.30*/("""{"""),format.raw/*129.31*/(""" 
            	var $container = $("#" + id).masonry();
	            // layout Masonry again after all images have loaded
    	        imagesLoaded( '#masonry', function() """),format.raw/*132.51*/("""{"""),format.raw/*132.52*/("""
        	        $container.masonry("""),format.raw/*133.37*/("""{"""),format.raw/*133.38*/("""
            	    itemSelector: '.post-box',
                	columnWidth: '.post-box',
                	transitionDuration: 4
                	"""),format.raw/*137.18*/("""}"""),format.raw/*137.19*/(""");
            	"""),format.raw/*138.14*/("""}"""),format.raw/*138.15*/(""");
            """),format.raw/*139.13*/("""}"""),format.raw/*139.14*/("""
        """),format.raw/*140.9*/("""}"""),format.raw/*140.10*/("""

        function activate()"""),format.raw/*142.28*/("""{"""),format.raw/*142.29*/("""
            activateOne("masonry-datasets");
            activateOne("masonry-collections");
            $('#tab-publish .grid').masonry();
            doSummarizeAbstracts();
        """),format.raw/*147.9*/("""}"""),format.raw/*147.10*/("""

        $(document).ready(function () """),format.raw/*149.39*/("""{"""),format.raw/*149.40*/("""
            activate();

        """),format.raw/*152.9*/("""}"""),format.raw/*152.10*/(""");

        // fire when showing from tab
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) """),format.raw/*155.67*/("""{"""),format.raw/*155.68*/("""
            activate();
        """),format.raw/*157.9*/("""}"""),format.raw/*157.10*/(""")
    </script>
    <script src=""""),_display_(Seq[Any](/*159.19*/routes/*159.25*/.Assets.at("javascripts/spaceListProcess.js"))),format.raw/*159.70*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*160.19*/routes/*160.25*/.Assets.at("javascripts/collectionListProcess.js"))),format.raw/*160.75*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*161.19*/routes/*161.25*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*161.72*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*162.19*/routes/*162.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*162.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*163.19*/routes/*163.25*/.Assets.at("javascripts/spaceModify.js"))),format.raw/*163.65*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*164.19*/routes/*164.25*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*164.67*/("""" language="javascript"></script>
    <script src=""""),_display_(Seq[Any](/*165.19*/routes/*165.25*/.Assets.at("javascripts/select.js"))),format.raw/*165.60*/("""" type="text/javascript"></script>
""")))})),format.raw/*166.2*/("""
"""))}
    }
    
    def render(space:ProjectSpace,collections:List[Collection],publicDatasets:List[Dataset],datasets:List[Dataset],publishedData:List[play.api.libs.json.JsValue],servicesUrl:String,userRoleMap:Map[User, String],userSelections:List[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space,collections,publicDatasets,datasets,publishedData,servicesUrl,userRoleMap,userSelections)(user)
    
    def f:((ProjectSpace,List[Collection],List[Dataset],List[Dataset],List[play.api.libs.json.JsValue],String,Map[User, String],List[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space,collections,publicDatasets,datasets,publishedData,servicesUrl,userRoleMap,userSelections) => (user) => apply(space,collections,publicDatasets,datasets,publishedData,servicesUrl,userRoleMap,userSelections)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:47:00 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/space.scala.html
                    HASH: afe332139b625e07c70aee91fa01c8e5b789a28e
                    MATRIX: 736->1|1219->277|1246->307|1282->393|1306->409|1345->411|1388->419|1454->476|1493->477|1565->513|1603->542|1643->544|1789->653|1829->670|1863->686|1876->691|1915->692|2013->758|2063->772|2099->799|2139->801|2239->865|2276->880|2334->901|2369->913|2417->929|2488->969|2730->1174|2745->1179|2773->1184|2893->1268|2960->1312|3021->1337|3048->1355|3088->1357|3164->1397|3270->1493|3310->1494|3439->1587|3485->1610|3526->1614|3559->1624|3600->1628|3656->1661|3703->1671|3750->1695|3850->1759|3895->1782|4056->1911|4110->1929|4221->2030|4262->2032|4352->2086|4367->2092|4443->2145|4511->2176|4589->2231|4698->2304|4776->2359|4830->2381|4900->2415|5008->2513|5049->2515|5136->2566|5151->2572|5227->2625|5295->2656|5369->2707|5478->2780|5552->2831|5606->2853|5661->2872|5933->3134|5973->3135|6049->3179|6095->3193|6311->3373|6390->3443|6429->3444|6487->3466|6514->3484|6554->3486|6883->3779|6963->3849|7003->3850|7198->4013|7239->4036|7252->4041|7291->4042|7499->4214|7578->4284|7618->4286|7808->4444|7862->4466|7912->4484|8031->4567|8058->4585|8097->4586|8246->4699|8261->4705|8328->4750|8390->4776|8405->4782|8482->4837|8680->4999|8725->5022|8791->5052|8827->5078|8862->5090|8905->5096|8944->5125|8979->5137|9065->5187|9080->5193|9169->5260|9354->5409|9381->5427|9421->5429|9487->5459|9502->5465|9575->5516|9620->5543|9633->5548|9672->5549|9742->5583|9793->5612|9920->5703|9966->5726|10030->5758|10094->5804|10107->5809|10146->5810|10305->5933|10350->5956|10416->5986|10452->6012|10487->6024|10530->6030|10569->6059|10604->6071|10728->6159|10773->6182|10830->6202|10866->6228|10901->6240|10944->6246|10983->6275|11018->6287|11110->6343|11125->6349|11214->6416|11276->6442|11303->6460|11343->6462|11409->6492|11424->6498|11497->6549|11542->6576|11555->6581|11594->6582|11664->6616|11715->6645|11842->6736|11888->6759|11952->6791|12030->6836|12179->6948|12215->6974|12250->6986|12323->7021|12370->7044|12429->7065|12476->7088|12541->7116|12560->7125|12631->7173|12788->7293|12816->7311|12857->7313|12912->7331|12928->7337|12971->7357|13005->7372|13019->7377|13059->7378|13114->7396|13130->7402|13208->7457|13255->7471|13298->7582|13349->7596|13429->7666|13470->7668|13525->7686|13541->7692|13639->7767|13686->7781|13737->7795|13827->7875|13868->7877|13923->7895|13939->7901|13988->7927|14035->7941|14117->7986|14133->7992|14203->8039|14293->8092|14309->8098|14377->8143|14487->8224|14517->8225|14648->8327|14678->8328|14878->8499|14908->8500|14974->8537|15004->8538|15177->8682|15207->8683|15252->8699|15282->8700|15326->8715|15356->8716|15393->8725|15423->8726|15481->8755|15511->8756|15724->8941|15754->8942|15823->8982|15853->8983|15915->9017|15945->9018|16082->9126|16112->9127|16173->9160|16203->9161|16274->9195|16290->9201|16358->9246|16448->9299|16464->9305|16537->9355|16627->9408|16643->9414|16713->9461|16803->9514|16819->9520|16882->9560|16972->9613|16988->9619|17051->9659|17141->9712|17157->9718|17222->9760|17311->9812|17327->9818|17385->9853|17453->9889
                    LINES: 20->1|30->1|31->3|32->7|32->7|32->7|35->10|35->10|35->10|37->12|37->12|37->12|38->13|38->13|39->14|39->14|39->14|41->16|42->17|42->17|42->17|43->18|43->18|43->18|43->18|44->19|47->22|52->27|52->27|52->27|54->29|54->29|55->30|55->30|55->30|57->32|57->32|57->32|58->33|58->33|58->33|58->33|58->33|58->33|58->33|58->33|59->34|59->34|61->36|62->37|62->37|62->37|63->38|63->38|63->38|63->38|63->38|64->39|64->39|65->40|66->41|66->41|66->41|67->42|67->42|67->42|67->42|67->42|68->43|68->43|69->44|71->46|71->46|71->46|73->48|74->49|80->55|80->55|80->55|81->56|81->56|81->56|84->59|84->59|84->59|86->61|87->62|87->62|87->62|89->64|89->64|89->64|91->66|92->67|93->68|96->71|96->71|96->71|98->73|98->73|98->73|99->74|99->74|99->74|103->78|103->78|103->78|103->78|103->78|103->78|103->78|103->78|104->79|104->79|104->79|106->81|106->81|106->81|107->82|107->82|107->82|108->83|108->83|108->83|109->84|109->84|110->85|110->85|111->86|113->88|113->88|113->88|115->90|115->90|115->90|115->90|115->90|115->90|115->90|115->90|116->91|116->91|116->91|116->91|116->91|116->91|116->91|116->91|118->93|118->93|118->93|119->94|119->94|119->94|120->95|120->95|120->95|121->96|121->96|121->96|122->97|122->97|123->98|123->98|124->99|126->101|128->103|128->103|128->103|128->103|128->103|128->103|128->103|129->104|129->104|129->104|134->109|134->109|134->109|135->110|135->110|135->110|136->111|136->111|136->111|137->112|137->112|137->112|138->113|139->114|140->115|140->115|140->115|141->116|141->116|141->116|142->117|143->118|143->118|143->118|144->119|144->119|144->119|145->120|148->123|148->123|148->123|149->124|149->124|149->124|151->126|151->126|154->129|154->129|157->132|157->132|158->133|158->133|162->137|162->137|163->138|163->138|164->139|164->139|165->140|165->140|167->142|167->142|172->147|172->147|174->149|174->149|177->152|177->152|180->155|180->155|182->157|182->157|184->159|184->159|184->159|185->160|185->160|185->160|186->161|186->161|186->161|187->162|187->162|187->162|188->163|188->163|188->163|189->164|189->164|189->164|190->165|190->165|190->165|191->166
                    -- GENERATED --
                */
            