
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object inviteNotificationEmail extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template6[String,String,models.MiniUser,String,String,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(spaceid: String, spaceName: String, inviter: models.MiniUser, newmember: String, role: String)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider

import play.api.i18n.Messages


Seq[Any](format.raw/*1.130*/("""
"""),format.raw/*4.1*/("""<html>
    <body>

        <p>Hello """),_display_(Seq[Any](/*7.19*/newmember)),format.raw/*7.28*/(""",</p>

        <p>You have been added by <a href=""""),_display_(Seq[Any](/*9.45*/routes/*9.51*/.Profile.viewProfileUUID(inviter.id).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.128*/("""">"""),_display_(Seq[Any](/*9.131*/inviter/*9.138*/.fullName)),format.raw/*9.147*/("""</a>
            to """),_display_(Seq[Any](/*10.17*/Messages("space.title"))),format.raw/*10.40*/(""" <a href=""""),_display_(Seq[Any](/*10.51*/routes/*10.57*/.Spaces.getSpace(UUID(spaceid)).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*10.129*/("""">"""),_display_(Seq[Any](/*10.132*/spaceName)),format.raw/*10.141*/("""</a>
            on <a href=""""),_display_(Seq[Any](/*11.26*/routes/*11.32*/.Application.index().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*11.93*/("""">"""),_display_(Seq[Any](/*11.96*/services/*11.104*/.AppConfiguration.getDisplayName)),format.raw/*11.136*/("""</a> as """),_display_(Seq[Any](/*11.145*/role)),format.raw/*11.149*/(""".
        </p>
    </body>
</html>"""))}
    }
    
    def render(spaceid:String,spaceName:String,inviter:models.MiniUser,newmember:String,role:String,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(spaceid,spaceName,inviter,newmember,role)(request)
    
    def f:((String,String,models.MiniUser,String,String) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (spaceid,spaceName,inviter,newmember,role) => (request) => apply(spaceid,spaceName,inviter,newmember,role)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/inviteNotificationEmail.scala.html
                    HASH: 80440a8d1cba49dbbd03072a3b9473a39adf69c1
                    MATRIX: 663->1|959->129|986->204|1058->241|1088->250|1174->301|1188->307|1287->384|1326->387|1342->394|1373->403|1430->424|1475->447|1522->458|1537->464|1632->536|1672->539|1704->548|1770->578|1785->584|1868->645|1907->648|1925->656|1980->688|2026->697|2053->701
                    LINES: 20->1|26->1|27->4|30->7|30->7|32->9|32->9|32->9|32->9|32->9|32->9|33->10|33->10|33->10|33->10|33->10|33->10|33->10|34->11|34->11|34->11|34->11|34->11|34->11|34->11|34->11
                    -- GENERATED --
                */
            