
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object statistics extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[ProjectSpace,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(space: ProjectSpace, classes: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages


Seq[Any](format.raw/*1.76*/("""

"""),format.raw/*4.1*/("""<div class=""""),_display_(Seq[Any](/*4.14*/classes)),format.raw/*4.21*/("""">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <h3>Statistics</h3>
        <p><span class="glyphicon glyphicon-user"></span> Members: """),_display_(Seq[Any](/*7.69*/space/*7.74*/.userCount)),format.raw/*7.84*/("""</p>
        <p><span class="glyphicon glyphicon-th-list"></span> Collections: """),_display_(Seq[Any](/*8.76*/space/*8.81*/.collectionCount)),format.raw/*8.97*/("""</p>
        <p><span class="glyphicon glyphicon-briefcase"></span> Datasets: """),_display_(Seq[Any](/*9.75*/space/*9.80*/.datasetCount)),format.raw/*9.93*/("""</p>
    </div>
</div>
"""))}
    }
    
    def render(space:ProjectSpace,classes:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(space,classes)(user)
    
    def f:((ProjectSpace,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (space,classes) => (user) => apply(space,classes)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:38 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/statistics.scala.html
                    HASH: b56a95969949c29dd00b91e7782041214c114198
                    MATRIX: 632->1|830->75|858->108|906->121|934->128|1116->275|1129->280|1160->290|1275->370|1288->375|1325->391|1439->470|1452->475|1486->488
                    LINES: 20->1|24->1|26->4|26->4|26->4|29->7|29->7|29->7|30->8|30->8|30->8|31->9|31->9|31->9
                    -- GENERATED --
                */
            