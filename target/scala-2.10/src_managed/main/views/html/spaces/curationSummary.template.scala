
package views.html.spaces

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object curationSummary extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[models.CurationObject,Boolean,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(curationObject: models.CurationObject, editCurationObject: Boolean)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission


Seq[Any](format.raw/*1.106*/("""
"""),format.raw/*3.1*/("""    <div class="panel panel-default">
        <div class="panel-body break-word">
            <h4>"""),_display_(Seq[Any](/*5.18*/curationObject/*5.32*/.name)),format.raw/*5.37*/(""" <h4>
            <h5>"""),_display_(Seq[Any](/*6.18*/Messages("dataset.title"))),format.raw/*6.43*/(""": <a href =""""),_display_(Seq[Any](/*6.56*/routes/*6.62*/.Datasets.dataset(curationObject.datasets(0).id))),format.raw/*6.110*/("""">"""),_display_(Seq[Any](/*6.113*/curationObject/*6.127*/.datasets(0).name)),format.raw/*6.144*/("""</a></h5>
            <ul class="list-unstyled">
                <li class = "abstractsummary">"""),_display_(Seq[Any](/*8.48*/Messages("publicationrequest.description"))),format.raw/*8.90*/(""": """),_display_(Seq[Any](/*8.93*/Html(curationObject.description.replace("\n","<br>")))),format.raw/*8.146*/("""</li>
                <li>Creator(s): 

                	"""),_display_(Seq[Any](/*11.19*/if(curationObject.creators.length !=0)/*11.57*/ {_display_(Seq[Any](format.raw/*11.59*/("""
            			"""),_display_(Seq[Any](/*12.17*/for(i <- 0 to (curationObject.creators.length  - 2) ) yield /*12.70*/ {_display_(Seq[Any](format.raw/*12.72*/("""
                			<span class="creator" >
                  				<span class="authname person  break-word" data-creator=""""),_display_(Seq[Any](/*14.80*/curationObject/*14.94*/.creators(i).trim)),format.raw/*14.111*/("""">"""),_display_(Seq[Any](/*14.114*/curationObject/*14.128*/.creators(i).trim)),format.raw/*14.145*/("""</span><span>, </span>
                			</span>
            			""")))})),format.raw/*16.17*/("""
                		<span class="creator" >
	                  		<span class="authname person  break-word" data-creator=""""),_display_(Seq[Any](/*18.79*/curationObject/*18.93*/.creators(curationObject.creators.length-1).trim)),format.raw/*18.141*/("""">"""),_display_(Seq[Any](/*18.144*/curationObject/*18.158*/.creators(curationObject.creators.length-1).trim)),format.raw/*18.206*/("""</span>
        	        	</span>
                	""")))})),format.raw/*20.19*/("""	
                </li>
                <li><div id="size">Size: </div> <li/>
                <li><div id="format">File Formats: </div></li>
                <li>"""),_display_(Seq[Any](/*24.22*/Messages("owner.label"))),format.raw/*24.45*/(""": """),_display_(Seq[Any](/*24.48*/curationObject/*24.62*/.author.fullName)),format.raw/*24.78*/("""</li>
                <li>Created: """),_display_(Seq[Any](/*25.31*/curationObject/*25.45*/.created.format("MMM dd, yyyy"))),format.raw/*25.76*/("""</li>
            </ul>
            """),_display_(Seq[Any](/*27.14*/if(Permission.checkPermission(Permission.EditStagingArea, ResourceRef(ResourceRef.space, curationObject.space)))/*27.126*/{_display_(Seq[Any](format.raw/*27.127*/("""
                <a class="btn btn-sm btn-link" href=""""),_display_(Seq[Any](/*28.55*/routes/*28.61*/.Spaces.stagingArea(curationObject.space))),format.raw/*28.102*/("""">
                    <span class="glyphicon glyphicon-briefcase"></span> Staging Area</a>
                """),_display_(Seq[Any](/*30.18*/if(!editCurationObject)/*30.41*/{_display_(Seq[Any](format.raw/*30.42*/("""
                    <a class="btn btn-sm btn-link" href =""""),_display_(Seq[Any](/*31.60*/routes/*31.66*/.CurationObjects.getCurationObject(curationObject.id))),format.raw/*31.119*/("""">
                        <span class="glyphicon glyphicon-chevron-left"></span> Go to Edit Metadata</a>
                """)))})),format.raw/*33.18*/("""
                <a class="btn btn-sm btn-link"  href =""""),_display_(Seq[Any](/*34.57*/(routes.CurationObjects.editCuration(curationObject.id)))),format.raw/*34.113*/(""""  >
                    <span class="glyphicon glyphicon-edit"></span> Edit """),_display_(Seq[Any](/*35.74*/Messages("curationobject.label"))),format.raw/*35.106*/("""</a>
                <a class="btn btn-sm btn-link" onclick="confirmDeleteResource('curation object','curation object','"""),_display_(Seq[Any](/*36.117*/(curationObject.id))),format.raw/*36.136*/("""','"""),_display_(Seq[Any](/*36.140*/(curationObject.name.replace("'","&#39;")))),format.raw/*36.182*/("""',true, '"""),_display_(Seq[Any](/*36.192*/routes/*36.198*/.Spaces.stagingArea(curationObject.space))),format.raw/*36.239*/("""')"  >
                    <span class="glyphicon glyphicon-trash"></span> Delete """),_display_(Seq[Any](/*37.77*/Messages("curationobject.label"))),format.raw/*37.109*/("""</a>
            """)))})),format.raw/*38.14*/("""
        </div>
    </div>
    <script src=""""),_display_(Seq[Any](/*41.19*/routes/*41.25*/.Assets.at("javascripts/curationProcess.js"))),format.raw/*41.69*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*42.19*/routes/*42.25*/.Assets.at("javascripts/people.js"))),format.raw/*42.60*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*43.19*/routes/*43.25*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*43.70*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*44.19*/routes/*44.25*/.Assets.at("javascripts/descriptionSummary.js"))),format.raw/*44.72*/("""" type="text/javascript"></script>
    <script>
    $(document).ready(getFiles('"""),_display_(Seq[Any](/*46.34*/curationObject/*46.48*/.id)),format.raw/*46.51*/("""'));
    </script>"""))}
    }
    
    def render(curationObject:models.CurationObject,editCurationObject:Boolean,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(curationObject,editCurationObject)(user)
    
    def f:((models.CurationObject,Boolean) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (curationObject,editCurationObject) => (user) => apply(curationObject,editCurationObject)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/spaces/curationSummary.scala.html
                    HASH: 422914006832ba3d7ddcb6135d133325b18ae40a
                    MATRIX: 647->1|868->105|895->129|1029->228|1051->242|1077->247|1135->270|1181->295|1229->308|1243->314|1313->362|1352->365|1375->379|1414->396|1545->492|1608->534|1646->537|1721->590|1815->648|1862->686|1902->688|1955->705|2024->758|2064->760|2223->883|2246->897|2286->914|2326->917|2350->931|2390->948|2488->1014|2645->1135|2668->1149|2739->1197|2779->1200|2803->1214|2874->1262|2958->1314|3156->1476|3201->1499|3240->1502|3263->1516|3301->1532|3373->1568|3396->1582|3449->1613|3522->1650|3644->1762|3684->1763|3775->1818|3790->1824|3854->1865|3999->1974|4031->1997|4070->1998|4166->2058|4181->2064|4257->2117|4412->2240|4505->2297|4584->2353|4698->2431|4753->2463|4911->2584|4953->2603|4994->2607|5059->2649|5106->2659|5122->2665|5186->2706|5305->2789|5360->2821|5410->2839|5491->2884|5506->2890|5572->2934|5661->2987|5676->2993|5733->3028|5822->3081|5837->3087|5904->3132|5993->3185|6008->3191|6077->3238|6194->3319|6217->3333|6242->3336
                    LINES: 20->1|24->1|25->3|27->5|27->5|27->5|28->6|28->6|28->6|28->6|28->6|28->6|28->6|28->6|30->8|30->8|30->8|30->8|33->11|33->11|33->11|34->12|34->12|34->12|36->14|36->14|36->14|36->14|36->14|36->14|38->16|40->18|40->18|40->18|40->18|40->18|40->18|42->20|46->24|46->24|46->24|46->24|46->24|47->25|47->25|47->25|49->27|49->27|49->27|50->28|50->28|50->28|52->30|52->30|52->30|53->31|53->31|53->31|55->33|56->34|56->34|57->35|57->35|58->36|58->36|58->36|58->36|58->36|58->36|58->36|59->37|59->37|60->38|63->41|63->41|63->41|64->42|64->42|64->42|65->43|65->43|65->43|66->44|66->44|66->44|68->46|68->46|68->46
                    -- GENERATED --
                */
            