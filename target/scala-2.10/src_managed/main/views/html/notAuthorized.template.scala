
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object notAuthorized extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[String,String,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(msg: String, id: String, resourceType: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.85*/("""
"""),_display_(Seq[Any](/*2.2*/main("Not authorized")/*2.24*/ {_display_(Seq[Any](format.raw/*2.26*/("""
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12 text-center">
			<h2>
				"""),_display_(Seq[Any](/*6.6*/if(msg != null)/*6.21*/{_display_(Seq[Any](format.raw/*6.22*/("""
					"""),_display_(Seq[Any](/*7.7*/msg)),format.raw/*7.10*/("""
				""")))}/*8.6*/else/*8.10*/{_display_(Seq[Any](format.raw/*8.11*/("""
					You are not authorized to proceed with your request
				""")))})),format.raw/*10.6*/("""
			</h2>
		</div>
	</div>
""")))})))}
    }
    
    def render(msg:String,id:String,resourceType:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(msg,id,resourceType)(user)
    
    def f:((String,String,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (msg,id,resourceType) => (user) => apply(msg,id,resourceType)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/notAuthorized.scala.html
                    HASH: c74dd6dd69da6681f78d0d6957e1ac4a9d36f055
                    MATRIX: 629->1|806->84|842->86|872->108|911->110|1036->201|1059->216|1097->217|1138->224|1162->227|1185->233|1197->237|1235->238|1329->301
                    LINES: 20->1|23->1|24->2|24->2|24->2|28->6|28->6|28->6|29->7|29->7|30->8|30->8|30->8|32->10
                    -- GENERATED --
                */
            