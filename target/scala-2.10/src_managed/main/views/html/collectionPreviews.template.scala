
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object collectionPreviews extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[String,List[Preview],Array[Previewer],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(collection_id: String, previews: List[Preview], previewers: Array[Previewer]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.80*/("""

"""),_display_(Seq[Any](/*3.2*/main("Collection Previews")/*3.29*/ {_display_(Seq[Any](format.raw/*3.31*/("""
<div class="page-header">
  <h1>Collection Previews</h1>
</div>
<div class="row">
    <div class="col-md-12">
    <h3>Previews available for this collection</h3>
    <ul>
    """),_display_(Seq[Any](/*11.6*/for(p <- previews) yield /*11.24*/ {_display_(Seq[Any](format.raw/*11.26*/("""
      <li><b>Preview type:</b> """),_display_(Seq[Any](/*12.33*/p/*12.34*/.preview_type.get)),format.raw/*12.51*/(""" (preview id: """),_display_(Seq[Any](/*12.66*/p/*12.67*/.id)),format.raw/*12.70*/(""")</li>
    """)))})),format.raw/*13.6*/("""
    </ul>
    """),_display_(Seq[Any](/*15.6*/if(previews.size == 0)/*15.28*/ {_display_(Seq[Any](format.raw/*15.30*/("""
      <h4>No previews currently associated with this collection</h4>
    """)))})),format.raw/*17.6*/("""
    </div>
    <div class="col-md-12">
    <h3>Previewers available for this collection</h3>
    <ul>
    """),_display_(Seq[Any](/*22.6*/for(p <- previewers; supported <- p.supportedPreviews) yield /*22.60*/ {_display_(Seq[Any](format.raw/*22.62*/("""
      <li>
        <button type="button" class="btn btn-primary btn-xs addPreviewer" data-id=""""),_display_(Seq[Any](/*24.85*/p/*24.86*/.id)),format.raw/*24.89*/("""" data-preview-type=""""),_display_(Seq[Any](/*24.111*/supported)),format.raw/*24.120*/("""">
           <span class="glyphicon glyphicon-plus"></span> Add
        </button>
        <b>Supported Type:</b> """),_display_(Seq[Any](/*27.33*/supported)),format.raw/*27.42*/(""" (<b>Previewer:</b> """),_display_(Seq[Any](/*27.63*/p/*27.64*/.id)),format.raw/*27.67*/(""")
      </li>
    """)))})),format.raw/*29.6*/("""
    </ul>
    </div>
</div>

<script>
$(".addPreviewer").click(function() """),format.raw/*35.37*/("""{"""),format.raw/*35.38*/("""
    var id = $(this).data("id");
    var preview_type = $(this).data("preview-type");
    // empty file
    var data = new FormData();
    data.append("File", new Blob([new Uint8Array([])], """),format.raw/*40.56*/("""{"""),format.raw/*40.57*/("""type: "*/*""""),format.raw/*40.68*/("""}"""),format.raw/*40.69*/("""), "preview");

    // upload empty file as preview
    var request = window.jsRoutes.api.Previews.upload().ajax("""),format.raw/*43.62*/("""{"""),format.raw/*43.63*/("""
            type:        "POST",
            data:        data,
            contentType: false,
            processData: false
        """),format.raw/*48.9*/("""}"""),format.raw/*48.10*/(""");

    request.done(function(response, textStatus, jqXHR) """),format.raw/*50.56*/("""{"""),format.raw/*50.57*/("""
      var preview_id = response.id;
      console.log("Preview created " + preview_id);

      var request = window.jsRoutes.api.Collections.attachPreview(""""),_display_(Seq[Any](/*54.69*/collection_id)),format.raw/*54.82*/("""", preview_id).ajax("""),format.raw/*54.102*/("""{"""),format.raw/*54.103*/("""
        data: JSON.stringify("""),format.raw/*55.30*/("""{"""),format.raw/*55.31*/(""""preview_type": preview_type"""),format.raw/*55.59*/("""}"""),format.raw/*55.60*/("""),
        type: 'POST',
        contentType: "application/json"
      """),format.raw/*58.7*/("""}"""),format.raw/*58.8*/(""");

        request.done(function(response, textStatus, jqXHR) """),format.raw/*60.60*/("""{"""),format.raw/*60.61*/("""
          console.log("Preview associated " + preview_type);
        """),format.raw/*62.9*/("""}"""),format.raw/*62.10*/(""");

        request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*64.63*/("""{"""),format.raw/*64.64*/("""
            console.error("The following error occured: " + textStatus, errorThrown);
        """),format.raw/*66.9*/("""}"""),format.raw/*66.10*/(""");
    """),format.raw/*67.5*/("""}"""),format.raw/*67.6*/(""");

    request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*69.59*/("""{"""),format.raw/*69.60*/("""
        console.error("The following error occured: " + textStatus, errorThrown);
    """),format.raw/*71.5*/("""}"""),format.raw/*71.6*/(""");



"""),format.raw/*75.1*/("""}"""),format.raw/*75.2*/(""");
</script>
""")))})),format.raw/*77.2*/("""
"""))}
    }
    
    def render(collection_id:String,previews:List[Preview],previewers:Array[Previewer]): play.api.templates.HtmlFormat.Appendable = apply(collection_id,previews,previewers)
    
    def f:((String,List[Preview],Array[Previewer]) => play.api.templates.HtmlFormat.Appendable) = (collection_id,previews,previewers) => apply(collection_id,previews,previewers)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/collectionPreviews.scala.html
                    HASH: eee86edbc17c74c74702e86a676882ce2f51b684
                    MATRIX: 631->1|803->79|840->82|875->109|914->111|1126->288|1160->306|1200->308|1269->341|1279->342|1318->359|1369->374|1379->375|1404->378|1447->390|1498->406|1529->428|1569->430|1675->505|1818->613|1888->667|1928->669|2060->765|2070->766|2095->769|2154->791|2186->800|2337->915|2368->924|2425->945|2435->946|2460->949|2510->968|2613->1043|2642->1044|2861->1235|2890->1236|2929->1247|2958->1248|3099->1361|3128->1362|3291->1498|3320->1499|3407->1558|3436->1559|3630->1717|3665->1730|3714->1750|3744->1751|3802->1781|3831->1782|3887->1810|3916->1811|4014->1882|4042->1883|4133->1946|4162->1947|4259->2017|4288->2018|4382->2084|4411->2085|4533->2180|4562->2181|4596->2188|4624->2189|4714->2251|4743->2252|4857->2339|4885->2340|4918->2346|4946->2347|4991->2361
                    LINES: 20->1|23->1|25->3|25->3|25->3|33->11|33->11|33->11|34->12|34->12|34->12|34->12|34->12|34->12|35->13|37->15|37->15|37->15|39->17|44->22|44->22|44->22|46->24|46->24|46->24|46->24|46->24|49->27|49->27|49->27|49->27|49->27|51->29|57->35|57->35|62->40|62->40|62->40|62->40|65->43|65->43|70->48|70->48|72->50|72->50|76->54|76->54|76->54|76->54|77->55|77->55|77->55|77->55|80->58|80->58|82->60|82->60|84->62|84->62|86->64|86->64|88->66|88->66|89->67|89->67|91->69|91->69|93->71|93->71|97->75|97->75|99->77
                    -- GENERATED --
                */
            