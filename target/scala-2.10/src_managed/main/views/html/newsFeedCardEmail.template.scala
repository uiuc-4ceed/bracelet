
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object newsFeedCardEmail extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[models.Event,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(event: models.Event)(implicit user: Option[models.User] = None):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import services.AppConfiguration

import play.Logger

def /*4.2*/linkForObject/*4.15*/(obj_type: String, obj_id: Option[UUID], obj_name: Option[String]):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*4.85*/("""
  """),_display_(Seq[Any](/*5.4*/obj_type/*5.12*/ match/*5.18*/ {/*6.5*/case "file" =>/*6.19*/ {_display_(Seq[Any](format.raw/*6.21*/("""
      file: """),_display_(Seq[Any](/*7.14*/obj_name/*7.22*/.get)),format.raw/*7.26*/("""
    """)))}/*9.5*/case "dataset" =>/*9.22*/ {_display_(Seq[Any](format.raw/*9.24*/("""
      dataset: """),_display_(Seq[Any](/*10.17*/obj_name/*10.25*/.get)),format.raw/*10.29*/("""
    """)))}/*12.5*/case "collection" =>/*12.25*/ {_display_(Seq[Any](format.raw/*12.27*/("""
      collection: """),_display_(Seq[Any](/*13.20*/obj_name/*13.28*/.get)),format.raw/*13.32*/("""
    """)))}/*15.5*/case "user" =>/*15.19*/ {_display_(Seq[Any](format.raw/*15.21*/("""
      user: """),_display_(Seq[Any](/*16.14*/obj_name/*16.22*/.get)),format.raw/*16.26*/("""
    """)))}/*18.5*/case "curation" =>/*18.23*/ {_display_(Seq[Any](format.raw/*18.25*/("""
      curation object: """),_display_(Seq[Any](/*19.25*/obj_name/*19.33*/.get)),format.raw/*19.37*/("""
    """)))}/*21.5*/case "subcollection" =>/*21.28*/ {_display_(Seq[Any](format.raw/*21.30*/("""
      """),_display_(Seq[Any](/*22.8*/Messages("collection.title")/*22.36*/.toLowerCase)),format.raw/*22.48*/(""" : """),_display_(Seq[Any](/*22.52*/obj_name/*22.60*/.get)),format.raw/*22.64*/(""" from collection: """),_display_(Seq[Any](/*22.83*/event/*22.88*/.source_name.get)),format.raw/*22.104*/("""
    """)))}/*24.5*/case _ =>/*24.14*/ {_display_(Seq[Any](format.raw/*24.16*/("""
      """),_display_(Seq[Any](/*25.8*/obj_name/*25.16*/ match/*25.22*/ {/*26.9*/case None =>/*26.21*/ {_display_(Seq[Any](format.raw/*26.23*/("""
          """),_display_(Seq[Any](/*27.12*/obj_type)),format.raw/*27.20*/("""
        """)))}/*29.9*/case Some(name) =>/*29.27*/ {_display_(Seq[Any](format.raw/*29.29*/("""
          """),_display_(Seq[Any](/*30.12*/obj_type)),format.raw/*30.20*/(""": """),_display_(Seq[Any](/*30.23*/name)),format.raw/*30.27*/("""
        """)))}})),format.raw/*32.8*/("""
    """)))}})),format.raw/*34.4*/("""
""")))};def /*37.2*/actionTextForEvent/*37.20*/(event_type_prefix: String) = {{
  event_type_prefix match {
    case "follow" => "is now following "
    case "unfollow" => "stopped following "
    case "upload" => "uploaded "
    case "create" => "created "
    case "delete" => "deleted "
    case "edit" => "edited their comment to "
    case "download" => "downloaded "
    case "postrequest" => "requested access to"
    case _ => {
      Logger.warn("unknown event type prefix in actionTextForEvent: "+event_type_prefix)
      event_type_prefix
    }
  }
}};def /*54.2*/actionTextsForEvent/*54.21*/(event_type_prefix: String) = {{
  event_type_prefix match {
    case "add" => List("added ", " to ")
    case "attach" => List("attached ", " to ")
    case "remove" => List("removed ", " from ")
    case "detach" => List("detached ", " from ")
    case _ => {
      Logger.warn("unknown event type prefix in actionTextsForEvent: "+event_type_prefix)
      List(event_type_prefix+" ", " ")
    }
  }
}};
Seq[Any](format.raw/*1.66*/("""
"""),format.raw/*35.2*/("""

"""),format.raw/*52.2*/("""

"""),format.raw/*65.2*/("""

"""),_display_(Seq[Any](/*67.2*/event/*67.7*/.event_type/*67.18*/ match/*67.24*/ {/*68.3*/case "edit_profile" =>/*68.25*/ {_display_(Seq[Any](format.raw/*68.27*/("""
    edited their profile
  """)))}/*71.3*/case "update_dataset_information" =>/*71.39*/ {_display_(Seq[Any](format.raw/*71.41*/("""
    updated information for """),_display_(Seq[Any](/*72.30*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*72.90*/("""
  """)))}/*74.3*/case "update_space_information" =>/*74.37*/ {_display_(Seq[Any](format.raw/*74.39*/("""
    updated information for """),_display_(Seq[Any](/*75.30*/linkForObject("space", event.object_id, event.object_name))),format.raw/*75.88*/("""
  """)))}/*77.3*/case "update_file_information" =>/*77.36*/ {_display_(Seq[Any](format.raw/*77.38*/("""
    updated information for """),_display_(Seq[Any](/*78.30*/linkForObject("file", event.object_id, event.object_name))),format.raw/*78.87*/("""
  """)))}/*80.3*/case "update_collection_information" =>/*80.42*/ {_display_(Seq[Any](format.raw/*80.44*/("""
    updated information for """),_display_(Seq[Any](/*81.30*/linkForObject("collection", event.object_id, event.object_name))),format.raw/*81.93*/("""
  """)))}/*83.3*/case "update_curation_information" =>/*83.40*/ {_display_(Seq[Any](format.raw/*83.42*/("""
    updated information for """),_display_(Seq[Any](/*84.30*/linkForObject("curation", event.object_id, event.object_name))),format.raw/*84.91*/("""
  """)))}/*86.3*/case "set_note_file" =>/*86.26*/ {_display_(Seq[Any](format.raw/*86.28*/("""
    set note for """),_display_(Seq[Any](/*87.19*/linkForObject("file", event.object_id, event.object_name))),format.raw/*87.76*/("""
  """)))}/*89.3*/case "add_user_to_space" =>/*89.30*/ {_display_(Seq[Any](format.raw/*89.32*/("""
    added """),_display_(Seq[Any](/*90.12*/linkForObject("user", event.targetuser.map(_.id), event.targetuser.map(_.fullName)))),format.raw/*90.95*/(""" as a user to """),_display_(Seq[Any](/*90.110*/linkForObject("space", event.object_id, event.object_name))),format.raw/*90.168*/("""
  """)))}/*92.3*/case "remove_user_from_space" =>/*92.35*/ {_display_(Seq[Any](format.raw/*92.37*/("""
    removed """),_display_(Seq[Any](/*93.14*/linkForObject("user", event.targetuser.map(_.id), event.targetuser.map(_.fullName)))),format.raw/*93.97*/(""" as a user from """),_display_(Seq[Any](/*93.114*/linkForObject("space", event.object_id, event.object_name))),format.raw/*93.172*/("""
  """)))}/*95.3*/case "added_folder" =>/*95.25*/ {_display_(Seq[Any](format.raw/*95.27*/("""
    created a folder in """),_display_(Seq[Any](/*96.26*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*96.86*/("""
  """)))}/*98.3*/case "deleted_folder" =>/*98.27*/ {_display_(Seq[Any](format.raw/*98.29*/("""
    deleted a folder from """),_display_(Seq[Any](/*99.28*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*99.88*/("""
  """)))}/*101.3*/case "updated_folder" =>/*101.27*/ {_display_(Seq[Any](format.raw/*101.29*/("""
    updated a folder in """),_display_(Seq[Any](/*102.26*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*102.86*/("""
  """)))}/*104.3*/case "add_file" | "add_file_1" | "add_file_2" | "add_file_3" =>/*104.66*/ {_display_(Seq[Any](format.raw/*104.68*/("""
    added a file to """),_display_(Seq[Any](/*105.22*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*105.82*/("""
  """)))}/*107.3*/case "add_file_folder" | "add_file_folder_1" | "add_file_folder_2" | "add_file_folder_3" =>/*107.94*/ {_display_(Seq[Any](format.raw/*107.96*/("""
    added a file to a folder within """),_display_(Seq[Any](/*108.38*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*108.98*/("""
  """)))}/*110.3*/case "add_sub_collection" =>/*110.31*/ {_display_(Seq[Any](format.raw/*110.33*/("""
    added """),_display_(Seq[Any](/*111.12*/linkForObject("collection", event.object_id, event.object_name))),format.raw/*111.75*/(""" to
    """),_display_(Seq[Any](/*112.6*/linkForObject("collection", event.source_id, event.source_name))),format.raw/*112.69*/("""
  """)))}/*114.3*/case "addMetadata_file" | "addMetadata_dataset" =>/*114.53*/ {_display_(Seq[Any](format.raw/*114.55*/("""
    added metadata to """),_display_(Seq[Any](/*115.24*/linkForObject(
      event.event_type.substring(event.event_type.indexOf('_')+1),
      event.object_id,
      event.object_name
    ))),format.raw/*119.6*/("""
  """)))}/*121.3*/case "acceptrequest_space" =>/*121.32*/ {_display_(Seq[Any](format.raw/*121.34*/("""
    accepted """),_display_(Seq[Any](/*122.15*/linkForObject("user", event.targetuser.map(_.id), event.targetuser.map(_.fullName)))),format.raw/*122.98*/("""'s request to
    """),_display_(Seq[Any](/*123.6*/linkForObject("space", event.object_id, event.object_name))),format.raw/*123.64*/("""
  """)))}/*125.3*/case "rejectrequest_space" =>/*125.32*/ {_display_(Seq[Any](format.raw/*125.34*/("""
    rejected  """),_display_(Seq[Any](/*126.16*/linkForObject("user", event.targetuser.map(_.id), event.targetuser.map(_.fullName)))),format.raw/*126.99*/("""'s  request to
    """),_display_(Seq[Any](/*127.6*/linkForObject("space", event.object_id, event.object_name))),format.raw/*127.64*/("""
  """)))}/*129.3*/case "tos_update" =>/*129.23*/ {_display_(Seq[Any](format.raw/*129.25*/("""
    updated the <a href=""""),_display_(Seq[Any](/*130.27*/routes/*130.33*/.Application.tos())),format.raw/*130.51*/("""">Terms of Service</a>
  """)))}/*132.3*/case "added_metadata_space" =>/*132.33*/ {_display_(Seq[Any](format.raw/*132.35*/("""
    added a new metadata definition to """),_display_(Seq[Any](/*133.41*/linkForObject("space", event.object_id, event.object_name))),format.raw/*133.99*/("""
  """)))}/*135.3*/case "added_metadata_instance" =>/*135.36*/ {_display_(Seq[Any](format.raw/*135.38*/("""
    added a new metadata definition to """),_display_(Seq[Any](/*136.41*/(AppConfiguration.getDisplayName))),format.raw/*136.74*/("""
  """)))}/*138.3*/case "edit_metadata_space" =>/*138.32*/ {_display_(Seq[Any](format.raw/*138.34*/("""
    edited a metadata definition for """),_display_(Seq[Any](/*139.39*/linkForObject("space", event.object_id, event.object_name))),format.raw/*139.97*/("""
  """)))}/*141.3*/case "edit_metadata_instance" =>/*141.35*/ {_display_(Seq[Any](format.raw/*141.37*/("""
    edited a metadata definition for """),_display_(Seq[Any](/*142.39*/(AppConfiguration.getDisplayName))),format.raw/*142.72*/("""
  """)))}/*144.3*/case "delete_metadata_space" =>/*144.34*/ {_display_(Seq[Any](format.raw/*144.36*/("""
    delete a metadata definition from """),_display_(Seq[Any](/*145.40*/linkForObject("space", event.object_id, event.object_name))),format.raw/*145.98*/("""
  """)))}/*147.3*/case "delete_metadata_instance" =>/*147.37*/ {_display_(Seq[Any](format.raw/*147.39*/("""
    deleted a metadata definition from """),_display_(Seq[Any](/*148.41*/(AppConfiguration.getDisplayName))),format.raw/*148.74*/("""
  """)))}/*150.3*/case "mention_file_comment" =>/*150.33*/ {_display_(Seq[Any](format.raw/*150.35*/("""
    """),_display_(Seq[Any](/*151.6*/event/*151.11*/.targetuser/*151.22*/ match/*151.28*/ {/*152.7*/case Some(u) =>/*152.22*/ {_display_(Seq[Any](format.raw/*152.24*/("""
        was mentioned by """),_display_(Seq[Any](/*153.27*/linkForObject("user", Some(u.id), Some(u.fullName)))),format.raw/*153.78*/(""" in a comment on """),_display_(Seq[Any](/*153.96*/linkForObject("file", event.object_id, event.object_name))),format.raw/*153.153*/("""
      """)))}/*155.7*/case None =>/*155.19*/ {_display_(Seq[Any](format.raw/*155.21*/("""
        was mentioned in a comment on """),_display_(Seq[Any](/*156.40*/linkForObject("file", event.object_id, event.object_name))),format.raw/*156.97*/("""
      """)))}})),format.raw/*158.6*/("""
  """)))}/*160.3*/case "mention_dataset_comment" =>/*160.36*/ {_display_(Seq[Any](format.raw/*160.38*/("""
    """),_display_(Seq[Any](/*161.6*/event/*161.11*/.targetuser/*161.22*/ match/*161.28*/ {/*162.7*/case Some(u) =>/*162.22*/ {_display_(Seq[Any](format.raw/*162.24*/("""
        was mentioned by """),_display_(Seq[Any](/*163.27*/linkForObject("user", Some(u.id), Some(u.fullName)))),format.raw/*163.78*/(""" in a comment on """),_display_(Seq[Any](/*163.96*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*163.156*/("""
      """)))}/*165.7*/case None =>/*165.19*/ {_display_(Seq[Any](format.raw/*165.21*/("""
        was mentioned in a comment on """),_display_(Seq[Any](/*166.40*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*166.100*/("""
      """)))}})),format.raw/*168.6*/("""
  """)))}/*170.3*/case _ =>/*170.12*/ {_display_(Seq[Any](format.raw/*170.14*/("""
    """),_display_(Seq[Any](/*171.6*/defining(event.event_type.split("_"))/*171.43*/ { event_type_split =>_display_(Seq[Any](format.raw/*171.65*/("""
      """),_display_(Seq[Any](/*172.8*/event_type_split/*172.24*/.length/*172.31*/ match/*172.37*/ {/*173.9*/case 2 =>/*173.18*/ {_display_(Seq[Any](format.raw/*173.20*/("""
          """),_display_(Seq[Any](/*174.12*/if(event_type_split(0) == "comment")/*174.48*/{_display_(Seq[Any](format.raw/*174.49*/("""
            commented """),_display_(Seq[Any](/*175.24*/event/*175.29*/.object_name)),format.raw/*175.41*/(""" on """),_display_(Seq[Any](/*175.46*/linkForObject("file", event.source_id, event.source_name))),format.raw/*175.103*/("""
          """)))}/*176.12*/else/*176.16*/{_display_(Seq[Any](format.raw/*176.17*/("""
            """),_display_(Seq[Any](/*177.14*/actionTextForEvent(event_type_split(0)))),format.raw/*177.53*/(""" """),_display_(Seq[Any](/*177.55*/linkForObject(event_type_split(1), event.object_id, event.object_name))),format.raw/*177.125*/("""
          """)))})),format.raw/*178.12*/("""
        """)))}/*180.9*/case 3 =>/*180.18*/ {_display_(Seq[Any](format.raw/*180.20*/("""
          """),_display_(Seq[Any](/*181.12*/defining(actionTextsForEvent(event_type_split(0)))/*181.62*/ { texts =>_display_(Seq[Any](format.raw/*181.73*/("""
            """),_display_(Seq[Any](/*182.14*/if(event.event_type.indexOf("tag")>=0)/*182.52*/{_display_(Seq[Any](format.raw/*182.53*/("""
              """),_display_(Seq[Any](/*183.16*/texts(0))),format.raw/*183.24*/(""" """),_display_(Seq[Any](/*183.26*/linkForObject(event_type_split(1), event.object_id, Option("")))),format.raw/*183.89*/(""" """),_display_(Seq[Any](/*183.91*/texts(1))),format.raw/*183.99*/(""" """),_display_(Seq[Any](/*183.101*/linkForObject(event_type_split(2), event.object_id, event.object_name))),format.raw/*183.171*/("""
            """)))}/*184.14*/else/*184.19*/{_display_(Seq[Any](format.raw/*184.20*/("""
              """),format.raw/*185.50*/("""
              """),_display_(Seq[Any](/*186.16*/if(event.event_type.indexOf("file")>=0 && event.event_type.indexOf("dataset") < 0 && event.event_type.indexOf("folder") < 0)/*186.140*/ {_display_(Seq[Any](format.raw/*186.142*/("""
                added """),_display_(Seq[Any](/*187.24*/event_type_split(2))),format.raw/*187.43*/(""" files to """),_display_(Seq[Any](/*187.54*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*187.114*/("""
            """)))}/*188.15*/else/*188.20*/{_display_(Seq[Any](format.raw/*188.21*/("""
                """),_display_(Seq[Any](/*189.18*/texts(0))),format.raw/*189.26*/(""" """),_display_(Seq[Any](/*189.28*/linkForObject(event_type_split(1), event.object_id, event.object_name))),format.raw/*189.98*/(""" """),_display_(Seq[Any](/*189.100*/texts(1))),format.raw/*189.108*/(""" """),_display_(Seq[Any](/*189.110*/linkForObject(event_type_split(2), event.source_id, event.source_name))),format.raw/*189.180*/("""
              """)))})),format.raw/*190.16*/("""
            """)))})),format.raw/*191.14*/("""
          """)))})),format.raw/*192.12*/("""
        """)))}/*194.9*/case 4 =>/*194.18*/{_display_(Seq[Any](format.raw/*194.19*/("""
          """),format.raw/*195.58*/("""
          """),_display_(Seq[Any](/*196.12*/if(event.event_type.indexOf("file")>=0 && event.event_type.indexOf("dataset") < 0)/*196.94*/ {_display_(Seq[Any](format.raw/*196.96*/("""
            added """),_display_(Seq[Any](/*197.20*/event_type_split(3))),format.raw/*197.39*/(""" files to a folder within """),_display_(Seq[Any](/*197.66*/linkForObject("dataset", event.object_id, event.object_name))),format.raw/*197.126*/("""
          """)))}/*198.13*/else/*198.18*/{_display_(Seq[Any](format.raw/*198.19*/("""
            """),_display_(Seq[Any](/*199.14*/event_type_split(0))),format.raw/*199.33*/(""" """),_display_(Seq[Any](/*199.35*/linkForObject(event_type_split(1), event.object_id, event.object_name))),format.raw/*199.105*/(""" """),_display_(Seq[Any](/*199.107*/event_type_split(1))),format.raw/*199.126*/(""" """),_display_(Seq[Any](/*199.128*/linkForObject(event_type_split(2), event.source_id, event.source_name))),format.raw/*199.198*/("""
          """)))})),format.raw/*200.12*/("""
        """)))}/*202.9*/case _ =>/*202.18*/ {_display_(Seq[Any](format.raw/*202.20*/("""
          """),_display_(Seq[Any](/*203.12*/Logger/*203.18*/.warn("unknown event type encountered: "+event.event_type))),format.raw/*203.76*/("""
          """),_display_(Seq[Any](/*204.12*/event/*204.17*/.event_type)),format.raw/*204.28*/("""
        """)))}})),format.raw/*206.8*/("""
    """)))})),format.raw/*207.6*/("""
  """)))}})),format.raw/*209.2*/("""
"""))}
    }
    
    def render(event:models.Event,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(event)(user)
    
    def f:((models.Event) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (event) => (user) => apply(event)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/newsFeedCardEmail.scala.html
                    HASH: 44b9b902f9ef625dfac9592d1c7d2fd7efb16b30
                    MATRIX: 625->1|819->121|840->134|990->204|1028->208|1044->216|1058->222|1067->229|1089->243|1128->245|1177->259|1193->267|1218->271|1241->282|1266->299|1305->301|1358->318|1375->326|1401->330|1425->341|1454->361|1494->363|1550->383|1567->391|1593->395|1617->406|1640->420|1680->422|1730->436|1747->444|1773->448|1797->459|1824->477|1864->479|1925->504|1942->512|1968->516|1992->527|2024->550|2064->552|2107->560|2144->588|2178->600|2218->604|2235->612|2261->616|2316->635|2330->640|2369->656|2393->667|2411->676|2451->678|2494->686|2511->694|2526->700|2536->711|2557->723|2597->725|2645->737|2675->745|2703->764|2730->782|2770->784|2818->796|2848->804|2887->807|2913->811|2955->829|2993->839|3018->844|3045->862|3573->1379|3601->1398|4033->65|4061->841|4090->1376|4119->1800|4157->1803|4170->1808|4190->1819|4205->1825|4215->1830|4246->1852|4286->1854|4333->1886|4378->1922|4418->1924|4484->1954|4566->2014|4588->2021|4631->2055|4671->2057|4737->2087|4817->2145|4839->2152|4881->2185|4921->2187|4987->2217|5066->2274|5088->2281|5136->2320|5176->2322|5242->2352|5327->2415|5349->2422|5395->2459|5435->2461|5501->2491|5584->2552|5606->2559|5638->2582|5678->2584|5733->2603|5812->2660|5834->2667|5870->2694|5910->2696|5958->2708|6063->2791|6115->2806|6196->2864|6218->2871|6259->2903|6299->2905|6349->2919|6454->3002|6508->3019|6589->3077|6611->3084|6642->3106|6682->3108|6744->3134|6826->3194|6848->3201|6881->3225|6921->3227|6985->3255|7067->3315|7090->3322|7124->3346|7165->3348|7228->3374|7311->3434|7334->3441|7407->3504|7448->3506|7507->3528|7590->3588|7613->3595|7714->3686|7755->3688|7830->3726|7913->3786|7936->3793|7974->3821|8015->3823|8064->3835|8150->3898|8195->3907|8281->3970|8304->3977|8364->4027|8405->4029|8466->4053|8622->4187|8645->4194|8684->4223|8725->4225|8777->4240|8883->4323|8938->4342|9019->4400|9042->4407|9081->4436|9122->4438|9175->4454|9281->4537|9337->4557|9418->4615|9441->4622|9471->4642|9512->4644|9576->4671|9592->4677|9633->4695|9678->4724|9718->4754|9759->4756|9837->4797|9918->4855|9941->4862|9984->4895|10025->4897|10103->4938|10159->4971|10182->4978|10221->5007|10262->5009|10338->5048|10419->5106|10442->5113|10484->5145|10525->5147|10601->5186|10657->5219|10680->5226|10721->5257|10762->5259|10839->5299|10920->5357|10943->5364|10987->5398|11028->5400|11106->5441|11162->5474|11185->5481|11225->5511|11266->5513|11308->5519|11323->5524|11344->5535|11360->5541|11371->5550|11396->5565|11437->5567|11501->5594|11575->5645|11630->5663|11711->5720|11738->5735|11760->5747|11801->5749|11878->5789|11958->5846|11999->5860|12022->5867|12065->5900|12106->5902|12148->5908|12163->5913|12184->5924|12200->5930|12211->5939|12236->5954|12277->5956|12341->5983|12415->6034|12470->6052|12554->6112|12581->6127|12603->6139|12644->6141|12721->6181|12805->6241|12846->6255|12869->6262|12888->6271|12929->6273|12971->6279|13018->6316|13079->6338|13123->6346|13149->6362|13166->6369|13182->6375|13193->6386|13212->6395|13253->6397|13302->6409|13348->6445|13388->6446|13449->6470|13464->6475|13499->6487|13541->6492|13622->6549|13654->6561|13668->6565|13708->6566|13759->6580|13821->6619|13860->6621|13954->6691|13999->6703|14028->6722|14047->6731|14088->6733|14137->6745|14197->6795|14247->6806|14298->6820|14346->6858|14386->6859|14439->6875|14470->6883|14509->6885|14595->6948|14634->6950|14665->6958|14705->6960|14799->7030|14833->7044|14847->7049|14887->7050|14931->7100|14984->7116|15119->7240|15161->7242|15222->7266|15264->7285|15312->7296|15396->7356|15430->7371|15444->7376|15484->7377|15539->7395|15570->7403|15609->7405|15702->7475|15742->7477|15774->7485|15814->7487|15908->7557|15957->7573|16004->7587|16049->7599|16078->7618|16097->7627|16137->7628|16177->7686|16226->7698|16318->7780|16359->7782|16416->7802|16458->7821|16522->7848|16606->7908|16638->7921|16652->7926|16692->7927|16743->7941|16785->7960|16824->7962|16918->8032|16958->8034|17001->8053|17041->8055|17135->8125|17180->8137|17209->8156|17228->8165|17269->8167|17318->8179|17334->8185|17415->8243|17464->8255|17479->8260|17513->8271|17556->8289|17594->8295|17631->8301
                    LINES: 20->1|25->4|25->4|27->4|28->5|28->5|28->5|28->6|28->6|28->6|29->7|29->7|29->7|30->9|30->9|30->9|31->10|31->10|31->10|32->12|32->12|32->12|33->13|33->13|33->13|34->15|34->15|34->15|35->16|35->16|35->16|36->18|36->18|36->18|37->19|37->19|37->19|38->21|38->21|38->21|39->22|39->22|39->22|39->22|39->22|39->22|39->22|39->22|39->22|40->24|40->24|40->24|41->25|41->25|41->25|41->26|41->26|41->26|42->27|42->27|43->29|43->29|43->29|44->30|44->30|44->30|44->30|45->32|46->34|47->37|47->37|62->54|62->54|74->1|75->35|77->52|79->65|81->67|81->67|81->67|81->67|81->68|81->68|81->68|83->71|83->71|83->71|84->72|84->72|85->74|85->74|85->74|86->75|86->75|87->77|87->77|87->77|88->78|88->78|89->80|89->80|89->80|90->81|90->81|91->83|91->83|91->83|92->84|92->84|93->86|93->86|93->86|94->87|94->87|95->89|95->89|95->89|96->90|96->90|96->90|96->90|97->92|97->92|97->92|98->93|98->93|98->93|98->93|99->95|99->95|99->95|100->96|100->96|101->98|101->98|101->98|102->99|102->99|103->101|103->101|103->101|104->102|104->102|105->104|105->104|105->104|106->105|106->105|107->107|107->107|107->107|108->108|108->108|109->110|109->110|109->110|110->111|110->111|111->112|111->112|112->114|112->114|112->114|113->115|117->119|118->121|118->121|118->121|119->122|119->122|120->123|120->123|121->125|121->125|121->125|122->126|122->126|123->127|123->127|124->129|124->129|124->129|125->130|125->130|125->130|126->132|126->132|126->132|127->133|127->133|128->135|128->135|128->135|129->136|129->136|130->138|130->138|130->138|131->139|131->139|132->141|132->141|132->141|133->142|133->142|134->144|134->144|134->144|135->145|135->145|136->147|136->147|136->147|137->148|137->148|138->150|138->150|138->150|139->151|139->151|139->151|139->151|139->152|139->152|139->152|140->153|140->153|140->153|140->153|141->155|141->155|141->155|142->156|142->156|143->158|144->160|144->160|144->160|145->161|145->161|145->161|145->161|145->162|145->162|145->162|146->163|146->163|146->163|146->163|147->165|147->165|147->165|148->166|148->166|149->168|150->170|150->170|150->170|151->171|151->171|151->171|152->172|152->172|152->172|152->172|152->173|152->173|152->173|153->174|153->174|153->174|154->175|154->175|154->175|154->175|154->175|155->176|155->176|155->176|156->177|156->177|156->177|156->177|157->178|158->180|158->180|158->180|159->181|159->181|159->181|160->182|160->182|160->182|161->183|161->183|161->183|161->183|161->183|161->183|161->183|161->183|162->184|162->184|162->184|163->185|164->186|164->186|164->186|165->187|165->187|165->187|165->187|166->188|166->188|166->188|167->189|167->189|167->189|167->189|167->189|167->189|167->189|167->189|168->190|169->191|170->192|171->194|171->194|171->194|172->195|173->196|173->196|173->196|174->197|174->197|174->197|174->197|175->198|175->198|175->198|176->199|176->199|176->199|176->199|176->199|176->199|176->199|176->199|177->200|178->202|178->202|178->202|179->203|179->203|179->203|180->204|180->204|180->204|181->206|182->207|183->209
                    -- GENERATED --
                */
            