
package views.html.util

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object masonry extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply():play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.1*/("""<script src=""""),_display_(Seq[Any](/*1.15*/routes/*1.21*/.Assets.at("javascripts/lib/masonry.pkgd.min.js"))),format.raw/*1.70*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*2.15*/routes/*2.21*/.Assets.at("javascripts/lib/imagesloaded.pkgd.min.js"))),format.raw/*2.75*/("""" type="text/javascript"></script>

<script type="text/javascript">
    function activate() """),format.raw/*5.25*/("""{"""),format.raw/*5.26*/("""
        // initialize Masonry
        var $container = $('#masonry').masonry();
        // layout Masonry again after all images have loaded
        imagesLoaded( '#masonry', function() """),format.raw/*9.46*/("""{"""),format.raw/*9.47*/("""
            $container.masonry("""),format.raw/*10.32*/("""{"""),format.raw/*10.33*/("""
            itemSelector: '.post-box',
            columnWidth: '.post-box',
            transitionDuration: 4
            """),format.raw/*14.13*/("""}"""),format.raw/*14.14*/(""");
        """),format.raw/*15.9*/("""}"""),format.raw/*15.10*/(""");
    """),format.raw/*16.5*/("""}"""),format.raw/*16.6*/("""
    $(document).ready(function () """),format.raw/*17.35*/("""{"""),format.raw/*17.36*/("""
        activate();
    """),format.raw/*19.5*/("""}"""),format.raw/*19.6*/(""");
    // fire when showing from tab
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) """),format.raw/*21.63*/("""{"""),format.raw/*21.64*/("""
        activate();
    """),format.raw/*23.5*/("""}"""),format.raw/*23.6*/(""")
</script>"""))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/util/masonry.scala.html
                    HASH: 662f7757b47dc65dcd629726dd5684672427fcbe
                    MATRIX: 675->0|724->14|738->20|808->69|892->118|906->124|981->178|1100->270|1128->271|1342->458|1370->459|1430->491|1459->492|1611->616|1640->617|1678->628|1707->629|1741->636|1769->637|1832->672|1861->673|1913->698|1941->699|2068->798|2097->799|2149->824|2177->825
                    LINES: 23->1|23->1|23->1|23->1|24->2|24->2|24->2|27->5|27->5|31->9|31->9|32->10|32->10|36->14|36->14|37->15|37->15|38->16|38->16|39->17|39->17|41->19|41->19|43->21|43->21|45->23|45->23
                    -- GENERATED --
                */
            