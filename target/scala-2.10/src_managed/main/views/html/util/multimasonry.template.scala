
package views.html.util

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object multimasonry extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply():play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.1*/("""<script src=""""),_display_(Seq[Any](/*1.15*/routes/*1.21*/.Assets.at("javascripts/lib/masonry.pkgd.min.js"))),format.raw/*1.70*/("""" type="text/javascript"></script>
<script src=""""),_display_(Seq[Any](/*2.15*/routes/*2.21*/.Assets.at("javascripts/lib/imagesloaded.pkgd.min.js"))),format.raw/*2.75*/("""" type="text/javascript"></script>

<script type="text/javascript">
  function activate() """),format.raw/*5.23*/("""{"""),format.raw/*5.24*/("""
        // initialize Masonry
        var $container = $('.masonry').masonry();
        // layout Masonry again after all images have loaded
        imagesLoaded( '#masonry', function() """),format.raw/*9.46*/("""{"""),format.raw/*9.47*/("""
            $container.masonry("""),format.raw/*10.32*/("""{"""),format.raw/*10.33*/("""
            itemSelector: '.post-box',
            columnWidth: '.post-box',
            transitionDuration: 4
            """),format.raw/*14.13*/("""}"""),format.raw/*14.14*/(""");
        """),format.raw/*15.9*/("""}"""),format.raw/*15.10*/(""");
    """),format.raw/*16.5*/("""}"""),format.raw/*16.6*/("""
    $(document).ready(function () """),format.raw/*17.35*/("""{"""),format.raw/*17.36*/("""
        activate();
    """),format.raw/*19.5*/("""}"""),format.raw/*19.6*/(""");
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) """),format.raw/*20.67*/("""{"""),format.raw/*20.68*/("""
        activate();
    """),format.raw/*22.5*/("""}"""),format.raw/*22.6*/(""")
</script>"""))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:37 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/util/multimasonry.scala.html
                    HASH: 9f55b2c388fe93bfcbd92063a6c6c8d0265a52e4
                    MATRIX: 680->0|729->14|743->20|813->69|897->118|911->124|986->178|1103->268|1131->269|1345->456|1373->457|1433->489|1462->490|1614->614|1643->615|1681->626|1710->627|1744->634|1772->635|1835->670|1864->671|1916->696|1944->697|2041->766|2070->767|2122->792|2150->793
                    LINES: 23->1|23->1|23->1|23->1|24->2|24->2|24->2|27->5|27->5|31->9|31->9|32->10|32->10|36->14|36->14|37->15|37->15|38->16|38->16|39->17|39->17|41->19|41->19|42->20|42->20|44->22|44->22
                    -- GENERATED --
                */
            