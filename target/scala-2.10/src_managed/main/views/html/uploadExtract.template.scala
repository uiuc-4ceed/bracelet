
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object uploadExtract extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[FileMD],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[FileMD])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.60*/("""

"""),format.raw/*4.75*/("""

"""),_display_(Seq[Any](/*6.2*/main("Clowder")/*6.17*/ {_display_(Seq[Any](format.raw/*6.19*/("""
  <div class="page-header">
    <h1>File Upload</h1>
  </div>
  <div class="row">
	<div class="col-md-12">
	  """),_display_(Seq[Any](/*12.5*/form(action = routes.Files.uploadExtract, 'enctype -> "multipart/form-data", 'class -> "form-horizontal")/*12.110*/ {_display_(Seq[Any](format.raw/*12.112*/("""
	    <fieldset>
	      <legend>Select file</legend>
	      """),_display_(Seq[Any](/*15.9*/inputFile(myForm("File")))),format.raw/*15.34*/("""
	    </fieldset>
	    <fieldset id="fileRadioFieldSet" style="margin-top:10px;">
	      """),_display_(Seq[Any](/*18.9*/inputRadioGroup(
	          myForm("datasetLevel").copy(value=myForm("datasetLevel").value.orElse(Some("DatasetLevel")) ),
	          options = options("DatasetLevel"->"Everywhere","FileLevel"->"On file page","None"->"Nowhere"),
	          '_label -> "Show file preview(s)",
	          '_error -> myForm("datasetLevel").error.map(_.withMessage("Select where the preview will be viewable."))))),format.raw/*22.117*/("""
	    </fieldset>
        <div class="form-actions">
          	<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-upload"></span> Upload</button>
        </div>
	  """)))})),format.raw/*27.5*/("""
    </div>
  </div>
  <script>
  		$('#datasetLevel label').css('top', '0px');
  		$('#datasetLevel label').css('left', '0px');
  		$('#datasetLevel label').css('margin-right', '10px');
  </script> 
""")))})),format.raw/*35.2*/("""
"""))}
    }
    
    def render(myForm:Form[FileMD],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm)(user)
    
    def f:((Form[FileMD]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm) => (user) => apply(myForm)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:27 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/uploadExtract.scala.html
                    HASH: 684d951244acc72803975c56d07c0a18f37e67e0
                    MATRIX: 621->1|781->79|813->103|892->59|921->152|958->155|981->170|1020->172|1167->284|1282->389|1323->391|1419->452|1466->477|1591->567|2005->958|2230->1152|2462->1353
                    LINES: 20->1|23->4|23->4|24->1|26->4|28->6|28->6|28->6|34->12|34->12|34->12|37->15|37->15|40->18|44->22|49->27|57->35
                    -- GENERATED --
                */
            