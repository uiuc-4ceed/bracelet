
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object notFound extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(msg: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.51*/("""
"""),_display_(Seq[Any](/*2.2*/main("Not Found")/*2.19*/ {_display_(Seq[Any](format.raw/*2.21*/("""
  <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 text-center">
      <h2>
      """),_display_(Seq[Any](/*6.8*/if(msg != null)/*6.23*/{_display_(Seq[Any](format.raw/*6.24*/("""
        """),_display_(Seq[Any](/*7.10*/msg)),format.raw/*7.13*/("""
      """)))}/*8.8*/else/*8.12*/{_display_(Seq[Any](format.raw/*8.13*/("""
        The resource does not exist.
      """)))})),format.raw/*10.8*/("""
      </h2>
    </div>
  </div>
""")))})))}
    }
    
    def render(msg:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(msg)(user)
    
    def f:((String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (msg) => (user) => apply(msg)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/notFound.scala.html
                    HASH: 6c466d1d74d8e5fb0493f7f93e6bc0bc12b87b9c
                    MATRIX: 610->1|753->50|789->52|814->69|853->71|986->170|1009->185|1047->186|1092->196|1116->199|1141->207|1153->211|1191->212|1267->257
                    LINES: 20->1|23->1|24->2|24->2|24->2|28->6|28->6|28->6|29->7|29->7|30->8|30->8|30->8|32->10
                    -- GENERATED --
                */
            