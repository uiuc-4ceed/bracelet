
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object uploadWithExtraction extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[FileMD],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[FileMD])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

implicit def /*6.2*/implicitFieldConstructor/*6.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.60*/("""



"""),format.raw/*6.75*/("""

<!-- 
The majority of this file has now been changed to be based off elements from the demonstration HTML page
of the blueimp jQuery File Upload library. An open source project locatd here: http://blueimp.github.io/jQuery-File-Upload/
 -->

<!-- Force latest IE rendering engine or ChromeFrame if installed -->
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->

<!-- blueimp Gallery styles - downloaded to make the resource local -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*19.31*/routes/*19.37*/.Assets.at("stylesheets/file-uploader/blueimp-gallery.min.css"))),format.raw/*19.100*/("""">
<!-- Generic page styles -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*21.31*/routes/*21.37*/.Assets.at("stylesheets/file-uploader/style.css"))),format.raw/*21.86*/("""">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*23.31*/routes/*23.37*/.Assets.at("stylesheets/file-uploader/jquery.fileupload.css"))),format.raw/*23.98*/("""">
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*24.31*/routes/*24.37*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-ui.css"))),format.raw/*24.101*/("""">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href=""""),_display_(Seq[Any](/*26.41*/routes/*26.47*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-noscript.css"))),format.raw/*26.117*/(""""></noscript>
<noscript><link rel="stylesheet" href=""""),_display_(Seq[Any](/*27.41*/routes/*27.47*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-ui-noscript.css"))),format.raw/*27.120*/(""""></noscript>

"""),_display_(Seq[Any](/*29.2*/main("Clowder")/*29.17*/ {_display_(Seq[Any](format.raw/*29.19*/("""
<div class="container">
  <div class="page-header">
    <h1>File Upload</h1>
  </div>
	   
    <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" action='"""),_display_(Seq[Any](/*36.36*/routes/*36.42*/.Files.uploadAndExtract)),format.raw/*36.65*/("""' method="POST" enctype="multipart/form-data">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript>Javascript is required to use the file uploader.</noscript>
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-link fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files</span>
                        <!-- The file input had "multiple" removed for the current dataset creation method. -->
                    <input type="file" name="files[]" multiple>
                    <input type="hidden" name="datasetLevel" value="DatasetLevel">
                </span>
                <button type="submit" class="btn btn-link start" type="submit">
                    <span class="glyphicon glyphicon-upload"></span> Start upload
                </button>
                <button type="reset" class="btn btn-link cancel">
                    <span class="glyphicon glyphicon-remove"></span> Cancel upload
                </button>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <label>
                    <input type="checkbox" class="toggle"> Select all
                </label>
                <button type="button" class="btn btn-link delete" style="margin-bottom: 0px">
                    <span class="glyphicon glyphicon-trash"></span> Delete selected
                </button>
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
                
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    </form>
</div>
	
	<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
"""),format.raw/*95.1*/("""{"""),format.raw/*95.2*/("""% for (var i=0, file; file=o.files[i]; i++) """),format.raw/*95.46*/("""{"""),format.raw/*95.47*/(""" %"""),format.raw/*95.49*/("""}"""),format.raw/*95.50*/("""
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">"""),format.raw/*101.29*/("""{"""),format.raw/*101.30*/("""%=file.name%"""),format.raw/*101.42*/("""}"""),format.raw/*101.43*/("""</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            """),format.raw/*109.13*/("""{"""),format.raw/*109.14*/("""% if (!i && !o.options.autoUpload) """),format.raw/*109.49*/("""{"""),format.raw/*109.50*/(""" %"""),format.raw/*109.52*/("""}"""),format.raw/*109.53*/("""
                <button type="button" class="btn btn-link start" disabled>
                    <span class="glyphicon glyphicon-play"></span> Start
                </button>
            """),format.raw/*113.13*/("""{"""),format.raw/*113.14*/("""% """),format.raw/*113.16*/("""}"""),format.raw/*113.17*/(""" %"""),format.raw/*113.19*/("""}"""),format.raw/*113.20*/("""
            """),format.raw/*114.13*/("""{"""),format.raw/*114.14*/("""% if (!i) """),format.raw/*114.24*/("""{"""),format.raw/*114.25*/(""" %"""),format.raw/*114.27*/("""}"""),format.raw/*114.28*/("""
                <button type="button" class="btn btn-link cancel">
                    <span class="glyphicon glyphicon-remove"></span> Cancel
                </button>
            """),format.raw/*118.13*/("""{"""),format.raw/*118.14*/("""% """),format.raw/*118.16*/("""}"""),format.raw/*118.17*/(""" %"""),format.raw/*118.19*/("""}"""),format.raw/*118.20*/("""
        </td>
    </tr>
"""),format.raw/*121.1*/("""{"""),format.raw/*121.2*/("""% """),format.raw/*121.4*/("""}"""),format.raw/*121.5*/(""" %"""),format.raw/*121.7*/("""}"""),format.raw/*121.8*/("""
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
"""),format.raw/*125.1*/("""{"""),format.raw/*125.2*/("""% for (var i=0, file; file=o.files[i]; i++) """),format.raw/*125.46*/("""{"""),format.raw/*125.47*/(""" %"""),format.raw/*125.49*/("""}"""),format.raw/*125.50*/("""
    <tr class="template-download fade">
        """),format.raw/*127.9*/("""{"""),format.raw/*127.10*/("""% if (file.deleteUrl) """),format.raw/*127.32*/("""{"""),format.raw/*127.33*/(""" %"""),format.raw/*127.35*/("""}"""),format.raw/*127.36*/("""
        <td>
            <input type="checkbox" name="delete" value="1" class="toggle">
        </td>
        """),format.raw/*131.9*/("""{"""),format.raw/*131.10*/("""% """),format.raw/*131.12*/("""}"""),format.raw/*131.13*/(""" %"""),format.raw/*131.15*/("""}"""),format.raw/*131.16*/("""
        <td>
            <span class="preview">
                """),format.raw/*134.17*/("""{"""),format.raw/*134.18*/("""% if (file.thumbnailUrl) """),format.raw/*134.43*/("""{"""),format.raw/*134.44*/(""" %"""),format.raw/*134.46*/("""}"""),format.raw/*134.47*/("""
                    <a href=""""),format.raw/*135.30*/("""{"""),format.raw/*135.31*/("""%=file.url%"""),format.raw/*135.42*/("""}"""),format.raw/*135.43*/("""" title=""""),format.raw/*135.52*/("""{"""),format.raw/*135.53*/("""%=file.name%"""),format.raw/*135.65*/("""}"""),format.raw/*135.66*/("""" download=""""),format.raw/*135.78*/("""{"""),format.raw/*135.79*/("""%=file.name%"""),format.raw/*135.91*/("""}"""),format.raw/*135.92*/("""" data-gallery><img src=""""),format.raw/*135.117*/("""{"""),format.raw/*135.118*/("""%=file.thumbnailUrl%"""),format.raw/*135.138*/("""}"""),format.raw/*135.139*/(""""></a>
                """),format.raw/*136.17*/("""{"""),format.raw/*136.18*/("""% """),format.raw/*136.20*/("""}"""),format.raw/*136.21*/(""" %"""),format.raw/*136.23*/("""}"""),format.raw/*136.24*/("""
            </span>
        </td>
        <td>
            <p class="name">
                """),format.raw/*141.17*/("""{"""),format.raw/*141.18*/("""% if (file.url) """),format.raw/*141.34*/("""{"""),format.raw/*141.35*/(""" %"""),format.raw/*141.37*/("""}"""),format.raw/*141.38*/("""
                    <a href=""""),format.raw/*142.30*/("""{"""),format.raw/*142.31*/("""%=file.url%"""),format.raw/*142.42*/("""}"""),format.raw/*142.43*/("""" title=""""),format.raw/*142.52*/("""{"""),format.raw/*142.53*/("""%=file.name%"""),format.raw/*142.65*/("""}"""),format.raw/*142.66*/("""" target="_blank" """),format.raw/*142.84*/("""{"""),format.raw/*142.85*/("""%=file.thumbnailUrl?'data-gallery':''%"""),format.raw/*142.123*/("""}"""),format.raw/*142.124*/(""">"""),format.raw/*142.125*/("""{"""),format.raw/*142.126*/("""%=file.name%"""),format.raw/*142.138*/("""}"""),format.raw/*142.139*/("""</a>
                """),format.raw/*143.17*/("""{"""),format.raw/*143.18*/("""% """),format.raw/*143.20*/("""}"""),format.raw/*143.21*/(""" else """),format.raw/*143.27*/("""{"""),format.raw/*143.28*/(""" %"""),format.raw/*143.30*/("""}"""),format.raw/*143.31*/("""
                    <span>"""),format.raw/*144.27*/("""{"""),format.raw/*144.28*/("""%=file.name%"""),format.raw/*144.40*/("""}"""),format.raw/*144.41*/("""</span>
                """),format.raw/*145.17*/("""{"""),format.raw/*145.18*/("""% """),format.raw/*145.20*/("""}"""),format.raw/*145.21*/(""" %"""),format.raw/*145.23*/("""}"""),format.raw/*145.24*/("""
            </p>
            """),format.raw/*147.13*/("""{"""),format.raw/*147.14*/("""% if (file.error) """),format.raw/*147.32*/("""{"""),format.raw/*147.33*/(""" %"""),format.raw/*147.35*/("""}"""),format.raw/*147.36*/("""
                <div><span class="label label-danger">Error</span> """),format.raw/*148.68*/("""{"""),format.raw/*148.69*/("""%=file.error%"""),format.raw/*148.82*/("""}"""),format.raw/*148.83*/("""</div>
            """),format.raw/*149.13*/("""{"""),format.raw/*149.14*/("""% """),format.raw/*149.16*/("""}"""),format.raw/*149.17*/(""" %"""),format.raw/*149.19*/("""}"""),format.raw/*149.20*/("""
        </td>
        <td>
            <span class="size">"""),format.raw/*152.32*/("""{"""),format.raw/*152.33*/("""%=o.formatFileSize(file.size)%"""),format.raw/*152.63*/("""}"""),format.raw/*152.64*/("""</span>
        </td>
        <td>
            """),format.raw/*155.13*/("""{"""),format.raw/*155.14*/("""% if (file.deleteUrl) """),format.raw/*155.36*/("""{"""),format.raw/*155.37*/(""" %"""),format.raw/*155.39*/("""}"""),format.raw/*155.40*/("""
                <button type="button" class="btn btn-link delete" data-type=""""),format.raw/*156.78*/("""{"""),format.raw/*156.79*/("""%=file.deleteType%"""),format.raw/*156.97*/("""}"""),format.raw/*156.98*/("""" data-url=""""),format.raw/*156.110*/("""{"""),format.raw/*156.111*/("""%=file.deleteUrl%"""),format.raw/*156.128*/("""}"""),format.raw/*156.129*/("""""""),format.raw/*156.130*/("""{"""),format.raw/*156.131*/("""% if (file.deleteWithCredentials) """),format.raw/*156.165*/("""{"""),format.raw/*156.166*/(""" %"""),format.raw/*156.168*/("""}"""),format.raw/*156.169*/(""" data-xhr-fields='"""),format.raw/*156.187*/("""{"""),format.raw/*156.188*/(""""withCredentials":true"""),format.raw/*156.210*/("""}"""),format.raw/*156.211*/("""'"""),format.raw/*156.212*/("""{"""),format.raw/*156.213*/("""% """),format.raw/*156.215*/("""}"""),format.raw/*156.216*/(""" %"""),format.raw/*156.218*/("""}"""),format.raw/*156.219*/(""">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </button>
            """),format.raw/*159.13*/("""{"""),format.raw/*159.14*/("""% """),format.raw/*159.16*/("""}"""),format.raw/*159.17*/(""" else """),format.raw/*159.23*/("""{"""),format.raw/*159.24*/(""" %"""),format.raw/*159.26*/("""}"""),format.raw/*159.27*/("""
                <button type="button" class="btn btn-link cancel">
                    <span class="glyphicon glyphicon-remove"></span> Cancel
                </button>
            """),format.raw/*163.13*/("""{"""),format.raw/*163.14*/("""% """),format.raw/*163.16*/("""}"""),format.raw/*163.17*/(""" %"""),format.raw/*163.19*/("""}"""),format.raw/*163.20*/("""
        </td>
    </tr>
"""),format.raw/*166.1*/("""{"""),format.raw/*166.2*/("""% """),format.raw/*166.4*/("""}"""),format.raw/*166.5*/(""" %"""),format.raw/*166.7*/("""}"""),format.raw/*166.8*/("""
</script>

<!-- The Templates plugin is included to render the upload/download listings - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*170.15*/routes/*170.21*/.Assets.at("javascripts/file-uploader/tmpl.min.js"))),format.raw/*170.72*/(""""></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*172.15*/routes/*172.21*/.Assets.at("javascripts/file-uploader/load-image.all.min.js"))),format.raw/*172.82*/(""""></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*174.15*/routes/*174.21*/.Assets.at("javascripts/file-uploader/canvas-to-blob.min.js"))),format.raw/*174.82*/(""""></script>
<!-- blueimp Gallery script - downloaded to make the resource local-->
<script src=""""),_display_(Seq[Any](/*176.15*/routes/*176.21*/.Assets.at("javascripts/file-uploader/jquery.blueimp-gallery.min.js"))),format.raw/*176.90*/(""""></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src=""""),_display_(Seq[Any](/*178.15*/routes/*178.21*/.Assets.at("javascripts/file-uploader/jquery.iframe-transport.js"))),format.raw/*178.87*/(""""></script>
<!-- The basic File Upload plugin -->
<script src=""""),_display_(Seq[Any](/*180.15*/routes/*180.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload.js"))),format.raw/*180.81*/(""""></script>
<!-- The File Upload processing plugin -->
<script src=""""),_display_(Seq[Any](/*182.15*/routes/*182.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-process.js"))),format.raw/*182.89*/(""""></script>
<!-- The File Upload image preview & resize plugin -->
<script src=""""),_display_(Seq[Any](/*184.15*/routes/*184.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-image.js"))),format.raw/*184.87*/(""""></script>
<!-- The File Upload audio preview plugin -->
<script src=""""),_display_(Seq[Any](/*186.15*/routes/*186.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-audio.js"))),format.raw/*186.87*/(""""></script>
<!-- The File Upload video preview plugin -->
<script src=""""),_display_(Seq[Any](/*188.15*/routes/*188.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-video.js"))),format.raw/*188.87*/(""""></script>
<!-- The File Upload validation plugin -->
<script src=""""),_display_(Seq[Any](/*190.15*/routes/*190.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-validate.js"))),format.raw/*190.90*/(""""></script>
<!-- The File Upload user interface plugin -->
<script src=""""),_display_(Seq[Any](/*192.15*/routes/*192.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-ui.js"))),format.raw/*192.84*/(""""></script>
<!-- The main application script -->
<script src=""""),_display_(Seq[Any](/*194.15*/routes/*194.21*/.Assets.at("javascripts/file-uploader/main.js"))),format.raw/*194.68*/(""""></script>
<!-- Scripts below to facilitate authentication checking on callback before the library uploading is invoked. -->
<script src=""""),_display_(Seq[Any](/*196.15*/routes/*196.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-medici-auth.js"))),format.raw/*196.93*/(""""></script>
<script src=""""),_display_(Seq[Any](/*197.15*/routes/*197.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*197.63*/("""" type="text/javascript"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="js/cors/jquery.xdr-transport.js"></script>
<![endif]-->
	
""")))})),format.raw/*203.2*/("""
"""))}
    }
    
    def render(myForm:Form[FileMD],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm)(user)
    
    def f:((Form[FileMD]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm) => (user) => apply(myForm)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/uploadWithExtraction.scala.html
                    HASH: 98aa85f6ea243e77156547f3120b8479fc81fb1a
                    MATRIX: 628->1|788->81|820->105|899->59|930->154|1471->659|1486->665|1572->728|1670->790|1685->796|1756->845|1917->970|1932->976|2015->1037|2084->1070|2099->1076|2186->1140|2328->1246|2343->1252|2436->1322|2526->1376|2541->1382|2637->1455|2688->1471|2712->1486|2752->1488|2991->1691|3006->1697|3051->1720|6113->4755|6141->4756|6213->4800|6242->4801|6272->4803|6301->4804|6479->4953|6509->4954|6550->4966|6580->4967|6993->5351|7023->5352|7087->5387|7117->5388|7148->5390|7178->5391|7394->5578|7424->5579|7455->5581|7485->5582|7516->5584|7546->5585|7588->5598|7618->5599|7657->5609|7687->5610|7718->5612|7748->5613|7959->5795|7989->5796|8020->5798|8050->5799|8081->5801|8111->5802|8164->5827|8193->5828|8223->5830|8252->5831|8282->5833|8311->5834|8463->5958|8492->5959|8565->6003|8595->6004|8626->6006|8656->6007|8733->6056|8763->6057|8814->6079|8844->6080|8875->6082|8905->6083|9044->6194|9074->6195|9105->6197|9135->6198|9166->6200|9196->6201|9290->6266|9320->6267|9374->6292|9404->6293|9435->6295|9465->6296|9524->6326|9554->6327|9594->6338|9624->6339|9662->6348|9692->6349|9733->6361|9763->6362|9804->6374|9834->6375|9875->6387|9905->6388|9960->6413|9991->6414|10041->6434|10072->6435|10124->6458|10154->6459|10185->6461|10215->6462|10246->6464|10276->6465|10398->6558|10428->6559|10473->6575|10503->6576|10534->6578|10564->6579|10623->6609|10653->6610|10693->6621|10723->6622|10761->6631|10791->6632|10832->6644|10862->6645|10909->6663|10939->6664|11007->6702|11038->6703|11069->6704|11100->6705|11142->6717|11173->6718|11223->6739|11253->6740|11284->6742|11314->6743|11349->6749|11379->6750|11410->6752|11440->6753|11496->6780|11526->6781|11567->6793|11597->6794|11650->6818|11680->6819|11711->6821|11741->6822|11772->6824|11802->6825|11861->6855|11891->6856|11938->6874|11968->6875|11999->6877|12029->6878|12126->6946|12156->6947|12198->6960|12228->6961|12276->6980|12306->6981|12337->6983|12367->6984|12398->6986|12428->6987|12516->7046|12546->7047|12605->7077|12635->7078|12711->7125|12741->7126|12792->7148|12822->7149|12853->7151|12883->7152|12990->7230|13020->7231|13067->7249|13097->7250|13139->7262|13170->7263|13217->7280|13248->7281|13279->7282|13310->7283|13374->7317|13405->7318|13437->7320|13468->7321|13516->7339|13547->7340|13599->7362|13630->7363|13661->7364|13692->7365|13724->7367|13755->7368|13787->7370|13818->7371|13962->7486|13992->7487|14023->7489|14053->7490|14088->7496|14118->7497|14149->7499|14179->7500|14390->7682|14420->7683|14451->7685|14481->7686|14512->7688|14542->7689|14595->7714|14624->7715|14654->7717|14683->7718|14713->7720|14742->7721|14926->7868|14942->7874|15016->7925|15218->8090|15234->8096|15318->8157|15501->8303|15517->8309|15601->8370|15735->8467|15751->8473|15843->8542|15998->8660|16014->8666|16103->8732|16204->8796|16220->8802|16303->8862|16409->8931|16425->8937|16516->9005|16634->9086|16650->9092|16739->9158|16848->9230|16864->9236|16953->9302|17062->9374|17078->9380|17167->9446|17273->9515|17289->9521|17381->9590|17491->9663|17507->9669|17593->9732|17693->9795|17709->9801|17779->9848|17956->9988|17972->9994|18067->10066|18130->10092|18146->10098|18211->10140|18481->10378
                    LINES: 20->1|23->6|23->6|24->1|28->6|41->19|41->19|41->19|43->21|43->21|43->21|45->23|45->23|45->23|46->24|46->24|46->24|48->26|48->26|48->26|49->27|49->27|49->27|51->29|51->29|51->29|58->36|58->36|58->36|117->95|117->95|117->95|117->95|117->95|117->95|123->101|123->101|123->101|123->101|131->109|131->109|131->109|131->109|131->109|131->109|135->113|135->113|135->113|135->113|135->113|135->113|136->114|136->114|136->114|136->114|136->114|136->114|140->118|140->118|140->118|140->118|140->118|140->118|143->121|143->121|143->121|143->121|143->121|143->121|147->125|147->125|147->125|147->125|147->125|147->125|149->127|149->127|149->127|149->127|149->127|149->127|153->131|153->131|153->131|153->131|153->131|153->131|156->134|156->134|156->134|156->134|156->134|156->134|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|158->136|158->136|158->136|158->136|158->136|158->136|163->141|163->141|163->141|163->141|163->141|163->141|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|165->143|165->143|165->143|165->143|165->143|165->143|165->143|165->143|166->144|166->144|166->144|166->144|167->145|167->145|167->145|167->145|167->145|167->145|169->147|169->147|169->147|169->147|169->147|169->147|170->148|170->148|170->148|170->148|171->149|171->149|171->149|171->149|171->149|171->149|174->152|174->152|174->152|174->152|177->155|177->155|177->155|177->155|177->155|177->155|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|185->163|185->163|185->163|185->163|185->163|185->163|188->166|188->166|188->166|188->166|188->166|188->166|192->170|192->170|192->170|194->172|194->172|194->172|196->174|196->174|196->174|198->176|198->176|198->176|200->178|200->178|200->178|202->180|202->180|202->180|204->182|204->182|204->182|206->184|206->184|206->184|208->186|208->186|208->186|210->188|210->188|210->188|212->190|212->190|212->190|214->192|214->192|214->192|216->194|216->194|216->194|218->196|218->196|218->196|219->197|219->197|219->197|225->203
                    -- GENERATED --
                */
            