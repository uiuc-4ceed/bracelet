
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object extractionRequests extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[List[models.ExtractionRequests],Integer,java.util.Date,java.util.Date,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dtsrequests:List[models.ExtractionRequests], size:Integer,startTime: java.util.Date,currentTime:java.util.Date):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import collection.JavaConverters._

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.114*/("""
"""),format.raw/*4.75*/("""

"""),_display_(Seq[Any](/*6.2*/main("Extraction Requests")/*6.29*/{_display_(Seq[Any](format.raw/*6.30*/("""
	<div class="page-header">
		<h1>DTS Requests</h1>
	</div>
	"""),_display_(Seq[Any](/*10.3*/if(size == 0)/*10.16*/ {_display_(Seq[Any](format.raw/*10.18*/("""
		<div class="row">
			<div class="col-md-12">
				No Requests found. Sorry!
			</div>
		</div>
	""")))})),format.raw/*16.3*/("""
	<div>
	<div class="row">
		<div class="col-md-12">
			<p>Server Start Time: """),_display_(Seq[Any](/*20.27*/startTime)),format.raw/*20.36*/("""</p>
			<br>
			<p>Current Time     : """),_display_(Seq[Any](/*22.27*/currentTime)),format.raw/*22.38*/("""</p>
			<br>
		</div>
	</div>
	</div>
  <div class="container" > 
		<div class="row-fluid" >
		  <div>
			<table id='tablec'  style="table-layout: fixed;width: 100%" cellpadding="10"  cellspacing="0" border="1">
			<thead>
				<tr>
				<th>Client IP</th><th>File Name</th><th>File Type</th><th>File Size</th><th>Upload Date</th>
				<th>Extractors</th><th>Start Time</th><th>End Time </th>
				</tr>
			</thead>
			<tbody>
				"""),_display_(Seq[Any](/*38.6*/for(req <- dtsrequests) yield /*38.29*/{_display_(Seq[Any](format.raw/*38.30*/("""
					<tr>
					<td>"""),_display_(Seq[Any](/*40.11*/req/*40.14*/.clientIP)),format.raw/*40.23*/("""</td>
					<td>"""),_display_(Seq[Any](/*41.11*/req/*41.14*/.fileName)),format.raw/*41.23*/("""</td>
					<td>"""),_display_(Seq[Any](/*42.11*/req/*42.14*/.fileType)),format.raw/*42.23*/("""</td>
					<td>"""),_display_(Seq[Any](/*43.11*/req/*43.14*/.fileSize)),format.raw/*43.23*/("""</td>
					<td>"""),_display_(Seq[Any](/*44.11*/req/*44.14*/.uploadDate)),format.raw/*44.25*/("""</td>
					<td>"""),_display_(Seq[Any](/*45.11*/req/*45.14*/.extractors)),format.raw/*45.25*/("""</td>
					<td>"""),_display_(Seq[Any](/*46.11*/req/*46.14*/.startTime)),format.raw/*46.24*/("""</td>
					<td>"""),_display_(Seq[Any](/*47.11*/req/*47.14*/.endTime)),format.raw/*47.22*/("""</td>
					</tr>	
				""")))})),format.raw/*49.6*/("""
		   </tbody>
	       </table>
	    </div>
    </div>
 </div>
""")))})),format.raw/*55.2*/("""
"""))}
    }
    
    def render(dtsrequests:List[models.ExtractionRequests],size:Integer,startTime:java.util.Date,currentTime:java.util.Date): play.api.templates.HtmlFormat.Appendable = apply(dtsrequests,size,startTime,currentTime)
    
    def f:((List[models.ExtractionRequests],Integer,java.util.Date,java.util.Date) => play.api.templates.HtmlFormat.Appendable) = (dtsrequests,size,startTime,currentTime) => apply(dtsrequests,size,startTime,currentTime)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/extractionRequests.scala.html
                    HASH: b55409b000c52760b537c6e28e70110d7b808aa9
                    MATRIX: 663->1|913->168|945->192|1025->113|1053->241|1090->244|1125->271|1163->272|1260->334|1282->347|1322->349|1452->448|1567->527|1598->536|1673->575|1706->586|2169->1014|2208->1037|2247->1038|2304->1059|2316->1062|2347->1071|2399->1087|2411->1090|2442->1099|2494->1115|2506->1118|2537->1127|2589->1143|2601->1146|2632->1155|2684->1171|2696->1174|2729->1185|2781->1201|2793->1204|2826->1215|2878->1231|2890->1234|2922->1244|2974->1260|2986->1263|3016->1271|3070->1294|3165->1358
                    LINES: 20->1|25->4|25->4|26->1|27->4|29->6|29->6|29->6|33->10|33->10|33->10|39->16|43->20|43->20|45->22|45->22|61->38|61->38|61->38|63->40|63->40|63->40|64->41|64->41|64->41|65->42|65->42|65->42|66->43|66->43|66->43|67->44|67->44|67->44|68->45|68->45|68->45|69->46|69->46|69->46|70->47|70->47|70->47|72->49|78->55
                    -- GENERATED --
                */
            