
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object JSConsole extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template0[play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply():play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](_display_(Seq[Any](/*1.2*/play/*1.6*/.api.Play.maybeApplication.map(_.mode)/*1.44*/ match/*1.50*/ {/*2.3*/case Some(play.api.Mode.Prod) =>/*2.35*/ {_display_(Seq[Any](format.raw/*2.37*/("""
    <script>
      console.log("Turning of console.log since we are in production mode");
      var console = """),format.raw/*5.21*/("""{"""),format.raw/*5.22*/("""}"""),format.raw/*5.23*/(""";
      console.log = function()"""),format.raw/*6.31*/("""{"""),format.raw/*6.32*/("""}"""),format.raw/*6.33*/(""";
    </script>
  """)))}/*9.3*/case _ =>/*9.12*/ {}})))}
    }
    
    def render(): play.api.templates.HtmlFormat.Appendable = apply()
    
    def f:(() => play.api.templates.HtmlFormat.Appendable) = () => apply()
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/JSConsole.scala.html
                    HASH: 38bda14f36d707ebfe48d513aecebabed9f1d3b2
                    MATRIX: 681->1|692->5|738->43|752->49|761->54|801->86|840->88|978->199|1006->200|1034->201|1093->233|1121->234|1149->235|1185->257|1202->266
                    LINES: 23->1|23->1|23->1|23->1|23->2|23->2|23->2|26->5|26->5|26->5|27->6|27->6|27->6|29->9|29->9
                    -- GENERATED --
                */
            