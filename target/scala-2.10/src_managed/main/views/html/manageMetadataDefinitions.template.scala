
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object manageMetadataDefinitions extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[List[models.MetadataDefinition],Option[UUID],Option[String],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(metadata: List[models.MetadataDefinition], spaceId: Option[UUID], spaceName: Option[String])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import _root_.util.Formatters._


Seq[Any](format.raw/*1.131*/("""

"""),format.raw/*5.1*/("""
"""),_display_(Seq[Any](/*6.2*/main("Metadata Definitions")/*6.30*/ {_display_(Seq[Any](format.raw/*6.32*/("""
    <script src=""""),_display_(Seq[Any](/*7.19*/routes/*7.25*/.Assets.at("javascripts/metadata/manageDefinitions.js"))),format.raw/*7.80*/("""" type="text/javascript"></script>
	<script>
        $(document).ready(function() """),format.raw/*9.38*/("""{"""),format.raw/*9.39*/("""
            $("#type").change(function() """),format.raw/*10.42*/("""{"""),format.raw/*10.43*/("""
                var definitions = $("#definitions_url");
                var validateButton = $("#validateButton");
                var queryParameter = $("#query_parameter");
                var queryParameterValue = $("#query_parameter_value");
                if ($(this).val() == "string") """),format.raw/*15.48*/("""{"""),format.raw/*15.49*/("""
                    definitions.attr("disabled", true);
                    validateButton.attr("disabled", true);
                    queryParameter.attr("disabled", true);
                    queryParameterValue.attr("disabled", true);
                """),format.raw/*20.17*/("""}"""),format.raw/*20.18*/(""" else if ($(this).val() == "list") """),format.raw/*20.53*/("""{"""),format.raw/*20.54*/("""
                    definitions.removeAttr("disabled");
                    validateButton.removeAttr("disabled");
                    queryParameter.attr("disabled", true);
                    queryParameterValue.attr("disabled", true);
                """),format.raw/*25.17*/("""}"""),format.raw/*25.18*/(""" else if ($(this).val() == "wkt") """),format.raw/*25.52*/("""{"""),format.raw/*25.53*/("""
                    definitions.attr("disabled", true);
                    validateButton.attr("disabled", true);
                    queryParameter.attr("disabled", true);
                    queryParameterValue.attr("disabled", true);
                """),format.raw/*30.17*/("""}"""),format.raw/*30.18*/(""" else """),format.raw/*30.24*/("""{"""),format.raw/*30.25*/("""
                    definitions.removeAttr("disabled");
                    validateButton.removeAttr("disabled");
                    queryParameter.removeAttr("disabled");
                    queryParameterValue.removeAttr("disabled");
                """),format.raw/*35.17*/("""}"""),format.raw/*35.18*/("""
            """),format.raw/*36.13*/("""}"""),format.raw/*36.14*/(""");
            $(function () """),format.raw/*37.27*/("""{"""),format.raw/*37.28*/("""
                $('[data-toggle="popover"]').popover("""),format.raw/*38.54*/("""{"""),format.raw/*38.55*/("""
                    trigger: 'focus'
                """),format.raw/*40.17*/("""}"""),format.raw/*40.18*/(""")
            """),format.raw/*41.13*/("""}"""),format.raw/*41.14*/(""");

            $("#validateButton").click(function(e)"""),format.raw/*43.51*/("""{"""),format.raw/*43.52*/("""
                e.preventDefault();
                // get the value of this metadata definition url from the form
                var definitionsValue = $("#definitions_url").val();
                // get a reference to the form boxes for a query param and value in case this is a queryable list
                var queryParam = $("#query_parameter");
                var queryParamValue = $("#query_parameter_value");
                // set a variable where we will store the query parameter key and value if it is filled out
                var queryString;
                // if this is a queryable list, we will enable the user to add a query parameter that will be registered with the definition
                // we will also allow them to submit a sample value for the query parameter so they can test that the url works
                if (!queryParam.attr("disabled") && queryParam.val() && queryParamValue.val()) """),format.raw/*54.96*/("""{"""),format.raw/*54.97*/("""
                    var paramString = """),format.raw/*55.39*/("""{"""),format.raw/*55.40*/("""}"""),format.raw/*55.41*/(""";
                    paramString[queryParam.val()] = queryParamValue.val();
                    queryString = $.param(paramString);
                """),format.raw/*58.17*/("""}"""),format.raw/*58.18*/("""
                if (queryString) """),format.raw/*59.34*/("""{"""),format.raw/*59.35*/("""
                    definitionsValue = definitionsValue + "?" + queryString;
                """),format.raw/*61.17*/("""}"""),format.raw/*61.18*/("""
                if (definitionsValue) """),format.raw/*62.39*/("""{"""),format.raw/*62.40*/("""
                    var request = $.ajax("""),format.raw/*63.42*/("""{"""),format.raw/*63.43*/("""
                        url: definitionsValue,
                        dataType: 'jsonp',
                        timeout: 1500,
                        error: function() """),format.raw/*67.43*/("""{"""),format.raw/*67.44*/("""
                            $("#validationResults").html(
                                '<div class="alert alert-danger fade in">' +
                                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                                '<strong>Error!</strong> Data was not found at the URL!</div>');
                        """),format.raw/*72.25*/("""}"""),format.raw/*72.26*/("""
                    """),format.raw/*73.21*/("""}"""),format.raw/*73.22*/(""");

                    request.done(function(data, textStatus) """),format.raw/*75.61*/("""{"""),format.raw/*75.62*/("""
                        // TODO: validation of the results would require either knowing which element
                        // to look for in the response, or requiring all API's to use a standard response structure.
                        // For now, if we got a successful response from the GET request, we'll proceed
                        var linkText;
                        if (data && data.length) """),format.raw/*80.50*/("""{"""),format.raw/*80.51*/("""
                            linkText = data.length + " items";
                        """),format.raw/*82.25*/("""}"""),format.raw/*82.26*/(""" else """),format.raw/*82.32*/("""{"""),format.raw/*82.33*/("""
                            linkText = "View";
                        """),format.raw/*84.25*/("""}"""),format.raw/*84.26*/("""
                        $("#validationResults").html(
                            '<div class="alert alert-success fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong>Success!</strong> Data was found at the URL: (<a href=' +
                            definitionsValue + ' target="_blank">' + linkText + '</a>)</div>');
                    """),format.raw/*90.21*/("""}"""),format.raw/*90.22*/(""");
                """),format.raw/*91.17*/("""}"""),format.raw/*91.18*/("""
            """),format.raw/*92.13*/("""}"""),format.raw/*92.14*/(""");

            $("#addDefinition").submit(function(e)"""),format.raw/*94.51*/("""{"""),format.raw/*94.52*/("""
                e.preventDefault();
                var data = """),format.raw/*96.28*/("""{"""),format.raw/*96.29*/("""}"""),format.raw/*96.30*/(""";
                $(this.elements).each(function(index, element)"""),format.raw/*97.63*/("""{"""),format.raw/*97.64*/("""
                    var value = $(element).val();
                    if (value) """),format.raw/*99.32*/("""{"""),format.raw/*99.33*/("""
                        data[$(element).attr('id')] = value;
                    """),format.raw/*101.21*/("""}"""),format.raw/*101.22*/("""
                """),format.raw/*102.17*/("""}"""),format.raw/*102.18*/(""");
                if (data['query_parameter_value']) delete data['query_parameter_value'];
                if (data.type == "list" || data.type == "listjquery" || data.type == "scientific_variable") """),format.raw/*104.109*/("""{"""),format.raw/*104.110*/("""
                    if (!data.definitions_url) """),format.raw/*105.48*/("""{"""),format.raw/*105.49*/("""
                        $("#validationResults").html(
                            '<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong>Error!</strong> Definitions URL is required for Lists</div>');
                        return false;
                    """),format.raw/*111.21*/("""}"""),format.raw/*111.22*/("""
                """),format.raw/*112.17*/("""}"""),format.raw/*112.18*/("""

                addDefinition(data, window.location.href, """"),_display_(Seq[Any](/*114.61*/spaceId)),format.raw/*114.68*/("""");
            """),format.raw/*115.13*/("""}"""),format.raw/*115.14*/(""");

             $('.delete-definition').unbind().on('click', function()"""),format.raw/*117.69*/("""{"""),format.raw/*117.70*/("""
                        var delete_object = $(this);
                        var request = jsRoutes.api.Metadata.deleteDefinition(this.id).ajax("""),format.raw/*119.92*/("""{"""),format.raw/*119.93*/("""
                            type: 'DELETE'
                        """),format.raw/*121.25*/("""}"""),format.raw/*121.26*/(""");

                        request.done(function (response, textStatus, jqXHR) """),format.raw/*123.77*/("""{"""),format.raw/*123.78*/("""
                            console.log("success");
                            delete_object.closest("TR").remove();
                        """),format.raw/*126.25*/("""}"""),format.raw/*126.26*/(""");

                        request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*128.79*/("""{"""),format.raw/*128.80*/("""
                            console.error("The following error occured: " + textStatus, errorThrown);
                            var errMsg = "You must be logged in to delete metadata definition";
                            if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*131.72*/("""{"""),format.raw/*131.73*/("""
                              notify("Metadata definition was not removed due to : " + errorThrown, "error");
                            """),format.raw/*133.29*/("""}"""),format.raw/*133.30*/("""
                        """),format.raw/*134.25*/("""}"""),format.raw/*134.26*/(""");
                     """),format.raw/*135.22*/("""}"""),format.raw/*135.23*/(""");
        """),format.raw/*136.9*/("""}"""),format.raw/*136.10*/(""");

    </script>

	<div class="page-header">


        """),_display_(Seq[Any](/*143.10*/(spaceName, spaceId)/*143.30*/ match/*143.36*/ {/*144.13*/case (Some(space_name), Some(space_id)) =>/*144.55*/ {_display_(Seq[Any](format.raw/*144.57*/("""
                <ol class="breadcrumb">
                    <li><span class="glyphicon glyphicon-hdd"></span><a href=""""),_display_(Seq[Any](/*146.80*/routes/*146.86*/.Spaces.getSpace(space_id))),format.raw/*146.112*/("""" title=""""),_display_(Seq[Any](/*146.122*/space_name)),format.raw/*146.132*/(""""> """),_display_(Seq[Any](/*146.136*/Html(ellipsize(space_name, 18)))),format.raw/*146.167*/("""</a></li>
                    <li><span class="glyphicon glyphicon-list"></span> Metadata Definitions</li>
                </ol>
                <h1>Metadata Terms & Definitions</h1>

                <p> The following metadata terms will be available for this """),_display_(Seq[Any](/*151.78*/Messages("space.title"))),format.raw/*151.101*/("""'s """),_display_(Seq[Any](/*151.105*/Messages("datasets.title"))),format.raw/*151.131*/(""" and files. To add a new term scroll to the bottom of the page.</p>
            """)))}/*153.13*/case (_, _) =>/*153.27*/ {_display_(Seq[Any](format.raw/*153.29*/("""
                <h1>Metadata Terms & Definitions</h1>
                <p>The following metadata definitions will be available in """),_display_(Seq[Any](/*155.77*/Messages("datasets.title"))),format.raw/*155.103*/("""' and files that are not in a """),_display_(Seq[Any](/*155.134*/Messages("space.title"))),format.raw/*155.157*/("""', and inherited by new """),_display_(Seq[Any](/*155.182*/Messages("spaces.title"))),format.raw/*155.206*/(""".</p>
                """)))}})),format.raw/*157.10*/("""

	</div>
	"""),_display_(Seq[Any](/*160.3*/if(metadata.size == 0)/*160.25*/ {_display_(Seq[Any](format.raw/*160.27*/("""
	<div class="row">
		<div class="col-md-12">
			No metadata present.
		</div>
	</div>
	""")))})),format.raw/*166.3*/("""
	"""),_display_(Seq[Any](/*167.3*/if(metadata.size > 0)/*167.24*/ {_display_(Seq[Any](format.raw/*167.26*/("""
        <table class="table">
            <thead>
                <tr>
                    <th>Label <a tabindex="0" role="button" aria-hidden="true" data-toggle="popover" title="Label" data-content="A short label or name."><span class="glyphicon glyphicon-info-sign"></span></a></th>
                    <th>Description <a tabindex="0" role="button" aria-hidden="true" data-toggle="popover" title="Description" data-content="A long description."><span class="glyphicon glyphicon-info-sign"></span></a></th>
                    <th>URI <a tabindex="0" role="button" aria-hidden="true" data-toggle="popover" title="URI" data-content="A unique URI where this metadata is defined. This is not queried or used to populate metadata values, only for reference."><span class="glyphicon glyphicon-info-sign"></span></a></th>
                    <th>Type <a tabindex="0" role="button" aria-hidden="true" data-toggle="popover" title="Metadata Type" data-html=true data-content="Currently supported types:<br /><ul><li>String: this is free-form, plain-text field.</li><li>List: an array or list of metadata options that can be retrieved from a URL. The list type requires you to define a Definitions URL that will return the available metadata options in JSON format.</li><li>Queryable List: similar to list, except that you can also pass a query parameter to filter the list at the Definitions URL.</li><li>Location: also a queryable list, but it expects the response to contain a latitude and longitude.</li></ul>"><span class="glyphicon glyphicon-info-sign"></span></a></th>
                    <th>Definitions URL <a tabindex="0" role="button" data-toggle="popover" title="Metadata Definitions URL" data-content="This URL should return a list of values that the user can select from as possible options for this metadata definition." aria-hidden="true" ><span class="glyphicon glyphicon-info-sign"></span></a></th>
                    <th>Query Parameter <a tabindex="0" role="button" data-toggle="popover" title="Query Parameter" data-content="Some list types support filtering the data by use of a query parameter. Example: example.com?searchterm=value" aria-hidden="true" ><span class="glyphicon glyphicon-info-sign"></span></a></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                """),_display_(Seq[Any](/*181.18*/for(m <- metadata) yield /*181.36*/ {_display_(Seq[Any](format.raw/*181.38*/("""
                    <tr>
                        <td>
                            """),_display_(Seq[Any](/*184.30*/((m.json \ "label").asOpt[String].getOrElse("")))),format.raw/*184.78*/("""
                        </td>
                        <td>
                            """),_display_(Seq[Any](/*187.30*/((m.json \ "description").asOpt[String].getOrElse("")))),format.raw/*187.84*/("""
                        </td>
                        <td>
                            """),_display_(Seq[Any](/*190.30*/((m.json \ "uri").asOpt[String].getOrElse("")))),format.raw/*190.76*/("""
                        </td>
                        <td>
                            """),_display_(Seq[Any](/*193.30*/((m.json \ "type").asOpt[String].getOrElse(""))/*193.77*/ match/*193.83*/ {/*194.33*/case "string" =>/*194.49*/ {_display_(Seq[Any](format.raw/*194.51*/("""String""")))}/*195.33*/case "list" =>/*195.47*/ {_display_(Seq[Any](format.raw/*195.49*/("""List""")))}/*196.33*/case "listjquery" =>/*196.53*/ {_display_(Seq[Any](format.raw/*196.55*/("""Queryable List""")))}/*197.33*/case "scientific_variable" =>/*197.62*/ {_display_(Seq[Any](format.raw/*197.64*/("""Scientific Variable""")))}/*198.33*/case "datetime" =>/*198.51*/ {_display_(Seq[Any](format.raw/*198.53*/("""Date and Time""")))}/*199.33*/case "listgeocode" =>/*199.54*/ {_display_(Seq[Any](format.raw/*199.56*/("""Location""")))}/*200.33*/case "wkt" =>/*200.46*/ {_display_(Seq[Any](format.raw/*200.48*/("""WKT Location""")))}/*201.33*/case _ =>/*201.42*/ {_display_(Seq[Any](format.raw/*201.44*/("""Error: unrecognized type""")))}})),format.raw/*202.30*/("""
                        </td>
                        <td>
                            """),_display_(Seq[Any](/*205.30*/((m.json \ "definitions_url").asOpt[String].getOrElse("")))),format.raw/*205.88*/("""
                        </td>
                        <td>
                            """),_display_(Seq[Any](/*208.30*/((m.json \ "query_parameter").asOpt[String].getOrElse("")))),format.raw/*208.88*/("""
                        </td>
                        <td><a href="#addDefinition" onclick="editDefinition('"""),_display_(Seq[Any](/*210.80*/(m.id))),format.raw/*210.86*/("""', '"""),_display_(Seq[Any](/*210.91*/(m.json))),format.raw/*210.99*/("""', this);">Edit</a>
                            / <a id=""""),_display_(Seq[Any](/*211.39*/m/*211.40*/.id)),format.raw/*211.43*/("""" href="#" title="Delete metadata definition" class="delete-definition">Delete</a>
                        </td>
                    </tr>
                """)))})),format.raw/*214.18*/("""
            </tbody>
        </table>
	""")))})),format.raw/*217.3*/("""

    <hr/>
    <br/>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <span class="definitionAction">Add</span> a Metadata Term <span class="caret"></span>
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <form id="addDefinition" class="form-horizontal">
                        <div class="form-group has-feedback">
                            <div class="col-md-3">
                                <label for="label">Label </label>
                                <a tabindex="0" role="button" aria-hidden="true" data-toggle="popover" title="Label (required)"
                                    data-content="A short label or name.">
                                <span class="glyphicon glyphicon-info-sign"></span></a></div>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="label" placeholder="Label" required />
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="col-md-3">
                                <label for="description">Description </label>
                                <a tabindex="0" role="button" aria-hidden="true" data-toggle="popover" title="Description (Required)"
                                    data-content="A longer description.">
                                    <span class="glyphicon glyphicon-info-sign"></span></a></div>
                            <div class="col-sm-9">
                                <textarea rows="3" class="form-control" id="description" placeholder="Description" required></textarea>
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div  class="col-sm-3"> <label for="uri">URI </label> <a tabindex="0" role="button" aria-hidden="true" data-toggle="popover" title="URI" data-content=""""),_display_(Seq[Any](/*257.181*/Messages("metadata.uri", Messages("space.title")))),format.raw/*257.230*/("""" data-original-title="URI"><span class="glyphicon glyphicon-info-sign"></span></a></div>
                            <div class="col-sm-9">
                                <input type="url" class="form-control" id="uri" placeholder="http://" />
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-3"><label for="type">Type </label> <a tabindex="0" role="button" aria-hidden="true" data-toggle="popover" title="" data-html="true" data-content="Currently supported types:<br /><ul><li>String: this is free-form, plain-text field.</li><li>List: an array or list of metadata options that can be retrieved from a URL. The list type requires you to define a Definitions URL that will return the available metadata options in JSON format.</li><li>Queryable List: similar to list, except that you can also pass a query parameter to filter the list at the Definitions URL.</li><li>Location: also a queryable list, but it expects the response to contain a latitude and longitude.</li></ul>" data-original-title="Metadata Type (required)"><span class="glyphicon glyphicon-info-sign"></span></a></div>
                            <div class="col-sm-4">
                                <select name="type" id="type" class="form-control" required>
                                    <option value="string">String</option>
                                    <option value="list">List</option>
                                    <option value="listjquery">Queryable List</option>
                                    <option value="listgeocode">Location</option>
                                    <option value="wkt">WKT Location</option>
                                    <option value="scientific_variable">Scientific Variable</option>
                                    <option value="datetime">Date and Time</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3"><label for="definitions_url">Definitions URL</label> <a tabindex="0" role="button" data-toggle="popover" title="Metadata Definitions URL" data-content="This URL should return a list of values that the user can select from as possible options for this metadata definition." aria-hidden="true" ><span class="glyphicon glyphicon-info-sign"></span></a></div>
                            <div class="col-sm-7">
                                <input type="url" class="form-control" id="definitions_url" placeholder="http://" disabled />
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-default" id="validateButton" disabled><span class="glyphicon glyphicon-ok"></span> Validate URL</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3"><label for="query_parameter">Query Parameter </label> <a tabindex="0" role="button" data-toggle="popover" title="Query Parameter" data-content="Some list types support filtering the data by use of a query parameter. Example: example.com?searchterm=value" aria-hidden="true" ><span class="glyphicon glyphicon-info-sign"></span></a></div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="query_parameter" placeholder="term" disabled />
                            </div>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="query_parameter_value" placeholder="value" disabled />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-8" id="validationResults"></div>
                        </div>
                        <hr/>
                        <a href="#" class="btn btn-default pull-left" id="cancelButton" onclick="reset(this)" style="display:none;"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
                        <button type="submit" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus"></span><span class="definitionActionButton"> Add</span></button>

                    </form>
                </div>
            </div>
        </div>
    </div>
""")))})),format.raw/*308.2*/("""
"""))}
    }
    
    def render(metadata:List[models.MetadataDefinition],spaceId:Option[UUID],spaceName:Option[String],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(metadata,spaceId,spaceName)(user)
    
    def f:((List[models.MetadataDefinition],Option[UUID],Option[String]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (metadata,spaceId,spaceName) => (user) => apply(metadata,spaceId,spaceName)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:29 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/manageMetadataDefinitions.scala.html
                    HASH: 97650ca8f847c3aad8d4f04727c370fce118d856
                    MATRIX: 680->1|967->130|995->196|1031->198|1067->226|1106->228|1160->247|1174->253|1250->308|1359->390|1387->391|1457->433|1486->434|1809->729|1838->730|2121->985|2150->986|2213->1021|2242->1022|2525->1277|2554->1278|2616->1312|2645->1313|2928->1568|2957->1569|2991->1575|3020->1576|3303->1831|3332->1832|3373->1845|3402->1846|3459->1875|3488->1876|3570->1930|3599->1931|3681->1985|3710->1986|3752->2000|3781->2001|3863->2055|3892->2056|4846->2982|4875->2983|4942->3022|4971->3023|5000->3024|5177->3173|5206->3174|5268->3208|5297->3209|5419->3303|5448->3304|5515->3343|5544->3344|5614->3386|5643->3387|5843->3559|5872->3560|6252->3912|6281->3913|6330->3934|6359->3935|6451->3999|6480->4000|6919->4411|6948->4412|7064->4500|7093->4501|7127->4507|7156->4508|7256->4580|7285->4581|7744->5012|7773->5013|7820->5032|7849->5033|7890->5046|7919->5047|8001->5101|8030->5102|8122->5166|8151->5167|8180->5168|8272->5232|8301->5233|8411->5315|8440->5316|8551->5398|8581->5399|8627->5416|8657->5417|8887->5617|8918->5618|8995->5666|9025->5667|9431->6044|9461->6045|9507->6062|9537->6063|9636->6125|9666->6132|9711->6148|9741->6149|9842->6221|9872->6222|10046->6367|10076->6368|10173->6436|10203->6437|10312->6517|10342->6518|10514->6661|10544->6662|10655->6744|10685->6745|10984->7015|11014->7016|11182->7155|11212->7156|11266->7181|11296->7182|11349->7206|11379->7207|11418->7218|11448->7219|11542->7276|11572->7296|11588->7302|11600->7317|11652->7359|11693->7361|11850->7481|11866->7487|11916->7513|11964->7523|11998->7533|12040->7537|12095->7568|12393->7829|12440->7852|12482->7856|12532->7882|12633->7976|12657->7990|12698->7992|12866->8123|12916->8149|12985->8180|13032->8203|13095->8228|13143->8252|13200->8285|13248->8297|13280->8319|13321->8321|13442->8410|13481->8413|13512->8434|13553->8436|15938->10784|15973->10802|16014->10804|16135->10888|16206->10936|16332->11025|16409->11079|16535->11168|16604->11214|16730->11303|16787->11350|16803->11356|16815->11391|16841->11407|16882->11409|16909->11449|16933->11463|16974->11465|16999->11503|17029->11523|17070->11525|17105->11573|17144->11602|17185->11604|17225->11657|17253->11675|17294->11677|17328->11724|17359->11745|17400->11747|17429->11789|17452->11802|17493->11804|17526->11850|17545->11859|17586->11861|17645->11916|17771->12005|17852->12063|17978->12152|18059->12210|18206->12320|18235->12326|18277->12331|18308->12339|18403->12397|18414->12398|18440->12401|18629->12557|18702->12598|21455->15313|21528->15362|26151->19953
                    LINES: 20->1|26->1|28->5|29->6|29->6|29->6|30->7|30->7|30->7|32->9|32->9|33->10|33->10|38->15|38->15|43->20|43->20|43->20|43->20|48->25|48->25|48->25|48->25|53->30|53->30|53->30|53->30|58->35|58->35|59->36|59->36|60->37|60->37|61->38|61->38|63->40|63->40|64->41|64->41|66->43|66->43|77->54|77->54|78->55|78->55|78->55|81->58|81->58|82->59|82->59|84->61|84->61|85->62|85->62|86->63|86->63|90->67|90->67|95->72|95->72|96->73|96->73|98->75|98->75|103->80|103->80|105->82|105->82|105->82|105->82|107->84|107->84|113->90|113->90|114->91|114->91|115->92|115->92|117->94|117->94|119->96|119->96|119->96|120->97|120->97|122->99|122->99|124->101|124->101|125->102|125->102|127->104|127->104|128->105|128->105|134->111|134->111|135->112|135->112|137->114|137->114|138->115|138->115|140->117|140->117|142->119|142->119|144->121|144->121|146->123|146->123|149->126|149->126|151->128|151->128|154->131|154->131|156->133|156->133|157->134|157->134|158->135|158->135|159->136|159->136|166->143|166->143|166->143|166->144|166->144|166->144|168->146|168->146|168->146|168->146|168->146|168->146|168->146|173->151|173->151|173->151|173->151|174->153|174->153|174->153|176->155|176->155|176->155|176->155|176->155|176->155|177->157|180->160|180->160|180->160|186->166|187->167|187->167|187->167|201->181|201->181|201->181|204->184|204->184|207->187|207->187|210->190|210->190|213->193|213->193|213->193|213->194|213->194|213->194|213->195|213->195|213->195|213->196|213->196|213->196|213->197|213->197|213->197|213->198|213->198|213->198|213->199|213->199|213->199|213->200|213->200|213->200|213->201|213->201|213->201|213->202|216->205|216->205|219->208|219->208|221->210|221->210|221->210|221->210|222->211|222->211|222->211|225->214|228->217|268->257|268->257|319->308
                    -- GENERATED --
                */
            