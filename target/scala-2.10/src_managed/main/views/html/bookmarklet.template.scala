
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object bookmarklet extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(baseurl: String):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.19*/("""

var key = 'r1ek3rs';

/**
 * DTS Bookmarklet
 *
 * Based on: https://gist.github.com/2897748
 */
var DTSBookmarklet = DTSBookmarklet || (DTSBookmarklet = new Bookmarklet("""),format.raw/*10.74*/("""{"""),format.raw/*10.75*/("""
  // debug: true, // use debug to bust the cache on your resources
  css: ['"""),_display_(Seq[Any](/*12.11*/baseurl)),format.raw/*12.18*/("""/assets/stylesheets/dtsstylediv.css',
  '"""),_display_(Seq[Any](/*13.5*/baseurl)),format.raw/*13.12*/("""/assets/javascripts/DTSbookmarklet/css/bootstrap.bd.css',
  '"""),_display_(Seq[Any](/*14.5*/baseurl)),format.raw/*14.12*/("""/assets/javascripts/DTSbookmarklet/css/bootstrap-theme.bd.css'
  ],
  js: ['"""),_display_(Seq[Any](/*16.10*/baseurl)),format.raw/*16.17*/("""/assets/javascripts/DTSbookmarklet/js/lunr.js',
  '"""),_display_(Seq[Any](/*17.5*/baseurl)),format.raw/*17.12*/("""/assets/javascripts/DTSbookmarklet/js/bootstrap.min.js',
  '"""),_display_(Seq[Any](/*18.5*/baseurl)),format.raw/*18.12*/("""/assets/javascripts/handlebars-v1.3.0.js',
  '"""),_display_(Seq[Any](/*19.5*/baseurl)),format.raw/*19.12*/("""/assets/javascripts/handlebars-loader.js',
  '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js'
  ],
  //jqpath: '"""),_display_(Seq[Any](/*22.15*/baseurl)),format.raw/*22.22*/("""/assets/javascripts/jquery-1.10.2.js', // defaults to google cdn-hosted jquery
  ready: function(base) """),format.raw/*23.25*/("""{"""),format.raw/*23.26*/(""" // use base to expose a public method

    base.init = function() """),format.raw/*25.28*/("""{"""),format.raw/*25.29*/("""

      console.log("DTS Bookmarklet started");

      var uploadsPromises = [];
      var filesInfo = new Array();
      var outstandingExtractions = new Array();
      var tags = """),format.raw/*32.18*/("""{"""),format.raw/*32.19*/("""}"""),format.raw/*32.20*/(""";
      var failedCountDown;
      var sucessfullyDone;
      var countDone;
      var status = '';
      var imgdocs = """),format.raw/*37.21*/("""{"""),format.raw/*37.22*/("""}"""),format.raw/*37.23*/(""";

      // index definition
      var index = lunr(function() """),format.raw/*40.35*/("""{"""),format.raw/*40.36*/("""
        this.field('tags', """),format.raw/*41.28*/("""{"""),format.raw/*41.29*/("""
          boost : 10
        """),format.raw/*43.9*/("""}"""),format.raw/*43.10*/(""");
        this.field('metadata', """),format.raw/*44.32*/("""{"""),format.raw/*44.33*/("""boost : 10"""),format.raw/*44.43*/("""}"""),format.raw/*44.44*/(""");
        this.field('url');
        this.ref('id');
      """),format.raw/*47.7*/("""}"""),format.raw/*47.8*/(""");

      // compile modal template and add it to page
      var modalTemplate = Handlebars.getTemplate('"""),_display_(Seq[Any](/*50.52*/baseurl)),format.raw/*50.59*/("""/assets/javascripts/DTSbookmarklet/modal');
      var html = modalTemplate("""),format.raw/*51.32*/("""{"""),format.raw/*51.33*/("""base_url : """"),_display_(Seq[Any](/*51.46*/baseurl)),format.raw/*51.53*/("""/assets/javascripts/DTSbookmarklet""""),format.raw/*51.88*/("""}"""),format.raw/*51.89*/(""");
      jQuery(document.body).append(html);

      jQuery("#DTSquery").keyup(function(event)"""),format.raw/*54.48*/("""{"""),format.raw/*54.49*/("""
          if(event.keyCode == 13)"""),format.raw/*55.34*/("""{"""),format.raw/*55.35*/("""
              jQuery("#DTSSearch").click();
          """),format.raw/*57.11*/("""}"""),format.raw/*57.12*/("""
      """),format.raw/*58.7*/("""}"""),format.raw/*58.8*/(""");

      /**
       * Check the status of an extraction.
       */
      function checkfetch(fileid, g) """),format.raw/*63.38*/("""{"""),format.raw/*63.39*/("""
        console.log('-----[checkfetch-- ', fileid, ' --]-----');
        jQuery.ajax("""),format.raw/*65.21*/("""{"""),format.raw/*65.22*/("""
          url : """"),_display_(Seq[Any](/*66.19*/baseurl)),format.raw/*66.26*/("""/api/extractions/" + fileid + "/metadata?key=" + key,
          accepts : "application/json",
          success : function(data)"""),format.raw/*68.35*/("""{"""),format.raw/*68.36*/("""
            indexResults(data, g)
          """),format.raw/*70.11*/("""}"""),format.raw/*70.12*/("""
        """),format.raw/*71.9*/("""}"""),format.raw/*71.10*/(""");
        console.log('----- END -----[checkfetch-', fileid, '-]---------');
      """),format.raw/*73.7*/("""}"""),format.raw/*73.8*/("""

      /**
        * Index results
        */
        function indexResults(extraction, g) """),format.raw/*78.46*/("""{"""),format.raw/*78.47*/("""
          console.log(extraction);
        // var str = JSON.parse(value);
        console.log('----[value_status_handler]  STATUS OK  [RESPONSE]='.fontcolor("red")+ extraction);

        for ( var l = 0; l < filesInfo.length; l++) """),format.raw/*83.53*/("""{"""),format.raw/*83.54*/("""
          console.log('filesInfo[', l, '].id=', filesInfo[l].id, '  extraction.fild_id=', extraction.file_id);

          if ((filesInfo[l].id) == (extraction.file_id)) """),format.raw/*86.58*/("""{"""),format.raw/*86.59*/("""

            filesInfo[l].tags = extraction.tags;
            console.log('[value_status_handler-', l, '-]  filesInfo[].url='+ filesInfo[l].url);
            console.log('[value_status_handler-', l, '-]  filesInfo[].id='+ filesInfo[l].id);
            console.log('[value_status_handler-', l,'-]  filesInfo[].status=' + filesInfo[l].status);
            console.log('[value_status_handler-', l, '-]  filesInfo[].tags='+ filesInfo[l].tags);

            // execution is done
            if (filesInfo[l].status != 'Done') """),format.raw/*95.48*/("""{"""),format.raw/*95.49*/("""
              filesInfo[l].status = extraction.Status;
              console.log('if status!=Done:[value_status_handler-', l,'-]  filesInfo[].status=' + filesInfo[l].status);

              // parse tags even if not done
              var tlen = filesInfo[l].tags.length;
              var tagsArr = new Array();
              for ( var m = 0; m < tlen; m++) """),format.raw/*102.47*/("""{"""),format.raw/*102.48*/("""
                var vlen = filesInfo[l].tags[m].values.length;
                for ( var n = 0; n < vlen; n++) """),format.raw/*104.49*/("""{"""),format.raw/*104.50*/("""
                  var t = filesInfo[l].tags[m].values[n];
                  tagsArr.push(t);
                """),format.raw/*107.17*/("""}"""),format.raw/*107.18*/("""
              """),format.raw/*108.15*/("""}"""),format.raw/*108.16*/(""" 
              console.log("id=" + filesInfo[l].id, "    tagsArr=",tagsArr, "   url=", filesInfo[l].url);

              // check if metadata is present
              getMetadataAboutFile(filesInfo[l], tagsArr);

              // update cache image metadata
              imgdocs[filesInfo[l].id] = createdoc(filesInfo[l].id, tagsArr, filesInfo[l].url, filesInfo[l].src);

              outstandingExtractions[g] = setTimeout(checkfetch,2000, extraction.file_id,g);
            """),format.raw/*118.13*/("""}"""),format.raw/*118.14*/(""" else """),format.raw/*118.20*/("""{"""),format.raw/*118.21*/("""

              console.log("if status is Done : Do Nothing");
              console.log('else:[value_status_handler-', l,'-]  filesInfo[].status=' + filesInfo[l].status); 
            """),format.raw/*122.13*/("""}"""),format.raw/*122.14*/(""" // end of if-else
            break;
          """),format.raw/*124.11*/("""}"""),format.raw/*124.12*/("""
      """),format.raw/*125.7*/("""}"""),format.raw/*125.8*/(""" //end of for
    """),format.raw/*126.5*/("""}"""),format.raw/*126.6*/("""

    function getMetadataAboutFile(fileInfo, tagsArr) """),format.raw/*128.54*/("""{"""),format.raw/*128.55*/("""
      console.log("Getting metadata about file " + fileInfo.id);
      jQuery.ajax("""),format.raw/*130.19*/("""{"""),format.raw/*130.20*/("""
        url : """"),_display_(Seq[Any](/*131.17*/baseurl)),format.raw/*131.24*/("""/api/files/" + fileInfo.id + "/technicalmetadatajson?key=" + key,
        accepts : "application/json",
        success : function(data)"""),format.raw/*133.33*/("""{"""),format.raw/*133.34*/("""
          console.log("fileInfo " + data);
          console.log("Found metadata on file " + data);
          console.log("Found tags on file " + tagsArr);
          console.log("Found id on file " + fileInfo.id);
          console.log(data);
          filesInfo.metadata = data;
          // index with lunr
          index.add("""),format.raw/*141.21*/("""{"""),format.raw/*141.22*/("""
            id : fileInfo.id,
            tags : tagsArr.join(" "),
            metadata : data
          """),format.raw/*145.11*/("""}"""),format.raw/*145.12*/(""");
        """),format.raw/*146.9*/("""}"""),format.raw/*146.10*/("""
      """),format.raw/*147.7*/("""}"""),format.raw/*147.8*/(""");
    """),format.raw/*148.5*/("""}"""),format.raw/*148.6*/("""

      /**
        * Create document for lunr
        */
      function createdoc(fid, tags, url, src) """),format.raw/*153.47*/("""{"""),format.raw/*153.48*/("""
        var x = """),format.raw/*154.17*/("""{"""),format.raw/*154.18*/("""
          id:fid,
          tags : tags,
          url : url,
          src : src
        """),format.raw/*159.9*/("""}"""),format.raw/*159.10*/(""";
        return x;
      """),format.raw/*161.7*/("""}"""),format.raw/*161.8*/("""

      /**
       * Check status of uploaded files by checking promises
       * array and set timeout for successful uploads.
       */
      function getMetadata()"""),format.raw/*167.29*/("""{"""),format.raw/*167.30*/("""
        var plen=uploadsPromises.length;
        console.log("Promises Array Length:",plen);
        for(var g=0;g<plen;g++) """),format.raw/*170.33*/("""{"""),format.raw/*170.34*/("""
          console.log("Promise status: [",g,"]=", uploadsPromises[g].status);
          // is upload successful?
          if(uploadsPromises[g].status==200)"""),format.raw/*173.45*/("""{"""),format.raw/*173.46*/("""
            pid=uploadsPromises[g].responseText;
            console.log("[getMetadata] responseText: =",pid," index="+g);
            var pidstr=pid.substring(7,pid.length-2);
            console.log("ResponseText id: " , pidstr);
            outstandingExtractions[g] = setTimeout(checkfetch, 5000, pidstr,g);
          """),format.raw/*179.11*/("""}"""),format.raw/*179.12*/(""" else """),format.raw/*179.18*/("""{"""),format.raw/*179.19*/("""
            outstandingExtractions[g] = setTimeout(doNothing,10000,pidstr);
          """),format.raw/*181.11*/("""}"""),format.raw/*181.12*/("""
        """),format.raw/*182.9*/("""}"""),format.raw/*182.10*/("""
      """),format.raw/*183.7*/("""}"""),format.raw/*183.8*/("""

      function doNothing(id) """),format.raw/*185.30*/("""{"""),format.raw/*185.31*/("""
        console.log("----Doing  Nothing ---");
      """),format.raw/*187.7*/("""}"""),format.raw/*187.8*/("""


      /**
       *  Check upload promises to see if upload has complite successfully.
       */
       function checkUploadStatus(id) """),format.raw/*193.39*/("""{"""),format.raw/*193.40*/("""
        for(var u=0;u<uploadsPromises.length;u++)"""),format.raw/*194.50*/("""{"""),format.raw/*194.51*/("""
          if(uploadsPromises[u].status==200)"""),format.raw/*195.45*/("""{"""),format.raw/*195.46*/("""
            console.log("promise.responseText=", uploadsPromises[u].responseText,"  id=",id);
            if(uploadsPromises[u].responseText.indexOf(id)!=-1)"""),format.raw/*197.64*/("""{"""),format.raw/*197.65*/("""
              console.log("[checkUploadStatus]:", uploadsPromises[u].responseText);
              return 200;
            """),format.raw/*200.13*/("""}"""),format.raw/*200.14*/("""
          """),format.raw/*201.11*/("""}"""),format.raw/*201.12*/("""
        """),format.raw/*202.9*/("""}"""),format.raw/*202.10*/("""
        return 0;
      """),format.raw/*204.7*/("""}"""),format.raw/*204.8*/("""


      /**
       * Callback after upload is successful.
       */
       function reqProcess(imgurl, src) """),format.raw/*210.41*/("""{"""),format.raw/*210.42*/("""
        return function(fileidjson) """),format.raw/*211.37*/("""{"""),format.raw/*211.38*/("""
          var str = fileidjson;
          var furl = imgurl;
          console.log('----[handler]----  STATUS: OK  [RESPONSE]=' + str.id + '  url: ', furl);
          var fileidObj = new Object();
          fileidObj.url = furl;
          fileidObj.src = src;
          fileidObj.id = str.id;
          fileidObj.tags = '';
          fileidObj.status = '';
          fileidObj.metadata = [];
          console.log('----[handler]---- fileObj pushed to xMap');
          filesInfo.push(fileidObj);

        """),format.raw/*225.9*/("""}"""),format.raw/*225.10*/("""
      """),format.raw/*226.7*/("""}"""),format.raw/*226.8*/("""

      /**
        * Submit upload.
        */
      function startUpload(imgurl, src) """),format.raw/*231.41*/("""{"""),format.raw/*231.42*/("""
        var fd = """),format.raw/*232.18*/("""{"""),format.raw/*232.19*/("""}"""),format.raw/*232.20*/(""";
        fd['fileurl'] = imgurl;
        var request = jQuery.ajax("""),format.raw/*234.35*/("""{"""),format.raw/*234.36*/("""
          type : "POST",
          url : """"),_display_(Seq[Any](/*236.19*/baseurl)),format.raw/*236.26*/("""/api/extractions/upload_url?key=" + key,
          accepts : "application/json",
          processData : false,
          contentType : "application/json",
          data : JSON.stringify(fd),
          success : reqProcess(imgurl, src)
        """),format.raw/*242.9*/("""}"""),format.raw/*242.10*/(""");

        uploadsPromises.push(request);
      """),format.raw/*245.7*/("""}"""),format.raw/*245.8*/("""

      /**
        * Search index.
        */
      jQuery('#DTSSearch').click(function() """),format.raw/*250.45*/("""{"""),format.raw/*250.46*/("""
        var keyword = jQuery('#DTSquery').val();
        console.log("Searching for " + keyword);
        if (keyword != null) """),format.raw/*253.30*/("""{"""),format.raw/*253.31*/("""
          // console.log('------Results:--- ', index.search(keyword));
          // clear table
          jQuery('#DTSSearchResults tbody tr').remove();
          // create row template
          var searchResults = index.search(keyword);
          var slen = searchResults.length;
          if (slen > 0) """),format.raw/*260.25*/("""{"""),format.raw/*260.26*/("""
            var r = new Array();
            for ( var i = 0; i < slen; i++) """),format.raw/*262.45*/("""{"""),format.raw/*262.46*/("""
              var id = searchResults[i].ref;
              console.log("Search result: ",imgdocs[searchResults[i].ref].tags, "   url:", imgdocs[searchResults[i].ref].url);
              r.push(imgdocs[searchResults[i].ref].url);

              // find image on page
              var original = jQuery("img[src='" + imgdocs[searchResults[i].ref].src + "']")[0];

              console.log(original);

              var rowTemplate = Handlebars.getTemplate('"""),_display_(Seq[Any](/*272.58*/baseurl)),format.raw/*272.65*/("""/assets/javascripts/DTSbookmarklet/row');
              // make copy to get rendered size
              var image = new Image();
              image.src = original.src;
              // create html
              var html = rowTemplate("""),format.raw/*277.38*/("""{"""),format.raw/*277.39*/(""" src : imgdocs[searchResults[i].ref].url, width: image.width, height: image.height, 
                                       description: original.alt, 
                                       linkback: """"),_display_(Seq[Any](/*279.52*/baseurl)),format.raw/*279.59*/("""/files/" + imgdocs[searchResults[i].ref].id"""),format.raw/*279.102*/("""}"""),format.raw/*279.103*/(""");
              // add to table
              jQuery('#DTSSearchResults tbody').append(html);
            """),format.raw/*282.13*/("""}"""),format.raw/*282.14*/("""
            console.log('Total number results : ' + slen.toString() + '  Matched Image URLs=' + r);
          """),format.raw/*284.11*/("""}"""),format.raw/*284.12*/(""" else """),format.raw/*284.18*/("""{"""),format.raw/*284.19*/("""
            console.log("Not Found");
          """),format.raw/*286.11*/("""}"""),format.raw/*286.12*/("""
        """),format.raw/*287.9*/("""}"""),format.raw/*287.10*/("""
        return false;
      """),format.raw/*289.7*/("""}"""),format.raw/*289.8*/(""");


      /**
        * Check for status of uploads.
        */
      var uploadStatus=false;
      var uploadCount;
      var failCount=0;
      var undefinedStatu=0;
      var h=0;
      var b=0;

      function trackCounts()"""),format.raw/*302.29*/("""{"""),format.raw/*302.30*/("""
        b++;
        console.log("Number of times Function Entered b= ",b);
        uploadCount=0;
        failCount=0;
        var plen=uploadsPromises.length; 
        console.log("Promises Array Length:",plen);
        for(var g=0;g<plen;g++)"""),format.raw/*309.32*/("""{"""),format.raw/*309.33*/("""
          console.log("Promise status: [",g,"]=", uploadsPromises[g].status);
          if(uploadsPromises[g].status==undefined)"""),format.raw/*311.51*/("""{"""),format.raw/*311.52*/("""
            console.log("Status undefined for g=",g);
          """),format.raw/*313.11*/("""}"""),format.raw/*313.12*/("""else if(uploadsPromises[g].status==200)"""),format.raw/*313.51*/("""{"""),format.raw/*313.52*/("""
            uploadCount++;
            console.log("if: Upload sucuessful for promise p=",g)
          """),format.raw/*316.11*/("""}"""),format.raw/*316.12*/("""else"""),format.raw/*316.16*/("""{"""),format.raw/*316.17*/("""
            console.log("else: Upload not Sucessful for Promise p=",g);
            failCount++;
          """),format.raw/*319.11*/("""}"""),format.raw/*319.12*/("""
        """),format.raw/*320.9*/("""}"""),format.raw/*320.10*/(""" 
        if(plen==(uploadCount+failCount))"""),format.raw/*321.42*/("""{"""),format.raw/*321.43*/("""
          console.log("uploadStatus becomes true");
          uploadStatus=true;
          getMetadata();
        """),format.raw/*325.9*/("""}"""),format.raw/*325.10*/("""
        else"""),format.raw/*326.13*/("""{"""),format.raw/*326.14*/("""
          statusTrackCounts=setTimeout(trackCounts,1000);
          
        """),format.raw/*329.9*/("""}"""),format.raw/*329.10*/("""
      """),format.raw/*330.7*/("""}"""),format.raw/*330.8*/("""

      /**
       * Reset all variables to start over. 
       * ** Work in progress. Not fully functional. **
       */
      function reset() """),format.raw/*336.24*/("""{"""),format.raw/*336.25*/("""

        clearInterval(statusCheck);

        for ( var k = 0; k < outstandingExtractions.length; k++) """),format.raw/*340.66*/("""{"""),format.raw/*340.67*/("""
          clearInterval(outstandingExtractions[k]);
        """),format.raw/*342.9*/("""}"""),format.raw/*342.10*/("""

        // ui counters
        jQuery('#DTSImageFound').text(0);
        jQuery('#DTSSuccessfulExtractions').text(0);
        jQuery('#DTSFailedExtractions').text(0);

        imagesSrc = [];
        uploadsPromises = [];
        filesInfo = new Array();
        outstandingExtractions = new Array();
        tags = """),format.raw/*353.16*/("""{"""),format.raw/*353.17*/("""}"""),format.raw/*353.18*/(""";
        failedCountDown = 0;
        sucessfullyDone = 0;
        countDone = 0;
        status = '';
        imgdocs = """),format.raw/*358.19*/("""{"""),format.raw/*358.20*/("""}"""),format.raw/*358.21*/(""";
        console.log("Reset");
      """),format.raw/*360.7*/("""}"""),format.raw/*360.8*/("""

      function showImages() """),format.raw/*362.29*/("""{"""),format.raw/*362.30*/("""
        // clear table
        jQuery('#DTSSearchResults tbody tr').remove();

        // find all images
        var images = jQuery('img');

        // upload all images
        for (i=0; i<images.length; i++) """),format.raw/*370.41*/("""{"""),format.raw/*370.42*/("""
          // console.log(jQuery(images[i]).prop('src') + " != " + """"),_display_(Seq[Any](/*371.69*/baseurl)),format.raw/*371.76*/("""/assets/javascripts/DTSbookmarklet/browndog-small.png");
          if (jQuery(images[i]).prop('src') != (""""),_display_(Seq[Any](/*372.51*/baseurl)),format.raw/*372.58*/("""/assets/javascripts/DTSbookmarklet/browndog-small.png")) """),format.raw/*372.115*/("""{"""),format.raw/*372.116*/("""
            var rowTemplate = Handlebars.getTemplate('"""),_display_(Seq[Any](/*373.56*/baseurl)),format.raw/*373.63*/("""/assets/javascripts/DTSbookmarklet/row');
            // make copy to get rendered size
            var image = new Image();
            image.src = images[i].src;
            // create html
            var html = rowTemplate("""),format.raw/*378.36*/("""{"""),format.raw/*378.37*/(""" src : jQuery(images[i]).prop('src'), width: image.width, height: image.height, description: images[i].alt"""),format.raw/*378.143*/("""}"""),format.raw/*378.144*/(""");
            // add to page
            jQuery('#DTSSearchResults tbody').append(html);
          """),format.raw/*381.11*/("""}"""),format.raw/*381.12*/("""
        """),format.raw/*382.9*/("""}"""),format.raw/*382.10*/("""
      """),format.raw/*383.7*/("""}"""),format.raw/*383.8*/("""
      jQuery('#DTSListImages').click(showImages);

      function submitImages() """),format.raw/*386.31*/("""{"""),format.raw/*386.32*/("""
        // clear table
        jQuery('#DTSSearchResults tbody tr').remove();

        // reset();

        // find all images
        imagesSrc = jQuery('img').map(function()"""),format.raw/*393.49*/("""{"""),format.raw/*393.50*/("""
          return """),format.raw/*394.18*/("""{"""),format.raw/*394.19*/("""url: jQuery(this).prop('src'), src: jQuery(this).attr('src')"""),format.raw/*394.79*/("""}"""),format.raw/*394.80*/(""";
        """),format.raw/*395.9*/("""}"""),format.raw/*395.10*/(""");

        var numImages = imagesSrc.length;

        // upload all images
        for (i=0; i<imagesSrc.length; i++) """),format.raw/*400.44*/("""{"""),format.raw/*400.45*/("""
          console.log("CHECK " + imagesSrc[i].url + " != " + """"),_display_(Seq[Any](/*401.64*/baseurl)),format.raw/*401.71*/("""/assets/javascripts/DTSbookmarklet/browndog-small.png");
          if (imagesSrc[i].url != """"),_display_(Seq[Any](/*402.37*/baseurl)),format.raw/*402.44*/("""/assets/javascripts/DTSbookmarklet/browndog-small.png") """),format.raw/*402.100*/("""{"""),format.raw/*402.101*/("""
            console.log("Uploading image " + imagesSrc[i].url + " " + imagesSrc[i].src);
            startUpload(imagesSrc[i].url, imagesSrc[i].src);
          """),format.raw/*405.11*/("""}"""),format.raw/*405.12*/(""" else """),format.raw/*405.18*/("""{"""),format.raw/*405.19*/("""
            console.log("Skipping image: " + imagesSrc[i].url + " != " + """"),_display_(Seq[Any](/*406.76*/baseurl)),format.raw/*406.83*/("""/assets/javascripts/DTSbookmarklet/browndog-small.png");
            numImages -= 1;
          """),format.raw/*408.11*/("""}"""),format.raw/*408.12*/("""
        """),format.raw/*409.9*/("""}"""),format.raw/*409.10*/("""
        // update ui
        // FIXME skipping browndog icon in the count; handle 
        jQuery('#DTSImageFound').text(numImages);
        jQuery('#DTSIndexImages').toggleClass('active');
        // launch dog
        addGraphic();
        // track submissions
        setTimeout(trackCounts, 1000);
      """),format.raw/*418.7*/("""}"""),format.raw/*418.8*/("""
      jQuery('#DTSIndexImages').click(submitImages);

      // open modal
      jQuery('#DTSModal').modal("""),format.raw/*422.33*/("""{"""),format.raw/*422.34*/("""backdrop: "static""""),format.raw/*422.52*/("""}"""),format.raw/*422.53*/(""");

      jQuery("#DTSModal").draggable("""),format.raw/*424.37*/("""{"""),format.raw/*424.38*/("""
          handle: ".modal-header"
      """),format.raw/*426.7*/("""}"""),format.raw/*426.8*/(""");

      // tracking outstanding extractions by successful and failed
      function checkResults() """),format.raw/*429.31*/("""{"""),format.raw/*429.32*/("""
        // console.log("Checking submission status");
        countDone=0;
        failedCountDown=0;
        sucessfullyDone=0;

        for ( var j = 0; j < filesInfo.length; j++) """),format.raw/*435.53*/("""{"""),format.raw/*435.54*/("""
          if (filesInfo[j].status == 'Processing') """),format.raw/*436.52*/("""{"""),format.raw/*436.53*/("""
            console.log('---[check Results]---status: Processing');
          """),format.raw/*438.11*/("""}"""),format.raw/*438.12*/(""" else if (filesInfo[j].status == 'Done') """),format.raw/*438.53*/("""{"""),format.raw/*438.54*/("""
            countDone++;
            console.log('---countDone Incremented-- ');
            sucessfullyDone++;
            // jQuery('#DTSSuccessfulExtractions').text(sucessfullyDone);
          """),format.raw/*443.11*/("""}"""),format.raw/*443.12*/(""" else if (filesInfo[j].status == 'Required Extractor is either busy or is not currently running. Try after some time.') """),format.raw/*443.132*/("""{"""),format.raw/*443.133*/("""
            console.log('---[check Results]---status: Required Extractor is either busy or is not currently running. Try after some time.');
          """),format.raw/*445.11*/("""}"""),format.raw/*445.12*/("""
          else"""),format.raw/*446.15*/("""{"""),format.raw/*446.16*/("""
            if(filesInfo[j].status=='')"""),format.raw/*447.40*/("""{"""),format.raw/*447.41*/("""
              console.log('[checkResults]-- Status: is empty');
              if(checkUploadStatus(filesInfo[j].id)==200)"""),format.raw/*449.58*/("""{"""),format.raw/*449.59*/("""
                console.log("--[checkResults] ---wait for execution---");
              """),format.raw/*451.15*/("""}"""),format.raw/*451.16*/("""else"""),format.raw/*451.20*/("""{"""),format.raw/*451.21*/("""
                console.log("--upload has failed, so skip the execution--");
                countDone++;
                failedCountDown++;
              """),format.raw/*455.15*/("""}"""),format.raw/*455.16*/("""
            """),format.raw/*456.13*/("""}"""),format.raw/*456.14*/("""
          """),format.raw/*457.11*/("""}"""),format.raw/*457.12*/("""
        """),format.raw/*458.9*/("""}"""),format.raw/*458.10*/("""
        // TODO update ui counts
        jQuery('#DTSSuccessfulExtractions').text(sucessfullyDone);
        jQuery('#DTSFailedExtractions').text(failedCountDown);
        console.log('Successfull: ' + sucessfullyDone);
        console.log('Failed: ' + failedCountDown);
        if (countDone > 0 && countDone == filesInfo.length) """),format.raw/*464.61*/("""{"""),format.raw/*464.62*/("""
          status = 'Done';
          // TODO update ui done
          jQuery('#DTSIndexImages').toggleClass('active');
          clearTimeout(statusCheck);
        """),format.raw/*469.9*/("""}"""),format.raw/*469.10*/(""" else """),format.raw/*469.16*/("""{"""),format.raw/*469.17*/("""
          statusCheck=setTimeout(checkResults,2000);
        """),format.raw/*471.9*/("""}"""),format.raw/*471.10*/("""
      """),format.raw/*472.7*/("""}"""),format.raw/*472.8*/("""

      var statusCheck = setTimeout(checkResults, 2000);

      // brown dog running across page
      function addGraphic() """),format.raw/*477.29*/("""{"""),format.raw/*477.30*/("""
        //Preload images
        //jQuery.get(protocol + dap + '/dap/images/browndog-small-transparent.gif');
        //jQuery.get(protocol + dap + '/dap/images/poweredby-transparent.gif');

        var graphic = jQuery('<img>')
          .attr('src', '"""),_display_(Seq[Any](/*483.26*/baseurl)),format.raw/*483.33*/("""/assets/javascripts/DTSbookmarklet/browndog-small-transparent.gif')
          .attr('width', '25')
          .attr('id', 'graphic')
          .css('position', 'absolute')
          .css('left', '0px')
          .css('bottom', '25px')
        jQuery("body").append(graphic);

        setTimeout(moveGraphicRight, 10);
      """),format.raw/*492.7*/("""}"""),format.raw/*492.8*/("""

      function moveGraphicRight() """),format.raw/*494.35*/("""{"""),format.raw/*494.36*/("""
        var graphic = document.getElementById('graphic');
        graphic.style.left = parseInt(graphic.style.left) + 25 + 'px';

        if(parseInt(graphic.style.left) < jQuery(window).width() - 50) """),format.raw/*498.72*/("""{"""),format.raw/*498.73*/("""
          setTimeout(moveGraphicRight, 10);
        """),format.raw/*500.9*/("""}"""),format.raw/*500.10*/(""" else """),format.raw/*500.16*/("""{"""),format.raw/*500.17*/("""
          //graphic.remove();
          graphic.parentNode.removeChild(graphic);

          //Add powered by graphic
          graphic = jQuery('<img>')
            .attr('src', '"""),_display_(Seq[Any](/*506.28*/baseurl)),format.raw/*506.35*/("""/assets/javascripts/DTSbookmarklet/poweredby-transparent.gif')
            .attr('width', '100');

          var link = jQuery('<a/>')
            .attr('href', 'http://browndog.ncsa.illinois.edu')
            .attr('id', 'poweredby')
            .css('position', 'fixed')
            .css('right', '10px')
            .css('bottom', '10px')
            .append(graphic);

          jQuery("body").append(link);
        """),format.raw/*518.9*/("""}"""),format.raw/*518.10*/("""
      """),format.raw/*519.7*/("""}"""),format.raw/*519.8*/("""

    """),format.raw/*521.5*/("""}"""),format.raw/*521.6*/("""


        base.init();

    




  """),format.raw/*531.3*/("""}"""),format.raw/*531.4*/("""
"""),format.raw/*532.1*/("""}"""),format.raw/*532.2*/("""));

function Bookmarklet(options)"""),format.raw/*534.30*/("""{"""),format.raw/*534.31*/("""
  // Avoid confusion when setting
  // public methods.
  var self = this;

  // Merges objects. B overwrites A.
  function extend(a, b)"""),format.raw/*540.24*/("""{"""),format.raw/*540.25*/("""
    var c = """),format.raw/*541.13*/("""{"""),format.raw/*541.14*/("""}"""),format.raw/*541.15*/(""";
    for (var key in a) """),format.raw/*542.24*/("""{"""),format.raw/*542.25*/(""" c[key] = a[key]; """),format.raw/*542.43*/("""}"""),format.raw/*542.44*/("""
      for (var key in b) """),format.raw/*543.26*/("""{"""),format.raw/*543.27*/(""" c[key] = b[key]; """),format.raw/*543.45*/("""}"""),format.raw/*543.46*/("""
        return c;
    """),format.raw/*545.5*/("""}"""),format.raw/*545.6*/("""

    function loadCSS(sheets) """),format.raw/*547.30*/("""{"""),format.raw/*547.31*/("""
    // Synchronous loop for css files
    jQuery.each(sheets, function(i, sheet)"""),format.raw/*549.43*/("""{"""),format.raw/*549.44*/("""
      jQuery('<link>').attr("""),format.raw/*550.29*/("""{"""),format.raw/*550.30*/("""
        href: (sheet + cachebuster),
        rel: 'stylesheet'
      """),format.raw/*553.7*/("""}"""),format.raw/*553.8*/(""").prependTo('body');
    """),format.raw/*554.5*/("""}"""),format.raw/*554.6*/(""");
  """),format.raw/*555.3*/("""}"""),format.raw/*555.4*/("""

  function loadJS(scripts)"""),format.raw/*557.27*/("""{"""),format.raw/*557.28*/("""
    // Check if we've processed all
    // of the JS files (or if there are none).
    if (scripts.length === 0) """),format.raw/*560.31*/("""{"""),format.raw/*560.32*/("""
      o.ready(self);
      return;
    """),format.raw/*563.5*/("""}"""),format.raw/*563.6*/("""

    // Load the first js file in the array.
    jQuery.getScript(scripts[0] + cachebuster, function()"""),format.raw/*566.58*/("""{"""),format.raw/*566.59*/("""
        // asyncronous recursion, courtesy Paul Irish.
        loadJS(scripts.slice(1));
      """),format.raw/*569.7*/("""}"""),format.raw/*569.8*/(""");
  """),format.raw/*570.3*/("""}"""),format.raw/*570.4*/("""

  function init(callback) """),format.raw/*572.27*/("""{"""),format.raw/*572.28*/("""
    <!--if(!window.jQuery) """),format.raw/*573.28*/("""{"""),format.raw/*573.29*/("""-->
      // Create jQuery script element.
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = o.jqpath;
      document.body.appendChild(script);

      // exit on jQuery load.
      script.onload = function()"""),format.raw/*581.33*/("""{"""),format.raw/*581.34*/(""" 

        jQuery.noConflict();

                callback(); 
 

        // callback(); 
      """),format.raw/*589.7*/("""}"""),format.raw/*589.8*/(""";
      script.onreadystatechange = function() """),format.raw/*590.46*/("""{"""),format.raw/*590.47*/("""
        if (this.readyState == 'complete') 

        jQuery.noConflict();

                callback(); 



          // callback();



      """),format.raw/*603.7*/("""}"""),format.raw/*603.8*/("""
      <!--"""),format.raw/*604.11*/("""}"""),format.raw/*604.12*/(""" else """),format.raw/*604.18*/("""{"""),format.raw/*604.19*/("""-->
        <!--callback();-->
        <!--"""),format.raw/*606.13*/("""}"""),format.raw/*606.14*/("""-->
      """),format.raw/*607.7*/("""}"""),format.raw/*607.8*/("""

      var defaults = """),format.raw/*609.22*/("""{"""),format.raw/*609.23*/("""
        debug: false
        , css: []
        , js: []
        , jqpath: "//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.js"
      """),format.raw/*614.7*/("""}"""),format.raw/*614.8*/("""

  // If we don't pass options, use the defaults.
  , o = extend(defaults, options)

  , cachebuster = o.debug ?
  ('?v=' + (new Date()).getTime()) : '';


  // Kick it off.
  init(function()"""),format.raw/*624.18*/("""{"""),format.raw/*624.19*/("""
    loadCSS(o.css);
    loadJS(o.js);
  """),format.raw/*627.3*/("""}"""),format.raw/*627.4*/(""");

"""),format.raw/*629.1*/("""}"""),format.raw/*629.2*/(""";"""))}
    }
    
    def render(baseurl:String): play.api.templates.HtmlFormat.Appendable = apply(baseurl)
    
    def f:((String) => play.api.templates.HtmlFormat.Appendable) = (baseurl) => apply(baseurl)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/bookmarklet.scala.html
                    HASH: 1469ac829343d4b49cc8898932a7750885279ddc
                    MATRIX: 593->1|704->18|904->190|933->191|1047->269|1076->276|1153->318|1182->325|1279->387|1308->394|1421->471|1450->478|1537->530|1566->537|1662->598|1691->605|1773->652|1802->659|1969->790|1998->797|2129->900|2158->901|2253->968|2282->969|2491->1150|2520->1151|2549->1152|2697->1272|2726->1273|2755->1274|2846->1337|2875->1338|2931->1366|2960->1367|3017->1397|3046->1398|3108->1432|3137->1433|3175->1443|3204->1444|3291->1504|3319->1505|3461->1611|3490->1618|3593->1693|3622->1694|3671->1707|3700->1714|3763->1749|3792->1750|3913->1843|3942->1844|4004->1878|4033->1879|4116->1934|4145->1935|4179->1942|4207->1943|4340->2048|4369->2049|4483->2135|4512->2136|4567->2155|4596->2162|4752->2290|4781->2291|4854->2336|4883->2337|4919->2346|4948->2347|5059->2431|5087->2432|5207->2524|5236->2525|5497->2758|5526->2759|5724->2929|5753->2930|6303->3452|6332->3453|6721->3813|6751->3814|6892->3926|6922->3927|7061->4037|7091->4038|7135->4053|7165->4054|7673->4533|7703->4534|7738->4540|7768->4541|7982->4726|8012->4727|8089->4775|8119->4776|8154->4783|8183->4784|8229->4802|8258->4803|8342->4858|8372->4859|8485->4943|8515->4944|8569->4961|8599->4968|8764->5104|8794->5105|9153->5435|9183->5436|9319->5543|9349->5544|9388->5555|9418->5556|9453->5563|9482->5564|9517->5571|9546->5572|9679->5676|9709->5677|9755->5694|9785->5695|9904->5786|9934->5787|9988->5813|10017->5814|10212->5980|10242->5981|10397->6107|10427->6108|10614->6266|10644->6267|10996->6590|11026->6591|11061->6597|11091->6598|11207->6685|11237->6686|11274->6695|11304->6696|11339->6703|11368->6704|11428->6735|11458->6736|11540->6790|11569->6791|11735->6928|11765->6929|11844->6979|11874->6980|11948->7025|11978->7026|12165->7184|12195->7185|12347->7308|12377->7309|12417->7320|12447->7321|12484->7330|12514->7331|12567->7356|12596->7357|12734->7466|12764->7467|12830->7504|12860->7505|13394->8011|13424->8012|13459->8019|13488->8020|13605->8108|13635->8109|13682->8127|13712->8128|13742->8129|13839->8197|13869->8198|13950->8242|13980->8249|14253->8494|14283->8495|14360->8544|14389->8545|14509->8636|14539->8637|14696->8765|14726->8766|15062->9073|15092->9074|15199->9152|15229->9153|15725->9612|15755->9619|16019->9854|16049->9855|16289->10058|16319->10065|16392->10108|16423->10109|16559->10216|16589->10217|16729->10328|16759->10329|16794->10335|16824->10336|16902->10385|16932->10386|16969->10395|16999->10396|17056->10425|17085->10426|17342->10654|17372->10655|17647->10901|17677->10902|17835->11031|17865->11032|17959->11097|17989->11098|18057->11137|18087->11138|18220->11242|18250->11243|18283->11247|18313->11248|18450->11356|18480->11357|18517->11366|18547->11367|18619->11410|18649->11411|18792->11526|18822->11527|18864->11540|18894->11541|19000->11619|19030->11620|19065->11627|19094->11628|19268->11773|19298->11774|19431->11878|19461->11879|19550->11940|19580->11941|19927->12259|19957->12260|19987->12261|20138->12383|20168->12384|20198->12385|20264->12423|20293->12424|20352->12454|20382->12455|20624->12668|20654->12669|20760->12738|20790->12745|20934->12852|20964->12859|21051->12916|21082->12917|21175->12973|21205->12980|21460->13206|21490->13207|21626->13313|21657->13314|21786->13414|21816->13415|21853->13424|21883->13425|21918->13432|21947->13433|22058->13515|22088->13516|22293->13692|22323->13693|22370->13711|22400->13712|22489->13772|22519->13773|22557->13783|22587->13784|22735->13903|22765->13904|22866->13968|22896->13975|23026->14068|23056->14075|23142->14131|23173->14132|23363->14293|23393->14294|23428->14300|23458->14301|23571->14377|23601->14384|23725->14479|23755->14480|23792->14489|23822->14490|24159->14799|24188->14800|24324->14907|24354->14908|24401->14926|24431->14927|24500->14967|24530->14968|24599->15009|24628->15010|24758->15111|24788->15112|25000->15295|25030->15296|25111->15348|25141->15349|25249->15428|25279->15429|25349->15470|25379->15471|25605->15668|25635->15669|25785->15789|25816->15790|25997->15942|26027->15943|26071->15958|26101->15959|26170->15999|26200->16000|26351->16122|26381->16123|26499->16212|26529->16213|26562->16217|26592->16218|26777->16374|26807->16375|26849->16388|26879->16389|26919->16400|26949->16401|26986->16410|27016->16411|27376->16742|27406->16743|27599->16908|27629->16909|27664->16915|27694->16916|27784->16978|27814->16979|27849->16986|27878->16987|28033->17113|28063->17114|28355->17369|28385->17376|28736->17699|28765->17700|28830->17736|28860->17737|29091->17939|29121->17940|29202->17993|29232->17994|29267->18000|29297->18001|29515->18182|29545->18189|29993->18609|30023->18610|30058->18617|30087->18618|30121->18624|30150->18625|30214->18661|30243->18662|30272->18663|30301->18664|30364->18698|30394->18699|30559->18835|30589->18836|30631->18849|30661->18850|30691->18851|30745->18876|30775->18877|30822->18895|30852->18896|30907->18922|30937->18923|30984->18941|31014->18942|31065->18965|31094->18966|31154->18997|31184->18998|31294->19079|31324->19080|31382->19109|31412->19110|31510->19180|31539->19181|31592->19206|31621->19207|31654->19212|31683->19213|31740->19241|31770->19242|31913->19356|31943->19357|32011->19397|32040->19398|32172->19501|32202->19502|32326->19598|32355->19599|32388->19604|32417->19605|32474->19633|32504->19634|32561->19662|32591->19663|32888->19931|32918->19932|33041->20027|33070->20028|33146->20075|33176->20076|33346->20218|33375->20219|33415->20230|33445->20231|33480->20237|33510->20238|33582->20281|33612->20282|33650->20292|33679->20293|33731->20316|33761->20317|33927->20455|33956->20456|34177->20648|34207->20649|34276->20690|34305->20691|34337->20695|34366->20696
                    LINES: 20->1|23->1|32->10|32->10|34->12|34->12|35->13|35->13|36->14|36->14|38->16|38->16|39->17|39->17|40->18|40->18|41->19|41->19|44->22|44->22|45->23|45->23|47->25|47->25|54->32|54->32|54->32|59->37|59->37|59->37|62->40|62->40|63->41|63->41|65->43|65->43|66->44|66->44|66->44|66->44|69->47|69->47|72->50|72->50|73->51|73->51|73->51|73->51|73->51|73->51|76->54|76->54|77->55|77->55|79->57|79->57|80->58|80->58|85->63|85->63|87->65|87->65|88->66|88->66|90->68|90->68|92->70|92->70|93->71|93->71|95->73|95->73|100->78|100->78|105->83|105->83|108->86|108->86|117->95|117->95|124->102|124->102|126->104|126->104|129->107|129->107|130->108|130->108|140->118|140->118|140->118|140->118|144->122|144->122|146->124|146->124|147->125|147->125|148->126|148->126|150->128|150->128|152->130|152->130|153->131|153->131|155->133|155->133|163->141|163->141|167->145|167->145|168->146|168->146|169->147|169->147|170->148|170->148|175->153|175->153|176->154|176->154|181->159|181->159|183->161|183->161|189->167|189->167|192->170|192->170|195->173|195->173|201->179|201->179|201->179|201->179|203->181|203->181|204->182|204->182|205->183|205->183|207->185|207->185|209->187|209->187|215->193|215->193|216->194|216->194|217->195|217->195|219->197|219->197|222->200|222->200|223->201|223->201|224->202|224->202|226->204|226->204|232->210|232->210|233->211|233->211|247->225|247->225|248->226|248->226|253->231|253->231|254->232|254->232|254->232|256->234|256->234|258->236|258->236|264->242|264->242|267->245|267->245|272->250|272->250|275->253|275->253|282->260|282->260|284->262|284->262|294->272|294->272|299->277|299->277|301->279|301->279|301->279|301->279|304->282|304->282|306->284|306->284|306->284|306->284|308->286|308->286|309->287|309->287|311->289|311->289|324->302|324->302|331->309|331->309|333->311|333->311|335->313|335->313|335->313|335->313|338->316|338->316|338->316|338->316|341->319|341->319|342->320|342->320|343->321|343->321|347->325|347->325|348->326|348->326|351->329|351->329|352->330|352->330|358->336|358->336|362->340|362->340|364->342|364->342|375->353|375->353|375->353|380->358|380->358|380->358|382->360|382->360|384->362|384->362|392->370|392->370|393->371|393->371|394->372|394->372|394->372|394->372|395->373|395->373|400->378|400->378|400->378|400->378|403->381|403->381|404->382|404->382|405->383|405->383|408->386|408->386|415->393|415->393|416->394|416->394|416->394|416->394|417->395|417->395|422->400|422->400|423->401|423->401|424->402|424->402|424->402|424->402|427->405|427->405|427->405|427->405|428->406|428->406|430->408|430->408|431->409|431->409|440->418|440->418|444->422|444->422|444->422|444->422|446->424|446->424|448->426|448->426|451->429|451->429|457->435|457->435|458->436|458->436|460->438|460->438|460->438|460->438|465->443|465->443|465->443|465->443|467->445|467->445|468->446|468->446|469->447|469->447|471->449|471->449|473->451|473->451|473->451|473->451|477->455|477->455|478->456|478->456|479->457|479->457|480->458|480->458|486->464|486->464|491->469|491->469|491->469|491->469|493->471|493->471|494->472|494->472|499->477|499->477|505->483|505->483|514->492|514->492|516->494|516->494|520->498|520->498|522->500|522->500|522->500|522->500|528->506|528->506|540->518|540->518|541->519|541->519|543->521|543->521|553->531|553->531|554->532|554->532|556->534|556->534|562->540|562->540|563->541|563->541|563->541|564->542|564->542|564->542|564->542|565->543|565->543|565->543|565->543|567->545|567->545|569->547|569->547|571->549|571->549|572->550|572->550|575->553|575->553|576->554|576->554|577->555|577->555|579->557|579->557|582->560|582->560|585->563|585->563|588->566|588->566|591->569|591->569|592->570|592->570|594->572|594->572|595->573|595->573|603->581|603->581|611->589|611->589|612->590|612->590|625->603|625->603|626->604|626->604|626->604|626->604|628->606|628->606|629->607|629->607|631->609|631->609|636->614|636->614|646->624|646->624|649->627|649->627|651->629|651->629
                    -- GENERATED --
                */
            