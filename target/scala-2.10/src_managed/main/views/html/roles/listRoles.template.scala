
package views.html.roles

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listRoles extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[Role],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(roles: List[Role])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission; var prevResourceStr = ""

import play.api.i18n.Messages


Seq[Any](format.raw/*1.57*/("""

<!-- Need this in order to declare the var: -->
"""),format.raw/*6.1*/("""
"""),_display_(Seq[Any](/*7.2*/main("Manage Roles")/*7.22*/ {_display_(Seq[Any](format.raw/*7.24*/("""
    <script src=""""),_display_(Seq[Any](/*8.19*/routes/*8.25*/.Assets.at("javascripts/manageRoles.js"))),format.raw/*8.65*/("""" type="text/javascript"></script>
    <script>
		function handleRemoval(roleid, theElement, url)"""),format.raw/*10.50*/("""{"""),format.raw/*10.51*/("""
			var couldRemove = removeRole(roleid, url);
		"""),format.raw/*12.3*/("""}"""),format.raw/*12.4*/("""
	</script>
    <div class="page-header">
        <h1>Roles</h1>
        <p>The list below shows all the roles available on the system.
            Roles are used to moderate access to """),_display_(Seq[Any](/*17.51*/Messages("spaces.title"))),format.raw/*17.75*/(""", the top data Spaces.
            Use the create new role button to create a new one.</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*22.14*/if(roles.isEmpty)/*22.31*/ {_display_(Seq[Any](format.raw/*22.33*/("""
                <div>No roles defined</div>
            """)))})),format.raw/*24.14*/("""
            <div><a href=""""),_display_(Seq[Any](/*25.28*/routes/*25.34*/.Admin.createRole())),format.raw/*25.53*/("""" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Create New Role</a></div>
            """),_display_(Seq[Any](/*26.14*/if(!roles.isEmpty)/*26.32*/ {_display_(Seq[Any](format.raw/*26.34*/("""
                <table class="table">
                    <colgroup>
                        <col class="col-md-2">
                        <col class="col-md-4">
                        <col class="col-md-4">
                        <col class="col-md-2">
                    </colgroup>
                    <thead><tr><th>Name</th><th>Description</th><th>Permissions</th><th>Edit / Delete</th></tr></thead>
                    """),_display_(Seq[Any](/*35.22*/for(role <- roles) yield /*35.40*/ {_display_(Seq[Any](format.raw/*35.42*/("""
                        <tr><td>"""),_display_(Seq[Any](/*36.34*/role/*36.38*/.name)),format.raw/*36.43*/("""</td><td>"""),_display_(Seq[Any](/*36.53*/role/*36.57*/.description)),format.raw/*36.69*/("""</td><td id="permission">
                        """),_display_(Seq[Any](/*37.26*/if(!role.permissions.isEmpty)/*37.55*/ {_display_(Seq[Any](format.raw/*37.57*/("""
                            <table class="table-progressive">
                                <tr><td>
                                    """),_display_(Seq[Any](/*40.38*/for( p <- api.Permission.values.toList filter { role.permissions contains _.toString() } map { _.toString() } ) yield /*40.149*/ {_display_(Seq[Any](format.raw/*40.151*/("""
                                        """),_display_(Seq[Any](/*41.42*/defining(p)/*41.53*/ { permissionStr =>_display_(Seq[Any](format.raw/*41.72*/("""
                                            """),_display_(Seq[Any](/*42.46*/defining(p.replaceFirst("(\\p{Ll})(\\p{Lu})", "$1 $2").replaceFirst("\\w+ (.+[^s])[s]?", "$1"))/*42.141*/ { currResourceStr =>_display_(Seq[Any](format.raw/*42.162*/("""
                                                """),_display_(Seq[Any](/*43.50*/if(currResourceStr == prevResourceStr)/*43.88*/ {_display_(Seq[Any](format.raw/*43.90*/("""
                                                    |
                                                """)))}/*45.51*/else/*45.56*/{_display_(Seq[Any](format.raw/*45.57*/("""
                                                    """),_display_(Seq[Any](/*46.54*/{prevResourceStr = currResourceStr})),format.raw/*46.89*/("""
                                                    """),_display_(Seq[Any](/*47.54*/Html("</td></tr><tr><td><b>"+ currResourceStr.replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2") +": </b>"))),format.raw/*47.152*/("""
                                                """)))})),format.raw/*48.50*/("""
                                                """),_display_(Seq[Any](/*49.50*/Html(permissionStr.replaceFirst("(\\p{Ll})(\\p{Lu})", "$1 $2").replaceFirst("(\\w+) .+", "$1")))),format.raw/*49.145*/("""
                                            """)))})),format.raw/*50.46*/("""
                                        """)))})),format.raw/*51.42*/("""
                                    """)))})),format.raw/*52.38*/("""
                                </td></tr>
                            </table>
                        """)))})),format.raw/*55.26*/("""
                        </td>
                        <td>
                            <a href=""""),_display_(Seq[Any](/*58.39*/routes/*58.45*/.Admin.editRole(role.id))),format.raw/*58.69*/("""" class ="btn btn-link"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                            <button onclick="handleRemoval('"""),_display_(Seq[Any](/*59.62*/role/*59.66*/.id)),format.raw/*59.69*/("""',this, '"""),_display_(Seq[Any](/*59.79*/(routes.Admin.listRoles()))),format.raw/*59.105*/("""')"
                            type="button" class="btn btn-link"><span class="glyphicon glyphicon-trash"/></button></td>
                        </tr>
                    """)))})),format.raw/*62.22*/("""
                </table>
            """)))})),format.raw/*64.14*/("""
        </div>
    </div>
""")))})),format.raw/*67.2*/("""

"""))}
    }
    
    def render(roles:List[Role],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(roles)(user)
    
    def f:((List[Role]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (roles) => (user) => apply(roles)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/roles/listRoles.scala.html
                    HASH: aced261d6a6dcda097daf653b2484dea589d28fa
                    MATRIX: 621->1|849->56|925->186|961->188|989->208|1028->210|1082->229|1096->235|1157->275|1282->372|1311->373|1387->422|1415->423|1637->609|1683->633|1888->802|1914->819|1954->821|2044->879|2108->907|2123->913|2164->932|2310->1042|2337->1060|2377->1062|2844->1493|2878->1511|2918->1513|2988->1547|3001->1551|3028->1556|3074->1566|3087->1570|3121->1582|3208->1633|3246->1662|3286->1664|3463->1805|3591->1916|3632->1918|3710->1960|3730->1971|3787->1990|3869->2036|3974->2131|4034->2152|4120->2202|4167->2240|4207->2242|4330->2347|4343->2352|4382->2353|4472->2407|4529->2442|4619->2496|4740->2594|4822->2644|4908->2694|5026->2789|5104->2835|5178->2877|5248->2915|5386->3021|5520->3119|5535->3125|5581->3149|5772->3304|5785->3308|5810->3311|5856->3321|5905->3347|6111->3521|6182->3560|6241->3588
                    LINES: 20->1|26->1|29->6|30->7|30->7|30->7|31->8|31->8|31->8|33->10|33->10|35->12|35->12|40->17|40->17|45->22|45->22|45->22|47->24|48->25|48->25|48->25|49->26|49->26|49->26|58->35|58->35|58->35|59->36|59->36|59->36|59->36|59->36|59->36|60->37|60->37|60->37|63->40|63->40|63->40|64->41|64->41|64->41|65->42|65->42|65->42|66->43|66->43|66->43|68->45|68->45|68->45|69->46|69->46|70->47|70->47|71->48|72->49|72->49|73->50|74->51|75->52|78->55|81->58|81->58|81->58|82->59|82->59|82->59|82->59|82->59|85->62|87->64|90->67
                    -- GENERATED --
                */
            