
package views.html.roles

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object editRole extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Form[controllers.roleFormData],Map[String, Boolean],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(newRoleForm: Form[controllers.roleFormData], permissionsMap: Map[String, Boolean])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import views.html.bootstrap3._

implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ FieldConstructor(twitterBootstrapInput.f)}};
Seq[Any](format.raw/*1.121*/("""

"""),format.raw/*5.74*/("""

"""),_display_(Seq[Any](/*7.2*/main("Update Role")/*7.21*/ {_display_(Seq[Any](format.raw/*7.23*/("""
    <div class="page-header">
        <h1>Edit Role</h1>
    </div>
    <div class="row">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*13.14*/if(newRoleForm.hasGlobalErrors)/*13.45*/ {_display_(Seq[Any](format.raw/*13.47*/("""
                <ul>
                """),_display_(Seq[Any](/*15.18*/for(error <- newRoleForm.globalErrors) yield /*15.56*/ {_display_(Seq[Any](format.raw/*15.58*/("""
                    <li>"""),_display_(Seq[Any](/*16.26*/error/*16.31*/.message)),format.raw/*16.39*/("""</li>
                """)))})),format.raw/*17.18*/("""
                </ul>
            """)))})),format.raw/*19.14*/("""

            """),_display_(Seq[Any](/*21.14*/form(action = routes.Admin.updateRole(), 'enctype -> "multipart/form-data", 'class -> "form-horizontal")/*21.118*/ {_display_(Seq[Any](format.raw/*21.120*/("""
                <fieldset id="nameDescrFieldSet">
                    """),_display_(Seq[Any](/*23.22*/inputText(newRoleForm("name"), '_label -> "Name"))),format.raw/*23.71*/("""
                    """),_display_(Seq[Any](/*24.22*/textarea(newRoleForm("description"), '_label -> "Description"))),format.raw/*24.84*/("""
                    """),_display_(Seq[Any](/*25.22*/checkboxes(newRoleForm("permissions"), label = "Permissions", checkboxMap = permissionsMap, help ="Select the permissions you want to add to this role" ))),format.raw/*25.175*/("""
                    """),_display_(Seq[Any](/*26.22*/defining(newRoleForm("id"))/*26.49*/ { id =>_display_(Seq[Any](format.raw/*26.57*/("""
                      <input type="hidden" name="id" id ="id" value =""""),_display_(Seq[Any](/*27.72*/id/*27.74*/.value)),format.raw/*27.80*/("""" >
                    """)))})),format.raw/*28.22*/("""
                </fieldset>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary" name = "submitValue"><span class="glyphicon glyphicon-send"></span> Update</button>
                </div>
            """)))})),format.raw/*33.14*/("""

        </div>
    </div>
""")))})))}
    }
    
    def render(newRoleForm:Form[controllers.roleFormData],permissionsMap:Map[String, Boolean],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(newRoleForm,permissionsMap)(user)
    
    def f:((Form[controllers.roleFormData],Map[String, Boolean]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (newRoleForm,permissionsMap) => (user) => apply(newRoleForm,permissionsMap)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/roles/editRole.scala.html
                    HASH: 7bdcbb59ced273587e4b76f359957ec66b35eece
                    MATRIX: 661->1|914->172|946->196|1025->120|1054->244|1091->247|1118->266|1157->268|1329->404|1369->435|1409->437|1484->476|1538->514|1578->516|1640->542|1654->547|1684->555|1739->578|1807->614|1858->629|1972->733|2013->735|2121->807|2192->856|2250->878|2334->940|2392->962|2568->1115|2626->1137|2662->1164|2708->1172|2816->1244|2827->1246|2855->1252|2912->1277|3202->1535
                    LINES: 20->1|25->5|25->5|26->1|28->5|30->7|30->7|30->7|36->13|36->13|36->13|38->15|38->15|38->15|39->16|39->16|39->16|40->17|42->19|44->21|44->21|44->21|46->23|46->23|47->24|47->24|48->25|48->25|49->26|49->26|49->26|50->27|50->27|50->27|51->28|56->33
                    -- GENERATED --
                */
            