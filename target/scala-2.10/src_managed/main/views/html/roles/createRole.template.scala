
package views.html.roles

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object createRole extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Form[controllers.roleFormData],Map[String, Boolean],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(newRoleForm: Form[controllers.roleFormData], permissionsMap: Map[String, Boolean])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n.Messages

import helper._

import views.html.bootstrap3._

implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ FieldConstructor(twitterBootstrapInput.f)}};
Seq[Any](format.raw/*1.121*/("""
"""),format.raw/*5.74*/("""

"""),_display_(Seq[Any](/*7.2*/main("Create Role")/*7.21*/ {_display_(Seq[Any](format.raw/*7.23*/("""
    <div class="page-header">
        <h1>Create new role</h1>
        <p>Roles are used to moderate access to """),_display_(Seq[Any](/*10.50*/Messages("spaces.title"))),format.raw/*10.74*/(""", the top data Spaces.
            Select the permissions from the list and add it to the new role.</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            """),_display_(Seq[Any](/*15.14*/if(newRoleForm.hasGlobalErrors)/*15.45*/ {_display_(Seq[Any](format.raw/*15.47*/("""
                <ul>
                """),_display_(Seq[Any](/*17.18*/for(error <- newRoleForm.globalErrors) yield /*17.56*/ {_display_(Seq[Any](format.raw/*17.58*/("""
                    <li>"""),_display_(Seq[Any](/*18.26*/error/*18.31*/.message)),format.raw/*18.39*/("""</li>
                """)))})),format.raw/*19.18*/("""
                </ul>
            """)))})),format.raw/*21.14*/("""

            """),_display_(Seq[Any](/*23.14*/form(action = routes.Admin.submitCreateRole(), 'enctype -> "multipart/form-data", 'class -> "form-horizontal")/*23.124*/ {_display_(Seq[Any](format.raw/*23.126*/("""
                <fieldset id="nameDescrFieldSet">
                    """),_display_(Seq[Any](/*25.22*/inputText(newRoleForm("name"), 'class-> "form-control", '_label -> "Name"))),format.raw/*25.96*/("""
                    """),_display_(Seq[Any](/*26.22*/textarea(newRoleForm("description"), 'class-> "form-control", '_label -> "Description"))),format.raw/*26.109*/("""
                    """),_display_(Seq[Any](/*27.22*/checkboxes(newRoleForm("permissions"), label = "Permissions", checkboxMap = permissionsMap, help ="" ))),format.raw/*27.124*/("""
                </fieldset>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary" name = "submitValue"><span class="glyphicon glyphicon-ok"></span> Create</button>
                </div>
            """)))})),format.raw/*32.14*/("""
        </div>
    </div>
""")))})))}
    }
    
    def render(newRoleForm:Form[controllers.roleFormData],permissionsMap:Map[String, Boolean],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(newRoleForm,permissionsMap)(user)
    
    def f:((Form[controllers.roleFormData],Map[String, Boolean]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (newRoleForm,permissionsMap) => (user) => apply(newRoleForm,permissionsMap)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:39 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/roles/createRole.scala.html
                    HASH: 9fccacc56eb25efb9a7f1a943e11c08452e55b0f
                    MATRIX: 663->1|947->202|979->226|1058->120|1086->274|1123->277|1150->296|1189->298|1338->411|1384->435|1602->617|1642->648|1682->650|1757->689|1811->727|1851->729|1913->755|1927->760|1957->768|2012->791|2080->827|2131->842|2251->952|2292->954|2400->1026|2496->1100|2554->1122|2664->1209|2722->1231|2847->1333|3135->1589
                    LINES: 20->1|27->5|27->5|28->1|29->5|31->7|31->7|31->7|34->10|34->10|39->15|39->15|39->15|41->17|41->17|41->17|42->18|42->18|42->18|43->19|45->21|47->23|47->23|47->23|49->25|49->25|50->26|50->26|51->27|51->27|56->32
                    -- GENERATED --
                */
            