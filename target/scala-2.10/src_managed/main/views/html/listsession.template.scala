
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listsession extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[models.AnalysisSession],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(sessionList: List[models.AnalysisSession])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.81*/("""

"""),_display_(Seq[Any](/*3.2*/main("Sessions")/*3.18*/ {_display_(Seq[Any](format.raw/*3.20*/("""
<div class="page-header">
    <h1>List of Active Jupyter Sessions</h1>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Url</th>
                <th>StartDate</th>
                <th>Reserved Hours</th>
                <th>Reserved Minutes</th>
                <th>Actions</th>

                <th>
            </tr>
            </thead>
            <tbody>
            """),_display_(Seq[Any](/*23.14*/sessionList/*23.25*/.map/*23.29*/ { session =>_display_(Seq[Any](format.raw/*23.42*/("""
              """),_display_(Seq[Any](/*24.16*/if(session.status == "Running")/*24.47*/ {_display_(Seq[Any](format.raw/*24.49*/("""
            <tr>
                <td>"""),_display_(Seq[Any](/*26.22*/session/*26.29*/.sessionName)),format.raw/*26.41*/("""</td>
                <td><a href="""),_display_(Seq[Any](/*27.30*/session/*27.37*/.url)),format.raw/*27.41*/(""" target="_blank">"""),_display_(Seq[Any](/*27.59*/session/*27.66*/.url)),format.raw/*27.70*/("""</a></td>
                <td>"""),_display_(Seq[Any](/*28.22*/session/*28.29*/.startDate)),format.raw/*28.39*/("""</td>
                <td>"""),_display_(Seq[Any](/*29.22*/session/*29.29*/.reservedHours)),format.raw/*29.43*/("""</td>
                <td>"""),_display_(Seq[Any](/*30.22*/session/*30.29*/.reservedMinutes)),format.raw/*30.45*/("""</td>
                <td><a href=""""),_display_(Seq[Any](/*31.31*/controllers/*31.42*/.routes.DataAnalysis.terminate(session.id))),format.raw/*31.84*/("""">Terminate</a></td>

            </tr>
              """)))})),format.raw/*34.16*/("""
            """)))})),format.raw/*35.14*/("""
            </tbody>
        </table>
    </div>
</div>
""")))})))}
    }
    
    def render(sessionList:List[models.AnalysisSession],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(sessionList)(user)
    
    def f:((List[models.AnalysisSession]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (sessionList) => (user) => apply(sessionList)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/listsession.scala.html
                    HASH: f6784777a7ec9d54f6476a85fa3cfbc6d98fc853
                    MATRIX: 635->1|808->80|845->83|869->99|908->101|1466->623|1486->634|1499->638|1550->651|1602->667|1642->698|1682->700|1757->739|1773->746|1807->758|1878->793|1894->800|1920->804|1974->822|1990->829|2016->833|2083->864|2099->871|2131->881|2194->908|2210->915|2246->929|2309->956|2325->963|2363->979|2435->1015|2455->1026|2519->1068|2606->1123|2652->1137
                    LINES: 20->1|23->1|25->3|25->3|25->3|45->23|45->23|45->23|45->23|46->24|46->24|46->24|48->26|48->26|48->26|49->27|49->27|49->27|49->27|49->27|49->27|50->28|50->28|50->28|51->29|51->29|51->29|52->30|52->30|52->30|53->31|53->31|53->31|56->34|57->35
                    -- GENERATED --
                */
            