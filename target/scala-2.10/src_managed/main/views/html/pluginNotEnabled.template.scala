
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object pluginNotEnabled extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(plugin: String):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.18*/("""

"""),_display_(Seq[Any](/*3.2*/main("Plugin Not Enabled")/*3.28*/ {_display_(Seq[Any](format.raw/*3.30*/("""
	<div class="page-header">
		<h1>"""),_display_(Seq[Any](/*5.8*/plugin)),format.raw/*5.14*/(""" plugin not enabled</h1>
	</div>
	<div class="row">
		<div class="col-md-12">
			The required plugin <strong>"""),_display_(Seq[Any](/*9.33*/plugin)),format.raw/*9.39*/("""</strong> is not enabled. Please contact an administrator for further questions.
		</div>
	</div>
""")))})))}
    }
    
    def render(plugin:String): play.api.templates.HtmlFormat.Appendable = apply(plugin)
    
    def f:((String) => play.api.templates.HtmlFormat.Appendable) = (plugin) => apply(plugin)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/pluginNotEnabled.scala.html
                    HASH: 395501f8efb4980a5d89af4bba37b03cc26bbe21
                    MATRIX: 598->1|708->17|745->20|779->46|818->48|887->83|914->89|1059->199|1086->205
                    LINES: 20->1|23->1|25->3|25->3|25->3|27->5|27->5|31->9|31->9
                    -- GENERATED --
                */
            