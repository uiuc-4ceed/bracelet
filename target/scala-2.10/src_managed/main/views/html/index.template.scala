
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object index extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template9[Long,Long,Long,Long,Long,Long,String,String,Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(datasetsCount: Long, filesCount: Long, filesBytes: Long,
        collectionsCount: Long, spacesCount: Long, usersCount: Long,
        displayedName: String, welcomeMessage: String)(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters._


Seq[Any](format.raw/*3.91*/("""

"""),format.raw/*6.1*/("""
"""),_display_(Seq[Any](/*7.2*/main(displayedName)/*7.21*/ {_display_(Seq[Any](format.raw/*7.23*/("""
    <div class="row featurette">
        <div style="text-align: center;">
            <h2 class="featurette-heading">Welcome to """),_display_(Seq[Any](/*10.56*/displayedName)),format.raw/*10.69*/("""</h2>
            <p class="lead">"""),_display_(Seq[Any](/*11.30*/welcomeMessage)),format.raw/*11.44*/("""</p>
            <hr />
           <!--  <p class="lead">If you are in a lab, please take advantage of the streamlined <a href="""),_display_(Seq[Any](/*13.105*/play/*13.109*/.Play.application().configuration().getString("uploaderURL"))),format.raw/*13.169*/(""">uploader.</a>

              <span style="position: relative;top: 2px;display: inline-block;font-family: 'Glyphicons Halflings'; -webkit-font-smoothing: antialiased; font-style: normal; font-weight: normal; line-height: 1;"
              class="glyphicon glyphicon-info-sign" aria-hidden="true" data-toggle="popover" title="4CeeD Uploader" data-trigger="hover" data-content="The uploader is a simple 3 step process for uploading files and using templates to describe metadata.">
              </span>
            </p> -->
        </div>
        <div class="row icons_pad" style="margin-top:50px;">
            <div class="col-md-3"> 
                <center>
                    <img src=""""),_display_(Seq[Any](/*23.32*/routes/*23.38*/.Assets.at("images/lock.png"))),format.raw/*23.67*/("""" width="85" /><br /><br />  
                    <h4 class="no_italics">Trusted Capture</h4><br />
                </center>
            </div>
            <div class="col-md-3"> 
                <center>
                 <img src=""""),_display_(Seq[Any](/*29.29*/routes/*29.35*/.Assets.at("images/search.png"))),format.raw/*29.66*/("""" width="85" /><br /><br />  
                    <h4 class="no_italics">Metadata Extraction</h4><br />
                </center>
            </div>
            <div class="col-md-3"> 
                <center>
                    <img src=""""),_display_(Seq[Any](/*35.32*/routes/*35.38*/.Assets.at("images/cloud.png"))),format.raw/*35.68*/("""" width="85" /><br /><br />  
                    <h4 class="no_italics">Smart File Management</h4><br />
                </center>
            </div>
            <div class="col-md-3"> 
                <center>
                     <img src=""""),_display_(Seq[Any](/*41.33*/routes/*41.39*/.Assets.at("images/check.png"))),format.raw/*41.69*/("""" width="85" /><br /><br />  
                    <h4 class="no_italics">Share and Discover</h4><br />
                </center>
            </div>
        </div>
    <!-- <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Welcome to """),_display_(Seq[Any](/*48.56*/displayedName)),format.raw/*48.69*/("""</h2>
            <p class="lead">"""),_display_(Seq[Any](/*49.30*/welcomeMessage)),format.raw/*49.44*/("""</p>
        </div>
        <div class="col-md-5" id="resources-panel-container">
            <div class="panel panel-default" id="resources-panel" data-clampedwidth=".col-md-5">
              <div class="panel-heading">
                <h3 class="panel-title">Resources</h3>
              </div>
              <div class="panel-body">
                  <ul class="list-group">

                        <li class="list-group-item" title=""""),_display_(Seq[Any](/*59.61*/humanReadableNumber(spacesCount))),format.raw/*59.93*/(""" """),_display_(Seq[Any](/*59.95*/Messages("spaces.title"))),format.raw/*59.119*/("""">
                            <a href=""""),_display_(Seq[Any](/*60.39*/routes/*60.45*/.Spaces.list())),format.raw/*60.59*/("""">"""),_display_(Seq[Any](/*60.62*/Messages("spaces.title"))),format.raw/*60.86*/("""<span class="badge pull-right">"""),_display_(Seq[Any](/*60.118*/humanReadableNumber(spacesCount))),format.raw/*60.150*/("""</span></a>
                        </li>

                      <li class="list-group-item" title=""""),_display_(Seq[Any](/*63.59*/humanReadableNumber(collectionsCount))),format.raw/*63.96*/(""" """),_display_(Seq[Any](/*63.98*/Messages("collections.title"))),format.raw/*63.127*/("""">
                          <a href=""""),_display_(Seq[Any](/*64.37*/routes/*64.43*/.Collections.list(""))),format.raw/*64.64*/("""">"""),_display_(Seq[Any](/*64.67*/Messages("collections.title"))),format.raw/*64.96*/(""" <span class="badge pull-right">"""),_display_(Seq[Any](/*64.129*/humanReadableNumber(collectionsCount))),format.raw/*64.166*/("""</span></a>
                      </li>
                      <li class="list-group-item" title=""""),_display_(Seq[Any](/*66.59*/humanReadableNumber(datasetsCount))),format.raw/*66.93*/(""" """),_display_(Seq[Any](/*66.95*/Messages("datasets.title"))),format.raw/*66.121*/("""">
                          <a href=""""),_display_(Seq[Any](/*67.37*/routes/*67.43*/.Datasets.list(""))),format.raw/*67.61*/("""">"""),_display_(Seq[Any](/*67.64*/Messages("datasets.title"))),format.raw/*67.90*/(""" <span class="badge pull-right">"""),_display_(Seq[Any](/*67.123*/humanReadableNumber(datasetsCount))),format.raw/*67.157*/("""</span></a>
                      </li>
                      <li class="list-group-item">
                          Files <span class="badge pull-right">"""),_display_(Seq[Any](/*70.65*/humanReadableNumber(filesCount))),format.raw/*70.96*/("""</span>
                      </li>
                      <li class="list-group-item" title="Total number of raw bytes stored.">
                          Bytes <span class="badge pull-right">"""),_display_(Seq[Any](/*73.65*/humanReadableByteCount(filesBytes))),format.raw/*73.99*/("""</span>
                      </li>
                      <li class="list-group-item" title="Total number users signed up for this server.">
                          """),_display_(Seq[Any](/*76.28*/if(user.isDefined)/*76.46*/ {_display_(Seq[Any](format.raw/*76.48*/("""
                              <a href=""""),_display_(Seq[Any](/*77.41*/routes/*77.47*/.Users.getUsers())),format.raw/*77.64*/(""""> Users <span class="badge pull-right">"""),_display_(Seq[Any](/*77.105*/humanReadableNumber(usersCount))),format.raw/*77.136*/("""</span></a>
                          """)))}/*78.29*/else/*78.34*/{_display_(Seq[Any](format.raw/*78.35*/("""
                             Users <span class="badge pull-right">"""),_display_(Seq[Any](/*79.68*/humanReadableNumber(usersCount))),format.raw/*79.99*/("""</span>
                          """)))})),format.raw/*80.28*/("""
                      </li>
                  </ul>
              </div>
            </div>
        </div>
    </div> -->
""")))})),format.raw/*87.2*/("""
"""))}
    }
    
    def render(datasetsCount:Long,filesCount:Long,filesBytes:Long,collectionsCount:Long,spacesCount:Long,usersCount:Long,displayedName:String,welcomeMessage:String,user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(datasetsCount,filesCount,filesBytes,collectionsCount,spacesCount,usersCount,displayedName,welcomeMessage)(user)
    
    def f:((Long,Long,Long,Long,Long,Long,String,String) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (datasetsCount,filesCount,filesBytes,collectionsCount,spacesCount,usersCount,displayedName,welcomeMessage) => (user) => apply(datasetsCount,filesCount,filesBytes,collectionsCount,spacesCount,usersCount,displayedName,welcomeMessage)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:36 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/index.scala.html
                    HASH: ff5c0cfd9e1b5e738e7cfc3aa85c0259ee1b79f0
                    MATRIX: 644->1|987->218|1015->253|1051->255|1078->274|1117->276|1284->407|1319->420|1390->455|1426->469|1591->597|1605->601|1688->661|2415->1352|2430->1358|2481->1387|2751->1621|2766->1627|2819->1658|3096->1899|3111->1905|3163->1935|3443->2179|3458->2185|3510->2215|3833->2502|3868->2515|3939->2550|3975->2564|4450->3003|4504->3035|4542->3037|4589->3061|4666->3102|4681->3108|4717->3122|4756->3125|4802->3149|4871->3181|4926->3213|5063->3314|5122->3351|5160->3353|5212->3382|5287->3421|5302->3427|5345->3448|5384->3451|5435->3480|5505->3513|5565->3550|5699->3648|5755->3682|5793->3684|5842->3710|5917->3749|5932->3755|5972->3773|6011->3776|6059->3802|6129->3835|6186->3869|6377->4024|6430->4055|6659->4248|6715->4282|6919->4450|6946->4468|6986->4470|7063->4511|7078->4517|7117->4534|7195->4575|7249->4606|7307->4646|7320->4651|7359->4652|7463->4720|7516->4751|7583->4786|7738->4910
                    LINES: 20->1|26->3|28->6|29->7|29->7|29->7|32->10|32->10|33->11|33->11|35->13|35->13|35->13|45->23|45->23|45->23|51->29|51->29|51->29|57->35|57->35|57->35|63->41|63->41|63->41|70->48|70->48|71->49|71->49|81->59|81->59|81->59|81->59|82->60|82->60|82->60|82->60|82->60|82->60|82->60|85->63|85->63|85->63|85->63|86->64|86->64|86->64|86->64|86->64|86->64|86->64|88->66|88->66|88->66|88->66|89->67|89->67|89->67|89->67|89->67|89->67|89->67|92->70|92->70|95->73|95->73|98->76|98->76|98->76|99->77|99->77|99->77|99->77|99->77|100->78|100->78|100->78|101->79|101->79|102->80|109->87
                    -- GENERATED --
                */
            