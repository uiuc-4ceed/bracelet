
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object selectDataset extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[String,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: String):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.19*/("""

	<button class="btn" id="selectButton">Select</button>

<script language="javascript">
$(function() """),format.raw/*6.14*/("""{"""),format.raw/*6.15*/("""

   console.log("*** registering selection ***");

   $('#selectButton').click(function() """),format.raw/*10.40*/("""{"""),format.raw/*10.41*/("""

     console.log("selecting dataset ");

     var request = jsRoutes.api.Selected.add().ajax("""),format.raw/*14.53*/("""{"""),format.raw/*14.54*/("""
       data: JSON.stringify("""),format.raw/*15.29*/("""{"""),format.raw/*15.30*/(""""dataset":""""),_display_(Seq[Any](/*15.42*/dataset)),format.raw/*15.49*/("""""""),format.raw/*15.50*/("""}"""),format.raw/*15.51*/("""),
       type: 'POST',
       contentType: "application/json"
     """),format.raw/*18.6*/("""}"""),format.raw/*18.7*/(""");

     request.done(function (response, textStatus, jqXHR)"""),format.raw/*20.57*/("""{"""),format.raw/*20.58*/("""
        console.log("Response " + response);
     """),format.raw/*22.6*/("""}"""),format.raw/*22.7*/(""");

     request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*24.60*/("""{"""),format.raw/*24.61*/("""
        console.error("The following error occured: "+ textStatus, errorThrown);
        window.location = "../login"; // FIXME hardcoded
     """),format.raw/*27.6*/("""}"""),format.raw/*27.7*/(""");
  """),format.raw/*28.3*/("""}"""),format.raw/*28.4*/(""");
 """),format.raw/*29.2*/("""}"""),format.raw/*29.3*/(""");
</script>"""))}
    }
    
    def render(dataset:String): play.api.templates.HtmlFormat.Appendable = apply(dataset)
    
    def f:((String) => play.api.templates.HtmlFormat.Appendable) = (dataset) => apply(dataset)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:27 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/selectDataset.scala.html
                    HASH: 4be076c2b6c8fbfd43d2ae1da1d2fa69ca2ff85d
                    MATRIX: 595->1|706->18|835->120|863->121|982->212|1011->213|1134->308|1163->309|1220->338|1249->339|1297->351|1326->358|1355->359|1384->360|1479->428|1507->429|1595->489|1624->490|1702->541|1730->542|1821->605|1850->606|2021->750|2049->751|2081->756|2109->757|2140->761|2168->762
                    LINES: 20->1|23->1|28->6|28->6|32->10|32->10|36->14|36->14|37->15|37->15|37->15|37->15|37->15|37->15|40->18|40->18|42->20|42->20|44->22|44->22|46->24|46->24|49->27|49->27|50->28|50->28|51->29|51->29
                    -- GENERATED --
                */
            