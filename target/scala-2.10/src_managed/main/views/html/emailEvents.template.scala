
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object emailEvents extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[models.Event],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(events: List[models.Event])(implicit user: Option[models.User] = None):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import _root_.util.Formatters


Seq[Any](format.raw/*1.73*/("""
"""),format.raw/*3.1*/("""<h1> Clowder Email Digest </h1>

Following is a list of events at """),_display_(Seq[Any](/*5.35*/services/*5.43*/.AppConfiguration.getDisplayName)),format.raw/*5.75*/("""
      """),_display_(Seq[Any](/*6.8*/for(event <- events) yield /*6.28*/ {_display_(Seq[Any](format.raw/*6.30*/("""
          <div class="panel panel-default">
              <div class="panel-body">
                  <div class="media">
                      <div class="media-left">

                      </div>
                      <p>
                      <div class="media-body">
                          """),_display_(Seq[Any](/*15.28*/event/*15.33*/.user.fullName)),format.raw/*15.47*/("""
                          """),_display_(Seq[Any](/*16.28*/newsFeedCardEmail(event))),format.raw/*16.52*/("""
                      </div>
                      <div class="media-right">
                          """),_display_(Seq[Any](/*19.28*/Formatters/*19.38*/.humanReadableTimeSince(event.created))),format.raw/*19.76*/("""
                      </div>
                      </p>
                  </div>
              </div>
          </div>
      """)))})),format.raw/*25.8*/("""
"""))}
    }
    
    def render(events:List[models.Event],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(events)(user)
    
    def f:((List[models.Event]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (events) => (user) => apply(events)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/emailEvents.scala.html
                    HASH: d3fc8c3b0c5b0dcf49444f4772420e113b7b132f
                    MATRIX: 625->1|820->72|847->104|949->171|965->179|1018->211|1060->219|1095->239|1134->241|1469->540|1483->545|1519->559|1583->587|1629->611|1770->716|1789->726|1849->764|2007->891
                    LINES: 20->1|24->1|25->3|27->5|27->5|27->5|28->6|28->6|28->6|37->15|37->15|37->15|38->16|38->16|41->19|41->19|41->19|47->25
                    -- GENERATED --
                */
            