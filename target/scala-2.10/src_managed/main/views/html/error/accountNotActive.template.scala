
package views.html.error

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object accountNotActive extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/()(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.40*/("""
"""),_display_(Seq[Any](/*2.2*/main("Account not active")/*2.28*/ {_display_(Seq[Any](format.raw/*2.30*/("""
    <div class="row">
        <div class="col-md-12">
            <h1>Account not active</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            Your account has not been enabled. If you feel this in error please use the
            <a href=""""),_display_(Seq[Any](/*11.23*/routes/*11.29*/.Application.email(s"Account for ${user.fold("??")(_.fullName)}"))),format.raw/*11.94*/("""">contact admin</a>
            form to ask the server admins about your account.
        </div>
    </div>
""")))})),format.raw/*15.2*/("""
"""))}
    }
    
    def render(user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply()(user)
    
    def f:(() => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = () => (user) => apply()(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:41 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/error/accountNotActive.scala.html
                    HASH: 873f5fe0878485128dc63566fd35681195579ae7
                    MATRIX: 617->1|749->39|785->41|819->67|858->69|1199->374|1214->380|1301->445|1441->554
                    LINES: 20->1|23->1|24->2|24->2|24->2|33->11|33->11|33->11|37->15
                    -- GENERATED --
                */
            