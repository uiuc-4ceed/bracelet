
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object listAllExtractions extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[models.Extraction],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(extractions: List[models.Extraction])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.76*/("""

"""),_display_(Seq[Any](/*3.2*/main("Extractions")/*3.21*/ {_display_(Seq[Any](format.raw/*3.23*/("""
	<div class="page-header">
		<h1>Extractions</h1>
	</div>
	
	"""),_display_(Seq[Any](/*8.3*/if(extractions.size == 0)/*8.28*/ {_display_(Seq[Any](format.raw/*8.30*/("""
	<div class="row">
		<div class="col-md-12">
			No extractions present.
		</div>
	</div>
	""")))})),format.raw/*14.3*/("""
	"""),_display_(Seq[Any](/*15.3*/if(extractions.size > 0)/*15.27*/ {_display_(Seq[Any](format.raw/*15.29*/("""
	<table class="table">
		<thead>
	       <tr>
	         <th>Extractor</th>
	         <th>File</th>
	         <th>Status</th>
	         <th>Started</th>
	         <th>Finished</th>
	       </tr>
	     </thead>
	     <tbody>
	    """),_display_(Seq[Any](/*27.7*/for(e <- extractions) yield /*27.28*/ {_display_(Seq[Any](format.raw/*27.30*/("""
	      <tr>
			<td>
				"""),_display_(Seq[Any](/*30.6*/e/*30.7*/.extractor_id)),format.raw/*30.20*/("""
			</td>
			<td>
				"""),_display_(Seq[Any](/*33.6*/e/*33.7*/.file_id)),format.raw/*33.15*/("""
			</td>
			<td>
				<span class="label label-success">"""),_display_(Seq[Any](/*36.40*/e/*36.41*/.status)),format.raw/*36.48*/("""</span>
			</td>
			<td>
				"""),_display_(Seq[Any](/*39.6*/e/*39.7*/.start)),format.raw/*39.13*/("""
			</td>
			<td>
				-
			</td>
		  </tr>
		""")))})),format.raw/*45.4*/("""
		</tbody>
		</table>
	""")))})),format.raw/*48.3*/("""
""")))})),format.raw/*49.2*/("""
"""))}
    }
    
    def render(extractions:List[models.Extraction],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(extractions)(user)
    
    def f:((List[models.Extraction]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (extractions) => (user) => apply(extractions)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:31 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/listAllExtractions.scala.html
                    HASH: d8c4ba414671ee3c34dfbbeec8dde7e0a4c6308b
                    MATRIX: 637->1|805->75|842->78|869->97|908->99|1005->162|1038->187|1077->189|1200->281|1238->284|1271->308|1311->310|1576->540|1613->561|1653->563|1714->589|1723->590|1758->603|1816->626|1825->627|1855->635|1948->692|1958->693|1987->700|2052->730|2061->731|2089->737|2166->783|2222->808|2255->810
                    LINES: 20->1|23->1|25->3|25->3|25->3|30->8|30->8|30->8|36->14|37->15|37->15|37->15|49->27|49->27|49->27|52->30|52->30|52->30|55->33|55->33|55->33|58->36|58->36|58->36|61->39|61->39|61->39|67->45|70->48|71->49
                    -- GENERATED --
                */
            