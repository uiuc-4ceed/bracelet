
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object searchTextResults extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[String,Integer,Array[scala.Tuple4[String, String, Double, String]],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(query: String,size: Integer,results: Array[(String,String,Double,String)]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.77*/("""
"""),_display_(Seq[Any](/*2.2*/main("Search Results")/*2.24*/ {_display_(Seq[Any](format.raw/*2.26*/("""
	<div class="page-header">
		<h1>Search Results For <small>"""),_display_(Seq[Any](/*4.34*/query)),format.raw/*4.39*/("""</small></h1>
	</div>
	"""),_display_(Seq[Any](/*6.3*/if(size == 0)/*6.16*/ {_display_(Seq[Any](format.raw/*6.18*/("""
	<div class="row">
		<div class="col-md-12">
			No results found. Sorry!
		</div>
	</div>
	""")))})),format.raw/*12.3*/("""
	<div class="row">
		<div class="col-md-12">
		
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Proximity</th>
					
				</tr>
			</thead>
			<tbody>
			   """),_display_(Seq[Any](/*25.8*/results/*25.15*/.map/*25.19*/{   re=>_display_(Seq[Any](format.raw/*25.27*/("""
	                  <tr>
						<td><a href="""),_display_(Seq[Any](/*27.20*/routes/*27.26*/.Files.file(UUID(re._1)))),format.raw/*27.50*/("""> """),_display_(Seq[Any](/*27.53*/re/*27.55*/._4)),format.raw/*27.58*/("""</a></td>
						<td>"""),_display_(Seq[Any](/*28.12*/re/*28.14*/._3)),format.raw/*28.17*/("""</td>
					
				""")))})),format.raw/*30.6*/("""
			</tbody>
		</table>
		
		 
""")))})))}
    }
    
    def render(query:String,size:Integer,results:Array[scala.Tuple4[String, String, Double, String]]): play.api.templates.HtmlFormat.Appendable = apply(query,size,results)
    
    def f:((String,Integer,Array[scala.Tuple4[String, String, Double, String]]) => play.api.templates.HtmlFormat.Appendable) = (query,size,results) => apply(query,size,results)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:35 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/searchTextResults.scala.html
                    HASH: 770639fe78b255ae024ac7c490bfd88acbc95630
                    MATRIX: 659->1|828->76|864->78|894->100|933->102|1029->163|1055->168|1113->192|1134->205|1173->207|1297->300|1539->507|1555->514|1568->518|1614->526|1694->570|1709->576|1755->600|1794->603|1805->605|1830->608|1887->629|1898->631|1923->634|1971->651
                    LINES: 20->1|23->1|24->2|24->2|24->2|26->4|26->4|28->6|28->6|28->6|34->12|47->25|47->25|47->25|47->25|49->27|49->27|49->27|49->27|49->27|49->27|50->28|50->28|50->28|52->30
                    -- GENERATED --
                */
            