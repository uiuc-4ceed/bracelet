
package views.html.ss.mails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object welcomeEmailPendingVerification extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[securesocial.core.Identity,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(user: securesocial.core.Identity)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.69*/("""

<html>
    <body>

        <p>Welcome """),_display_(Seq[Any](/*6.21*/user/*6.25*/.firstName)),format.raw/*6.35*/(""",</p>

        <p>
            Thank you for providing your information.
            You will receive an email to log in once your account is enabled.
        </p>

        """),_display_(Seq[Any](/*13.10*/emails/*13.16*/.footer())),format.raw/*13.25*/("""

    </body>
</html>"""))}
    }
    
    def render(user:securesocial.core.Identity,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(user)(request)
    
    def f:((securesocial.core.Identity) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (user) => (request) => apply(user)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/mails/welcomeEmailPendingVerification.scala.html
                    HASH: 0f3a13af0841b1192703b53a78a9a5cba1a0d3b9
                    MATRIX: 656->1|817->68|893->109|905->113|936->123|1146->297|1161->303|1192->312
                    LINES: 20->1|23->1|28->6|28->6|28->6|35->13|35->13|35->13
                    -- GENERATED --
                */
            