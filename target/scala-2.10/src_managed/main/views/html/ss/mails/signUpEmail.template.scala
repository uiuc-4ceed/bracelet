
package views.html.ss.mails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object signUpEmail extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(token: String)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider


Seq[Any](format.raw/*1.50*/("""
"""),format.raw/*3.1*/("""<html>
<body>
<p>Hello,</p>

<p>Please follow this
    <a href=""""),_display_(Seq[Any](/*8.15*/securesocial/*8.27*/.core.providers.utils.RoutesHelper.signUp(token).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*8.116*/("""">link</a> to complete your registration
    at <a href=""""),_display_(Seq[Any](/*9.18*/routes/*9.24*/.Application.index().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.85*/("""">"""),_display_(Seq[Any](/*9.88*/services/*9.96*/.AppConfiguration.getDisplayName)),format.raw/*9.128*/("""</a>.
</p>
"""),_display_(Seq[Any](/*11.2*/emails/*11.8*/.footer())),format.raw/*11.17*/("""
</body>
</html>"""))}
    }
    
    def render(token:String,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(token)(request)
    
    def f:((String) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (token) => (request) => apply(token)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/mails/signUpEmail.scala.html
                    HASH: 609b4055098bb0b9f230bccd7e358af5682575f8
                    MATRIX: 616->1|800->49|827->93|927->158|947->170|1058->259|1151->317|1165->323|1247->384|1285->387|1301->395|1355->427|1402->439|1416->445|1447->454
                    LINES: 20->1|24->1|25->3|30->8|30->8|30->8|31->9|31->9|31->9|31->9|31->9|31->9|33->11|33->11|33->11
                    -- GENERATED --
                */
            