
package views.html.ss.mails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object passwordResetEmail extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[securesocial.core.Identity,String,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(user: securesocial.core.Identity, token: String)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider


Seq[Any](format.raw/*1.84*/("""
"""),format.raw/*3.1*/("""<html>
<body>
<p>Hello """),_display_(Seq[Any](/*5.11*/user/*5.15*/.firstName)),format.raw/*5.25*/(""",</p>

<p>Please follow this
    <a href=""""),_display_(Seq[Any](/*8.15*/securesocial/*8.27*/.core.providers.utils.RoutesHelper.resetPassword(token).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*8.123*/("""">
    link</a> to reset your password on <a href=""""),_display_(Seq[Any](/*9.50*/routes/*9.56*/.Application.index().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.117*/("""">"""),_display_(Seq[Any](/*9.120*/services/*9.128*/.AppConfiguration.getDisplayName)),format.raw/*9.160*/("""</a>.
</p>
"""),_display_(Seq[Any](/*11.2*/emails/*11.8*/.footer())),format.raw/*11.17*/("""
</body>
</html>"""))}
    }
    
    def render(user:securesocial.core.Identity,token:String,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(user,token)(request)
    
    def f:((securesocial.core.Identity,String) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (user,token) => (request) => apply(user,token)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/mails/passwordResetEmail.scala.html
                    HASH: 8811f55906272668d746041e00e8ada862308ad2
                    MATRIX: 650->1|868->83|895->127|954->151|966->155|997->165|1075->208|1095->220|1213->316|1300->368|1314->374|1397->435|1436->438|1453->446|1507->478|1554->490|1568->496|1599->505
                    LINES: 20->1|24->1|25->3|27->5|27->5|27->5|30->8|30->8|30->8|31->9|31->9|31->9|31->9|31->9|31->9|33->11|33->11|33->11
                    -- GENERATED --
                */
            