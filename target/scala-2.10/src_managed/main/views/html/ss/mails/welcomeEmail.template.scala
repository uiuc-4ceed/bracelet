
package views.html.ss.mails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object welcomeEmail extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[securesocial.core.Identity,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(user: securesocial.core.Identity)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider


Seq[Any](format.raw/*1.69*/("""
"""),format.raw/*3.1*/("""
<html>
    <body>

        <p>Welcome """),_display_(Seq[Any](/*7.21*/user/*7.25*/.firstName)),format.raw/*7.35*/(""",</p>

        <p>
            Your new account is ready at
            <a href=""""),_display_(Seq[Any](/*11.23*/routes/*11.29*/.Application.index().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*11.90*/(""""> """),_display_(Seq[Any](/*11.94*/services/*11.102*/.AppConfiguration.getDisplayName)),format.raw/*11.134*/("""</a>.
            Click <a href=""""),_display_(Seq[Any](/*12.29*/securesocial/*12.41*/.core.providers.utils.RoutesHelper.login.absoluteURL(IdentityProvider.sslEnabled))),format.raw/*12.122*/("""">here</a>
            to log in
        </p>

        """),_display_(Seq[Any](/*16.10*/emails/*16.16*/.footer())),format.raw/*16.25*/("""

    </body>
</html>
"""))}
    }
    
    def render(user:securesocial.core.Identity,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(user)(request)
    
    def f:((securesocial.core.Identity) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (user) => (request) => apply(user)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/mails/welcomeEmail.scala.html
                    HASH: 8bc3586b6c7e036c2879b61934afee1c2172eb1d
                    MATRIX: 637->1|840->68|867->112|942->152|954->156|985->166|1103->248|1118->254|1201->315|1241->319|1259->327|1314->359|1384->393|1405->405|1509->486|1601->542|1616->548|1647->557
                    LINES: 20->1|24->1|25->3|29->7|29->7|29->7|33->11|33->11|33->11|33->11|33->11|33->11|34->12|34->12|34->12|38->16|38->16|38->16
                    -- GENERATED --
                */
            