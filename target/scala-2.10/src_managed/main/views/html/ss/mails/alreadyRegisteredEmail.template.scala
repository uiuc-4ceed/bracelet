
package views.html.ss.mails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object alreadyRegisteredEmail extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[securesocial.core.Identity,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(user: securesocial.core.Identity)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider


Seq[Any](format.raw/*1.69*/("""
"""),format.raw/*3.1*/("""
<html>
    <body>
        <p>Hello """),_display_(Seq[Any](/*6.19*/user/*6.23*/.firstName)),format.raw/*6.33*/(""",</p>

        <p>You tried to sign up but you already have an account with us at <a href=""""),_display_(Seq[Any](/*8.86*/routes/*8.92*/.Application.index().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*8.153*/("""">"""),_display_(Seq[Any](/*8.156*/services/*8.164*/.AppConfiguration.getDisplayName)),format.raw/*8.196*/("""</a>.
            If you don't remember your password please go <a href=""""),_display_(Seq[Any](/*9.69*/securesocial/*9.81*/.core.providers.utils.RoutesHelper.startResetPassword().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.177*/("""">here</a> to reset it.</p>
        """),_display_(Seq[Any](/*10.10*/emails/*10.16*/.footer())),format.raw/*10.25*/("""
    </body>
</html>"""))}
    }
    
    def render(user:securesocial.core.Identity,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(user)(request)
    
    def f:((securesocial.core.Identity) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (user) => (request) => apply(user)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/mails/alreadyRegisteredEmail.scala.html
                    HASH: b2d2768226cdc5396c5644d10b4196cbcab88d74
                    MATRIX: 647->1|850->68|877->112|949->149|961->153|992->163|1119->255|1133->261|1216->322|1255->325|1272->333|1326->365|1435->439|1455->451|1573->547|1646->584|1661->590|1692->599
                    LINES: 20->1|24->1|25->3|28->6|28->6|28->6|30->8|30->8|30->8|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10
                    -- GENERATED --
                */
            