
package views.html.ss.mails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object unknownEmailNotice extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider


Seq[Any](format.raw/*1.35*/("""
"""),format.raw/*3.1*/("""<html>
<body>
<p>Hello,</p>

<p>We received a request to reset a password in our system at <a href=""""),_display_(Seq[Any](/*7.73*/routes/*7.79*/.Application.index().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*7.140*/("""">"""),_display_(Seq[Any](/*7.143*/services/*7.151*/.AppConfiguration.getDisplayName)),format.raw/*7.183*/("""</a>.  The attempt has failed because we do not have
    a registered account with this email address. It could be that you logged in using an external account such as Twitter
    or Facebook.</p>

<p>
    If you never created an account with us ignore this email, otherwise if you think you have an account with us contact
    tech support for further assistance.
</p>
"""),_display_(Seq[Any](/*15.2*/emails/*15.8*/.footer())),format.raw/*15.17*/("""
</body>
</html>"""))}
    }
    
    def render(request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(request)
    
    def f:((RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (request) => apply(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/mails/unknownEmailNotice.scala.html
                    HASH: 27a7642d37791e228784af4566a512f02837b5b8
                    MATRIX: 616->1|785->34|812->78|948->179|962->185|1045->246|1084->249|1101->257|1155->289|1561->660|1575->666|1606->675
                    LINES: 20->1|24->1|25->3|29->7|29->7|29->7|29->7|29->7|29->7|37->15|37->15|37->15
                    -- GENERATED --
                */
            