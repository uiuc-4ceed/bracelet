
package views.html.ss.mails

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object passwordChangedNotice extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[securesocial.core.Identity,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(user: securesocial.core.Identity)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.IdentityProvider


Seq[Any](format.raw/*1.69*/("""
"""),format.raw/*3.1*/("""
<html>
<body>
<p>Hello """),_display_(Seq[Any](/*6.11*/user/*6.15*/.firstName)),format.raw/*6.25*/(""",</p>

<p>Your password was updated at <a href=""""),_display_(Seq[Any](/*8.43*/routes/*8.49*/.Application.index().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*8.110*/("""">"""),_display_(Seq[Any](/*8.113*/services/*8.121*/.AppConfiguration.getDisplayName)),format.raw/*8.153*/("""</a>.
    Please log in using your new password by clicking <a href=""""),_display_(Seq[Any](/*9.65*/securesocial/*9.77*/.core.providers.utils.RoutesHelper.login.absoluteURL(IdentityProvider.sslEnabled))),format.raw/*9.158*/("""">here</a></p>
"""),_display_(Seq[Any](/*10.2*/emails/*10.8*/.footer())),format.raw/*10.17*/("""
</body>
</html>"""))}
    }
    
    def render(user:securesocial.core.Identity,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(user)(request)
    
    def f:((securesocial.core.Identity) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (user) => (request) => apply(user)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/mails/passwordChangedNotice.scala.html
                    HASH: 0d855026b52f50874359fccc3ab5294c443f9a69
                    MATRIX: 646->1|849->68|876->112|936->137|948->141|979->151|1063->200|1077->206|1160->267|1199->270|1216->278|1270->310|1375->380|1395->392|1498->473|1549->489|1563->495|1594->504
                    LINES: 20->1|24->1|25->3|28->6|28->6|28->6|30->8|30->8|30->8|30->8|30->8|30->8|31->9|31->9|31->9|32->10|32->10|32->10
                    -- GENERATED --
                */
            