
package views.html.ss.Registration

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object startResetPassword extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[String],RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(startForm:Form[String])(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import securesocial.core.IdentityProvider

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(securesocial.views.html.inputFieldConstructor.f) }};
Seq[Any](format.raw/*1.59*/("""
"""),format.raw/*4.99*/("""

"""),_display_(Seq[Any](/*6.2*/main(Messages("securesocial.password.reset") )/*6.48*/ {_display_(Seq[Any](format.raw/*6.50*/("""
    <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 panel panel-default">
        <div class="page-header">
            <h1>Reset Password</h1>
        </div>

        """),_display_(Seq[Any](/*12.10*/request/*12.17*/.flash.get("error").map/*12.40*/ { msg =>_display_(Seq[Any](format.raw/*12.49*/("""
        <div class="alert alert-danger">
            """),_display_(Seq[Any](/*14.14*/Messages(msg))),format.raw/*14.27*/("""
        </div>
        """)))})),format.raw/*16.10*/("""

        <div class="clearfix">
            <p>"""),_display_(Seq[Any](/*19.17*/Messages("Please provide your email for further instructions"))),format.raw/*19.79*/("""</p>
        </div>

        <form action=""""),_display_(Seq[Any](/*22.24*/securesocial/*22.36*/.core.providers.utils.RoutesHelper.handleStartResetPassword().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*22.138*/(""""
              class="form-horizontal"
              autocomplete="off"
              method="POST"
        >
            <fieldset>
                """),_display_(Seq[Any](/*28.18*/helper/*28.24*/.inputText(
                    startForm("email"),
                    '_label -> Messages("securesocial.signup.email1"),
                    'class -> "form-control",
                    '_help -> "",
                    '_showErrors -> true
                ))),format.raw/*34.18*/("""

                <div class="form-actions login-nav-box">
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-wrench"></span> """),_display_(Seq[Any](/*37.117*/Messages("securesocial.password.reset"))),format.raw/*37.156*/("""</button>
                    <a class="btn btn-default" href=""""),_display_(Seq[Any](/*38.55*/securesocial/*38.67*/.core.providers.utils.RoutesHelper.login())),format.raw/*38.109*/(""""><span class="glyphicon glyphicon-remove"></span> """),_display_(Seq[Any](/*38.161*/Messages("securesocial.signup.cancel"))),format.raw/*38.199*/("""</a>
                </div>
            </fieldset>
        </form>
    </div>
""")))})))}
    }
    
    def render(startForm:Form[String],request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(startForm)(request)
    
    def f:((Form[String]) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (startForm) => (request) => apply(startForm)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/Registration/startResetPassword.scala.html
                    HASH: 52ac394bffa797dd12a15ae6bbfa1a4ac3c5afb5
                    MATRIX: 636->1|838->120|870->144|973->58|1001->217|1038->220|1092->266|1131->268|1351->452|1367->459|1399->482|1446->491|1537->546|1572->559|1629->584|1714->633|1798->695|1878->739|1899->751|2024->853|2211->1004|2226->1010|2509->1271|2721->1446|2783->1485|2883->1549|2904->1561|2969->1603|3058->1655|3119->1693
                    LINES: 20->1|25->4|25->4|26->1|27->4|29->6|29->6|29->6|35->12|35->12|35->12|35->12|37->14|37->14|39->16|42->19|42->19|45->22|45->22|45->22|51->28|51->28|57->34|60->37|60->37|61->38|61->38|61->38|61->38|61->38
                    -- GENERATED --
                */
            