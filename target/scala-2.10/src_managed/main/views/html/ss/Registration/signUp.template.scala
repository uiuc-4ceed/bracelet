
package views.html.ss.Registration

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object signUp extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Form[securesocial.controllers.Registration.RegistrationInfo],String,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(signUpForm:Form[securesocial.controllers.Registration.RegistrationInfo], token: String)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.providers.UsernamePasswordProvider

import securesocial.core.IdentityProvider

import helper._

import services.AppConfiguration

implicit def /*5.2*/implicitFieldConstructor/*5.26*/ = {{ FieldConstructor(securesocial.views.html.inputFieldConstructor.f) }};
Seq[Any](format.raw/*1.123*/("""
"""),format.raw/*5.99*/("""
"""),format.raw/*7.1*/("""
"""),_display_(Seq[Any](/*8.2*/main(Messages("securesocial.signup.title") )/*8.46*/ {_display_(Seq[Any](format.raw/*8.48*/("""
    <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 panel panel-default">
        <div class="page-header">
            <h1>Sign Up</h1>
        </div>

        """),_display_(Seq[Any](/*14.10*/request/*14.17*/.flash.get("error").map/*14.40*/ { msg =>_display_(Seq[Any](format.raw/*14.49*/("""
            <div class="alert alert-info">
                """),_display_(Seq[Any](/*16.18*/msg)),format.raw/*16.21*/("""
            </div>
        """)))})),format.raw/*18.10*/("""

        <div class="clearfix">
            <p>"""),_display_(Seq[Any](/*21.17*/Messages("Please enter your personal information to finish registration"))),format.raw/*21.90*/("""</p>
        </div>

        """),format.raw/*24.136*/("""
      <form action = """),_display_(Seq[Any](/*25.23*/routes/*25.29*/.Registration.handleSignUp(token))),format.raw/*25.62*/("""
              class="form-horizontal"
              autocomplete= "off"
              method="POST"
        >
            <fieldset>
                """),_display_(Seq[Any](/*31.18*/if( UsernamePasswordProvider.withUserNameSupport )/*31.68*/ {_display_(Seq[Any](format.raw/*31.70*/("""
                    """),_display_(Seq[Any](/*32.22*/helper/*32.28*/.inputText(
                        signUpForm("userName"),
                        '_label -> Messages("securesocial.signup.username"),
                        'class -> "form-control",
                        '_help -> "",
                        '_showErrors -> true
                    ))),format.raw/*38.22*/("""
                """)))})),format.raw/*39.18*/("""

                """),_display_(Seq[Any](/*41.18*/helper/*41.24*/.inputText(
                    signUpForm("firstName"),
                    '_label -> Messages("securesocial.signup.firstName"),
                    'class -> "form-control",
                    '_help -> "",
                    '_showErrors -> true
                ))),format.raw/*47.18*/("""

                """),_display_(Seq[Any](/*49.18*/helper/*49.24*/.inputText(
                    signUpForm("lastName"),
                    '_label -> Messages("securesocial.signup.lastName"),
                    'class -> "form-control",
                    '_help -> "",
                    '_showErrors -> true
                ))),format.raw/*55.18*/("""

                """),_display_(Seq[Any](/*57.18*/helper/*57.24*/.inputPassword(
                    signUpForm("password.password1"),
                    '_label -> Messages("securesocial.signup.password1"),
                    'class -> "form-control",
                    '_help -> "",
                    '_showErrors -> true
                ))),format.raw/*63.18*/("""

                """),_display_(Seq[Any](/*65.18*/helper/*65.24*/.inputPassword(
                    signUpForm("password.password2"),
                    '_label -> Messages("securesocial.signup.password2"),
                    'class -> "form-control",
                    '_help -> "",
                    '_showErrors -> true
                ))),format.raw/*71.18*/("""

                """),_display_(Seq[Any](/*73.18*/helper/*73.24*/.checkbox(
                    signUpForm("agreementAcknowledged"),
                    '_label -> "Terms of Service",
                    '_text -> Html("Check the box to accept the <a target=\"tos\" href=\"../tos\">Terms of Service</a>"),
                    '_help -> "",
                    '_showErrors -> true
                ))),format.raw/*79.18*/("""

                <div class="form-actions btn-margin-bottom">
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*82.113*/Messages("securesocial.signup.createAccount"))),format.raw/*82.158*/("""</button>
                    <a class="btn btn-default" href=""""),_display_(Seq[Any](/*83.55*/securesocial/*83.67*/.core.providers.utils.RoutesHelper.login())),format.raw/*83.109*/(""""><span class="glyphicon glyphicon-remove"></span> """),_display_(Seq[Any](/*83.161*/Messages("securesocial.signup.cancel"))),format.raw/*83.199*/("""</a>
                </div>
            </fieldset>
      </form>

      <script type="text/javascript">
        $(document).ready(function() """),format.raw/*89.38*/("""{"""),format.raw/*89.39*/("""
            var userAgreementElement = $("#agreementAcknowledged");
            if (userAgreementElement && userAgreementElement.length) """),format.raw/*91.70*/("""{"""),format.raw/*91.71*/("""
                // if there is a value in the Admin -> Customize menu for User Agreement,
                // this element will present. In that case, disable the submit button until
                // the user clicks the checkbox to agree to the terms
                $(":submit").prop('disabled', true);

                // if the user submits the form with the checkbox checked, but there are validation errors
                // this will uncheck the form on the validation page
                userAgreementElement.attr('checked', false);

                // toggle whether the button is disabled depending on if the checkbox is checked
                userAgreementElement.on('click', function() """),format.raw/*102.61*/("""{"""),format.raw/*102.62*/("""
                    $(":submit").prop('disabled', function(i, v) """),format.raw/*103.66*/("""{"""),format.raw/*103.67*/(""" return !v; """),format.raw/*103.79*/("""}"""),format.raw/*103.80*/(""");
                """),format.raw/*104.17*/("""}"""),format.raw/*104.18*/(""");
            """),format.raw/*105.13*/("""}"""),format.raw/*105.14*/("""

        """),format.raw/*107.9*/("""}"""),format.raw/*107.10*/(""")
      </script>
    </div>
""")))})),format.raw/*110.2*/("""
"""))}
    }
    
    def render(signUpForm:Form[securesocial.controllers.Registration.RegistrationInfo],token:String,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(signUpForm,token)(request)
    
    def f:((Form[securesocial.controllers.Registration.RegistrationInfo],String) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (signUpForm,token) => (request) => apply(signUpForm,token)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/Registration/signUp.scala.html
                    HASH: 97cf044a71502c8e68e646977346bfe642be45a2
                    MATRIX: 679->1|1040->245|1072->269|1176->122|1204->342|1231->377|1267->379|1319->423|1358->425|1571->602|1587->609|1619->632|1666->641|1763->702|1788->705|1849->734|1934->783|2029->856|2087->1012|2146->1035|2161->1041|2216->1074|2403->1225|2462->1275|2502->1277|2560->1299|2575->1305|2888->1596|2938->1614|2993->1633|3008->1639|3299->1908|3354->1927|3369->1933|3658->2200|3713->2219|3728->2225|4032->2507|4087->2526|4102->2532|4406->2814|4461->2833|4476->2839|4831->3172|5043->3347|5111->3392|5211->3456|5232->3468|5297->3510|5386->3562|5447->3600|5617->3742|5646->3743|5812->3881|5841->3882|6572->4584|6602->4585|6697->4651|6727->4652|6768->4664|6798->4665|6846->4684|6876->4685|6920->4700|6950->4701|6988->4711|7018->4712|7080->4742
                    LINES: 20->1|29->5|29->5|30->1|31->5|32->7|33->8|33->8|33->8|39->14|39->14|39->14|39->14|41->16|41->16|43->18|46->21|46->21|49->24|50->25|50->25|50->25|56->31|56->31|56->31|57->32|57->32|63->38|64->39|66->41|66->41|72->47|74->49|74->49|80->55|82->57|82->57|88->63|90->65|90->65|96->71|98->73|98->73|104->79|107->82|107->82|108->83|108->83|108->83|108->83|108->83|114->89|114->89|116->91|116->91|127->102|127->102|128->103|128->103|128->103|128->103|129->104|129->104|130->105|130->105|132->107|132->107|135->110
                    -- GENERATED --
                */
            