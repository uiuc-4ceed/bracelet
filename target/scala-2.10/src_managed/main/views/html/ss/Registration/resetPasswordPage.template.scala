
package views.html.ss.Registration

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object resetPasswordPage extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Form[scala.Tuple2[String, String]],String,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(resetForm:Form[(String, String)], token: String)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import securesocial.core.IdentityProvider

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(securesocial.views.html.inputFieldConstructor.f) }};
Seq[Any](format.raw/*1.84*/("""
"""),format.raw/*4.99*/("""

"""),_display_(Seq[Any](/*6.2*/main(Messages("securesocial.password.reset") )/*6.48*/ {_display_(Seq[Any](format.raw/*6.50*/("""
    <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 panel panel-default">
        <div class="page-header">
            <h1>Reset Password</h1>
        </div>

        """),_display_(Seq[Any](/*12.10*/request/*12.17*/.flash.get("error").map/*12.40*/ { msg =>_display_(Seq[Any](format.raw/*12.49*/("""
            <div class="alert alert-info">
                """),_display_(Seq[Any](/*14.18*/Messages(msg))),format.raw/*14.31*/("""
            </div>
        """)))})),format.raw/*16.10*/("""

        <div class="clearfix">
            <p>"""),_display_(Seq[Any](/*19.17*/Messages("Please type in your new password with at least 8 characters and confirm it"))),format.raw/*19.103*/("""</p>
        </div>

        <form action=""""),_display_(Seq[Any](/*22.24*/securesocial/*22.36*/.core.providers.utils.RoutesHelper.handleResetPassword(token).absoluteURL(IdentityProvider.sslEnabled))),format.raw/*22.138*/(""""
              class="form-horizontal"
              autocomplete="off"
              method="POST"
        >
            <fieldset>
                """),_display_(Seq[Any](/*28.18*/helper/*28.24*/.inputPassword(
                    resetForm("password.password1"),
                    '_label -> Messages("securesocial.signup.password1"),
                    'class -> "form-control",
                    '_help -> "",
                    '_showErrors -> true
                ))),format.raw/*34.18*/("""

                """),_display_(Seq[Any](/*36.18*/helper/*36.24*/.inputPassword(
                    resetForm("password.password2"),
                    '_label -> Messages("securesocial.signup.password2"),
                    'class -> "form-control",
                    '_help -> "",
                    '_showErrors -> true
                ))),format.raw/*42.18*/("""

                <div class="form-actions login-nav-box">
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-wrench"></span> """),_display_(Seq[Any](/*45.117*/Messages("securesocial.password.reset"))),format.raw/*45.156*/("""</button>
                </div>
            </fieldset>
        </form>
    </div>
""")))})),format.raw/*50.2*/("""


"""))}
    }
    
    def render(resetForm:Form[scala.Tuple2[String, String]],token:String,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(resetForm,token)(request)
    
    def f:((Form[scala.Tuple2[String, String]],String) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (resetForm,token) => (request) => apply(resetForm,token)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/Registration/resetPasswordPage.scala.html
                    HASH: 81917d633b176fae10f661226e4903e8c98b8080
                    MATRIX: 664->1|891->145|923->169|1026->83|1054->242|1091->245|1145->291|1184->293|1404->477|1420->484|1452->507|1499->516|1596->577|1631->590|1692->619|1777->668|1886->754|1966->798|1987->810|2112->912|2299->1063|2314->1069|2617->1350|2672->1369|2687->1375|2990->1656|3202->1831|3264->1870|3380->1955
                    LINES: 20->1|25->4|25->4|26->1|27->4|29->6|29->6|29->6|35->12|35->12|35->12|35->12|37->14|37->14|39->16|42->19|42->19|45->22|45->22|45->22|51->28|51->28|57->34|59->36|59->36|65->42|68->45|68->45|73->50
                    -- GENERATED --
                */
            