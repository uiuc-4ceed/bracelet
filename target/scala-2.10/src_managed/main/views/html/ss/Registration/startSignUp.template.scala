
package views.html.ss.Registration

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object startSignUp extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[String],RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(startForm:Form[String])(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import securesocial.core.IdentityProvider

implicit def /*4.2*/implicitFieldConstructor/*4.26*/ = {{ FieldConstructor(securesocial.views.html.inputFieldConstructor.f) }};
Seq[Any](format.raw/*1.59*/("""
"""),format.raw/*4.99*/("""

"""),_display_(Seq[Any](/*6.2*/main(Messages("securesocial.signup.title") )/*6.46*/ {_display_(Seq[Any](format.raw/*6.48*/("""
    <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 panel panel-default">
        <div class="page-header">
            <h1>Sign Up</h1>
        </div>

        """),_display_(Seq[Any](/*12.10*/request/*12.17*/.flash.get("error").map/*12.40*/ { msg =>_display_(Seq[Any](format.raw/*12.49*/("""
        <div class="alert alert-error">
            """),_display_(Seq[Any](/*14.14*/Messages(msg))),format.raw/*14.27*/("""
        </div>
        """)))})),format.raw/*16.10*/("""

        <div class="clearfix">
            <p>"""),_display_(Seq[Any](/*19.17*/Messages("Please provide your email for further instructions"))),format.raw/*19.79*/("""</p>
        </div>

        <form action=""""),_display_(Seq[Any](/*22.24*/securesocial/*22.36*/.core.providers.utils.RoutesHelper.handleStartSignUp().absoluteURL(IdentityProvider.sslEnabled))),format.raw/*22.131*/(""""
              class="form-horizontal"
              autocomplete="off" method="post"
        >
            <fieldset>
                """),_display_(Seq[Any](/*27.18*/helper/*27.24*/.inputText(
                    startForm("email"),
                    '_label -> Messages("securesocial.signup.email1"),
                    'class -> "form-control",
                    '_help -> "",
                    '_showErrors -> true
                ))),format.raw/*33.18*/("""
                <div class="form-actions login-nav-box">
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> """),_display_(Seq[Any](/*35.113*/Messages("securesocial.signup.createAccount"))),format.raw/*35.158*/("""</button>
                    <a class="btn btn-default" href=""""),_display_(Seq[Any](/*36.55*/securesocial/*36.67*/.core.providers.utils.RoutesHelper.login())),format.raw/*36.109*/(""""><span class="glyphicon glyphicon-remove"></span> """),_display_(Seq[Any](/*36.161*/Messages("securesocial.signup.cancel"))),format.raw/*36.199*/("""</a>
                </div>
            </fieldset>
        </form>
    </div>
""")))})),format.raw/*41.2*/("""
"""))}
    }
    
    def render(startForm:Form[String],request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(startForm)(request)
    
    def f:((Form[String]) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (startForm) => (request) => apply(startForm)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/Registration/startSignUp.scala.html
                    HASH: bd6968fd09aad2630612ebf0ffc1163fd587ca5a
                    MATRIX: 629->1|831->120|863->144|966->58|994->217|1031->220|1083->264|1122->266|1335->443|1351->450|1383->473|1430->482|1520->536|1555->549|1612->574|1697->623|1781->685|1861->729|1882->741|2000->836|2173->973|2188->979|2471->1240|2678->1410|2746->1455|2846->1519|2867->1531|2932->1573|3021->1625|3082->1663|3193->1743
                    LINES: 20->1|25->4|25->4|26->1|27->4|29->6|29->6|29->6|35->12|35->12|35->12|35->12|37->14|37->14|39->16|42->19|42->19|45->22|45->22|45->22|50->27|50->27|56->33|58->35|58->35|59->36|59->36|59->36|59->36|59->36|64->41
                    -- GENERATED --
                */
            