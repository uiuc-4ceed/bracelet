
package views.html.ss

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object login extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[Form[scala.Tuple2[String, String]],Option[String],RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(loginForm: Form[(String,String)], errorMsg: Option[String] = None)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import securesocial.core.Registry

import securesocial.core.AuthenticationMethod._

import securesocial.core.providers.UsernamePasswordProvider.UsernamePassword


Seq[Any](format.raw/*1.102*/("""

"""),format.raw/*7.1*/("""


"""),_display_(Seq[Any](/*10.2*/main(Messages("securesocial.login.title"))/*10.44*/ {_display_(Seq[Any](format.raw/*10.46*/("""
    <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 panel panel-default">
        <div class="page-header">
            <h1>"""),_display_(Seq[Any](/*13.18*/Messages("securesocial.login.title"))),format.raw/*13.54*/("""</h1>
        </div>

        """),_display_(Seq[Any](/*16.10*/errorMsg/*16.18*/.map/*16.22*/ { msg =>_display_(Seq[Any](format.raw/*16.31*/("""
            <div class="alert alert-danger">
                """),_display_(Seq[Any](/*18.18*/Messages(msg))),format.raw/*18.31*/("""
            </div>
        """)))})),format.raw/*20.10*/("""

        """),_display_(Seq[Any](/*22.10*/request/*22.17*/.flash.get("success").map/*22.42*/ { msg =>_display_(Seq[Any](format.raw/*22.51*/("""
            <div class="alert alert-info">
                """),_display_(Seq[Any](/*24.18*/msg)),format.raw/*24.21*/("""
            </div>
        """)))})),format.raw/*26.10*/("""

        """),_display_(Seq[Any](/*28.10*/request/*28.17*/.flash.get("error").map/*28.40*/ { msg =>_display_(Seq[Any](format.raw/*28.49*/("""
            <div class="alert alert-danger">
                """),_display_(Seq[Any](/*30.18*/msg)),format.raw/*30.21*/("""
            </div>
        """)))})),format.raw/*32.10*/("""

        """),_display_(Seq[Any](/*34.10*/defining( Registry.providers.all.values.filter( _.id != UsernamePassword) )/*34.85*/ { externalProviders =>_display_(Seq[Any](format.raw/*34.108*/("""

            """),_display_(Seq[Any](/*36.14*/if( externalProviders.size > 0 )/*36.46*/ {_display_(Seq[Any](format.raw/*36.48*/("""
                <div class="clearfix">
                    <p>"""),_display_(Seq[Any](/*38.25*/Messages("securesocial.login.instructions"))),format.raw/*38.68*/("""</p>
                    <p>
                        """),_display_(Seq[Any](/*40.26*/for(p <- externalProviders) yield /*40.53*/ {_display_(Seq[Any](format.raw/*40.55*/("""
                            """),_display_(Seq[Any](/*41.30*/provider(p.id))),format.raw/*41.44*/("""
                        """)))})),format.raw/*42.26*/("""
                    </p>
                </div>
            """)))})),format.raw/*45.14*/("""

            """),_display_(Seq[Any](/*47.14*/if(play.Play.application().configuration().getBoolean("enableUsernamePassword"))/*47.94*/ {_display_(Seq[Any](format.raw/*47.96*/("""
                """),_display_(Seq[Any](/*48.18*/Registry/*48.26*/.providers.get(UsernamePassword).map/*48.62*/ { up =>_display_(Seq[Any](format.raw/*48.70*/("""
                    <div class="clearfix">
                        """),_display_(Seq[Any](/*50.26*/if( externalProviders.size > 0 )/*50.58*/ {_display_(Seq[Any](format.raw/*50.60*/("""
                            <p>"""),_display_(Seq[Any](/*51.33*/Messages("securesocial.login.useEmailAndPassword"))),format.raw/*51.83*/("""</p>
                        """)))}/*52.27*/else/*52.32*/{_display_(Seq[Any](format.raw/*52.33*/("""
                            <p>"""),_display_(Seq[Any](/*53.33*/Messages("securesocial.login.useEmailAndPasswordOnly"))),format.raw/*53.87*/("""</p>
                        """)))})),format.raw/*54.26*/("""

                       """),_display_(Seq[Any](/*56.25*/provider("userpass", Some(loginForm)))),format.raw/*56.62*/("""
                    </div>
                """)))})),format.raw/*58.18*/("""
            """)))}/*59.15*/else/*59.20*/{_display_(Seq[Any](format.raw/*59.21*/("""
                <div class="clearfix">
                    <p>Please select one of the login methods above.</p>
                </div>
            """)))})),format.raw/*63.14*/("""
        """)))})),format.raw/*64.10*/("""
    </div>
""")))})))}
    }
    
    def render(loginForm:Form[scala.Tuple2[String, String]],errorMsg:Option[String],request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(loginForm,errorMsg)(request)
    
    def f:((Form[scala.Tuple2[String, String]],Option[String]) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (loginForm,errorMsg) => (request) => apply(loginForm,errorMsg)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/login.scala.html
                    HASH: cc0284d7f7b9fbf4aeb99073cf4dc2ebd1271cb8
                    MATRIX: 647->1|1020->101|1048->282|1087->286|1138->328|1178->330|1354->470|1412->506|1479->537|1496->545|1509->549|1556->558|1655->621|1690->634|1751->663|1798->674|1814->681|1848->706|1895->715|1992->776|2017->779|2078->808|2125->819|2141->826|2173->849|2220->858|2319->921|2344->924|2405->953|2452->964|2536->1039|2598->1062|2649->1077|2690->1109|2730->1111|2830->1175|2895->1218|2985->1272|3028->1299|3068->1301|3134->1331|3170->1345|3228->1371|3322->1433|3373->1448|3462->1528|3502->1530|3556->1548|3573->1556|3618->1592|3664->1600|3769->1669|3810->1701|3850->1703|3919->1736|3991->1786|4040->1817|4053->1822|4092->1823|4161->1856|4237->1910|4299->1940|4361->1966|4420->2003|4497->2048|4530->2063|4543->2068|4582->2069|4763->2218|4805->2228
                    LINES: 20->1|30->1|32->7|35->10|35->10|35->10|38->13|38->13|41->16|41->16|41->16|41->16|43->18|43->18|45->20|47->22|47->22|47->22|47->22|49->24|49->24|51->26|53->28|53->28|53->28|53->28|55->30|55->30|57->32|59->34|59->34|59->34|61->36|61->36|61->36|63->38|63->38|65->40|65->40|65->40|66->41|66->41|67->42|70->45|72->47|72->47|72->47|73->48|73->48|73->48|73->48|75->50|75->50|75->50|76->51|76->51|77->52|77->52|77->52|78->53|78->53|79->54|81->56|81->56|83->58|84->59|84->59|84->59|88->63|89->64
                    -- GENERATED --
                */
            