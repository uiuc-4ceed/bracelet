
package views.html.ss

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object inputFieldConstructor extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template1[views.html.helper.FieldElements,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(elements: views.html.helper.FieldElements):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import play.api.i18n._

import views.html.helper._


Seq[Any](format.raw/*1.45*/("""

"""),format.raw/*5.1*/("""
<div class="control-group """),_display_(Seq[Any](/*6.28*/elements/*6.36*/.args.get('_class))),format.raw/*6.54*/(""" """),_display_(Seq[Any](/*6.56*/if(elements.hasErrors)/*6.78*/ {_display_(Seq[Any](format.raw/*6.80*/("""error""")))})),format.raw/*6.86*/("""" id=""""),_display_(Seq[Any](/*6.93*/elements/*6.101*/.args.get('_id).getOrElse(elements.id + "_field"))),format.raw/*6.150*/("""">
    <label class="control-label" for=""""),_display_(Seq[Any](/*7.40*/elements/*7.48*/.id)),format.raw/*7.51*/("""">"""),_display_(Seq[Any](/*7.54*/elements/*7.62*/.label(elements.lang))),format.raw/*7.83*/("""</label>
    <div class="controls">
        """),_display_(Seq[Any](/*9.10*/elements/*9.18*/.input)),format.raw/*9.24*/("""
	    """),_display_(Seq[Any](/*10.7*/if( elements.hasErrors )/*10.31*/ {_display_(Seq[Any](format.raw/*10.33*/("""
            <span class="help-inline">"""),_display_(Seq[Any](/*11.40*/elements/*11.48*/.errors(elements.lang).mkString(", "))),format.raw/*11.85*/("""</span>
        """)))}/*12.11*/else/*12.16*/{_display_(Seq[Any](format.raw/*12.17*/("""
            <span class="help-block">"""),_display_(Seq[Any](/*13.39*/elements/*13.47*/.infos(elements.lang).mkString(", "))),format.raw/*13.83*/("""</span>
        """)))})),format.raw/*14.10*/("""
    </div>
</div>"""))}
    }
    
    def render(elements:views.html.helper.FieldElements): play.api.templates.HtmlFormat.Appendable = apply(elements)
    
    def f:((views.html.helper.FieldElements) => play.api.templates.HtmlFormat.Appendable) = (elements) => apply(elements)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/inputFieldConstructor.scala.html
                    HASH: 1a1f083242915b0ea83df7e4473a52595c4d662c
                    MATRIX: 631->1|819->44|847->98|910->126|926->134|965->152|1002->154|1032->176|1071->178|1108->184|1150->191|1167->199|1238->248|1315->290|1331->298|1355->301|1393->304|1409->312|1451->333|1531->378|1547->386|1574->392|1616->399|1649->423|1689->425|1765->465|1782->473|1841->510|1877->528|1890->533|1929->534|2004->573|2021->581|2079->617|2128->634
                    LINES: 20->1|26->1|28->5|29->6|29->6|29->6|29->6|29->6|29->6|29->6|29->6|29->6|29->6|30->7|30->7|30->7|30->7|30->7|30->7|32->9|32->9|32->9|33->10|33->10|33->10|34->11|34->11|34->11|35->12|35->12|35->12|36->13|36->13|36->13|37->14
                    -- GENERATED --
                */
            