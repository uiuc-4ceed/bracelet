
package views.html.ss

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object ldap extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[String,String,String,RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(redirecturl:String, token:String, provider: String)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.87*/("""
"""),_display_(Seq[Any](/*2.2*/main("LDAP login")/*2.20*/{_display_(Seq[Any](format.raw/*2.21*/("""
    <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 panel panel-default">
        <div class="page-header">
            <h1>LDAP Login </h1>
            <p>Provided by """),_display_(Seq[Any](/*6.29*/provider)),format.raw/*6.37*/("""</p>
        </div>

        <form  autocomplete="off" onsubmit="return false;">
            <input type="hidden" name="redirecturl" value=""""),_display_(Seq[Any](/*10.61*/redirecturl)),format.raw/*10.72*/("""" />
            <input type="hidden" name="token" value=""""),_display_(Seq[Any](/*11.55*/token)),format.raw/*11.60*/("""" />
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="username" />
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" />
            </div>
                <button class="btn btn-primary btn-margins btn-margin-bottom" onclick="login()">Submit</button>
        </form>
    </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src=""""),_display_(Seq[Any](/*24.46*/routes/*24.52*/.Application.javascriptRoutes)),format.raw/*24.81*/(""""></script>
        <script type="text/javascript">
                function login()"""),format.raw/*26.33*/("""{"""),format.raw/*26.34*/("""
                    var username = $('#username').val();
                    var password = $('#password').val();
                    ldap_session_login(username, password);
                    var myNode = document.getElementById("errormessage");
                    myNode.innerHTML = '';
                """),format.raw/*32.17*/("""}"""),format.raw/*32.18*/("""

                function ldap_session_login(username, password) """),format.raw/*34.65*/("""{"""),format.raw/*34.66*/("""
                    return authenticate(function(result)"""),format.raw/*35.57*/("""{"""),format.raw/*35.58*/("""

                        if( ldap_cookie_check(result) !== false)"""),format.raw/*37.65*/("""{"""),format.raw/*37.66*/("""
                            document.cookie = 'ldap.token_key="""),_display_(Seq[Any](/*38.64*/token)),format.raw/*38.69*/("""; expires=' + new Date().getTime() +3600;
                            var user = result.user;
                            var userString = encodeURIComponent(JSON.stringify(result.user));
                            window.location.replace(""""),_display_(Seq[Any](/*41.55*/redirecturl)),format.raw/*41.66*/("""?token="""),_display_(Seq[Any](/*41.74*/token)),format.raw/*41.79*/("""&user="+ userString);
                        """),format.raw/*42.25*/("""}"""),format.raw/*42.26*/(""" else """),format.raw/*42.32*/("""{"""),format.raw/*42.33*/("""
                            window.location.replace(""""),_display_(Seq[Any](/*43.55*/redirecturl)),format.raw/*43.66*/("""?token="""),_display_(Seq[Any](/*43.74*/token)),format.raw/*43.79*/("""");
                        """),format.raw/*44.25*/("""}"""),format.raw/*44.26*/("""
                    """),format.raw/*45.21*/("""}"""),format.raw/*45.22*/(""", function()"""),format.raw/*45.34*/("""{"""),format.raw/*45.35*/("""
                        window.location.replace(""""),_display_(Seq[Any](/*46.51*/redirecturl)),format.raw/*46.62*/("""?token="""),_display_(Seq[Any](/*46.70*/token)),format.raw/*46.75*/("""");
                    """),format.raw/*47.21*/("""}"""),format.raw/*47.22*/("""
                    );
                """),format.raw/*49.17*/("""}"""),format.raw/*49.18*/("""

                //delete the ldap token
                function ldap_session_logout() """),format.raw/*52.48*/("""{"""),format.raw/*52.49*/("""
                    setcookie('ldap.token_key', '', new Date().getTime()-3600, '/');
                """),format.raw/*54.17*/("""}"""),format.raw/*54.18*/("""

                // check if cookie is valid, will return FALSE or user object
                function ldap_cookie_check(result) """),format.raw/*57.52*/("""{"""),format.raw/*57.53*/("""

                    if (result === false) """),format.raw/*59.43*/("""{"""),format.raw/*59.44*/("""
                        ldap_session_logout();
                        return false;
                    """),format.raw/*62.21*/("""}"""),format.raw/*62.22*/("""
                    if (!result.user.hasOwnProperty("active") || !result.user.active) """),format.raw/*63.87*/("""{"""),format.raw/*63.88*/("""
                        ldap_session_logout();
                        return false;
                    """),format.raw/*66.21*/("""}"""),format.raw/*66.22*/("""
                    if (result.user.hasOwnProperty("fullname")) """),format.raw/*67.65*/("""{"""),format.raw/*67.66*/("""
                        ldap_session_logout();
                        return false;
                    """),format.raw/*70.21*/("""}"""),format.raw/*70.22*/("""
                    return result.user;
                """),format.raw/*72.17*/("""}"""),format.raw/*72.18*/("""

                function authenticate(done, err)"""),format.raw/*74.49*/("""{"""),format.raw/*74.50*/("""
                    var username = $('#username').val();
                    var password = $('#password').val();
                    var req = $.ajax("""),format.raw/*77.38*/("""{"""),format.raw/*77.39*/("""
                        method: "POST",
                        url: jsRoutes.controllers. Login.ldapAuthenticate(username, password).url,
                        async: false,
                        success: function(data) """),format.raw/*81.49*/("""{"""),format.raw/*81.50*/("""
                            console.log("Authenticate User: " + data.user['fullName']);
                            done(data);
                            return data;
                        """),format.raw/*85.25*/("""}"""),format.raw/*85.26*/(""",
                        error: function(xhr) """),format.raw/*86.46*/("""{"""),format.raw/*86.47*/("""
                            console.log(xhr.responseText);
                            // code to show error message, not used for now since we redirect to the from page.
                            // var myNode = document.getElementById("errormessage");
                            // myNode.innerHTML = xhr.responseText;
                            err();
                            return false;
                        """),format.raw/*93.25*/("""}"""),format.raw/*93.26*/("""

                    """),format.raw/*95.21*/("""}"""),format.raw/*95.22*/(""");
                """),format.raw/*96.17*/("""}"""),format.raw/*96.18*/("""

                function setcookie(cname, cvalue, exdays, path, severname) """),format.raw/*98.76*/("""{"""),format.raw/*98.77*/("""
                    var d = new Date();
                    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                    var expires = "expires="+d.toUTCString();
                    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=" + path+ ";";
                """),format.raw/*103.17*/("""}"""),format.raw/*103.18*/("""

        </script>

""")))})),format.raw/*107.2*/("""
"""))}
    }
    
    def render(redirecturl:String,token:String,provider:String,request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(redirecturl,token,provider)(request)
    
    def f:((String,String,String) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (redirecturl,token,provider) => (request) => apply(redirecturl,token,provider)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/ldap.scala.html
                    HASH: 29720f2a107907576d1bf00d7658719a40a6aaf8
                    MATRIX: 617->1|796->86|832->88|858->106|896->107|1115->291|1144->299|1321->440|1354->451|1449->510|1476->515|2202->1205|2217->1211|2268->1240|2380->1324|2409->1325|2745->1633|2774->1634|2868->1700|2897->1701|2982->1758|3011->1759|3105->1825|3134->1826|3234->1890|3261->1895|3539->2137|3572->2148|3616->2156|3643->2161|3717->2207|3746->2208|3780->2214|3809->2215|3900->2270|3933->2281|3977->2289|4004->2294|4060->2322|4089->2323|4138->2344|4167->2345|4207->2357|4236->2358|4323->2409|4356->2420|4400->2428|4427->2433|4479->2457|4508->2458|4576->2498|4605->2499|4722->2588|4751->2589|4881->2691|4910->2692|5069->2823|5098->2824|5170->2868|5199->2869|5333->2975|5362->2976|5477->3063|5506->3064|5640->3170|5669->3171|5762->3236|5791->3237|5925->3343|5954->3344|6039->3401|6068->3402|6146->3452|6175->3453|6355->3605|6384->3606|6638->3832|6667->3833|6889->4027|6918->4028|6993->4075|7022->4076|7476->4502|7505->4503|7555->4525|7584->4526|7631->4545|7660->4546|7765->4623|7794->4624|8118->4919|8148->4920|8202->4942
                    LINES: 20->1|23->1|24->2|24->2|24->2|28->6|28->6|32->10|32->10|33->11|33->11|46->24|46->24|46->24|48->26|48->26|54->32|54->32|56->34|56->34|57->35|57->35|59->37|59->37|60->38|60->38|63->41|63->41|63->41|63->41|64->42|64->42|64->42|64->42|65->43|65->43|65->43|65->43|66->44|66->44|67->45|67->45|67->45|67->45|68->46|68->46|68->46|68->46|69->47|69->47|71->49|71->49|74->52|74->52|76->54|76->54|79->57|79->57|81->59|81->59|84->62|84->62|85->63|85->63|88->66|88->66|89->67|89->67|92->70|92->70|94->72|94->72|96->74|96->74|99->77|99->77|103->81|103->81|107->85|107->85|108->86|108->86|115->93|115->93|117->95|117->95|118->96|118->96|120->98|120->98|125->103|125->103|129->107
                    -- GENERATED --
                */
            