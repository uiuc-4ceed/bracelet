
package views.html.ss

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object provider extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template3[String,Option[Form[scala.Tuple2[String, String]]],RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(providerId: String, loginForm: Option[Form[(String, String)]] = None)(implicit request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import securesocial.core.Registry

import securesocial.core.IdentityProvider

import securesocial.core.providers.UsernamePasswordProvider

import securesocial.core.AuthenticationMethod._

import securesocial.core.providers.utils.RoutesHelper

import play.api.Logger

import helper._

implicit def /*10.2*/implicitFieldConstructor/*10.26*/ = {{ FieldConstructor(securesocial.views.html.inputFieldConstructor.f) }};
Seq[Any](format.raw/*1.105*/("""

"""),format.raw/*10.99*/("""

"""),_display_(Seq[Any](/*12.2*/Registry/*12.10*/.providers.get(providerId).map/*12.40*/ { provider =>_display_(Seq[Any](format.raw/*12.54*/("""
            """),_display_(Seq[Any](/*13.14*/if( provider.authMethod == OAuth1 || provider.authMethod == OAuth2 )/*13.82*/ {_display_(Seq[Any](format.raw/*13.84*/("""
                """),_display_(Seq[Any](/*14.18*/defining( "securesocial/images/providers/%s.png".format(provider.id) )/*14.88*/ { imageUrl =>_display_(Seq[Any](format.raw/*14.102*/("""
                    <a href=""""),_display_(Seq[Any](/*15.31*/provider/*15.39*/.authenticationUrl)),format.raw/*15.57*/(""""> <img src=""""),_display_(Seq[Any](/*15.71*/RoutesHelper/*15.83*/.at(imageUrl))),format.raw/*15.96*/(""""/></a>
                """)))})),format.raw/*16.18*/("""
            """)))})),format.raw/*17.14*/("""

            """),_display_(Seq[Any](/*19.14*/if( provider.authMethod == services.CrowdProvider.authMethod )/*19.76*/ {_display_(Seq[Any](format.raw/*19.78*/("""
                """),_display_(Seq[Any](/*20.18*/defining( "securesocial/images/providers/%s.png".format(provider.id) )/*20.88*/ { imageUrl =>_display_(Seq[Any](format.raw/*20.102*/("""
                    <a href=""""),_display_(Seq[Any](/*21.31*/provider/*21.39*/.authenticationUrl)),format.raw/*21.57*/(""""> <img src=""""),_display_(Seq[Any](/*21.71*/RoutesHelper/*21.83*/.at(imageUrl))),format.raw/*21.96*/(""""/></a>
                """)))})),format.raw/*22.18*/("""
            """)))})),format.raw/*23.14*/("""

            """),_display_(Seq[Any](/*25.14*/if( provider.authMethod == services.LdapProvider.authMethod )/*25.75*/ {_display_(Seq[Any](format.raw/*25.77*/("""
                """),_display_(Seq[Any](/*26.18*/defining( "securesocial/images/providers/%s.png".format(provider.id) )/*26.88*/ { imageUrl =>_display_(Seq[Any](format.raw/*26.102*/("""
                    <a href=""""),_display_(Seq[Any](/*27.31*/provider/*27.39*/.authenticationUrl)),format.raw/*27.57*/(""""> <img src=""""),_display_(Seq[Any](/*27.71*/RoutesHelper/*27.83*/.at(imageUrl))),format.raw/*27.96*/(""""/></a>
                """)))})),format.raw/*28.18*/("""
            """)))})),format.raw/*29.14*/("""

            """),_display_(Seq[Any](/*31.14*/if( provider.authMethod == UserPassword )/*31.55*/ {_display_(Seq[Any](format.raw/*31.57*/("""
                <form action = """"),_display_(Seq[Any](/*32.34*/securesocial/*32.46*/.core.providers.utils.RoutesHelper.authenticateByPost("userpass").absoluteURL(IdentityProvider.sslEnabled))),format.raw/*32.152*/(""""
                      class="form-horizontal" autocomplete="off" method="POST">
                    <fieldset>                                                    
                        """),_display_(Seq[Any](/*35.26*/if( UsernamePasswordProvider.withUserNameSupport )/*35.76*/ {_display_(Seq[Any](format.raw/*35.78*/("""
                            """),_display_(Seq[Any](/*36.30*/helper/*36.36*/.inputText(
                                loginForm.get("username"),
                                '_label -> Messages("securesocial.signup.username"),
                                'class -> "form-control",
                                'autofocus -> "autofocus",
                                '_help -> "",
                                '_showErrors -> true
                            ))),format.raw/*43.30*/("""
                        """)))}/*44.27*/else/*44.32*/{_display_(Seq[Any](format.raw/*44.33*/("""                                                   
                            """),_display_(Seq[Any](/*45.30*/helper/*45.36*/.inputText(
                                loginForm.get("username"),
                                '_label -> Messages("securesocial.signup.email1"),
                                'class -> "form-control",
                                'autofocus -> "autofocus",
                                '_help -> "",
                                '_showErrors -> true
                            ))),format.raw/*52.30*/("""                                                   
                        """)))})),format.raw/*53.26*/("""

                        """),_display_(Seq[Any](/*55.26*/helper/*55.32*/.inputPassword(
                            loginForm.get("password"),
                            '_label -> Messages("securesocial.signup.password1"),
                            'class -> "form-control",
                            '_help -> "",
                            '_showErrors -> true
                        ))),format.raw/*61.26*/("""

                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary btn-margins"><span class="glyphicon glyphicon-log-in"></span> """),_display_(Seq[Any](/*64.137*/Messages("securesocial.login.button"))),format.raw/*64.174*/("""</button>
                        </div>
                        <div class="clearfix">
                            <a href=""""),_display_(Seq[Any](/*67.39*/securesocial/*67.51*/.core.providers.utils.RoutesHelper.startResetPassword())),format.raw/*67.106*/(""""><h5>"""),_display_(Seq[Any](/*67.113*/Messages("Forgot password?"))),format.raw/*67.141*/(""" </a></h5>
                        </div>
                        <div><hr></div>
                        <div class="clearfix">
                            <div class="h4 text-center login-nav-box">"""),_display_(Seq[Any](/*71.72*/Messages("Don't have an account?"))),format.raw/*71.106*/("""&nbsp;<a href=""""),_display_(Seq[Any](/*71.122*/securesocial/*71.134*/.core.providers.utils.RoutesHelper.startSignUp())),format.raw/*71.182*/("""">"""),_display_(Seq[Any](/*71.185*/Messages("Sign up."))),format.raw/*71.205*/("""</a></div>
                        </div>
                    </fieldset>
                </form>
            """)))})),format.raw/*75.14*/("""
""")))}/*76.2*/.getOrElse/*76.12*/ {_display_(Seq[Any](format.raw/*76.14*/("""
    """),format.raw/*79.48*/("""
    """),_display_(Seq[Any](/*80.6*/Logger/*80.12*/.error("[securesocial] unknown provider '%s'. Can't render it.".format(providerId)))),format.raw/*80.95*/("""
    """),format.raw/*81.5*/("""{"""),format.raw/*81.6*/(""" throw new RuntimeException("Unknown provider '%s') """),format.raw/*81.58*/("""}"""),format.raw/*81.59*/("""
""")))})),format.raw/*82.2*/("""
"""))}
    }
    
    def render(providerId:String,loginForm:Option[Form[scala.Tuple2[String, String]]],request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(providerId,loginForm)(request)
    
    def f:((String,Option[Form[scala.Tuple2[String, String]]]) => (RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (providerId,loginForm) => (request) => apply(providerId,loginForm)(request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:40 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/ss/provider.scala.html
                    HASH: 504e070af778c0620a1d9c5db001b37d47f0c22b
                    MATRIX: 650->1|1123->391|1156->415|1260->104|1290->488|1328->491|1345->499|1384->529|1436->543|1486->557|1563->625|1603->627|1657->645|1736->715|1789->729|1856->760|1873->768|1913->786|1963->800|1984->812|2019->825|2076->850|2122->864|2173->879|2244->941|2284->943|2338->961|2417->1031|2470->1045|2537->1076|2554->1084|2594->1102|2644->1116|2665->1128|2700->1141|2757->1166|2803->1180|2854->1195|2924->1256|2964->1258|3018->1276|3097->1346|3150->1360|3217->1391|3234->1399|3274->1417|3324->1431|3345->1443|3380->1456|3437->1481|3483->1495|3534->1510|3584->1551|3624->1553|3694->1587|3715->1599|3844->1705|4070->1895|4129->1945|4169->1947|4235->1977|4250->1983|4673->2384|4718->2411|4731->2416|4770->2417|4887->2498|4902->2504|5323->2903|5432->2980|5495->3007|5510->3013|5855->3336|6081->3525|6141->3562|6303->3688|6324->3700|6402->3755|6446->3762|6497->3790|6733->3990|6790->4024|6843->4040|6865->4052|6936->4100|6976->4103|7019->4123|7162->4234|7182->4236|7201->4246|7241->4248|7274->4411|7315->4417|7330->4423|7435->4506|7467->4511|7495->4512|7575->4564|7604->4565|7637->4567
                    LINES: 20->1|35->10|35->10|36->1|38->10|40->12|40->12|40->12|40->12|41->13|41->13|41->13|42->14|42->14|42->14|43->15|43->15|43->15|43->15|43->15|43->15|44->16|45->17|47->19|47->19|47->19|48->20|48->20|48->20|49->21|49->21|49->21|49->21|49->21|49->21|50->22|51->23|53->25|53->25|53->25|54->26|54->26|54->26|55->27|55->27|55->27|55->27|55->27|55->27|56->28|57->29|59->31|59->31|59->31|60->32|60->32|60->32|63->35|63->35|63->35|64->36|64->36|71->43|72->44|72->44|72->44|73->45|73->45|80->52|81->53|83->55|83->55|89->61|92->64|92->64|95->67|95->67|95->67|95->67|95->67|99->71|99->71|99->71|99->71|99->71|99->71|99->71|103->75|104->76|104->76|104->76|105->79|106->80|106->80|106->80|107->81|107->81|107->81|107->81|108->82
                    -- GENERATED --
                */
            