
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object logoSelect extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template4[String,String,Option[String],Option[String],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(path: String, name: String, default: Option[String], showText: Option[String]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.81*/("""

<div class="radio">
    """),_display_(Seq[Any](/*4.6*/if(default.isDefined)/*4.27*/ {_display_(Seq[Any](format.raw/*4.29*/("""
        <label><input type="radio" name=""""),_display_(Seq[Any](/*5.43*/(path))),_display_(Seq[Any](/*5.50*/(name))),format.raw/*5.56*/("""Opt" id=""""),_display_(Seq[Any](/*5.66*/(path))),_display_(Seq[Any](/*5.73*/(name))),format.raw/*5.79*/("""Default" value="default">Default <img height="16px" width="16px" src=""""),_display_(Seq[Any](/*5.150*/routes/*5.156*/.Assets.at(default.get))),format.raw/*5.179*/(""""></label>
    """)))}/*6.7*/else/*6.12*/{_display_(Seq[Any](format.raw/*6.13*/("""
        <label><input type="radio" name=""""),_display_(Seq[Any](/*7.43*/(path))),_display_(Seq[Any](/*7.50*/(name))),format.raw/*7.56*/("""Opt" id=""""),_display_(Seq[Any](/*7.66*/(path))),_display_(Seq[Any](/*7.73*/(name))),format.raw/*7.79*/("""Default" value="default">No Logo</label>
    """)))})),format.raw/*8.6*/("""
</div>
"""),_display_(Seq[Any](/*10.2*/services/*10.10*/.DI.injector.getInstance(classOf[services.LogoService]).get(path, name).map/*10.85*/{ logo =>_display_(Seq[Any](format.raw/*10.94*/("""
    <div class="radio">
        <label><input type="radio" name=""""),_display_(Seq[Any](/*12.43*/(path))),_display_(Seq[Any](/*12.50*/(name))),format.raw/*12.56*/("""Opt" value="current" id=""""),_display_(Seq[Any](/*12.82*/(path))),_display_(Seq[Any](/*12.89*/(name))),format.raw/*12.95*/("""Current">Current <img height="16px" src=""""),_display_(Seq[Any](/*12.137*/api/*12.140*/.routes.Logos.downloadPath(path, name))),format.raw/*12.178*/(""""></label>
    </div>
""")))})),format.raw/*14.2*/("""
<div class="radio">
    <label><input type="radio" name=""""),_display_(Seq[Any](/*16.39*/(path))),_display_(Seq[Any](/*16.46*/(name))),format.raw/*16.52*/("""Opt" id=""""),_display_(Seq[Any](/*16.62*/(path))),_display_(Seq[Any](/*16.69*/(name))),format.raw/*16.75*/("""Upload" value="upload" disabled><input class="form-control" id=""""),_display_(Seq[Any](/*16.140*/(path))),_display_(Seq[Any](/*16.147*/(name))),format.raw/*16.153*/("""File" type="file"></label>
</div>
"""),_display_(Seq[Any](/*18.2*/if(showText.isDefined)/*18.24*/ {_display_(Seq[Any](format.raw/*18.26*/("""
    """),_display_(Seq[Any](/*19.6*/services/*19.14*/.DI.injector.getInstance(classOf[services.LogoService]).get(path, name)/*19.85*/ match/*19.91*/ {/*20.9*/case Some(logo) =>/*20.27*/ {_display_(Seq[Any](format.raw/*20.29*/("""
            """),_display_(Seq[Any](/*21.14*/if(logo.showText)/*21.31*/ {_display_(Seq[Any](format.raw/*21.33*/("""
                <input type="checkbox" id=""""),_display_(Seq[Any](/*22.45*/(path))),_display_(Seq[Any](/*22.52*/(name))),format.raw/*22.58*/("""Text" checked> """),_display_(Seq[Any](/*22.74*/showText)),format.raw/*22.82*/("""
            """)))}/*23.15*/else/*23.20*/{_display_(Seq[Any](format.raw/*23.21*/("""
                <input type="checkbox" id=""""),_display_(Seq[Any](/*24.45*/(path))),_display_(Seq[Any](/*24.52*/(name))),format.raw/*24.58*/("""Text"> """),_display_(Seq[Any](/*24.66*/showText)),format.raw/*24.74*/("""
            """)))})),format.raw/*25.14*/("""
        """)))}/*27.9*/case None =>/*27.21*/ {_display_(Seq[Any](format.raw/*27.23*/("""
            <input type="checkbox" id=""""),_display_(Seq[Any](/*28.41*/(path))),_display_(Seq[Any](/*28.48*/(name))),format.raw/*28.54*/("""Text" checked disabled> """),_display_(Seq[Any](/*28.79*/showText)),format.raw/*28.87*/("""
        """)))}})),format.raw/*30.6*/("""
""")))})),format.raw/*31.2*/("""

<script>
    $(function() """),format.raw/*34.18*/("""{"""),format.raw/*34.19*/("""
        $("#"""),_display_(Seq[Any](/*35.14*/(path))),_display_(Seq[Any](/*35.21*/(name))),format.raw/*35.27*/("""File").on("change", select"""),_display_(Seq[Any](/*35.54*/(path))),_display_(Seq[Any](/*35.61*/(name))),format.raw/*35.67*/("""File);
        $("input[name='"""),_display_(Seq[Any](/*36.25*/(path))),_display_(Seq[Any](/*36.32*/(name))),format.raw/*36.38*/("""Opt']").on("change", select"""),_display_(Seq[Any](/*36.66*/(path))),_display_(Seq[Any](/*36.73*/(name))),format.raw/*36.79*/("""Button);

        var current = $('#"""),_display_(Seq[Any](/*38.28*/(path))),_display_(Seq[Any](/*38.35*/(name))),format.raw/*38.41*/("""Current');
        if (current.length != 0) """),format.raw/*39.34*/("""{"""),format.raw/*39.35*/("""
            current.prop('checked', true);
        """),format.raw/*41.9*/("""}"""),format.raw/*41.10*/(""" else """),format.raw/*41.16*/("""{"""),format.raw/*41.17*/("""
            $('#"""),_display_(Seq[Any](/*42.18*/(path))),_display_(Seq[Any](/*42.25*/(name))),format.raw/*42.31*/("""Default').prop('checked', true);
        """),format.raw/*43.9*/("""}"""),format.raw/*43.10*/("""
    """),format.raw/*44.5*/("""}"""),format.raw/*44.6*/(""");

    function select"""),_display_(Seq[Any](/*46.21*/(path))),_display_(Seq[Any](/*46.28*/(name))),format.raw/*46.34*/("""File() """),format.raw/*46.41*/("""{"""),format.raw/*46.42*/("""
        var upload = $('#"""),_display_(Seq[Any](/*47.27*/(path))),_display_(Seq[Any](/*47.34*/(name))),format.raw/*47.40*/("""Upload');
        var enable = typeof $("#"""),_display_(Seq[Any](/*48.34*/(path))),_display_(Seq[Any](/*48.41*/(name))),format.raw/*48.47*/("""File")[0].files[0] !== 'undefined';

        upload.prop('disabled', !enable);
        upload.prop('checked', enable);
        if (enable) """),format.raw/*52.21*/("""{"""),format.raw/*52.22*/("""
            $('#"""),_display_(Seq[Any](/*53.18*/(path))),_display_(Seq[Any](/*53.25*/(name))),format.raw/*53.31*/("""Text').prop('disabled', false);
        """),format.raw/*54.9*/("""}"""),format.raw/*54.10*/(""" else """),format.raw/*54.16*/("""{"""),format.raw/*54.17*/("""
            var current = $('#"""),_display_(Seq[Any](/*55.32*/(path))),_display_(Seq[Any](/*55.39*/(name))),format.raw/*55.45*/("""Current')[0];
            if (current) """),format.raw/*56.26*/("""{"""),format.raw/*56.27*/("""
                current.prop('checked', true);
                $('#"""),_display_(Seq[Any](/*58.22*/(path))),_display_(Seq[Any](/*58.29*/(name))),format.raw/*58.35*/("""Text').prop('disabled', false);
            """),format.raw/*59.13*/("""}"""),format.raw/*59.14*/(""" else """),format.raw/*59.20*/("""{"""),format.raw/*59.21*/("""
                $('#"""),_display_(Seq[Any](/*60.22*/(path))),_display_(Seq[Any](/*60.29*/(name))),format.raw/*60.35*/("""Default').prop('checked', true);
                $('#"""),_display_(Seq[Any](/*61.22*/(path))),_display_(Seq[Any](/*61.29*/(name))),format.raw/*61.35*/("""Text').prop('disabled', true);
            """),format.raw/*62.13*/("""}"""),format.raw/*62.14*/("""
        """),format.raw/*63.9*/("""}"""),format.raw/*63.10*/("""
    """),format.raw/*64.5*/("""}"""),format.raw/*64.6*/("""

    function select"""),_display_(Seq[Any](/*66.21*/(path))),_display_(Seq[Any](/*66.28*/(name))),format.raw/*66.34*/("""Button() """),format.raw/*66.43*/("""{"""),format.raw/*66.44*/("""
        var disable = $('input[name="""),_display_(Seq[Any](/*67.38*/(path))),_display_(Seq[Any](/*67.45*/(name))),format.raw/*67.51*/("""Opt]:checked').val() == "default";
        $('#"""),_display_(Seq[Any](/*68.14*/(path))),_display_(Seq[Any](/*68.21*/(name))),format.raw/*68.27*/("""Text').prop('disabled', disable);
    """),format.raw/*69.5*/("""}"""),format.raw/*69.6*/("""

    function upload"""),_display_(Seq[Any](/*71.21*/(path))),_display_(Seq[Any](/*71.28*/(name))),format.raw/*71.34*/("""File(nextFunction) """),format.raw/*71.53*/("""{"""),format.raw/*71.54*/("""
        switch ($('input[name="""),_display_(Seq[Any](/*72.32*/(path))),_display_(Seq[Any](/*72.39*/(name))),format.raw/*72.45*/("""Opt]:checked').val()) """),format.raw/*72.67*/("""{"""),format.raw/*72.68*/("""
            case "default":
                """),_display_(Seq[Any](/*74.18*/services/*74.26*/.DI.injector.getInstance(classOf[services.LogoService]).get(path, name)/*74.97*/ match/*74.103*/ {/*75.21*/case Some(x) =>/*75.36*/ {_display_(Seq[Any](format.raw/*75.38*/("""
                        $.ajax("""),format.raw/*76.32*/("""{"""),format.raw/*76.33*/("""
                            url:  """"),_display_(Seq[Any](/*77.37*/api/*77.40*/.routes.Logos.deletePath(path, name).url)),format.raw/*77.80*/("""",
                            type: "DELETE",
                        """),format.raw/*79.25*/("""}"""),format.raw/*79.26*/(""").done(function() """),format.raw/*79.44*/("""{"""),format.raw/*79.45*/("""
                            nextFunction();
                        """),format.raw/*81.25*/("""}"""),format.raw/*81.26*/(""").fail(function(jqXHR) """),format.raw/*81.49*/("""{"""),format.raw/*81.50*/("""
                            console.error("The following error occured: " + jqXHR.responseText);
                            notify("The logo for """),_display_(Seq[Any](/*83.51*/path)),format.raw/*83.55*/("""/"""),_display_(Seq[Any](/*83.57*/name)),format.raw/*83.61*/(""" was not updated", "error");
                        """),format.raw/*84.25*/("""}"""),format.raw/*84.26*/(""");
                    """)))}/*86.21*/case None =>/*86.33*/ {_display_(Seq[Any](format.raw/*86.35*/("""
                        nextFunction();
                    """)))}})),format.raw/*89.18*/("""
                break;

            case "current":
                var text = $("#"""),_display_(Seq[Any](/*93.33*/(path))),_display_(Seq[Any](/*93.40*/(name))),format.raw/*93.46*/("""Text")[0];
                $.ajax("""),format.raw/*94.24*/("""{"""),format.raw/*94.25*/("""
                    url:  """"),_display_(Seq[Any](/*95.29*/api/*95.32*/.routes.Logos.putPath(path, name).url)),format.raw/*95.69*/("""",
                    data: JSON.stringify("""),format.raw/*96.42*/("""{"""),format.raw/*96.43*/("""
                        showText: !text || text.checked
                    """),format.raw/*98.21*/("""}"""),format.raw/*98.22*/("""),
                    type: "PUT",
                    contentType: "application/json"
                """),format.raw/*101.17*/("""}"""),format.raw/*101.18*/(""").done(function(response, textStatus, jqXHR) """),format.raw/*101.63*/("""{"""),format.raw/*101.64*/("""
                    nextFunction();
                """),format.raw/*103.17*/("""}"""),format.raw/*103.18*/(""").fail(function(jqXHR) """),format.raw/*103.41*/("""{"""),format.raw/*103.42*/("""
                    console.error("The following error occured: " + jqXHR.responseText);
                    notify("The logo for """),_display_(Seq[Any](/*105.43*/path)),format.raw/*105.47*/("""/"""),_display_(Seq[Any](/*105.49*/name)),format.raw/*105.53*/(""" was not updated", "error");
                """),format.raw/*106.17*/("""}"""),format.raw/*106.18*/(""");
                break;

            case "upload":
                var file = $("#"""),_display_(Seq[Any](/*110.33*/(path))),_display_(Seq[Any](/*110.40*/(name))),format.raw/*110.46*/("""File")[0].files[0];
                var text = $("#"""),_display_(Seq[Any](/*111.33*/(path))),_display_(Seq[Any](/*111.40*/(name))),format.raw/*111.46*/("""Text")[0];
                if (file) """),format.raw/*112.27*/("""{"""),format.raw/*112.28*/("""
                    var fd = new FormData();
                    fd.append('image', file);
                    fd.append('name', '"""),_display_(Seq[Any](/*115.41*/name)),format.raw/*115.45*/("""');
                    fd.append('path', '"""),_display_(Seq[Any](/*116.41*/path)),format.raw/*116.45*/("""');
                    fd.append('showText', !text || text.checked);

                    $.ajax("""),format.raw/*119.28*/("""{"""),format.raw/*119.29*/("""
                        url: """"),_display_(Seq[Any](/*120.32*/api/*120.35*/.routes.Logos.upload().url)),format.raw/*120.61*/("""",
                        data: fd,
                        processData: false,
                        contentType: false,
                        type: "POST"
                    """),format.raw/*125.21*/("""}"""),format.raw/*125.22*/(""").done(function() """),format.raw/*125.40*/("""{"""),format.raw/*125.41*/("""
                        nextFunction();
                    """),format.raw/*127.21*/("""}"""),format.raw/*127.22*/(""").fail(function(jqXHR) """),format.raw/*127.45*/("""{"""),format.raw/*127.46*/("""
                        console.error("The following error occured: " + jqXHR.responseText);
                        notify("The logo for """),_display_(Seq[Any](/*129.47*/path)),format.raw/*129.51*/("""/"""),_display_(Seq[Any](/*129.53*/name)),format.raw/*129.57*/(""" was not updated", "error");
                    """),format.raw/*130.21*/("""}"""),format.raw/*130.22*/(""");
                """),format.raw/*131.17*/("""}"""),format.raw/*131.18*/(""" else """),format.raw/*131.24*/("""{"""),format.raw/*131.25*/("""
                    nextFunction();
                """),format.raw/*133.17*/("""}"""),format.raw/*133.18*/("""
                break;

            default:
                nextFunction();
        """),format.raw/*138.9*/("""}"""),format.raw/*138.10*/("""
    """),format.raw/*139.5*/("""}"""),format.raw/*139.6*/("""
</script>"""))}
    }
    
    def render(path:String,name:String,default:Option[String],showText:Option[String]): play.api.templates.HtmlFormat.Appendable = apply(path,name,default,showText)
    
    def f:((String,String,Option[String],Option[String]) => play.api.templates.HtmlFormat.Appendable) = (path,name,default,showText) => apply(path,name,default,showText)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:24 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/logoSelect.scala.html
                    HASH: 5fa6dce80020322bc88d29fff43cd61707ab4f63
                    MATRIX: 629->1|802->80|863->107|892->128|931->130|1009->173|1045->180|1072->186|1117->196|1153->203|1180->209|1287->280|1302->286|1347->309|1380->326|1392->331|1430->332|1508->375|1544->382|1571->388|1616->398|1652->405|1679->411|1755->457|1799->466|1816->474|1900->549|1947->558|2050->625|2087->632|2115->638|2177->664|2214->671|2242->677|2321->719|2334->722|2395->760|2449->783|2544->842|2581->849|2609->855|2655->865|2692->872|2720->878|2822->943|2860->950|2889->956|2959->991|2990->1013|3030->1015|3071->1021|3088->1029|3168->1100|3183->1106|3193->1117|3220->1135|3260->1137|3310->1151|3336->1168|3376->1170|3457->1215|3494->1222|3522->1228|3574->1244|3604->1252|3637->1267|3650->1272|3689->1273|3770->1318|3807->1325|3835->1331|3879->1339|3909->1347|3955->1361|3983->1380|4004->1392|4044->1394|4121->1435|4158->1442|4186->1448|4247->1473|4277->1481|4319->1497|4352->1499|4408->1527|4437->1528|4487->1542|4524->1549|4552->1555|4615->1582|4652->1589|4680->1595|4747->1626|4784->1633|4812->1639|4876->1667|4913->1674|4941->1680|5014->1717|5051->1724|5079->1730|5151->1774|5180->1775|5259->1827|5288->1828|5322->1834|5351->1835|5405->1853|5442->1860|5470->1866|5538->1907|5567->1908|5599->1913|5627->1914|5687->1938|5724->1945|5752->1951|5787->1958|5816->1959|5879->1986|5916->1993|5944->1999|6023->2042|6060->2049|6088->2055|6255->2194|6284->2195|6338->2213|6375->2220|6403->2226|6470->2266|6499->2267|6533->2273|6562->2274|6630->2306|6667->2313|6695->2319|6762->2358|6791->2359|6896->2428|6933->2435|6961->2441|7033->2485|7062->2486|7096->2492|7125->2493|7183->2515|7220->2522|7248->2528|7338->2582|7375->2589|7403->2595|7474->2638|7503->2639|7539->2648|7568->2649|7600->2654|7628->2655|7686->2677|7723->2684|7751->2690|7788->2699|7817->2700|7891->2738|7928->2745|7956->2751|8040->2799|8077->2806|8105->2812|8170->2850|8198->2851|8256->2873|8293->2880|8321->2886|8368->2905|8397->2906|8465->2938|8502->2945|8530->2951|8580->2973|8609->2974|8691->3020|8708->3028|8788->3099|8804->3105|8815->3128|8839->3143|8879->3145|8939->3177|8968->3178|9041->3215|9053->3218|9115->3258|9214->3329|9243->3330|9289->3348|9318->3349|9415->3418|9444->3419|9495->3442|9524->3443|9708->3591|9734->3595|9772->3597|9798->3601|9879->3654|9908->3655|9951->3700|9972->3712|10012->3714|10107->3794|10228->3879|10265->3886|10293->3892|10355->3926|10384->3927|10449->3956|10461->3959|10520->3996|10592->4040|10621->4041|10726->4118|10755->4119|10888->4223|10918->4224|10992->4269|11022->4270|11104->4323|11134->4324|11186->4347|11216->4348|11385->4480|11412->4484|11451->4486|11478->4490|11552->4535|11582->4536|11705->4622|11743->4629|11772->4635|11861->4687|11899->4694|11928->4700|11994->4737|12024->4738|12193->4870|12220->4874|12301->4918|12328->4922|12455->5020|12485->5021|12554->5053|12567->5056|12616->5082|12827->5264|12857->5265|12904->5283|12934->5284|13024->5345|13054->5346|13106->5369|13136->5370|13313->5510|13340->5514|13379->5516|13406->5520|13484->5569|13514->5570|13562->5589|13592->5590|13627->5596|13657->5597|13739->5650|13769->5651|13883->5737|13913->5738|13946->5743|13975->5744
                    LINES: 20->1|23->1|26->4|26->4|26->4|27->5|27->5|27->5|27->5|27->5|27->5|27->5|27->5|27->5|28->6|28->6|28->6|29->7|29->7|29->7|29->7|29->7|29->7|30->8|32->10|32->10|32->10|32->10|34->12|34->12|34->12|34->12|34->12|34->12|34->12|34->12|34->12|36->14|38->16|38->16|38->16|38->16|38->16|38->16|38->16|38->16|38->16|40->18|40->18|40->18|41->19|41->19|41->19|41->19|41->20|41->20|41->20|42->21|42->21|42->21|43->22|43->22|43->22|43->22|43->22|44->23|44->23|44->23|45->24|45->24|45->24|45->24|45->24|46->25|47->27|47->27|47->27|48->28|48->28|48->28|48->28|48->28|49->30|50->31|53->34|53->34|54->35|54->35|54->35|54->35|54->35|54->35|55->36|55->36|55->36|55->36|55->36|55->36|57->38|57->38|57->38|58->39|58->39|60->41|60->41|60->41|60->41|61->42|61->42|61->42|62->43|62->43|63->44|63->44|65->46|65->46|65->46|65->46|65->46|66->47|66->47|66->47|67->48|67->48|67->48|71->52|71->52|72->53|72->53|72->53|73->54|73->54|73->54|73->54|74->55|74->55|74->55|75->56|75->56|77->58|77->58|77->58|78->59|78->59|78->59|78->59|79->60|79->60|79->60|80->61|80->61|80->61|81->62|81->62|82->63|82->63|83->64|83->64|85->66|85->66|85->66|85->66|85->66|86->67|86->67|86->67|87->68|87->68|87->68|88->69|88->69|90->71|90->71|90->71|90->71|90->71|91->72|91->72|91->72|91->72|91->72|93->74|93->74|93->74|93->74|93->75|93->75|93->75|94->76|94->76|95->77|95->77|95->77|97->79|97->79|97->79|97->79|99->81|99->81|99->81|99->81|101->83|101->83|101->83|101->83|102->84|102->84|103->86|103->86|103->86|105->89|109->93|109->93|109->93|110->94|110->94|111->95|111->95|111->95|112->96|112->96|114->98|114->98|117->101|117->101|117->101|117->101|119->103|119->103|119->103|119->103|121->105|121->105|121->105|121->105|122->106|122->106|126->110|126->110|126->110|127->111|127->111|127->111|128->112|128->112|131->115|131->115|132->116|132->116|135->119|135->119|136->120|136->120|136->120|141->125|141->125|141->125|141->125|143->127|143->127|143->127|143->127|145->129|145->129|145->129|145->129|146->130|146->130|147->131|147->131|147->131|147->131|149->133|149->133|154->138|154->138|155->139|155->139
                    -- GENERATED --
                */
            