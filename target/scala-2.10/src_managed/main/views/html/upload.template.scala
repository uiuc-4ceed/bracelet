
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object upload extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[Form[FileMD],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(myForm: Form[FileMD])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

implicit def /*6.2*/implicitFieldConstructor/*6.26*/ = {{ FieldConstructor(twitterBootstrapInput.f) }};
Seq[Any](format.raw/*1.60*/("""



"""),format.raw/*6.75*/("""

<!-- 
The majority of this file has now been changed to be based off elements from the demonstration HTML page
of the blueimp jQuery File Upload library. An open source project locatd here: http://blueimp.github.io/jQuery-File-Upload/
 -->

<!-- Force latest IE rendering engine or ChromeFrame if installed -->
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->

<!-- blueimp Gallery styles - downloaded to make the resource local -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*19.31*/routes/*19.37*/.Assets.at("stylesheets/file-uploader/blueimp-gallery.min.css"))),format.raw/*19.100*/("""">
<!-- Generic page styles -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*21.31*/routes/*21.37*/.Assets.at("stylesheets/file-uploader/style.css"))),format.raw/*21.86*/("""">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*23.31*/routes/*23.37*/.Assets.at("stylesheets/file-uploader/jquery.fileupload.css"))),format.raw/*23.98*/("""">
<link rel="stylesheet" href=""""),_display_(Seq[Any](/*24.31*/routes/*24.37*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-ui.css"))),format.raw/*24.101*/("""">
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href=""""),_display_(Seq[Any](/*26.41*/routes/*26.47*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-noscript.css"))),format.raw/*26.117*/(""""></noscript>
<noscript><link rel="stylesheet" href=""""),_display_(Seq[Any](/*27.41*/routes/*27.47*/.Assets.at("stylesheets/file-uploader/jquery.fileupload-ui-noscript.css"))),format.raw/*27.120*/(""""></noscript>

"""),_display_(Seq[Any](/*29.2*/main("Clowder")/*29.17*/ {_display_(Seq[Any](format.raw/*29.19*/("""
<div class="container">
  <div class="page-header">
    <h1>File Upload</h1>
  </div>
	   
    <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" action='"""),_display_(Seq[Any](/*36.36*/routes/*36.42*/.Files.upload)),format.raw/*36.55*/("""' method="POST" enctype="multipart/form-data">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript>Javascript is required to use the file uploader.</noscript>
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-link fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files</span>
                        <!-- The file input had "multiple" removed for the current dataset creation method. -->
                    <input type="file" name="files[]" multiple>
                    <input type="hidden" name="datasetLevel" value="DatasetLevel">
                </span>
                <button type="submit" class="btn btn-link start" type="submit">
                    <span class="glyphicon glyphicon-upload"></span> Start upload
                </button>
                <button type="reset" class="btn btn-link cancel">
                    <span class="glyphicon glyphicon-remove"></span> Cancel upload
                </button>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <label>
                    <input type="checkbox" class="toggle"> Select all
                </label>
                <button type="button" class="btn btn-link delete" style="margin-bottom: 0px">
                    <span class="glyphicon glyphicon-trash"></span> Delete selected
                </button>
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
                
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    </form>
</div>
	
	<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
"""),format.raw/*95.1*/("""{"""),format.raw/*95.2*/("""% for (var i=0, file; file=o.files[i]; i++) """),format.raw/*95.46*/("""{"""),format.raw/*95.47*/(""" %"""),format.raw/*95.49*/("""}"""),format.raw/*95.50*/("""
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">"""),format.raw/*101.29*/("""{"""),format.raw/*101.30*/("""%=file.name%"""),format.raw/*101.42*/("""}"""),format.raw/*101.43*/("""</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            """),format.raw/*109.13*/("""{"""),format.raw/*109.14*/("""% if (!i && !o.options.autoUpload) """),format.raw/*109.49*/("""{"""),format.raw/*109.50*/(""" %"""),format.raw/*109.52*/("""}"""),format.raw/*109.53*/("""
                <button type="button" class="btn btn-link start" disabled>
                    <span class="glyphicon glyphicon-play"></span> Start
                </button>
            """),format.raw/*113.13*/("""{"""),format.raw/*113.14*/("""% """),format.raw/*113.16*/("""}"""),format.raw/*113.17*/(""" %"""),format.raw/*113.19*/("""}"""),format.raw/*113.20*/("""
            """),format.raw/*114.13*/("""{"""),format.raw/*114.14*/("""% if (!i) """),format.raw/*114.24*/("""{"""),format.raw/*114.25*/(""" %"""),format.raw/*114.27*/("""}"""),format.raw/*114.28*/("""
                <button type="button" class="btn btn-link cancel">
                    <span class="glyphicon glyphicon-remove"></span> Cancel
                </button>
            """),format.raw/*118.13*/("""{"""),format.raw/*118.14*/("""% """),format.raw/*118.16*/("""}"""),format.raw/*118.17*/(""" %"""),format.raw/*118.19*/("""}"""),format.raw/*118.20*/("""
        </td>
    </tr>
"""),format.raw/*121.1*/("""{"""),format.raw/*121.2*/("""% """),format.raw/*121.4*/("""}"""),format.raw/*121.5*/(""" %"""),format.raw/*121.7*/("""}"""),format.raw/*121.8*/("""
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
"""),format.raw/*125.1*/("""{"""),format.raw/*125.2*/("""% for (var i=0, file; file=o.files[i]; i++) """),format.raw/*125.46*/("""{"""),format.raw/*125.47*/(""" %"""),format.raw/*125.49*/("""}"""),format.raw/*125.50*/("""
    <tr class="template-download fade">
        """),format.raw/*127.9*/("""{"""),format.raw/*127.10*/("""% if (file.deleteUrl) """),format.raw/*127.32*/("""{"""),format.raw/*127.33*/(""" %"""),format.raw/*127.35*/("""}"""),format.raw/*127.36*/("""
        <td>
            <input type="checkbox" name="delete" value="1" class="toggle">
        </td>
        """),format.raw/*131.9*/("""{"""),format.raw/*131.10*/("""% """),format.raw/*131.12*/("""}"""),format.raw/*131.13*/(""" %"""),format.raw/*131.15*/("""}"""),format.raw/*131.16*/("""
        <td>
            <span class="preview">
                """),format.raw/*134.17*/("""{"""),format.raw/*134.18*/("""% if (file.thumbnailUrl) """),format.raw/*134.43*/("""{"""),format.raw/*134.44*/(""" %"""),format.raw/*134.46*/("""}"""),format.raw/*134.47*/("""
                    <a href=""""),format.raw/*135.30*/("""{"""),format.raw/*135.31*/("""%=file.url%"""),format.raw/*135.42*/("""}"""),format.raw/*135.43*/("""" title=""""),format.raw/*135.52*/("""{"""),format.raw/*135.53*/("""%=file.name%"""),format.raw/*135.65*/("""}"""),format.raw/*135.66*/("""" download=""""),format.raw/*135.78*/("""{"""),format.raw/*135.79*/("""%=file.name%"""),format.raw/*135.91*/("""}"""),format.raw/*135.92*/("""" data-gallery><img src=""""),format.raw/*135.117*/("""{"""),format.raw/*135.118*/("""%=file.thumbnailUrl%"""),format.raw/*135.138*/("""}"""),format.raw/*135.139*/(""""></a>
                """),format.raw/*136.17*/("""{"""),format.raw/*136.18*/("""% """),format.raw/*136.20*/("""}"""),format.raw/*136.21*/(""" %"""),format.raw/*136.23*/("""}"""),format.raw/*136.24*/("""
            </span>
        </td>
        <td>
            <p class="name">
                """),format.raw/*141.17*/("""{"""),format.raw/*141.18*/("""% if (file.url) """),format.raw/*141.34*/("""{"""),format.raw/*141.35*/(""" %"""),format.raw/*141.37*/("""}"""),format.raw/*141.38*/("""
                    <a href=""""),format.raw/*142.30*/("""{"""),format.raw/*142.31*/("""%=file.url%"""),format.raw/*142.42*/("""}"""),format.raw/*142.43*/("""" title=""""),format.raw/*142.52*/("""{"""),format.raw/*142.53*/("""%=file.name%"""),format.raw/*142.65*/("""}"""),format.raw/*142.66*/("""" target="_blank" """),format.raw/*142.84*/("""{"""),format.raw/*142.85*/("""%=file.thumbnailUrl?'data-gallery':''%"""),format.raw/*142.123*/("""}"""),format.raw/*142.124*/(""">"""),format.raw/*142.125*/("""{"""),format.raw/*142.126*/("""%=file.name%"""),format.raw/*142.138*/("""}"""),format.raw/*142.139*/("""</a>
                """),format.raw/*143.17*/("""{"""),format.raw/*143.18*/("""% """),format.raw/*143.20*/("""}"""),format.raw/*143.21*/(""" else """),format.raw/*143.27*/("""{"""),format.raw/*143.28*/(""" %"""),format.raw/*143.30*/("""}"""),format.raw/*143.31*/("""
                    <span>"""),format.raw/*144.27*/("""{"""),format.raw/*144.28*/("""%=file.name%"""),format.raw/*144.40*/("""}"""),format.raw/*144.41*/("""</span>
                """),format.raw/*145.17*/("""{"""),format.raw/*145.18*/("""% """),format.raw/*145.20*/("""}"""),format.raw/*145.21*/(""" %"""),format.raw/*145.23*/("""}"""),format.raw/*145.24*/("""
            </p>
            """),format.raw/*147.13*/("""{"""),format.raw/*147.14*/("""% if (file.error) """),format.raw/*147.32*/("""{"""),format.raw/*147.33*/(""" %"""),format.raw/*147.35*/("""}"""),format.raw/*147.36*/("""
                <div><span class="label label-danger">Error</span> """),format.raw/*148.68*/("""{"""),format.raw/*148.69*/("""%=file.error%"""),format.raw/*148.82*/("""}"""),format.raw/*148.83*/("""</div>
            """),format.raw/*149.13*/("""{"""),format.raw/*149.14*/("""% """),format.raw/*149.16*/("""}"""),format.raw/*149.17*/(""" %"""),format.raw/*149.19*/("""}"""),format.raw/*149.20*/("""
        </td>
        <td>
            <span class="size">"""),format.raw/*152.32*/("""{"""),format.raw/*152.33*/("""%=o.formatFileSize(file.size)%"""),format.raw/*152.63*/("""}"""),format.raw/*152.64*/("""</span>
        </td>
        <td>
            """),format.raw/*155.13*/("""{"""),format.raw/*155.14*/("""% if (file.deleteUrl) """),format.raw/*155.36*/("""{"""),format.raw/*155.37*/(""" %"""),format.raw/*155.39*/("""}"""),format.raw/*155.40*/("""
                <button type="button" class="btn btn-link delete" data-type=""""),format.raw/*156.78*/("""{"""),format.raw/*156.79*/("""%=file.deleteType%"""),format.raw/*156.97*/("""}"""),format.raw/*156.98*/("""" data-url=""""),format.raw/*156.110*/("""{"""),format.raw/*156.111*/("""%=file.deleteUrl%"""),format.raw/*156.128*/("""}"""),format.raw/*156.129*/("""""""),format.raw/*156.130*/("""{"""),format.raw/*156.131*/("""% if (file.deleteWithCredentials) """),format.raw/*156.165*/("""{"""),format.raw/*156.166*/(""" %"""),format.raw/*156.168*/("""}"""),format.raw/*156.169*/(""" data-xhr-fields='"""),format.raw/*156.187*/("""{"""),format.raw/*156.188*/(""""withCredentials":true"""),format.raw/*156.210*/("""}"""),format.raw/*156.211*/("""'"""),format.raw/*156.212*/("""{"""),format.raw/*156.213*/("""% """),format.raw/*156.215*/("""}"""),format.raw/*156.216*/(""" %"""),format.raw/*156.218*/("""}"""),format.raw/*156.219*/(""">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </button>
            """),format.raw/*159.13*/("""{"""),format.raw/*159.14*/("""% """),format.raw/*159.16*/("""}"""),format.raw/*159.17*/(""" else """),format.raw/*159.23*/("""{"""),format.raw/*159.24*/(""" %"""),format.raw/*159.26*/("""}"""),format.raw/*159.27*/("""
                <button type="button" class="btn btn-link cancel">
                    <span class="glyphicon glyphicon-remove"></span> Cancel
                </button>
            """),format.raw/*163.13*/("""{"""),format.raw/*163.14*/("""% """),format.raw/*163.16*/("""}"""),format.raw/*163.17*/(""" %"""),format.raw/*163.19*/("""}"""),format.raw/*163.20*/("""
        </td>
    </tr>
"""),format.raw/*166.1*/("""{"""),format.raw/*166.2*/("""% """),format.raw/*166.4*/("""}"""),format.raw/*166.5*/(""" %"""),format.raw/*166.7*/("""}"""),format.raw/*166.8*/("""
</script>

<!-- The Templates plugin is included to render the upload/download listings - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*170.15*/routes/*170.21*/.Assets.at("javascripts/file-uploader/tmpl.min.js"))),format.raw/*170.72*/(""""></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*172.15*/routes/*172.21*/.Assets.at("javascripts/file-uploader/load-image.all.min.js"))),format.raw/*172.82*/(""""></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality - downloaded to make the resource local -->
<script src=""""),_display_(Seq[Any](/*174.15*/routes/*174.21*/.Assets.at("javascripts/file-uploader/canvas-to-blob.min.js"))),format.raw/*174.82*/(""""></script>
<!-- blueimp Gallery script - downloaded to make the resource local-->
<script src=""""),_display_(Seq[Any](/*176.15*/routes/*176.21*/.Assets.at("javascripts/file-uploader/jquery.blueimp-gallery.min.js"))),format.raw/*176.90*/(""""></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src=""""),_display_(Seq[Any](/*178.15*/routes/*178.21*/.Assets.at("javascripts/file-uploader/jquery.iframe-transport.js"))),format.raw/*178.87*/(""""></script>
<!-- The basic File Upload plugin -->
<script src=""""),_display_(Seq[Any](/*180.15*/routes/*180.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload.js"))),format.raw/*180.81*/(""""></script>
<!-- The File Upload processing plugin -->
<script src=""""),_display_(Seq[Any](/*182.15*/routes/*182.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-process.js"))),format.raw/*182.89*/(""""></script>
"""),_display_(Seq[Any](/*183.2*/if(play.Play.application.configuration.getBoolean("clowder.upload.previews", true))/*183.85*/ {_display_(Seq[Any](format.raw/*183.87*/("""
    <!-- The File Upload image preview & resize plugin -->
    <script src=""""),_display_(Seq[Any](/*185.19*/routes/*185.25*/.Assets.at("javascripts/file-uploader/jquery.fileupload-image.js"))),format.raw/*185.91*/(""""></script>
    <!-- The File Upload audio preview plugin -->
    <script src=""""),_display_(Seq[Any](/*187.19*/routes/*187.25*/.Assets.at("javascripts/file-uploader/jquery.fileupload-audio.js"))),format.raw/*187.91*/(""""></script>
    <!-- The File Upload video preview plugin -->
    <script src=""""),_display_(Seq[Any](/*189.19*/routes/*189.25*/.Assets.at("javascripts/file-uploader/jquery.fileupload-video.js"))),format.raw/*189.91*/(""""></script>
""")))})),format.raw/*190.2*/("""
<!-- The File Upload validation plugin -->
<script src=""""),_display_(Seq[Any](/*192.15*/routes/*192.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-validate.js"))),format.raw/*192.90*/(""""></script>
<!-- The File Upload user interface plugin -->
<script src=""""),_display_(Seq[Any](/*194.15*/routes/*194.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-ui.js"))),format.raw/*194.84*/(""""></script>
<!-- The main application script -->
<script src=""""),_display_(Seq[Any](/*196.15*/routes/*196.21*/.Assets.at("javascripts/file-uploader/main.js"))),format.raw/*196.68*/(""""></script>
<!-- Scripts below to facilitate authentication checking on callback before the library uploading is invoked. -->
<script src=""""),_display_(Seq[Any](/*198.15*/routes/*198.21*/.Assets.at("javascripts/file-uploader/jquery.fileupload-clowder-auth.js"))),format.raw/*198.94*/(""""></script>
<script src=""""),_display_(Seq[Any](/*199.15*/routes/*199.21*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*199.63*/("""" type="text/javascript"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="js/cors/jquery.xdr-transport.js"></script>
<![endif]-->
	
""")))})),format.raw/*205.2*/("""
"""))}
    }
    
    def render(myForm:Form[FileMD],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(myForm)(user)
    
    def f:((Form[FileMD]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (myForm) => (user) => apply(myForm)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:28 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/upload.scala.html
                    HASH: 0091e8a95f16c032a22f5c4c8e74f61b2379c713
                    MATRIX: 614->1|774->81|806->105|885->59|916->154|1457->659|1472->665|1558->728|1656->790|1671->796|1742->845|1903->970|1918->976|2001->1037|2070->1070|2085->1076|2172->1140|2314->1246|2329->1252|2422->1322|2512->1376|2527->1382|2623->1455|2674->1471|2698->1486|2738->1488|2977->1691|2992->1697|3027->1710|6089->4745|6117->4746|6189->4790|6218->4791|6248->4793|6277->4794|6455->4943|6485->4944|6526->4956|6556->4957|6969->5341|6999->5342|7063->5377|7093->5378|7124->5380|7154->5381|7370->5568|7400->5569|7431->5571|7461->5572|7492->5574|7522->5575|7564->5588|7594->5589|7633->5599|7663->5600|7694->5602|7724->5603|7935->5785|7965->5786|7996->5788|8026->5789|8057->5791|8087->5792|8140->5817|8169->5818|8199->5820|8228->5821|8258->5823|8287->5824|8439->5948|8468->5949|8541->5993|8571->5994|8602->5996|8632->5997|8709->6046|8739->6047|8790->6069|8820->6070|8851->6072|8881->6073|9020->6184|9050->6185|9081->6187|9111->6188|9142->6190|9172->6191|9266->6256|9296->6257|9350->6282|9380->6283|9411->6285|9441->6286|9500->6316|9530->6317|9570->6328|9600->6329|9638->6338|9668->6339|9709->6351|9739->6352|9780->6364|9810->6365|9851->6377|9881->6378|9936->6403|9967->6404|10017->6424|10048->6425|10100->6448|10130->6449|10161->6451|10191->6452|10222->6454|10252->6455|10374->6548|10404->6549|10449->6565|10479->6566|10510->6568|10540->6569|10599->6599|10629->6600|10669->6611|10699->6612|10737->6621|10767->6622|10808->6634|10838->6635|10885->6653|10915->6654|10983->6692|11014->6693|11045->6694|11076->6695|11118->6707|11149->6708|11199->6729|11229->6730|11260->6732|11290->6733|11325->6739|11355->6740|11386->6742|11416->6743|11472->6770|11502->6771|11543->6783|11573->6784|11626->6808|11656->6809|11687->6811|11717->6812|11748->6814|11778->6815|11837->6845|11867->6846|11914->6864|11944->6865|11975->6867|12005->6868|12102->6936|12132->6937|12174->6950|12204->6951|12252->6970|12282->6971|12313->6973|12343->6974|12374->6976|12404->6977|12492->7036|12522->7037|12581->7067|12611->7068|12687->7115|12717->7116|12768->7138|12798->7139|12829->7141|12859->7142|12966->7220|12996->7221|13043->7239|13073->7240|13115->7252|13146->7253|13193->7270|13224->7271|13255->7272|13286->7273|13350->7307|13381->7308|13413->7310|13444->7311|13492->7329|13523->7330|13575->7352|13606->7353|13637->7354|13668->7355|13700->7357|13731->7358|13763->7360|13794->7361|13938->7476|13968->7477|13999->7479|14029->7480|14064->7486|14094->7487|14125->7489|14155->7490|14366->7672|14396->7673|14427->7675|14457->7676|14488->7678|14518->7679|14571->7704|14600->7705|14630->7707|14659->7708|14689->7710|14718->7711|14902->7858|14918->7864|14992->7915|15194->8080|15210->8086|15294->8147|15477->8293|15493->8299|15577->8360|15711->8457|15727->8463|15819->8532|15974->8650|15990->8656|16079->8722|16180->8786|16196->8792|16279->8852|16385->8921|16401->8927|16492->8995|16541->9008|16634->9091|16675->9093|16790->9171|16806->9177|16895->9243|17012->9323|17028->9329|17117->9395|17234->9475|17250->9481|17339->9547|17384->9560|17479->9618|17495->9624|17587->9693|17697->9766|17713->9772|17799->9835|17899->9898|17915->9904|17985->9951|18162->10091|18178->10097|18274->10170|18337->10196|18353->10202|18418->10244|18688->10482
                    LINES: 20->1|23->6|23->6|24->1|28->6|41->19|41->19|41->19|43->21|43->21|43->21|45->23|45->23|45->23|46->24|46->24|46->24|48->26|48->26|48->26|49->27|49->27|49->27|51->29|51->29|51->29|58->36|58->36|58->36|117->95|117->95|117->95|117->95|117->95|117->95|123->101|123->101|123->101|123->101|131->109|131->109|131->109|131->109|131->109|131->109|135->113|135->113|135->113|135->113|135->113|135->113|136->114|136->114|136->114|136->114|136->114|136->114|140->118|140->118|140->118|140->118|140->118|140->118|143->121|143->121|143->121|143->121|143->121|143->121|147->125|147->125|147->125|147->125|147->125|147->125|149->127|149->127|149->127|149->127|149->127|149->127|153->131|153->131|153->131|153->131|153->131|153->131|156->134|156->134|156->134|156->134|156->134|156->134|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|157->135|158->136|158->136|158->136|158->136|158->136|158->136|163->141|163->141|163->141|163->141|163->141|163->141|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|164->142|165->143|165->143|165->143|165->143|165->143|165->143|165->143|165->143|166->144|166->144|166->144|166->144|167->145|167->145|167->145|167->145|167->145|167->145|169->147|169->147|169->147|169->147|169->147|169->147|170->148|170->148|170->148|170->148|171->149|171->149|171->149|171->149|171->149|171->149|174->152|174->152|174->152|174->152|177->155|177->155|177->155|177->155|177->155|177->155|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|178->156|181->159|181->159|181->159|181->159|181->159|181->159|181->159|181->159|185->163|185->163|185->163|185->163|185->163|185->163|188->166|188->166|188->166|188->166|188->166|188->166|192->170|192->170|192->170|194->172|194->172|194->172|196->174|196->174|196->174|198->176|198->176|198->176|200->178|200->178|200->178|202->180|202->180|202->180|204->182|204->182|204->182|205->183|205->183|205->183|207->185|207->185|207->185|209->187|209->187|209->187|211->189|211->189|211->189|212->190|214->192|214->192|214->192|216->194|216->194|216->194|218->196|218->196|218->196|220->198|220->198|220->198|221->199|221->199|221->199|227->205
                    -- GENERATED --
                */
            