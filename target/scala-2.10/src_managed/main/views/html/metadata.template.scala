
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object metadata extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template7[models.Dataset,Map[String, Any],scala.collection.mutable.Map[String, Any],List[models.DatasetXMLMetadata],Boolean,Option[UUID],Option[models.User],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: models.Dataset, metadata: Map[String,Any], userMetadata: scala.collection.mutable.Map[String,Any], xmlMetadata: List[models.DatasetXMLMetadata], rdfExported: Boolean, curationId: Option[UUID])(implicit user: Option[models.User]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import com.mongodb.casbah.Imports.DBObject

import collection.JavaConverters._

def /*6.2*/printLevel/*6.12*/(metadata: scala.collection.immutable.Map[String,Any]):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*6.70*/("""
	"""),_display_(Seq[Any](/*7.3*/for((key,value) <- metadata) yield /*7.31*/ {_display_(Seq[Any](format.raw/*7.33*/("""
		<li><b>"""),_display_(Seq[Any](/*8.11*/(key))),format.raw/*8.16*/(""":</b>
		"""),_display_(Seq[Any](/*9.4*/if(value.isInstanceOf[String])/*9.34*/ {_display_(Seq[Any](format.raw/*9.36*/("""
			"""),_display_(Seq[Any](/*10.5*/if(value.asInstanceOf[String].startsWith("http://") || value.asInstanceOf[String].startsWith("https://"))/*10.110*/ {_display_(Seq[Any](format.raw/*10.112*/("""
				<a href=""""),_display_(Seq[Any](/*11.15*/value)),format.raw/*11.20*/("""">"""),_display_(Seq[Any](/*11.23*/value)),format.raw/*11.28*/("""</a>
			""")))}/*12.6*/else/*12.11*/{_display_(Seq[Any](format.raw/*12.12*/("""
				"""),_display_(Seq[Any](/*13.6*/value)),format.raw/*13.11*/("""
			""")))})),format.raw/*14.5*/("""
		""")))}/*15.5*/else/*15.10*/{_display_(Seq[Any](format.raw/*15.11*/("""
			"""),_display_(Seq[Any](/*16.5*/if(value.isInstanceOf[java.lang.Integer] || value.isInstanceOf[java.lang.Double] || value.isInstanceOf[java.lang.Float] )/*16.126*/ {_display_(Seq[Any](format.raw/*16.128*/("""
				"""),_display_(Seq[Any](/*17.6*/value/*17.11*/.toString)),format.raw/*17.20*/("""
			""")))}/*18.6*/else/*18.11*/{_display_(Seq[Any](format.raw/*18.12*/("""
				"""),_display_(Seq[Any](/*19.6*/if(value.isInstanceOf[com.mongodb.BasicDBList])/*19.53*/ {_display_(Seq[Any](format.raw/*19.55*/("""
					<ul>
						"""),_display_(Seq[Any](/*21.8*/printList(value.asInstanceOf[com.mongodb.BasicDBList].toList))),format.raw/*21.69*/("""
					</ul>
				""")))}/*23.7*/else/*23.12*/{_display_(Seq[Any](format.raw/*23.13*/("""
					"""),_display_(Seq[Any](/*24.7*/if(value.isInstanceOf[com.mongodb.BasicDBObject])/*24.56*/ {_display_(Seq[Any](format.raw/*24.58*/("""
						<ul>
							"""),_display_(Seq[Any](/*26.9*/printLevel(value.asInstanceOf[com.mongodb.BasicDBObject].toMap().asScala.asInstanceOf[scala.collection.mutable.Map[String, Any]].toMap))),format.raw/*26.144*/("""
						</ul>
					""")))}/*28.8*/else/*28.13*/{_display_(Seq[Any](format.raw/*28.14*/("""
						"""),_display_(Seq[Any](/*29.8*/value/*29.13*/.toString)),format.raw/*29.22*/("""
					""")))})),format.raw/*30.7*/("""
				""")))})),format.raw/*31.6*/("""
			""")))})),format.raw/*32.5*/("""
		""")))})),format.raw/*33.4*/("""</li>
	""")))})),format.raw/*34.3*/("""
""")))};def /*37.2*/printList/*37.11*/(metadata: List[AnyRef]):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*37.39*/("""
	"""),_display_(Seq[Any](/*38.3*/metadata/*38.11*/.indices.map/*38.23*/ { i =>_display_(Seq[Any](format.raw/*38.30*/("""
		<li><b>"""),_display_(Seq[Any](/*39.11*/i)),format.raw/*39.12*/(""":</b>
		"""),_display_(Seq[Any](/*40.4*/if(metadata(i).isInstanceOf[String])/*40.40*/ {_display_(Seq[Any](format.raw/*40.42*/("""
			"""),_display_(Seq[Any](/*41.5*/if(metadata(i).asInstanceOf[String].startsWith("http://") || metadata(i).asInstanceOf[String].startsWith("https://"))/*41.122*/ {_display_(Seq[Any](format.raw/*41.124*/("""
				<a href=""""),_display_(Seq[Any](/*42.15*/metadata(i))),format.raw/*42.26*/("""">"""),_display_(Seq[Any](/*42.29*/metadata(i))),format.raw/*42.40*/("""</a>
			""")))}/*43.6*/else/*43.11*/{_display_(Seq[Any](format.raw/*43.12*/("""
				"""),_display_(Seq[Any](/*44.6*/metadata(i))),format.raw/*44.17*/("""
			""")))})),format.raw/*45.5*/("""
		""")))}/*46.5*/else/*46.10*/{_display_(Seq[Any](format.raw/*46.11*/("""
			"""),_display_(Seq[Any](/*47.5*/if(metadata(i).isInstanceOf[java.lang.Integer] || metadata(i).isInstanceOf[java.lang.Double] || metadata(i).isInstanceOf[java.lang.Float] )/*47.144*/ {_display_(Seq[Any](format.raw/*47.146*/("""
				"""),_display_(Seq[Any](/*48.6*/metadata(i)/*48.17*/.toString)),format.raw/*48.26*/("""
			""")))}/*49.6*/else/*49.11*/{_display_(Seq[Any](format.raw/*49.12*/("""
				"""),_display_(Seq[Any](/*50.6*/if(metadata(i).isInstanceOf[com.mongodb.BasicDBList])/*50.59*/ {_display_(Seq[Any](format.raw/*50.61*/("""
					<ul>
					"""),_display_(Seq[Any](/*52.7*/printList(metadata(i).asInstanceOf[com.mongodb.BasicDBList].toList))),format.raw/*52.74*/("""
					</ul>
				""")))}/*54.7*/else/*54.12*/{_display_(Seq[Any](format.raw/*54.13*/("""
					"""),_display_(Seq[Any](/*55.7*/if(metadata(i).isInstanceOf[com.mongodb.BasicDBObject])/*55.62*/ {_display_(Seq[Any](format.raw/*55.64*/("""
						<ul>
						"""),_display_(Seq[Any](/*57.8*/printLevel(metadata(i).asInstanceOf[com.mongodb.BasicDBObject].toMap().asScala.asInstanceOf[scala.collection.mutable.Map[String, Any]].toMap))),format.raw/*57.149*/("""
						</ul>
					""")))}/*59.8*/else/*59.13*/{_display_(Seq[Any](format.raw/*59.14*/("""
						"""),_display_(Seq[Any](/*60.8*/metadata(i))),format.raw/*60.19*/("""
					""")))})),format.raw/*61.7*/("""
				""")))})),format.raw/*62.6*/("""
			""")))})),format.raw/*63.5*/("""
		""")))})),format.raw/*64.4*/("""</li>
	""")))})),format.raw/*65.3*/("""
""")))};def /*67.3*/printLevelUserMetadata/*67.25*/(metadata: scala.collection.mutable.Map[String,Any]):play.api.templates.HtmlFormat.Appendable = {_display_(

Seq[Any](format.raw/*67.81*/("""
		<ul class="usr_md_">
		"""),_display_(Seq[Any](/*69.4*/for((key,value) <- metadata) yield /*69.32*/ {_display_(Seq[Any](format.raw/*69.34*/("""

			"""),_display_(Seq[Any](/*71.5*/if(value.isInstanceOf[com.mongodb.BasicDBList])/*71.52*/{_display_(Seq[Any](format.raw/*71.53*/("""
				"""),_display_(Seq[Any](/*72.6*/for(listValue <- value.asInstanceOf[com.mongodb.BasicDBList]) yield /*72.67*/ {_display_(Seq[Any](format.raw/*72.69*/("""

                    """),_display_(Seq[Any](/*74.22*/if(!(key == "@context"))/*74.46*/{_display_(Seq[Any](format.raw/*74.47*/("""


						<li class="usr_md_"><b class="usr_md_">"""),_display_(Seq[Any](/*77.47*/(key))),format.raw/*77.52*/(""":</b>
						"""),_display_(Seq[Any](/*78.8*/if(listValue.isInstanceOf[String])/*78.42*/ {_display_(Seq[Any](format.raw/*78.44*/("""
							<span class="usr_md_">"""),_display_(Seq[Any](/*79.31*/listValue)),format.raw/*79.40*/("""</span><button class="usr_md_ btn btn-link btn-sm" type="button">Modify</button>
                            <button class="usr_md_ btn btn-link btn-sm" >Delete</button>
						""")))}/*81.9*/else/*81.14*/{_display_(Seq[Any](format.raw/*81.15*/("""
							<button class="usr_md_ btn btn-link btn-sm" type="button">Add property</button>
                            <button class="usr_md_ btn btn-link btn-sm" type="button">
                                Delete</button>"""),_display_(Seq[Any](/*84.49*/printLevelUserMetadata(listValue.asInstanceOf[DBObject].toMap.asScala.asInstanceOf[scala.collection.mutable.Map[String, Any]]))),format.raw/*84.175*/("""
						""")))})),format.raw/*85.8*/("""</li>
                    """)))})),format.raw/*86.22*/("""

				""")))})),format.raw/*88.6*/("""
			""")))}/*89.5*/else/*89.9*/{_display_(Seq[Any](format.raw/*89.10*/("""
            """),_display_(Seq[Any](/*90.14*/if(!(key == "@context"))/*90.38*/{_display_(Seq[Any](format.raw/*90.39*/("""
				<li class="usr_md_"><b class="usr_md_">"""),_display_(Seq[Any](/*91.45*/(key))),format.raw/*91.50*/(""":</b>
				"""),_display_(Seq[Any](/*92.6*/if(value.isInstanceOf[String])/*92.36*/ {_display_(Seq[Any](format.raw/*92.38*/("""
					<span class="usr_md_">"""),_display_(Seq[Any](/*93.29*/value)),format.raw/*93.34*/("""</span><button class="usr_md_ btn btn-link btn-sm" type="button">Modify</button>
                    <button class="usr_md_ btn btn-link btn-sm" >Delete</button>
				""")))}/*95.7*/else/*95.12*/{_display_(Seq[Any](format.raw/*95.13*/("""
					<button class="usr_md_ btn btn-link btn-sm" type="button">Add property</button>
                    <button class="usr_md_ btn btn-link btn-sm" type="button">
                        Delete</button>"""),_display_(Seq[Any](/*98.41*/printLevelUserMetadata(value.asInstanceOf[DBObject].toMap.asScala.asInstanceOf[scala.collection.mutable.Map[String, Any]]))),format.raw/*98.163*/("""
				""")))})),format.raw/*99.6*/("""</li>
			    """)))})),format.raw/*100.9*/("""
            """)))})),format.raw/*101.14*/("""

		""")))})),format.raw/*103.4*/("""
		</ul>
	""")))};
Seq[Any](format.raw/*1.240*/("""

"""),format.raw/*5.1*/("""
"""),format.raw/*35.2*/("""

"""),format.raw/*66.2*/("""
	"""),format.raw/*105.3*/("""

    <div class="row">
        <div class="col-md-12 box-white-space">
            <h4>Community generated metadata</h4>
            <div id='generalUserMetadata_"""),_display_(Seq[Any](/*110.43*/(dataset.id.stringify))),format.raw/*110.65*/("""' class='usr_md_'>
                <button class="usr_md_ btn btn-link btn-sm" type="button">Add property</button>
                """),_display_(Seq[Any](/*112.18*/printLevelUserMetadata(userMetadata))),format.raw/*112.54*/("""
                <button class="usr_md_submit btn btn-primary btn-xs" type="button"><span class="glyphicon glyphicon-saved"></span> Submit</button>
                """),_display_(Seq[Any](/*114.18*/if(rdfExported)/*114.33*/{_display_(Seq[Any](format.raw/*114.34*/("""
                    <button class="usr_md_submit btn btn-default rdfbtn" type="button"><span class="glyphicon glyphicon-pencil"></span> Get as RDF</button>
                """)))})),format.raw/*116.18*/("""
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 box-white-space">
            <h4>System generated metadata</h4>
            """),_display_(Seq[Any](/*123.14*/if(metadata.isEmpty)/*123.34*/ {_display_(Seq[Any](format.raw/*123.36*/("""
                <div>No system generated metadata available yet</div>
            """)))}/*125.15*/else/*125.20*/{_display_(Seq[Any](format.raw/*125.21*/("""
                <div>"""),_display_(Seq[Any](/*126.23*/printLevel(metadata))),format.raw/*126.43*/("""</div>
            """)))})),format.raw/*127.14*/("""
        </div>
    </div>
	<div class="row">
		<div class="col-md-12  box-white-space">
			<h4>JSON-LD metadata</h4>
			<a href=""""),_display_(Seq[Any](/*133.14*/routes/*133.20*/.Metadata.dataset(dataset.id))),format.raw/*133.49*/("""">Show JSON-LD metadata in new page</a>
		</div>
	</div>

	<script language="javascript">

   var topId = "generalUserMetadata_"""),_display_(Seq[Any](/*139.38*/(dataset.id.stringify))),format.raw/*139.60*/("""";
   window["uploadIp"+topId] = """"),_display_(Seq[Any](/*140.33*/api/*140.36*/.routes.Datasets.addUserMetadata(dataset.id))),format.raw/*140.80*/("""";
   window["rdfIp"+topId] = """"),_display_(Seq[Any](/*141.30*/api/*141.33*/.routes.Datasets.getRDFUserMetadata(dataset.id))),format.raw/*141.80*/("""";
   window["modelIp"+topId] = """"),_display_(Seq[Any](/*142.32*/(routes.Assets.at("datasetsUserMetadataModel")))),format.raw/*142.79*/("""";
   </script>
   <script src=""""),_display_(Seq[Any](/*144.18*/routes/*144.24*/.Assets.at("javascripts/userMetadata.js"))),format.raw/*144.65*/("""" type="text/javascript"></script>
   """),_display_(Seq[Any](/*145.5*/if(!user.isDefined)/*145.24*/ {_display_(Seq[Any](format.raw/*145.26*/("""
		<script language="javascript">
   $("#generalUserMetadata_"""),_display_(Seq[Any](/*147.29*/(dataset.id.stringify))),format.raw/*147.51*/(""" button").css("display","none");
   $("#generalUserMetadata_"""),_display_(Seq[Any](/*148.29*/(dataset.id.stringify))),format.raw/*148.51*/(""" .rdfbtn").css("display","inline");
   </script>
	""")))})),format.raw/*150.3*/("""

"""))}
    }
    
    def render(dataset:models.Dataset,metadata:Map[String, Any],userMetadata:scala.collection.mutable.Map[String, Any],xmlMetadata:List[models.DatasetXMLMetadata],rdfExported:Boolean,curationId:Option[UUID],user:Option[models.User]): play.api.templates.HtmlFormat.Appendable = apply(dataset,metadata,userMetadata,xmlMetadata,rdfExported,curationId)(user)
    
    def f:((models.Dataset,Map[String, Any],scala.collection.mutable.Map[String, Any],List[models.DatasetXMLMetadata],Boolean,Option[UUID]) => (Option[models.User]) => play.api.templates.HtmlFormat.Appendable) = (dataset,metadata,userMetadata,xmlMetadata,rdfExported,curationId) => (user) => apply(dataset,metadata,userMetadata,xmlMetadata,rdfExported,curationId)(user)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Wed Jul 03 14:39:30 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/metadata.scala.html
                    HASH: 4d50e6a884232d18236a20cef3365791f598b664
                    MATRIX: 730->1|1124->323|1142->333|1280->391|1317->394|1360->422|1399->424|1445->435|1471->440|1514->449|1552->479|1591->481|1631->486|1746->591|1787->593|1838->608|1865->613|1904->616|1931->621|1958->631|1971->636|2010->637|2051->643|2078->648|2114->653|2136->658|2149->663|2188->664|2228->669|2359->790|2400->792|2441->798|2455->803|2486->812|2509->818|2522->823|2561->824|2602->830|2658->877|2698->879|2751->897|2834->958|2869->976|2882->981|2921->982|2963->989|3021->1038|3061->1040|3116->1060|3274->1195|3311->1215|3324->1220|3363->1221|3406->1229|3420->1234|3451->1243|3489->1250|3526->1256|3562->1261|3597->1265|3636->1273|3661->1278|3679->1287|3788->1315|3826->1318|3843->1326|3864->1338|3909->1345|3956->1356|3979->1357|4023->1366|4068->1402|4108->1404|4148->1409|4275->1526|4316->1528|4367->1543|4400->1554|4439->1557|4472->1568|4499->1578|4512->1583|4551->1584|4592->1590|4625->1601|4661->1606|4683->1611|4696->1616|4735->1617|4775->1622|4924->1761|4965->1763|5006->1769|5026->1780|5057->1789|5080->1795|5093->1800|5132->1801|5173->1807|5235->1860|5275->1862|5327->1879|5416->1946|5451->1964|5464->1969|5503->1970|5545->1977|5609->2032|5649->2034|5703->2053|5867->2194|5904->2214|5917->2219|5956->2220|5999->2228|6032->2239|6070->2246|6107->2252|6143->2257|6178->2261|6217->2269|6242->2274|6273->2296|6410->2352|6472->2379|6516->2407|6556->2409|6597->2415|6653->2462|6692->2463|6733->2469|6810->2530|6850->2532|6909->2555|6942->2579|6981->2580|7066->2629|7093->2634|7141->2647|7184->2681|7224->2683|7291->2714|7322->2723|7517->2901|7530->2906|7569->2907|7828->3130|7977->3256|8016->3264|8075->3291|8113->3298|8136->3303|8148->3307|8187->3308|8237->3322|8270->3346|8309->3347|8390->3392|8417->3397|8463->3408|8502->3438|8542->3440|8607->3469|8634->3474|8819->3642|8832->3647|8871->3648|9112->3853|9257->3975|9294->3981|9340->3995|9387->4009|9424->4014|9475->239|9503->321|9531->1275|9560->2271|9590->4025|9791->4189|9836->4211|10005->4343|10064->4379|10266->4544|10291->4559|10331->4560|10538->4734|10751->4910|10781->4930|10822->4932|10926->5017|10940->5022|10980->5023|11040->5046|11083->5066|11136->5086|11304->5217|11320->5223|11372->5252|11537->5380|11582->5402|11654->5437|11667->5440|11734->5484|11803->5516|11816->5519|11886->5566|11957->5600|12027->5647|12097->5680|12113->5686|12177->5727|12252->5766|12281->5785|12322->5787|12421->5849|12466->5871|12564->5932|12609->5954|12692->6005
                    LINES: 20->1|25->6|25->6|27->6|28->7|28->7|28->7|29->8|29->8|30->9|30->9|30->9|31->10|31->10|31->10|32->11|32->11|32->11|32->11|33->12|33->12|33->12|34->13|34->13|35->14|36->15|36->15|36->15|37->16|37->16|37->16|38->17|38->17|38->17|39->18|39->18|39->18|40->19|40->19|40->19|42->21|42->21|44->23|44->23|44->23|45->24|45->24|45->24|47->26|47->26|49->28|49->28|49->28|50->29|50->29|50->29|51->30|52->31|53->32|54->33|55->34|56->37|56->37|58->37|59->38|59->38|59->38|59->38|60->39|60->39|61->40|61->40|61->40|62->41|62->41|62->41|63->42|63->42|63->42|63->42|64->43|64->43|64->43|65->44|65->44|66->45|67->46|67->46|67->46|68->47|68->47|68->47|69->48|69->48|69->48|70->49|70->49|70->49|71->50|71->50|71->50|73->52|73->52|75->54|75->54|75->54|76->55|76->55|76->55|78->57|78->57|80->59|80->59|80->59|81->60|81->60|82->61|83->62|84->63|85->64|86->65|87->67|87->67|89->67|91->69|91->69|91->69|93->71|93->71|93->71|94->72|94->72|94->72|96->74|96->74|96->74|99->77|99->77|100->78|100->78|100->78|101->79|101->79|103->81|103->81|103->81|106->84|106->84|107->85|108->86|110->88|111->89|111->89|111->89|112->90|112->90|112->90|113->91|113->91|114->92|114->92|114->92|115->93|115->93|117->95|117->95|117->95|120->98|120->98|121->99|122->100|123->101|125->103|128->1|130->5|131->35|133->66|134->105|139->110|139->110|141->112|141->112|143->114|143->114|143->114|145->116|152->123|152->123|152->123|154->125|154->125|154->125|155->126|155->126|156->127|162->133|162->133|162->133|168->139|168->139|169->140|169->140|169->140|170->141|170->141|170->141|171->142|171->142|173->144|173->144|173->144|174->145|174->145|174->145|176->147|176->147|177->148|177->148|179->150
                    -- GENERATED --
                */
            