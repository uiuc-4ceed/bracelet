
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import play.api.i18n._
import play.api.mvc._
import play.api.data._
import views.html._
import org.bson.types.ObjectId
/**/
object dataset extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template21[models.Dataset,List[Comment],List[Previewer],List[models.Metadata],List[models.Collection],Boolean,List[scala.Tuple3[String, String, String]],Option[Map[ProjectSpace, Boolean]],List[File],scala.collection.immutable.TreeSet[String],Boolean,List[models.CurationObject],Option[String],Int,Boolean,models.DatasetAccess,Boolean,Boolean,play.api.mvc.Flash,Option[models.User],RequestHeader,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(dataset: models.Dataset,
  comments: List[Comment],
  previewers : List[Previewer],
  m: List[models.Metadata],
  collectionsInside: List[models.Collection],
  rdfExported: Boolean,
  relatedSensors: List[(String,String,String)],
  datasetSpaces_canRemove : Option[Map[ProjectSpace,Boolean]],
  fileList: List[File],
  filesTags: scala.collection.immutable.TreeSet[String],
  toPublish: Boolean,
  curationObjects: List[models.CurationObject],
  currentSpace: Option[String],
  limit: Int,
  showDownload: Boolean,
  accessData: models.DatasetAccess,
  canAddDatasetToCollection: Boolean,
  stagingAreaDefined: Boolean)(implicit flash: play.api.mvc.Flash, user: Option[models.User], request: RequestHeader):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import api.Permission

import play.api.Play.current

import _root_.util.Formatters._


Seq[Any](format.raw/*18.118*/("""

"""),format.raw/*23.1*/("""
"""),_display_(Seq[Any](/*24.2*/main(dataset.name)/*24.20*/ {_display_(Seq[Any](format.raw/*24.22*/("""

    <script src=""""),_display_(Seq[Any](/*26.19*/routes/*26.25*/.Assets.at("javascripts/htmlEncodeDecode.js"))),format.raw/*26.70*/("""" language="javascript"></script>
    <script type="text/javascript">
        var spaceId = """"),_display_(Seq[Any](/*28.25*/currentSpace)),format.raw/*28.37*/("""";
        var descLabel = """"),_display_(Seq[Any](/*29.27*/Messages("dataset.description"))),format.raw/*29.58*/("""";
    </script>
    <style>
 /*           .nav>li>a """),format.raw/*32.25*/("""{"""),format.raw/*32.26*/("""
                display: block;
            """),format.raw/*34.13*/("""}"""),format.raw/*34.14*/("""*/
            .nav-tabs>li>a """),format.raw/*35.28*/("""{"""),format.raw/*35.29*/("""
                display: block;
            """),format.raw/*37.13*/("""}"""),format.raw/*37.14*/("""
    </style>
    <div class="row treePadding">
        <!-- left column -->
        <div class="col-md-8">
            <div class="row">
                <ol class="breadcrumb">

                """),_display_(Seq[Any](/*45.18*/datasetSpaces_canRemove/*45.41*/ match/*45.47*/ {/*46.21*/case Some(spaces_map) =>/*46.45*/ {_display_(Seq[Any](format.raw/*46.47*/("""
                        """),_display_(Seq[Any](/*47.26*/if(spaces_map.size == 1)/*47.50*/ {_display_(Seq[Any](format.raw/*47.52*/("""
                                   <li> <span class="glyphicon glyphicon-hdd"></span> <a href=""""),_display_(Seq[Any](/*48.97*/routes/*48.103*/.Spaces.getSpace(spaces_map.head._1.id))),format.raw/*48.142*/("""" title=""""),_display_(Seq[Any](/*48.152*/spaces_map/*48.162*/.head._1.name)),format.raw/*48.175*/(""""> """),_display_(Seq[Any](/*48.179*/Html(ellipsize(spaces_map.head._1.name, 18)))),format.raw/*48.223*/("""</a></li>
                        """)))}/*49.27*/else/*49.33*/{_display_(Seq[Any](format.raw/*49.34*/("""
                            """),_display_(Seq[Any](/*50.30*/if(spaces_map.size > 1)/*50.53*/ {_display_(Seq[Any](format.raw/*50.55*/("""
                                <li>
                                <span class="dropdown">
                                    <button class="btn-link dropdown-toggle" type="button" id="dropdown_space_list" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="true">
                                        <span class="glyphicon glyphicon-hdd"></span> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" arialanelledby="dropdown_space_list">
                                        """),_display_(Seq[Any](/*58.42*/spaces_map/*58.52*/.map/*58.56*/ { case (s,v) =>_display_(Seq[Any](format.raw/*58.72*/("""
                                            <li><a href=""""),_display_(Seq[Any](/*59.59*/routes/*59.65*/.Spaces.getSpace(s.id))),format.raw/*59.87*/("""" title=""""),_display_(Seq[Any](/*59.97*/s/*59.98*/.name)),format.raw/*59.103*/(""""><span class="glyphicon glyphicon-hdd"></span> """),_display_(Seq[Any](/*59.152*/Html(ellipsize(s.name, 18)))),format.raw/*59.179*/("""</a></li>
                                        """)))})),format.raw/*60.42*/("""
                                    </ul>
                                </span>
                                </li>
                            """)))}/*64.31*/else/*64.36*/{_display_(Seq[Any](format.raw/*64.37*/("""
                                <li><span class="glyphicon glyphicon-user"></span> <a href= """"),_display_(Seq[Any](/*65.95*/routes/*65.101*/.Profile.viewProfileUUID(dataset.author.id))),format.raw/*65.144*/(""""> """),_display_(Seq[Any](/*65.148*/dataset/*65.155*/.author.fullName)),format.raw/*65.171*/(""" </a></li>
                            """)))})),format.raw/*66.30*/("""
                        """)))})),format.raw/*67.26*/("""

                    """)))}/*70.21*/case None =>/*70.33*/ {}})),format.raw/*71.18*/("""

                        <li><span class="glyphicon glyphicon-briefcase"></span> <span title=""""),_display_(Seq[Any](/*73.95*/dataset/*73.102*/.name)),format.raw/*73.107*/(""""> """),_display_(Seq[Any](/*73.111*/Html(ellipsize(dataset.name, 18)))),format.raw/*73.144*/("""</span></li>

                </ol>

                <div class="col-md-12 caption break-word dataset-title" id="ds-title">
                    <h1 id="datasettitle" class="inline"> <span class="glyphicon glyphicon-briefcase" ></span> """),_display_(Seq[Any](/*78.113*/Html(dataset.name))),format.raw/*78.131*/("""</h1>
                    """),_display_(Seq[Any](/*79.22*/if(Permission.checkPermission(Permission.EditDataset, ResourceRef(ResourceRef.dataset, dataset.id)) && !dataset.trash)/*79.140*/ {_display_(Seq[Any](format.raw/*79.142*/("""
                        <h3 id="h-edit-title" class="hiddencomplete">
                            <a id ="edit-title" href = "javascript:updateTitle()" title="Click to edit title">
                            <span class ="glyphicon glyphicon-edit" aria-hidden ="true"></span>
                            </a>
                        </h3>
                    """)))})),format.raw/*85.22*/("""

                </div>
                <div class="col-md-6 hideTree">
                    <ul class="list-unstyled">
                        <li>Created by <a href= """"),_display_(Seq[Any](/*90.51*/routes/*90.57*/.Profile.viewProfileUUID(dataset.author.id))),format.raw/*90.100*/(""""> """),_display_(Seq[Any](/*90.104*/dataset/*90.111*/.author.fullName)),format.raw/*90.127*/(""" </a></li>
                        <li>Created on """),_display_(Seq[Any](/*91.41*/dataset/*91.48*/.created.format("MMM dd, yyyy"))),format.raw/*91.79*/("""</li>
                        """),_display_(Seq[Any](/*92.26*/if(accessData.showAccess)/*92.51*/ {_display_(Seq[Any](format.raw/*92.53*/("""
                            """),_display_(Seq[Any](/*93.30*/if(Permission.checkPermission(Permission.PublicDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*93.132*/ {_display_(Seq[Any](format.raw/*93.134*/("""
                                <li id="access">Access:
                                    <label class="radio-inline"><input type="radio" name="access" id="access-DEFAULT" value=""""),_display_(Seq[Any](/*95.127*/DatasetStatus/*95.140*/.DEFAULT.toString)),format.raw/*95.157*/("""">"""),_display_(Seq[Any](/*95.160*/accessData/*95.170*/.accessOptions(0))),format.raw/*95.187*/("""</label>
                                    <label class="radio-inline"><input type="radio" name="access" id="access-PRIVATE" value=""""),_display_(Seq[Any](/*96.127*/DatasetStatus/*96.140*/.PRIVATE.toString)),format.raw/*96.157*/("""">"""),_display_(Seq[Any](/*96.160*/accessData/*96.170*/.accessOptions(1))),format.raw/*96.187*/("""</label>
                                    <label class="radio-inline"><input type="radio" name="access" id="access-PUBLIC" value=""""),_display_(Seq[Any](/*97.126*/DatasetStatus/*97.139*/.PUBLIC.toString)),format.raw/*97.155*/("""">"""),_display_(Seq[Any](/*97.158*/accessData/*97.168*/.accessOptions(2))),format.raw/*97.185*/("""</label>
                                </li>

                            """)))}/*100.31*/else/*100.36*/{_display_(Seq[Any](format.raw/*100.37*/("""
                                <li>Access: """),_display_(Seq[Any](/*101.46*/accessData/*101.56*/.access)),format.raw/*101.63*/("""</li>
                            """)))})),format.raw/*102.30*/("""
                        """)))})),format.raw/*103.26*/("""

                    </ul>
                </div>
                <div class="col-md-6 hideTree">
                    <ul class="list-unstyled">
                        <div id="edit-license">
                            <li class="inline"><span id="licensetextdata">blank</span> <span id="rightsholderdata">blank</span></li>
                            """),_display_(Seq[Any](/*111.30*/if(Permission.checkPermission(Permission.EditLicense, ResourceRef(ResourceRef.dataset, dataset.id)))/*111.130*/ {_display_(Seq[Any](format.raw/*111.132*/("""

                                <a class="edit-tab hiddencomplete" data-toggle="collapse"
                                data-parent="#accordion6" href="#collapseSix" id="editlicense"
                                title=" Click to Edit License Information">
                                    <span class="glyphicon glyphicon-edit"></span>
                                </a>

                            """)))})),format.raw/*119.30*/("""
                        </div>
                        """),_display_(Seq[Any](/*121.26*/datasets/*121.34*/.editLicense(dataset))),format.raw/*121.55*/("""
                    </ul>
                </div>
                <div class="col-md-12 box-white-space" id="aboutdesc" style="padding-top: 0px;">
                    <p id ="ds_description" class="inline">"""),_display_(Seq[Any](/*125.61*/if(dataset.description.length > 0)/*125.95*/ {_display_(Seq[Any](format.raw/*125.97*/(""" """),_display_(Seq[Any](/*125.99*/Html(dataset.description))),format.raw/*125.124*/(""" """)))}/*125.127*/else/*125.132*/{_display_(Seq[Any](format.raw/*125.133*/("""Add a description""")))})),format.raw/*125.151*/("""</p>
                    """),_display_(Seq[Any](/*126.22*/if(Permission.checkPermission(Permission.EditDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*126.122*/ {_display_(Seq[Any](format.raw/*126.124*/("""
                        <a id="edit-description" class="hiddencomplete" href="javascript:updateDescription()" title="Click to edit description">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </a>
                    """)))})),format.raw/*130.22*/("""
                </div>
                    <!-- Added support for mobile view -->
                <div class="col-xs-12 hidden-lg">
                    <hr/>
                    """),_display_(Seq[Any](/*135.22*/if(Permission.checkPermission(Permission.AddResourceToDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*135.131*/ {_display_(Seq[Any](format.raw/*135.133*/("""
                        <a id="addFilesLink" href=""""),_display_(Seq[Any](/*136.53*/(routes.Datasets.addFiles(dataset.id)))),format.raw/*136.91*/(""""
                        class="btn btn-default fileinput-button" id="fileUploadBtn" title="Add File to Dataset" role="button">
                            <span class="glyphicon glyphicon-plus"></span> Add Files
                        </a><p>
                    """)))})),format.raw/*140.22*/("""
                </div>
                <div class="col-xs-12 visible-lg">
                    <hr/>
                        <!-- If the user can add files to the dataset, the button is enabled, otherwise the button is present but disabled to provide consistent UE. -->
                    """),_display_(Seq[Any](/*145.22*/if(Permission.checkPermission(Permission.AddResourceToDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*145.131*/ {_display_(Seq[Any](format.raw/*145.133*/("""
                        <a id="addFilesLink" href=""""),_display_(Seq[Any](/*146.53*/(routes.Datasets.addFiles(dataset.id)))),format.raw/*146.91*/(""""
                        class="btn btn-link" id="fileUploadBtn" title="Add File to Dataset" role="button">
                            <span class="glyphicon glyphicon-plus"></span> Add Files
                        </a>
                    """)))})),format.raw/*150.22*/("""
                    """),_display_(Seq[Any](/*151.22*/if(showDownload && Permission.checkPermission(Permission.DownloadFiles, ResourceRef(ResourceRef.dataset, dataset.id)))/*151.140*/ {_display_(Seq[Any](format.raw/*151.142*/("""
                        <a id='download-url' href="#"
                        onclick="window.open(jsRoutes.api.Datasets.download('"""),_display_(Seq[Any](/*153.79*/dataset/*153.86*/.id)),format.raw/*153.89*/("""').url, '_blank');"
                        class="btn btn-link" title="Download All Files as Zip" role="button">
                            <span class="glyphicon glyphicon-download-alt"></span>
                            Download All Files
                        </a>
                    """)))}/*158.23*/else/*158.28*/{_display_(Seq[Any](format.raw/*158.29*/("""
                        <a id='download-url' href="#" disabled
                        class="btn btn-link disabled" title="There are no files in this dataset or you don't have permission to Download it" role="button">
                            <span class="glyphicon glyphicon-download-alt"></span>
                            Download All Files
                        </a>
                    """)))})),format.raw/*164.22*/("""

                    """),_display_(Seq[Any](/*166.22*/if(dataset.trash)/*166.39*/{_display_(Seq[Any](format.raw/*166.40*/("""
                        """),_display_(Seq[Any](/*167.26*/datasets/*167.34*/.restoreButton(dataset))),format.raw/*167.57*/("""
                    """)))})),format.raw/*168.22*/("""

                    """),_display_(Seq[Any](/*170.22*/datasets/*170.30*/.deleteButton(dataset))),format.raw/*170.52*/("""

                    """),format.raw/*173.49*/("""
                    """),_display_(Seq[Any](/*174.22*/if(user.isDefined)/*174.40*/ {_display_(Seq[Any](format.raw/*174.42*/("""
                        """),_display_(Seq[Any](/*175.26*/datasets/*175.34*/.userLink(dataset))),format.raw/*175.52*/("""
                    """)))})),format.raw/*176.22*/("""
                    """),_display_(Seq[Any](/*177.22*/if(user.isDefined && play.api.Play.current.plugin[services.StagingAreaPlugin].isDefined)/*177.110*/ {_display_(Seq[Any](format.raw/*177.112*/("""
                        """),_display_(Seq[Any](/*178.26*/if(toPublish && currentSpace.isDefined && Permission.checkPermission(Permission.EditStagingArea, ResourceRef(ResourceRef.space, UUID(currentSpace.getOrElse("")))))/*178.189*/ {_display_(Seq[Any](format.raw/*178.191*/("""
                            <a href=""""),_display_(Seq[Any](/*179.39*/(routes.CurationObjects.newCO(dataset.id, currentSpace.get)))),format.raw/*179.99*/("""" class="btn btn-link">
                                <span class="glyphicon glyphicon-globe"></span> Publish
                            </a>
                        """)))}/*182.27*/else/*182.32*/{_display_(Seq[Any](format.raw/*182.33*/("""
                            """),_display_(Seq[Any](/*183.30*/if(toPublish && dataset.spaces.size > 0)/*183.70*/ {_display_(Seq[Any](format.raw/*183.72*/("""
                                <a href=""""),_display_(Seq[Any](/*184.43*/(routes.CurationObjects.newCO(dataset.id, "")))),format.raw/*184.89*/("""" class="btn btn-link">
                                    <span class="glyphicon glyphicon-globe"></span> Publish
                                </a>
                            """)))}/*187.31*/else/*187.36*/{_display_(Seq[Any](format.raw/*187.37*/("""
                                <button class="btn btn-link" disabled>
                                    <span class="glyphicon glyphicon-globe"></span> Publish
                                </button>
                            """)))})),format.raw/*191.30*/("""
                        """)))})),format.raw/*192.26*/("""
                    """)))})),format.raw/*193.22*/("""
                    """),_display_(Seq[Any](/*194.22*/if(Permission.checkPermission(Permission.AddResourceToDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*194.131*/ {_display_(Seq[Any](format.raw/*194.133*/("""
                        """),format.raw/*223.28*/("""
                    """)))})),format.raw/*224.22*/("""
                    <hr/>
                </div>
            </div>



            <div class="tabbable" id="bottomDatasetTabbable"> <!-- Only required for left/right tabs -->
                <ul class="nav nav-tabs margin-bottom-20" >
                    <li role="presentation" id="tab-files1"><a href="#tab-files" role="tab" data-toggle="tab">Files </a></li>
                    <li role="presentation" id="tab-metadata1"><a href="#tab-metadata" role="tab" data-toggle="tab">Metadata </a></li>
                    """),format.raw/*237.24*/("""
                    <li role="presentation"><a href="#tab-comments" role="tab" data-toggle="tab">Comments ("""),_display_(Seq[Any](/*238.109*/comments/*238.117*/.size)),format.raw/*238.122*/(""")</a></li>
                </ul>
                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane" id="tab-files"></div>
                    <div role="tabpanel" class="tab-pane" id="tab-comments">
                            <!-- If the user can add comments, the form will be present, otherwise the form is not available at all. -->
                        """),_display_(Seq[Any](/*245.26*/if(Permission.checkPermission(Permission.AddComment, ResourceRef(ResourceRef.dataset, dataset.id)))/*245.125*/ {_display_(Seq[Any](format.raw/*245.127*/("""
                            """),_display_(Seq[Any](/*246.30*/commentform(dataset.id.stringify, "dataset", dataset.name))),format.raw/*246.88*/("""
                        """)))})),format.raw/*247.26*/("""
                        <div class="list-unstyled" id="reply_"""),_display_(Seq[Any](/*248.63*/dataset/*248.70*/.id.toString)),format.raw/*248.82*/(""""> """),_display_(Seq[Any](/*248.86*/comment(comments))),format.raw/*248.103*/("""</div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tab-metadata">
                        """),_display_(Seq[Any](/*251.26*/if(user.isDefined)/*251.44*/ {_display_(Seq[Any](format.raw/*251.46*/("""
                            """),format.raw/*258.32*/("""
                        """)))})),format.raw/*259.26*/("""
                        <div class="row">
                             <div class="col-md-12" id="metadata-content">

                            <a  href="#" class="btn btn-success btn-large" data-toggle="modal" data-target="#metadataModal" id="editMetaData" title="Dataset metadata">
                                <span class="glyphicon glyphicon-plus"></span>Edit Metadata Template
                            </a>
                            <hr />
                            <div class="templateDataReadOnly">
                           </div>
                           <hr />

                        </div>
                         <div class="col-md-12" id="metadata-content">
                        """),_display_(Seq[Any](/*273.26*/metadatald/*273.36*/.view(m, true))),format.raw/*273.50*/("""
                        </div>
                        </div>
                        """),_display_(Seq[Any](/*276.26*/if(Permission.checkPermission(Permission.EditDataset, ResourceRef(ResourceRef.dataset, dataset.id))
                                && play.api.Play.current.plugin[services.RabbitmqPlugin].isDefined)/*277.100*/ {_display_(Seq[Any](format.raw/*277.102*/("""
                            <div class="row bottom-padding hideTree">
                                <div class="col-md-12">
                                    <h4>Extractions</h4>
                                    <a href=""""),_display_(Seq[Any](/*281.47*/routes/*281.53*/.Extractors.submitDatasetExtraction(dataset.id))),format.raw/*281.100*/("""" class="btn btn-link" >
                                        <span class="glyphicon glyphicon glyphicon-send" aria-hidden="true"></span> Submit dataset for extraction
                                    </a>
                                </div>
                            </div>
                        """)))})),format.raw/*286.26*/("""
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab-visua">
                    """),_display_(Seq[Any](/*289.22*/datasets/*289.30*/.previews(dataset, previewers))),format.raw/*289.60*/("""
                    </div>
                </div>
            </div>
        </div>

            <!-- right column -->
        <div class="col-md-4 hideTree">
            """),_display_(Seq[Any](/*297.14*/spaces/*297.20*/.spaceAllocation(dataset.id, ResourceRef.dataset,datasetSpaces_canRemove.getOrElse(Map.empty),None))),format.raw/*297.119*/("""
            """),_display_(Seq[Any](/*298.14*/datasets/*298.22*/.collections(dataset, collectionsInside, true))),format.raw/*298.68*/("""
            """),_display_(Seq[Any](/*299.14*/datasets/*299.22*/.tags(dataset, fileList, filesTags))),format.raw/*299.57*/("""
            """),_display_(Seq[Any](/*300.14*/if(play.api.Play.current.plugin[services.StagingAreaPlugin].isDefined)/*300.84*/ {_display_(Seq[Any](format.raw/*300.86*/("""
                """),_display_(Seq[Any](/*301.18*/datasets/*301.26*/.listCurationObjects(curationObjects))),format.raw/*301.63*/("""
            """)))})),format.raw/*302.14*/("""
            """),_display_(Seq[Any](/*303.14*/if(current.plugin[services.PostgresPlugin].isDefined)/*303.67*/ {_display_(Seq[Any](format.raw/*303.69*/("""
                """),_display_(Seq[Any](/*304.18*/sensors/*304.25*/.relatedSensors(relatedSensors, "Dataset", dataset.id.stringify))),format.raw/*304.89*/("""
            """)))})),format.raw/*305.14*/("""
            <div id="toolListContainer"></div>
        </div>
    </div>

    """),format.raw/*310.83*/("""
    """),format.raw/*311.101*/("""

    <script src=""""),_display_(Seq[Any](/*313.19*/routes/*313.25*/.Assets.at("javascripts/errorRedirect.js"))),format.raw/*313.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*314.19*/routes/*314.25*/.Assets.at("javascripts/updateLicenseInfo.js"))),format.raw/*314.71*/("""" language="javascript"></script>
    <script src=""""),_display_(Seq[Any](/*315.19*/routes/*315.25*/.Assets.at("javascripts/datasetListProcess.js"))),format.raw/*315.72*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*316.19*/routes/*316.25*/.Assets.at("javascripts/folderListProcess.js"))),format.raw/*316.71*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*317.19*/routes/*317.25*/.Assets.at("javascripts/fileListProcess.js"))),format.raw/*317.69*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*318.19*/routes/*318.25*/.Assets.at("javascripts/follow-button.js"))),format.raw/*318.67*/("""" type="text/javascript"></script>
    <script src=""""),_display_(Seq[Any](/*319.19*/routes/*319.25*/.Assets.at("javascripts/deleteUtils.js"))),format.raw/*319.65*/("""" type="text/javascript"></script>
    <script type="text/javascript">
            var removeIndicator = false;
            var parentId = """"),_display_(Seq[Any](/*322.30*/dataset/*322.37*/.id)),format.raw/*322.40*/("""";
            var parentType = "dataset";
            var pageIndex = 0;
            var folderId;
            var cur_title;

            function updateTitle() """),format.raw/*328.36*/("""{"""),format.raw/*328.37*/("""
                cur_title = $('#datasettitle').text().trim();
                $('<span class="inline edit_title_div"> </span>').insertAfter($('#datasettitle'));
                $('.edit_title_div').append('<input type="text" id = "title_input" class="form-control" required/>');
                $('.edit_title_div').append('<div class="hiddencomplete" aria-hidden="true" id="title-error"> <span class="error"> Title is required </span> </div>');
                $('.edit_title_div').append('<button id="update_title" class= "btn btn-sm btn-success btn-margins" onclick="saveTitle()"> <span class="glyphicon glyphicon-send"></span> Save </button>');
                $('.edit_title_div').append('<button id="cancel_title" onclick="cancelTitle()" class="btn btn-sm edit-tab btn-danger btn-margins"> <span class="glyphicon glyphicon-remove"></span>Cancel </button>');
                $('#title_input').val(cur_title);
                $('#datasettitle').text("");
                $('#ds-title').removeClass("dataset-title");
                $('#h-edit-title').addClass("hiddencomplete");
                $('#h-edit-title').css("display", "none");
            """),format.raw/*340.13*/("""}"""),format.raw/*340.14*/("""
            function parseHash() """),format.raw/*341.34*/("""{"""),format.raw/*341.35*/("""
                var hash = location.hash.split('#')[1];
                var folderSet = false;
                var pageSet = false;
                if(hash != undefined) """),format.raw/*345.39*/("""{"""),format.raw/*345.40*/("""
                    var values = hash.split('&');
                    for (var i = 0; i < values.length; i++)"""),format.raw/*347.60*/("""{"""),format.raw/*347.61*/("""
                        var temp = values[i].split('=');
                        if(temp[0] == 'folderId') """),format.raw/*349.51*/("""{"""),format.raw/*349.52*/("""
                            folderId = temp[1];
                            folderSet = true
                        """),format.raw/*352.25*/("""}"""),format.raw/*352.26*/("""
                        if(temp[0] == 'page')"""),format.raw/*353.46*/("""{"""),format.raw/*353.47*/("""
                            pageIndex = temp[1];
                            pageSet = true
                        """),format.raw/*356.25*/("""}"""),format.raw/*356.26*/("""
                    """),format.raw/*357.21*/("""}"""),format.raw/*357.22*/("""
                """),format.raw/*358.17*/("""}"""),format.raw/*358.18*/("""
                if(!folderSet) """),format.raw/*359.32*/("""{"""),format.raw/*359.33*/("""
                    folderId ='';
                """),format.raw/*361.17*/("""}"""),format.raw/*361.18*/("""
                if(!pageSet) """),format.raw/*362.30*/("""{"""),format.raw/*362.31*/("""
                    pageIndex = 0;
                """),format.raw/*364.17*/("""}"""),format.raw/*364.18*/("""
            """),format.raw/*365.13*/("""}"""),format.raw/*365.14*/("""
            $(document).ready(function()"""),format.raw/*366.41*/("""{"""),format.raw/*366.42*/("""


                get4ceedMetaDataReadOnly();

                $(document).on('click', '#addNewTemplate', function(e)"""),format.raw/*371.71*/("""{"""),format.raw/*371.72*/("""


                    // console.log('adding a new template');
                    var request = jsRoutes.api.Datasets.createEmptyVocabularyForDataset(""""),_display_(Seq[Any](/*375.91*/dataset/*375.98*/.id)),format.raw/*375.101*/("""").ajax("""),format.raw/*375.109*/("""{"""),format.raw/*375.110*/("""
                        type: "POST"
                    """),format.raw/*377.21*/("""}"""),format.raw/*377.22*/(""");
                    request.done(function(response, textStatus, jqXHR)"""),format.raw/*378.71*/("""{"""),format.raw/*378.72*/("""
                        // console.log('done')
                        location.reload();
                    """),format.raw/*381.21*/("""}"""),format.raw/*381.22*/(""");

                    request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*383.75*/("""{"""),format.raw/*383.76*/("""
                        console.error("The following error occurred: " + textStatus, errorThrown);
                        var errMsg = "Yoy must be logged in to update the information about a dataset.";
                        if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*386.67*/("""{"""),format.raw/*386.68*/("""
                            notify("The dataset did not have template attached: "+ errorThrown, "error");
                        """),format.raw/*388.25*/("""}"""),format.raw/*388.26*/("""
                    """),format.raw/*389.21*/("""}"""),format.raw/*389.22*/(""");

                """),format.raw/*391.17*/("""}"""),format.raw/*391.18*/(""");

                getUpdatedFilesAndFolders();

                if("""),_display_(Seq[Any](/*395.21*/dataset/*395.28*/.spaces.size)),format.raw/*395.40*/(""" < 1 ) """),format.raw/*395.47*/("""{"""),format.raw/*395.48*/("""
                    $('#dataset-users').hide();
                """),format.raw/*397.17*/("""}"""),format.raw/*397.18*/("""

                if("""),_display_(Seq[Any](/*399.21*/Permission/*399.31*/.checkPermission(Permission.EditDataset, ResourceRef(ResourceRef.dataset, dataset.id)))),format.raw/*399.117*/(""") """),format.raw/*399.119*/("""{"""),format.raw/*399.120*/("""
                    $(document).on('mouseenter', '#ds-title', function() """),format.raw/*400.74*/("""{"""),format.raw/*400.75*/("""
                        $('#h-edit-title').removeClass("hiddencomplete");
                        $('#h-edit-title').addClass("inline");
                    """),format.raw/*403.21*/("""}"""),format.raw/*403.22*/(""").on('mouseleave', '#ds-title', function() """),format.raw/*403.65*/("""{"""),format.raw/*403.66*/("""
                        $('#h-edit-title').removeClass("inline");
                        $('#h-edit-title').addClass("hiddencomplete");
                    """),format.raw/*406.21*/("""}"""),format.raw/*406.22*/(""");

                    $(document).on('mouseenter', '#aboutdesc', function() """),format.raw/*408.75*/("""{"""),format.raw/*408.76*/("""
                        $('#edit-description').removeClass("hiddencomplete");
                        $('#edit-description').addClass("inline");
                    """),format.raw/*411.21*/("""}"""),format.raw/*411.22*/(""").on('mouseleave', '#aboutdesc', function() """),format.raw/*411.66*/("""{"""),format.raw/*411.67*/("""
                        $('#edit-description').removeClass("inline");
                        $('#edit-description').addClass("hiddencomplete");
                    """),format.raw/*414.21*/("""}"""),format.raw/*414.22*/(""");
                """),format.raw/*415.17*/("""}"""),format.raw/*415.18*/("""

                if("""),_display_(Seq[Any](/*417.21*/Permission/*417.31*/.checkPermission(Permission.EditLicense, ResourceRef(ResourceRef.dataset, dataset.id)))),format.raw/*417.117*/(""") """),format.raw/*417.119*/("""{"""),format.raw/*417.120*/("""
                    $(document).on('mouseenter', '#edit-license', function() """),format.raw/*418.78*/("""{"""),format.raw/*418.79*/("""
                        $('#editlicense').removeClass("hiddencomplete");
                        $('#editlicense').addClass("inline");
                    """),format.raw/*421.21*/("""}"""),format.raw/*421.22*/(""").on('mouseleave', '#edit-license', function() """),format.raw/*421.69*/("""{"""),format.raw/*421.70*/("""
                        $('#editlicense').removeClass("inline");
                        $('#editlicense').addClass("hiddencomplete");
                    """),format.raw/*424.21*/("""}"""),format.raw/*424.22*/(""");
                """),format.raw/*425.17*/("""}"""),format.raw/*425.18*/("""

                var description = $('#ds_description').html().trim();
                $('#ds_description').html(urlify(description));

                """),_display_(Seq[Any](/*430.18*/play/*430.22*/.api.Play.current.plugin[services.ToolManagerPlugin]/*430.74*/ match/*430.80*/ {/*431.21*/case Some(p) =>/*431.36*/ {_display_(Seq[Any](format.raw/*431.38*/("""
                    """),_display_(Seq[Any](/*432.22*/if(p.enabled && dataset.files.nonEmpty && Permission.checkPermission(Permission.ExecuteOnDataset, ResourceRef(ResourceRef.dataset, dataset.id)))/*432.166*/ {_display_(Seq[Any](format.raw/*432.168*/("""
                    refreshToolSidebar()
                    """)))})),format.raw/*434.22*/("""
                    """)))}/*436.21*/case None =>/*436.33*/ {_display_(Seq[Any](format.raw/*436.35*/(""" """)))}})),format.raw/*438.18*/("""

                $("input[value='"""),_display_(Seq[Any](/*440.34*/dataset/*440.41*/.status)),format.raw/*440.48*/("""']").attr("checked", true);
                setOriginAccess();
            """),format.raw/*442.13*/("""}"""),format.raw/*442.14*/(""");

            function cancelTitle() """),format.raw/*444.36*/("""{"""),format.raw/*444.37*/("""
                $('#datasettitle').text( cur_title);
                $('#datasettitle').prepend('<span class="glyphicon glyphicon-briefcase"></span> ');
                $('.edit_title_div').remove();
                $('#datasettitle').css("display", "inline");
                $('#h-edit-title').removeClass("inline");
                $('#h-edit-title').css("display", "");
                $('#ds-title').addClass("dataset-title");
                $('#ds-title').mouseleave();
            """),format.raw/*453.13*/("""}"""),format.raw/*453.14*/("""

            function saveTitle() """),format.raw/*455.34*/("""{"""),format.raw/*455.35*/("""
                if($('#title_input').val().length < 1) """),format.raw/*456.56*/("""{"""),format.raw/*456.57*/("""
                    $('#title-error').show();
                    return false;
                """),format.raw/*459.17*/("""}"""),format.raw/*459.18*/("""
                var name = $('#title_input').val();
                var encName = htmlEncode(name);
                jsonData = JSON.stringify("""),format.raw/*462.43*/("""{"""),format.raw/*462.44*/(""""name": encName"""),format.raw/*462.59*/("""}"""),format.raw/*462.60*/(""");

                var request = jsRoutes.api.Datasets.updateName(""""),_display_(Seq[Any](/*464.66*/dataset/*464.73*/.id)),format.raw/*464.76*/("""").ajax("""),format.raw/*464.84*/("""{"""),format.raw/*464.85*/("""
                    data: jsonData,
                    type: 'PUT',
                    contentType: "application/json"
                """),format.raw/*468.17*/("""}"""),format.raw/*468.18*/(""");

                request.done(function(response, textStatus, jqXHR)"""),format.raw/*470.67*/("""{"""),format.raw/*470.68*/("""
                    $('#datasettitle').text(encName.replace(/\n/g, "<br>"));
                    $('#datasettitle').prepend('<span class="glyphicon glyphicon-briefcase"></span> ');
                    $('.edit_title_div').remove();
                    $('#datasettitle').css("display", "inline");
                    $('#h-edit-title').removeClass("inline");
                    $('#h-edit-title').css("display", "");
                    $('#ds-title').addClass("dataset-title");
                    $('#ds-title').mouseleave();
                    notify("Dataset name updated successfully", "success", false, 2000 );
                """),format.raw/*480.17*/("""}"""),format.raw/*480.18*/(""");

                request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*482.71*/("""{"""),format.raw/*482.72*/("""
                    console.error("The following error occurred: " + textStatus, errorThrown);
                    var errMsg = "Yoy must be logged in to update the information about a dataset.";
                    if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*485.63*/("""{"""),format.raw/*485.64*/("""
                        notify("The dataset information was not updated due to: "+ errorThrown, "error");
                    """),format.raw/*487.21*/("""}"""),format.raw/*487.22*/("""
                """),format.raw/*488.17*/("""}"""),format.raw/*488.18*/(""");

            """),format.raw/*490.13*/("""}"""),format.raw/*490.14*/("""
            var cur_description;
            function updateDescription() """),format.raw/*492.42*/("""{"""),format.raw/*492.43*/("""
                cur_description = $('#ds_description').html().trim();
                $('<div class="edit_desc"> </div>').insertAfter($('#ds_description'));
                $('.edit_desc').append('<textarea id = "desc_input" class="form-control"/>');
                $('.edit_desc').append('<button id = "update_desc" class= "btn btn-sm btn-success btn-margins" onclick = "saveDescription()"> <span class="glyphicon glyphicon-send"> </span> Save </button>');
                $('.edit_desc').append('<button id="cancel_desc" class="btn btn-sm edit-tab btn-danger btn-margins" onclick="cancelDescription()"> <span class="glyphicon glyphicon-remove"></span> Cancel </button>');
                if(cur_description.indexOf("Add a description") != 0) """),format.raw/*498.71*/("""{"""),format.raw/*498.72*/("""
                    $('#desc_input').val(htmlDecode(cur_description.replace(new RegExp("<br>", "g"), "\n")));
                """),format.raw/*500.17*/("""}"""),format.raw/*500.18*/("""
                $('#ds_description').text("");
                $('#edit-description').css("display", "none");
                $('#edit-description').addClass("hiddencomplete");
            """),format.raw/*504.13*/("""}"""),format.raw/*504.14*/("""

            function cancelDescription() """),format.raw/*506.42*/("""{"""),format.raw/*506.43*/("""
                $('#ds_description').html(cur_description);
                $('.edit_desc').remove();
                $('#ds_description').css("display", "inline");
                $('#edit-description').removeClass("inline");
                $('#edit-description').css("display", "");
                $('#ds-description').mouseleave();
            """),format.raw/*513.13*/("""}"""),format.raw/*513.14*/("""

            function saveDescription() """),format.raw/*515.40*/("""{"""),format.raw/*515.41*/("""
                var description = $('#desc_input').val();
                var encDescription = htmlEncode(description);
                jsonData = JSON.stringify("""),format.raw/*518.43*/("""{"""),format.raw/*518.44*/(""""description": encDescription"""),format.raw/*518.73*/("""}"""),format.raw/*518.74*/(""");

                var request = jsRoutes.api.Datasets.updateDescription(""""),_display_(Seq[Any](/*520.73*/dataset/*520.80*/.id)),format.raw/*520.83*/("""").ajax("""),format.raw/*520.91*/("""{"""),format.raw/*520.92*/("""
                    data: jsonData,
                    type: 'PUT',
                    contentType: "application/json"
                """),format.raw/*524.17*/("""}"""),format.raw/*524.18*/(""");

                request.done(function(response, textStatus,jqXHR) """),format.raw/*526.67*/("""{"""),format.raw/*526.68*/("""
                    $('#ds_description').html(urlify(htmlEncode(description).replace(/\n/g, "<br>")));
                    $('.edit_desc').remove();
                    $('#ds_description').css("display", "inline");
                    $('#edit-description').removeClass("inline");
                    $('#edit-description').css("display", "");
                    $('#ds-description').mouseleave();
                    notify("Dataset description updated successfully", "success", false, 2000 );
                """),format.raw/*534.17*/("""}"""),format.raw/*534.18*/(""");

                request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*536.71*/("""{"""),format.raw/*536.72*/("""
                    console.error("The following error occurred: " + textStatus, errorThrown);
                    var errMsg = " You must be logged in to update the information about a dataset.";
                    if(!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*539.63*/("""{"""),format.raw/*539.64*/("""
                        notify("The dataset information was not updated due to: " + errorThrown, "error");
                    """),format.raw/*541.21*/("""}"""),format.raw/*541.22*/("""
                """),format.raw/*542.17*/("""}"""),format.raw/*542.18*/(""");

            """),format.raw/*544.13*/("""}"""),format.raw/*544.14*/("""

            function urlify(text) """),format.raw/*546.35*/("""{"""),format.raw/*546.36*/("""
                var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
                return text.replace(urlRegex, function(url) """),format.raw/*548.61*/("""{"""),format.raw/*548.62*/("""
                    return '<a href="' + url + '">' + url + '</a>';
                """),format.raw/*550.17*/("""}"""),format.raw/*550.18*/(""")
            """),format.raw/*551.13*/("""}"""),format.raw/*551.14*/("""

            function refreshToolSidebar() """),format.raw/*553.43*/("""{"""),format.raw/*553.44*/("""
                // Get current status from ToolManagerPlugin and refresh sidebar table
                var request = new XMLHttpRequest();
                request.onreadystatechange = function() """),format.raw/*556.57*/("""{"""),format.raw/*556.58*/("""
                    if (request.readyState == 4 && request.status == 200) """),format.raw/*557.75*/("""{"""),format.raw/*557.76*/("""
                        $('#toolListContainer').html(request.response);
                    """),format.raw/*559.21*/("""}"""),format.raw/*559.22*/("""
                """),format.raw/*560.17*/("""}"""),format.raw/*560.18*/("""

                request.open("GET", jsRoutes.controllers.ToolManager.refreshToolSidebar(""""),_display_(Seq[Any](/*562.91*/dataset/*562.98*/.id)),format.raw/*562.101*/("""", """"),_display_(Seq[Any](/*562.106*/dataset/*562.113*/.name)),format.raw/*562.118*/("""").url, true);
                request.send();
            """),format.raw/*564.13*/("""}"""),format.raw/*564.14*/("""

            $(function() """),format.raw/*566.26*/("""{"""),format.raw/*566.27*/("""
                $(window).on('fileDelete hashchange', function() """),format.raw/*567.66*/("""{"""),format.raw/*567.67*/("""

                    getUpdatedFilesAndFolders();
                """),format.raw/*570.17*/("""}"""),format.raw/*570.18*/(""");

                $('#createFolder').click(function() """),format.raw/*572.53*/("""{"""),format.raw/*572.54*/("""
                    var name=$('#name');

                    var error = false;
                    if(!name.val()) """),format.raw/*576.37*/("""{"""),format.raw/*576.38*/("""
                        $('#nameerror').show();
                        error = true;
                    """),format.raw/*579.21*/("""}"""),format.raw/*579.22*/(""" else"""),format.raw/*579.27*/("""{"""),format.raw/*579.28*/("""
                        $('#nameerror').hide();
                    """),format.raw/*581.21*/("""}"""),format.raw/*581.22*/("""
                    if(!error) """),format.raw/*582.32*/("""{"""),format.raw/*582.33*/("""

                        var encName  = htmlEncode(name.val());
                        var jsonData = JSON.stringify("""),format.raw/*585.55*/("""{"""),format.raw/*585.56*/(""""name": encName, "parentId": parentId, "parentType": parentType, "currentSpace": """"),_display_(Seq[Any](/*585.139*/currentSpace)),format.raw/*585.151*/("""""""),format.raw/*585.152*/("""}"""),format.raw/*585.153*/(""");
                        var request = jsRoutes.controllers.Folders.createFolder(""""),_display_(Seq[Any](/*586.83*/dataset/*586.90*/.id)),format.raw/*586.93*/("""").ajax("""),format.raw/*586.101*/("""{"""),format.raw/*586.102*/("""
                            data:jsonData,
                            type: 'POST',
                            contentType: "application/json"
                        """),format.raw/*590.25*/("""}"""),format.raw/*590.26*/(""");
                        request.done(function(response, textStatus, jqXHR)"""),format.raw/*591.75*/("""{"""),format.raw/*591.76*/("""
                            $('#folder-modal').modal('hide');
                            $('#folderListDiv').prepend(response);
                            notify("Folder created successfully", "success", false, 2000 );
                            $('#name').val("");
                        """),format.raw/*596.25*/("""}"""),format.raw/*596.26*/(""");

                        request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*598.79*/("""{"""),format.raw/*598.80*/("""
                            $('#name').val("");
                            console.error("The following error occured: " + textStatus, errorThrown);
                            var errMsg = "You must be logged in to create a new folder.";
                            if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*602.72*/("""{"""),format.raw/*602.73*/("""
                                notify("Error in creating folder: " + errorThrown, "error");
                            """),format.raw/*604.29*/("""}"""),format.raw/*604.30*/("""
                        """),format.raw/*605.25*/("""}"""),format.raw/*605.26*/(""");

                    """),format.raw/*607.21*/("""}"""),format.raw/*607.22*/(""";
                    return false;
                """),format.raw/*609.17*/("""}"""),format.raw/*609.18*/(""");
            """),format.raw/*610.13*/("""}"""),format.raw/*610.14*/(""");

            function updatePageAndFolder(idx, folderIdd)"""),format.raw/*612.57*/("""{"""),format.raw/*612.58*/("""
                if(folderIdd != "") """),format.raw/*613.37*/("""{"""),format.raw/*613.38*/("""
                    location.hash = "folderId="+folderIdd+"&page="+idx;
                """),format.raw/*615.17*/("""}"""),format.raw/*615.18*/(""" else """),format.raw/*615.24*/("""{"""),format.raw/*615.25*/("""
                    location.hash="page="+idx;
                """),format.raw/*617.17*/("""}"""),format.raw/*617.18*/("""
                $(window).trigger("hashchange");
            """),format.raw/*619.13*/("""}"""),format.raw/*619.14*/("""

            function getUpdatedFilesAndFolders() """),format.raw/*621.50*/("""{"""),format.raw/*621.51*/("""
                parseHash();
                if(folderId == "") """),format.raw/*623.36*/("""{"""),format.raw/*623.37*/("""
                    folderId = null;
                """),format.raw/*625.17*/("""}"""),format.raw/*625.18*/("""
                var currentSpace = null;
                if("""),_display_(Seq[Any](/*627.21*/currentSpace/*627.33*/.isDefined)),format.raw/*627.43*/(""")"""),format.raw/*627.44*/("""{"""),format.raw/*627.45*/("""
                    currentSpace = """"),_display_(Seq[Any](/*628.38*/currentSpace)),format.raw/*628.50*/("""";
                """),format.raw/*629.17*/("""}"""),format.raw/*629.18*/("""
                var jsonData = JSON.stringify("""),format.raw/*630.47*/("""{"""),format.raw/*630.48*/(""""currentSpace": currentSpace, "folderId": folderId"""),format.raw/*630.98*/("""}"""),format.raw/*630.99*/(""");
                var request = jsRoutes.controllers.Datasets.getUpdatedFilesAndFolders(""""),_display_(Seq[Any](/*631.89*/dataset/*631.96*/.id)),format.raw/*631.99*/("""", """),_display_(Seq[Any](/*631.103*/limit)),format.raw/*631.108*/(""", pageIndex, """"),_display_(Seq[Any](/*631.123*/currentSpace)),format.raw/*631.135*/("""").ajax("""),format.raw/*631.143*/("""{"""),format.raw/*631.144*/("""
                    data: jsonData,
                    type: 'POST',
                    contentType: "application/json"
                """),format.raw/*635.17*/("""}"""),format.raw/*635.18*/(""");

                request.done(function(response, textStatus, jsXHR)"""),format.raw/*637.67*/("""{"""),format.raw/*637.68*/("""
                    $('#tab-files').html("");
                    $('#tab-files').html(response);
                    if(folderId != null) """),format.raw/*640.42*/("""{"""),format.raw/*640.43*/("""
                        parentType = 'folder';
                        parentId = folderId;
                        $('#addFilesLink').attr("href", jsRoutes.controllers.Folders.addFiles('"""),_display_(Seq[Any](/*643.97*/dataset/*643.104*/.id)),format.raw/*643.107*/("""', folderId).url);
                    """),format.raw/*644.21*/("""}"""),format.raw/*644.22*/(""" else """),format.raw/*644.28*/("""{"""),format.raw/*644.29*/("""
                        parentId = """"),_display_(Seq[Any](/*645.38*/dataset/*645.45*/.id)),format.raw/*645.48*/("""";
                        parentType = "dataset";
                        $('#addFilesLink').attr("href", jsRoutes.controllers.Datasets.addFiles('"""),_display_(Seq[Any](/*647.98*/dataset/*647.105*/.id)),format.raw/*647.108*/("""').url);
                    """),format.raw/*648.21*/("""}"""),format.raw/*648.22*/("""
                """),format.raw/*649.17*/("""}"""),format.raw/*649.18*/(""");
                request.fail(function (jqXHR, textStatus, errorThrown)"""),format.raw/*650.71*/("""{"""),format.raw/*650.72*/("""
                    console.error("The following error occurred: " + textStatus, errorThrown);
                    var errMsg = "You must be logged in to see files and folders.";
                    if (!checkErrorAndRedirect(jqXHR, errMsg)) """),format.raw/*653.64*/("""{"""),format.raw/*653.65*/("""
                        notify("Error in getting more files and folders : " + errorThrown, "error");
                    """),format.raw/*655.21*/("""}"""),format.raw/*655.22*/("""
                """),format.raw/*656.17*/("""}"""),format.raw/*656.18*/(""");

            """),format.raw/*658.13*/("""}"""),format.raw/*658.14*/("""

            $('input[type=radio][name="access"]').change(function() """),format.raw/*660.69*/("""{"""),format.raw/*660.70*/("""
                var name = '"""),_display_(Seq[Any](/*661.30*/dataset/*661.37*/.name)),format.raw/*661.42*/("""';
                var id = '"""),_display_(Seq[Any](/*662.28*/dataset/*662.35*/.id)),format.raw/*662.38*/("""';
                var access = $("input[name='access']:checked").val();
                if(access !== undefined)"""),format.raw/*664.41*/("""{"""),format.raw/*664.42*/("""
                    var msg = "Are you sure you want to set " + name + " to " +access+"?";
                    var modalHTML = confirmTemplate(msg, id, access);
                    var confirmModal = $(modalHTML);
                    confirmModal.modal();
                    confirmModal.modal("show");
                """),format.raw/*670.17*/("""}"""),format.raw/*670.18*/("""
            """),format.raw/*671.13*/("""}"""),format.raw/*671.14*/(""");

            function moveFile(fileId) """),format.raw/*673.39*/("""{"""),format.raw/*673.40*/("""
                $('.modal').modal('hide');
                var newFolderId = $('#move_file_select').val();
                var datasetId = """"),_display_(Seq[Any](/*676.35*/dataset/*676.42*/.id)),format.raw/*676.45*/("""";
                var jsonData = JSON.stringify("""),format.raw/*677.47*/("""{"""),format.raw/*677.48*/(""""folderId": folderId"""),format.raw/*677.68*/("""}"""),format.raw/*677.69*/(""");
                var url;
                if(newFolderId == "root") """),format.raw/*679.43*/("""{"""),format.raw/*679.44*/("""
                    url =jsRoutes.api.Folders.moveFileToDataset(datasetId, folderId, fileId)
                """),format.raw/*681.17*/("""}"""),format.raw/*681.18*/(""" else """),format.raw/*681.24*/("""{"""),format.raw/*681.25*/("""
                    url =jsRoutes.api.Folders.moveFileBetweenFolders(datasetId, newFolderId, fileId)
                """),format.raw/*683.17*/("""}"""),format.raw/*683.18*/("""
                var request = url.ajax("""),format.raw/*684.40*/("""{"""),format.raw/*684.41*/("""
                    data: jsonData,
                    type: 'POST',
                    contentType: "application/json"
                """),format.raw/*688.17*/("""}"""),format.raw/*688.18*/(""");
                request.done(function(response, textStatus, jsXHR)"""),format.raw/*689.67*/("""{"""),format.raw/*689.68*/("""
                    if(newFolderId == "root") """),format.raw/*690.47*/("""{"""),format.raw/*690.48*/("""
                        updatePageAndFolder(0, '');
                        notify("The file " + response.fileName + " has been succesfully moved to the dataset.", "success",  false, 2000);
                    """),format.raw/*693.21*/("""}"""),format.raw/*693.22*/(""" else """),format.raw/*693.28*/("""{"""),format.raw/*693.29*/("""
                        updatePageAndFolder(0, newFolderId);
                        notify("The file " + response.fileName + " has been succesfully moved to folder: " + response.folderName, "success",  false, 2000);
                    """),format.raw/*696.21*/("""}"""),format.raw/*696.22*/("""

                """),format.raw/*698.17*/("""}"""),format.raw/*698.18*/(""");
                request.fail(function(jqXHR, textStatus, errorThrown) """),format.raw/*699.71*/("""{"""),format.raw/*699.72*/("""
                    notify("Error moving file " + errorThrown, "error")
                """),format.raw/*701.17*/("""}"""),format.raw/*701.18*/(""");

            """),format.raw/*703.13*/("""}"""),format.raw/*703.14*/(""";

            function showMoveModal(fileId) """),format.raw/*705.44*/("""{"""),format.raw/*705.45*/("""

                var request = jsRoutes.api.Folders.getAllFoldersByDatasetId(""""),_display_(Seq[Any](/*707.79*/dataset/*707.86*/.id)),format.raw/*707.89*/("""").ajax("""),format.raw/*707.97*/("""{"""),format.raw/*707.98*/("""
                    type: "GET",
                    contentType: "application/json"
                """),format.raw/*710.17*/("""}"""),format.raw/*710.18*/(""");
                request.done(function(response, textStatus, jsXHR)"""),format.raw/*711.67*/("""{"""),format.raw/*711.68*/("""
                    $('#move_file_select').remove();
                    var modalHtml = '<div class="modal fade" id="moveFileModal" tabindex="-1" role="dialog">';
                    modalHtml += '<div class="modal-dialog">';
                    modalHtml += '<div class="modal-content">';
                    modalHtml += '<div class="modal-header">';
                    modalHtml += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                    modalHtml += '<h4 class="modal-title">Move File to Folder</h4>';
                    modalHtml += '</div>';
                    modalHtml += '<div class="modal-body">';
                    modalHtml += '<select id="move_file_select" class="form-control">';
                    if(folderId != null) """),format.raw/*722.42*/("""{"""),format.raw/*722.43*/("""
                        modalHtml += '<option value="root">/</option>';
                    """),format.raw/*724.21*/("""}"""),format.raw/*724.22*/("""
                    for (var i = 0; i < response.length; i ++) """),format.raw/*725.64*/("""{"""),format.raw/*725.65*/("""
                        if(folderId != response[i].id) """),format.raw/*726.56*/("""{"""),format.raw/*726.57*/("""
                            modalHtml +='<option value="'+response[i].id+'">'+ response[i].name + '</option>';
                        """),format.raw/*728.25*/("""}"""),format.raw/*728.26*/("""
                    """),format.raw/*729.21*/("""}"""),format.raw/*729.22*/("""

                    modalHtml += '</select>';
                    modalHtml += '</div>';
                    modalHtml += '<div class="modal-footer">';
                    modalHtml += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
                    modalHtml += '<a type="button" class="btn btn-success" id="moveFileButton" href="javascript:moveFile(\''+fileId+'\')"> <span class="glyphicon glyphicon-move"></span> Move</a>';
                    modalHtml += '</div>';
                    modalHtml += '</div>';
                    modalHtml += '</div>';
                    modalHtml += '</div>';
                    var moveBetweenFoldersModal = $(modalHtml);
                    moveBetweenFoldersModal.modal("show");
                """),format.raw/*742.17*/("""}"""),format.raw/*742.18*/(""");

                request.fail(function(jqXHR, textStatus, errorThrown)"""),format.raw/*744.70*/("""{"""),format.raw/*744.71*/("""
                    notify("Error getting all folders in dataset" + errorThrown, "error");
                """),format.raw/*746.17*/("""}"""),format.raw/*746.18*/(""");

            """),format.raw/*748.13*/("""}"""),format.raw/*748.14*/("""

            //Should we show a template for this dataset?
            function get4ceedMetaDataReadOnly() """),format.raw/*751.49*/("""{"""),format.raw/*751.50*/("""
                var url = window.location.pathname;
                var id = url.substring(url.lastIndexOf('/') + 1);
                $.ajax("""),format.raw/*754.24*/("""{"""),format.raw/*754.25*/("""
                    url: jsRoutes.api.T2C2.getDatasetWithAttachedVocab(id).url,
                    type:"GET",
                    dataType: "json",
                    beforeSend: function(xhr)"""),format.raw/*758.46*/("""{"""),format.raw/*758.47*/("""
                        xhr.setRequestHeader("Content-Type", "application/json");
                        xhr.setRequestHeader("Accept", "application/json");
                    """),format.raw/*761.21*/("""}"""),format.raw/*761.22*/(""",
                    success: function(data)"""),format.raw/*762.44*/("""{"""),format.raw/*762.45*/("""
                        if (data.template.id === "none")"""),format.raw/*763.57*/("""{"""),format.raw/*763.58*/("""
                            // console.log(data.template.id);
                        """),format.raw/*765.25*/("""}"""),format.raw/*765.26*/("""else"""),format.raw/*765.30*/("""{"""),format.raw/*765.31*/("""

                            createBoxesForPreviousDatasetReadOnly(data);
                        """),format.raw/*768.25*/("""}"""),format.raw/*768.26*/("""
                    """),format.raw/*769.21*/("""}"""),format.raw/*769.22*/("""
                """),format.raw/*770.17*/("""}"""),format.raw/*770.18*/(""")

            """),format.raw/*772.13*/("""}"""),format.raw/*772.14*/("""

            function createBoxesForPreviousDatasetReadOnly(data)"""),format.raw/*774.65*/("""{"""),format.raw/*774.66*/("""

                $.each(data.template.terms, function(i, val) """),format.raw/*776.62*/("""{"""),format.raw/*776.63*/("""
                    // console.log(val.key);
                    var div3 = $("<div />");
                    div3.html(createDivReadOnly(val.key, val.default_value, val.units));
                    $(".templateDataReadOnly").append(div3);

                """),format.raw/*782.17*/("""}"""),format.raw/*782.18*/(""");
            """),format.raw/*783.13*/("""}"""),format.raw/*783.14*/("""   
            
        //Create dynamic textbox
        function createDivReadOnly(keyName, val, units) """),format.raw/*786.57*/("""{"""),format.raw/*786.58*/("""
            //console.log(keyName, val, units);
            var valKeyName = $.trim(keyName);
            var valStr = $.trim(val);
            var valUnits = $.trim(units);

            //format text
            var txtToWrite = "Value";

            var i = counter1.add();
            return '<div class="row top-buffer"><div class="col-xs-7"><b>' + "<label for='name'>Key: " + '</b></label>' +
                    '<input width="100%" readonly class="form-control" type="text" value=' + valKeyName.replace(/ /g,"&nbsp;") +'></div>' +

                    '<div class="col-xs-2" style="margin-left:-15px;"><b>' + "<label for='val'>"+ txtToWrite + '</b></label>' +
                    '<input readonly class="form-control" type="text" value=' + valStr.replace(/ /g,"&nbsp;") +'></div>' +

                    '<div class="col-xs-2"><b>' + "<label for='name'>Units: " + '</label></b>' +
                    '<input readonly class="form-control" type="text" value=' + valUnits.replace(/ /g,"&nbsp;") +'></div>' +

                    '<div class="col-xs-1" style="margin-left:-15px;"><b>' + "<label for='val'>&nbsp;" + '</label></b></div>'

        """),format.raw/*807.9*/("""}"""),format.raw/*807.10*/("""

    </script>
""")))})),format.raw/*810.2*/("""

"""))}
    }
    
    def render(dataset:models.Dataset,comments:List[Comment],previewers:List[Previewer],m:List[models.Metadata],collectionsInside:List[models.Collection],rdfExported:Boolean,relatedSensors:List[scala.Tuple3[String, String, String]],datasetSpaces_canRemove:Option[Map[ProjectSpace, Boolean]],fileList:List[File],filesTags:scala.collection.immutable.TreeSet[String],toPublish:Boolean,curationObjects:List[models.CurationObject],currentSpace:Option[String],limit:Int,showDownload:Boolean,accessData:models.DatasetAccess,canAddDatasetToCollection:Boolean,stagingAreaDefined:Boolean,flash:play.api.mvc.Flash,user:Option[models.User],request:RequestHeader): play.api.templates.HtmlFormat.Appendable = apply(dataset,comments,previewers,m,collectionsInside,rdfExported,relatedSensors,datasetSpaces_canRemove,fileList,filesTags,toPublish,curationObjects,currentSpace,limit,showDownload,accessData,canAddDatasetToCollection,stagingAreaDefined)(flash,user,request)
    
    def f:((models.Dataset,List[Comment],List[Previewer],List[models.Metadata],List[models.Collection],Boolean,List[scala.Tuple3[String, String, String]],Option[Map[ProjectSpace, Boolean]],List[File],scala.collection.immutable.TreeSet[String],Boolean,List[models.CurationObject],Option[String],Int,Boolean,models.DatasetAccess,Boolean,Boolean) => (play.api.mvc.Flash,Option[models.User],RequestHeader) => play.api.templates.HtmlFormat.Appendable) = (dataset,comments,previewers,m,collectionsInside,rdfExported,relatedSensors,datasetSpaces_canRemove,fileList,filesTags,toPublish,curationObjects,currentSpace,limit,showDownload,accessData,canAddDatasetToCollection,stagingAreaDefined) => (flash,user,request) => apply(dataset,comments,previewers,m,collectionsInside,rdfExported,relatedSensors,datasetSpaces_canRemove,fileList,filesTags,toPublish,curationObjects,currentSpace,limit,showDownload,accessData,canAddDatasetToCollection,stagingAreaDefined)(flash,user,request)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Thu Sep 26 16:46:55 CDT 2019
                    SOURCE: /Users/stevek/4ceed-clowder/app/views/dataset.scala.html
                    HASH: 535d04b098f37b8f70405940775b95da0dd55fb1
                    MATRIX: 967->1|1855->708|1884->796|1921->798|1948->816|1988->818|2044->838|2059->844|2126->889|2256->983|2290->995|2355->1024|2408->1055|2489->1108|2518->1109|2591->1154|2620->1155|2678->1185|2707->1186|2780->1231|2809->1232|3041->1428|3073->1451|3088->1457|3099->1480|3132->1504|3172->1506|3234->1532|3267->1556|3307->1558|3440->1655|3456->1661|3518->1700|3565->1710|3585->1720|3621->1733|3662->1737|3729->1781|3783->1817|3796->1823|3835->1824|3901->1854|3933->1877|3973->1879|4622->2492|4641->2502|4654->2506|4708->2522|4803->2581|4818->2587|4862->2609|4908->2619|4918->2620|4946->2625|5032->2674|5082->2701|5165->2752|5334->2903|5347->2908|5386->2909|5517->3004|5533->3010|5599->3053|5640->3057|5657->3064|5696->3080|5768->3120|5826->3146|5868->3190|5889->3202|5915->3223|6047->3319|6064->3326|6092->3331|6133->3335|6189->3368|6462->3604|6503->3622|6566->3649|6694->3767|6735->3769|7129->4131|7335->4301|7350->4307|7416->4350|7457->4354|7474->4361|7513->4377|7600->4428|7616->4435|7669->4466|7736->4497|7770->4522|7810->4524|7876->4554|7988->4656|8029->4658|8249->4841|8272->4854|8312->4871|8352->4874|8372->4884|8412->4901|8584->5036|8607->5049|8647->5066|8687->5069|8707->5079|8747->5096|8918->5230|8941->5243|8980->5259|9020->5262|9040->5272|9080->5289|9177->5367|9191->5372|9231->5373|9314->5419|9334->5429|9364->5436|9432->5471|9491->5497|9884->5853|9995->5953|10037->5955|10483->6368|10577->6425|10595->6433|10639->6454|10883->6661|10927->6695|10968->6697|11007->6699|11056->6724|11079->6727|11094->6732|11135->6733|11187->6751|11250->6777|11361->6877|11403->6879|11726->7169|11943->7349|12063->7458|12105->7460|12195->7513|12256->7551|12556->7818|12884->8109|13004->8218|13046->8220|13136->8273|13197->8311|13474->8555|13533->8577|13662->8695|13704->8697|13874->8830|13891->8837|13917->8840|14231->9135|14245->9140|14285->9141|14718->9541|14778->9564|14805->9581|14845->9582|14908->9608|14926->9616|14972->9639|15027->9661|15087->9684|15105->9692|15150->9714|15201->9788|15260->9810|15288->9828|15329->9830|15392->9856|15410->9864|15451->9882|15506->9904|15565->9926|15664->10014|15706->10016|15769->10042|15943->10205|15985->10207|16061->10246|16144->10306|16334->10477|16348->10482|16388->10483|16455->10513|16505->10553|16546->10555|16626->10598|16695->10644|16897->10827|16911->10832|16951->10833|17219->11068|17278->11094|17333->11116|17392->11138|17512->11247|17554->11249|17608->13122|17663->13144|18210->13807|18357->13916|18376->13924|18405->13929|18837->14324|18947->14423|18989->14425|19056->14455|19137->14513|19196->14539|19296->14602|19313->14609|19348->14621|19389->14625|19430->14642|19603->14778|19631->14796|19672->14798|19730->15134|19789->15160|20541->15875|20561->15885|20598->15899|20723->15987|20933->16186|20975->16188|21242->16418|21258->16424|21329->16471|21673->16782|21838->16910|21856->16918|21909->16948|22119->17121|22135->17127|22258->17226|22309->17240|22327->17248|22396->17294|22447->17308|22465->17316|22523->17351|22574->17365|22654->17435|22695->17437|22750->17455|22768->17463|22828->17500|22875->17514|22926->17528|22989->17581|23030->17583|23085->17601|23102->17608|23189->17672|23236->17686|23344->17843|23379->17944|23436->17964|23452->17970|23517->18012|23607->18065|23623->18071|23692->18117|23781->18169|23797->18175|23867->18222|23957->18275|23973->18281|24042->18327|24132->18380|24148->18386|24215->18430|24305->18483|24321->18489|24386->18531|24476->18584|24492->18590|24555->18630|24733->18771|24750->18778|24776->18781|24968->18944|24998->18945|26182->20100|26212->20101|26275->20135|26305->20136|26505->20307|26535->20308|26674->20418|26704->20419|26841->20527|26871->20528|27018->20646|27048->20647|27123->20693|27153->20694|27299->20811|27329->20812|27379->20833|27409->20834|27455->20851|27485->20852|27546->20884|27576->20885|27656->20936|27686->20937|27745->20967|27775->20968|27856->21020|27886->21021|27928->21034|27958->21035|28028->21076|28058->21077|28205->21195|28235->21196|28426->21350|28443->21357|28470->21360|28508->21368|28539->21369|28626->21427|28656->21428|28758->21501|28788->21502|28928->21613|28958->21614|29065->21692|29095->21693|29395->21964|29425->21965|29585->22096|29615->22097|29665->22118|29695->22119|29744->22139|29774->22140|29881->22210|29898->22217|29933->22229|29969->22236|29999->22237|30093->22302|30123->22303|30182->22325|30202->22335|30312->22421|30344->22423|30375->22424|30478->22498|30508->22499|30695->22657|30725->22658|30797->22701|30827->22702|31014->22860|31044->22861|31151->22939|31181->22940|31376->23106|31406->23107|31479->23151|31509->23152|31704->23318|31734->23319|31782->23338|31812->23339|31871->23361|31891->23371|32001->23457|32033->23459|32064->23460|32171->23538|32201->23539|32386->23695|32416->23696|32492->23743|32522->23744|32707->23900|32737->23901|32785->23920|32815->23921|33006->24075|33020->24079|33082->24131|33098->24137|33110->24160|33135->24175|33176->24177|33235->24199|33390->24343|33432->24345|33528->24408|33570->24451|33592->24463|33633->24465|33669->24486|33741->24521|33758->24528|33788->24535|33892->24610|33922->24611|33990->24650|34020->24651|34539->25141|34569->25142|34633->25177|34663->25178|34748->25234|34778->25235|34904->25332|34934->25333|35106->25476|35136->25477|35180->25492|35210->25493|35316->25562|35333->25569|35359->25572|35396->25580|35426->25581|35593->25719|35623->25720|35722->25790|35752->25791|36417->26427|36447->26428|36550->26502|36580->26503|36868->26762|36898->26763|37054->26890|37084->26891|37130->26908|37160->26909|37205->26925|37235->26926|37339->27001|37369->27002|38144->27748|38174->27749|38330->27876|38360->27877|38579->28067|38609->28068|38681->28111|38711->28112|39090->28462|39120->28463|39190->28504|39220->28505|39412->28668|39442->28669|39500->28698|39530->28699|39643->28775|39660->28782|39686->28785|39723->28793|39753->28794|39920->28932|39950->28933|40049->29003|40079->29004|40622->29518|40652->29519|40755->29593|40785->29594|41074->29854|41104->29855|41261->29983|41291->29984|41337->30001|41367->30002|41412->30018|41442->30019|41507->30055|41537->30056|41736->30228|41766->30229|41880->30314|41910->30315|41953->30329|41983->30330|42056->30374|42086->30375|42311->30571|42341->30572|42445->30647|42475->30648|42597->30741|42627->30742|42673->30759|42703->30760|42832->30852|42849->30859|42876->30862|42919->30867|42937->30874|42966->30879|43054->30938|43084->30939|43140->30966|43170->30967|43265->31033|43295->31034|43391->31101|43421->31102|43506->31158|43536->31159|43683->31277|43713->31278|43849->31385|43879->31386|43913->31391|43943->31392|44041->31461|44071->31462|44132->31494|44162->31495|44310->31614|44340->31615|44461->31698|44497->31710|44528->31711|44559->31712|44681->31797|44698->31804|44724->31807|44762->31815|44793->31816|44992->31986|45022->31987|45128->32064|45158->32065|45481->32359|45511->32360|45622->32442|45652->32443|45993->32755|46023->32756|46174->32878|46204->32879|46258->32904|46288->32905|46341->32929|46371->32930|46452->32982|46482->32983|46526->32998|46556->32999|46645->33059|46675->33060|46741->33097|46771->33098|46889->33187|46919->33188|46954->33194|46984->33195|47077->33259|47107->33260|47198->33322|47228->33323|47308->33374|47338->33375|47432->33440|47462->33441|47545->33495|47575->33496|47674->33558|47696->33570|47729->33580|47759->33581|47789->33582|47864->33620|47899->33632|47947->33651|47977->33652|48053->33699|48083->33700|48162->33750|48192->33751|48320->33842|48337->33849|48363->33852|48405->33856|48434->33861|48487->33876|48523->33888|48561->33896|48592->33897|48760->34036|48790->34037|48889->34107|48919->34108|49088->34248|49118->34249|49344->34438|49362->34445|49389->34448|49457->34487|49487->34488|49522->34494|49552->34495|49627->34533|49644->34540|49670->34543|49855->34691|49873->34698|49900->34701|49958->34730|49988->34731|50034->34748|50064->34749|50166->34822|50196->34823|50468->35066|50498->35067|50649->35189|50679->35190|50725->35207|50755->35208|50800->35224|50830->35225|50929->35295|50959->35296|51026->35326|51043->35333|51071->35338|51138->35368|51155->35375|51181->35378|51323->35491|51353->35492|51703->35813|51733->35814|51775->35827|51805->35828|51876->35870|51906->35871|52085->36013|52102->36020|52128->36023|52206->36072|52236->36073|52285->36093|52315->36094|52414->36164|52444->36165|52583->36275|52613->36276|52648->36282|52678->36283|52825->36401|52855->36402|52924->36442|52954->36443|53122->36582|53152->36583|53250->36652|53280->36653|53356->36700|53386->36701|53626->36912|53656->36913|53691->36919|53721->36920|53988->37158|54018->37159|54065->37177|54095->37178|54197->37251|54227->37252|54345->37341|54375->37342|54420->37358|54450->37359|54525->37405|54555->37406|54672->37486|54689->37493|54715->37496|54752->37504|54782->37505|54913->37607|54943->37608|55041->37677|55071->37678|55934->38512|55964->38513|56086->38606|56116->38607|56209->38671|56239->38672|56324->38728|56354->38729|56519->38865|56549->38866|56599->38887|56629->38888|57438->39668|57468->39669|57570->39742|57600->39743|57737->39851|57767->39852|57812->39868|57842->39869|57979->39977|58009->39978|58180->40120|58210->40121|58435->40317|58465->40318|58673->40497|58703->40498|58777->40543|58807->40544|58893->40601|58923->40602|59039->40689|59069->40690|59102->40694|59132->40695|59260->40794|59290->40795|59340->40816|59370->40817|59416->40834|59446->40835|59490->40850|59520->40851|59615->40917|59645->40918|59737->40981|59767->40982|60054->41240|60084->41241|60128->41256|60158->41257|60293->41363|60323->41364|61501->42514|61531->42515|61580->42532
                    LINES: 20->1|45->18|47->23|48->24|48->24|48->24|50->26|50->26|50->26|52->28|52->28|53->29|53->29|56->32|56->32|58->34|58->34|59->35|59->35|61->37|61->37|69->45|69->45|69->45|69->46|69->46|69->46|70->47|70->47|70->47|71->48|71->48|71->48|71->48|71->48|71->48|71->48|71->48|72->49|72->49|72->49|73->50|73->50|73->50|81->58|81->58|81->58|81->58|82->59|82->59|82->59|82->59|82->59|82->59|82->59|82->59|83->60|87->64|87->64|87->64|88->65|88->65|88->65|88->65|88->65|88->65|89->66|90->67|92->70|92->70|92->71|94->73|94->73|94->73|94->73|94->73|99->78|99->78|100->79|100->79|100->79|106->85|111->90|111->90|111->90|111->90|111->90|111->90|112->91|112->91|112->91|113->92|113->92|113->92|114->93|114->93|114->93|116->95|116->95|116->95|116->95|116->95|116->95|117->96|117->96|117->96|117->96|117->96|117->96|118->97|118->97|118->97|118->97|118->97|118->97|121->100|121->100|121->100|122->101|122->101|122->101|123->102|124->103|132->111|132->111|132->111|140->119|142->121|142->121|142->121|146->125|146->125|146->125|146->125|146->125|146->125|146->125|146->125|146->125|147->126|147->126|147->126|151->130|156->135|156->135|156->135|157->136|157->136|161->140|166->145|166->145|166->145|167->146|167->146|171->150|172->151|172->151|172->151|174->153|174->153|174->153|179->158|179->158|179->158|185->164|187->166|187->166|187->166|188->167|188->167|188->167|189->168|191->170|191->170|191->170|193->173|194->174|194->174|194->174|195->175|195->175|195->175|196->176|197->177|197->177|197->177|198->178|198->178|198->178|199->179|199->179|202->182|202->182|202->182|203->183|203->183|203->183|204->184|204->184|207->187|207->187|207->187|211->191|212->192|213->193|214->194|214->194|214->194|215->223|216->224|227->237|228->238|228->238|228->238|235->245|235->245|235->245|236->246|236->246|237->247|238->248|238->248|238->248|238->248|238->248|241->251|241->251|241->251|242->258|243->259|257->273|257->273|257->273|260->276|261->277|261->277|265->281|265->281|265->281|270->286|273->289|273->289|273->289|281->297|281->297|281->297|282->298|282->298|282->298|283->299|283->299|283->299|284->300|284->300|284->300|285->301|285->301|285->301|286->302|287->303|287->303|287->303|288->304|288->304|288->304|289->305|294->310|295->311|297->313|297->313|297->313|298->314|298->314|298->314|299->315|299->315|299->315|300->316|300->316|300->316|301->317|301->317|301->317|302->318|302->318|302->318|303->319|303->319|303->319|306->322|306->322|306->322|312->328|312->328|324->340|324->340|325->341|325->341|329->345|329->345|331->347|331->347|333->349|333->349|336->352|336->352|337->353|337->353|340->356|340->356|341->357|341->357|342->358|342->358|343->359|343->359|345->361|345->361|346->362|346->362|348->364|348->364|349->365|349->365|350->366|350->366|355->371|355->371|359->375|359->375|359->375|359->375|359->375|361->377|361->377|362->378|362->378|365->381|365->381|367->383|367->383|370->386|370->386|372->388|372->388|373->389|373->389|375->391|375->391|379->395|379->395|379->395|379->395|379->395|381->397|381->397|383->399|383->399|383->399|383->399|383->399|384->400|384->400|387->403|387->403|387->403|387->403|390->406|390->406|392->408|392->408|395->411|395->411|395->411|395->411|398->414|398->414|399->415|399->415|401->417|401->417|401->417|401->417|401->417|402->418|402->418|405->421|405->421|405->421|405->421|408->424|408->424|409->425|409->425|414->430|414->430|414->430|414->430|414->431|414->431|414->431|415->432|415->432|415->432|417->434|418->436|418->436|418->436|418->438|420->440|420->440|420->440|422->442|422->442|424->444|424->444|433->453|433->453|435->455|435->455|436->456|436->456|439->459|439->459|442->462|442->462|442->462|442->462|444->464|444->464|444->464|444->464|444->464|448->468|448->468|450->470|450->470|460->480|460->480|462->482|462->482|465->485|465->485|467->487|467->487|468->488|468->488|470->490|470->490|472->492|472->492|478->498|478->498|480->500|480->500|484->504|484->504|486->506|486->506|493->513|493->513|495->515|495->515|498->518|498->518|498->518|498->518|500->520|500->520|500->520|500->520|500->520|504->524|504->524|506->526|506->526|514->534|514->534|516->536|516->536|519->539|519->539|521->541|521->541|522->542|522->542|524->544|524->544|526->546|526->546|528->548|528->548|530->550|530->550|531->551|531->551|533->553|533->553|536->556|536->556|537->557|537->557|539->559|539->559|540->560|540->560|542->562|542->562|542->562|542->562|542->562|542->562|544->564|544->564|546->566|546->566|547->567|547->567|550->570|550->570|552->572|552->572|556->576|556->576|559->579|559->579|559->579|559->579|561->581|561->581|562->582|562->582|565->585|565->585|565->585|565->585|565->585|565->585|566->586|566->586|566->586|566->586|566->586|570->590|570->590|571->591|571->591|576->596|576->596|578->598|578->598|582->602|582->602|584->604|584->604|585->605|585->605|587->607|587->607|589->609|589->609|590->610|590->610|592->612|592->612|593->613|593->613|595->615|595->615|595->615|595->615|597->617|597->617|599->619|599->619|601->621|601->621|603->623|603->623|605->625|605->625|607->627|607->627|607->627|607->627|607->627|608->628|608->628|609->629|609->629|610->630|610->630|610->630|610->630|611->631|611->631|611->631|611->631|611->631|611->631|611->631|611->631|611->631|615->635|615->635|617->637|617->637|620->640|620->640|623->643|623->643|623->643|624->644|624->644|624->644|624->644|625->645|625->645|625->645|627->647|627->647|627->647|628->648|628->648|629->649|629->649|630->650|630->650|633->653|633->653|635->655|635->655|636->656|636->656|638->658|638->658|640->660|640->660|641->661|641->661|641->661|642->662|642->662|642->662|644->664|644->664|650->670|650->670|651->671|651->671|653->673|653->673|656->676|656->676|656->676|657->677|657->677|657->677|657->677|659->679|659->679|661->681|661->681|661->681|661->681|663->683|663->683|664->684|664->684|668->688|668->688|669->689|669->689|670->690|670->690|673->693|673->693|673->693|673->693|676->696|676->696|678->698|678->698|679->699|679->699|681->701|681->701|683->703|683->703|685->705|685->705|687->707|687->707|687->707|687->707|687->707|690->710|690->710|691->711|691->711|702->722|702->722|704->724|704->724|705->725|705->725|706->726|706->726|708->728|708->728|709->729|709->729|722->742|722->742|724->744|724->744|726->746|726->746|728->748|728->748|731->751|731->751|734->754|734->754|738->758|738->758|741->761|741->761|742->762|742->762|743->763|743->763|745->765|745->765|745->765|745->765|748->768|748->768|749->769|749->769|750->770|750->770|752->772|752->772|754->774|754->774|756->776|756->776|762->782|762->782|763->783|763->783|766->786|766->786|787->807|787->807|790->810
                    -- GENERATED --
                */
            