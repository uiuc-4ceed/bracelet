// @SOURCE:/Users/stevek/4ceed-clowder/conf/routes
// @HASH:8de3ef234bb2a5fe5ca89814b4a202b0871d3dd3
// @DATE:Thu Aug 29 17:10:48 CDT 2019

package api;

public class routes {
public static final api.ReversePreviews Previews = new api.ReversePreviews();
public static final api.ReverseNotebooks Notebooks = new api.ReverseNotebooks();
public static final api.ReverseProxy Proxy = new api.ReverseProxy();
public static final api.ReverseMetadata Metadata = new api.ReverseMetadata();
public static final api.ReverseFolders Folders = new api.ReverseFolders();
public static final api.ReverseAdmin Admin = new api.ReverseAdmin();
public static final api.ReverseUsers Users = new api.ReverseUsers();
public static final api.ReverseLogos Logos = new api.ReverseLogos();
public static final api.ReverseGeometry Geometry = new api.ReverseGeometry();
public static final api.ReverseSections Sections = new api.ReverseSections();
public static final api.ReverseInstitutions Institutions = new api.ReverseInstitutions();
public static final api.ReverseComments Comments = new api.ReverseComments();
public static final api.ReverseTree Tree = new api.ReverseTree();
public static final api.ReverseVocabularyTerms VocabularyTerms = new api.ReverseVocabularyTerms();
public static final api.ReverseFiles Files = new api.ReverseFiles();
public static final api.ReverseApiHelp ApiHelp = new api.ReverseApiHelp();
public static final api.ReverseThumbnails Thumbnails = new api.ReverseThumbnails();
public static final api.ReverseIndexes Indexes = new api.ReverseIndexes();
public static final api.ReverseCollections Collections = new api.ReverseCollections();
public static final api.ReverseDatasets Datasets = new api.ReverseDatasets();
public static final api.ReverseSelected Selected = new api.ReverseSelected();
public static final api.ReverseExtractions Extractions = new api.ReverseExtractions();
public static final api.ReverseEvents Events = new api.ReverseEvents();
public static final api.ReverseGeostreams Geostreams = new api.ReverseGeostreams();
public static final api.ReverseFullTree FullTree = new api.ReverseFullTree();
public static final api.ReverseCurationObjects CurationObjects = new api.ReverseCurationObjects();
public static final api.ReverseSensors Sensors = new api.ReverseSensors();
public static final api.ReverseSearch Search = new api.ReverseSearch();
public static final api.ReverseT2C2 T2C2 = new api.ReverseT2C2();
public static final api.ReverseProjects Projects = new api.ReverseProjects();
public static final api.ReverseVocabularies Vocabularies = new api.ReverseVocabularies();
public static final api.ReverseZoomIt ZoomIt = new api.ReverseZoomIt();
public static final api.ReverseThreeDTexture ThreeDTexture = new api.ReverseThreeDTexture();
public static final api.ReverseSpaces Spaces = new api.ReverseSpaces();
public static final api.ReverseRelations Relations = new api.ReverseRelations();
public static final api.ReverseStatus Status = new api.ReverseStatus();
public static final api.ReverseContextLD ContextLD = new api.ReverseContextLD();
public static class javascript {
public static final api.javascript.ReversePreviews Previews = new api.javascript.ReversePreviews();
public static final api.javascript.ReverseNotebooks Notebooks = new api.javascript.ReverseNotebooks();
public static final api.javascript.ReverseProxy Proxy = new api.javascript.ReverseProxy();
public static final api.javascript.ReverseMetadata Metadata = new api.javascript.ReverseMetadata();
public static final api.javascript.ReverseFolders Folders = new api.javascript.ReverseFolders();
public static final api.javascript.ReverseAdmin Admin = new api.javascript.ReverseAdmin();
public static final api.javascript.ReverseUsers Users = new api.javascript.ReverseUsers();
public static final api.javascript.ReverseLogos Logos = new api.javascript.ReverseLogos();
public static final api.javascript.ReverseGeometry Geometry = new api.javascript.ReverseGeometry();
public static final api.javascript.ReverseSections Sections = new api.javascript.ReverseSections();
public static final api.javascript.ReverseInstitutions Institutions = new api.javascript.ReverseInstitutions();
public static final api.javascript.ReverseComments Comments = new api.javascript.ReverseComments();
public static final api.javascript.ReverseTree Tree = new api.javascript.ReverseTree();
public static final api.javascript.ReverseVocabularyTerms VocabularyTerms = new api.javascript.ReverseVocabularyTerms();
public static final api.javascript.ReverseFiles Files = new api.javascript.ReverseFiles();
public static final api.javascript.ReverseApiHelp ApiHelp = new api.javascript.ReverseApiHelp();
public static final api.javascript.ReverseThumbnails Thumbnails = new api.javascript.ReverseThumbnails();
public static final api.javascript.ReverseIndexes Indexes = new api.javascript.ReverseIndexes();
public static final api.javascript.ReverseCollections Collections = new api.javascript.ReverseCollections();
public static final api.javascript.ReverseDatasets Datasets = new api.javascript.ReverseDatasets();
public static final api.javascript.ReverseSelected Selected = new api.javascript.ReverseSelected();
public static final api.javascript.ReverseExtractions Extractions = new api.javascript.ReverseExtractions();
public static final api.javascript.ReverseEvents Events = new api.javascript.ReverseEvents();
public static final api.javascript.ReverseGeostreams Geostreams = new api.javascript.ReverseGeostreams();
public static final api.javascript.ReverseFullTree FullTree = new api.javascript.ReverseFullTree();
public static final api.javascript.ReverseCurationObjects CurationObjects = new api.javascript.ReverseCurationObjects();
public static final api.javascript.ReverseSensors Sensors = new api.javascript.ReverseSensors();
public static final api.javascript.ReverseSearch Search = new api.javascript.ReverseSearch();
public static final api.javascript.ReverseT2C2 T2C2 = new api.javascript.ReverseT2C2();
public static final api.javascript.ReverseProjects Projects = new api.javascript.ReverseProjects();
public static final api.javascript.ReverseVocabularies Vocabularies = new api.javascript.ReverseVocabularies();
public static final api.javascript.ReverseZoomIt ZoomIt = new api.javascript.ReverseZoomIt();
public static final api.javascript.ReverseThreeDTexture ThreeDTexture = new api.javascript.ReverseThreeDTexture();
public static final api.javascript.ReverseSpaces Spaces = new api.javascript.ReverseSpaces();
public static final api.javascript.ReverseRelations Relations = new api.javascript.ReverseRelations();
public static final api.javascript.ReverseStatus Status = new api.javascript.ReverseStatus();
public static final api.javascript.ReverseContextLD ContextLD = new api.javascript.ReverseContextLD();
}
public static class ref {
public static final api.ref.ReversePreviews Previews = new api.ref.ReversePreviews();
public static final api.ref.ReverseNotebooks Notebooks = new api.ref.ReverseNotebooks();
public static final api.ref.ReverseProxy Proxy = new api.ref.ReverseProxy();
public static final api.ref.ReverseMetadata Metadata = new api.ref.ReverseMetadata();
public static final api.ref.ReverseFolders Folders = new api.ref.ReverseFolders();
public static final api.ref.ReverseAdmin Admin = new api.ref.ReverseAdmin();
public static final api.ref.ReverseUsers Users = new api.ref.ReverseUsers();
public static final api.ref.ReverseLogos Logos = new api.ref.ReverseLogos();
public static final api.ref.ReverseGeometry Geometry = new api.ref.ReverseGeometry();
public static final api.ref.ReverseSections Sections = new api.ref.ReverseSections();
public static final api.ref.ReverseInstitutions Institutions = new api.ref.ReverseInstitutions();
public static final api.ref.ReverseComments Comments = new api.ref.ReverseComments();
public static final api.ref.ReverseTree Tree = new api.ref.ReverseTree();
public static final api.ref.ReverseVocabularyTerms VocabularyTerms = new api.ref.ReverseVocabularyTerms();
public static final api.ref.ReverseFiles Files = new api.ref.ReverseFiles();
public static final api.ref.ReverseApiHelp ApiHelp = new api.ref.ReverseApiHelp();
public static final api.ref.ReverseThumbnails Thumbnails = new api.ref.ReverseThumbnails();
public static final api.ref.ReverseIndexes Indexes = new api.ref.ReverseIndexes();
public static final api.ref.ReverseCollections Collections = new api.ref.ReverseCollections();
public static final api.ref.ReverseDatasets Datasets = new api.ref.ReverseDatasets();
public static final api.ref.ReverseSelected Selected = new api.ref.ReverseSelected();
public static final api.ref.ReverseExtractions Extractions = new api.ref.ReverseExtractions();
public static final api.ref.ReverseEvents Events = new api.ref.ReverseEvents();
public static final api.ref.ReverseGeostreams Geostreams = new api.ref.ReverseGeostreams();
public static final api.ref.ReverseFullTree FullTree = new api.ref.ReverseFullTree();
public static final api.ref.ReverseCurationObjects CurationObjects = new api.ref.ReverseCurationObjects();
public static final api.ref.ReverseSensors Sensors = new api.ref.ReverseSensors();
public static final api.ref.ReverseSearch Search = new api.ref.ReverseSearch();
public static final api.ref.ReverseT2C2 T2C2 = new api.ref.ReverseT2C2();
public static final api.ref.ReverseProjects Projects = new api.ref.ReverseProjects();
public static final api.ref.ReverseVocabularies Vocabularies = new api.ref.ReverseVocabularies();
public static final api.ref.ReverseZoomIt ZoomIt = new api.ref.ReverseZoomIt();
public static final api.ref.ReverseThreeDTexture ThreeDTexture = new api.ref.ReverseThreeDTexture();
public static final api.ref.ReverseSpaces Spaces = new api.ref.ReverseSpaces();
public static final api.ref.ReverseRelations Relations = new api.ref.ReverseRelations();
public static final api.ref.ReverseStatus Status = new api.ref.ReverseStatus();
public static final api.ref.ReverseContextLD ContextLD = new api.ref.ReverseContextLD();
}
}
          