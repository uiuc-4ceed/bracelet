// @SOURCE:/Users/stevek/4ceed-clowder/conf/routes
// @HASH:8de3ef234bb2a5fe5ca89814b4a202b0871d3dd3
// @DATE:Thu Aug 29 17:10:48 CDT 2019

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import models._
import Binders._

import Router.queryString


// @LINE:970
// @LINE:968
// @LINE:934
// @LINE:921
// @LINE:841
// @LINE:838
// @LINE:837
// @LINE:834
// @LINE:831
// @LINE:827
// @LINE:826
// @LINE:825
// @LINE:824
// @LINE:823
// @LINE:822
// @LINE:821
// @LINE:819
// @LINE:818
// @LINE:817
// @LINE:816
// @LINE:815
// @LINE:809
// @LINE:808
// @LINE:797
// @LINE:796
// @LINE:744
// @LINE:639
// @LINE:638
// @LINE:637
// @LINE:290
// @LINE:289
// @LINE:284
// @LINE:278
// @LINE:277
// @LINE:276
// @LINE:275
// @LINE:274
// @LINE:273
// @LINE:272
// @LINE:271
// @LINE:270
// @LINE:269
// @LINE:268
// @LINE:267
// @LINE:266
// @LINE:265
// @LINE:264
// @LINE:263
// @LINE:262
// @LINE:261
// @LINE:260
// @LINE:259
// @LINE:258
// @LINE:257
// @LINE:256
// @LINE:255
// @LINE:254
// @LINE:235
// @LINE:218
// @LINE:213
// @LINE:212
// @LINE:211
// @LINE:210
// @LINE:208
// @LINE:205
// @LINE:200
// @LINE:199
// @LINE:198
// @LINE:197
// @LINE:196
// @LINE:195
// @LINE:194
// @LINE:193
// @LINE:191
// @LINE:190
// @LINE:185
// @LINE:184
// @LINE:183
// @LINE:181
// @LINE:180
// @LINE:179
// @LINE:178
// @LINE:173
// @LINE:172
// @LINE:171
// @LINE:170
// @LINE:165
// @LINE:164
// @LINE:163
// @LINE:162
// @LINE:161
// @LINE:160
// @LINE:159
// @LINE:158
// @LINE:157
// @LINE:156
// @LINE:155
// @LINE:154
// @LINE:153
// @LINE:152
// @LINE:148
// @LINE:147
// @LINE:146
// @LINE:145
// @LINE:144
// @LINE:143
// @LINE:142
// @LINE:141
// @LINE:140
// @LINE:139
// @LINE:138
// @LINE:137
// @LINE:136
// @LINE:131
// @LINE:130
// @LINE:129
// @LINE:128
// @LINE:127
// @LINE:126
// @LINE:125
// @LINE:124
// @LINE:123
// @LINE:122
// @LINE:121
// @LINE:120
// @LINE:119
// @LINE:118
// @LINE:117
// @LINE:116
// @LINE:115
// @LINE:114
// @LINE:113
// @LINE:112
// @LINE:111
// @LINE:110
// @LINE:109
// @LINE:104
// @LINE:103
// @LINE:102
// @LINE:101
// @LINE:99
// @LINE:97
// @LINE:96
// @LINE:95
// @LINE:92
// @LINE:91
// @LINE:90
// @LINE:89
// @LINE:88
// @LINE:87
// @LINE:86
// @LINE:84
// @LINE:83
// @LINE:77
// @LINE:76
// @LINE:74
// @LINE:69
// @LINE:68
// @LINE:63
// @LINE:62
// @LINE:61
// @LINE:60
// @LINE:55
// @LINE:54
// @LINE:53
// @LINE:52
// @LINE:51
// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:41
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:8
package controllers {

// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
class ReverseAssets {
    

// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
def at(file:String): Call = {
   (file: @unchecked) match {
// @LINE:22
case (file) if file == "/jsonld/contexts/metadata.jsonld" => Call("GET", _prefix + { _defaultPrefix } + "contexts/metadata.jsonld")
                                                        
// @LINE:23
case (file) if file == "/images/glyphicons-halflings-white.png" => Call("GET", _prefix + { _defaultPrefix } + "assets/img/glyphicons-halflings-white.png")
                                                        
// @LINE:24
case (file) if file == "/images/glyphicons-halflings.png" => Call("GET", _prefix + { _defaultPrefix } + "assets/img/glyphicons-halflings.png")
                                                        
// @LINE:25
case (file) if true => Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
                                                        
   }
}
                                                
    
}
                          

// @LINE:181
// @LINE:180
// @LINE:179
// @LINE:178
// @LINE:163
class ReverseMetadata {
    

// @LINE:179
def view(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "metadata/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:181
def dataset(dataset_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/" + implicitly[PathBindable[UUID]].unbind("dataset_id", dataset_id) + "/metadata")
}
                                                

// @LINE:180
def file(file_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id) + "/metadata")
}
                                                

// @LINE:163
def getMetadataBySpace(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata")
}
                                                

// @LINE:178
def search(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "metadata/search")
}
                                                
    
}
                          

// @LINE:122
// @LINE:121
// @LINE:120
// @LINE:119
// @LINE:118
// @LINE:117
// @LINE:116
class ReverseToolManager {
    

// @LINE:116
def toolManager(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "toolManager")
}
                                                

// @LINE:121
def removeInstance(toolType:String, instanceID:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/removeInstance" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("toolType", toolType)), Some(implicitly[QueryStringBindable[UUID]].unbind("instanceID", instanceID)))))
}
                                                

// @LINE:119
def uploadDatasetToTool(instanceID:UUID, datasetID:UUID, datasetName:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/uploadToTool" + queryString(List(Some(implicitly[QueryStringBindable[UUID]].unbind("instanceID", instanceID)), Some(implicitly[QueryStringBindable[UUID]].unbind("datasetID", datasetID)), Some(implicitly[QueryStringBindable[String]].unbind("datasetName", datasetName)))))
}
                                                

// @LINE:122
def launchTool(sessionName:String, ttype:String, datasetId:UUID, datasetName:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/launchTool" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("sessionName", sessionName)), Some(implicitly[QueryStringBindable[String]].unbind("ttype", ttype)), Some(implicitly[QueryStringBindable[UUID]].unbind("datasetId", datasetId)), Some(implicitly[QueryStringBindable[String]].unbind("datasetName", datasetName)))))
}
                                                

// @LINE:118
def getLaunchableTools(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/launchableTools")
}
                                                

// @LINE:117
def refreshToolSidebar(datasetid:UUID, datasetName:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/refreshToolList" + queryString(List(Some(implicitly[QueryStringBindable[UUID]].unbind("datasetid", datasetid)), Some(implicitly[QueryStringBindable[String]].unbind("datasetName", datasetName)))))
}
                                                

// @LINE:120
def getInstances(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/getInstances")
}
                                                
    
}
                          

// @LINE:130
// @LINE:129
class ReverseFolders {
    

// @LINE:129
def addFiles(id:UUID, folderId:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/" + implicitly[PathBindable[String]].unbind("folderId", dynamicString(folderId)) + "/addFiles")
}
                                                

// @LINE:130
def createFolder(parentDatasetId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "datasets/" + implicitly[PathBindable[UUID]].unbind("parentDatasetId", parentDatasetId) + "/newFolder")
}
                                                
    
}
                          

// @LINE:278
// @LINE:277
// @LINE:275
// @LINE:274
// @LINE:273
// @LINE:272
// @LINE:271
// @LINE:270
// @LINE:269
// @LINE:268
// @LINE:267
// @LINE:266
// @LINE:265
// @LINE:264
// @LINE:263
// @LINE:262
// @LINE:261
// @LINE:260
// @LINE:259
// @LINE:258
// @LINE:257
// @LINE:256
// @LINE:255
// @LINE:254
class ReverseAdmin {
    

// @LINE:262
def getIndexers(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/indexers")
}
                                                

// @LINE:263
def getIndexes(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/indexes")
}
                                                

// @LINE:270
def listRoles(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/roles")
}
                                                

// @LINE:260
def getExtractors(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/extractors")
}
                                                

// @LINE:269
def deleteAllIndexes(): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "admin/index")
}
                                                

// @LINE:264
def getSections(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/sections")
}
                                                

// @LINE:257
def adminIndex(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/indexAdmin")
}
                                                

// @LINE:266
def createIndex(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "admin/createIndex")
}
                                                

// @LINE:259
def getAdapters(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/adapters")
}
                                                

// @LINE:271
def createRole(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/roles/new")
}
                                                

// @LINE:272
def submitCreateRole(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "admin/roles/submitNew")
}
                                                

// @LINE:268
def deleteIndex(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "admin/index/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:256
def users(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/users")
}
                                                

// @LINE:258
def reindexFiles(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/reindexFiles")
}
                                                

// @LINE:265
def getMetadataDefinitions(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/metadata/definitions")
}
                                                

// @LINE:277
def viewDumpers(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/dataDumps")
}
                                                

// @LINE:255
def tos(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/tos")
}
                                                

// @LINE:274
def editRole(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/roles/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/edit")
}
                                                

// @LINE:275
def updateRole(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "admin/roles/update")
}
                                                

// @LINE:267
def buildIndex(id:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "admin/index/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)) + "/build")
}
                                                

// @LINE:278
def sensors(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/sensors")
}
                                                

// @LINE:254
def customize(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/customize")
}
                                                

// @LINE:273
def removeRole(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "admin/roles/delete/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:261
def getMeasures(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/measures")
}
                                                
    
}
                          

// @LINE:797
// @LINE:796
// @LINE:34
// @LINE:31
// @LINE:30
class ReverseUsers {
    

// @LINE:31
def acceptTermsOfServices(redirect:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users/acceptTermsOfServices" + queryString(List(if(redirect == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("redirect", redirect)))))
}
                                                

// @LINE:796
def getFollowers(index:Int = 0, limit:Int = 12): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users/followers" + queryString(List(if(index == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("index", index)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:30
def getUsers(when:String = "", id:String = "", limit:Int = 24): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users" + queryString(List(if(when == "") None else Some(implicitly[QueryStringBindable[String]].unbind("when", when)), if(id == "") None else Some(implicitly[QueryStringBindable[String]].unbind("id", id)), if(limit == 24) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:34
def sendEmail(subject:String, from:String, recipient:String, body:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "users/sendEmail" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("subject", subject)), Some(implicitly[QueryStringBindable[String]].unbind("from", from)), Some(implicitly[QueryStringBindable[String]].unbind("recipient", recipient)), Some(implicitly[QueryStringBindable[String]].unbind("body", body)))))
}
                                                

// @LINE:797
def getFollowing(index:Int = 0, limit:Int = 12): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users/following" + queryString(List(if(index == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("index", index)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                
    
}
                          

// @LINE:74
// @LINE:68
// @LINE:63
// @LINE:62
// @LINE:61
// @LINE:60
class ReverseExtractionInfo {
    

// @LINE:61
def getExtractorServersIP(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "extractions/servers_ips")
}
                                                

// @LINE:60
def getDTSRequests(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "extractions/requests")
}
                                                

// @LINE:68
def getBookmarkletPage(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "bookmarklet")
}
                                                

// @LINE:63
def getExtractorInputTypes(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "extractions/supported_input_types")
}
                                                

// @LINE:74
def getExtensionPage(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "extensions/dts/chrome")
}
                                                

// @LINE:62
def getExtractorNames(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "extractions/extractors_names")
}
                                                
    
}
                          

// @LINE:276
// @LINE:131
// @LINE:104
class ReverseExtractors {
    

// @LINE:131
def submitDatasetExtraction(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/extractions")
}
                                                

// @LINE:104
def submitFileExtraction(file_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id) + "/extractions")
}
                                                

// @LINE:276
def listAllExtractions(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/extractions")
}
                                                
    
}
                          

// @LINE:55
// @LINE:54
// @LINE:53
// @LINE:52
// @LINE:51
class ReverseError {
    

// @LINE:53
def incorrectPermissions(msg:String = null): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "error/noPermissions" + queryString(List(if(msg == null) None else Some(implicitly[QueryStringBindable[String]].unbind("msg", msg)))))
}
                                                

// @LINE:52
def authenticationRequiredMessage(msg:String, url:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "error/authenticationRequiredMessage/" + implicitly[PathBindable[String]].unbind("msg", dynamicString(msg)) + "/" + implicitly[PathBindable[String]].unbind("url", dynamicString(url)))
}
                                                

// @LINE:54
def notActivated(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "error/notActivated")
}
                                                

// @LINE:51
def authenticationRequired(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "error/authenticationRequired")
}
                                                

// @LINE:55
def notAuthorized(msg:String, id:String, resourceType:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "error/notAuthorized" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("msg", msg)), Some(implicitly[QueryStringBindable[String]].unbind("id", id)), Some(implicitly[QueryStringBindable[String]].unbind("resourceType", resourceType)))))
}
                                                
    
}
                          

// @LINE:208
// @LINE:205
class ReverseDataAnalysis {
    

// @LINE:205
def listSessions(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "analysisSessions")
}
                                                

// @LINE:208
def terminate(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "terminateSession/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                
    
}
                          

// @LINE:103
// @LINE:102
// @LINE:101
// @LINE:99
// @LINE:97
// @LINE:96
// @LINE:95
// @LINE:92
// @LINE:91
// @LINE:90
// @LINE:89
// @LINE:88
// @LINE:87
// @LINE:86
// @LINE:84
// @LINE:83
// @LINE:77
// @LINE:76
class ReverseFiles {
    

// @LINE:99
def uploaddnd(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "uploaddnd/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:86
def metadataSearch(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files/metadataSearch")
}
                                                

// @LINE:88
def followingFiles(index:Int = 0, size:Int = 12, mode:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files/following" + queryString(List(if(index == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("index", index)), if(size == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("size", size)), if(mode == "") None else Some(implicitly[QueryStringBindable[String]].unbind("mode", mode)))))
}
                                                

// @LINE:103
def fileBySection(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "file_by_section/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:95
def upload(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "upload")
}
                                                

// @LINE:76
def extractFile(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "extraction/form")
}
                                                

// @LINE:84
def uploadFile(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files/new")
}
                                                

// @LINE:97
def uploadSelectQuery(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "uploadSelectQuery")
}
                                                

// @LINE:101
def uploadDragDrop(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "uploadDragDrop")
}
                                                

// @LINE:87
def generalMetadataSearch(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files/generalMetadataSearch")
}
                                                

// @LINE:102
def thumbnail(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "fileThumbnail/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/blob")
}
                                                

// @LINE:83
def list(when:String = "", date:String = "", size:Int = 12, mode:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files" + queryString(List(if(when == "") None else Some(implicitly[QueryStringBindable[String]].unbind("when", when)), if(date == "") None else Some(implicitly[QueryStringBindable[String]].unbind("date", date)), if(size == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("size", size)), if(mode == "") None else Some(implicitly[QueryStringBindable[String]].unbind("mode", mode)))))
}
                                                

// @LINE:89
def file(id:UUID, dataset:Option[String] = None, space:Option[String] = None, folder:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(dataset == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("dataset", dataset)), if(space == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)), if(folder == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("folder", folder)))))
}
                                                

// @LINE:91
def download(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/blob")
}
                                                

// @LINE:96
def uploadAndExtract(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "uploadAndExtract")
}
                                                

// @LINE:77
def uploadExtract(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "extraction/upload")
}
                                                

// @LINE:90
def filePreview(id:UUID, dataset:Option[String] = None, space:Option[String] = None, folder:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "filesPreview/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(dataset == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("dataset", dataset)), if(space == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)), if(folder == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("folder", folder)))))
}
                                                

// @LINE:92
def downloadAsFormat(id:UUID, format:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/download/" + implicitly[PathBindable[String]].unbind("format", dynamicString(format)))
}
                                                
    
}
                          

// @LINE:218
class ReversePreviewers {
    

// @LINE:218
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "previewers/list")
}
                                                
    
}
                          

// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:41
class ReverseProfile {
    

// @LINE:46
// @LINE:43
def viewProfileUUID(uuid:UUID): Call = {
   (uuid: @unchecked) match {
// @LINE:43
case (uuid) if true => Call("GET", _prefix + { _defaultPrefix } + "profile/viewProfile/" + implicitly[PathBindable[UUID]].unbind("uuid", uuid))
                                                        
// @LINE:46
case (uuid) if true => Call("GET", _prefix + { _defaultPrefix } + "profile/" + implicitly[PathBindable[UUID]].unbind("uuid", uuid))
                                                        
   }
}
                                                

// @LINE:41
def viewProfile(email:Option[String]): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "profile/viewProfile" + queryString(List(Some(implicitly[QueryStringBindable[Option[String]]].unbind("email", email)))))
}
                                                

// @LINE:45
def submitChanges(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "profile/submitChanges")
}
                                                

// @LINE:44
def editProfile(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "profile/editProfile")
}
                                                
    
}
                          

// @LINE:213
// @LINE:212
// @LINE:211
// @LINE:210
class ReverseNotebook {
    

// @LINE:211
def viewNotebook(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "notebook/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:212
def editNotebook(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "editNotebook/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:213
def deleteNotebook(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "deleteNotebook/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:210
def listNotebooks(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "notebooks")
}
                                                
    
}
                          

// @LINE:148
// @LINE:147
// @LINE:146
// @LINE:145
// @LINE:144
// @LINE:143
// @LINE:142
// @LINE:141
// @LINE:140
// @LINE:139
// @LINE:138
// @LINE:137
// @LINE:136
class ReverseCollections {
    

// @LINE:138
def listChildCollections(parentCollectionId:String, when:String = "", date:String = "", size:Int = 12, space:Option[String] = None, mode:String = "", owner:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collections/listChildCollections" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("parentCollectionId", parentCollectionId)), if(when == "") None else Some(implicitly[QueryStringBindable[String]].unbind("when", when)), if(date == "") None else Some(implicitly[QueryStringBindable[String]].unbind("date", date)), if(size == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("size", size)), if(space == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)), if(mode == "") None else Some(implicitly[QueryStringBindable[String]].unbind("mode", mode)), if(owner == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("owner", owner)))))
}
                                                

// @LINE:140
def newCollectionWithParent(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/newchildCollection")
}
                                                

// @LINE:141
def followingCollections(index:Int = 0, size:Int = 12, mode:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collections/following" + queryString(List(if(index == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("index", index)), if(size == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("size", size)), if(mode == "") None else Some(implicitly[QueryStringBindable[String]].unbind("mode", mode)))))
}
                                                

// @LINE:142
def submit(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "collection/submit")
}
                                                

// @LINE:143
def createStep2(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collection/createStep2")
}
                                                

// @LINE:145
def users(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collection/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/users")
}
                                                

// @LINE:148
def getUpdatedChildCollections(id:UUID, index:Int = 0, limit:Int = 12): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collection/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/childCollections" + queryString(List(if(index == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("index", index)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:146
def previews(collection_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collections/" + implicitly[PathBindable[UUID]].unbind("collection_id", collection_id) + "/previews")
}
                                                

// @LINE:144
def collection(id:UUID, limit:Int = 12): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collection/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:136
def list(when:String = "", date:String = "", size:Int = 12, space:Option[String] = None, mode:String = "", owner:Option[String] = None, showPublic:Boolean = true, showOnlyShared:Boolean = false, showTrash:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collections" + queryString(List(if(when == "") None else Some(implicitly[QueryStringBindable[String]].unbind("when", when)), if(date == "") None else Some(implicitly[QueryStringBindable[String]].unbind("date", date)), if(size == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("size", size)), if(space == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)), if(mode == "") None else Some(implicitly[QueryStringBindable[String]].unbind("mode", mode)), if(owner == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("owner", owner)), if(showPublic == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showPublic", showPublic)), if(showOnlyShared == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showOnlyShared", showOnlyShared)), if(showTrash == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showTrash", showTrash)))))
}
                                                

// @LINE:137
def sortedListInSpace(space:String, offset:Integer, limit:Integer, showPublic:Boolean = true): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collections/sorted" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("space", space)), Some(implicitly[QueryStringBindable[Integer]].unbind("offset", offset)), Some(implicitly[QueryStringBindable[Integer]].unbind("limit", limit)), if(showPublic == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showPublic", showPublic)))))
}
                                                

// @LINE:147
def getUpdatedDatasets(id:UUID, index:Int = 0, limit:Int = 12): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collection/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/datasets" + queryString(List(if(index == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("index", index)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:139
def newCollection(space:Option[String]): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "collections/new" + queryString(List(Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)))))
}
                                                
    
}
                          

// @LINE:235
class ReverseRegistration {
    

// @LINE:235
def handleSignUp(token:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "signup/" + implicitly[PathBindable[String]].unbind("token", dynamicString(token)))
}
                                                
    
}
                          

// @LINE:128
// @LINE:127
// @LINE:126
// @LINE:125
// @LINE:124
// @LINE:123
// @LINE:115
// @LINE:114
// @LINE:113
// @LINE:112
// @LINE:111
// @LINE:110
// @LINE:109
class ReverseDatasets {
    

// @LINE:127
def submit(folderId:Option[String] = None): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "dataset/submit" + queryString(List(if(folderId == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("folderId", folderId)))))
}
                                                

// @LINE:113
def metadataSearch(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/metadataSearch")
}
                                                

// @LINE:109
def list(when:String = "", date:String = "", size:Int = 12, space:Option[String] = None, status:Option[String] = None, mode:String = "", owner:Option[String] = None, showPublic:Boolean = true, showOnlyShared:Boolean = false, showTrash:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets" + queryString(List(if(when == "") None else Some(implicitly[QueryStringBindable[String]].unbind("when", when)), if(date == "") None else Some(implicitly[QueryStringBindable[String]].unbind("date", date)), if(size == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("size", size)), if(space == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)), if(status == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("status", status)), if(mode == "") None else Some(implicitly[QueryStringBindable[String]].unbind("mode", mode)), if(owner == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("owner", owner)), if(showPublic == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showPublic", showPublic)), if(showOnlyShared == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showOnlyShared", showOnlyShared)), if(showTrash == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showTrash", showTrash)))))
}
                                                

// @LINE:114
def generalMetadataSearch(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/generalMetadataSearch")
}
                                                

// @LINE:112
def createStep2(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/createStep2" + queryString(List(Some(implicitly[QueryStringBindable[UUID]].unbind("id", id)))))
}
                                                

// @LINE:125
def users(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/users")
}
                                                

// @LINE:115
def followingDatasets(index:Int = 0, size:Int = 12, mode:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/following" + queryString(List(if(index == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("index", index)), if(size == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("size", size)), if(mode == "") None else Some(implicitly[QueryStringBindable[String]].unbind("mode", mode)))))
}
                                                

// @LINE:111
def newDataset(space:Option[String], collection:Option[String]): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/new" + queryString(List(Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)), Some(implicitly[QueryStringBindable[Option[String]]].unbind("collection", collection)))))
}
                                                

// @LINE:126
def datasetBySection(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets_by_section/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:128
def addFiles(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/addFiles")
}
                                                

// @LINE:123
def dataset(id:UUID, space:Option[String] = None, limit:Int = 20): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(space == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)), if(limit == 20) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:110
def sortedListInSpace(space:String, offset:Integer, limit:Integer, showPublic:Boolean = true): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "datasets/sorted" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("space", space)), Some(implicitly[QueryStringBindable[Integer]].unbind("offset", offset)), Some(implicitly[QueryStringBindable[Integer]].unbind("limit", limit)), if(showPublic == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showPublic", showPublic)))))
}
                                                

// @LINE:124
def getUpdatedFilesAndFolders(datasetId:UUID, limit:Int = 20, pageIndex:Int = 0, space:Option[String] = None): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "datasets/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId) + "/updatedFilesAndFolders" + queryString(List(if(limit == 20) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(pageIndex == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("pageIndex", pageIndex)), if(space == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)))))
}
                                                
    
}
                          

// @LINE:36
// @LINE:35
// @LINE:32
class ReverseLogin {
    

// @LINE:32
def isLoggedIn(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "login/isLoggedIn")
}
                                                

// @LINE:36
def ldapAuthenticate(uid:String, password:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "ldap/authenticate" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("uid", uid)), Some(implicitly[QueryStringBindable[String]].unbind("password", password)))))
}
                                                

// @LINE:35
def ldap(redirecturl:String, token:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "ldap" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("redirecturl", redirecturl)), Some(implicitly[QueryStringBindable[String]].unbind("token", token)))))
}
                                                
    
}
                          

// @LINE:744
class ReverseSelected {
    

// @LINE:744
def get(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "selected")
}
                                                
    
}
                          

// @LINE:841
class ReverseEvents {
    

// @LINE:841
def getEvents(index:Int): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "getEvents" + queryString(List(Some(implicitly[QueryStringBindable[Int]].unbind("index", index)))))
}
                                                
    
}
                          

// @LINE:290
// @LINE:289
// @LINE:284
// @LINE:69
// @LINE:33
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:8
class ReverseApplication {
    

// @LINE:16
def tos(redirect:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "tos" + queryString(List(if(redirect == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("redirect", redirect)))))
}
                                                

// @LINE:69
def bookmarklet(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "bookmarklet.js")
}
                                                

// @LINE:15
def about(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "about")
}
                                                

// @LINE:290
def swaggerUI(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "swaggerUI")
}
                                                

// @LINE:17
def email(subject:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "email" + queryString(List(if(subject == "") None else Some(implicitly[QueryStringBindable[String]].unbind("subject", subject)))))
}
                                                

// @LINE:8
def untrail(path:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + implicitly[PathBindable[String]].unbind("path", path) + "/")
}
                                                

// @LINE:289
def swagger(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "swagger")
}
                                                

// @LINE:33
def onlyGoogle(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "login/isLoggedInWithGoogle")
}
                                                

// @LINE:14
def index(): Call = {
   Call("GET", _prefix)
}
                                                

// @LINE:284
def javascriptRoutes(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "javascriptRoutes")
}
                                                
    
}
                          

// @LINE:173
// @LINE:172
// @LINE:171
// @LINE:170
class ReverseGeostreams {
    

// @LINE:173
def edit(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "geostreams/sensors/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:170
def map(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "geostreams")
}
                                                

// @LINE:171
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "geostreams/sensors")
}
                                                

// @LINE:172
def newSensor(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "geostreams/sensors/new")
}
                                                
    
}
                          

// @LINE:185
// @LINE:184
// @LINE:183
class ReverseTags {
    

// @LINE:183
def search(tag:String, start:String = "", size:Integer = 12, mode:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "tags/search" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("tag", tag)), if(start == "") None else Some(implicitly[QueryStringBindable[String]].unbind("start", start)), if(size == 12) None else Some(implicitly[QueryStringBindable[Integer]].unbind("size", size)), if(mode == "") None else Some(implicitly[QueryStringBindable[String]].unbind("mode", mode)))))
}
                                                

// @LINE:185
def tagListWeighted(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "tags/list/weighted")
}
                                                

// @LINE:184
def tagListOrdered(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "tags/list/ordered")
}
                                                
    
}
                          

// @LINE:809
// @LINE:808
class ReverseRSS {
    

// @LINE:809
def siteRSSOfType(limit:Option[Int], etype:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "rss/" + implicitly[PathBindable[String]].unbind("etype", dynamicString(etype)) + queryString(List(Some(implicitly[QueryStringBindable[Option[Int]]].unbind("limit", limit)))))
}
                                                

// @LINE:808
def siteRSS(limit:Option[Int]): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "rss" + queryString(List(Some(implicitly[QueryStringBindable[Option[Int]]].unbind("limit", limit)))))
}
                                                
    
}
                          

// @LINE:838
// @LINE:837
// @LINE:834
// @LINE:831
// @LINE:827
// @LINE:826
// @LINE:825
// @LINE:824
// @LINE:823
// @LINE:822
// @LINE:821
// @LINE:819
// @LINE:817
// @LINE:816
// @LINE:815
class ReverseCurationObjects {
    

// @LINE:831
def getStatusFromRepository(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/getStatusFromRepository")
}
                                                

// @LINE:821
def editCuration(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/edit")
}
                                                

// @LINE:816
def list(when:String = "", date:String = "", limit:Int = 4, space:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "space/curations" + queryString(List(if(when == "") None else Some(implicitly[QueryStringBindable[String]].unbind("when", when)), if(date == "") None else Some(implicitly[QueryStringBindable[String]].unbind("date", date)), if(limit == 4) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(space == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)))))
}
                                                

// @LINE:826
def sendToRepository(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/sendToRepository")
}
                                                

// @LINE:823
def deleteCuration(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:834
def getUpdatedFilesAndFolders(id:UUID, curationFolderId:String, limit:Int, pageIndex:Int): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/updatedFilesAndFolders" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("curationFolderId", curationFolderId)), Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), Some(implicitly[QueryStringBindable[Int]].unbind("pageIndex", pageIndex)))))
}
                                                

// @LINE:822
def updateCuration(id:UUID, spaceId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/spaces/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId) + "/update")
}
                                                

// @LINE:827
def findMatchingRepositories(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/matchmaker")
}
                                                

// @LINE:815
def newCO(id:UUID, space:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "dataset/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/curations/new" + queryString(List(if(space == "") None else Some(implicitly[QueryStringBindable[String]].unbind("space", space)))))
}
                                                

// @LINE:819
def getCurationObject(id:UUID, limit:Int = 5): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(limit == 5) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:825
def submitRepositorySelection(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/submitRepositorySelection")
}
                                                

// @LINE:824
def compareToRepository(id:UUID, repository:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/compareToRepository" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("repository", repository)))))
}
                                                

// @LINE:838
// @LINE:837
def getPublishedData(space:String): Call = {
   (space: @unchecked) match {
// @LINE:837
case (space) if space == "" => Call("GET", _prefix + { _defaultPrefix } + "publishedData")
                                                        
// @LINE:838
case (space) if true => Call("GET", _prefix + { _defaultPrefix } + "publishedData/" + implicitly[PathBindable[String]].unbind("space", dynamicString(space)))
                                                        
   }
}
                                                

// @LINE:817
def submit(id:UUID, spaceId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "dataset/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/curations/spaces/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId) + "/submit")
}
                                                
    
}
                          

// @LINE:200
// @LINE:199
// @LINE:198
// @LINE:197
// @LINE:196
// @LINE:195
// @LINE:194
// @LINE:193
// @LINE:191
// @LINE:190
class ReverseSearch {
    

// @LINE:198
def findSimilarToExistingFile(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/similar")
}
                                                

// @LINE:193
def advanced(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "advanced")
}
                                                

// @LINE:194
def SearchByText(query:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "SearchByText" + queryString(List(if(query == "") None else Some(implicitly[QueryStringBindable[String]].unbind("query", query)))))
}
                                                

// @LINE:190
def search(query:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "search" + queryString(List(if(query == "") None else Some(implicitly[QueryStringBindable[String]].unbind("query", query)))))
}
                                                

// @LINE:195
def uploadquery(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "uploadquery")
}
                                                

// @LINE:199
def findSimilarToQueryFile(id:UUID, typeToSearch:String, sectionsSelected:List[String] = List.empty ): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "queries/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/similar" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("typeToSearch", typeToSearch)), if(sectionsSelected == List.empty ) None else Some(implicitly[QueryStringBindable[List[String]]].unbind("sectionsSelected", sectionsSelected)))))
}
                                                

// @LINE:200
def findSimilarWeightedIndexes(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "queries/similarWeightedIndexes")
}
                                                

// @LINE:197
def callSearchMultimediaIndexView(section_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "searchbyfeature/" + implicitly[PathBindable[UUID]].unbind("section_id", section_id))
}
                                                

// @LINE:191
def multimediasearch(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "multimediasearch")
}
                                                

// @LINE:196
def searchbyURL(query:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "searchbyURL" + queryString(List(if(query == "") None else Some(implicitly[QueryStringBindable[String]].unbind("query", query)))))
}
                                                
    
}
                          

// @LINE:970
// @LINE:968
// @LINE:934
// @LINE:921
class ReverseT2C2 {
    

// @LINE:968
def uploader(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/uploader")
}
                                                

// @LINE:970
def zipUploader(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/zipUploader")
}
                                                

// @LINE:921
def newTemplate(space:Option[String]): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/new" + queryString(List(Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)))))
}
                                                

// @LINE:934
def uploadFile(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/zipUpload")
}
                                                
    
}
                          

// @LINE:639
// @LINE:638
// @LINE:637
class ReverseVocabularies {
    

// @LINE:637
def newVocabulary(space:Option[String]): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "vocabularies/new" + queryString(List(Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)))))
}
                                                

// @LINE:639
def vocabulary(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "vocabularies/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:638
def submit(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "vocabularies/submit")
}
                                                
    
}
                          

// @LINE:818
// @LINE:165
// @LINE:164
// @LINE:162
// @LINE:161
// @LINE:160
// @LINE:159
// @LINE:158
// @LINE:157
// @LINE:156
// @LINE:155
// @LINE:154
// @LINE:153
// @LINE:152
class ReverseSpaces {
    

// @LINE:161
def updateExtractors(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/extractors")
}
                                                

// @LINE:156
def getSpace(id:UUID, size:Int = 9, direction:String = "desc"): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(size == 9) None else Some(implicitly[QueryStringBindable[Int]].unbind("size", size)), if(direction == "desc") None else Some(implicitly[QueryStringBindable[String]].unbind("direction", direction)))))
}
                                                

// @LINE:155
def followingSpaces(index:Int = 0, size:Int = 12, mode:String = ""): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/following" + queryString(List(if(index == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("index", index)), if(size == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("size", size)), if(mode == "") None else Some(implicitly[QueryStringBindable[String]].unbind("mode", mode)))))
}
                                                

// @LINE:162
def inviteToSpace(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/invite")
}
                                                

// @LINE:164
def submit(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "spaces/submit")
}
                                                

// @LINE:160
def selectExtractors(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/extractors")
}
                                                

// @LINE:159
def manageUsers(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/users")
}
                                                

// @LINE:818
def stagingArea(id:UUID, index:Int = 0, limit:Int = 12): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/stagingArea" + queryString(List(if(index == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("index", index)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:153
def newSpace(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/new")
}
                                                

// @LINE:165
def submitCopy(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "spaces/submitCopy")
}
                                                

// @LINE:154
def copySpace(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/copy/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:157
def updateSpace(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/updateSpace")
}
                                                

// @LINE:158
def addRequest(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/addRequest")
}
                                                

// @LINE:152
def list(when:String = "", date:String = "", size:Int = 12, mode:String = "", owner:Option[String] = None, showAll:Boolean = true, showPublic:Boolean = true, onlyTrial:Boolean = false, showOnlyShared:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces" + queryString(List(if(when == "") None else Some(implicitly[QueryStringBindable[String]].unbind("when", when)), if(date == "") None else Some(implicitly[QueryStringBindable[String]].unbind("date", date)), if(size == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("size", size)), if(mode == "") None else Some(implicitly[QueryStringBindable[String]].unbind("mode", mode)), if(owner == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("owner", owner)), if(showAll == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showAll", showAll)), if(showPublic == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showPublic", showPublic)), if(onlyTrial == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("onlyTrial", onlyTrial)), if(showOnlyShared == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showOnlyShared", showOnlyShared)))))
}
                                                
    
}
                          
}
                  

// @LINE:249
// @LINE:247
// @LINE:246
// @LINE:241
// @LINE:240
// @LINE:239
// @LINE:238
// @LINE:237
// @LINE:236
// @LINE:234
// @LINE:233
// @LINE:232
// @LINE:227
// @LINE:226
package securesocial.controllers {

// @LINE:239
// @LINE:238
// @LINE:237
// @LINE:236
// @LINE:234
// @LINE:233
// @LINE:232
class ReverseRegistration {
    

// @LINE:232
def startSignUp(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "signup")
}
                                                

// @LINE:233
def handleStartSignUp(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "signup")
}
                                                

// @LINE:237
def handleStartResetPassword(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "reset")
}
                                                

// @LINE:238
def resetPassword(token:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "reset/" + implicitly[PathBindable[String]].unbind("token", dynamicString(token)))
}
                                                

// @LINE:234
def signUp(token:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "signup/" + implicitly[PathBindable[String]].unbind("token", dynamicString(token)))
}
                                                

// @LINE:236
def startResetPassword(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "reset")
}
                                                

// @LINE:239
def handleResetPassword(token:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "reset/" + implicitly[PathBindable[String]].unbind("token", dynamicString(token)))
}
                                                
    
}
                          

// @LINE:249
// @LINE:247
// @LINE:246
class ReverseProviderController {
    

// @LINE:249
def notAuthorized(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "not-authorized")
}
                                                

// @LINE:246
def authenticate(provider:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "authenticate/" + implicitly[PathBindable[String]].unbind("provider", dynamicString(provider)))
}
                                                

// @LINE:247
def authenticateByPost(provider:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "authenticate/" + implicitly[PathBindable[String]].unbind("provider", dynamicString(provider)))
}
                                                
    
}
                          

// @LINE:241
// @LINE:240
class ReversePasswordChange {
    

// @LINE:241
def handlePasswordChange(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "password")
}
                                                

// @LINE:240
def page(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "password")
}
                                                
    
}
                          

// @LINE:227
// @LINE:226
class ReverseLoginPage {
    

// @LINE:227
def logout(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "logout")
}
                                                

// @LINE:226
def login(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "login")
}
                                                
    
}
                          
}
                  

// @LINE:978
// @LINE:977
// @LINE:966
// @LINE:965
// @LINE:964
// @LINE:963
// @LINE:962
// @LINE:961
// @LINE:960
// @LINE:959
// @LINE:958
// @LINE:957
// @LINE:956
// @LINE:955
// @LINE:954
// @LINE:953
// @LINE:952
// @LINE:951
// @LINE:950
// @LINE:949
// @LINE:948
// @LINE:947
// @LINE:946
// @LINE:945
// @LINE:944
// @LINE:943
// @LINE:942
// @LINE:941
// @LINE:940
// @LINE:939
// @LINE:938
// @LINE:936
// @LINE:926
// @LINE:919
// @LINE:917
// @LINE:916
// @LINE:915
// @LINE:914
// @LINE:913
// @LINE:912
// @LINE:911
// @LINE:910
// @LINE:909
// @LINE:908
// @LINE:906
// @LINE:905
// @LINE:904
// @LINE:903
// @LINE:902
// @LINE:901
// @LINE:900
// @LINE:899
// @LINE:898
// @LINE:896
// @LINE:895
// @LINE:893
// @LINE:892
// @LINE:891
// @LINE:885
// @LINE:884
// @LINE:883
// @LINE:882
// @LINE:881
// @LINE:880
// @LINE:879
// @LINE:878
// @LINE:877
// @LINE:876
// @LINE:875
// @LINE:874
// @LINE:868
// @LINE:867
// @LINE:861
// @LINE:860
// @LINE:858
// @LINE:857
// @LINE:856
// @LINE:855
// @LINE:854
// @LINE:851
// @LINE:850
// @LINE:849
// @LINE:848
// @LINE:847
// @LINE:840
// @LINE:836
// @LINE:835
// @LINE:833
// @LINE:832
// @LINE:830
// @LINE:829
// @LINE:828
// @LINE:820
// @LINE:803
// @LINE:802
// @LINE:791
// @LINE:790
// @LINE:789
// @LINE:788
// @LINE:787
// @LINE:786
// @LINE:785
// @LINE:784
// @LINE:783
// @LINE:782
// @LINE:781
// @LINE:780
// @LINE:779
// @LINE:778
// @LINE:777
// @LINE:776
// @LINE:771
// @LINE:770
// @LINE:769
// @LINE:768
// @LINE:767
// @LINE:766
// @LINE:765
// @LINE:760
// @LINE:759
// @LINE:758
// @LINE:757
// @LINE:756
// @LINE:751
// @LINE:750
// @LINE:749
// @LINE:748
// @LINE:747
// @LINE:746
// @LINE:745
// @LINE:739
// @LINE:738
// @LINE:737
// @LINE:736
// @LINE:731
// @LINE:730
// @LINE:729
// @LINE:728
// @LINE:727
// @LINE:722
// @LINE:721
// @LINE:720
// @LINE:715
// @LINE:714
// @LINE:713
// @LINE:712
// @LINE:711
// @LINE:710
// @LINE:709
// @LINE:708
// @LINE:707
// @LINE:706
// @LINE:705
// @LINE:704
// @LINE:703
// @LINE:702
// @LINE:701
// @LINE:700
// @LINE:699
// @LINE:698
// @LINE:697
// @LINE:696
// @LINE:695
// @LINE:694
// @LINE:693
// @LINE:692
// @LINE:691
// @LINE:690
// @LINE:689
// @LINE:688
// @LINE:687
// @LINE:686
// @LINE:681
// @LINE:680
// @LINE:679
// @LINE:674
// @LINE:673
// @LINE:672
// @LINE:671
// @LINE:670
// @LINE:669
// @LINE:668
// @LINE:667
// @LINE:666
// @LINE:665
// @LINE:664
// @LINE:659
// @LINE:658
// @LINE:657
// @LINE:656
// @LINE:655
// @LINE:654
// @LINE:653
// @LINE:652
// @LINE:651
// @LINE:650
// @LINE:649
// @LINE:648
// @LINE:647
// @LINE:646
// @LINE:645
// @LINE:644
// @LINE:631
// @LINE:630
// @LINE:629
// @LINE:628
// @LINE:627
// @LINE:626
// @LINE:625
// @LINE:624
// @LINE:623
// @LINE:622
// @LINE:621
// @LINE:620
// @LINE:619
// @LINE:618
// @LINE:617
// @LINE:616
// @LINE:615
// @LINE:614
// @LINE:613
// @LINE:612
// @LINE:611
// @LINE:610
// @LINE:609
// @LINE:608
// @LINE:607
// @LINE:606
// @LINE:605
// @LINE:604
// @LINE:603
// @LINE:602
// @LINE:601
// @LINE:600
// @LINE:599
// @LINE:598
// @LINE:597
// @LINE:596
// @LINE:595
// @LINE:594
// @LINE:593
// @LINE:592
// @LINE:591
// @LINE:590
// @LINE:589
// @LINE:588
// @LINE:587
// @LINE:586
// @LINE:585
// @LINE:584
// @LINE:583
// @LINE:582
// @LINE:581
// @LINE:580
// @LINE:579
// @LINE:578
// @LINE:577
// @LINE:576
// @LINE:575
// @LINE:574
// @LINE:573
// @LINE:572
// @LINE:571
// @LINE:570
// @LINE:569
// @LINE:568
// @LINE:566
// @LINE:565
// @LINE:564
// @LINE:563
// @LINE:562
// @LINE:561
// @LINE:560
// @LINE:554
// @LINE:553
// @LINE:552
// @LINE:551
// @LINE:550
// @LINE:549
// @LINE:548
// @LINE:547
// @LINE:546
// @LINE:545
// @LINE:544
// @LINE:543
// @LINE:542
// @LINE:541
// @LINE:539
// @LINE:538
// @LINE:537
// @LINE:536
// @LINE:535
// @LINE:533
// @LINE:532
// @LINE:530
// @LINE:529
// @LINE:527
// @LINE:526
// @LINE:524
// @LINE:523
// @LINE:522
// @LINE:521
// @LINE:520
// @LINE:518
// @LINE:517
// @LINE:516
// @LINE:511
// @LINE:500
// @LINE:499
// @LINE:498
// @LINE:497
// @LINE:496
// @LINE:495
// @LINE:494
// @LINE:493
// @LINE:492
// @LINE:491
// @LINE:490
// @LINE:489
// @LINE:488
// @LINE:483
// @LINE:482
// @LINE:481
// @LINE:480
// @LINE:479
// @LINE:478
// @LINE:477
// @LINE:476
// @LINE:475
// @LINE:474
// @LINE:473
// @LINE:472
// @LINE:471
// @LINE:470
// @LINE:469
// @LINE:468
// @LINE:467
// @LINE:466
// @LINE:465
// @LINE:464
// @LINE:463
// @LINE:462
// @LINE:461
// @LINE:460
// @LINE:454
// @LINE:448
// @LINE:447
// @LINE:446
// @LINE:441
// @LINE:439
// @LINE:438
// @LINE:436
// @LINE:435
// @LINE:434
// @LINE:433
// @LINE:432
// @LINE:426
// @LINE:425
// @LINE:424
// @LINE:423
// @LINE:422
// @LINE:421
// @LINE:420
// @LINE:418
// @LINE:417
// @LINE:416
// @LINE:415
// @LINE:413
// @LINE:412
// @LINE:410
// @LINE:408
// @LINE:407
// @LINE:403
// @LINE:402
// @LINE:401
// @LINE:400
// @LINE:399
// @LINE:398
// @LINE:397
// @LINE:396
// @LINE:395
// @LINE:394
// @LINE:393
// @LINE:392
// @LINE:391
// @LINE:390
// @LINE:389
// @LINE:388
// @LINE:387
// @LINE:386
// @LINE:385
// @LINE:384
// @LINE:383
// @LINE:382
// @LINE:381
// @LINE:380
// @LINE:379
// @LINE:378
// @LINE:377
// @LINE:376
// @LINE:375
// @LINE:374
// @LINE:373
// @LINE:372
// @LINE:371
// @LINE:370
// @LINE:369
// @LINE:368
// @LINE:367
// @LINE:366
// @LINE:365
// @LINE:364
// @LINE:363
// @LINE:362
// @LINE:361
// @LINE:360
// @LINE:359
// @LINE:354
// @LINE:353
// @LINE:352
// @LINE:351
// @LINE:350
// @LINE:349
// @LINE:348
// @LINE:347
// @LINE:346
// @LINE:345
// @LINE:341
// @LINE:339
// @LINE:338
// @LINE:336
// @LINE:335
// @LINE:334
// @LINE:333
// @LINE:332
// @LINE:331
// @LINE:330
// @LINE:329
// @LINE:325
// @LINE:324
// @LINE:322
// @LINE:321
// @LINE:320
// @LINE:319
// @LINE:318
// @LINE:317
// @LINE:314
// @LINE:313
// @LINE:312
// @LINE:311
// @LINE:306
// @LINE:305
// @LINE:304
// @LINE:303
// @LINE:302
// @LINE:301
// @LINE:300
// @LINE:299
// @LINE:279
// @LINE:7
package api {

// @LINE:657
// @LINE:656
// @LINE:655
// @LINE:654
// @LINE:653
// @LINE:652
// @LINE:651
// @LINE:650
// @LINE:649
// @LINE:648
// @LINE:647
// @LINE:646
// @LINE:644
class ReversePreviews {
    

// @LINE:651
def editAnnotation(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/annotationEdit")
}
                                                

// @LINE:652
def listAnnotations(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/annotationsList")
}
                                                

// @LINE:650
def attachAnnotation(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/annotationAdd")
}
                                                

// @LINE:648
def uploadMetadata(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata")
}
                                                

// @LINE:649
def getMetadata(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata")
}
                                                

// @LINE:653
def setTitle(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/title")
}
                                                

// @LINE:644
def downloadPreview(preview_id:UUID, datasetid:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("preview_id", preview_id) + "/textures/dataset/" + implicitly[PathBindable[UUID]].unbind("datasetid", datasetid) + "/json")
}
                                                

// @LINE:657
def upload(iipKey:String = ""): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/previews" + queryString(List(if(iipKey == "") None else Some(implicitly[QueryStringBindable[String]].unbind("iipKey", iipKey)))))
}
                                                

// @LINE:654
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/previews")
}
                                                

// @LINE:656
def removePreview(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:655
def download(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:647
def attachTile(dzi_id:UUID, tile_id:UUID, level:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("dzi_id", dzi_id) + "/tiles/" + implicitly[PathBindable[UUID]].unbind("tile_id", tile_id) + "/" + implicitly[PathBindable[String]].unbind("level", dynamicString(level)))
}
                                                

// @LINE:646
def getTile(dzi_id_dir:String, level:String, filename:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[String]].unbind("dzi_id_dir", dynamicString(dzi_id_dir)) + "/" + implicitly[PathBindable[String]].unbind("level", dynamicString(level)) + "/" + implicitly[PathBindable[String]].unbind("filename", dynamicString(filename)))
}
                                                
    
}
                          

// @LINE:926
class ReverseNotebooks {
    

// @LINE:926
def submit(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/notebook/submit")
}
                                                
    
}
                          

// @LINE:885
// @LINE:884
// @LINE:883
// @LINE:882
// @LINE:881
// @LINE:880
// @LINE:879
// @LINE:878
// @LINE:877
// @LINE:876
// @LINE:875
// @LINE:874
class ReverseProxy {
    

// @LINE:885
// @LINE:884
// @LINE:883
def delete(endpoint_key:String, pathSuffix:String): Call = {
   (endpoint_key: @unchecked, pathSuffix: @unchecked) match {
// @LINE:883
case (endpoint_key, pathSuffix) if pathSuffix == null => Call("DELETE", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)))
                                                        
// @LINE:884
case (endpoint_key, pathSuffix) if pathSuffix == null => Call("DELETE", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)) + "/")
                                                        
// @LINE:885
case (endpoint_key, pathSuffix) if true => Call("DELETE", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)) + "/" + implicitly[PathBindable[String]].unbind("pathSuffix", pathSuffix))
                                                        
   }
}
                                                

// @LINE:879
// @LINE:878
// @LINE:877
def post(endpoint_key:String, pathSuffix:String): Call = {
   (endpoint_key: @unchecked, pathSuffix: @unchecked) match {
// @LINE:877
case (endpoint_key, pathSuffix) if pathSuffix == null => Call("POST", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)))
                                                        
// @LINE:878
case (endpoint_key, pathSuffix) if pathSuffix == null => Call("POST", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)) + "/")
                                                        
// @LINE:879
case (endpoint_key, pathSuffix) if true => Call("POST", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)) + "/" + implicitly[PathBindable[String]].unbind("pathSuffix", pathSuffix))
                                                        
   }
}
                                                

// @LINE:876
// @LINE:875
// @LINE:874
def get(endpoint_key:String, pathSuffix:String): Call = {
   (endpoint_key: @unchecked, pathSuffix: @unchecked) match {
// @LINE:874
case (endpoint_key, pathSuffix) if pathSuffix == null => Call("GET", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)))
                                                        
// @LINE:875
case (endpoint_key, pathSuffix) if pathSuffix == null => Call("GET", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)) + "/")
                                                        
// @LINE:876
case (endpoint_key, pathSuffix) if true => Call("GET", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)) + "/" + implicitly[PathBindable[String]].unbind("pathSuffix", pathSuffix))
                                                        
   }
}
                                                

// @LINE:882
// @LINE:881
// @LINE:880
def put(endpoint_key:String, pathSuffix:String): Call = {
   (endpoint_key: @unchecked, pathSuffix: @unchecked) match {
// @LINE:880
case (endpoint_key, pathSuffix) if pathSuffix == null => Call("PUT", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)))
                                                        
// @LINE:881
case (endpoint_key, pathSuffix) if pathSuffix == null => Call("PUT", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)) + "/")
                                                        
// @LINE:882
case (endpoint_key, pathSuffix) if true => Call("PUT", _prefix + { _defaultPrefix } + "api/proxy/" + implicitly[PathBindable[String]].unbind("endpoint_key", dynamicString(endpoint_key)) + "/" + implicitly[PathBindable[String]].unbind("pathSuffix", pathSuffix))
                                                        
   }
}
                                                
    
}
                          

// @LINE:478
// @LINE:341
// @LINE:339
// @LINE:338
// @LINE:336
// @LINE:335
// @LINE:334
// @LINE:333
// @LINE:332
// @LINE:331
// @LINE:330
// @LINE:329
// @LINE:325
// @LINE:324
class ReverseMetadata {
    

// @LINE:329
def getDefinitions(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/metadata/definitions")
}
                                                

// @LINE:333
def getUrl(in_url:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/metadata/url/" + implicitly[PathBindable[String]].unbind("in_url", in_url))
}
                                                

// @LINE:341
def getRepository(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/metadata/repository/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:338
def listPeople(term:String, limit:Int): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/metadata/people" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("term", term)), Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:331
def getAutocompleteName(filter:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/metadata/autocompletenames" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("filter", filter)))))
}
                                                

// @LINE:336
def deleteDefinition(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "admin/metadata/definitions/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:324
def addUserMetadata(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/metadata.jsonld")
}
                                                

// @LINE:335
def addDefinition(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/metadata/definitions")
}
                                                

// @LINE:332
def getDefinition(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/metadata/definitions/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:330
def getDefinitionsDistinctName(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/metadata/distinctdefinitions")
}
                                                

// @LINE:339
def getPerson(pid:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/metadata/people/" + implicitly[PathBindable[String]].unbind("pid", dynamicString(pid)))
}
                                                

// @LINE:478
def addDefinitionToSpace(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata")
}
                                                

// @LINE:334
def editDefinition(id:UUID, spaceId:Option[String] = None): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/metadata/definitions/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(spaceId == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("spaceId", spaceId)))))
}
                                                

// @LINE:325
def removeMetadata(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/metadata.jsonld/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                
    
}
                          

// @LINE:625
// @LINE:586
// @LINE:583
// @LINE:582
// @LINE:581
// @LINE:579
class ReverseFolders {
    

// @LINE:582
def deleteFolder(parentDatasetId:UUID, folderId:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("parentDatasetId", parentDatasetId) + "/deleteFolder/" + implicitly[PathBindable[UUID]].unbind("folderId", folderId))
}
                                                

// @LINE:586
def getAllFoldersByDatasetId(datasetId:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId) + "/folders")
}
                                                

// @LINE:583
def updateFolderName(parentDatasetId:UUID, folderId:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("parentDatasetId", parentDatasetId) + "/updateName/" + implicitly[PathBindable[UUID]].unbind("folderId", folderId))
}
                                                

// @LINE:625
def createFolder(parentDatasetId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("parentDatasetId", parentDatasetId) + "/newFolder")
}
                                                

// @LINE:581
def moveFileToDataset(datasetId:UUID, fromFolder:UUID, fileId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId) + "/moveToDataset/" + implicitly[PathBindable[UUID]].unbind("fromFolder", fromFolder) + "/" + implicitly[PathBindable[UUID]].unbind("fileId", fileId))
}
                                                

// @LINE:579
def moveFileBetweenFolders(datasetId:UUID, toFolder:UUID, fileId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId) + "/moveFile/" + implicitly[PathBindable[UUID]].unbind("toFolder", toFolder) + "/" + implicitly[PathBindable[UUID]].unbind("fileId", fileId))
}
                                                
    
}
                          

// @LINE:306
// @LINE:305
// @LINE:304
// @LINE:303
// @LINE:302
// @LINE:301
// @LINE:300
// @LINE:299
// @LINE:279
class ReverseAdmin {
    

// @LINE:303
def deleteAllData(resetAll:Boolean = false): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/delete-data" + queryString(List(if(resetAll == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("resetAll", resetAll)))))
}
                                                

// @LINE:304
def submitAppearance(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/changeAppearance")
}
                                                

// @LINE:302
// @LINE:279
def sensorsConfig(): Call = {
   () match {
// @LINE:279
case () if true => Call("POST", _prefix + { _defaultPrefix } + "api/sensors/config")
                                                        
// @LINE:302
case () if true => Call("POST", _prefix + { _defaultPrefix } + "api/sensors/config")
                                                        
   }
}
                                                

// @LINE:305
def reindex(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/reindex")
}
                                                

// @LINE:299
def mail(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/admin/mail")
}
                                                

// @LINE:306
def updateConfiguration(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/admin/configuration")
}
                                                

// @LINE:301
def users(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/admin/users")
}
                                                

// @LINE:300
def emailZipUpload(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/admin/mailZip")
}
                                                
    
}
                          

// @LINE:791
// @LINE:789
// @LINE:788
// @LINE:787
// @LINE:786
// @LINE:785
// @LINE:784
// @LINE:783
// @LINE:782
// @LINE:781
// @LINE:780
// @LINE:779
// @LINE:778
// @LINE:777
// @LINE:776
class ReverseUsers {
    

// @LINE:780
def updateName(id:UUID, firstName:String, lastName:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/users/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/updateName" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("firstName", firstName)), Some(implicitly[QueryStringBindable[String]].unbind("lastName", lastName)))))
}
                                                

// @LINE:783
def keysAdd(name:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/users/keys" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("name", name)))))
}
                                                

// @LINE:791
def findById(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/users/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:781
def keysList(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/users/keys")
}
                                                

// @LINE:779
def getUser(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/me")
}
                                                

// @LINE:776
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/users")
}
                                                

// @LINE:787
// @LINE:786
def createNewListInUser(email:String, field:String, fieldList:List[String]): Call = {
   (email: @unchecked, field: @unchecked, fieldList: @unchecked) match {
// @LINE:786
case (email, field, fieldList) if true => Call("POST", _prefix + { _defaultPrefix } + "api/users/createNewListInUser" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("email", email)), Some(implicitly[QueryStringBindable[String]].unbind("field", field)), Some(implicitly[QueryStringBindable[List[String]]].unbind("fieldList", fieldList)))))
                                                        
// @LINE:787
case (email, field, fieldList) if true => Call("POST", _prefix + { _defaultPrefix } + "api/users/createNewListInUser" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("email", email)), Some(implicitly[QueryStringBindable[String]].unbind("field", field)), Some(implicitly[QueryStringBindable[List[String]]].unbind("fieldList", fieldList)))))
                                                        
   }
}
                                                

// @LINE:777
def findByEmail(email:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/users/email/" + implicitly[PathBindable[String]].unbind("email", dynamicString(email)))
}
                                                

// @LINE:785
def addUserDatasetView(email:String, dataset:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/users/addUserDatasetView" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("email", email)), Some(implicitly[QueryStringBindable[UUID]].unbind("dataset", dataset)))))
}
                                                

// @LINE:789
def unfollow(followeeUUID:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/users/unfollow" + queryString(List(Some(implicitly[QueryStringBindable[UUID]].unbind("followeeUUID", followeeUUID)))))
}
                                                

// @LINE:778
def deleteUser(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/users/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:788
def follow(followeeUUID:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/users/follow" + queryString(List(Some(implicitly[QueryStringBindable[UUID]].unbind("followeeUUID", followeeUUID)))))
}
                                                

// @LINE:782
def keysGet(name:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/users/keys/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
}
                                                

// @LINE:784
def keysDelete(key:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/users/keys/" + implicitly[PathBindable[String]].unbind("key", dynamicString(key)))
}
                                                
    
}
                          

// @LINE:354
// @LINE:353
// @LINE:352
// @LINE:351
// @LINE:350
// @LINE:349
// @LINE:348
// @LINE:347
// @LINE:346
// @LINE:345
class ReverseLogos {
    

// @LINE:354
def deletePath(path:String, name:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/logos/" + implicitly[PathBindable[String]].unbind("path", dynamicString(path)) + "/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
}
                                                

// @LINE:346
def upload(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/logos")
}
                                                

// @LINE:349
def downloadId(id:UUID, default:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/logos/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/blob" + queryString(List(if(default == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("default", default)))))
}
                                                

// @LINE:345
def list(path:Option[String] = None, name:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/logos" + queryString(List(if(path == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("path", path)), if(name == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("name", name)))))
}
                                                

// @LINE:350
def deleteId(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/logos/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:348
def putId(id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/logos/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:353
def downloadPath(path:String, name:String, default:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/logos/" + implicitly[PathBindable[String]].unbind("path", dynamicString(path)) + "/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)) + "/blob" + queryString(List(if(default == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("default", default)))))
}
                                                

// @LINE:347
def getId(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/logos/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:351
def getPath(path:String, name:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/logos/" + implicitly[PathBindable[String]].unbind("path", dynamicString(path)) + "/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
}
                                                

// @LINE:352
def putPath(path:String, name:String): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/logos/" + implicitly[PathBindable[String]].unbind("path", dynamicString(path)) + "/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
}
                                                
    
}
                          

// @LINE:766
class ReverseGeometry {
    

// @LINE:766
def uploadGeometry(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/geometries")
}
                                                
    
}
                          

// @LINE:674
// @LINE:673
// @LINE:672
// @LINE:671
// @LINE:670
// @LINE:669
// @LINE:668
// @LINE:667
// @LINE:666
// @LINE:665
// @LINE:664
class ReverseSections {
    

// @LINE:667
def attachThumbnail(id:UUID, thumbnail_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/sections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/thumbnails/" + implicitly[PathBindable[UUID]].unbind("thumbnail_id", thumbnail_id))
}
                                                

// @LINE:674
def delete(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/sections/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:672
// @LINE:670
def removeTags(id:UUID): Call = {
   (id: @unchecked) match {
// @LINE:670
case (id) if true => Call("POST", _prefix + { _defaultPrefix } + "api/sections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags/remove")
                                                        
// @LINE:672
case (id) if true => Call("DELETE", _prefix + { _defaultPrefix } + "api/sections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags")
                                                        
   }
}
                                                

// @LINE:671
def removeAllTags(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/sections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags/remove_all")
}
                                                

// @LINE:673
def get(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/sections/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:669
def addTags(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/sections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags")
}
                                                

// @LINE:664
def add(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/sections")
}
                                                

// @LINE:668
def getTags(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/sections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags")
}
                                                

// @LINE:666
def description(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/sections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/description")
}
                                                

// @LINE:665
def comment(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/sections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/comments")
}
                                                
    
}
                          

// @LINE:771
class ReverseInstitutions {
    

// @LINE:771
def addinstitution(institution:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/institutions/addinstitution" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("institution", institution)))))
}
                                                
    
}
                          

// @LINE:739
// @LINE:738
// @LINE:737
// @LINE:736
class ReverseComments {
    

// @LINE:739
def editComment(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/comment/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/editComment")
}
                                                

// @LINE:737
def comment(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/comment/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:736
def mentionInComment(userID:UUID, resourceID:UUID, resourceName:String, resourceType:String, commenterId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/comment/mention" + queryString(List(Some(implicitly[QueryStringBindable[UUID]].unbind("userID", userID)), Some(implicitly[QueryStringBindable[UUID]].unbind("resourceID", resourceID)), Some(implicitly[QueryStringBindable[String]].unbind("resourceName", resourceName)), Some(implicitly[QueryStringBindable[String]].unbind("resourceType", resourceType)), Some(implicitly[QueryStringBindable[UUID]].unbind("commenterId", commenterId)))))
}
                                                

// @LINE:738
def removeComment(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/comment/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/removeComment")
}
                                                
    
}
                          

// @LINE:977
class ReverseTree {
    

// @LINE:977
def getChildrenOfNode(nodeId:Option[String], nodeType:String, mine:Boolean = true, shared:Boolean = false, public:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/tree/getChildrenOfNode" + queryString(List(Some(implicitly[QueryStringBindable[Option[String]]].unbind("nodeId", nodeId)), Some(implicitly[QueryStringBindable[String]].unbind("nodeType", nodeType)), if(mine == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("mine", mine)), if(shared == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("shared", shared)), if(public == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("public", public)))))
}
                                                
    
}
                          

// @LINE:868
// @LINE:867
class ReverseVocabularyTerms {
    

// @LINE:868
def get(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/vocabterms/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:867
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/vocabterms/list")
}
                                                
    
}
                          

// @LINE:959
// @LINE:956
// @LINE:645
// @LINE:454
// @LINE:446
// @LINE:441
// @LINE:439
// @LINE:438
// @LINE:436
// @LINE:435
// @LINE:434
// @LINE:433
// @LINE:432
// @LINE:403
// @LINE:402
// @LINE:401
// @LINE:400
// @LINE:399
// @LINE:398
// @LINE:397
// @LINE:396
// @LINE:395
// @LINE:394
// @LINE:393
// @LINE:392
// @LINE:391
// @LINE:390
// @LINE:389
// @LINE:388
// @LINE:387
// @LINE:386
// @LINE:385
// @LINE:384
// @LINE:383
// @LINE:382
// @LINE:381
// @LINE:380
// @LINE:379
// @LINE:378
// @LINE:377
// @LINE:376
// @LINE:375
// @LINE:374
// @LINE:373
// @LINE:372
// @LINE:371
// @LINE:370
// @LINE:369
// @LINE:368
// @LINE:367
// @LINE:366
// @LINE:365
// @LINE:364
// @LINE:363
// @LINE:362
// @LINE:361
// @LINE:360
// @LINE:359
// @LINE:322
// @LINE:321
// @LINE:320
class ReverseFiles {
    

// @LINE:398
def updateFileName(id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/filename")
}
                                                

// @LINE:380
def updateMetadata(id:UUID, extractor_id:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/updateMetadata" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("extractor_id", extractor_id)))))
}
                                                

// @LINE:374
def getRDFUserMetadata(id:UUID, mappingNum:String = "1"): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/rdfUserMetadata/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(mappingNum == "1") None else Some(implicitly[QueryStringBindable[String]].unbind("mappingNum", mappingNum)))))
}
                                                

// @LINE:435
def getPreviews(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/getPreviews")
}
                                                

// @LINE:360
def attachTexture(three_d_file_id:UUID, texture_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("three_d_file_id", three_d_file_id) + "/3dTextures/" + implicitly[PathBindable[UUID]].unbind("texture_id", texture_id))
}
                                                

// @LINE:396
def updateLicense(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/license")
}
                                                

// @LINE:366
def searchFilesUserMetadata(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/searchusermetadata")
}
                                                

// @LINE:369
// @LINE:368
def uploadToDataset(id:UUID, showPreviews:String = "DatasetLevel", originalZipFile:String = "", flags:String): Call = {
   (id: @unchecked, showPreviews: @unchecked, originalZipFile: @unchecked, flags: @unchecked) match {
// @LINE:368
case (id, showPreviews, originalZipFile, flags) if true => Call("POST", _prefix + { _defaultPrefix } + "api/uploadToDataset/withFlags/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/" + implicitly[PathBindable[String]].unbind("flags", dynamicString(flags)) + queryString(List(if(showPreviews == "DatasetLevel") None else Some(implicitly[QueryStringBindable[String]].unbind("showPreviews", showPreviews)), if(originalZipFile == "") None else Some(implicitly[QueryStringBindable[String]].unbind("originalZipFile", originalZipFile)))))
                                                        
// @LINE:369
case (id, showPreviews, originalZipFile, flags) if true => Call("POST", _prefix + { _defaultPrefix } + "api/uploadToDataset/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(showPreviews == "DatasetLevel") None else Some(implicitly[QueryStringBindable[String]].unbind("showPreviews", showPreviews)), if(originalZipFile == "") None else Some(implicitly[QueryStringBindable[String]].unbind("originalZipFile", originalZipFile)), if(flags == "") None else Some(implicitly[QueryStringBindable[String]].unbind("flags", flags)))))
                                                        
   }
}
                                                

// @LINE:322
def removeMetadataJsonLD(id:UUID, extractor:Option[String] = None): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata.jsonld" + queryString(List(if(extractor == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("extractor", extractor)))))
}
                                                

// @LINE:361
def attachThumbnail(file_id:UUID, thumbnail_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id) + "/thumbnails/" + implicitly[PathBindable[UUID]].unbind("thumbnail_id", thumbnail_id))
}
                                                

// @LINE:446
def dumpFilesMetadata(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/dumpFilesMd")
}
                                                

// @LINE:381
def addVersusMetadata(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/versus_metadata")
}
                                                

// @LINE:372
def sendJob(fileId:UUID, fileType:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/sendJob/" + implicitly[PathBindable[UUID]].unbind("fileId", fileId) + "/" + implicitly[PathBindable[String]].unbind("fileType", dynamicString(fileType)))
}
                                                

// @LINE:399
def reindex(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/reindex")
}
                                                

// @LINE:401
def paths(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/paths")
}
                                                

// @LINE:436
def isBeingProcessed(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/isBeingProcessed")
}
                                                

// @LINE:379
def getMetadataDefinitions(id:UUID, space:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadataDefinitions" + queryString(List(if(space == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)))))
}
                                                

// @LINE:390
// @LINE:376
def removeFile(id:UUID): Call = {
   (id: @unchecked) match {
// @LINE:376
case (id) if true => Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/remove")
                                                        
// @LINE:390
case (id) if true => Call("DELETE", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id))
                                                        
   }
}
                                                

// @LINE:382
def getVersusMetadataJSON(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/versus_metadata")
}
                                                

// @LINE:400
def users(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/users")
}
                                                

// @LINE:395
// @LINE:393
def removeTags(id:UUID): Call = {
   (id: @unchecked) match {
// @LINE:393
case (id) if true => Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags/remove")
                                                        
// @LINE:395
case (id) if true => Call("DELETE", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags")
                                                        
   }
}
                                                

// @LINE:378
def addMetadata(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata")
}
                                                

// @LINE:432
def attachPreview(id:UUID, p_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/previews/" + implicitly[PathBindable[UUID]].unbind("p_id", p_id))
}
                                                

// @LINE:394
def removeAllTags(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags/remove_all")
}
                                                

// @LINE:433
def getWithPreviewUrls(file:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/previewJson/" + implicitly[PathBindable[UUID]].unbind("file", file))
}
                                                

// @LINE:454
// @LINE:438
def getTexture(three_d_file_id:UUID, filename:String): Call = {
   (three_d_file_id: @unchecked, filename: @unchecked) match {
// @LINE:438
case (three_d_file_id, filename) if true => Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("three_d_file_id", three_d_file_id) + "/" + implicitly[PathBindable[String]].unbind("filename", dynamicString(filename)))
                                                        
// @LINE:454
case (three_d_file_id, filename) if true => Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("three_d_file_id", three_d_file_id) + "/" + implicitly[PathBindable[String]].unbind("filename", dynamicString(filename)))
                                                        
   }
}
                                                

// @LINE:645
def downloadByDatasetAndFilename(dataset_id:UUID, filename:String, preview_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/previews/" + implicitly[PathBindable[UUID]].unbind("preview_id", preview_id) + "/textures/dataset/" + implicitly[PathBindable[UUID]].unbind("dataset_id", dataset_id) + "//" + implicitly[PathBindable[String]].unbind("filename", dynamicString(filename)))
}
                                                

// @LINE:377
def get(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata")
}
                                                

// @LINE:402
def changeOwner(id:UUID, ownerId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/changeOwner/" + implicitly[PathBindable[UUID]].unbind("ownerId", ownerId))
}
                                                

// @LINE:362
def attachQueryThumbnail(file_id:UUID, thumbnail_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/queries/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id) + "/thumbnails/" + implicitly[PathBindable[UUID]].unbind("thumbnail_id", thumbnail_id))
}
                                                

// @LINE:373
def getRDFURLsForFile(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/getRDFURLsForFile/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:392
def addTags(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags")
}
                                                

// @LINE:367
def searchFilesGeneralMetadata(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/searchmetadata")
}
                                                

// @LINE:384
def addUserMetadata(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/usermetadata")
}
                                                

// @LINE:391
def getTags(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags")
}
                                                

// @LINE:365
// @LINE:364
def upload(showPreviews:String = "DatasetLevel", originalZipFile:String = "", flags:String): Call = {
   (showPreviews: @unchecked, originalZipFile: @unchecked, flags: @unchecked) match {
// @LINE:364
case (showPreviews, originalZipFile, flags) if true => Call("POST", _prefix + { _defaultPrefix } + "api/files" + queryString(List(if(showPreviews == "DatasetLevel") None else Some(implicitly[QueryStringBindable[String]].unbind("showPreviews", showPreviews)), if(originalZipFile == "") None else Some(implicitly[QueryStringBindable[String]].unbind("originalZipFile", originalZipFile)), if(flags == "") None else Some(implicitly[QueryStringBindable[String]].unbind("flags", flags)))))
                                                        
// @LINE:365
case (showPreviews, originalZipFile, flags) if true => Call("POST", _prefix + { _defaultPrefix } + "api/files/withFlags/" + implicitly[PathBindable[String]].unbind("flags", dynamicString(flags)) + queryString(List(if(showPreviews == "DatasetLevel") None else Some(implicitly[QueryStringBindable[String]].unbind("showPreviews", showPreviews)), if(originalZipFile == "") None else Some(implicitly[QueryStringBindable[String]].unbind("originalZipFile", originalZipFile)))))
                                                        
   }
}
                                                

// @LINE:389
def comment(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/comment")
}
                                                

// @LINE:363
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files")
}
                                                

// @LINE:434
def filePreviewsList(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/listpreviews")
}
                                                

// @LINE:359
def attachGeometry(three_d_file_id:UUID, geometry_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("three_d_file_id", three_d_file_id) + "/geometries/" + implicitly[PathBindable[UUID]].unbind("geometry_id", geometry_id))
}
                                                

// @LINE:320
def addMetadataJsonLD(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata.jsonld")
}
                                                

// @LINE:371
def uploadIntermediate(idAndFlags:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/uploadIntermediate/" + implicitly[PathBindable[String]].unbind("idAndFlags", dynamicString(idAndFlags)))
}
                                                

// @LINE:441
// @LINE:375
def download(id:UUID): Call = {
   (id: @unchecked) match {
// @LINE:375
case (id) if true => Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/blob")
                                                        
// @LINE:441
case (id) if true => Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id))
                                                        
   }
}
                                                

// @LINE:386
def getXMLMetadataJSON(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/xmlmetadatajson")
}
                                                

// @LINE:959
// @LINE:370
def updateDescription(id:UUID): Call = {
   (id: @unchecked) match {
// @LINE:370
case (id) if true => Call("PUT", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/updateDescription")
                                                        
// @LINE:959
case (id) if true => Call("PUT", _prefix + { _defaultPrefix } + "t2c2/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/updateDescription")
                                                        
   }
}
                                                

// @LINE:403
def getOwner(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/getOwner")
}
                                                

// @LINE:956
def uploadToDatasetWithDescription(id:UUID, showPreviews:String = "DatasetLevel", originalZipFile:String = "", flags:String = ""): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "t2c2/uploadToDataset/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(showPreviews == "DatasetLevel") None else Some(implicitly[QueryStringBindable[String]].unbind("showPreviews", showPreviews)), if(originalZipFile == "") None else Some(implicitly[QueryStringBindable[String]].unbind("originalZipFile", originalZipFile)), if(flags == "") None else Some(implicitly[QueryStringBindable[String]].unbind("flags", flags)))))
}
                                                

// @LINE:388
def unfollow(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/unfollow")
}
                                                

// @LINE:439
def downloadquery(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/queries/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:321
def getMetadataJsonLD(id:UUID, extractor:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata.jsonld" + queryString(List(if(extractor == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("extractor", extractor)))))
}
                                                

// @LINE:397
def extract(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/extracted_metadata")
}
                                                

// @LINE:387
def follow(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/follow")
}
                                                

// @LINE:385
def getTechnicalMetadataJSON(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/technicalmetadatajson")
}
                                                

// @LINE:383
def getUserMetadataJSON(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/usermetadatajson")
}
                                                
    
}
                          

// @LINE:7
class ReverseApiHelp {
    

// @LINE:7
def options(path:String): Call = {
   Call("OPTIONS", _prefix + { _defaultPrefix } + implicitly[PathBindable[String]].unbind("path", path))
}
                                                
    
}
                          

// @LINE:722
// @LINE:721
// @LINE:720
class ReverseThumbnails {
    

// @LINE:722
def removeThumbnail(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/thumbnails/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:721
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/thumbnails")
}
                                                

// @LINE:720
def uploadThumbnail(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/fileThumbnail")
}
                                                
    
}
                          

// @LINE:659
// @LINE:658
class ReverseIndexes {
    

// @LINE:659
def features(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/indexes/features")
}
                                                

// @LINE:658
def index(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/indexes")
}
                                                
    
}
                          

// @LINE:554
// @LINE:553
// @LINE:552
// @LINE:551
// @LINE:550
// @LINE:549
// @LINE:548
// @LINE:547
// @LINE:546
// @LINE:545
// @LINE:544
// @LINE:543
// @LINE:542
// @LINE:541
// @LINE:539
// @LINE:538
// @LINE:537
// @LINE:536
// @LINE:535
// @LINE:533
// @LINE:530
// @LINE:529
// @LINE:527
// @LINE:526
// @LINE:524
// @LINE:522
// @LINE:521
// @LINE:520
// @LINE:518
// @LINE:517
// @LINE:516
// @LINE:511
// @LINE:500
// @LINE:499
// @LINE:498
// @LINE:497
// @LINE:496
// @LINE:495
// @LINE:494
// @LINE:493
// @LINE:492
// @LINE:491
// @LINE:490
// @LINE:489
// @LINE:488
class ReverseCollections {
    

// @LINE:553
def changeOwnerDatasetOnly(ds_id:UUID, userId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("ds_id", ds_id) + "/changeOwnerDataset/" + implicitly[PathBindable[UUID]].unbind("userId", userId))
}
                                                

// @LINE:511
def uploadZipToSpace(spaceId:Option[String] = None): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/uploadZipToSpace" + queryString(List(if(spaceId == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("spaceId", spaceId)))))
}
                                                

// @LINE:548
def getParentCollections(coll_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/getParentCollections")
}
                                                

// @LINE:492
def createCollection(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections")
}
                                                

// @LINE:491
def listPossibleParents(currentCollectionId:String, title:Option[String] = None, date:Option[String] = None, limit:Int = 12, exact:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/possibleParents" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("currentCollectionId", currentCollectionId)), if(title == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("title", title)), if(date == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("date", date)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(exact == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("exact", exact)))))
}
                                                

// @LINE:530
def reindex(coll_id:UUID, recursive:Boolean = true): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/reindex" + queryString(List(if(recursive == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("recursive", recursive)))))
}
                                                

// @LINE:550
def removeFromSpaceAllowed(coll_id:UUID, space_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/removeFromSpaceAllowed/" + implicitly[PathBindable[UUID]].unbind("space_id", space_id))
}
                                                

// @LINE:549
def attachSubCollection(coll_id:UUID, sub_coll_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/addSubCollection/" + implicitly[PathBindable[UUID]].unbind("sub_coll_id", sub_coll_id))
}
                                                

// @LINE:544
def createCollectionWithParent(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/newCollectionWithParent")
}
                                                

// @LINE:545
def getChildCollectionIds(coll_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/getChildCollectionIds")
}
                                                

// @LINE:497
def listCollectionsInTrash(limit:Int = 12): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/listTrash" + queryString(List(if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:516
def moveChildCollection(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/moveChildCollection")
}
                                                

// @LINE:518
def moveDatasetToNewCollection(oldCollection:String, dataset:String, newCollection:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[String]].unbind("oldCollection", dynamicString(oldCollection)) + "/moveDataset/" + implicitly[PathBindable[String]].unbind("dataset", dynamicString(dataset)) + "/newCollection/" + implicitly[PathBindable[String]].unbind("newCollection", dynamicString(newCollection)))
}
                                                

// @LINE:499
def clearOldCollectionsTrash(days:Int = 30): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/collections/clearOldCollectionsTrash" + queryString(List(if(days == 30) None else Some(implicitly[QueryStringBindable[Int]].unbind("days", days)))))
}
                                                

// @LINE:538
def updateCollectionName(coll_id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/title")
}
                                                

// @LINE:539
def updateCollectionDescription(coll_id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/description")
}
                                                

// @LINE:520
// @LINE:488
def list(title:Option[String] = None, date:Option[String] = None, limit:Int = 12, exact:Boolean = false): Call = {
   (title: @unchecked, date: @unchecked, limit: @unchecked, exact: @unchecked) match {
// @LINE:488
case (title, date, limit, exact) if true => Call("GET", _prefix + { _defaultPrefix } + "api/collections" + queryString(List(if(title == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("title", title)), if(date == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("date", date)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(exact == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("exact", exact)))))
                                                        
// @LINE:520
case (title, date, limit, exact) if true => Call("GET", _prefix + { _defaultPrefix } + "api/collections/list" + queryString(List(if(title == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("title", title)), if(date == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("date", date)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(exact == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("exact", exact)))))
                                                        
   }
}
                                                

// @LINE:541
def removeSubCollection(coll_id:UUID, sub_coll_id:UUID, ignoreNotFound:String = "True"): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/removeSubCollection/" + implicitly[PathBindable[UUID]].unbind("sub_coll_id", sub_coll_id) + queryString(List(if(ignoreNotFound == "True") None else Some(implicitly[QueryStringBindable[String]].unbind("ignoreNotFound", ignoreNotFound)))))
}
                                                

// @LINE:543
def unsetRootSpace(coll_id:UUID, spaceId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/unsetRootFlag/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId))
}
                                                

// @LINE:527
// @LINE:526
def removeDataset(coll_id:UUID, ds_id:UUID, ignoreNotFound:String): Call = {
   (coll_id: @unchecked, ds_id: @unchecked, ignoreNotFound: @unchecked) match {
// @LINE:526
case (coll_id, ds_id, ignoreNotFound) if true => Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/datasetsRemove/" + implicitly[PathBindable[UUID]].unbind("ds_id", ds_id) + "/" + implicitly[PathBindable[String]].unbind("ignoreNotFound", dynamicString(ignoreNotFound)))
                                                        
// @LINE:527
case (coll_id, ds_id, ignoreNotFound) if true => Call("DELETE", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/datasets/" + implicitly[PathBindable[UUID]].unbind("ds_id", ds_id) + queryString(List(if(ignoreNotFound == "True") None else Some(implicitly[QueryStringBindable[String]].unbind("ignoreNotFound", ignoreNotFound)))))
                                                        
   }
}
                                                

// @LINE:536
def attachPreview(c_id:UUID, p_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("c_id", c_id) + "/previews/" + implicitly[PathBindable[UUID]].unbind("p_id", p_id))
}
                                                

// @LINE:517
def removeCollectionAndContents(collectionId:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("collectionId", collectionId) + "/deleteCollectionAndContents")
}
                                                

// @LINE:546
def getChildCollections(coll_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/getChildCollections")
}
                                                

// @LINE:500
def restoreCollection(collectionId:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/collections/restore/" + implicitly[PathBindable[UUID]].unbind("collectionId", collectionId))
}
                                                

// @LINE:551
def changeOwner(coll_id:UUID, userId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/changeOwner/" + implicitly[PathBindable[UUID]].unbind("userId", userId))
}
                                                

// @LINE:533
// @LINE:529
def removeCollection(coll_id:UUID): Call = {
   (coll_id: @unchecked) match {
// @LINE:529
case (coll_id) if true => Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/remove")
                                                        
// @LINE:533
case (coll_id) if true => Call("DELETE", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id))
                                                        
   }
}
                                                

// @LINE:495
def getAllCollections(limit:Int = 0, showAll:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/allCollections" + queryString(List(if(limit == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(showAll == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("showAll", showAll)))))
}
                                                

// @LINE:494
def getTopLevelCollections(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/topLevelCollections")
}
                                                

// @LINE:489
def listCanEdit(title:Option[String] = None, date:Option[String] = None, limit:Int = 12, exact:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/canEdit" + queryString(List(if(title == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("title", title)), if(date == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("date", date)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(exact == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("exact", exact)))))
}
                                                

// @LINE:537
def getCollection(coll_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id))
}
                                                

// @LINE:542
def setRootSpace(coll_id:UUID, spaceId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/rootFlag/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId))
}
                                                

// @LINE:496
def download(id:UUID, compression:Int = -1): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/download" + queryString(List(if(compression == -1) None else Some(implicitly[QueryStringBindable[Int]].unbind("compression", compression)))))
}
                                                

// @LINE:493
def getRootCollections(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/rootCollections")
}
                                                

// @LINE:554
def changeOwnerFileOnly(file_id:UUID, userId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id) + "/changeOwnerFile/" + implicitly[PathBindable[UUID]].unbind("userId", userId))
}
                                                

// @LINE:524
def attachDataset(coll_id:UUID, ds_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/datasets/" + implicitly[PathBindable[UUID]].unbind("ds_id", ds_id))
}
                                                

// @LINE:490
def addDatasetToCollectionOptions(ds_id:UUID, title:Option[String] = None, date:Option[String] = None, limit:Int = 12, exact:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/datasetPossibleParents/" + implicitly[PathBindable[UUID]].unbind("ds_id", ds_id) + queryString(List(if(title == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("title", title)), if(date == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("date", date)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(exact == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("exact", exact)))))
}
                                                

// @LINE:552
def changeOwnerCollectionOnly(coll_id:UUID, userId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/changeOwnerCollection/" + implicitly[PathBindable[UUID]].unbind("userId", userId))
}
                                                

// @LINE:535
def deleteCollectionAndContents(coll_id:UUID, userid:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/deleteContents/" + implicitly[PathBindable[UUID]].unbind("userid", userid))
}
                                                

// @LINE:547
def getParentCollectionIds(coll_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/getParentCollectionIds")
}
                                                

// @LINE:498
def emptyTrash(): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/collections/emptyTrash")
}
                                                

// @LINE:522
def unfollow(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/unfollow")
}
                                                

// @LINE:521
def follow(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/follow")
}
                                                
    
}
                          

// @LINE:960
// @LINE:631
// @LINE:630
// @LINE:629
// @LINE:628
// @LINE:627
// @LINE:626
// @LINE:624
// @LINE:623
// @LINE:622
// @LINE:621
// @LINE:620
// @LINE:619
// @LINE:618
// @LINE:617
// @LINE:616
// @LINE:615
// @LINE:614
// @LINE:613
// @LINE:612
// @LINE:611
// @LINE:610
// @LINE:609
// @LINE:608
// @LINE:607
// @LINE:606
// @LINE:605
// @LINE:604
// @LINE:603
// @LINE:602
// @LINE:601
// @LINE:600
// @LINE:599
// @LINE:598
// @LINE:597
// @LINE:596
// @LINE:595
// @LINE:594
// @LINE:593
// @LINE:592
// @LINE:591
// @LINE:590
// @LINE:589
// @LINE:588
// @LINE:587
// @LINE:585
// @LINE:584
// @LINE:580
// @LINE:578
// @LINE:577
// @LINE:576
// @LINE:575
// @LINE:574
// @LINE:573
// @LINE:572
// @LINE:571
// @LINE:570
// @LINE:569
// @LINE:568
// @LINE:566
// @LINE:565
// @LINE:564
// @LINE:563
// @LINE:562
// @LINE:561
// @LINE:560
// @LINE:532
// @LINE:523
// @LINE:448
// @LINE:447
// @LINE:319
// @LINE:318
// @LINE:317
class ReverseDatasets {
    

// @LINE:624
def detachAndDeleteDataset(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/detachdelete")
}
                                                

// @LINE:588
def getRDFURLsForDataset(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/getRDFURLsForDataset/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:589
def getRDFUserMetadata(id:UUID, mappingNum:String = "1"): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/rdfUserMetadata/" + implicitly[PathBindable[UUID]].unbind("id", id) + queryString(List(if(mappingNum == "1") None else Some(implicitly[QueryStringBindable[String]].unbind("mappingNum", mappingNum)))))
}
                                                

// @LINE:574
def listDatasetsInTrash(limit:Int = 12): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/listTrash" + queryString(List(if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:631
def hasAttachedVocabulary(datasetId:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/hasVocabulary/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId))
}
                                                

// @LINE:620
def getPreviews(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/getPreviews")
}
                                                

// @LINE:603
def reindex(id:UUID, recursive:Boolean = true): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/reindex" + queryString(List(if(recursive == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("recursive", recursive)))))
}
                                                

// @LINE:572
def searchDatasetsGeneralMetadata(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/searchmetadata")
}
                                                

// @LINE:606
def removeTag(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/removeTag")
}
                                                

// @LINE:618
def updateLicense(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/license")
}
                                                

// @LINE:562
def listMoveFileToDataset(file_id:UUID, title:Option[String] = None, limit:Int = 12, exact:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/moveFileToDataset" + queryString(List(Some(implicitly[QueryStringBindable[UUID]].unbind("file_id", file_id)), if(title == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("title", title)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(exact == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("exact", exact)))))
}
                                                

// @LINE:621
def attachExistingFile(ds_id:UUID, file_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("ds_id", ds_id) + "/files/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id))
}
                                                

// @LINE:598
// @LINE:597
def datasetAllFilesList(id:UUID, max:Option[Int] = None): Call = {
   (id: @unchecked, max: @unchecked) match {
// @LINE:597
case (id, max) if true => Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/listAllFiles" + queryString(List(if(max == None) None else Some(implicitly[QueryStringBindable[Option[Int]]].unbind("max", max)))))
                                                        
// @LINE:598
case (id, max) if true => Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/files" + queryString(List(if(max == None) None else Some(implicitly[QueryStringBindable[Option[Int]]].unbind("max", max)))))
                                                        
   }
}
                                                

// @LINE:319
def removeMetadataJsonLD(id:UUID, extractor:Option[String] = None): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata.jsonld" + queryString(List(if(extractor == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("extractor", extractor)))))
}
                                                

// @LINE:587
// @LINE:578
def detachFile(ds_id:UUID, file_id:UUID, ignoreNotFound:String): Call = {
   (ds_id: @unchecked, file_id: @unchecked, ignoreNotFound: @unchecked) match {
// @LINE:578
case (ds_id, file_id, ignoreNotFound) if true => Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("ds_id", ds_id) + "/filesRemove/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id) + "/" + implicitly[PathBindable[String]].unbind("ignoreNotFound", dynamicString(ignoreNotFound)))
                                                        
// @LINE:587
case (ds_id, file_id, ignoreNotFound) if true => Call("DELETE", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("ds_id", ds_id) + "/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id) + queryString(List(if(ignoreNotFound == "True") None else Some(implicitly[QueryStringBindable[String]].unbind("ignoreNotFound", ignoreNotFound)))))
                                                        
   }
}
                                                

// @LINE:627
def addFileEvent(id:UUID, inFolder:Boolean, fileCount:Int): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/addFileEvent" + queryString(List(Some(implicitly[QueryStringBindable[Boolean]].unbind("inFolder", inFolder)), Some(implicitly[QueryStringBindable[Int]].unbind("fileCount", fileCount)))))
}
                                                

// @LINE:570
def attachMultipleFiles(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/attachmultiple")
}
                                                

// @LINE:616
def moveCreator(id:UUID, creator:String, newPos:Int): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/creator/reorder" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("creator", creator)), Some(implicitly[QueryStringBindable[Int]].unbind("newPos", newPos)))))
}
                                                

// @LINE:560
def list(title:Option[String] = None, date:Option[String] = None, limit:Int = 12, exact:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets" + queryString(List(if(title == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("title", title)), if(date == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("date", date)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(exact == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("exact", exact)))))
}
                                                

// @LINE:448
def dumpDatasetGroupings(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/dumpDatasetGroupings")
}
                                                

// @LINE:619
def isBeingProcessed(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/isBeingProcessed")
}
                                                

// @LINE:590
def getMetadataDefinitions(id:UUID, space:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata" + queryString(List(if(space == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("space", space)))))
}
                                                

// @LINE:960
// @LINE:532
// @LINE:523
def listInCollection(coll_id:UUID): Call = {
   (coll_id: @unchecked) match {
// @LINE:523
case (coll_id) if true => Call("GET", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/datasets")
                                                        
// @LINE:532
case (coll_id) if true => Call("GET", _prefix + { _defaultPrefix } + "api/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/getDatasets")
                                                        
// @LINE:960
case (coll_id) if true => Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/datasets")
                                                        
   }
}
                                                

// @LINE:614
def addCreator(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/creator")
}
                                                

// @LINE:577
// @LINE:566
def restoreDataset(id:UUID): Call = {
   (id: @unchecked) match {
// @LINE:566
case (id) if true => Call("PUT", _prefix + { _defaultPrefix } + "api/datasets/restore/" + implicitly[PathBindable[UUID]].unbind("id", id))
                                                        
// @LINE:577
case (id) if true => Call("PUT", _prefix + { _defaultPrefix } + "api/datasets/restore/" + implicitly[PathBindable[UUID]].unbind("id", id))
                                                        
   }
}
                                                

// @LINE:580
def moveFileBetweenDatasets(datasetId:UUID, toDataset:UUID, fileId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId) + "/moveBetweenDatasets/" + implicitly[PathBindable[UUID]].unbind("toDataset", toDataset) + "/" + implicitly[PathBindable[UUID]].unbind("fileId", fileId))
}
                                                

// @LINE:628
def users(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/users")
}
                                                

// @LINE:611
// @LINE:609
def removeTags(id:UUID): Call = {
   (id: @unchecked) match {
// @LINE:609
case (id) if true => Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags/remove")
                                                        
// @LINE:611
case (id) if true => Call("DELETE", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags")
                                                        
   }
}
                                                

// @LINE:564
def createEmptyVocabularyForDataset(datasetId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/newVocabulary/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId))
}
                                                

// @LINE:591
def addMetadata(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata")
}
                                                

// @LINE:599
def uploadToDatasetFile(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/files")
}
                                                

// @LINE:610
def removeAllTags(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags/remove_all")
}
                                                

// @LINE:585
def copyDatasetToSpaceNotAuthor(datasetId:UUID, spaceId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId) + "/copyDatasetToSpaceNotAuthor/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId))
}
                                                

// @LINE:584
def copyDatasetToSpace(datasetId:UUID, spaceId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId) + "/copyDatasetToSpace/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId))
}
                                                

// @LINE:622
def get(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:563
def createDataset(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets")
}
                                                

// @LINE:629
def changeOwner(datasetId:UUID, userId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId) + "/changeOwner/" + implicitly[PathBindable[UUID]].unbind("userId", userId))
}
                                                

// @LINE:571
def searchDatasetsUserMetadata(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/searchusermetadata")
}
                                                

// @LINE:608
def addTags(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags")
}
                                                

// @LINE:615
def removeCreator(id:UUID, creator:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/creator/remove" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("creator", creator)))))
}
                                                

// @LINE:592
def addUserMetadata(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/usermetadata")
}
                                                

// @LINE:561
def listCanEdit(title:Option[String] = None, date:Option[String] = None, limit:Int = 12, exact:Boolean = false): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/canEdit" + queryString(List(if(title == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("title", title)), if(date == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("date", date)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)), if(exact == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("exact", exact)))))
}
                                                

// @LINE:569
def createEmptyDataset(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/createempty")
}
                                                

// @LINE:607
def getTags(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/tags")
}
                                                

// @LINE:612
def updateName(id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/title")
}
                                                

// @LINE:602
def comment(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/comment")
}
                                                

// @LINE:601
def download(id:UUID, compression:Int = -1): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/download" + queryString(List(if(compression == -1) None else Some(implicitly[QueryStringBindable[Int]].unbind("compression", compression)))))
}
                                                

// @LINE:600
def uploadToDatasetJSON(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/urls")
}
                                                

// @LINE:565
def markDatasetAsTrash(id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/datasets/trash/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:596
def datasetFilesList(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/listFiles")
}
                                                

// @LINE:317
def addMetadataJsonLD(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata.jsonld")
}
                                                

// @LINE:626
def updateAccess(id:UUID, aceess:String = "PRIVATE"): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/access" + queryString(List(if(aceess == "PRIVATE") None else Some(implicitly[QueryStringBindable[String]].unbind("aceess", aceess)))))
}
                                                

// @LINE:594
def getXMLMetadataJSON(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/xmlmetadatajson")
}
                                                

// @LINE:613
def updateDescription(id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/description")
}
                                                

// @LINE:617
def updateInformation(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/editing")
}
                                                

// @LINE:623
def deleteDataset(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:575
// @LINE:568
def emptyTrash(): Call = {
   () match {
// @LINE:568
case () if true => Call("DELETE", _prefix + { _defaultPrefix } + "api/datasets/emptytrash")
                                                        
// @LINE:575
case () if true => Call("DELETE", _prefix + { _defaultPrefix } + "api/datasets/emptyTrash")
                                                        
   }
}
                                                

// @LINE:605
def unfollow(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/unfollow")
}
                                                

// @LINE:318
def getMetadataJsonLD(id:UUID, extractor:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata.jsonld" + queryString(List(if(extractor == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("extractor", extractor)))))
}
                                                

// @LINE:604
def follow(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/follow")
}
                                                

// @LINE:593
def getTechnicalMetadataJSON(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/technicalmetadatajson")
}
                                                

// @LINE:447
def dumpDatasetsMetadata(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/dumpDatasetsMd")
}
                                                

// @LINE:630
def moveDataset(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/moveCollection")
}
                                                

// @LINE:576
def clearOldDatasetsTrash(days:Int = 30): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/datasets/clearOldDatasetsTrash" + queryString(List(if(days == 30) None else Some(implicitly[QueryStringBindable[Int]].unbind("days", days)))))
}
                                                

// @LINE:573
def listOutsideCollection(coll_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/listOutsideCollection/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id))
}
                                                

// @LINE:595
def getUserMetadataJSON(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/usermetadatajson")
}
                                                
    
}
                          

// @LINE:751
// @LINE:750
// @LINE:749
// @LINE:748
// @LINE:747
// @LINE:746
// @LINE:745
class ReverseSelected {
    

// @LINE:750
def clearAll(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/selected/clear")
}
                                                

// @LINE:749
def downloadAll(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/selected/files")
}
                                                

// @LINE:747
def remove(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/selected/remove")
}
                                                

// @LINE:748
def deleteAll(): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/selected/files")
}
                                                

// @LINE:746
def add(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/selected")
}
                                                

// @LINE:745
def get(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/selected")
}
                                                

// @LINE:751
def tagAll(tags:List[String]): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/selected/tag" + queryString(List(Some(implicitly[QueryStringBindable[List[String]]].unbind("tags", tags)))))
}
                                                
    
}
                          

// @LINE:426
// @LINE:425
// @LINE:424
// @LINE:423
// @LINE:422
// @LINE:421
// @LINE:420
// @LINE:418
// @LINE:417
// @LINE:416
// @LINE:415
// @LINE:413
// @LINE:412
// @LINE:410
// @LINE:408
// @LINE:407
class ReverseExtractions {
    

// @LINE:410
def addExtractorInfo(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/extractors")
}
                                                

// @LINE:416
def getExtractorServersIP(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/extractions/servers_ips")
}
                                                

// @LINE:415
def getDTSRequests(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/extractions/requests")
}
                                                

// @LINE:422
def multipleUploadByURL(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/extractions/multiple_uploadby_url")
}
                                                

// @LINE:425
def fetch(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/extractions/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadata")
}
                                                

// @LINE:423
def uploadExtract(showPreviews:String = "DatasetLevel", extract:Boolean = true): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/extractions/upload_file" + queryString(List(if(showPreviews == "DatasetLevel") None else Some(implicitly[QueryStringBindable[String]].unbind("showPreviews", showPreviews)), if(extract == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("extract", extract)))))
}
                                                

// @LINE:408
def getExtractorInfo(name:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/extractors/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
}
                                                

// @LINE:412
def submitFileToExtractor(file_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/files/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id) + "/extractions")
}
                                                

// @LINE:424
def checkExtractorsStatus(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/extractions/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/status")
}
                                                

// @LINE:413
def submitDatasetToExtractor(ds_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/datasets/" + implicitly[PathBindable[UUID]].unbind("ds_id", ds_id) + "/extractions")
}
                                                

// @LINE:418
def getExtractorInputTypes(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/extractions/supported_input_types")
}
                                                

// @LINE:407
def listExtractors(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/extractors")
}
                                                

// @LINE:420
def getExtractorDetails(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/extractions/extractors_details")
}
                                                

// @LINE:421
def uploadByURL(extract:Boolean = true): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/extractions/upload_url" + queryString(List(if(extract == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("extract", extract)))))
}
                                                

// @LINE:426
def checkExtractionsStatuses(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/extractions/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/statuses")
}
                                                

// @LINE:417
def getExtractorNames(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/extractions/extractors_names")
}
                                                
    
}
                          

// @LINE:840
class ReverseEvents {
    

// @LINE:840
def sendExceptionEmail(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "sendEmail")
}
                                                
    
}
                          

// @LINE:715
// @LINE:714
// @LINE:713
// @LINE:712
// @LINE:711
// @LINE:710
// @LINE:709
// @LINE:708
// @LINE:707
// @LINE:706
// @LINE:705
// @LINE:704
// @LINE:703
// @LINE:702
// @LINE:701
// @LINE:700
// @LINE:699
// @LINE:698
// @LINE:697
// @LINE:696
// @LINE:695
// @LINE:694
// @LINE:693
// @LINE:692
// @LINE:691
// @LINE:690
// @LINE:689
// @LINE:688
// @LINE:687
// @LINE:686
class ReverseGeostreams {
    

// @LINE:708
def getStream(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/streams/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:692
def binDatapoints(time:String, depth:Double, raw:Boolean = false, since:Option[String] = None, until:Option[String] = None, geocode:Option[String] = None, stream_id:Option[String] = None, sensor_id:Option[String] = None, sources:List[String] = List.empty, attributes:List[String] = List.empty): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/datapoints/bin/" + implicitly[PathBindable[String]].unbind("time", dynamicString(time)) + "/" + implicitly[PathBindable[Double]].unbind("depth", depth) + queryString(List(if(raw == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("raw", raw)), if(since == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("since", since)), if(until == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("until", until)), if(geocode == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geocode", geocode)), if(stream_id == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("stream_id", stream_id)), if(sensor_id == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("sensor_id", sensor_id)), if(sources == List.empty) None else Some(implicitly[QueryStringBindable[List[String]]].unbind("sources", sources)), if(attributes == List.empty) None else Some(implicitly[QueryStringBindable[List[String]]].unbind("attributes", attributes)))))
}
                                                

// @LINE:707
// @LINE:698
def updateStatisticsStreamSensor(): Call = {
   () match {
// @LINE:698
case () if true => Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/sensors/update")
                                                        
// @LINE:707
case () if true => Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/streams/update")
                                                        
   }
}
                                                

// @LINE:694
def cacheListAction(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/cache")
}
                                                

// @LINE:686
def addDatapoint(invalidateCache:Boolean = true): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/geostreams/datapoints" + queryString(List(if(invalidateCache == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("invalidateCache", invalidateCache)))))
}
                                                

// @LINE:702
def getSensorStreams(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/sensors/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)) + "/streams")
}
                                                

// @LINE:704
def searchSensors(geocode:Option[String] = None, sensor_name:Option[String] = None, geojson:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/sensors" + queryString(List(if(geocode == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geocode", geocode)), if(sensor_name == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("sensor_name", sensor_name)), if(geojson == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geojson", geojson)))))
}
                                                

// @LINE:701
def getSensorStatistics(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/sensors/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)) + "/stats")
}
                                                

// @LINE:706
def createStream(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/geostreams/streams")
}
                                                

// @LINE:687
def addDatapoints(invalidateCache:Boolean = true): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/geostreams/datapoints/bulk" + queryString(List(if(invalidateCache == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("invalidateCache", invalidateCache)))))
}
                                                

// @LINE:696
def cacheFetchAction(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/cache/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:715
def getConfig(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/config")
}
                                                

// @LINE:691
// @LINE:690
// @LINE:689
def searchDatapoints(operator:String, since:Option[String] = None, until:Option[String] = None, geocode:Option[String] = None, stream_id:Option[String] = None, sensor_id:Option[String] = None, sources:List[String] = List.empty, attributes:List[String] = List.empty, format:String = "json", semi:Option[String], onlyCount:Boolean = false, window_start:Option[String], window_end:Option[String], binning:String = "semi", geojson:Option[String] = None): Call = {
   (operator: @unchecked, since: @unchecked, until: @unchecked, geocode: @unchecked, stream_id: @unchecked, sensor_id: @unchecked, sources: @unchecked, attributes: @unchecked, format: @unchecked, semi: @unchecked, onlyCount: @unchecked, window_start: @unchecked, window_end: @unchecked, binning: @unchecked, geojson: @unchecked) match {
// @LINE:689
case (operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson) if operator == "" && window_start == None && window_end == None => Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/datapoints" + queryString(List(if(since == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("since", since)), if(until == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("until", until)), if(geocode == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geocode", geocode)), if(stream_id == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("stream_id", stream_id)), if(sensor_id == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("sensor_id", sensor_id)), if(sources == List.empty) None else Some(implicitly[QueryStringBindable[List[String]]].unbind("sources", sources)), if(attributes == List.empty) None else Some(implicitly[QueryStringBindable[List[String]]].unbind("attributes", attributes)), if(format == "json") None else Some(implicitly[QueryStringBindable[String]].unbind("format", format)), Some(implicitly[QueryStringBindable[Option[String]]].unbind("semi", semi)), if(onlyCount == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("onlyCount", onlyCount)), if(binning == "semi") None else Some(implicitly[QueryStringBindable[String]].unbind("binning", binning)), if(geojson == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geojson", geojson)))))
                                                        
// @LINE:690
case (operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson) if operator == "averages" && window_start == None && window_end == None => Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/datapoints/averages" + queryString(List(if(since == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("since", since)), if(until == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("until", until)), if(geocode == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geocode", geocode)), if(stream_id == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("stream_id", stream_id)), if(sensor_id == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("sensor_id", sensor_id)), if(sources == List.empty) None else Some(implicitly[QueryStringBindable[List[String]]].unbind("sources", sources)), if(attributes == List.empty) None else Some(implicitly[QueryStringBindable[List[String]]].unbind("attributes", attributes)), if(format == "json") None else Some(implicitly[QueryStringBindable[String]].unbind("format", format)), Some(implicitly[QueryStringBindable[Option[String]]].unbind("semi", semi)), if(onlyCount == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("onlyCount", onlyCount)), if(binning == "semi") None else Some(implicitly[QueryStringBindable[String]].unbind("binning", binning)), if(geojson == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geojson", geojson)))))
                                                        
// @LINE:691
case (operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson) if operator == "trends" => Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/datapoints/trends" + queryString(List(if(since == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("since", since)), if(until == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("until", until)), if(geocode == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geocode", geocode)), if(stream_id == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("stream_id", stream_id)), if(sensor_id == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("sensor_id", sensor_id)), if(sources == List.empty) None else Some(implicitly[QueryStringBindable[List[String]]].unbind("sources", sources)), if(attributes == List.empty) None else Some(implicitly[QueryStringBindable[List[String]]].unbind("attributes", attributes)), if(format == "json") None else Some(implicitly[QueryStringBindable[String]].unbind("format", format)), Some(implicitly[QueryStringBindable[Option[String]]].unbind("semi", semi)), if(onlyCount == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("onlyCount", onlyCount)), if(window_start == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("window_start", window_start)), if(window_end == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("window_end", window_end)), if(binning == "semi") None else Some(implicitly[QueryStringBindable[String]].unbind("binning", binning)), if(geojson == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geojson", geojson)))))
                                                        
   }
}
                                                

// @LINE:714
def counts(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/counts")
}
                                                

// @LINE:709
def patchStreamMetadata(id:String): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/geostreams/streams/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:697
def createSensor(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/geostreams/sensors")
}
                                                

// @LINE:703
def updateStatisticsSensor(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/sensors/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)) + "/update")
}
                                                

// @LINE:713
def deleteAll(): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/geostreams/dropall")
}
                                                

// @LINE:688
def deleteDatapoint(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/geostreams/datapoints/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:699
def getSensor(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/sensors/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:711
def searchStreams(geocode:Option[String] = None, stream_name:Option[String] = None, geojson:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/streams" + queryString(List(if(geocode == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geocode", geocode)), if(stream_name == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("stream_name", stream_name)), if(geojson == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("geojson", geojson)))))
}
                                                

// @LINE:705
def deleteSensor(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/geostreams/sensors/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:712
def deleteStream(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/geostreams/streams/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:700
def updateSensorMetadata(id:String): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/geostreams/sensors/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:695
def cacheInvalidateAction(sensor_id:Option[String] = None, stream_id:Option[String] = None): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/cache/invalidate" + queryString(List(if(sensor_id == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("sensor_id", sensor_id)), if(stream_id == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("stream_id", stream_id)))))
}
                                                

// @LINE:710
def updateStatisticsStream(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/streams/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)) + "/update")
}
                                                

// @LINE:693
def getDatapoint(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/geostreams/datapoints/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                
    
}
                          

// @LINE:978
class ReverseFullTree {
    

// @LINE:978
def getChildrenOfNode(nodeId:Option[String], nodeType:String, role:Option[String]): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/fulltree/getChildrenOfNode" + queryString(List(Some(implicitly[QueryStringBindable[Option[String]]].unbind("nodeId", nodeId)), Some(implicitly[QueryStringBindable[String]].unbind("nodeType", nodeType)), Some(implicitly[QueryStringBindable[Option[String]]].unbind("role", role)))))
}
                                                
    
}
                          

// @LINE:836
// @LINE:835
// @LINE:833
// @LINE:832
// @LINE:830
// @LINE:829
// @LINE:828
// @LINE:820
// @LINE:790
class ReverseCurationObjects {
    

// @LINE:828
def findMatchmakingRepositories(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/matchmaker")
}
                                                

// @LINE:790
def getCurationObjectOre(curationId:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/curations/" + implicitly[PathBindable[UUID]].unbind("curationId", curationId) + "/ore")
}
                                                

// @LINE:836
def getMetadataDefinitions(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/metadataDefinitions")
}
                                                

// @LINE:832
def deleteCurationFile(id:UUID, parentId:UUID, curationFileId:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/files/" + implicitly[PathBindable[UUID]].unbind("curationFileId", curationFileId) + queryString(List(Some(implicitly[QueryStringBindable[UUID]].unbind("parentId", parentId)))))
}
                                                

// @LINE:835
def getMetadataDefinitionsByFile(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/getMetadataDefinitionsByFile")
}
                                                

// @LINE:820
def retractCurationObject(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "spaces/curations/retract/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:830
def savePublishedObject(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/status")
}
                                                

// @LINE:829
def getCurationFiles(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/curationFile")
}
                                                

// @LINE:833
def deleteCurationFolder(id:UUID, parentId:UUID, curationFolderId:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "spaces/curations/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/folders/" + implicitly[PathBindable[UUID]].unbind("curationFolderId", curationFolderId) + queryString(List(Some(implicitly[QueryStringBindable[UUID]].unbind("parentId", parentId)))))
}
                                                
    
}
                          

// @LINE:731
// @LINE:730
// @LINE:729
// @LINE:728
// @LINE:727
class ReverseSensors {
    

// @LINE:728
def add(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/sensors")
}
                                                

// @LINE:727
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/sensors")
}
                                                

// @LINE:731
def delete(id:String): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/sensors/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                

// @LINE:730
def search(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/sensors/search")
}
                                                

// @LINE:729
def get(id:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/sensors/" + implicitly[PathBindable[String]].unbind("id", dynamicString(id)))
}
                                                
    
}
                          

// @LINE:769
// @LINE:768
// @LINE:681
// @LINE:680
// @LINE:679
class ReverseSearch {
    

// @LINE:679
def searchJson(query:String = "", grouping:String = "AND", from:Option[Int], size:Option[Int]): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/search/json" + queryString(List(if(query == "") None else Some(implicitly[QueryStringBindable[String]].unbind("query", query)), if(grouping == "AND") None else Some(implicitly[QueryStringBindable[String]].unbind("grouping", grouping)), Some(implicitly[QueryStringBindable[Option[Int]]].unbind("from", from)), Some(implicitly[QueryStringBindable[Option[Int]]].unbind("size", size)))))
}
                                                

// @LINE:680
def searchMultimediaIndex(section_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/search/multimediasearch" + queryString(List(Some(implicitly[QueryStringBindable[UUID]].unbind("section_id", section_id)))))
}
                                                

// @LINE:769
def querySPARQL(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/sparqlquery")
}
                                                

// @LINE:768
// @LINE:681
def search(query:String = ""): Call = {
   (query: @unchecked) match {
// @LINE:681
case (query) if true => Call("GET", _prefix + { _defaultPrefix } + "api/search" + queryString(List(if(query == "") None else Some(implicitly[QueryStringBindable[String]].unbind("query", query)))))
                                                        
// @LINE:768
case (query) if true => Call("GET", _prefix + { _defaultPrefix } + "api/search" + queryString(List(if(query == "") None else Some(implicitly[QueryStringBindable[String]].unbind("query", query)))))
                                                        
   }
}
                                                
    
}
                          

// @LINE:966
// @LINE:965
// @LINE:964
// @LINE:963
// @LINE:962
// @LINE:961
// @LINE:958
// @LINE:957
// @LINE:955
// @LINE:954
// @LINE:953
// @LINE:952
// @LINE:951
// @LINE:950
// @LINE:949
// @LINE:948
// @LINE:947
// @LINE:946
// @LINE:945
// @LINE:944
// @LINE:943
// @LINE:942
// @LINE:941
// @LINE:940
// @LINE:939
// @LINE:938
// @LINE:936
// @LINE:919
// @LINE:917
// @LINE:916
// @LINE:915
// @LINE:914
// @LINE:912
// @LINE:911
// @LINE:909
// @LINE:908
// @LINE:906
// @LINE:905
// @LINE:901
// @LINE:900
class ReverseT2C2 {
    

// @LINE:914
def attachVocabToDataset(vocab_id:UUID, dataset_id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "t2c2/templates/" + implicitly[PathBindable[UUID]].unbind("vocab_id", vocab_id) + "/attachToDataset/" + implicitly[PathBindable[UUID]].unbind("dataset_id", dataset_id))
}
                                                

// @LINE:919
def listCollectionsCanEdit(id:UUID, limit:Integer = 0): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/collectionsCanEdit" + queryString(List(if(limit == 0) None else Some(implicitly[QueryStringBindable[Integer]].unbind("limit", limit)))))
}
                                                

// @LINE:963
def getKeysValuesFromLastDataset(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/getKeyValuesLastDataset")
}
                                                

// @LINE:951
def getWhoICanUploadFor(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/getUsersICanUploadFor")
}
                                                

// @LINE:908
// @LINE:900
def listMyDatasets(limit:Int = 20): Call = {
   (limit: @unchecked) match {
// @LINE:900
case (limit) if true => Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/listMyDatasets" + queryString(List(if(limit == 20) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
                                                        
// @LINE:908
case (limit) if true => Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/listMyDatasets" + queryString(List(if(limit == 20) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
                                                        
   }
}
                                                

// @LINE:917
def detachVocabFromFile(vocab_id:UUID, file_id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "t2c2/templates/" + implicitly[PathBindable[UUID]].unbind("vocab_id", vocab_id) + "/detachFromFile/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id))
}
                                                

// @LINE:955
def getNumUsersOfSpace(spaceId:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/getNumUsersOfSpace/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId))
}
                                                

// @LINE:952
def getSpacesOfUser(userId:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/getSpacesOfUser/" + implicitly[PathBindable[UUID]].unbind("userId", userId))
}
                                                

// @LINE:965
def getKeysValuesFromDatasetId(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/getKeyValuesForDatasetId/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:939
def getAllCollectionsWithDatasetIdsAndFiles(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/allCollectionDatasetFiles")
}
                                                

// @LINE:915
def detachVocabFromDataset(vocab_id:UUID, dataset_id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "t2c2/templates/" + implicitly[PathBindable[UUID]].unbind("vocab_id", vocab_id) + "/detachFromDataset/" + implicitly[PathBindable[UUID]].unbind("dataset_id", dataset_id))
}
                                                

// @LINE:958
def getDatasetWithAttachedVocab(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/datasets/getDatasetAndTemplate/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:941
def getLevelOfTree(currentId:Option[String] = None, currentType:String = "collection"): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/getLevelOfTree" + queryString(List(if(currentId == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("currentId", currentId)), if(currentType == "collection") None else Some(implicitly[QueryStringBindable[String]].unbind("currentType", currentType)))))
}
                                                

// @LINE:957
def getTemplateFromLastDataset(limit:Int = 10): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/lastTemplate" + queryString(List(if(limit == 10) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:901
def getVocabulary(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/getExperimentTemplateById/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:944
def getLevelOfTreeNotSharedInSpace(currentId:Option[String] = None, currentType:String = "collection"): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/getLevelOfTreeNotShared" + queryString(List(if(currentId == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("currentId", currentId)), if(currentType == "collection") None else Some(implicitly[QueryStringBindable[String]].unbind("currentType", currentType)))))
}
                                                

// @LINE:966
def getVocabIdNameFromTag(tag:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/getIdNameFromTag/" + implicitly[PathBindable[String]].unbind("tag", dynamicString(tag)))
}
                                                

// @LINE:948
def createEmptyDataset(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "t2c2/datasets/createEmpty")
}
                                                

// @LINE:962
def getAllCollectionsWithDatasetIds(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/allCollectionsWithDatasetIds")
}
                                                

// @LINE:912
def makeTemplatePrivate(id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "t2c2/templates/makePrivate/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:946
def getAllCollectionsForFullTree(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/collectionsForFullTree")
}
                                                

// @LINE:954
def getUsersOfSpace(spaceId:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/getUsersOfSpace/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId))
}
                                                

// @LINE:943
def getLevelOfTreeSharedWithOthers(currentId:Option[String] = None, currentType:String = "collection"): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/getLevelOfTreeSharedWithOthers" + queryString(List(if(currentId == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("currentId", currentId)), if(currentType == "collection") None else Some(implicitly[QueryStringBindable[String]].unbind("currentType", currentType)))))
}
                                                

// @LINE:906
def getVocabByFileId(file_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/getByFileId/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id))
}
                                                

// @LINE:938
def getAllCollectionsOfUser(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/allCollection")
}
                                                

// @LINE:945
def getLevelOfTreeInSpace(currentId:Option[String] = None, currentType:String = "collection", spaceId:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/getLevelOfTreeInSpace" + queryString(List(if(currentId == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("currentId", currentId)), if(currentType == "collection") None else Some(implicitly[QueryStringBindable[String]].unbind("currentType", currentType)), Some(implicitly[QueryStringBindable[String]].unbind("spaceId", spaceId)))))
}
                                                

// @LINE:936
def findYoungestChild(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "t2c2/findYoungestChild")
}
                                                

// @LINE:916
def attachVocabToFile(vocab_id:UUID, file_id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "t2c2/templates/" + implicitly[PathBindable[UUID]].unbind("vocab_id", vocab_id) + "/attachToFile/" + implicitly[PathBindable[UUID]].unbind("file_id", file_id))
}
                                                

// @LINE:947
def getAllCollectionsOfUserNotSharedInSpace(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/allCollectionsNotSharedInSpace")
}
                                                

// @LINE:905
def getVocabByDatasetId(dataset_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/getByDatasetId/" + implicitly[PathBindable[UUID]].unbind("dataset_id", dataset_id))
}
                                                

// @LINE:909
def listMySpaces(limit:Int = 20): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/listMySpaces" + queryString(List(if(limit == 20) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:961
def getDatasetsInCollectionWithColId(coll_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/collections/" + implicitly[PathBindable[UUID]].unbind("coll_id", coll_id) + "/datasetsWithParentColId")
}
                                                

// @LINE:940
def getAllCollectionsForTree(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/collectionsForTree")
}
                                                

// @LINE:953
def getSpacesUserHasAccessTo(userId:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/t2c2/getSpacesUserCanEdit/" + implicitly[PathBindable[UUID]].unbind("userId", userId))
}
                                                

// @LINE:942
def getLevelOfTreeSharedWithMe(currentId:Option[String] = None, currentType:String = "collection"): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/collections/getLevelOfTreeSharedWithMe" + queryString(List(if(currentId == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("currentId", currentId)), if(currentType == "collection") None else Some(implicitly[QueryStringBindable[String]].unbind("currentType", currentType)))))
}
                                                

// @LINE:949
def moveKeysToTermsTemplates(): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "t2c2/templates/moveKeysToTerms")
}
                                                

// @LINE:950
def bulkDelete(): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/t2c2/bulkDelete")
}
                                                

// @LINE:911
def makeTemplatePublic(id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "t2c2/templates/makePublic/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:964
def getKeysValuesFromLastDatasets(limit:Int = 10): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/getKeyValuesForLastDatasets" + queryString(List(if(limit == 10) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                
    
}
                          

// @LINE:770
class ReverseProjects {
    

// @LINE:770
def addproject(project:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/projects/addproject" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("project", project)))))
}
                                                
    
}
                          

// @LINE:913
// @LINE:910
// @LINE:904
// @LINE:903
// @LINE:902
// @LINE:899
// @LINE:898
// @LINE:896
// @LINE:895
// @LINE:893
// @LINE:892
// @LINE:891
// @LINE:861
// @LINE:860
// @LINE:858
// @LINE:857
// @LINE:856
// @LINE:855
// @LINE:854
// @LINE:851
// @LINE:850
// @LINE:849
// @LINE:848
// @LINE:847
class ReverseVocabularies {
    

// @LINE:910
def removeVocabulary(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "t2c2/templates/deleteTemplate/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:898
// @LINE:851
def getPublicVocabularies(): Call = {
   () match {
// @LINE:851
case () if true => Call("GET", _prefix + { _defaultPrefix } + "api/vocabularies/getPublic")
                                                        
// @LINE:898
case () if true => Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/getPublic")
                                                        
   }
}
                                                

// @LINE:903
def getByTag(containsAll:Boolean = false): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "t2c2/templates/findByTag" + queryString(List(if(containsAll == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("containsAll", containsAll)))))
}
                                                

// @LINE:896
def listAll(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/allExperimentTemplates")
}
                                                

// @LINE:904
def getBySingleTag(tag:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/findByTag/" + implicitly[PathBindable[String]].unbind("tag", dynamicString(tag)))
}
                                                

// @LINE:893
// @LINE:892
// @LINE:891
// @LINE:848
def createVocabularyFromJson(isPublic:Boolean = false): Call = {
   (isPublic: @unchecked) match {
// @LINE:848
case (isPublic) if true => Call("POST", _prefix + { _defaultPrefix } + "api/vocabularies/createVocabularyFromJson" + queryString(List(if(isPublic == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("isPublic", isPublic)))))
                                                        
// @LINE:891
case (isPublic) if true => Call("POST", _prefix + { _defaultPrefix } + "t2c2/templates/createExperimentTemplate" + queryString(List(if(isPublic == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("isPublic", isPublic)))))
                                                        
// @LINE:892
case (isPublic) if true => Call("POST", _prefix + { _defaultPrefix } + "t2c2/templates/createExperimentTemplateFromJson" + queryString(List(if(isPublic == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("isPublic", isPublic)))))
                                                        
// @LINE:893
case (isPublic) if true => Call("POST", _prefix + { _defaultPrefix } + "t2c2/templates/createExperimentTemplateDefaultValues" + queryString(List(if(isPublic == false) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("isPublic", isPublic)))))
                                                        
   }
}
                                                

// @LINE:849
def createVocabularyFromForm(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/vocabularies/createVocabulary")
}
                                                

// @LINE:854
def get(vocab_id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/vocabularies/" + implicitly[PathBindable[UUID]].unbind("vocab_id", vocab_id))
}
                                                

// @LINE:858
def getByNameAndAuthor(name:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/vocabularies/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)) + "/getByNameAuthor")
}
                                                

// @LINE:895
// @LINE:847
def list(): Call = {
   () match {
// @LINE:847
case () if true => Call("GET", _prefix + { _defaultPrefix } + "api/vocabularies/list")
                                                        
// @LINE:895
case () if true => Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/listExperimentTemplates")
                                                        
   }
}
                                                

// @LINE:913
// @LINE:857
// @LINE:855
def editVocabulary(vocab_id:UUID): Call = {
   (vocab_id: @unchecked) match {
// @LINE:855
case (vocab_id) if true => Call("PUT", _prefix + { _defaultPrefix } + "api/editVocabulary/" + implicitly[PathBindable[UUID]].unbind("vocab_id", vocab_id))
                                                        
// @LINE:857
case (vocab_id) if true => Call("PUT", _prefix + { _defaultPrefix } + "api/vocabularies/" + implicitly[PathBindable[UUID]].unbind("vocab_id", vocab_id) + "/editVocabulary")
                                                        
// @LINE:913
case (vocab_id) if true => Call("PUT", _prefix + { _defaultPrefix } + "t2c2/templates/editTemplate/" + implicitly[PathBindable[UUID]].unbind("id", vocab_id))
                                                        
   }
}
                                                

// @LINE:899
def getAllTagsOfAllVocabularies(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/allTags")
}
                                                

// @LINE:902
// @LINE:856
def getByName(name:String): Call = {
   (name: @unchecked) match {
// @LINE:856
case (name) if true => Call("GET", _prefix + { _defaultPrefix } + "api/vocabularies/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
                                                        
// @LINE:902
case (name) if true => Call("GET", _prefix + { _defaultPrefix } + "t2c2/templates/getExperimentTemplateByName/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)))
                                                        
   }
}
                                                

// @LINE:860
def addToSpace(vocab_id:UUID, space_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/vocabularies/" + implicitly[PathBindable[UUID]].unbind("vocab_id", vocab_id) + "/addToSpace/" + implicitly[PathBindable[UUID]].unbind("space_id", space_id))
}
                                                

// @LINE:861
def removeFromSpace(vocab_id:UUID, space_id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/vocabuliaries/" + implicitly[PathBindable[UUID]].unbind("vocab_id", vocab_id) + "/removeFromSpace/" + implicitly[PathBindable[UUID]].unbind("space_id", space_id))
}
                                                

// @LINE:850
def getByAuthor(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/vocabularies/getByAuthor")
}
                                                
    
}
                          

// @LINE:765
class ReverseZoomIt {
    

// @LINE:765
def uploadTile(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/tiles")
}
                                                
    
}
                          

// @LINE:767
class ReverseThreeDTexture {
    

// @LINE:767
def uploadTexture(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/3dTextures")
}
                                                
    
}
                          

// @LINE:483
// @LINE:482
// @LINE:481
// @LINE:480
// @LINE:479
// @LINE:477
// @LINE:476
// @LINE:475
// @LINE:474
// @LINE:473
// @LINE:472
// @LINE:471
// @LINE:470
// @LINE:469
// @LINE:468
// @LINE:467
// @LINE:466
// @LINE:465
// @LINE:464
// @LINE:463
// @LINE:462
// @LINE:461
// @LINE:460
class ReverseSpaces {
    

// @LINE:474
def addDatasetToSpace(spaceId:UUID, datasetId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId) + "/addDatasetToSpace/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId))
}
                                                

// @LINE:464
def removeSpace(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:482
def listCollectionsCanEdit(id:UUID, limit:Integer = 0): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/collectionsCanEdit" + queryString(List(if(limit == 0) None else Some(implicitly[QueryStringBindable[Integer]].unbind("limit", limit)))))
}
                                                

// @LINE:466
def removeDataset(spaceId:UUID, datasetId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId) + "/removeDataset/" + implicitly[PathBindable[UUID]].unbind("datasetId", datasetId))
}
                                                

// @LINE:465
def removeCollection(spaceId:UUID, collectionId:UUID, removeDatasets:Boolean): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId) + "/removeCollection/" + implicitly[PathBindable[UUID]].unbind("collectionId", collectionId) + queryString(List(Some(implicitly[QueryStringBindable[Boolean]].unbind("removeDatasets", removeDatasets)))))
}
                                                

// @LINE:463
def createSpace(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces")
}
                                                

// @LINE:470
def updateUsers(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/updateUsers")
}
                                                

// @LINE:468
def copyContentsToNewSpace(spaceId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId) + "/copyContentsToNewSpace")
}
                                                

// @LINE:472
def rejectRequest(id:UUID, user:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/rejectRequest" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("user", user)))))
}
                                                

// @LINE:473
def removeUser(id:UUID, removeUser:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/removeUser" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("removeUser", removeUser)))))
}
                                                

// @LINE:475
def addCollectionToSpace(spaceId:UUID, collectionId:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId) + "/addCollectionToSpace/" + implicitly[PathBindable[UUID]].unbind("collectionId", collectionId))
}
                                                

// @LINE:462
def get(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:461
def listCanEdit(title:Option[String] = None, date:Option[String] = None, limit:Int = 12): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/spaces/canEdit" + queryString(List(if(title == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("title", title)), if(date == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("date", date)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:471
def acceptRequest(id:UUID, user:String, role:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/acceptRequest" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("user", user)), Some(implicitly[QueryStringBindable[String]].unbind("role", role)))))
}
                                                

// @LINE:469
def updateSpace(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/update")
}
                                                

// @LINE:480
def listDatasets(id:UUID, limit:Integer = 0): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/datasets" + queryString(List(if(limit == 0) None else Some(implicitly[QueryStringBindable[Integer]].unbind("limit", limit)))))
}
                                                

// @LINE:479
def verifySpace(id:UUID): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/verify")
}
                                                

// @LINE:460
def list(title:Option[String] = None, date:Option[String] = None, limit:Int = 12): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/spaces" + queryString(List(if(title == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("title", title)), if(date == None) None else Some(implicitly[QueryStringBindable[Option[String]]].unbind("date", date)), if(limit == 12) None else Some(implicitly[QueryStringBindable[Int]].unbind("limit", limit)))))
}
                                                

// @LINE:477
def unfollow(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/unfollow")
}
                                                

// @LINE:483
def getUserSpaceRoleMap(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/userSpaceRoleMap")
}
                                                

// @LINE:476
def follow(id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/follow")
}
                                                

// @LINE:481
def listCollections(id:UUID, limit:Integer = 0): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("id", id) + "/collections" + queryString(List(if(limit == 0) None else Some(implicitly[QueryStringBindable[Integer]].unbind("limit", limit)))))
}
                                                

// @LINE:467
def copyContentsToSpace(spaceId:UUID, id:UUID): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/spaces/" + implicitly[PathBindable[UUID]].unbind("spaceId", spaceId) + "/copyContentsToSpace/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                
    
}
                          

// @LINE:760
// @LINE:759
// @LINE:758
// @LINE:757
// @LINE:756
class ReverseRelations {
    

// @LINE:760
def delete(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/relations/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:758
def get(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/relations/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:757
def findTargets(sourceId:String, sourceType:String, targetType:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/relations/search" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("sourceId", sourceId)), Some(implicitly[QueryStringBindable[String]].unbind("sourceType", sourceType)), Some(implicitly[QueryStringBindable[String]].unbind("targetType", targetType)))))
}
                                                

// @LINE:759
def add(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/relations")
}
                                                

// @LINE:756
def list(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/relations")
}
                                                
    
}
                          

// @LINE:803
// @LINE:802
class ReverseStatus {
    

// @LINE:803
def status(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/status")
}
                                                

// @LINE:802
def version(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/version")
}
                                                
    
}
                          

// @LINE:314
// @LINE:313
// @LINE:312
// @LINE:311
class ReverseContextLD {
    

// @LINE:313
def getContextByName(name:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/contexts/" + implicitly[PathBindable[String]].unbind("name", dynamicString(name)) + "/context.json")
}
                                                

// @LINE:314
def removeById(id:UUID): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "api/contexts/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:312
def getContextById(id:UUID): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "api/contexts/" + implicitly[PathBindable[UUID]].unbind("id", id))
}
                                                

// @LINE:311
def addContext(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "api/contexts")
}
                                                
    
}
                          
}
                  


// @LINE:970
// @LINE:968
// @LINE:934
// @LINE:921
// @LINE:841
// @LINE:838
// @LINE:837
// @LINE:834
// @LINE:831
// @LINE:827
// @LINE:826
// @LINE:825
// @LINE:824
// @LINE:823
// @LINE:822
// @LINE:821
// @LINE:819
// @LINE:818
// @LINE:817
// @LINE:816
// @LINE:815
// @LINE:809
// @LINE:808
// @LINE:797
// @LINE:796
// @LINE:744
// @LINE:639
// @LINE:638
// @LINE:637
// @LINE:290
// @LINE:289
// @LINE:284
// @LINE:278
// @LINE:277
// @LINE:276
// @LINE:275
// @LINE:274
// @LINE:273
// @LINE:272
// @LINE:271
// @LINE:270
// @LINE:269
// @LINE:268
// @LINE:267
// @LINE:266
// @LINE:265
// @LINE:264
// @LINE:263
// @LINE:262
// @LINE:261
// @LINE:260
// @LINE:259
// @LINE:258
// @LINE:257
// @LINE:256
// @LINE:255
// @LINE:254
// @LINE:235
// @LINE:218
// @LINE:213
// @LINE:212
// @LINE:211
// @LINE:210
// @LINE:208
// @LINE:205
// @LINE:200
// @LINE:199
// @LINE:198
// @LINE:197
// @LINE:196
// @LINE:195
// @LINE:194
// @LINE:193
// @LINE:191
// @LINE:190
// @LINE:185
// @LINE:184
// @LINE:183
// @LINE:181
// @LINE:180
// @LINE:179
// @LINE:178
// @LINE:173
// @LINE:172
// @LINE:171
// @LINE:170
// @LINE:165
// @LINE:164
// @LINE:163
// @LINE:162
// @LINE:161
// @LINE:160
// @LINE:159
// @LINE:158
// @LINE:157
// @LINE:156
// @LINE:155
// @LINE:154
// @LINE:153
// @LINE:152
// @LINE:148
// @LINE:147
// @LINE:146
// @LINE:145
// @LINE:144
// @LINE:143
// @LINE:142
// @LINE:141
// @LINE:140
// @LINE:139
// @LINE:138
// @LINE:137
// @LINE:136
// @LINE:131
// @LINE:130
// @LINE:129
// @LINE:128
// @LINE:127
// @LINE:126
// @LINE:125
// @LINE:124
// @LINE:123
// @LINE:122
// @LINE:121
// @LINE:120
// @LINE:119
// @LINE:118
// @LINE:117
// @LINE:116
// @LINE:115
// @LINE:114
// @LINE:113
// @LINE:112
// @LINE:111
// @LINE:110
// @LINE:109
// @LINE:104
// @LINE:103
// @LINE:102
// @LINE:101
// @LINE:99
// @LINE:97
// @LINE:96
// @LINE:95
// @LINE:92
// @LINE:91
// @LINE:90
// @LINE:89
// @LINE:88
// @LINE:87
// @LINE:86
// @LINE:84
// @LINE:83
// @LINE:77
// @LINE:76
// @LINE:74
// @LINE:69
// @LINE:68
// @LINE:63
// @LINE:62
// @LINE:61
// @LINE:60
// @LINE:55
// @LINE:54
// @LINE:53
// @LINE:52
// @LINE:51
// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:41
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:8
package controllers.javascript {

// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
class ReverseAssets {
    

// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      if (file == """ + implicitly[JavascriptLitteral[String]].to("/jsonld/contexts/metadata.jsonld") + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "contexts/metadata.jsonld"})
      }
      if (file == """ + implicitly[JavascriptLitteral[String]].to("/images/glyphicons-halflings-white.png") + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/img/glyphicons-halflings-white.png"})
      }
      if (file == """ + implicitly[JavascriptLitteral[String]].to("/images/glyphicons-halflings.png") + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/img/glyphicons-halflings.png"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
      }
   """
)
                        
    
}
              

// @LINE:181
// @LINE:180
// @LINE:179
// @LINE:178
// @LINE:163
class ReverseMetadata {
    

// @LINE:179
def view : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Metadata.view",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "metadata/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:181
def dataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Metadata.dataset",
   """
      function(dataset_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("dataset_id", dataset_id) + "/metadata"})
      }
   """
)
                        

// @LINE:180
def file : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Metadata.file",
   """
      function(file_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id) + "/metadata"})
      }
   """
)
                        

// @LINE:163
def getMetadataBySpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Metadata.getMetadataBySpace",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata"})
      }
   """
)
                        

// @LINE:178
def search : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Metadata.search",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "metadata/search"})
      }
   """
)
                        
    
}
              

// @LINE:122
// @LINE:121
// @LINE:120
// @LINE:119
// @LINE:118
// @LINE:117
// @LINE:116
class ReverseToolManager {
    

// @LINE:116
def toolManager : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ToolManager.toolManager",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "toolManager"})
      }
   """
)
                        

// @LINE:121
def removeInstance : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ToolManager.removeInstance",
   """
      function(toolType,instanceID) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/removeInstance" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("toolType", toolType), (""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("instanceID", instanceID)])})
      }
   """
)
                        

// @LINE:119
def uploadDatasetToTool : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ToolManager.uploadDatasetToTool",
   """
      function(instanceID,datasetID,datasetName) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/uploadToTool" + _qS([(""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("instanceID", instanceID), (""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("datasetID", datasetID), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("datasetName", datasetName)])})
      }
   """
)
                        

// @LINE:122
def launchTool : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ToolManager.launchTool",
   """
      function(sessionName,ttype,datasetId,datasetName) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/launchTool" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("sessionName", sessionName), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("ttype", ttype), (""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("datasetName", datasetName)])})
      }
   """
)
                        

// @LINE:118
def getLaunchableTools : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ToolManager.getLaunchableTools",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/launchableTools"})
      }
   """
)
                        

// @LINE:117
def refreshToolSidebar : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ToolManager.refreshToolSidebar",
   """
      function(datasetid,datasetName) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/refreshToolList" + _qS([(""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("datasetid", datasetid), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("datasetName", datasetName)])})
      }
   """
)
                        

// @LINE:120
def getInstances : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ToolManager.getInstances",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/getInstances"})
      }
   """
)
                        
    
}
              

// @LINE:130
// @LINE:129
class ReverseFolders {
    

// @LINE:129
def addFiles : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Folders.addFiles",
   """
      function(id,folderId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("folderId", encodeURIComponent(folderId)) + "/addFiles"})
      }
   """
)
                        

// @LINE:130
def createFolder : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Folders.createFolder",
   """
      function(parentDatasetId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("parentDatasetId", parentDatasetId) + "/newFolder"})
      }
   """
)
                        
    
}
              

// @LINE:278
// @LINE:277
// @LINE:275
// @LINE:274
// @LINE:273
// @LINE:272
// @LINE:271
// @LINE:270
// @LINE:269
// @LINE:268
// @LINE:267
// @LINE:266
// @LINE:265
// @LINE:264
// @LINE:263
// @LINE:262
// @LINE:261
// @LINE:260
// @LINE:259
// @LINE:258
// @LINE:257
// @LINE:256
// @LINE:255
// @LINE:254
class ReverseAdmin {
    

// @LINE:262
def getIndexers : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.getIndexers",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/indexers"})
      }
   """
)
                        

// @LINE:263
def getIndexes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.getIndexes",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/indexes"})
      }
   """
)
                        

// @LINE:270
def listRoles : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.listRoles",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/roles"})
      }
   """
)
                        

// @LINE:260
def getExtractors : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.getExtractors",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/extractors"})
      }
   """
)
                        

// @LINE:269
def deleteAllIndexes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.deleteAllIndexes",
   """
      function() {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/index"})
      }
   """
)
                        

// @LINE:264
def getSections : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.getSections",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/sections"})
      }
   """
)
                        

// @LINE:257
def adminIndex : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.adminIndex",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/indexAdmin"})
      }
   """
)
                        

// @LINE:266
def createIndex : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.createIndex",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/createIndex"})
      }
   """
)
                        

// @LINE:259
def getAdapters : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.getAdapters",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/adapters"})
      }
   """
)
                        

// @LINE:271
def createRole : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.createRole",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/roles/new"})
      }
   """
)
                        

// @LINE:272
def submitCreateRole : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.submitCreateRole",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/roles/submitNew"})
      }
   """
)
                        

// @LINE:268
def deleteIndex : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.deleteIndex",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/index/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:256
def users : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.users",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/users"})
      }
   """
)
                        

// @LINE:258
def reindexFiles : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.reindexFiles",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/reindexFiles"})
      }
   """
)
                        

// @LINE:265
def getMetadataDefinitions : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.getMetadataDefinitions",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/metadata/definitions"})
      }
   """
)
                        

// @LINE:277
def viewDumpers : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.viewDumpers",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/dataDumps"})
      }
   """
)
                        

// @LINE:255
def tos : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.tos",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/tos"})
      }
   """
)
                        

// @LINE:274
def editRole : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.editRole",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/roles/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/edit"})
      }
   """
)
                        

// @LINE:275
def updateRole : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.updateRole",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/roles/update"})
      }
   """
)
                        

// @LINE:267
def buildIndex : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.buildIndex",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/index/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/build"})
      }
   """
)
                        

// @LINE:278
def sensors : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.sensors",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/sensors"})
      }
   """
)
                        

// @LINE:254
def customize : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.customize",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/customize"})
      }
   """
)
                        

// @LINE:273
def removeRole : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.removeRole",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/roles/delete/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:261
def getMeasures : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Admin.getMeasures",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/measures"})
      }
   """
)
                        
    
}
              

// @LINE:797
// @LINE:796
// @LINE:34
// @LINE:31
// @LINE:30
class ReverseUsers {
    

// @LINE:31
def acceptTermsOfServices : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.acceptTermsOfServices",
   """
      function(redirect) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users/acceptTermsOfServices" + _qS([(redirect == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("redirect", redirect))])})
      }
   """
)
                        

// @LINE:796
def getFollowers : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.getFollowers",
   """
      function(index,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users/followers" + _qS([(index == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("index", index)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:30
def getUsers : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.getUsers",
   """
      function(when,id,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users" + _qS([(when == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("when", when)), (id == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("id", id)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:34
def sendEmail : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.sendEmail",
   """
      function(subject,from,recipient,body) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "users/sendEmail" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("subject", subject), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("from", from), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("recipient", recipient), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("body", body)])})
      }
   """
)
                        

// @LINE:797
def getFollowing : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Users.getFollowing",
   """
      function(index,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users/following" + _qS([(index == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("index", index)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        
    
}
              

// @LINE:74
// @LINE:68
// @LINE:63
// @LINE:62
// @LINE:61
// @LINE:60
class ReverseExtractionInfo {
    

// @LINE:61
def getExtractorServersIP : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ExtractionInfo.getExtractorServersIP",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "extractions/servers_ips"})
      }
   """
)
                        

// @LINE:60
def getDTSRequests : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ExtractionInfo.getDTSRequests",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "extractions/requests"})
      }
   """
)
                        

// @LINE:68
def getBookmarkletPage : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ExtractionInfo.getBookmarkletPage",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "bookmarklet"})
      }
   """
)
                        

// @LINE:63
def getExtractorInputTypes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ExtractionInfo.getExtractorInputTypes",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "extractions/supported_input_types"})
      }
   """
)
                        

// @LINE:74
def getExtensionPage : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ExtractionInfo.getExtensionPage",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "extensions/dts/chrome"})
      }
   """
)
                        

// @LINE:62
def getExtractorNames : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.ExtractionInfo.getExtractorNames",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "extractions/extractors_names"})
      }
   """
)
                        
    
}
              

// @LINE:276
// @LINE:131
// @LINE:104
class ReverseExtractors {
    

// @LINE:131
def submitDatasetExtraction : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Extractors.submitDatasetExtraction",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/extractions"})
      }
   """
)
                        

// @LINE:104
def submitFileExtraction : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Extractors.submitFileExtraction",
   """
      function(file_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id) + "/extractions"})
      }
   """
)
                        

// @LINE:276
def listAllExtractions : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Extractors.listAllExtractions",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/extractions"})
      }
   """
)
                        
    
}
              

// @LINE:55
// @LINE:54
// @LINE:53
// @LINE:52
// @LINE:51
class ReverseError {
    

// @LINE:53
def incorrectPermissions : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Error.incorrectPermissions",
   """
      function(msg) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "error/noPermissions" + _qS([(msg == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("msg", msg))])})
      }
   """
)
                        

// @LINE:52
def authenticationRequiredMessage : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Error.authenticationRequiredMessage",
   """
      function(msg,url) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "error/authenticationRequiredMessage/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("msg", encodeURIComponent(msg)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("url", encodeURIComponent(url))})
      }
   """
)
                        

// @LINE:54
def notActivated : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Error.notActivated",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "error/notActivated"})
      }
   """
)
                        

// @LINE:51
def authenticationRequired : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Error.authenticationRequired",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "error/authenticationRequired"})
      }
   """
)
                        

// @LINE:55
def notAuthorized : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Error.notAuthorized",
   """
      function(msg,id,resourceType) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "error/notAuthorized" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("msg", msg), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("id", id), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("resourceType", resourceType)])})
      }
   """
)
                        
    
}
              

// @LINE:208
// @LINE:205
class ReverseDataAnalysis {
    

// @LINE:205
def listSessions : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.DataAnalysis.listSessions",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "analysisSessions"})
      }
   """
)
                        

// @LINE:208
def terminate : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.DataAnalysis.terminate",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "terminateSession/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        
    
}
              

// @LINE:103
// @LINE:102
// @LINE:101
// @LINE:99
// @LINE:97
// @LINE:96
// @LINE:95
// @LINE:92
// @LINE:91
// @LINE:90
// @LINE:89
// @LINE:88
// @LINE:87
// @LINE:86
// @LINE:84
// @LINE:83
// @LINE:77
// @LINE:76
class ReverseFiles {
    

// @LINE:99
def uploaddnd : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.uploaddnd",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "uploaddnd/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:86
def metadataSearch : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.metadataSearch",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files/metadataSearch"})
      }
   """
)
                        

// @LINE:88
def followingFiles : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.followingFiles",
   """
      function(index,size,mode) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files/following" + _qS([(index == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("index", index)), (size == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("size", size)), (mode == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mode", mode))])})
      }
   """
)
                        

// @LINE:103
def fileBySection : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.fileBySection",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "file_by_section/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:95
def upload : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.upload",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "upload"})
      }
   """
)
                        

// @LINE:76
def extractFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.extractFile",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "extraction/form"})
      }
   """
)
                        

// @LINE:84
def uploadFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.uploadFile",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files/new"})
      }
   """
)
                        

// @LINE:97
def uploadSelectQuery : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.uploadSelectQuery",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadSelectQuery"})
      }
   """
)
                        

// @LINE:101
def uploadDragDrop : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.uploadDragDrop",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadDragDrop"})
      }
   """
)
                        

// @LINE:87
def generalMetadataSearch : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.generalMetadataSearch",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files/generalMetadataSearch"})
      }
   """
)
                        

// @LINE:102
def thumbnail : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.thumbnail",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "fileThumbnail/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/blob"})
      }
   """
)
                        

// @LINE:83
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.list",
   """
      function(when,date,size,mode) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files" + _qS([(when == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("when", when)), (date == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("date", date)), (size == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("size", size)), (mode == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mode", mode))])})
      }
   """
)
                        

// @LINE:89
def file : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.file",
   """
      function(id,dataset,space,folder) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(dataset == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("dataset", dataset)), (space == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space)), (folder == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("folder", folder))])})
      }
   """
)
                        

// @LINE:91
def download : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.download",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/blob"})
      }
   """
)
                        

// @LINE:96
def uploadAndExtract : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.uploadAndExtract",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadAndExtract"})
      }
   """
)
                        

// @LINE:77
def uploadExtract : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.uploadExtract",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "extraction/upload"})
      }
   """
)
                        

// @LINE:90
def filePreview : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.filePreview",
   """
      function(id,dataset,space,folder) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "filesPreview/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(dataset == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("dataset", dataset)), (space == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space)), (folder == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("folder", folder))])})
      }
   """
)
                        

// @LINE:92
def downloadAsFormat : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Files.downloadAsFormat",
   """
      function(id,format) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/download/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("format", encodeURIComponent(format))})
      }
   """
)
                        
    
}
              

// @LINE:218
class ReversePreviewers {
    

// @LINE:218
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Previewers.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "previewers/list"})
      }
   """
)
                        
    
}
              

// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:41
class ReverseProfile {
    

// @LINE:46
// @LINE:43
def viewProfileUUID : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Profile.viewProfileUUID",
   """
      function(uuid) {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "profile/viewProfile/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("uuid", uuid)})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "profile/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("uuid", uuid)})
      }
      }
   """
)
                        

// @LINE:41
def viewProfile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Profile.viewProfile",
   """
      function(email) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "profile/viewProfile" + _qS([(""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("email", email)])})
      }
   """
)
                        

// @LINE:45
def submitChanges : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Profile.submitChanges",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "profile/submitChanges"})
      }
   """
)
                        

// @LINE:44
def editProfile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Profile.editProfile",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "profile/editProfile"})
      }
   """
)
                        
    
}
              

// @LINE:213
// @LINE:212
// @LINE:211
// @LINE:210
class ReverseNotebook {
    

// @LINE:211
def viewNotebook : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Notebook.viewNotebook",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "notebook/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:212
def editNotebook : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Notebook.editNotebook",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "editNotebook/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:213
def deleteNotebook : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Notebook.deleteNotebook",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "deleteNotebook/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:210
def listNotebooks : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Notebook.listNotebooks",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "notebooks"})
      }
   """
)
                        
    
}
              

// @LINE:148
// @LINE:147
// @LINE:146
// @LINE:145
// @LINE:144
// @LINE:143
// @LINE:142
// @LINE:141
// @LINE:140
// @LINE:139
// @LINE:138
// @LINE:137
// @LINE:136
class ReverseCollections {
    

// @LINE:138
def listChildCollections : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.listChildCollections",
   """
      function(parentCollectionId,when,date,size,space,mode,owner) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collections/listChildCollections" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("parentCollectionId", parentCollectionId), (when == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("when", when)), (date == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("date", date)), (size == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("size", size)), (space == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space)), (mode == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mode", mode)), (owner == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("owner", owner))])})
      }
   """
)
                        

// @LINE:140
def newCollectionWithParent : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.newCollectionWithParent",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/newchildCollection"})
      }
   """
)
                        

// @LINE:141
def followingCollections : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.followingCollections",
   """
      function(index,size,mode) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collections/following" + _qS([(index == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("index", index)), (size == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("size", size)), (mode == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mode", mode))])})
      }
   """
)
                        

// @LINE:142
def submit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.submit",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "collection/submit"})
      }
   """
)
                        

// @LINE:143
def createStep2 : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.createStep2",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collection/createStep2"})
      }
   """
)
                        

// @LINE:145
def users : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.users",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collection/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/users"})
      }
   """
)
                        

// @LINE:148
def getUpdatedChildCollections : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.getUpdatedChildCollections",
   """
      function(id,index,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collection/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/childCollections" + _qS([(index == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("index", index)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:146
def previews : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.previews",
   """
      function(collection_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("collection_id", collection_id) + "/previews"})
      }
   """
)
                        

// @LINE:144
def collection : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.collection",
   """
      function(id,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collection/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:136
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.list",
   """
      function(when,date,size,space,mode,owner,showPublic,showOnlyShared,showTrash) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collections" + _qS([(when == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("when", when)), (date == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("date", date)), (size == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("size", size)), (space == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space)), (mode == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mode", mode)), (owner == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("owner", owner)), (showPublic == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showPublic", showPublic)), (showOnlyShared == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showOnlyShared", showOnlyShared)), (showTrash == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showTrash", showTrash))])})
      }
   """
)
                        

// @LINE:137
def sortedListInSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.sortedListInSpace",
   """
      function(space,offset,limit,showPublic) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collections/sorted" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("space", space), (""" + implicitly[QueryStringBindable[Integer]].javascriptUnbind + """)("offset", offset), (""" + implicitly[QueryStringBindable[Integer]].javascriptUnbind + """)("limit", limit), (showPublic == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showPublic", showPublic))])})
      }
   """
)
                        

// @LINE:147
def getUpdatedDatasets : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.getUpdatedDatasets",
   """
      function(id,index,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collection/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/datasets" + _qS([(index == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("index", index)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:139
def newCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Collections.newCollection",
   """
      function(space) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "collections/new" + _qS([(""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space)])})
      }
   """
)
                        
    
}
              

// @LINE:235
class ReverseRegistration {
    

// @LINE:235
def handleSignUp : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Registration.handleSignUp",
   """
      function(token) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "signup/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("token", encodeURIComponent(token))})
      }
   """
)
                        
    
}
              

// @LINE:128
// @LINE:127
// @LINE:126
// @LINE:125
// @LINE:124
// @LINE:123
// @LINE:115
// @LINE:114
// @LINE:113
// @LINE:112
// @LINE:111
// @LINE:110
// @LINE:109
class ReverseDatasets {
    

// @LINE:127
def submit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.submit",
   """
      function(folderId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "dataset/submit" + _qS([(folderId == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("folderId", folderId))])})
      }
   """
)
                        

// @LINE:113
def metadataSearch : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.metadataSearch",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/metadataSearch"})
      }
   """
)
                        

// @LINE:109
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.list",
   """
      function(when,date,size,space,status,mode,owner,showPublic,showOnlyShared,showTrash) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets" + _qS([(when == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("when", when)), (date == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("date", date)), (size == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("size", size)), (space == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space)), (status == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("status", status)), (mode == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mode", mode)), (owner == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("owner", owner)), (showPublic == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showPublic", showPublic)), (showOnlyShared == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showOnlyShared", showOnlyShared)), (showTrash == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showTrash", showTrash))])})
      }
   """
)
                        

// @LINE:114
def generalMetadataSearch : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.generalMetadataSearch",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/generalMetadataSearch"})
      }
   """
)
                        

// @LINE:112
def createStep2 : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.createStep2",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/createStep2" + _qS([(""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("id", id)])})
      }
   """
)
                        

// @LINE:125
def users : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.users",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/users"})
      }
   """
)
                        

// @LINE:115
def followingDatasets : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.followingDatasets",
   """
      function(index,size,mode) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/following" + _qS([(index == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("index", index)), (size == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("size", size)), (mode == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mode", mode))])})
      }
   """
)
                        

// @LINE:111
def newDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.newDataset",
   """
      function(space,collection) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/new" + _qS([(""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space), (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("collection", collection)])})
      }
   """
)
                        

// @LINE:126
def datasetBySection : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.datasetBySection",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets_by_section/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:128
def addFiles : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.addFiles",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/addFiles"})
      }
   """
)
                        

// @LINE:123
def dataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.dataset",
   """
      function(id,space,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(space == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:110
def sortedListInSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.sortedListInSpace",
   """
      function(space,offset,limit,showPublic) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/sorted" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("space", space), (""" + implicitly[QueryStringBindable[Integer]].javascriptUnbind + """)("offset", offset), (""" + implicitly[QueryStringBindable[Integer]].javascriptUnbind + """)("limit", limit), (showPublic == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showPublic", showPublic))])})
      }
   """
)
                        

// @LINE:124
def getUpdatedFilesAndFolders : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Datasets.getUpdatedFilesAndFolders",
   """
      function(datasetId,limit,pageIndex,space) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId) + "/updatedFilesAndFolders" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (pageIndex == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("pageIndex", pageIndex)), (space == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space))])})
      }
   """
)
                        
    
}
              

// @LINE:36
// @LINE:35
// @LINE:32
class ReverseLogin {
    

// @LINE:32
def isLoggedIn : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Login.isLoggedIn",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login/isLoggedIn"})
      }
   """
)
                        

// @LINE:36
def ldapAuthenticate : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Login.ldapAuthenticate",
   """
      function(uid,password) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "ldap/authenticate" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("uid", uid), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("password", password)])})
      }
   """
)
                        

// @LINE:35
def ldap : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Login.ldap",
   """
      function(redirecturl,token) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "ldap" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("redirecturl", redirecturl), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("token", token)])})
      }
   """
)
                        
    
}
              

// @LINE:744
class ReverseSelected {
    

// @LINE:744
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Selected.get",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "selected"})
      }
   """
)
                        
    
}
              

// @LINE:841
class ReverseEvents {
    

// @LINE:841
def getEvents : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Events.getEvents",
   """
      function(index) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "getEvents" + _qS([(""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("index", index)])})
      }
   """
)
                        
    
}
              

// @LINE:290
// @LINE:289
// @LINE:284
// @LINE:69
// @LINE:33
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:8
class ReverseApplication {
    

// @LINE:16
def tos : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.tos",
   """
      function(redirect) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "tos" + _qS([(redirect == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("redirect", redirect))])})
      }
   """
)
                        

// @LINE:69
def bookmarklet : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.bookmarklet",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "bookmarklet.js"})
      }
   """
)
                        

// @LINE:15
def about : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.about",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "about"})
      }
   """
)
                        

// @LINE:290
def swaggerUI : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.swaggerUI",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "swaggerUI"})
      }
   """
)
                        

// @LINE:17
def email : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.email",
   """
      function(subject) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "email" + _qS([(subject == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("subject", subject))])})
      }
   """
)
                        

// @LINE:8
def untrail : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.untrail",
   """
      function(path) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("path", path) + "/"})
      }
   """
)
                        

// @LINE:289
def swagger : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.swagger",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "swagger"})
      }
   """
)
                        

// @LINE:33
def onlyGoogle : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.onlyGoogle",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login/isLoggedInWithGoogle"})
      }
   """
)
                        

// @LINE:14
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

// @LINE:284
def javascriptRoutes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.javascriptRoutes",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "javascriptRoutes"})
      }
   """
)
                        
    
}
              

// @LINE:173
// @LINE:172
// @LINE:171
// @LINE:170
class ReverseGeostreams {
    

// @LINE:173
def edit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Geostreams.edit",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "geostreams/sensors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:170
def map : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Geostreams.map",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "geostreams"})
      }
   """
)
                        

// @LINE:171
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Geostreams.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "geostreams/sensors"})
      }
   """
)
                        

// @LINE:172
def newSensor : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Geostreams.newSensor",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "geostreams/sensors/new"})
      }
   """
)
                        
    
}
              

// @LINE:185
// @LINE:184
// @LINE:183
class ReverseTags {
    

// @LINE:183
def search : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Tags.search",
   """
      function(tag,start,size,mode) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "tags/search" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("tag", tag), (start == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("start", start)), (size == null ? null : (""" + implicitly[QueryStringBindable[Integer]].javascriptUnbind + """)("size", size)), (mode == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mode", mode))])})
      }
   """
)
                        

// @LINE:185
def tagListWeighted : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Tags.tagListWeighted",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "tags/list/weighted"})
      }
   """
)
                        

// @LINE:184
def tagListOrdered : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Tags.tagListOrdered",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "tags/list/ordered"})
      }
   """
)
                        
    
}
              

// @LINE:809
// @LINE:808
class ReverseRSS {
    

// @LINE:809
def siteRSSOfType : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.RSS.siteRSSOfType",
   """
      function(limit,etype) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "rss/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("etype", encodeURIComponent(etype)) + _qS([(""" + implicitly[QueryStringBindable[Option[Int]]].javascriptUnbind + """)("limit", limit)])})
      }
   """
)
                        

// @LINE:808
def siteRSS : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.RSS.siteRSS",
   """
      function(limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "rss" + _qS([(""" + implicitly[QueryStringBindable[Option[Int]]].javascriptUnbind + """)("limit", limit)])})
      }
   """
)
                        
    
}
              

// @LINE:838
// @LINE:837
// @LINE:834
// @LINE:831
// @LINE:827
// @LINE:826
// @LINE:825
// @LINE:824
// @LINE:823
// @LINE:822
// @LINE:821
// @LINE:819
// @LINE:817
// @LINE:816
// @LINE:815
class ReverseCurationObjects {
    

// @LINE:831
def getStatusFromRepository : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.getStatusFromRepository",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/getStatusFromRepository"})
      }
   """
)
                        

// @LINE:821
def editCuration : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.editCuration",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/edit"})
      }
   """
)
                        

// @LINE:816
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.list",
   """
      function(when,date,limit,space) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "space/curations" + _qS([(when == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("when", when)), (date == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("date", date)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (space == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space))])})
      }
   """
)
                        

// @LINE:826
def sendToRepository : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.sendToRepository",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/sendToRepository"})
      }
   """
)
                        

// @LINE:823
def deleteCuration : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.deleteCuration",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:834
def getUpdatedFilesAndFolders : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.getUpdatedFilesAndFolders",
   """
      function(id,curationFolderId,limit,pageIndex) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/updatedFilesAndFolders" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("curationFolderId", curationFolderId), (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit), (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("pageIndex", pageIndex)])})
      }
   """
)
                        

// @LINE:822
def updateCuration : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.updateCuration",
   """
      function(id,spaceId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId) + "/update"})
      }
   """
)
                        

// @LINE:827
def findMatchingRepositories : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.findMatchingRepositories",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/matchmaker"})
      }
   """
)
                        

// @LINE:815
def newCO : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.newCO",
   """
      function(id,space) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "dataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/curations/new" + _qS([(space == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("space", space))])})
      }
   """
)
                        

// @LINE:819
def getCurationObject : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.getCurationObject",
   """
      function(id,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:825
def submitRepositorySelection : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.submitRepositorySelection",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/submitRepositorySelection"})
      }
   """
)
                        

// @LINE:824
def compareToRepository : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.compareToRepository",
   """
      function(id,repository) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/compareToRepository" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("repository", repository)])})
      }
   """
)
                        

// @LINE:838
// @LINE:837
def getPublishedData : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.getPublishedData",
   """
      function(space) {
      if (space == """ + implicitly[JavascriptLitteral[String]].to("") + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "publishedData"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "publishedData/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("space", encodeURIComponent(space))})
      }
      }
   """
)
                        

// @LINE:817
def submit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.CurationObjects.submit",
   """
      function(id,spaceId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "dataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/curations/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId) + "/submit"})
      }
   """
)
                        
    
}
              

// @LINE:200
// @LINE:199
// @LINE:198
// @LINE:197
// @LINE:196
// @LINE:195
// @LINE:194
// @LINE:193
// @LINE:191
// @LINE:190
class ReverseSearch {
    

// @LINE:198
def findSimilarToExistingFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Search.findSimilarToExistingFile",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/similar"})
      }
   """
)
                        

// @LINE:193
def advanced : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Search.advanced",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "advanced"})
      }
   """
)
                        

// @LINE:194
def SearchByText : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Search.SearchByText",
   """
      function(query) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "SearchByText" + _qS([(query == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("query", query))])})
      }
   """
)
                        

// @LINE:190
def search : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Search.search",
   """
      function(query) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "search" + _qS([(query == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("query", query))])})
      }
   """
)
                        

// @LINE:195
def uploadquery : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Search.uploadquery",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "uploadquery"})
      }
   """
)
                        

// @LINE:199
def findSimilarToQueryFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Search.findSimilarToQueryFile",
   """
      function(id,typeToSearch,sectionsSelected) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "queries/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/similar" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("typeToSearch", typeToSearch), (sectionsSelected == null ? null : (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("sectionsSelected", sectionsSelected))])})
      }
   """
)
                        

// @LINE:200
def findSimilarWeightedIndexes : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Search.findSimilarWeightedIndexes",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "queries/similarWeightedIndexes"})
      }
   """
)
                        

// @LINE:197
def callSearchMultimediaIndexView : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Search.callSearchMultimediaIndexView",
   """
      function(section_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchbyfeature/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("section_id", section_id)})
      }
   """
)
                        

// @LINE:191
def multimediasearch : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Search.multimediasearch",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "multimediasearch"})
      }
   """
)
                        

// @LINE:196
def searchbyURL : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Search.searchbyURL",
   """
      function(query) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "searchbyURL" + _qS([(query == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("query", query))])})
      }
   """
)
                        
    
}
              

// @LINE:970
// @LINE:968
// @LINE:934
// @LINE:921
class ReverseT2C2 {
    

// @LINE:968
def uploader : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.T2C2.uploader",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/uploader"})
      }
   """
)
                        

// @LINE:970
def zipUploader : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.T2C2.zipUploader",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/zipUploader"})
      }
   """
)
                        

// @LINE:921
def newTemplate : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.T2C2.newTemplate",
   """
      function(space) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/new" + _qS([(""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space)])})
      }
   """
)
                        

// @LINE:934
def uploadFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.T2C2.uploadFile",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/zipUpload"})
      }
   """
)
                        
    
}
              

// @LINE:639
// @LINE:638
// @LINE:637
class ReverseVocabularies {
    

// @LINE:637
def newVocabulary : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Vocabularies.newVocabulary",
   """
      function(space) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "vocabularies/new" + _qS([(""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space)])})
      }
   """
)
                        

// @LINE:639
def vocabulary : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Vocabularies.vocabulary",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "vocabularies/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:638
def submit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Vocabularies.submit",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "vocabularies/submit"})
      }
   """
)
                        
    
}
              

// @LINE:818
// @LINE:165
// @LINE:164
// @LINE:162
// @LINE:161
// @LINE:160
// @LINE:159
// @LINE:158
// @LINE:157
// @LINE:156
// @LINE:155
// @LINE:154
// @LINE:153
// @LINE:152
class ReverseSpaces {
    

// @LINE:161
def updateExtractors : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.updateExtractors",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/extractors"})
      }
   """
)
                        

// @LINE:156
def getSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.getSpace",
   """
      function(id,size,direction) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(size == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("size", size)), (direction == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("direction", direction))])})
      }
   """
)
                        

// @LINE:155
def followingSpaces : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.followingSpaces",
   """
      function(index,size,mode) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/following" + _qS([(index == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("index", index)), (size == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("size", size)), (mode == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mode", mode))])})
      }
   """
)
                        

// @LINE:162
def inviteToSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.inviteToSpace",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/invite"})
      }
   """
)
                        

// @LINE:164
def submit : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.submit",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/submit"})
      }
   """
)
                        

// @LINE:160
def selectExtractors : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.selectExtractors",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/extractors"})
      }
   """
)
                        

// @LINE:159
def manageUsers : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.manageUsers",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/users"})
      }
   """
)
                        

// @LINE:818
def stagingArea : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.stagingArea",
   """
      function(id,index,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/stagingArea" + _qS([(index == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("index", index)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:153
def newSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.newSpace",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/new"})
      }
   """
)
                        

// @LINE:165
def submitCopy : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.submitCopy",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/submitCopy"})
      }
   """
)
                        

// @LINE:154
def copySpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.copySpace",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/copy/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:157
def updateSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.updateSpace",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/updateSpace"})
      }
   """
)
                        

// @LINE:158
def addRequest : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.addRequest",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/addRequest"})
      }
   """
)
                        

// @LINE:152
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Spaces.list",
   """
      function(when,date,size,mode,owner,showAll,showPublic,onlyTrial,showOnlyShared) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces" + _qS([(when == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("when", when)), (date == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("date", date)), (size == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("size", size)), (mode == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mode", mode)), (owner == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("owner", owner)), (showAll == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showAll", showAll)), (showPublic == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showPublic", showPublic)), (onlyTrial == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("onlyTrial", onlyTrial)), (showOnlyShared == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showOnlyShared", showOnlyShared))])})
      }
   """
)
                        
    
}
              
}
        

// @LINE:249
// @LINE:247
// @LINE:246
// @LINE:241
// @LINE:240
// @LINE:239
// @LINE:238
// @LINE:237
// @LINE:236
// @LINE:234
// @LINE:233
// @LINE:232
// @LINE:227
// @LINE:226
package securesocial.controllers.javascript {

// @LINE:239
// @LINE:238
// @LINE:237
// @LINE:236
// @LINE:234
// @LINE:233
// @LINE:232
class ReverseRegistration {
    

// @LINE:232
def startSignUp : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.Registration.startSignUp",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "signup"})
      }
   """
)
                        

// @LINE:233
def handleStartSignUp : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.Registration.handleStartSignUp",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "signup"})
      }
   """
)
                        

// @LINE:237
def handleStartResetPassword : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.Registration.handleStartResetPassword",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "reset"})
      }
   """
)
                        

// @LINE:238
def resetPassword : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.Registration.resetPassword",
   """
      function(token) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "reset/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("token", encodeURIComponent(token))})
      }
   """
)
                        

// @LINE:234
def signUp : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.Registration.signUp",
   """
      function(token) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "signup/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("token", encodeURIComponent(token))})
      }
   """
)
                        

// @LINE:236
def startResetPassword : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.Registration.startResetPassword",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "reset"})
      }
   """
)
                        

// @LINE:239
def handleResetPassword : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.Registration.handleResetPassword",
   """
      function(token) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "reset/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("token", encodeURIComponent(token))})
      }
   """
)
                        
    
}
              

// @LINE:249
// @LINE:247
// @LINE:246
class ReverseProviderController {
    

// @LINE:249
def notAuthorized : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.ProviderController.notAuthorized",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "not-authorized"})
      }
   """
)
                        

// @LINE:246
def authenticate : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.ProviderController.authenticate",
   """
      function(provider) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "authenticate/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("provider", encodeURIComponent(provider))})
      }
   """
)
                        

// @LINE:247
def authenticateByPost : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.ProviderController.authenticateByPost",
   """
      function(provider) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "authenticate/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("provider", encodeURIComponent(provider))})
      }
   """
)
                        
    
}
              

// @LINE:241
// @LINE:240
class ReversePasswordChange {
    

// @LINE:241
def handlePasswordChange : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.PasswordChange.handlePasswordChange",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "password"})
      }
   """
)
                        

// @LINE:240
def page : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.PasswordChange.page",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "password"})
      }
   """
)
                        
    
}
              

// @LINE:227
// @LINE:226
class ReverseLoginPage {
    

// @LINE:227
def logout : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.LoginPage.logout",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
      }
   """
)
                        

// @LINE:226
def login : JavascriptReverseRoute = JavascriptReverseRoute(
   "securesocial.controllers.LoginPage.login",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        
    
}
              
}
        

// @LINE:978
// @LINE:977
// @LINE:966
// @LINE:965
// @LINE:964
// @LINE:963
// @LINE:962
// @LINE:961
// @LINE:960
// @LINE:959
// @LINE:958
// @LINE:957
// @LINE:956
// @LINE:955
// @LINE:954
// @LINE:953
// @LINE:952
// @LINE:951
// @LINE:950
// @LINE:949
// @LINE:948
// @LINE:947
// @LINE:946
// @LINE:945
// @LINE:944
// @LINE:943
// @LINE:942
// @LINE:941
// @LINE:940
// @LINE:939
// @LINE:938
// @LINE:936
// @LINE:926
// @LINE:919
// @LINE:917
// @LINE:916
// @LINE:915
// @LINE:914
// @LINE:913
// @LINE:912
// @LINE:911
// @LINE:910
// @LINE:909
// @LINE:908
// @LINE:906
// @LINE:905
// @LINE:904
// @LINE:903
// @LINE:902
// @LINE:901
// @LINE:900
// @LINE:899
// @LINE:898
// @LINE:896
// @LINE:895
// @LINE:893
// @LINE:892
// @LINE:891
// @LINE:885
// @LINE:884
// @LINE:883
// @LINE:882
// @LINE:881
// @LINE:880
// @LINE:879
// @LINE:878
// @LINE:877
// @LINE:876
// @LINE:875
// @LINE:874
// @LINE:868
// @LINE:867
// @LINE:861
// @LINE:860
// @LINE:858
// @LINE:857
// @LINE:856
// @LINE:855
// @LINE:854
// @LINE:851
// @LINE:850
// @LINE:849
// @LINE:848
// @LINE:847
// @LINE:840
// @LINE:836
// @LINE:835
// @LINE:833
// @LINE:832
// @LINE:830
// @LINE:829
// @LINE:828
// @LINE:820
// @LINE:803
// @LINE:802
// @LINE:791
// @LINE:790
// @LINE:789
// @LINE:788
// @LINE:787
// @LINE:786
// @LINE:785
// @LINE:784
// @LINE:783
// @LINE:782
// @LINE:781
// @LINE:780
// @LINE:779
// @LINE:778
// @LINE:777
// @LINE:776
// @LINE:771
// @LINE:770
// @LINE:769
// @LINE:768
// @LINE:767
// @LINE:766
// @LINE:765
// @LINE:760
// @LINE:759
// @LINE:758
// @LINE:757
// @LINE:756
// @LINE:751
// @LINE:750
// @LINE:749
// @LINE:748
// @LINE:747
// @LINE:746
// @LINE:745
// @LINE:739
// @LINE:738
// @LINE:737
// @LINE:736
// @LINE:731
// @LINE:730
// @LINE:729
// @LINE:728
// @LINE:727
// @LINE:722
// @LINE:721
// @LINE:720
// @LINE:715
// @LINE:714
// @LINE:713
// @LINE:712
// @LINE:711
// @LINE:710
// @LINE:709
// @LINE:708
// @LINE:707
// @LINE:706
// @LINE:705
// @LINE:704
// @LINE:703
// @LINE:702
// @LINE:701
// @LINE:700
// @LINE:699
// @LINE:698
// @LINE:697
// @LINE:696
// @LINE:695
// @LINE:694
// @LINE:693
// @LINE:692
// @LINE:691
// @LINE:690
// @LINE:689
// @LINE:688
// @LINE:687
// @LINE:686
// @LINE:681
// @LINE:680
// @LINE:679
// @LINE:674
// @LINE:673
// @LINE:672
// @LINE:671
// @LINE:670
// @LINE:669
// @LINE:668
// @LINE:667
// @LINE:666
// @LINE:665
// @LINE:664
// @LINE:659
// @LINE:658
// @LINE:657
// @LINE:656
// @LINE:655
// @LINE:654
// @LINE:653
// @LINE:652
// @LINE:651
// @LINE:650
// @LINE:649
// @LINE:648
// @LINE:647
// @LINE:646
// @LINE:645
// @LINE:644
// @LINE:631
// @LINE:630
// @LINE:629
// @LINE:628
// @LINE:627
// @LINE:626
// @LINE:625
// @LINE:624
// @LINE:623
// @LINE:622
// @LINE:621
// @LINE:620
// @LINE:619
// @LINE:618
// @LINE:617
// @LINE:616
// @LINE:615
// @LINE:614
// @LINE:613
// @LINE:612
// @LINE:611
// @LINE:610
// @LINE:609
// @LINE:608
// @LINE:607
// @LINE:606
// @LINE:605
// @LINE:604
// @LINE:603
// @LINE:602
// @LINE:601
// @LINE:600
// @LINE:599
// @LINE:598
// @LINE:597
// @LINE:596
// @LINE:595
// @LINE:594
// @LINE:593
// @LINE:592
// @LINE:591
// @LINE:590
// @LINE:589
// @LINE:588
// @LINE:587
// @LINE:586
// @LINE:585
// @LINE:584
// @LINE:583
// @LINE:582
// @LINE:581
// @LINE:580
// @LINE:579
// @LINE:578
// @LINE:577
// @LINE:576
// @LINE:575
// @LINE:574
// @LINE:573
// @LINE:572
// @LINE:571
// @LINE:570
// @LINE:569
// @LINE:568
// @LINE:566
// @LINE:565
// @LINE:564
// @LINE:563
// @LINE:562
// @LINE:561
// @LINE:560
// @LINE:554
// @LINE:553
// @LINE:552
// @LINE:551
// @LINE:550
// @LINE:549
// @LINE:548
// @LINE:547
// @LINE:546
// @LINE:545
// @LINE:544
// @LINE:543
// @LINE:542
// @LINE:541
// @LINE:539
// @LINE:538
// @LINE:537
// @LINE:536
// @LINE:535
// @LINE:533
// @LINE:532
// @LINE:530
// @LINE:529
// @LINE:527
// @LINE:526
// @LINE:524
// @LINE:523
// @LINE:522
// @LINE:521
// @LINE:520
// @LINE:518
// @LINE:517
// @LINE:516
// @LINE:511
// @LINE:500
// @LINE:499
// @LINE:498
// @LINE:497
// @LINE:496
// @LINE:495
// @LINE:494
// @LINE:493
// @LINE:492
// @LINE:491
// @LINE:490
// @LINE:489
// @LINE:488
// @LINE:483
// @LINE:482
// @LINE:481
// @LINE:480
// @LINE:479
// @LINE:478
// @LINE:477
// @LINE:476
// @LINE:475
// @LINE:474
// @LINE:473
// @LINE:472
// @LINE:471
// @LINE:470
// @LINE:469
// @LINE:468
// @LINE:467
// @LINE:466
// @LINE:465
// @LINE:464
// @LINE:463
// @LINE:462
// @LINE:461
// @LINE:460
// @LINE:454
// @LINE:448
// @LINE:447
// @LINE:446
// @LINE:441
// @LINE:439
// @LINE:438
// @LINE:436
// @LINE:435
// @LINE:434
// @LINE:433
// @LINE:432
// @LINE:426
// @LINE:425
// @LINE:424
// @LINE:423
// @LINE:422
// @LINE:421
// @LINE:420
// @LINE:418
// @LINE:417
// @LINE:416
// @LINE:415
// @LINE:413
// @LINE:412
// @LINE:410
// @LINE:408
// @LINE:407
// @LINE:403
// @LINE:402
// @LINE:401
// @LINE:400
// @LINE:399
// @LINE:398
// @LINE:397
// @LINE:396
// @LINE:395
// @LINE:394
// @LINE:393
// @LINE:392
// @LINE:391
// @LINE:390
// @LINE:389
// @LINE:388
// @LINE:387
// @LINE:386
// @LINE:385
// @LINE:384
// @LINE:383
// @LINE:382
// @LINE:381
// @LINE:380
// @LINE:379
// @LINE:378
// @LINE:377
// @LINE:376
// @LINE:375
// @LINE:374
// @LINE:373
// @LINE:372
// @LINE:371
// @LINE:370
// @LINE:369
// @LINE:368
// @LINE:367
// @LINE:366
// @LINE:365
// @LINE:364
// @LINE:363
// @LINE:362
// @LINE:361
// @LINE:360
// @LINE:359
// @LINE:354
// @LINE:353
// @LINE:352
// @LINE:351
// @LINE:350
// @LINE:349
// @LINE:348
// @LINE:347
// @LINE:346
// @LINE:345
// @LINE:341
// @LINE:339
// @LINE:338
// @LINE:336
// @LINE:335
// @LINE:334
// @LINE:333
// @LINE:332
// @LINE:331
// @LINE:330
// @LINE:329
// @LINE:325
// @LINE:324
// @LINE:322
// @LINE:321
// @LINE:320
// @LINE:319
// @LINE:318
// @LINE:317
// @LINE:314
// @LINE:313
// @LINE:312
// @LINE:311
// @LINE:306
// @LINE:305
// @LINE:304
// @LINE:303
// @LINE:302
// @LINE:301
// @LINE:300
// @LINE:299
// @LINE:279
// @LINE:7
package api.javascript {

// @LINE:657
// @LINE:656
// @LINE:655
// @LINE:654
// @LINE:653
// @LINE:652
// @LINE:651
// @LINE:650
// @LINE:649
// @LINE:648
// @LINE:647
// @LINE:646
// @LINE:644
class ReversePreviews {
    

// @LINE:651
def editAnnotation : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.editAnnotation",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/annotationEdit"})
      }
   """
)
                        

// @LINE:652
def listAnnotations : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.listAnnotations",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/annotationsList"})
      }
   """
)
                        

// @LINE:650
def attachAnnotation : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.attachAnnotation",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/annotationAdd"})
      }
   """
)
                        

// @LINE:648
def uploadMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.uploadMetadata",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata"})
      }
   """
)
                        

// @LINE:649
def getMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.getMetadata",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata"})
      }
   """
)
                        

// @LINE:653
def setTitle : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.setTitle",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/title"})
      }
   """
)
                        

// @LINE:644
def downloadPreview : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.downloadPreview",
   """
      function(preview_id,datasetid) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("preview_id", preview_id) + "/textures/dataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetid", datasetid) + "/json"})
      }
   """
)
                        

// @LINE:657
def upload : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.upload",
   """
      function(iipKey) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews" + _qS([(iipKey == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("iipKey", iipKey))])})
      }
   """
)
                        

// @LINE:654
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews"})
      }
   """
)
                        

// @LINE:656
def removePreview : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.removePreview",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:655
def download : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.download",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:647
def attachTile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.attachTile",
   """
      function(dzi_id,tile_id,level) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("dzi_id", dzi_id) + "/tiles/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("tile_id", tile_id) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("level", encodeURIComponent(level))})
      }
   """
)
                        

// @LINE:646
def getTile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Previews.getTile",
   """
      function(dzi_id_dir,level,filename) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("dzi_id_dir", encodeURIComponent(dzi_id_dir)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("level", encodeURIComponent(level)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("filename", encodeURIComponent(filename))})
      }
   """
)
                        
    
}
              

// @LINE:926
class ReverseNotebooks {
    

// @LINE:926
def submit : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Notebooks.submit",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/notebook/submit"})
      }
   """
)
                        
    
}
              

// @LINE:885
// @LINE:884
// @LINE:883
// @LINE:882
// @LINE:881
// @LINE:880
// @LINE:879
// @LINE:878
// @LINE:877
// @LINE:876
// @LINE:875
// @LINE:874
class ReverseProxy {
    

// @LINE:885
// @LINE:884
// @LINE:883
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Proxy.delete",
   """
      function(endpoint_key, pathSuffix) {
      if (pathSuffix == """ + implicitly[JavascriptLitteral[String]].to(null) + """) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key))})
      }
      if (pathSuffix == """ + implicitly[JavascriptLitteral[String]].to(null) + """) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key)) + "/"})
      }
      if (true) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("pathSuffix", pathSuffix)})
      }
      }
   """
)
                        

// @LINE:879
// @LINE:878
// @LINE:877
def post : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Proxy.post",
   """
      function(endpoint_key, pathSuffix) {
      if (pathSuffix == """ + implicitly[JavascriptLitteral[String]].to(null) + """) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key))})
      }
      if (pathSuffix == """ + implicitly[JavascriptLitteral[String]].to(null) + """) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key)) + "/"})
      }
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("pathSuffix", pathSuffix)})
      }
      }
   """
)
                        

// @LINE:876
// @LINE:875
// @LINE:874
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Proxy.get",
   """
      function(endpoint_key, pathSuffix) {
      if (pathSuffix == """ + implicitly[JavascriptLitteral[String]].to(null) + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key))})
      }
      if (pathSuffix == """ + implicitly[JavascriptLitteral[String]].to(null) + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key)) + "/"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("pathSuffix", pathSuffix)})
      }
      }
   """
)
                        

// @LINE:882
// @LINE:881
// @LINE:880
def put : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Proxy.put",
   """
      function(endpoint_key, pathSuffix) {
      if (pathSuffix == """ + implicitly[JavascriptLitteral[String]].to(null) + """) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key))})
      }
      if (pathSuffix == """ + implicitly[JavascriptLitteral[String]].to(null) + """) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key)) + "/"})
      }
      if (true) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/proxy/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("endpoint_key", encodeURIComponent(endpoint_key)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("pathSuffix", pathSuffix)})
      }
      }
   """
)
                        
    
}
              

// @LINE:478
// @LINE:341
// @LINE:339
// @LINE:338
// @LINE:336
// @LINE:335
// @LINE:334
// @LINE:333
// @LINE:332
// @LINE:331
// @LINE:330
// @LINE:329
// @LINE:325
// @LINE:324
class ReverseMetadata {
    

// @LINE:329
def getDefinitions : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.getDefinitions",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata/definitions"})
      }
   """
)
                        

// @LINE:333
def getUrl : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.getUrl",
   """
      function(in_url) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata/url/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("in_url", in_url)})
      }
   """
)
                        

// @LINE:341
def getRepository : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.getRepository",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata/repository/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:338
def listPeople : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.listPeople",
   """
      function(term,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata/people" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("term", term), (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)])})
      }
   """
)
                        

// @LINE:331
def getAutocompleteName : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.getAutocompleteName",
   """
      function(filter) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata/autocompletenames" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("filter", filter)])})
      }
   """
)
                        

// @LINE:336
def deleteDefinition : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.deleteDefinition",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/metadata/definitions/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:324
def addUserMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.addUserMetadata",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata.jsonld"})
      }
   """
)
                        

// @LINE:335
def addDefinition : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.addDefinition",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata/definitions"})
      }
   """
)
                        

// @LINE:332
def getDefinition : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.getDefinition",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata/definitions/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:330
def getDefinitionsDistinctName : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.getDefinitionsDistinctName",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata/distinctdefinitions"})
      }
   """
)
                        

// @LINE:339
def getPerson : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.getPerson",
   """
      function(pid) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata/people/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("pid", encodeURIComponent(pid))})
      }
   """
)
                        

// @LINE:478
def addDefinitionToSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.addDefinitionToSpace",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata"})
      }
   """
)
                        

// @LINE:334
def editDefinition : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.editDefinition",
   """
      function(id,spaceId) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata/definitions/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(spaceId == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("spaceId", spaceId))])})
      }
   """
)
                        

// @LINE:325
def removeMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Metadata.removeMetadata",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/metadata.jsonld/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        
    
}
              

// @LINE:625
// @LINE:586
// @LINE:583
// @LINE:582
// @LINE:581
// @LINE:579
class ReverseFolders {
    

// @LINE:582
def deleteFolder : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Folders.deleteFolder",
   """
      function(parentDatasetId,folderId) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("parentDatasetId", parentDatasetId) + "/deleteFolder/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("folderId", folderId)})
      }
   """
)
                        

// @LINE:586
def getAllFoldersByDatasetId : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Folders.getAllFoldersByDatasetId",
   """
      function(datasetId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId) + "/folders"})
      }
   """
)
                        

// @LINE:583
def updateFolderName : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Folders.updateFolderName",
   """
      function(parentDatasetId,folderId) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("parentDatasetId", parentDatasetId) + "/updateName/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("folderId", folderId)})
      }
   """
)
                        

// @LINE:625
def createFolder : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Folders.createFolder",
   """
      function(parentDatasetId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("parentDatasetId", parentDatasetId) + "/newFolder"})
      }
   """
)
                        

// @LINE:581
def moveFileToDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Folders.moveFileToDataset",
   """
      function(datasetId,fromFolder,fileId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId) + "/moveToDataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("fromFolder", fromFolder) + "/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("fileId", fileId)})
      }
   """
)
                        

// @LINE:579
def moveFileBetweenFolders : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Folders.moveFileBetweenFolders",
   """
      function(datasetId,toFolder,fileId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId) + "/moveFile/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("toFolder", toFolder) + "/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("fileId", fileId)})
      }
   """
)
                        
    
}
              

// @LINE:306
// @LINE:305
// @LINE:304
// @LINE:303
// @LINE:302
// @LINE:301
// @LINE:300
// @LINE:299
// @LINE:279
class ReverseAdmin {
    

// @LINE:303
def deleteAllData : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Admin.deleteAllData",
   """
      function(resetAll) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/delete-data" + _qS([(resetAll == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("resetAll", resetAll))])})
      }
   """
)
                        

// @LINE:304
def submitAppearance : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Admin.submitAppearance",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/changeAppearance"})
      }
   """
)
                        

// @LINE:302
// @LINE:279
def sensorsConfig : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Admin.sensorsConfig",
   """
      function() {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sensors/config"})
      }
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sensors/config"})
      }
      }
   """
)
                        

// @LINE:305
def reindex : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Admin.reindex",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/reindex"})
      }
   """
)
                        

// @LINE:299
def mail : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Admin.mail",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/admin/mail"})
      }
   """
)
                        

// @LINE:306
def updateConfiguration : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Admin.updateConfiguration",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/admin/configuration"})
      }
   """
)
                        

// @LINE:301
def users : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Admin.users",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/admin/users"})
      }
   """
)
                        

// @LINE:300
def emailZipUpload : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Admin.emailZipUpload",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/admin/mailZip"})
      }
   """
)
                        
    
}
              

// @LINE:791
// @LINE:789
// @LINE:788
// @LINE:787
// @LINE:786
// @LINE:785
// @LINE:784
// @LINE:783
// @LINE:782
// @LINE:781
// @LINE:780
// @LINE:779
// @LINE:778
// @LINE:777
// @LINE:776
class ReverseUsers {
    

// @LINE:780
def updateName : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.updateName",
   """
      function(id,firstName,lastName) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/updateName" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("firstName", firstName), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("lastName", lastName)])})
      }
   """
)
                        

// @LINE:783
def keysAdd : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.keysAdd",
   """
      function(name) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/keys" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("name", name)])})
      }
   """
)
                        

// @LINE:791
def findById : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.findById",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:781
def keysList : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.keysList",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/keys"})
      }
   """
)
                        

// @LINE:779
def getUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.getUser",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/me"})
      }
   """
)
                        

// @LINE:776
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users"})
      }
   """
)
                        

// @LINE:787
// @LINE:786
def createNewListInUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.createNewListInUser",
   """
      function(email, field, fieldList) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/createNewListInUser" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("email", email), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("field", field), (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("fieldList", fieldList)])})
      }
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/createNewListInUser" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("email", email), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("field", field), (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("fieldList", fieldList)])})
      }
      }
   """
)
                        

// @LINE:777
def findByEmail : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.findByEmail",
   """
      function(email) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/email/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("email", encodeURIComponent(email))})
      }
   """
)
                        

// @LINE:785
def addUserDatasetView : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.addUserDatasetView",
   """
      function(email,dataset) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/addUserDatasetView" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("email", email), (""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("dataset", dataset)])})
      }
   """
)
                        

// @LINE:789
def unfollow : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.unfollow",
   """
      function(followeeUUID) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/unfollow" + _qS([(""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("followeeUUID", followeeUUID)])})
      }
   """
)
                        

// @LINE:778
def deleteUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.deleteUser",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:788
def follow : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.follow",
   """
      function(followeeUUID) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/follow" + _qS([(""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("followeeUUID", followeeUUID)])})
      }
   """
)
                        

// @LINE:782
def keysGet : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.keysGet",
   """
      function(name) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/keys/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name))})
      }
   """
)
                        

// @LINE:784
def keysDelete : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Users.keysDelete",
   """
      function(key) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/users/keys/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("key", encodeURIComponent(key))})
      }
   """
)
                        
    
}
              

// @LINE:354
// @LINE:353
// @LINE:352
// @LINE:351
// @LINE:350
// @LINE:349
// @LINE:348
// @LINE:347
// @LINE:346
// @LINE:345
class ReverseLogos {
    

// @LINE:354
def deletePath : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Logos.deletePath",
   """
      function(path,name) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/logos/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("path", encodeURIComponent(path)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name))})
      }
   """
)
                        

// @LINE:346
def upload : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Logos.upload",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/logos"})
      }
   """
)
                        

// @LINE:349
def downloadId : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Logos.downloadId",
   """
      function(id,default) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/logos/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/blob" + _qS([(default == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("default", default))])})
      }
   """
)
                        

// @LINE:345
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Logos.list",
   """
      function(path,name) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/logos" + _qS([(path == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("path", path)), (name == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("name", name))])})
      }
   """
)
                        

// @LINE:350
def deleteId : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Logos.deleteId",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/logos/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:348
def putId : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Logos.putId",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/logos/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:353
def downloadPath : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Logos.downloadPath",
   """
      function(path,name,default) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/logos/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("path", encodeURIComponent(path)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name)) + "/blob" + _qS([(default == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("default", default))])})
      }
   """
)
                        

// @LINE:347
def getId : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Logos.getId",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/logos/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:351
def getPath : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Logos.getPath",
   """
      function(path,name) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/logos/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("path", encodeURIComponent(path)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name))})
      }
   """
)
                        

// @LINE:352
def putPath : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Logos.putPath",
   """
      function(path,name) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/logos/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("path", encodeURIComponent(path)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name))})
      }
   """
)
                        
    
}
              

// @LINE:766
class ReverseGeometry {
    

// @LINE:766
def uploadGeometry : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geometry.uploadGeometry",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geometries"})
      }
   """
)
                        
    
}
              

// @LINE:674
// @LINE:673
// @LINE:672
// @LINE:671
// @LINE:670
// @LINE:669
// @LINE:668
// @LINE:667
// @LINE:666
// @LINE:665
// @LINE:664
class ReverseSections {
    

// @LINE:667
def attachThumbnail : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sections.attachThumbnail",
   """
      function(id,thumbnail_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/thumbnails/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("thumbnail_id", thumbnail_id)})
      }
   """
)
                        

// @LINE:674
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sections.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:672
// @LINE:670
def removeTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sections.removeTags",
   """
      function(id) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags/remove"})
      }
      if (true) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags"})
      }
      }
   """
)
                        

// @LINE:671
def removeAllTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sections.removeAllTags",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags/remove_all"})
      }
   """
)
                        

// @LINE:673
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sections.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:669
def addTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sections.addTags",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags"})
      }
   """
)
                        

// @LINE:664
def add : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sections.add",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections"})
      }
   """
)
                        

// @LINE:668
def getTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sections.getTags",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags"})
      }
   """
)
                        

// @LINE:666
def description : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sections.description",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/description"})
      }
   """
)
                        

// @LINE:665
def comment : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sections.comment",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/comments"})
      }
   """
)
                        
    
}
              

// @LINE:771
class ReverseInstitutions {
    

// @LINE:771
def addinstitution : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Institutions.addinstitution",
   """
      function(institution) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/institutions/addinstitution" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("institution", institution)])})
      }
   """
)
                        
    
}
              

// @LINE:739
// @LINE:738
// @LINE:737
// @LINE:736
class ReverseComments {
    

// @LINE:739
def editComment : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Comments.editComment",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comment/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/editComment"})
      }
   """
)
                        

// @LINE:737
def comment : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Comments.comment",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comment/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:736
def mentionInComment : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Comments.mentionInComment",
   """
      function(userID,resourceID,resourceName,resourceType,commenterId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comment/mention" + _qS([(""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("userID", userID), (""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("resourceID", resourceID), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("resourceName", resourceName), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("resourceType", resourceType), (""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("commenterId", commenterId)])})
      }
   """
)
                        

// @LINE:738
def removeComment : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Comments.removeComment",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/comment/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/removeComment"})
      }
   """
)
                        
    
}
              

// @LINE:977
class ReverseTree {
    

// @LINE:977
def getChildrenOfNode : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Tree.getChildrenOfNode",
   """
      function(nodeId,nodeType,mine,shared,public) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/tree/getChildrenOfNode" + _qS([(""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("nodeId", nodeId), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("nodeType", nodeType), (mine == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("mine", mine)), (shared == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("shared", shared)), (public == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("public", public))])})
      }
   """
)
                        
    
}
              

// @LINE:868
// @LINE:867
class ReverseVocabularyTerms {
    

// @LINE:868
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.VocabularyTerms.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabterms/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:867
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.VocabularyTerms.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabterms/list"})
      }
   """
)
                        
    
}
              

// @LINE:959
// @LINE:956
// @LINE:645
// @LINE:454
// @LINE:446
// @LINE:441
// @LINE:439
// @LINE:438
// @LINE:436
// @LINE:435
// @LINE:434
// @LINE:433
// @LINE:432
// @LINE:403
// @LINE:402
// @LINE:401
// @LINE:400
// @LINE:399
// @LINE:398
// @LINE:397
// @LINE:396
// @LINE:395
// @LINE:394
// @LINE:393
// @LINE:392
// @LINE:391
// @LINE:390
// @LINE:389
// @LINE:388
// @LINE:387
// @LINE:386
// @LINE:385
// @LINE:384
// @LINE:383
// @LINE:382
// @LINE:381
// @LINE:380
// @LINE:379
// @LINE:378
// @LINE:377
// @LINE:376
// @LINE:375
// @LINE:374
// @LINE:373
// @LINE:372
// @LINE:371
// @LINE:370
// @LINE:369
// @LINE:368
// @LINE:367
// @LINE:366
// @LINE:365
// @LINE:364
// @LINE:363
// @LINE:362
// @LINE:361
// @LINE:360
// @LINE:359
// @LINE:322
// @LINE:321
// @LINE:320
class ReverseFiles {
    

// @LINE:398
def updateFileName : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.updateFileName",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/filename"})
      }
   """
)
                        

// @LINE:380
def updateMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.updateMetadata",
   """
      function(id,extractor_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/updateMetadata" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("extractor_id", extractor_id)])})
      }
   """
)
                        

// @LINE:374
def getRDFUserMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getRDFUserMetadata",
   """
      function(id,mappingNum) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/rdfUserMetadata/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(mappingNum == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mappingNum", mappingNum))])})
      }
   """
)
                        

// @LINE:435
def getPreviews : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getPreviews",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/getPreviews"})
      }
   """
)
                        

// @LINE:360
def attachTexture : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.attachTexture",
   """
      function(three_d_file_id,texture_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("three_d_file_id", three_d_file_id) + "/3dTextures/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("texture_id", texture_id)})
      }
   """
)
                        

// @LINE:396
def updateLicense : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.updateLicense",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/license"})
      }
   """
)
                        

// @LINE:366
def searchFilesUserMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.searchFilesUserMetadata",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/searchusermetadata"})
      }
   """
)
                        

// @LINE:369
// @LINE:368
def uploadToDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.uploadToDataset",
   """
      function(id, showPreviews, originalZipFile, flags) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/uploadToDataset/withFlags/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("flags", encodeURIComponent(flags)) + _qS([(showPreviews == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("showPreviews", showPreviews)), (originalZipFile == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("originalZipFile", originalZipFile))])})
      }
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/uploadToDataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(showPreviews == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("showPreviews", showPreviews)), (originalZipFile == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("originalZipFile", originalZipFile)), (flags == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("flags", flags))])})
      }
      }
   """
)
                        

// @LINE:322
def removeMetadataJsonLD : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.removeMetadataJsonLD",
   """
      function(id,extractor) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata.jsonld" + _qS([(extractor == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("extractor", extractor))])})
      }
   """
)
                        

// @LINE:361
def attachThumbnail : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.attachThumbnail",
   """
      function(file_id,thumbnail_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id) + "/thumbnails/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("thumbnail_id", thumbnail_id)})
      }
   """
)
                        

// @LINE:446
def dumpFilesMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.dumpFilesMetadata",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/dumpFilesMd"})
      }
   """
)
                        

// @LINE:381
def addVersusMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.addVersusMetadata",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/versus_metadata"})
      }
   """
)
                        

// @LINE:372
def sendJob : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.sendJob",
   """
      function(fileId,fileType) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/sendJob/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("fileId", fileId) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("fileType", encodeURIComponent(fileType))})
      }
   """
)
                        

// @LINE:399
def reindex : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.reindex",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/reindex"})
      }
   """
)
                        

// @LINE:401
def paths : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.paths",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/paths"})
      }
   """
)
                        

// @LINE:436
def isBeingProcessed : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.isBeingProcessed",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/isBeingProcessed"})
      }
   """
)
                        

// @LINE:379
def getMetadataDefinitions : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getMetadataDefinitions",
   """
      function(id,space) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadataDefinitions" + _qS([(space == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space))])})
      }
   """
)
                        

// @LINE:390
// @LINE:376
def removeFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.removeFile",
   """
      function(id) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/remove"})
      }
      if (true) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
      }
   """
)
                        

// @LINE:382
def getVersusMetadataJSON : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getVersusMetadataJSON",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/versus_metadata"})
      }
   """
)
                        

// @LINE:400
def users : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.users",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/users"})
      }
   """
)
                        

// @LINE:395
// @LINE:393
def removeTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.removeTags",
   """
      function(id) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags/remove"})
      }
      if (true) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags"})
      }
      }
   """
)
                        

// @LINE:378
def addMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.addMetadata",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata"})
      }
   """
)
                        

// @LINE:432
def attachPreview : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.attachPreview",
   """
      function(id,p_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("p_id", p_id)})
      }
   """
)
                        

// @LINE:394
def removeAllTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.removeAllTags",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags/remove_all"})
      }
   """
)
                        

// @LINE:433
def getWithPreviewUrls : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getWithPreviewUrls",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/previewJson/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        

// @LINE:454
// @LINE:438
def getTexture : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getTexture",
   """
      function(three_d_file_id, filename) {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("three_d_file_id", three_d_file_id) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("filename", encodeURIComponent(filename))})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("three_d_file_id", three_d_file_id) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("filename", encodeURIComponent(filename))})
      }
      }
   """
)
                        

// @LINE:645
def downloadByDatasetAndFilename : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.downloadByDatasetAndFilename",
   """
      function(dataset_id,filename,preview_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("preview_id", preview_id) + "/textures/dataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("dataset_id", dataset_id) + "//" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("filename", encodeURIComponent(filename))})
      }
   """
)
                        

// @LINE:377
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata"})
      }
   """
)
                        

// @LINE:402
def changeOwner : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.changeOwner",
   """
      function(id,ownerId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/changeOwner/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("ownerId", ownerId)})
      }
   """
)
                        

// @LINE:362
def attachQueryThumbnail : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.attachQueryThumbnail",
   """
      function(file_id,thumbnail_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/queries/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id) + "/thumbnails/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("thumbnail_id", thumbnail_id)})
      }
   """
)
                        

// @LINE:373
def getRDFURLsForFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getRDFURLsForFile",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/getRDFURLsForFile/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:392
def addTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.addTags",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags"})
      }
   """
)
                        

// @LINE:367
def searchFilesGeneralMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.searchFilesGeneralMetadata",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/searchmetadata"})
      }
   """
)
                        

// @LINE:384
def addUserMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.addUserMetadata",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/usermetadata"})
      }
   """
)
                        

// @LINE:391
def getTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getTags",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags"})
      }
   """
)
                        

// @LINE:365
// @LINE:364
def upload : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.upload",
   """
      function(showPreviews, originalZipFile, flags) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files" + _qS([(showPreviews == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("showPreviews", showPreviews)), (originalZipFile == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("originalZipFile", originalZipFile)), (flags == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("flags", flags))])})
      }
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/withFlags/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("flags", encodeURIComponent(flags)) + _qS([(showPreviews == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("showPreviews", showPreviews)), (originalZipFile == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("originalZipFile", originalZipFile))])})
      }
      }
   """
)
                        

// @LINE:389
def comment : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.comment",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/comment"})
      }
   """
)
                        

// @LINE:363
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files"})
      }
   """
)
                        

// @LINE:434
def filePreviewsList : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.filePreviewsList",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/listpreviews"})
      }
   """
)
                        

// @LINE:359
def attachGeometry : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.attachGeometry",
   """
      function(three_d_file_id,geometry_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("three_d_file_id", three_d_file_id) + "/geometries/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("geometry_id", geometry_id)})
      }
   """
)
                        

// @LINE:320
def addMetadataJsonLD : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.addMetadataJsonLD",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata.jsonld"})
      }
   """
)
                        

// @LINE:371
def uploadIntermediate : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.uploadIntermediate",
   """
      function(idAndFlags) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/uploadIntermediate/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("idAndFlags", encodeURIComponent(idAndFlags))})
      }
   """
)
                        

// @LINE:441
// @LINE:375
def download : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.download",
   """
      function(id) {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/blob"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
      }
   """
)
                        

// @LINE:386
def getXMLMetadataJSON : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getXMLMetadataJSON",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/xmlmetadatajson"})
      }
   """
)
                        

// @LINE:959
// @LINE:370
def updateDescription : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.updateDescription",
   """
      function(id) {
      if (true) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/updateDescription"})
      }
      if (true) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/updateDescription"})
      }
      }
   """
)
                        

// @LINE:403
def getOwner : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getOwner",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/getOwner"})
      }
   """
)
                        

// @LINE:956
def uploadToDatasetWithDescription : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.uploadToDatasetWithDescription",
   """
      function(id,showPreviews,originalZipFile,flags) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/uploadToDataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(showPreviews == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("showPreviews", showPreviews)), (originalZipFile == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("originalZipFile", originalZipFile)), (flags == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("flags", flags))])})
      }
   """
)
                        

// @LINE:388
def unfollow : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.unfollow",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/unfollow"})
      }
   """
)
                        

// @LINE:439
def downloadquery : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.downloadquery",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/queries/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:321
def getMetadataJsonLD : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getMetadataJsonLD",
   """
      function(id,extractor) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata.jsonld" + _qS([(extractor == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("extractor", extractor))])})
      }
   """
)
                        

// @LINE:397
def extract : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.extract",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/extracted_metadata"})
      }
   """
)
                        

// @LINE:387
def follow : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.follow",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/follow"})
      }
   """
)
                        

// @LINE:385
def getTechnicalMetadataJSON : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getTechnicalMetadataJSON",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/technicalmetadatajson"})
      }
   """
)
                        

// @LINE:383
def getUserMetadataJSON : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Files.getUserMetadataJSON",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/usermetadatajson"})
      }
   """
)
                        
    
}
              

// @LINE:7
class ReverseApiHelp {
    

// @LINE:7
def options : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.ApiHelp.options",
   """
      function(path) {
      return _wA({method:"OPTIONS", url:"""" + _prefix + { _defaultPrefix } + """" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("path", path)})
      }
   """
)
                        
    
}
              

// @LINE:722
// @LINE:721
// @LINE:720
class ReverseThumbnails {
    

// @LINE:722
def removeThumbnail : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Thumbnails.removeThumbnail",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/thumbnails/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:721
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Thumbnails.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/thumbnails"})
      }
   """
)
                        

// @LINE:720
def uploadThumbnail : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Thumbnails.uploadThumbnail",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/fileThumbnail"})
      }
   """
)
                        
    
}
              

// @LINE:659
// @LINE:658
class ReverseIndexes {
    

// @LINE:659
def features : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Indexes.features",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/indexes/features"})
      }
   """
)
                        

// @LINE:658
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Indexes.index",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/indexes"})
      }
   """
)
                        
    
}
              

// @LINE:554
// @LINE:553
// @LINE:552
// @LINE:551
// @LINE:550
// @LINE:549
// @LINE:548
// @LINE:547
// @LINE:546
// @LINE:545
// @LINE:544
// @LINE:543
// @LINE:542
// @LINE:541
// @LINE:539
// @LINE:538
// @LINE:537
// @LINE:536
// @LINE:535
// @LINE:533
// @LINE:530
// @LINE:529
// @LINE:527
// @LINE:526
// @LINE:524
// @LINE:522
// @LINE:521
// @LINE:520
// @LINE:518
// @LINE:517
// @LINE:516
// @LINE:511
// @LINE:500
// @LINE:499
// @LINE:498
// @LINE:497
// @LINE:496
// @LINE:495
// @LINE:494
// @LINE:493
// @LINE:492
// @LINE:491
// @LINE:490
// @LINE:489
// @LINE:488
class ReverseCollections {
    

// @LINE:553
def changeOwnerDatasetOnly : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.changeOwnerDatasetOnly",
   """
      function(ds_id,userId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("ds_id", ds_id) + "/changeOwnerDataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("userId", userId)})
      }
   """
)
                        

// @LINE:511
def uploadZipToSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.uploadZipToSpace",
   """
      function(spaceId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/uploadZipToSpace" + _qS([(spaceId == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("spaceId", spaceId))])})
      }
   """
)
                        

// @LINE:548
def getParentCollections : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.getParentCollections",
   """
      function(coll_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/getParentCollections"})
      }
   """
)
                        

// @LINE:492
def createCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.createCollection",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections"})
      }
   """
)
                        

// @LINE:491
def listPossibleParents : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.listPossibleParents",
   """
      function(currentCollectionId,title,date,limit,exact) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/possibleParents" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("currentCollectionId", currentCollectionId), (title == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("title", title)), (date == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("date", date)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (exact == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("exact", exact))])})
      }
   """
)
                        

// @LINE:530
def reindex : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.reindex",
   """
      function(coll_id,recursive) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/reindex" + _qS([(recursive == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("recursive", recursive))])})
      }
   """
)
                        

// @LINE:550
def removeFromSpaceAllowed : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.removeFromSpaceAllowed",
   """
      function(coll_id,space_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/removeFromSpaceAllowed/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("space_id", space_id)})
      }
   """
)
                        

// @LINE:549
def attachSubCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.attachSubCollection",
   """
      function(coll_id,sub_coll_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/addSubCollection/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("sub_coll_id", sub_coll_id)})
      }
   """
)
                        

// @LINE:544
def createCollectionWithParent : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.createCollectionWithParent",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/newCollectionWithParent"})
      }
   """
)
                        

// @LINE:545
def getChildCollectionIds : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.getChildCollectionIds",
   """
      function(coll_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/getChildCollectionIds"})
      }
   """
)
                        

// @LINE:497
def listCollectionsInTrash : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.listCollectionsInTrash",
   """
      function(limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/listTrash" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:516
def moveChildCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.moveChildCollection",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/moveChildCollection"})
      }
   """
)
                        

// @LINE:518
def moveDatasetToNewCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.moveDatasetToNewCollection",
   """
      function(oldCollection,dataset,newCollection) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("oldCollection", encodeURIComponent(oldCollection)) + "/moveDataset/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("dataset", encodeURIComponent(dataset)) + "/newCollection/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("newCollection", encodeURIComponent(newCollection))})
      }
   """
)
                        

// @LINE:499
def clearOldCollectionsTrash : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.clearOldCollectionsTrash",
   """
      function(days) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/clearOldCollectionsTrash" + _qS([(days == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("days", days))])})
      }
   """
)
                        

// @LINE:538
def updateCollectionName : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.updateCollectionName",
   """
      function(coll_id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/title"})
      }
   """
)
                        

// @LINE:539
def updateCollectionDescription : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.updateCollectionDescription",
   """
      function(coll_id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/description"})
      }
   """
)
                        

// @LINE:520
// @LINE:488
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.list",
   """
      function(title, date, limit, exact) {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections" + _qS([(title == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("title", title)), (date == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("date", date)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (exact == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("exact", exact))])})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/list" + _qS([(title == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("title", title)), (date == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("date", date)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (exact == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("exact", exact))])})
      }
      }
   """
)
                        

// @LINE:541
def removeSubCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.removeSubCollection",
   """
      function(coll_id,sub_coll_id,ignoreNotFound) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/removeSubCollection/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("sub_coll_id", sub_coll_id) + _qS([(ignoreNotFound == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("ignoreNotFound", ignoreNotFound))])})
      }
   """
)
                        

// @LINE:543
def unsetRootSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.unsetRootSpace",
   """
      function(coll_id,spaceId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/unsetRootFlag/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId)})
      }
   """
)
                        

// @LINE:527
// @LINE:526
def removeDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.removeDataset",
   """
      function(coll_id, ds_id, ignoreNotFound) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/datasetsRemove/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("ds_id", ds_id) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("ignoreNotFound", encodeURIComponent(ignoreNotFound))})
      }
      if (true) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("ds_id", ds_id) + _qS([(ignoreNotFound == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("ignoreNotFound", ignoreNotFound))])})
      }
      }
   """
)
                        

// @LINE:536
def attachPreview : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.attachPreview",
   """
      function(c_id,p_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("c_id", c_id) + "/previews/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("p_id", p_id)})
      }
   """
)
                        

// @LINE:517
def removeCollectionAndContents : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.removeCollectionAndContents",
   """
      function(collectionId) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("collectionId", collectionId) + "/deleteCollectionAndContents"})
      }
   """
)
                        

// @LINE:546
def getChildCollections : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.getChildCollections",
   """
      function(coll_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/getChildCollections"})
      }
   """
)
                        

// @LINE:500
def restoreCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.restoreCollection",
   """
      function(collectionId) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/restore/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("collectionId", collectionId)})
      }
   """
)
                        

// @LINE:551
def changeOwner : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.changeOwner",
   """
      function(coll_id,userId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/changeOwner/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("userId", userId)})
      }
   """
)
                        

// @LINE:533
// @LINE:529
def removeCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.removeCollection",
   """
      function(coll_id) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/remove"})
      }
      if (true) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id)})
      }
      }
   """
)
                        

// @LINE:495
def getAllCollections : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.getAllCollections",
   """
      function(limit,showAll) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/allCollections" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (showAll == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("showAll", showAll))])})
      }
   """
)
                        

// @LINE:494
def getTopLevelCollections : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.getTopLevelCollections",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/topLevelCollections"})
      }
   """
)
                        

// @LINE:489
def listCanEdit : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.listCanEdit",
   """
      function(title,date,limit,exact) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/canEdit" + _qS([(title == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("title", title)), (date == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("date", date)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (exact == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("exact", exact))])})
      }
   """
)
                        

// @LINE:537
def getCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.getCollection",
   """
      function(coll_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id)})
      }
   """
)
                        

// @LINE:542
def setRootSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.setRootSpace",
   """
      function(coll_id,spaceId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/rootFlag/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId)})
      }
   """
)
                        

// @LINE:496
def download : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.download",
   """
      function(id,compression) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/download" + _qS([(compression == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("compression", compression))])})
      }
   """
)
                        

// @LINE:493
def getRootCollections : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.getRootCollections",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/rootCollections"})
      }
   """
)
                        

// @LINE:554
def changeOwnerFileOnly : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.changeOwnerFileOnly",
   """
      function(file_id,userId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id) + "/changeOwnerFile/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("userId", userId)})
      }
   """
)
                        

// @LINE:524
def attachDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.attachDataset",
   """
      function(coll_id,ds_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("ds_id", ds_id)})
      }
   """
)
                        

// @LINE:490
def addDatasetToCollectionOptions : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.addDatasetToCollectionOptions",
   """
      function(ds_id,title,date,limit,exact) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/datasetPossibleParents/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("ds_id", ds_id) + _qS([(title == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("title", title)), (date == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("date", date)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (exact == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("exact", exact))])})
      }
   """
)
                        

// @LINE:552
def changeOwnerCollectionOnly : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.changeOwnerCollectionOnly",
   """
      function(coll_id,userId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/changeOwnerCollection/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("userId", userId)})
      }
   """
)
                        

// @LINE:535
def deleteCollectionAndContents : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.deleteCollectionAndContents",
   """
      function(coll_id,userid) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/deleteContents/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("userid", userid)})
      }
   """
)
                        

// @LINE:547
def getParentCollectionIds : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.getParentCollectionIds",
   """
      function(coll_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/getParentCollectionIds"})
      }
   """
)
                        

// @LINE:498
def emptyTrash : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.emptyTrash",
   """
      function() {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/emptyTrash"})
      }
   """
)
                        

// @LINE:522
def unfollow : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.unfollow",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/unfollow"})
      }
   """
)
                        

// @LINE:521
def follow : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Collections.follow",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/follow"})
      }
   """
)
                        
    
}
              

// @LINE:960
// @LINE:631
// @LINE:630
// @LINE:629
// @LINE:628
// @LINE:627
// @LINE:626
// @LINE:624
// @LINE:623
// @LINE:622
// @LINE:621
// @LINE:620
// @LINE:619
// @LINE:618
// @LINE:617
// @LINE:616
// @LINE:615
// @LINE:614
// @LINE:613
// @LINE:612
// @LINE:611
// @LINE:610
// @LINE:609
// @LINE:608
// @LINE:607
// @LINE:606
// @LINE:605
// @LINE:604
// @LINE:603
// @LINE:602
// @LINE:601
// @LINE:600
// @LINE:599
// @LINE:598
// @LINE:597
// @LINE:596
// @LINE:595
// @LINE:594
// @LINE:593
// @LINE:592
// @LINE:591
// @LINE:590
// @LINE:589
// @LINE:588
// @LINE:587
// @LINE:585
// @LINE:584
// @LINE:580
// @LINE:578
// @LINE:577
// @LINE:576
// @LINE:575
// @LINE:574
// @LINE:573
// @LINE:572
// @LINE:571
// @LINE:570
// @LINE:569
// @LINE:568
// @LINE:566
// @LINE:565
// @LINE:564
// @LINE:563
// @LINE:562
// @LINE:561
// @LINE:560
// @LINE:532
// @LINE:523
// @LINE:448
// @LINE:447
// @LINE:319
// @LINE:318
// @LINE:317
class ReverseDatasets {
    

// @LINE:624
def detachAndDeleteDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.detachAndDeleteDataset",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/detachdelete"})
      }
   """
)
                        

// @LINE:588
def getRDFURLsForDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.getRDFURLsForDataset",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/getRDFURLsForDataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:589
def getRDFUserMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.getRDFUserMetadata",
   """
      function(id,mappingNum) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/rdfUserMetadata/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + _qS([(mappingNum == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("mappingNum", mappingNum))])})
      }
   """
)
                        

// @LINE:574
def listDatasetsInTrash : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.listDatasetsInTrash",
   """
      function(limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/listTrash" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:631
def hasAttachedVocabulary : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.hasAttachedVocabulary",
   """
      function(datasetId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/hasVocabulary/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId)})
      }
   """
)
                        

// @LINE:620
def getPreviews : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.getPreviews",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/getPreviews"})
      }
   """
)
                        

// @LINE:603
def reindex : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.reindex",
   """
      function(id,recursive) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/reindex" + _qS([(recursive == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("recursive", recursive))])})
      }
   """
)
                        

// @LINE:572
def searchDatasetsGeneralMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.searchDatasetsGeneralMetadata",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/searchmetadata"})
      }
   """
)
                        

// @LINE:606
def removeTag : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.removeTag",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/removeTag"})
      }
   """
)
                        

// @LINE:618
def updateLicense : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.updateLicense",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/license"})
      }
   """
)
                        

// @LINE:562
def listMoveFileToDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.listMoveFileToDataset",
   """
      function(file_id,title,limit,exact) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/moveFileToDataset" + _qS([(""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("file_id", file_id), (title == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("title", title)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (exact == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("exact", exact))])})
      }
   """
)
                        

// @LINE:621
def attachExistingFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.attachExistingFile",
   """
      function(ds_id,file_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("ds_id", ds_id) + "/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id)})
      }
   """
)
                        

// @LINE:598
// @LINE:597
def datasetAllFilesList : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.datasetAllFilesList",
   """
      function(id, max) {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/listAllFiles" + _qS([(max == null ? null : (""" + implicitly[QueryStringBindable[Option[Int]]].javascriptUnbind + """)("max", max))])})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/files" + _qS([(max == null ? null : (""" + implicitly[QueryStringBindable[Option[Int]]].javascriptUnbind + """)("max", max))])})
      }
      }
   """
)
                        

// @LINE:319
def removeMetadataJsonLD : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.removeMetadataJsonLD",
   """
      function(id,extractor) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata.jsonld" + _qS([(extractor == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("extractor", extractor))])})
      }
   """
)
                        

// @LINE:587
// @LINE:578
def detachFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.detachFile",
   """
      function(ds_id, file_id, ignoreNotFound) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("ds_id", ds_id) + "/filesRemove/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("ignoreNotFound", encodeURIComponent(ignoreNotFound))})
      }
      if (true) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("ds_id", ds_id) + "/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id) + _qS([(ignoreNotFound == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("ignoreNotFound", ignoreNotFound))])})
      }
      }
   """
)
                        

// @LINE:627
def addFileEvent : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.addFileEvent",
   """
      function(id,inFolder,fileCount) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/addFileEvent" + _qS([(""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("inFolder", inFolder), (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("fileCount", fileCount)])})
      }
   """
)
                        

// @LINE:570
def attachMultipleFiles : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.attachMultipleFiles",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/attachmultiple"})
      }
   """
)
                        

// @LINE:616
def moveCreator : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.moveCreator",
   """
      function(id,creator,newPos) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/creator/reorder" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("creator", creator), (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("newPos", newPos)])})
      }
   """
)
                        

// @LINE:560
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.list",
   """
      function(title,date,limit,exact) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets" + _qS([(title == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("title", title)), (date == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("date", date)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (exact == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("exact", exact))])})
      }
   """
)
                        

// @LINE:448
def dumpDatasetGroupings : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.dumpDatasetGroupings",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/dumpDatasetGroupings"})
      }
   """
)
                        

// @LINE:619
def isBeingProcessed : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.isBeingProcessed",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/isBeingProcessed"})
      }
   """
)
                        

// @LINE:590
def getMetadataDefinitions : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.getMetadataDefinitions",
   """
      function(id,space) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata" + _qS([(space == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("space", space))])})
      }
   """
)
                        

// @LINE:960
// @LINE:532
// @LINE:523
def listInCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.listInCollection",
   """
      function(coll_id) {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/datasets"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/getDatasets"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/datasets"})
      }
      }
   """
)
                        

// @LINE:614
def addCreator : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.addCreator",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/creator"})
      }
   """
)
                        

// @LINE:577
// @LINE:566
def restoreDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.restoreDataset",
   """
      function(id) {
      if (true) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/restore/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
      if (true) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/restore/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
      }
   """
)
                        

// @LINE:580
def moveFileBetweenDatasets : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.moveFileBetweenDatasets",
   """
      function(datasetId,toDataset,fileId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId) + "/moveBetweenDatasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("toDataset", toDataset) + "/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("fileId", fileId)})
      }
   """
)
                        

// @LINE:628
def users : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.users",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/users"})
      }
   """
)
                        

// @LINE:611
// @LINE:609
def removeTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.removeTags",
   """
      function(id) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags/remove"})
      }
      if (true) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags"})
      }
      }
   """
)
                        

// @LINE:564
def createEmptyVocabularyForDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.createEmptyVocabularyForDataset",
   """
      function(datasetId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/newVocabulary/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId)})
      }
   """
)
                        

// @LINE:591
def addMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.addMetadata",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata"})
      }
   """
)
                        

// @LINE:599
def uploadToDatasetFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.uploadToDatasetFile",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/files"})
      }
   """
)
                        

// @LINE:610
def removeAllTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.removeAllTags",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags/remove_all"})
      }
   """
)
                        

// @LINE:585
def copyDatasetToSpaceNotAuthor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.copyDatasetToSpaceNotAuthor",
   """
      function(datasetId,spaceId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId) + "/copyDatasetToSpaceNotAuthor/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId)})
      }
   """
)
                        

// @LINE:584
def copyDatasetToSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.copyDatasetToSpace",
   """
      function(datasetId,spaceId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId) + "/copyDatasetToSpace/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId)})
      }
   """
)
                        

// @LINE:622
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:563
def createDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.createDataset",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets"})
      }
   """
)
                        

// @LINE:629
def changeOwner : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.changeOwner",
   """
      function(datasetId,userId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId) + "/changeOwner/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("userId", userId)})
      }
   """
)
                        

// @LINE:571
def searchDatasetsUserMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.searchDatasetsUserMetadata",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/searchusermetadata"})
      }
   """
)
                        

// @LINE:608
def addTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.addTags",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags"})
      }
   """
)
                        

// @LINE:615
def removeCreator : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.removeCreator",
   """
      function(id,creator) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/creator/remove" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("creator", creator)])})
      }
   """
)
                        

// @LINE:592
def addUserMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.addUserMetadata",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/usermetadata"})
      }
   """
)
                        

// @LINE:561
def listCanEdit : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.listCanEdit",
   """
      function(title,date,limit,exact) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/canEdit" + _qS([(title == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("title", title)), (date == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("date", date)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit)), (exact == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("exact", exact))])})
      }
   """
)
                        

// @LINE:569
def createEmptyDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.createEmptyDataset",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/createempty"})
      }
   """
)
                        

// @LINE:607
def getTags : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.getTags",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/tags"})
      }
   """
)
                        

// @LINE:612
def updateName : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.updateName",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/title"})
      }
   """
)
                        

// @LINE:602
def comment : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.comment",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/comment"})
      }
   """
)
                        

// @LINE:601
def download : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.download",
   """
      function(id,compression) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/download" + _qS([(compression == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("compression", compression))])})
      }
   """
)
                        

// @LINE:600
def uploadToDatasetJSON : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.uploadToDatasetJSON",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/urls"})
      }
   """
)
                        

// @LINE:565
def markDatasetAsTrash : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.markDatasetAsTrash",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/trash/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:596
def datasetFilesList : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.datasetFilesList",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/listFiles"})
      }
   """
)
                        

// @LINE:317
def addMetadataJsonLD : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.addMetadataJsonLD",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata.jsonld"})
      }
   """
)
                        

// @LINE:626
def updateAccess : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.updateAccess",
   """
      function(id,aceess) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/access" + _qS([(aceess == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("aceess", aceess))])})
      }
   """
)
                        

// @LINE:594
def getXMLMetadataJSON : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.getXMLMetadataJSON",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/xmlmetadatajson"})
      }
   """
)
                        

// @LINE:613
def updateDescription : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.updateDescription",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/description"})
      }
   """
)
                        

// @LINE:617
def updateInformation : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.updateInformation",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/editing"})
      }
   """
)
                        

// @LINE:623
def deleteDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.deleteDataset",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:575
// @LINE:568
def emptyTrash : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.emptyTrash",
   """
      function() {
      if (true) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/emptytrash"})
      }
      if (true) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/emptyTrash"})
      }
      }
   """
)
                        

// @LINE:605
def unfollow : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.unfollow",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/unfollow"})
      }
   """
)
                        

// @LINE:318
def getMetadataJsonLD : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.getMetadataJsonLD",
   """
      function(id,extractor) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata.jsonld" + _qS([(extractor == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("extractor", extractor))])})
      }
   """
)
                        

// @LINE:604
def follow : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.follow",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/follow"})
      }
   """
)
                        

// @LINE:593
def getTechnicalMetadataJSON : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.getTechnicalMetadataJSON",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/technicalmetadatajson"})
      }
   """
)
                        

// @LINE:447
def dumpDatasetsMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.dumpDatasetsMetadata",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/dumpDatasetsMd"})
      }
   """
)
                        

// @LINE:630
def moveDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.moveDataset",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/moveCollection"})
      }
   """
)
                        

// @LINE:576
def clearOldDatasetsTrash : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.clearOldDatasetsTrash",
   """
      function(days) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/clearOldDatasetsTrash" + _qS([(days == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("days", days))])})
      }
   """
)
                        

// @LINE:573
def listOutsideCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.listOutsideCollection",
   """
      function(coll_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/listOutsideCollection/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id)})
      }
   """
)
                        

// @LINE:595
def getUserMetadataJSON : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Datasets.getUserMetadataJSON",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/usermetadatajson"})
      }
   """
)
                        
    
}
              

// @LINE:751
// @LINE:750
// @LINE:749
// @LINE:748
// @LINE:747
// @LINE:746
// @LINE:745
class ReverseSelected {
    

// @LINE:750
def clearAll : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Selected.clearAll",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/selected/clear"})
      }
   """
)
                        

// @LINE:749
def downloadAll : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Selected.downloadAll",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/selected/files"})
      }
   """
)
                        

// @LINE:747
def remove : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Selected.remove",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/selected/remove"})
      }
   """
)
                        

// @LINE:748
def deleteAll : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Selected.deleteAll",
   """
      function() {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/selected/files"})
      }
   """
)
                        

// @LINE:746
def add : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Selected.add",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/selected"})
      }
   """
)
                        

// @LINE:745
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Selected.get",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/selected"})
      }
   """
)
                        

// @LINE:751
def tagAll : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Selected.tagAll",
   """
      function(tags) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/selected/tag" + _qS([(""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("tags", tags)])})
      }
   """
)
                        
    
}
              

// @LINE:426
// @LINE:425
// @LINE:424
// @LINE:423
// @LINE:422
// @LINE:421
// @LINE:420
// @LINE:418
// @LINE:417
// @LINE:416
// @LINE:415
// @LINE:413
// @LINE:412
// @LINE:410
// @LINE:408
// @LINE:407
class ReverseExtractions {
    

// @LINE:410
def addExtractorInfo : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.addExtractorInfo",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractors"})
      }
   """
)
                        

// @LINE:416
def getExtractorServersIP : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.getExtractorServersIP",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/servers_ips"})
      }
   """
)
                        

// @LINE:415
def getDTSRequests : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.getDTSRequests",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/requests"})
      }
   """
)
                        

// @LINE:422
def multipleUploadByURL : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.multipleUploadByURL",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/multiple_uploadby_url"})
      }
   """
)
                        

// @LINE:425
def fetch : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.fetch",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadata"})
      }
   """
)
                        

// @LINE:423
def uploadExtract : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.uploadExtract",
   """
      function(showPreviews,extract) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/upload_file" + _qS([(showPreviews == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("showPreviews", showPreviews)), (extract == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("extract", extract))])})
      }
   """
)
                        

// @LINE:408
def getExtractorInfo : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.getExtractorInfo",
   """
      function(name) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name))})
      }
   """
)
                        

// @LINE:412
def submitFileToExtractor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.submitFileToExtractor",
   """
      function(file_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id) + "/extractions"})
      }
   """
)
                        

// @LINE:424
def checkExtractorsStatus : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.checkExtractorsStatus",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/status"})
      }
   """
)
                        

// @LINE:413
def submitDatasetToExtractor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.submitDatasetToExtractor",
   """
      function(ds_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/datasets/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("ds_id", ds_id) + "/extractions"})
      }
   """
)
                        

// @LINE:418
def getExtractorInputTypes : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.getExtractorInputTypes",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/supported_input_types"})
      }
   """
)
                        

// @LINE:407
def listExtractors : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.listExtractors",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractors"})
      }
   """
)
                        

// @LINE:420
def getExtractorDetails : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.getExtractorDetails",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/extractors_details"})
      }
   """
)
                        

// @LINE:421
def uploadByURL : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.uploadByURL",
   """
      function(extract) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/upload_url" + _qS([(extract == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("extract", extract))])})
      }
   """
)
                        

// @LINE:426
def checkExtractionsStatuses : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.checkExtractionsStatuses",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/statuses"})
      }
   """
)
                        

// @LINE:417
def getExtractorNames : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Extractions.getExtractorNames",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/extractions/extractors_names"})
      }
   """
)
                        
    
}
              

// @LINE:840
class ReverseEvents {
    

// @LINE:840
def sendExceptionEmail : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Events.sendExceptionEmail",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "sendEmail"})
      }
   """
)
                        
    
}
              

// @LINE:715
// @LINE:714
// @LINE:713
// @LINE:712
// @LINE:711
// @LINE:710
// @LINE:709
// @LINE:708
// @LINE:707
// @LINE:706
// @LINE:705
// @LINE:704
// @LINE:703
// @LINE:702
// @LINE:701
// @LINE:700
// @LINE:699
// @LINE:698
// @LINE:697
// @LINE:696
// @LINE:695
// @LINE:694
// @LINE:693
// @LINE:692
// @LINE:691
// @LINE:690
// @LINE:689
// @LINE:688
// @LINE:687
// @LINE:686
class ReverseGeostreams {
    

// @LINE:708
def getStream : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.getStream",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/streams/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:692
def binDatapoints : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.binDatapoints",
   """
      function(time,depth,raw,since,until,geocode,stream_id,sensor_id,sources,attributes) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/datapoints/bin/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("time", encodeURIComponent(time)) + "/" + (""" + implicitly[PathBindable[Double]].javascriptUnbind + """)("depth", depth) + _qS([(raw == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("raw", raw)), (since == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("since", since)), (until == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("until", until)), (geocode == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geocode", geocode)), (stream_id == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("stream_id", stream_id)), (sensor_id == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("sensor_id", sensor_id)), (sources == null ? null : (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("sources", sources)), (attributes == null ? null : (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("attributes", attributes))])})
      }
   """
)
                        

// @LINE:707
// @LINE:698
def updateStatisticsStreamSensor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.updateStatisticsStreamSensor",
   """
      function() {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/sensors/update"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/streams/update"})
      }
      }
   """
)
                        

// @LINE:694
def cacheListAction : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.cacheListAction",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/cache"})
      }
   """
)
                        

// @LINE:686
def addDatapoint : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.addDatapoint",
   """
      function(invalidateCache) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/datapoints" + _qS([(invalidateCache == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("invalidateCache", invalidateCache))])})
      }
   """
)
                        

// @LINE:702
def getSensorStreams : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.getSensorStreams",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/sensors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/streams"})
      }
   """
)
                        

// @LINE:704
def searchSensors : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.searchSensors",
   """
      function(geocode,sensor_name,geojson) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/sensors" + _qS([(geocode == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geocode", geocode)), (sensor_name == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("sensor_name", sensor_name)), (geojson == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geojson", geojson))])})
      }
   """
)
                        

// @LINE:701
def getSensorStatistics : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.getSensorStatistics",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/sensors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/stats"})
      }
   """
)
                        

// @LINE:706
def createStream : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.createStream",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/streams"})
      }
   """
)
                        

// @LINE:687
def addDatapoints : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.addDatapoints",
   """
      function(invalidateCache) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/datapoints/bulk" + _qS([(invalidateCache == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("invalidateCache", invalidateCache))])})
      }
   """
)
                        

// @LINE:696
def cacheFetchAction : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.cacheFetchAction",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/cache/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:715
def getConfig : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.getConfig",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/config"})
      }
   """
)
                        

// @LINE:691
// @LINE:690
// @LINE:689
def searchDatapoints : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.searchDatapoints",
   """
      function(operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson) {
      if (operator == """ + implicitly[JavascriptLitteral[String]].to("") + """ && window_start == """ + implicitly[JavascriptLitteral[Option[String]]].to(None) + """ && window_end == """ + implicitly[JavascriptLitteral[Option[String]]].to(None) + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/datapoints" + _qS([(since == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("since", since)), (until == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("until", until)), (geocode == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geocode", geocode)), (stream_id == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("stream_id", stream_id)), (sensor_id == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("sensor_id", sensor_id)), (sources == null ? null : (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("sources", sources)), (attributes == null ? null : (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("attributes", attributes)), (format == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("format", format)), (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("semi", semi), (onlyCount == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("onlyCount", onlyCount)), (binning == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("binning", binning)), (geojson == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geojson", geojson))])})
      }
      if (operator == """ + implicitly[JavascriptLitteral[String]].to("averages") + """ && window_start == """ + implicitly[JavascriptLitteral[Option[String]]].to(None) + """ && window_end == """ + implicitly[JavascriptLitteral[Option[String]]].to(None) + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/datapoints/averages" + _qS([(since == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("since", since)), (until == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("until", until)), (geocode == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geocode", geocode)), (stream_id == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("stream_id", stream_id)), (sensor_id == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("sensor_id", sensor_id)), (sources == null ? null : (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("sources", sources)), (attributes == null ? null : (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("attributes", attributes)), (format == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("format", format)), (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("semi", semi), (onlyCount == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("onlyCount", onlyCount)), (binning == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("binning", binning)), (geojson == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geojson", geojson))])})
      }
      if (operator == """ + implicitly[JavascriptLitteral[String]].to("trends") + """) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/datapoints/trends" + _qS([(since == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("since", since)), (until == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("until", until)), (geocode == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geocode", geocode)), (stream_id == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("stream_id", stream_id)), (sensor_id == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("sensor_id", sensor_id)), (sources == null ? null : (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("sources", sources)), (attributes == null ? null : (""" + implicitly[QueryStringBindable[List[String]]].javascriptUnbind + """)("attributes", attributes)), (format == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("format", format)), (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("semi", semi), (onlyCount == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("onlyCount", onlyCount)), (window_start == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("window_start", window_start)), (window_end == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("window_end", window_end)), (binning == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("binning", binning)), (geojson == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geojson", geojson))])})
      }
      }
   """
)
                        

// @LINE:714
def counts : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.counts",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/counts"})
      }
   """
)
                        

// @LINE:709
def patchStreamMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.patchStreamMetadata",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/streams/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:697
def createSensor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.createSensor",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/sensors"})
      }
   """
)
                        

// @LINE:703
def updateStatisticsSensor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.updateStatisticsSensor",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/sensors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/update"})
      }
   """
)
                        

// @LINE:713
def deleteAll : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.deleteAll",
   """
      function() {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/dropall"})
      }
   """
)
                        

// @LINE:688
def deleteDatapoint : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.deleteDatapoint",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/datapoints/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:699
def getSensor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.getSensor",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/sensors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:711
def searchStreams : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.searchStreams",
   """
      function(geocode,stream_name,geojson) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/streams" + _qS([(geocode == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geocode", geocode)), (stream_name == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("stream_name", stream_name)), (geojson == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("geojson", geojson))])})
      }
   """
)
                        

// @LINE:705
def deleteSensor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.deleteSensor",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/sensors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:712
def deleteStream : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.deleteStream",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/streams/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:700
def updateSensorMetadata : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.updateSensorMetadata",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/sensors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:695
def cacheInvalidateAction : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.cacheInvalidateAction",
   """
      function(sensor_id,stream_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/cache/invalidate" + _qS([(sensor_id == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("sensor_id", sensor_id)), (stream_id == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("stream_id", stream_id))])})
      }
   """
)
                        

// @LINE:710
def updateStatisticsStream : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.updateStatisticsStream",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/streams/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/update"})
      }
   """
)
                        

// @LINE:693
def getDatapoint : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Geostreams.getDatapoint",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/geostreams/datapoints/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        
    
}
              

// @LINE:978
class ReverseFullTree {
    

// @LINE:978
def getChildrenOfNode : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.FullTree.getChildrenOfNode",
   """
      function(nodeId,nodeType,role) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/fulltree/getChildrenOfNode" + _qS([(""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("nodeId", nodeId), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("nodeType", nodeType), (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("role", role)])})
      }
   """
)
                        
    
}
              

// @LINE:836
// @LINE:835
// @LINE:833
// @LINE:832
// @LINE:830
// @LINE:829
// @LINE:828
// @LINE:820
// @LINE:790
class ReverseCurationObjects {
    

// @LINE:828
def findMatchmakingRepositories : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.CurationObjects.findMatchmakingRepositories",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/matchmaker"})
      }
   """
)
                        

// @LINE:790
def getCurationObjectOre : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.CurationObjects.getCurationObjectOre",
   """
      function(curationId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("curationId", curationId) + "/ore"})
      }
   """
)
                        

// @LINE:836
def getMetadataDefinitions : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.CurationObjects.getMetadataDefinitions",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/metadataDefinitions"})
      }
   """
)
                        

// @LINE:832
def deleteCurationFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.CurationObjects.deleteCurationFile",
   """
      function(id,parentId,curationFileId) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/files/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("curationFileId", curationFileId) + _qS([(""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("parentId", parentId)])})
      }
   """
)
                        

// @LINE:835
def getMetadataDefinitionsByFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.CurationObjects.getMetadataDefinitionsByFile",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/getMetadataDefinitionsByFile"})
      }
   """
)
                        

// @LINE:820
def retractCurationObject : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.CurationObjects.retractCurationObject",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/retract/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:830
def savePublishedObject : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.CurationObjects.savePublishedObject",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/status"})
      }
   """
)
                        

// @LINE:829
def getCurationFiles : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.CurationObjects.getCurationFiles",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/curationFile"})
      }
   """
)
                        

// @LINE:833
def deleteCurationFolder : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.CurationObjects.deleteCurationFolder",
   """
      function(id,parentId,curationFolderId) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "spaces/curations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/folders/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("curationFolderId", curationFolderId) + _qS([(""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("parentId", parentId)])})
      }
   """
)
                        
    
}
              

// @LINE:731
// @LINE:730
// @LINE:729
// @LINE:728
// @LINE:727
class ReverseSensors {
    

// @LINE:728
def add : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sensors.add",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sensors"})
      }
   """
)
                        

// @LINE:727
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sensors.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sensors"})
      }
   """
)
                        

// @LINE:731
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sensors.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sensors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        

// @LINE:730
def search : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sensors.search",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sensors/search"})
      }
   """
)
                        

// @LINE:729
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Sensors.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sensors/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
      }
   """
)
                        
    
}
              

// @LINE:769
// @LINE:768
// @LINE:681
// @LINE:680
// @LINE:679
class ReverseSearch {
    

// @LINE:679
def searchJson : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Search.searchJson",
   """
      function(query,grouping,from,size) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/search/json" + _qS([(query == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("query", query)), (grouping == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("grouping", grouping)), (""" + implicitly[QueryStringBindable[Option[Int]]].javascriptUnbind + """)("from", from), (""" + implicitly[QueryStringBindable[Option[Int]]].javascriptUnbind + """)("size", size)])})
      }
   """
)
                        

// @LINE:680
def searchMultimediaIndex : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Search.searchMultimediaIndex",
   """
      function(section_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/search/multimediasearch" + _qS([(""" + implicitly[QueryStringBindable[UUID]].javascriptUnbind + """)("section_id", section_id)])})
      }
   """
)
                        

// @LINE:769
def querySPARQL : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Search.querySPARQL",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/sparqlquery"})
      }
   """
)
                        

// @LINE:768
// @LINE:681
def search : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Search.search",
   """
      function(query) {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/search" + _qS([(query == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("query", query))])})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/search" + _qS([(query == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("query", query))])})
      }
      }
   """
)
                        
    
}
              

// @LINE:966
// @LINE:965
// @LINE:964
// @LINE:963
// @LINE:962
// @LINE:961
// @LINE:958
// @LINE:957
// @LINE:955
// @LINE:954
// @LINE:953
// @LINE:952
// @LINE:951
// @LINE:950
// @LINE:949
// @LINE:948
// @LINE:947
// @LINE:946
// @LINE:945
// @LINE:944
// @LINE:943
// @LINE:942
// @LINE:941
// @LINE:940
// @LINE:939
// @LINE:938
// @LINE:936
// @LINE:919
// @LINE:917
// @LINE:916
// @LINE:915
// @LINE:914
// @LINE:912
// @LINE:911
// @LINE:909
// @LINE:908
// @LINE:906
// @LINE:905
// @LINE:901
// @LINE:900
class ReverseT2C2 {
    

// @LINE:914
def attachVocabToDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.attachVocabToDataset",
   """
      function(vocab_id,dataset_id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("vocab_id", vocab_id) + "/attachToDataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("dataset_id", dataset_id)})
      }
   """
)
                        

// @LINE:919
def listCollectionsCanEdit : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.listCollectionsCanEdit",
   """
      function(id,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/collectionsCanEdit" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Integer]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:963
def getKeysValuesFromLastDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getKeysValuesFromLastDataset",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/getKeyValuesLastDataset"})
      }
   """
)
                        

// @LINE:951
def getWhoICanUploadFor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getWhoICanUploadFor",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/getUsersICanUploadFor"})
      }
   """
)
                        

// @LINE:908
// @LINE:900
def listMyDatasets : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.listMyDatasets",
   """
      function(limit) {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/listMyDatasets" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/listMyDatasets" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
      }
   """
)
                        

// @LINE:917
def detachVocabFromFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.detachVocabFromFile",
   """
      function(vocab_id,file_id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("vocab_id", vocab_id) + "/detachFromFile/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id)})
      }
   """
)
                        

// @LINE:955
def getNumUsersOfSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getNumUsersOfSpace",
   """
      function(spaceId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/getNumUsersOfSpace/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId)})
      }
   """
)
                        

// @LINE:952
def getSpacesOfUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getSpacesOfUser",
   """
      function(userId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/getSpacesOfUser/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("userId", userId)})
      }
   """
)
                        

// @LINE:965
def getKeysValuesFromDatasetId : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getKeysValuesFromDatasetId",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/getKeyValuesForDatasetId/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:939
def getAllCollectionsWithDatasetIdsAndFiles : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getAllCollectionsWithDatasetIdsAndFiles",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/allCollectionDatasetFiles"})
      }
   """
)
                        

// @LINE:915
def detachVocabFromDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.detachVocabFromDataset",
   """
      function(vocab_id,dataset_id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("vocab_id", vocab_id) + "/detachFromDataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("dataset_id", dataset_id)})
      }
   """
)
                        

// @LINE:958
def getDatasetWithAttachedVocab : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getDatasetWithAttachedVocab",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/datasets/getDatasetAndTemplate/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:941
def getLevelOfTree : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getLevelOfTree",
   """
      function(currentId,currentType) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/getLevelOfTree" + _qS([(currentId == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("currentId", currentId)), (currentType == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("currentType", currentType))])})
      }
   """
)
                        

// @LINE:957
def getTemplateFromLastDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getTemplateFromLastDataset",
   """
      function(limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/lastTemplate" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:901
def getVocabulary : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getVocabulary",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/getExperimentTemplateById/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:944
def getLevelOfTreeNotSharedInSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getLevelOfTreeNotSharedInSpace",
   """
      function(currentId,currentType) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/getLevelOfTreeNotShared" + _qS([(currentId == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("currentId", currentId)), (currentType == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("currentType", currentType))])})
      }
   """
)
                        

// @LINE:966
def getVocabIdNameFromTag : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getVocabIdNameFromTag",
   """
      function(tag) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/getIdNameFromTag/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("tag", encodeURIComponent(tag))})
      }
   """
)
                        

// @LINE:948
def createEmptyDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.createEmptyDataset",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/datasets/createEmpty"})
      }
   """
)
                        

// @LINE:962
def getAllCollectionsWithDatasetIds : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getAllCollectionsWithDatasetIds",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/allCollectionsWithDatasetIds"})
      }
   """
)
                        

// @LINE:912
def makeTemplatePrivate : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.makeTemplatePrivate",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/makePrivate/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:946
def getAllCollectionsForFullTree : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getAllCollectionsForFullTree",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/collectionsForFullTree"})
      }
   """
)
                        

// @LINE:954
def getUsersOfSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getUsersOfSpace",
   """
      function(spaceId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/getUsersOfSpace/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId)})
      }
   """
)
                        

// @LINE:943
def getLevelOfTreeSharedWithOthers : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getLevelOfTreeSharedWithOthers",
   """
      function(currentId,currentType) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/getLevelOfTreeSharedWithOthers" + _qS([(currentId == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("currentId", currentId)), (currentType == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("currentType", currentType))])})
      }
   """
)
                        

// @LINE:906
def getVocabByFileId : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getVocabByFileId",
   """
      function(file_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/getByFileId/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id)})
      }
   """
)
                        

// @LINE:938
def getAllCollectionsOfUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getAllCollectionsOfUser",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/allCollection"})
      }
   """
)
                        

// @LINE:945
def getLevelOfTreeInSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getLevelOfTreeInSpace",
   """
      function(currentId,currentType,spaceId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/getLevelOfTreeInSpace" + _qS([(currentId == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("currentId", currentId)), (currentType == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("currentType", currentType)), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("spaceId", spaceId)])})
      }
   """
)
                        

// @LINE:936
def findYoungestChild : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.findYoungestChild",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/findYoungestChild"})
      }
   """
)
                        

// @LINE:916
def attachVocabToFile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.attachVocabToFile",
   """
      function(vocab_id,file_id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("vocab_id", vocab_id) + "/attachToFile/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("file_id", file_id)})
      }
   """
)
                        

// @LINE:947
def getAllCollectionsOfUserNotSharedInSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getAllCollectionsOfUserNotSharedInSpace",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/allCollectionsNotSharedInSpace"})
      }
   """
)
                        

// @LINE:905
def getVocabByDatasetId : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getVocabByDatasetId",
   """
      function(dataset_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/getByDatasetId/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("dataset_id", dataset_id)})
      }
   """
)
                        

// @LINE:909
def listMySpaces : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.listMySpaces",
   """
      function(limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/listMySpaces" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:961
def getDatasetsInCollectionWithColId : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getDatasetsInCollectionWithColId",
   """
      function(coll_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/collections/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("coll_id", coll_id) + "/datasetsWithParentColId"})
      }
   """
)
                        

// @LINE:940
def getAllCollectionsForTree : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getAllCollectionsForTree",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/collectionsForTree"})
      }
   """
)
                        

// @LINE:953
def getSpacesUserHasAccessTo : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getSpacesUserHasAccessTo",
   """
      function(userId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/getSpacesUserCanEdit/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("userId", userId)})
      }
   """
)
                        

// @LINE:942
def getLevelOfTreeSharedWithMe : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getLevelOfTreeSharedWithMe",
   """
      function(currentId,currentType) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/collections/getLevelOfTreeSharedWithMe" + _qS([(currentId == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("currentId", currentId)), (currentType == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("currentType", currentType))])})
      }
   """
)
                        

// @LINE:949
def moveKeysToTermsTemplates : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.moveKeysToTermsTemplates",
   """
      function() {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/moveKeysToTerms"})
      }
   """
)
                        

// @LINE:950
def bulkDelete : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.bulkDelete",
   """
      function() {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/t2c2/bulkDelete"})
      }
   """
)
                        

// @LINE:911
def makeTemplatePublic : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.makeTemplatePublic",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/makePublic/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:964
def getKeysValuesFromLastDatasets : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.T2C2.getKeysValuesFromLastDatasets",
   """
      function(limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/getKeyValuesForLastDatasets" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        
    
}
              

// @LINE:770
class ReverseProjects {
    

// @LINE:770
def addproject : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Projects.addproject",
   """
      function(project) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/projects/addproject" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("project", project)])})
      }
   """
)
                        
    
}
              

// @LINE:913
// @LINE:910
// @LINE:904
// @LINE:903
// @LINE:902
// @LINE:899
// @LINE:898
// @LINE:896
// @LINE:895
// @LINE:893
// @LINE:892
// @LINE:891
// @LINE:861
// @LINE:860
// @LINE:858
// @LINE:857
// @LINE:856
// @LINE:855
// @LINE:854
// @LINE:851
// @LINE:850
// @LINE:849
// @LINE:848
// @LINE:847
class ReverseVocabularies {
    

// @LINE:910
def removeVocabulary : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.removeVocabulary",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/deleteTemplate/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:898
// @LINE:851
def getPublicVocabularies : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.getPublicVocabularies",
   """
      function() {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabularies/getPublic"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/getPublic"})
      }
      }
   """
)
                        

// @LINE:903
def getByTag : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.getByTag",
   """
      function(containsAll) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/findByTag" + _qS([(containsAll == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("containsAll", containsAll))])})
      }
   """
)
                        

// @LINE:896
def listAll : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.listAll",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/allExperimentTemplates"})
      }
   """
)
                        

// @LINE:904
def getBySingleTag : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.getBySingleTag",
   """
      function(tag) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/findByTag/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("tag", encodeURIComponent(tag))})
      }
   """
)
                        

// @LINE:893
// @LINE:892
// @LINE:891
// @LINE:848
def createVocabularyFromJson : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.createVocabularyFromJson",
   """
      function(isPublic) {
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabularies/createVocabularyFromJson" + _qS([(isPublic == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("isPublic", isPublic))])})
      }
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/createExperimentTemplate" + _qS([(isPublic == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("isPublic", isPublic))])})
      }
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/createExperimentTemplateFromJson" + _qS([(isPublic == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("isPublic", isPublic))])})
      }
      if (true) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/createExperimentTemplateDefaultValues" + _qS([(isPublic == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("isPublic", isPublic))])})
      }
      }
   """
)
                        

// @LINE:849
def createVocabularyFromForm : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.createVocabularyFromForm",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabularies/createVocabulary"})
      }
   """
)
                        

// @LINE:854
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.get",
   """
      function(vocab_id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabularies/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("vocab_id", vocab_id)})
      }
   """
)
                        

// @LINE:858
def getByNameAndAuthor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.getByNameAndAuthor",
   """
      function(name) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabularies/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name)) + "/getByNameAuthor"})
      }
   """
)
                        

// @LINE:895
// @LINE:847
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.list",
   """
      function() {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabularies/list"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/listExperimentTemplates"})
      }
      }
   """
)
                        

// @LINE:913
// @LINE:857
// @LINE:855
def editVocabulary : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.editVocabulary",
   """
      function(vocab_id) {
      if (true) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/editVocabulary/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("vocab_id", vocab_id)})
      }
      if (true) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabularies/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("vocab_id", vocab_id) + "/editVocabulary"})
      }
      if (true) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/editTemplate/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", vocab_id)})
      }
      }
   """
)
                        

// @LINE:899
def getAllTagsOfAllVocabularies : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.getAllTagsOfAllVocabularies",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/allTags"})
      }
   """
)
                        

// @LINE:902
// @LINE:856
def getByName : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.getByName",
   """
      function(name) {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabularies/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name))})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "t2c2/templates/getExperimentTemplateByName/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name))})
      }
      }
   """
)
                        

// @LINE:860
def addToSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.addToSpace",
   """
      function(vocab_id,space_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabularies/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("vocab_id", vocab_id) + "/addToSpace/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("space_id", space_id)})
      }
   """
)
                        

// @LINE:861
def removeFromSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.removeFromSpace",
   """
      function(vocab_id,space_id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabuliaries/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("vocab_id", vocab_id) + "/removeFromSpace/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("space_id", space_id)})
      }
   """
)
                        

// @LINE:850
def getByAuthor : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Vocabularies.getByAuthor",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vocabularies/getByAuthor"})
      }
   """
)
                        
    
}
              

// @LINE:765
class ReverseZoomIt {
    

// @LINE:765
def uploadTile : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.ZoomIt.uploadTile",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/tiles"})
      }
   """
)
                        
    
}
              

// @LINE:767
class ReverseThreeDTexture {
    

// @LINE:767
def uploadTexture : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.ThreeDTexture.uploadTexture",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/3dTextures"})
      }
   """
)
                        
    
}
              

// @LINE:483
// @LINE:482
// @LINE:481
// @LINE:480
// @LINE:479
// @LINE:477
// @LINE:476
// @LINE:475
// @LINE:474
// @LINE:473
// @LINE:472
// @LINE:471
// @LINE:470
// @LINE:469
// @LINE:468
// @LINE:467
// @LINE:466
// @LINE:465
// @LINE:464
// @LINE:463
// @LINE:462
// @LINE:461
// @LINE:460
class ReverseSpaces {
    

// @LINE:474
def addDatasetToSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.addDatasetToSpace",
   """
      function(spaceId,datasetId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId) + "/addDatasetToSpace/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId)})
      }
   """
)
                        

// @LINE:464
def removeSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.removeSpace",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:482
def listCollectionsCanEdit : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.listCollectionsCanEdit",
   """
      function(id,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/collectionsCanEdit" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Integer]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:466
def removeDataset : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.removeDataset",
   """
      function(spaceId,datasetId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId) + "/removeDataset/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("datasetId", datasetId)})
      }
   """
)
                        

// @LINE:465
def removeCollection : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.removeCollection",
   """
      function(spaceId,collectionId,removeDatasets) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId) + "/removeCollection/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("collectionId", collectionId) + _qS([(""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("removeDatasets", removeDatasets)])})
      }
   """
)
                        

// @LINE:463
def createSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.createSpace",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces"})
      }
   """
)
                        

// @LINE:470
def updateUsers : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.updateUsers",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/updateUsers"})
      }
   """
)
                        

// @LINE:468
def copyContentsToNewSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.copyContentsToNewSpace",
   """
      function(spaceId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId) + "/copyContentsToNewSpace"})
      }
   """
)
                        

// @LINE:472
def rejectRequest : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.rejectRequest",
   """
      function(id,user) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/rejectRequest" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("user", user)])})
      }
   """
)
                        

// @LINE:473
def removeUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.removeUser",
   """
      function(id,removeUser) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/removeUser" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("removeUser", removeUser)])})
      }
   """
)
                        

// @LINE:475
def addCollectionToSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.addCollectionToSpace",
   """
      function(spaceId,collectionId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId) + "/addCollectionToSpace/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("collectionId", collectionId)})
      }
   """
)
                        

// @LINE:462
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:461
def listCanEdit : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.listCanEdit",
   """
      function(title,date,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/canEdit" + _qS([(title == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("title", title)), (date == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("date", date)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:471
def acceptRequest : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.acceptRequest",
   """
      function(id,user,role) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/acceptRequest" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("user", user), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("role", role)])})
      }
   """
)
                        

// @LINE:469
def updateSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.updateSpace",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/update"})
      }
   """
)
                        

// @LINE:480
def listDatasets : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.listDatasets",
   """
      function(id,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/datasets" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Integer]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:479
def verifySpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.verifySpace",
   """
      function(id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/verify"})
      }
   """
)
                        

// @LINE:460
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.list",
   """
      function(title,date,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces" + _qS([(title == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("title", title)), (date == null ? null : (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("date", date)), (limit == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:477
def unfollow : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.unfollow",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/unfollow"})
      }
   """
)
                        

// @LINE:483
def getUserSpaceRoleMap : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.getUserSpaceRoleMap",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/userSpaceRoleMap"})
      }
   """
)
                        

// @LINE:476
def follow : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.follow",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/follow"})
      }
   """
)
                        

// @LINE:481
def listCollections : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.listCollections",
   """
      function(id,limit) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id) + "/collections" + _qS([(limit == null ? null : (""" + implicitly[QueryStringBindable[Integer]].javascriptUnbind + """)("limit", limit))])})
      }
   """
)
                        

// @LINE:467
def copyContentsToSpace : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Spaces.copyContentsToSpace",
   """
      function(spaceId,id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/spaces/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("spaceId", spaceId) + "/copyContentsToSpace/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        
    
}
              

// @LINE:760
// @LINE:759
// @LINE:758
// @LINE:757
// @LINE:756
class ReverseRelations {
    

// @LINE:760
def delete : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Relations.delete",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/relations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:758
def get : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Relations.get",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/relations/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:757
def findTargets : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Relations.findTargets",
   """
      function(sourceId,sourceType,targetType) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/relations/search" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("sourceId", sourceId), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("sourceType", sourceType), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("targetType", targetType)])})
      }
   """
)
                        

// @LINE:759
def add : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Relations.add",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/relations"})
      }
   """
)
                        

// @LINE:756
def list : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Relations.list",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/relations"})
      }
   """
)
                        
    
}
              

// @LINE:803
// @LINE:802
class ReverseStatus {
    

// @LINE:803
def status : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Status.status",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/status"})
      }
   """
)
                        

// @LINE:802
def version : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.Status.version",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/version"})
      }
   """
)
                        
    
}
              

// @LINE:314
// @LINE:313
// @LINE:312
// @LINE:311
class ReverseContextLD {
    

// @LINE:313
def getContextByName : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.ContextLD.getContextByName",
   """
      function(name) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/contexts/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("name", encodeURIComponent(name)) + "/context.json"})
      }
   """
)
                        

// @LINE:314
def removeById : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.ContextLD.removeById",
   """
      function(id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "api/contexts/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:312
def getContextById : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.ContextLD.getContextById",
   """
      function(id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/contexts/" + (""" + implicitly[PathBindable[UUID]].javascriptUnbind + """)("id", id)})
      }
   """
)
                        

// @LINE:311
def addContext : JavascriptReverseRoute = JavascriptReverseRoute(
   "api.ContextLD.addContext",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/contexts"})
      }
   """
)
                        
    
}
              
}
        


// @LINE:970
// @LINE:968
// @LINE:934
// @LINE:921
// @LINE:841
// @LINE:838
// @LINE:837
// @LINE:834
// @LINE:831
// @LINE:827
// @LINE:826
// @LINE:825
// @LINE:824
// @LINE:823
// @LINE:822
// @LINE:821
// @LINE:819
// @LINE:818
// @LINE:817
// @LINE:816
// @LINE:815
// @LINE:809
// @LINE:808
// @LINE:797
// @LINE:796
// @LINE:744
// @LINE:639
// @LINE:638
// @LINE:637
// @LINE:290
// @LINE:289
// @LINE:284
// @LINE:278
// @LINE:277
// @LINE:276
// @LINE:275
// @LINE:274
// @LINE:273
// @LINE:272
// @LINE:271
// @LINE:270
// @LINE:269
// @LINE:268
// @LINE:267
// @LINE:266
// @LINE:265
// @LINE:264
// @LINE:263
// @LINE:262
// @LINE:261
// @LINE:260
// @LINE:259
// @LINE:258
// @LINE:257
// @LINE:256
// @LINE:255
// @LINE:254
// @LINE:235
// @LINE:218
// @LINE:213
// @LINE:212
// @LINE:211
// @LINE:210
// @LINE:208
// @LINE:205
// @LINE:200
// @LINE:199
// @LINE:198
// @LINE:197
// @LINE:196
// @LINE:195
// @LINE:194
// @LINE:193
// @LINE:191
// @LINE:190
// @LINE:185
// @LINE:184
// @LINE:183
// @LINE:181
// @LINE:180
// @LINE:179
// @LINE:178
// @LINE:173
// @LINE:172
// @LINE:171
// @LINE:170
// @LINE:165
// @LINE:164
// @LINE:163
// @LINE:162
// @LINE:161
// @LINE:160
// @LINE:159
// @LINE:158
// @LINE:157
// @LINE:156
// @LINE:155
// @LINE:154
// @LINE:153
// @LINE:152
// @LINE:148
// @LINE:147
// @LINE:146
// @LINE:145
// @LINE:144
// @LINE:143
// @LINE:142
// @LINE:141
// @LINE:140
// @LINE:139
// @LINE:138
// @LINE:137
// @LINE:136
// @LINE:131
// @LINE:130
// @LINE:129
// @LINE:128
// @LINE:127
// @LINE:126
// @LINE:125
// @LINE:124
// @LINE:123
// @LINE:122
// @LINE:121
// @LINE:120
// @LINE:119
// @LINE:118
// @LINE:117
// @LINE:116
// @LINE:115
// @LINE:114
// @LINE:113
// @LINE:112
// @LINE:111
// @LINE:110
// @LINE:109
// @LINE:104
// @LINE:103
// @LINE:102
// @LINE:101
// @LINE:99
// @LINE:97
// @LINE:96
// @LINE:95
// @LINE:92
// @LINE:91
// @LINE:90
// @LINE:89
// @LINE:88
// @LINE:87
// @LINE:86
// @LINE:84
// @LINE:83
// @LINE:77
// @LINE:76
// @LINE:74
// @LINE:69
// @LINE:68
// @LINE:63
// @LINE:62
// @LINE:61
// @LINE:60
// @LINE:55
// @LINE:54
// @LINE:53
// @LINE:52
// @LINE:51
// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:41
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:8
package controllers.ref {


// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
class ReverseAssets {
    

// @LINE:22
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ ----------------------------------------------------------------------
 Map static resources from the /public folder to the /assets URL path
 ----------------------------------------------------------------------""", _prefix + """contexts/metadata.jsonld""")
)
                      
    
}
                          

// @LINE:181
// @LINE:180
// @LINE:179
// @LINE:178
// @LINE:163
class ReverseMetadata {
    

// @LINE:179
def view(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Metadata]).view(id), HandlerDef(this, "controllers.Metadata", "view", Seq(classOf[UUID]), "GET", """""", _prefix + """metadata/$id<[^/]+>""")
)
                      

// @LINE:181
def dataset(dataset_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Metadata]).dataset(dataset_id), HandlerDef(this, "controllers.Metadata", "dataset", Seq(classOf[UUID]), "GET", """""", _prefix + """datasets/$dataset_id<[^/]+>/metadata""")
)
                      

// @LINE:180
def file(file_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Metadata]).file(file_id), HandlerDef(this, "controllers.Metadata", "file", Seq(classOf[UUID]), "GET", """""", _prefix + """files/$file_id<[^/]+>/metadata""")
)
                      

// @LINE:163
def getMetadataBySpace(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Metadata]).getMetadataBySpace(id), HandlerDef(this, "controllers.Metadata", "getMetadataBySpace", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/$id<[^/]+>/metadata""")
)
                      

// @LINE:178
def search(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Metadata]).search(), HandlerDef(this, "controllers.Metadata", "search", Seq(), "GET", """ ----------------------------------------------------------------------
 JSON-LD Metadata
 ----------------------------------------------------------------------""", _prefix + """metadata/search""")
)
                      
    
}
                          

// @LINE:122
// @LINE:121
// @LINE:120
// @LINE:119
// @LINE:118
// @LINE:117
// @LINE:116
class ReverseToolManager {
    

// @LINE:116
def toolManager(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).toolManager(), HandlerDef(this, "controllers.ToolManager", "toolManager", Seq(), "GET", """""", _prefix + """toolManager""")
)
                      

// @LINE:121
def removeInstance(toolType:String, instanceID:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).removeInstance(toolType, instanceID), HandlerDef(this, "controllers.ToolManager", "removeInstance", Seq(classOf[String], classOf[UUID]), "GET", """""", _prefix + """datasets/removeInstance""")
)
                      

// @LINE:119
def uploadDatasetToTool(instanceID:UUID, datasetID:UUID, datasetName:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).uploadDatasetToTool(instanceID, datasetID, datasetName), HandlerDef(this, "controllers.ToolManager", "uploadDatasetToTool", Seq(classOf[UUID], classOf[UUID], classOf[String]), "GET", """""", _prefix + """datasets/uploadToTool""")
)
                      

// @LINE:122
def launchTool(sessionName:String, ttype:String, datasetId:UUID, datasetName:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).launchTool(sessionName, ttype, datasetId, datasetName), HandlerDef(this, "controllers.ToolManager", "launchTool", Seq(classOf[String], classOf[String], classOf[UUID], classOf[String]), "GET", """""", _prefix + """datasets/launchTool""")
)
                      

// @LINE:118
def getLaunchableTools(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).getLaunchableTools(), HandlerDef(this, "controllers.ToolManager", "getLaunchableTools", Seq(), "GET", """""", _prefix + """datasets/launchableTools""")
)
                      

// @LINE:117
def refreshToolSidebar(datasetid:UUID, datasetName:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).refreshToolSidebar(datasetid, datasetName), HandlerDef(this, "controllers.ToolManager", "refreshToolSidebar", Seq(classOf[UUID], classOf[String]), "GET", """""", _prefix + """datasets/refreshToolList""")
)
                      

// @LINE:120
def getInstances(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).getInstances(), HandlerDef(this, "controllers.ToolManager", "getInstances", Seq(), "GET", """""", _prefix + """datasets/getInstances""")
)
                      
    
}
                          

// @LINE:130
// @LINE:129
class ReverseFolders {
    

// @LINE:129
def addFiles(id:UUID, folderId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Folders]).addFiles(id, folderId), HandlerDef(this, "controllers.Folders", "addFiles", Seq(classOf[UUID], classOf[String]), "GET", """""", _prefix + """datasets/$id<[^/]+>/$folderId<[^/]+>/addFiles""")
)
                      

// @LINE:130
def createFolder(parentDatasetId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Folders]).createFolder(parentDatasetId), HandlerDef(this, "controllers.Folders", "createFolder", Seq(classOf[UUID]), "POST", """""", _prefix + """datasets/$parentDatasetId<[^/]+>/newFolder""")
)
                      
    
}
                          

// @LINE:278
// @LINE:277
// @LINE:275
// @LINE:274
// @LINE:273
// @LINE:272
// @LINE:271
// @LINE:270
// @LINE:269
// @LINE:268
// @LINE:267
// @LINE:266
// @LINE:265
// @LINE:264
// @LINE:263
// @LINE:262
// @LINE:261
// @LINE:260
// @LINE:259
// @LINE:258
// @LINE:257
// @LINE:256
// @LINE:255
// @LINE:254
class ReverseAdmin {
    

// @LINE:262
def getIndexers(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getIndexers(), HandlerDef(this, "controllers.Admin", "getIndexers", Seq(), "GET", """""", _prefix + """admin/indexers""")
)
                      

// @LINE:263
def getIndexes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getIndexes(), HandlerDef(this, "controllers.Admin", "getIndexes", Seq(), "GET", """""", _prefix + """admin/indexes""")
)
                      

// @LINE:270
def listRoles(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).listRoles(), HandlerDef(this, "controllers.Admin", "listRoles", Seq(), "GET", """""", _prefix + """admin/roles""")
)
                      

// @LINE:260
def getExtractors(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getExtractors(), HandlerDef(this, "controllers.Admin", "getExtractors", Seq(), "GET", """""", _prefix + """admin/extractors""")
)
                      

// @LINE:269
def deleteAllIndexes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).deleteAllIndexes(), HandlerDef(this, "controllers.Admin", "deleteAllIndexes", Seq(), "DELETE", """""", _prefix + """admin/index""")
)
                      

// @LINE:264
def getSections(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getSections(), HandlerDef(this, "controllers.Admin", "getSections", Seq(), "GET", """""", _prefix + """admin/sections""")
)
                      

// @LINE:257
def adminIndex(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).adminIndex(), HandlerDef(this, "controllers.Admin", "adminIndex", Seq(), "GET", """""", _prefix + """admin/indexAdmin""")
)
                      

// @LINE:266
def createIndex(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).createIndex(), HandlerDef(this, "controllers.Admin", "createIndex", Seq(), "POST", """""", _prefix + """admin/createIndex""")
)
                      

// @LINE:259
def getAdapters(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getAdapters(), HandlerDef(this, "controllers.Admin", "getAdapters", Seq(), "GET", """""", _prefix + """admin/adapters""")
)
                      

// @LINE:271
def createRole(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).createRole(), HandlerDef(this, "controllers.Admin", "createRole", Seq(), "GET", """""", _prefix + """admin/roles/new""")
)
                      

// @LINE:272
def submitCreateRole(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).submitCreateRole(), HandlerDef(this, "controllers.Admin", "submitCreateRole", Seq(), "POST", """""", _prefix + """admin/roles/submitNew""")
)
                      

// @LINE:268
def deleteIndex(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).deleteIndex(id), HandlerDef(this, "controllers.Admin", "deleteIndex", Seq(classOf[String]), "DELETE", """""", _prefix + """admin/index/$id<[^/]+>""")
)
                      

// @LINE:256
def users(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).users(), HandlerDef(this, "controllers.Admin", "users", Seq(), "GET", """""", _prefix + """admin/users""")
)
                      

// @LINE:258
def reindexFiles(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).reindexFiles(), HandlerDef(this, "controllers.Admin", "reindexFiles", Seq(), "GET", """""", _prefix + """admin/reindexFiles""")
)
                      

// @LINE:265
def getMetadataDefinitions(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getMetadataDefinitions(), HandlerDef(this, "controllers.Admin", "getMetadataDefinitions", Seq(), "GET", """""", _prefix + """admin/metadata/definitions""")
)
                      

// @LINE:277
def viewDumpers(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).viewDumpers(), HandlerDef(this, "controllers.Admin", "viewDumpers", Seq(), "GET", """""", _prefix + """admin/dataDumps""")
)
                      

// @LINE:255
def tos(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).tos(), HandlerDef(this, "controllers.Admin", "tos", Seq(), "GET", """""", _prefix + """admin/tos""")
)
                      

// @LINE:274
def editRole(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).editRole(id), HandlerDef(this, "controllers.Admin", "editRole", Seq(classOf[UUID]), "GET", """""", _prefix + """admin/roles/$id<[^/]+>/edit""")
)
                      

// @LINE:275
def updateRole(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).updateRole(), HandlerDef(this, "controllers.Admin", "updateRole", Seq(), "POST", """""", _prefix + """admin/roles/update""")
)
                      

// @LINE:267
def buildIndex(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).buildIndex(id), HandlerDef(this, "controllers.Admin", "buildIndex", Seq(classOf[String]), "POST", """""", _prefix + """admin/index/$id<[^/]+>/build""")
)
                      

// @LINE:278
def sensors(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).sensors(), HandlerDef(this, "controllers.Admin", "sensors", Seq(), "GET", """""", _prefix + """admin/sensors""")
)
                      

// @LINE:254
def customize(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).customize(), HandlerDef(this, "controllers.Admin", "customize", Seq(), "GET", """ ----------------------------------------------------------------------
 ADMIN PAGES
 ----------------------------------------------------------------------""", _prefix + """admin/customize""")
)
                      

// @LINE:273
def removeRole(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).removeRole(id), HandlerDef(this, "controllers.Admin", "removeRole", Seq(classOf[UUID]), "DELETE", """""", _prefix + """admin/roles/delete/$id<[^/]+>""")
)
                      

// @LINE:261
def getMeasures(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getMeasures(), HandlerDef(this, "controllers.Admin", "getMeasures", Seq(), "GET", """""", _prefix + """admin/measures""")
)
                      
    
}
                          

// @LINE:797
// @LINE:796
// @LINE:34
// @LINE:31
// @LINE:30
class ReverseUsers {
    

// @LINE:31
def acceptTermsOfServices(redirect:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Users]).acceptTermsOfServices(redirect), HandlerDef(this, "controllers.Users", "acceptTermsOfServices", Seq(classOf[Option[String]]), "GET", """""", _prefix + """users/acceptTermsOfServices""")
)
                      

// @LINE:796
def getFollowers(index:Int, limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Users]).getFollowers(index, limit), HandlerDef(this, "controllers.Users", "getFollowers", Seq(classOf[Int], classOf[Int]), "GET", """ ----------------------------------------------------------------------
 USERS Controller
 ----------------------------------------------------------------------""", _prefix + """users/followers""")
)
                      

// @LINE:30
def getUsers(when:String, id:String, limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Users]).getUsers(when, id, limit), HandlerDef(this, "controllers.Users", "getUsers", Seq(classOf[String], classOf[String], classOf[Int]), "GET", """ ----------------------------------------------------------------------
 USERS
 ----------------------------------------------------------------------""", _prefix + """users""")
)
                      

// @LINE:34
def sendEmail(subject:String, from:String, recipient:String, body:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Users]).sendEmail(subject, from, recipient, body), HandlerDef(this, "controllers.Users", "sendEmail", Seq(classOf[String], classOf[String], classOf[String], classOf[String]), "POST", """""", _prefix + """users/sendEmail""")
)
                      

// @LINE:797
def getFollowing(index:Int, limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Users]).getFollowing(index, limit), HandlerDef(this, "controllers.Users", "getFollowing", Seq(classOf[Int], classOf[Int]), "GET", """""", _prefix + """users/following""")
)
                      
    
}
                          

// @LINE:74
// @LINE:68
// @LINE:63
// @LINE:62
// @LINE:61
// @LINE:60
class ReverseExtractionInfo {
    

// @LINE:61
def getExtractorServersIP(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getExtractorServersIP(), HandlerDef(this, "controllers.ExtractionInfo", "getExtractorServersIP", Seq(), "GET", """""", _prefix + """extractions/servers_ips""")
)
                      

// @LINE:60
def getDTSRequests(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getDTSRequests(), HandlerDef(this, "controllers.ExtractionInfo", "getDTSRequests", Seq(), "GET", """ ----------------------------------------------------------------------
 DTS INFORMATION
 ----------------------------------------------------------------------""", _prefix + """extractions/requests""")
)
                      

// @LINE:68
def getBookmarkletPage(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getBookmarkletPage(), HandlerDef(this, "controllers.ExtractionInfo", "getBookmarkletPage", Seq(), "GET", """ ----------------------------------------------------------------------
 DTS BOOKMARKLET
 ----------------------------------------------------------------------v""", _prefix + """bookmarklet""")
)
                      

// @LINE:63
def getExtractorInputTypes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getExtractorInputTypes(), HandlerDef(this, "controllers.ExtractionInfo", "getExtractorInputTypes", Seq(), "GET", """""", _prefix + """extractions/supported_input_types""")
)
                      

// @LINE:74
def getExtensionPage(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getExtensionPage(), HandlerDef(this, "controllers.ExtractionInfo", "getExtensionPage", Seq(), "GET", """ ----------------------------------------------------------------------
 DTS CHROME EXTENSIONS
 ----------------------------------------------------------------------""", _prefix + """extensions/dts/chrome""")
)
                      

// @LINE:62
def getExtractorNames(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getExtractorNames(), HandlerDef(this, "controllers.ExtractionInfo", "getExtractorNames", Seq(), "GET", """""", _prefix + """extractions/extractors_names""")
)
                      
    
}
                          

// @LINE:276
// @LINE:131
// @LINE:104
class ReverseExtractors {
    

// @LINE:131
def submitDatasetExtraction(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Extractors]).submitDatasetExtraction(id), HandlerDef(this, "controllers.Extractors", "submitDatasetExtraction", Seq(classOf[UUID]), "GET", """""", _prefix + """datasets/$id<[^/]+>/extractions""")
)
                      

// @LINE:104
def submitFileExtraction(file_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Extractors]).submitFileExtraction(file_id), HandlerDef(this, "controllers.Extractors", "submitFileExtraction", Seq(classOf[UUID]), "GET", """""", _prefix + """files/$file_id<[^/]+>/extractions""")
)
                      

// @LINE:276
def listAllExtractions(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Extractors]).listAllExtractions(), HandlerDef(this, "controllers.Extractors", "listAllExtractions", Seq(), "GET", """""", _prefix + """admin/extractions""")
)
                      
    
}
                          

// @LINE:55
// @LINE:54
// @LINE:53
// @LINE:52
// @LINE:51
class ReverseError {
    

// @LINE:53
def incorrectPermissions(msg:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Error]).incorrectPermissions(msg), HandlerDef(this, "controllers.Error", "incorrectPermissions", Seq(classOf[String]), "GET", """""", _prefix + """error/noPermissions""")
)
                      

// @LINE:52
def authenticationRequiredMessage(msg:String, url:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Error]).authenticationRequiredMessage(msg, url), HandlerDef(this, "controllers.Error", "authenticationRequiredMessage", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """error/authenticationRequiredMessage/$msg<[^/]+>/$url<[^/]+>""")
)
                      

// @LINE:54
def notActivated(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Error]).notActivated(), HandlerDef(this, "controllers.Error", "notActivated", Seq(), "GET", """""", _prefix + """error/notActivated""")
)
                      

// @LINE:51
def authenticationRequired(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Error]).authenticationRequired(), HandlerDef(this, "controllers.Error", "authenticationRequired", Seq(), "GET", """ ----------------------------------------------------------------------
 ERRORS
 ----------------------------------------------------------------------""", _prefix + """error/authenticationRequired""")
)
                      

// @LINE:55
def notAuthorized(msg:String, id:String, resourceType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Error]).notAuthorized(msg, id, resourceType), HandlerDef(this, "controllers.Error", "notAuthorized", Seq(classOf[String], classOf[String], classOf[String]), "GET", """""", _prefix + """error/notAuthorized""")
)
                      
    
}
                          

// @LINE:208
// @LINE:205
class ReverseDataAnalysis {
    

// @LINE:205
def listSessions(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.DataAnalysis]).listSessions(), HandlerDef(this, "controllers.DataAnalysis", "listSessions", Seq(), "GET", """ ----------------------------------------------------------------------
 Jupyter Integration
 ----------------------------------------------------------------------""", _prefix + """analysisSessions""")
)
                      

// @LINE:208
def terminate(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.DataAnalysis]).terminate(id), HandlerDef(this, "controllers.DataAnalysis", "terminate", Seq(classOf[UUID]), "GET", """GET            /analysis                                                                 @controllers.DataAnalysis.reserveSession
GET            /startContainer                                                           @controllers.DataAnalysis.startContainer(nameOfSession: String ?="", hours: String ?="", minutes: String ?="")""", _prefix + """terminateSession/$id<[^/]+>""")
)
                      
    
}
                          

// @LINE:103
// @LINE:102
// @LINE:101
// @LINE:99
// @LINE:97
// @LINE:96
// @LINE:95
// @LINE:92
// @LINE:91
// @LINE:90
// @LINE:89
// @LINE:88
// @LINE:87
// @LINE:86
// @LINE:84
// @LINE:83
// @LINE:77
// @LINE:76
class ReverseFiles {
    

// @LINE:99
def uploaddnd(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploaddnd(id), HandlerDef(this, "controllers.Files", "uploaddnd", Seq(classOf[UUID]), "POST", """POST          /uploadAjax					                                            @controllers.Files.uploadAjax""", _prefix + """uploaddnd/$id<[^/]+>""")
)
                      

// @LINE:86
def metadataSearch(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).metadataSearch(), HandlerDef(this, "controllers.Files", "metadataSearch", Seq(), "GET", """GET            /files/newZipCollection                                                  @controllers.Files.uploadZipFile""", _prefix + """files/metadataSearch""")
)
                      

// @LINE:88
def followingFiles(index:Int, size:Int, mode:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).followingFiles(index, size, mode), HandlerDef(this, "controllers.Files", "followingFiles", Seq(classOf[Int], classOf[Int], classOf[String]), "GET", """""", _prefix + """files/following""")
)
                      

// @LINE:103
def fileBySection(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).fileBySection(id), HandlerDef(this, "controllers.Files", "fileBySection", Seq(classOf[UUID]), "GET", """""", _prefix + """file_by_section/$id<[^/]+>""")
)
                      

// @LINE:95
def upload(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).upload(), HandlerDef(this, "controllers.Files", "upload", Seq(), "POST", """GET           /queries/:id/blob				                                        @controllers.Files.downloadquery(id: UUID)
GET           /files/:id/similar                                                       @controllers.Files.findSimilar(id: UUID)""", _prefix + """upload""")
)
                      

// @LINE:76
def extractFile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).extractFile(), HandlerDef(this, "controllers.Files", "extractFile", Seq(), "GET", """This may not require; This is only for testing. It will change in subsequent version""", _prefix + """extraction/form""")
)
                      

// @LINE:84
def uploadFile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploadFile(), HandlerDef(this, "controllers.Files", "uploadFile", Seq(), "GET", """""", _prefix + """files/new""")
)
                      

// @LINE:97
def uploadSelectQuery(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploadSelectQuery(), HandlerDef(this, "controllers.Files", "uploadSelectQuery", Seq(), "POST", """""", _prefix + """uploadSelectQuery""")
)
                      

// @LINE:101
def uploadDragDrop(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploadDragDrop(), HandlerDef(this, "controllers.Files", "uploadDragDrop", Seq(), "POST", """POST  	       /reactiveUpload					                                        @controllers.Files.reactiveUpload""", _prefix + """uploadDragDrop""")
)
                      

// @LINE:87
def generalMetadataSearch(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).generalMetadataSearch(), HandlerDef(this, "controllers.Files", "generalMetadataSearch", Seq(), "GET", """""", _prefix + """files/generalMetadataSearch""")
)
                      

// @LINE:102
def thumbnail(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).thumbnail(id), HandlerDef(this, "controllers.Files", "thumbnail", Seq(classOf[UUID]), "GET", """""", _prefix + """fileThumbnail/$id<[^/]+>/blob""")
)
                      

// @LINE:83
def list(when:String, date:String, size:Int, mode:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).list(when, date, size, mode), HandlerDef(this, "controllers.Files", "list", Seq(classOf[String], classOf[String], classOf[Int], classOf[String]), "GET", """ ----------------------------------------------------------------------
 FILES
 ----------------------------------------------------------------------""", _prefix + """files""")
)
                      

// @LINE:89
def file(id:UUID, dataset:Option[String], space:Option[String], folder:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).file(id, dataset, space, folder), HandlerDef(this, "controllers.Files", "file", Seq(classOf[UUID], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]), "GET", """""", _prefix + """files/$id<[^/]+>""")
)
                      

// @LINE:91
def download(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).download(id), HandlerDef(this, "controllers.Files", "download", Seq(classOf[UUID]), "GET", """""", _prefix + """files/$id<[^/]+>/blob""")
)
                      

// @LINE:96
def uploadAndExtract(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploadAndExtract(), HandlerDef(this, "controllers.Files", "uploadAndExtract", Seq(), "POST", """""", _prefix + """uploadAndExtract""")
)
                      

// @LINE:77
def uploadExtract(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploadExtract(), HandlerDef(this, "controllers.Files", "uploadExtract", Seq(), "POST", """""", _prefix + """extraction/upload""")
)
                      

// @LINE:90
def filePreview(id:UUID, dataset:Option[String], space:Option[String], folder:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).filePreview(id, dataset, space, folder), HandlerDef(this, "controllers.Files", "filePreview", Seq(classOf[UUID], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]), "GET", """""", _prefix + """filesPreview/$id<[^/]+>""")
)
                      

// @LINE:92
def downloadAsFormat(id:UUID, format:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).downloadAsFormat(id, format), HandlerDef(this, "controllers.Files", "downloadAsFormat", Seq(classOf[UUID], classOf[String]), "GET", """""", _prefix + """files/$id<[^/]+>/download/$format<[^/]+>""")
)
                      
    
}
                          

// @LINE:218
class ReversePreviewers {
    

// @LINE:218
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Previewers.list(), HandlerDef(this, "controllers.Previewers", "list", Seq(), "GET", """ ----------------------------------------------------------------------
 PREVIEWERS
 ----------------------------------------------------------------------""", _prefix + """previewers/list""")
)
                      
    
}
                          

// @LINE:46
// @LINE:45
// @LINE:44
// @LINE:43
// @LINE:41
class ReverseProfile {
    

// @LINE:43
def viewProfileUUID(uuid:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Profile]).viewProfileUUID(uuid), HandlerDef(this, "controllers.Profile", "viewProfileUUID", Seq(classOf[UUID]), "GET", """ deprecated use profile/:uuid""", _prefix + """profile/viewProfile/$uuid<[^/]+>""")
)
                      

// @LINE:41
def viewProfile(email:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Profile]).viewProfile(email), HandlerDef(this, "controllers.Profile", "viewProfile", Seq(classOf[Option[String]]), "GET", """ ----------------------------------------------------------------------
 PROFILE
 ----------------------------------------------------------------------
 deprecated use profile/:uuid""", _prefix + """profile/viewProfile""")
)
                      

// @LINE:45
def submitChanges(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Profile]).submitChanges(), HandlerDef(this, "controllers.Profile", "submitChanges", Seq(), "POST", """""", _prefix + """profile/submitChanges""")
)
                      

// @LINE:44
def editProfile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Profile]).editProfile(), HandlerDef(this, "controllers.Profile", "editProfile", Seq(), "GET", """""", _prefix + """profile/editProfile""")
)
                      
    
}
                          

// @LINE:213
// @LINE:212
// @LINE:211
// @LINE:210
class ReverseNotebook {
    

// @LINE:211
def viewNotebook(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Notebook]).viewNotebook(id), HandlerDef(this, "controllers.Notebook", "viewNotebook", Seq(classOf[UUID]), "GET", """""", _prefix + """notebook/$id<[^/]+>""")
)
                      

// @LINE:212
def editNotebook(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Notebook]).editNotebook(id), HandlerDef(this, "controllers.Notebook", "editNotebook", Seq(classOf[UUID]), "GET", """""", _prefix + """editNotebook/$id<[^/]+>""")
)
                      

// @LINE:213
def deleteNotebook(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Notebook]).deleteNotebook(id), HandlerDef(this, "controllers.Notebook", "deleteNotebook", Seq(classOf[UUID]), "GET", """""", _prefix + """deleteNotebook/$id<[^/]+>""")
)
                      

// @LINE:210
def listNotebooks(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Notebook]).listNotebooks(), HandlerDef(this, "controllers.Notebook", "listNotebooks", Seq(), "GET", """""", _prefix + """notebooks""")
)
                      
    
}
                          

// @LINE:148
// @LINE:147
// @LINE:146
// @LINE:145
// @LINE:144
// @LINE:143
// @LINE:142
// @LINE:141
// @LINE:140
// @LINE:139
// @LINE:138
// @LINE:137
// @LINE:136
class ReverseCollections {
    

// @LINE:138
def listChildCollections(parentCollectionId:String, when:String, date:String, size:Int, space:Option[String], mode:String, owner:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).listChildCollections(parentCollectionId, when, date, size, space, mode, owner), HandlerDef(this, "controllers.Collections", "listChildCollections", Seq(classOf[String], classOf[String], classOf[String], classOf[Int], classOf[Option[String]], classOf[String], classOf[Option[String]]), "GET", """""", _prefix + """collections/listChildCollections""")
)
                      

// @LINE:140
def newCollectionWithParent(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).newCollectionWithParent(id), HandlerDef(this, "controllers.Collections", "newCollectionWithParent", Seq(classOf[UUID]), "GET", """""", _prefix + """collections/$id<[^/]+>/newchildCollection""")
)
                      

// @LINE:141
def followingCollections(index:Int, size:Int, mode:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).followingCollections(index, size, mode), HandlerDef(this, "controllers.Collections", "followingCollections", Seq(classOf[Int], classOf[Int], classOf[String]), "GET", """""", _prefix + """collections/following""")
)
                      

// @LINE:142
def submit(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).submit(), HandlerDef(this, "controllers.Collections", "submit", Seq(), "POST", """""", _prefix + """collection/submit""")
)
                      

// @LINE:143
def createStep2(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).createStep2(), HandlerDef(this, "controllers.Collections", "createStep2", Seq(), "GET", """""", _prefix + """collection/createStep2""")
)
                      

// @LINE:145
def users(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).users(id), HandlerDef(this, "controllers.Collections", "users", Seq(classOf[UUID]), "GET", """""", _prefix + """collection/$id<[^/]+>/users""")
)
                      

// @LINE:148
def getUpdatedChildCollections(id:UUID, index:Int, limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).getUpdatedChildCollections(id, index, limit), HandlerDef(this, "controllers.Collections", "getUpdatedChildCollections", Seq(classOf[UUID], classOf[Int], classOf[Int]), "GET", """""", _prefix + """collection/$id<[^/]+>/childCollections""")
)
                      

// @LINE:146
def previews(collection_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).previews(collection_id), HandlerDef(this, "controllers.Collections", "previews", Seq(classOf[UUID]), "GET", """""", _prefix + """collections/$collection_id<[^/]+>/previews""")
)
                      

// @LINE:144
def collection(id:UUID, limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).collection(id, limit), HandlerDef(this, "controllers.Collections", "collection", Seq(classOf[UUID], classOf[Int]), "GET", """""", _prefix + """collection/$id<[^/]+>""")
)
                      

// @LINE:136
def list(when:String, date:String, size:Int, space:Option[String], mode:String, owner:Option[String], showPublic:Boolean, showOnlyShared:Boolean, showTrash:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).list(when, date, size, space, mode, owner, showPublic, showOnlyShared, showTrash), HandlerDef(this, "controllers.Collections", "list", Seq(classOf[String], classOf[String], classOf[Int], classOf[Option[String]], classOf[String], classOf[Option[String]], classOf[Boolean], classOf[Boolean], classOf[Boolean]), "GET", """ ----------------------------------------------------------------------
 COLLECTIONS
 ----------------------------------------------------------------------""", _prefix + """collections""")
)
                      

// @LINE:137
def sortedListInSpace(space:String, offset:Integer, limit:Integer, showPublic:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).sortedListInSpace(space, offset, limit, showPublic), HandlerDef(this, "controllers.Collections", "sortedListInSpace", Seq(classOf[String], classOf[Integer], classOf[Integer], classOf[Boolean]), "GET", """""", _prefix + """collections/sorted""")
)
                      

// @LINE:147
def getUpdatedDatasets(id:UUID, index:Int, limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).getUpdatedDatasets(id, index, limit), HandlerDef(this, "controllers.Collections", "getUpdatedDatasets", Seq(classOf[UUID], classOf[Int], classOf[Int]), "GET", """""", _prefix + """collection/$id<[^/]+>/datasets""")
)
                      

// @LINE:139
def newCollection(space:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).newCollection(space), HandlerDef(this, "controllers.Collections", "newCollection", Seq(classOf[Option[String]]), "GET", """""", _prefix + """collections/new""")
)
                      
    
}
                          

// @LINE:235
class ReverseRegistration {
    

// @LINE:235
def handleSignUp(token:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Registration]).handleSignUp(token), HandlerDef(this, "controllers.Registration", "handleSignUp", Seq(classOf[String]), "POST", """""", _prefix + """signup/$token<[^/]+>""")
)
                      
    
}
                          

// @LINE:128
// @LINE:127
// @LINE:126
// @LINE:125
// @LINE:124
// @LINE:123
// @LINE:115
// @LINE:114
// @LINE:113
// @LINE:112
// @LINE:111
// @LINE:110
// @LINE:109
class ReverseDatasets {
    

// @LINE:127
def submit(folderId:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).submit(folderId), HandlerDef(this, "controllers.Datasets", "submit", Seq(classOf[Option[String]]), "POST", """""", _prefix + """dataset/submit""")
)
                      

// @LINE:113
def metadataSearch(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).metadataSearch(), HandlerDef(this, "controllers.Datasets", "metadataSearch", Seq(), "GET", """""", _prefix + """datasets/metadataSearch""")
)
                      

// @LINE:109
def list(when:String, date:String, size:Int, space:Option[String], status:Option[String], mode:String, owner:Option[String], showPublic:Boolean, showOnlyShared:Boolean, showTrash:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).list(when, date, size, space, status, mode, owner, showPublic, showOnlyShared, showTrash), HandlerDef(this, "controllers.Datasets", "list", Seq(classOf[String], classOf[String], classOf[Int], classOf[Option[String]], classOf[Option[String]], classOf[String], classOf[Option[String]], classOf[Boolean], classOf[Boolean], classOf[Boolean]), "GET", """ ----------------------------------------------------------------------
 DATASETS
 ----------------------------------------------------------------------""", _prefix + """datasets""")
)
                      

// @LINE:114
def generalMetadataSearch(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).generalMetadataSearch(), HandlerDef(this, "controllers.Datasets", "generalMetadataSearch", Seq(), "GET", """""", _prefix + """datasets/generalMetadataSearch""")
)
                      

// @LINE:112
def createStep2(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).createStep2(id), HandlerDef(this, "controllers.Datasets", "createStep2", Seq(classOf[UUID]), "GET", """""", _prefix + """datasets/createStep2""")
)
                      

// @LINE:125
def users(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).users(id), HandlerDef(this, "controllers.Datasets", "users", Seq(classOf[UUID]), "GET", """""", _prefix + """datasets/$id<[^/]+>/users""")
)
                      

// @LINE:115
def followingDatasets(index:Int, size:Int, mode:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).followingDatasets(index, size, mode), HandlerDef(this, "controllers.Datasets", "followingDatasets", Seq(classOf[Int], classOf[Int], classOf[String]), "GET", """""", _prefix + """datasets/following""")
)
                      

// @LINE:111
def newDataset(space:Option[String], collection:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).newDataset(space, collection), HandlerDef(this, "controllers.Datasets", "newDataset", Seq(classOf[Option[String]], classOf[Option[String]]), "GET", """""", _prefix + """datasets/new""")
)
                      

// @LINE:126
def datasetBySection(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).datasetBySection(id), HandlerDef(this, "controllers.Datasets", "datasetBySection", Seq(classOf[UUID]), "GET", """""", _prefix + """datasets_by_section/$id<[^/]+>""")
)
                      

// @LINE:128
def addFiles(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).addFiles(id), HandlerDef(this, "controllers.Datasets", "addFiles", Seq(classOf[UUID]), "GET", """""", _prefix + """datasets/$id<[^/]+>/addFiles""")
)
                      

// @LINE:123
def dataset(id:UUID, space:Option[String], limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).dataset(id, space, limit), HandlerDef(this, "controllers.Datasets", "dataset", Seq(classOf[UUID], classOf[Option[String]], classOf[Int]), "GET", """""", _prefix + """datasets/$id<[^/]+>""")
)
                      

// @LINE:110
def sortedListInSpace(space:String, offset:Integer, limit:Integer, showPublic:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).sortedListInSpace(space, offset, limit, showPublic), HandlerDef(this, "controllers.Datasets", "sortedListInSpace", Seq(classOf[String], classOf[Integer], classOf[Integer], classOf[Boolean]), "GET", """""", _prefix + """datasets/sorted""")
)
                      

// @LINE:124
def getUpdatedFilesAndFolders(datasetId:UUID, limit:Int, pageIndex:Int, space:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).getUpdatedFilesAndFolders(datasetId, limit, pageIndex, space), HandlerDef(this, "controllers.Datasets", "getUpdatedFilesAndFolders", Seq(classOf[UUID], classOf[Int], classOf[Int], classOf[Option[String]]), "POST", """""", _prefix + """datasets/$datasetId<[^/]+>/updatedFilesAndFolders""")
)
                      
    
}
                          

// @LINE:36
// @LINE:35
// @LINE:32
class ReverseLogin {
    

// @LINE:32
def isLoggedIn(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Login]).isLoggedIn(), HandlerDef(this, "controllers.Login", "isLoggedIn", Seq(), "GET", """""", _prefix + """login/isLoggedIn""")
)
                      

// @LINE:36
def ldapAuthenticate(uid:String, password:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Login]).ldapAuthenticate(uid, password), HandlerDef(this, "controllers.Login", "ldapAuthenticate", Seq(classOf[String], classOf[String]), "POST", """""", _prefix + """ldap/authenticate""")
)
                      

// @LINE:35
def ldap(redirecturl:String, token:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Login]).ldap(redirecturl, token), HandlerDef(this, "controllers.Login", "ldap", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """ldap""")
)
                      
    
}
                          

// @LINE:744
class ReverseSelected {
    

// @LINE:744
def get(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Selected]).get(), HandlerDef(this, "controllers.Selected", "get", Seq(), "GET", """ ----------------------------------------------------------------------
 SELECTIONS API
 ----------------------------------------------------------------------""", _prefix + """selected""")
)
                      
    
}
                          

// @LINE:841
class ReverseEvents {
    

// @LINE:841
def getEvents(index:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Events]).getEvents(index), HandlerDef(this, "controllers.Events", "getEvents", Seq(classOf[Int]), "GET", """""", _prefix + """getEvents""")
)
                      
    
}
                          

// @LINE:290
// @LINE:289
// @LINE:284
// @LINE:69
// @LINE:33
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:8
class ReverseApplication {
    

// @LINE:16
def tos(redirect:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).tos(redirect), HandlerDef(this, "controllers.Application", "tos", Seq(classOf[Option[String]]), "GET", """""", _prefix + """tos""")
)
                      

// @LINE:69
def bookmarklet(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).bookmarklet(), HandlerDef(this, "controllers.Application", "bookmarklet", Seq(), "GET", """""", _prefix + """bookmarklet.js""")
)
                      

// @LINE:15
def about(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).about(), HandlerDef(this, "controllers.Application", "about", Seq(), "GET", """""", _prefix + """about""")
)
                      

// @LINE:290
def swaggerUI(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).swaggerUI(), HandlerDef(this, "controllers.Application", "swaggerUI", Seq(), "GET", """""", _prefix + """swaggerUI""")
)
                      

// @LINE:17
def email(subject:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).email(subject), HandlerDef(this, "controllers.Application", "email", Seq(classOf[String]), "GET", """""", _prefix + """email""")
)
                      

// @LINE:8
def untrail(path:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).untrail(path), HandlerDef(this, "controllers.Application", "untrail", Seq(classOf[String]), "GET", """""", _prefix + """$path<.+>/""")
)
                      

// @LINE:289
def swagger(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).swagger(), HandlerDef(this, "controllers.Application", "swagger", Seq(), "GET", """ ----------------------------------------------------------------------
 API DOCUMENTATION USING SWAGGER
 ----------------------------------------------------------------------""", _prefix + """swagger""")
)
                      

// @LINE:33
def onlyGoogle(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).onlyGoogle(), HandlerDef(this, "controllers.Application", "onlyGoogle", Seq(), "GET", """""", _prefix + """login/isLoggedInWithGoogle""")
)
                      

// @LINE:14
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).index(), HandlerDef(this, "controllers.Application", "index", Seq(), "GET", """""", _prefix + """""")
)
                      

// @LINE:284
def javascriptRoutes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).javascriptRoutes(), HandlerDef(this, "controllers.Application", "javascriptRoutes", Seq(), "GET", """ ----------------------------------------------------------------------
 JAVASCRIPT ENDPOINTS
 ----------------------------------------------------------------------""", _prefix + """javascriptRoutes""")
)
                      
    
}
                          

// @LINE:173
// @LINE:172
// @LINE:171
// @LINE:170
class ReverseGeostreams {
    

// @LINE:173
def edit(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Geostreams]).edit(id), HandlerDef(this, "controllers.Geostreams", "edit", Seq(classOf[String]), "GET", """""", _prefix + """geostreams/sensors/$id<[^/]+>""")
)
                      

// @LINE:170
def map(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Geostreams]).map(), HandlerDef(this, "controllers.Geostreams", "map", Seq(), "GET", """ ----------------------------------------------------------------------
 SENSORS
 ----------------------------------------------------------------------""", _prefix + """geostreams""")
)
                      

// @LINE:171
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Geostreams]).list(), HandlerDef(this, "controllers.Geostreams", "list", Seq(), "GET", """""", _prefix + """geostreams/sensors""")
)
                      

// @LINE:172
def newSensor(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Geostreams]).newSensor(), HandlerDef(this, "controllers.Geostreams", "newSensor", Seq(), "GET", """""", _prefix + """geostreams/sensors/new""")
)
                      
    
}
                          

// @LINE:185
// @LINE:184
// @LINE:183
class ReverseTags {
    

// @LINE:183
def search(tag:String, start:String, size:Integer, mode:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Tags]).search(tag, start, size, mode), HandlerDef(this, "controllers.Tags", "search", Seq(classOf[String], classOf[String], classOf[Integer], classOf[String]), "GET", """ Tags""", _prefix + """tags/search""")
)
                      

// @LINE:185
def tagListWeighted(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Tags]).tagListWeighted(), HandlerDef(this, "controllers.Tags", "tagListWeighted", Seq(), "GET", """""", _prefix + """tags/list/weighted""")
)
                      

// @LINE:184
def tagListOrdered(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Tags]).tagListOrdered(), HandlerDef(this, "controllers.Tags", "tagListOrdered", Seq(), "GET", """""", _prefix + """tags/list/ordered""")
)
                      
    
}
                          

// @LINE:809
// @LINE:808
class ReverseRSS {
    

// @LINE:809
def siteRSSOfType(limit:Option[Int], etype:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.RSS]).siteRSSOfType(limit, etype), HandlerDef(this, "controllers.RSS", "siteRSSOfType", Seq(classOf[Option[Int]], classOf[String]), "GET", """""", _prefix + """rss/$etype<[^/]+>""")
)
                      

// @LINE:808
def siteRSS(limit:Option[Int]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.RSS]).siteRSS(limit), HandlerDef(this, "controllers.RSS", "siteRSS", Seq(classOf[Option[Int]]), "GET", """ ----------------------------------------------------------------------
 RSS
 ----------------------------------------------------------------------""", _prefix + """rss""")
)
                      
    
}
                          

// @LINE:838
// @LINE:837
// @LINE:834
// @LINE:831
// @LINE:827
// @LINE:826
// @LINE:825
// @LINE:824
// @LINE:823
// @LINE:822
// @LINE:821
// @LINE:819
// @LINE:817
// @LINE:816
// @LINE:815
class ReverseCurationObjects {
    

// @LINE:831
def getStatusFromRepository(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).getStatusFromRepository(id), HandlerDef(this, "controllers.CurationObjects", "getStatusFromRepository", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/curations/$id<[^/]+>/getStatusFromRepository""")
)
                      

// @LINE:821
def editCuration(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).editCuration(id), HandlerDef(this, "controllers.CurationObjects", "editCuration", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/curations/$id<[^/]+>/edit""")
)
                      

// @LINE:816
def list(when:String, date:String, limit:Int, space:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).list(when, date, limit, space), HandlerDef(this, "controllers.CurationObjects", "list", Seq(classOf[String], classOf[String], classOf[Int], classOf[Option[String]]), "GET", """""", _prefix + """space/curations""")
)
                      

// @LINE:826
def sendToRepository(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).sendToRepository(id), HandlerDef(this, "controllers.CurationObjects", "sendToRepository", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/curations/$id<[^/]+>/sendToRepository""")
)
                      

// @LINE:823
def deleteCuration(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).deleteCuration(id), HandlerDef(this, "controllers.CurationObjects", "deleteCuration", Seq(classOf[UUID]), "DELETE", """""", _prefix + """spaces/curations/$id<[^/]+>""")
)
                      

// @LINE:834
def getUpdatedFilesAndFolders(id:UUID, curationFolderId:String, limit:Int, pageIndex:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).getUpdatedFilesAndFolders(id, curationFolderId, limit, pageIndex), HandlerDef(this, "controllers.CurationObjects", "getUpdatedFilesAndFolders", Seq(classOf[UUID], classOf[String], classOf[Int], classOf[Int]), "GET", """""", _prefix + """spaces/curations/$id<[^/]+>/updatedFilesAndFolders""")
)
                      

// @LINE:822
def updateCuration(id:UUID, spaceId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).updateCuration(id, spaceId), HandlerDef(this, "controllers.CurationObjects", "updateCuration", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """spaces/curations/$id<[^/]+>/spaces/$spaceId<[^/]+>/update""")
)
                      

// @LINE:827
def findMatchingRepositories(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).findMatchingRepositories(id), HandlerDef(this, "controllers.CurationObjects", "findMatchingRepositories", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/curations/$id<[^/]+>/matchmaker""")
)
                      

// @LINE:815
def newCO(id:UUID, space:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).newCO(id, space), HandlerDef(this, "controllers.CurationObjects", "newCO", Seq(classOf[UUID], classOf[String]), "GET", """""", _prefix + """dataset/$id<[^/]+>/curations/new""")
)
                      

// @LINE:819
def getCurationObject(id:UUID, limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).getCurationObject(id, limit), HandlerDef(this, "controllers.CurationObjects", "getCurationObject", Seq(classOf[UUID], classOf[Int]), "GET", """""", _prefix + """spaces/curations/$id<[^/]+>""")
)
                      

// @LINE:825
def submitRepositorySelection(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).submitRepositorySelection(id), HandlerDef(this, "controllers.CurationObjects", "submitRepositorySelection", Seq(classOf[UUID]), "POST", """""", _prefix + """spaces/curations/$id<[^/]+>/submitRepositorySelection""")
)
                      

// @LINE:824
def compareToRepository(id:UUID, repository:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).compareToRepository(id, repository), HandlerDef(this, "controllers.CurationObjects", "compareToRepository", Seq(classOf[UUID], classOf[String]), "GET", """""", _prefix + """spaces/curations/$id<[^/]+>/compareToRepository""")
)
                      

// @LINE:837
def getPublishedData(space:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).getPublishedData(space), HandlerDef(this, "controllers.CurationObjects", "getPublishedData", Seq(classOf[String]), "GET", """""", _prefix + """publishedData""")
)
                      

// @LINE:817
def submit(id:UUID, spaceId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).submit(id, spaceId), HandlerDef(this, "controllers.CurationObjects", "submit", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """dataset/$id<[^/]+>/curations/spaces/$spaceId<[^/]+>/submit""")
)
                      
    
}
                          

// @LINE:200
// @LINE:199
// @LINE:198
// @LINE:197
// @LINE:196
// @LINE:195
// @LINE:194
// @LINE:193
// @LINE:191
// @LINE:190
class ReverseSearch {
    

// @LINE:198
def findSimilarToExistingFile(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).findSimilarToExistingFile(id), HandlerDef(this, "controllers.Search", "findSimilarToExistingFile", Seq(classOf[UUID]), "GET", """""", _prefix + """files/$id<[^/]+>/similar""")
)
                      

// @LINE:193
def advanced(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).advanced(), HandlerDef(this, "controllers.Search", "advanced", Seq(), "GET", """GET           /multimediaserach1                                                       @controllers.Search.multimediasearch1(f, id: UUID)""", _prefix + """advanced""")
)
                      

// @LINE:194
def SearchByText(query:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).SearchByText(query), HandlerDef(this, "controllers.Search", "SearchByText", Seq(classOf[String]), "GET", """""", _prefix + """SearchByText""")
)
                      

// @LINE:190
def search(query:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).search(query), HandlerDef(this, "controllers.Search", "search", Seq(classOf[String]), "GET", """ ----------------------------------------------------------------------

 ----------------------------------------------------------------------""", _prefix + """search""")
)
                      

// @LINE:195
def uploadquery(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).uploadquery(), HandlerDef(this, "controllers.Search", "uploadquery", Seq(), "POST", """""", _prefix + """uploadquery""")
)
                      

// @LINE:199
def findSimilarToQueryFile(id:UUID, typeToSearch:String, sectionsSelected:List[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).findSimilarToQueryFile(id, typeToSearch, sectionsSelected), HandlerDef(this, "controllers.Search", "findSimilarToQueryFile", Seq(classOf[UUID], classOf[String], classOf[List[String]]), "GET", """""", _prefix + """queries/$id<[^/]+>/similar""")
)
                      

// @LINE:200
def findSimilarWeightedIndexes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).findSimilarWeightedIndexes(), HandlerDef(this, "controllers.Search", "findSimilarWeightedIndexes", Seq(), "POST", """""", _prefix + """queries/similarWeightedIndexes""")
)
                      

// @LINE:197
def callSearchMultimediaIndexView(section_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).callSearchMultimediaIndexView(section_id), HandlerDef(this, "controllers.Search", "callSearchMultimediaIndexView", Seq(classOf[UUID]), "GET", """""", _prefix + """searchbyfeature/$section_id<[^/]+>""")
)
                      

// @LINE:191
def multimediasearch(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).multimediasearch(), HandlerDef(this, "controllers.Search", "multimediasearch", Seq(), "GET", """""", _prefix + """multimediasearch""")
)
                      

// @LINE:196
def searchbyURL(query:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).searchbyURL(query), HandlerDef(this, "controllers.Search", "searchbyURL", Seq(classOf[String]), "GET", """""", _prefix + """searchbyURL""")
)
                      
    
}
                          

// @LINE:970
// @LINE:968
// @LINE:934
// @LINE:921
class ReverseT2C2 {
    

// @LINE:968
def uploader(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.T2C2]).uploader(), HandlerDef(this, "controllers.T2C2", "uploader", Seq(), "GET", """GET         /t2c2/docs   																				@controllers.T2C2.getDocs()""", _prefix + """t2c2/uploader""")
)
                      

// @LINE:970
def zipUploader(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.T2C2]).zipUploader(), HandlerDef(this, "controllers.T2C2", "zipUploader", Seq(), "GET", """""", _prefix + """t2c2/zipUploader""")
)
                      

// @LINE:921
def newTemplate(space:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.T2C2]).newTemplate(space), HandlerDef(this, "controllers.T2C2", "newTemplate", Seq(classOf[Option[String]]), "GET", """""", _prefix + """t2c2/templates/new""")
)
                      

// @LINE:934
def uploadFile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.T2C2]).uploadFile(), HandlerDef(this, "controllers.T2C2", "uploadFile", Seq(), "GET", """POST        /t2c2/postDragDrop                                                               @controllers.T2C2.postDragDrop
POST        /t2c2/uploadSingleFile                                                                            @controllers.Files.uploadSingleFile""", _prefix + """t2c2/zipUpload""")
)
                      
    
}
                          

// @LINE:639
// @LINE:638
// @LINE:637
class ReverseVocabularies {
    

// @LINE:637
def newVocabulary(space:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Vocabularies]).newVocabulary(space), HandlerDef(this, "controllers.Vocabularies", "newVocabulary", Seq(classOf[Option[String]]), "GET", """""", _prefix + """vocabularies/new""")
)
                      

// @LINE:639
def vocabulary(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Vocabularies]).vocabulary(id), HandlerDef(this, "controllers.Vocabularies", "vocabulary", Seq(classOf[UUID]), "GET", """""", _prefix + """vocabularies/$id<[^/]+>""")
)
                      

// @LINE:638
def submit(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Vocabularies]).submit(), HandlerDef(this, "controllers.Vocabularies", "submit", Seq(), "POST", """""", _prefix + """vocabularies/submit""")
)
                      
    
}
                          

// @LINE:818
// @LINE:165
// @LINE:164
// @LINE:162
// @LINE:161
// @LINE:160
// @LINE:159
// @LINE:158
// @LINE:157
// @LINE:156
// @LINE:155
// @LINE:154
// @LINE:153
// @LINE:152
class ReverseSpaces {
    

// @LINE:161
def updateExtractors(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).updateExtractors(id), HandlerDef(this, "controllers.Spaces", "updateExtractors", Seq(classOf[UUID]), "POST", """""", _prefix + """spaces/$id<[^/]+>/extractors""")
)
                      

// @LINE:156
def getSpace(id:UUID, size:Int, direction:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).getSpace(id, size, direction), HandlerDef(this, "controllers.Spaces", "getSpace", Seq(classOf[UUID], classOf[Int], classOf[String]), "GET", """""", _prefix + """spaces/$id<[^/]+>""")
)
                      

// @LINE:155
def followingSpaces(index:Int, size:Int, mode:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).followingSpaces(index, size, mode), HandlerDef(this, "controllers.Spaces", "followingSpaces", Seq(classOf[Int], classOf[Int], classOf[String]), "GET", """""", _prefix + """spaces/following""")
)
                      

// @LINE:162
def inviteToSpace(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).inviteToSpace(id), HandlerDef(this, "controllers.Spaces", "inviteToSpace", Seq(classOf[UUID]), "POST", """""", _prefix + """spaces/$id<[^/]+>/invite""")
)
                      

// @LINE:164
def submit(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).submit(), HandlerDef(this, "controllers.Spaces", "submit", Seq(), "POST", """""", _prefix + """spaces/submit""")
)
                      

// @LINE:160
def selectExtractors(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).selectExtractors(id), HandlerDef(this, "controllers.Spaces", "selectExtractors", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/$id<[^/]+>/extractors""")
)
                      

// @LINE:159
def manageUsers(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).manageUsers(id), HandlerDef(this, "controllers.Spaces", "manageUsers", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/$id<[^/]+>/users""")
)
                      

// @LINE:818
def stagingArea(id:UUID, index:Int, limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).stagingArea(id, index, limit), HandlerDef(this, "controllers.Spaces", "stagingArea", Seq(classOf[UUID], classOf[Int], classOf[Int]), "GET", """""", _prefix + """spaces/$id<[^/]+>/stagingArea""")
)
                      

// @LINE:153
def newSpace(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).newSpace(), HandlerDef(this, "controllers.Spaces", "newSpace", Seq(), "GET", """""", _prefix + """spaces/new""")
)
                      

// @LINE:165
def submitCopy(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).submitCopy(), HandlerDef(this, "controllers.Spaces", "submitCopy", Seq(), "POST", """""", _prefix + """spaces/submitCopy""")
)
                      

// @LINE:154
def copySpace(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).copySpace(id), HandlerDef(this, "controllers.Spaces", "copySpace", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/copy/$id<[^/]+>""")
)
                      

// @LINE:157
def updateSpace(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).updateSpace(id), HandlerDef(this, "controllers.Spaces", "updateSpace", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/$id<[^/]+>/updateSpace""")
)
                      

// @LINE:158
def addRequest(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).addRequest(id), HandlerDef(this, "controllers.Spaces", "addRequest", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/$id<[^/]+>/addRequest""")
)
                      

// @LINE:152
def list(when:String, date:String, size:Int, mode:String, owner:Option[String], showAll:Boolean, showPublic:Boolean, onlyTrial:Boolean, showOnlyShared:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).list(when, date, size, mode, owner, showAll, showPublic, onlyTrial, showOnlyShared), HandlerDef(this, "controllers.Spaces", "list", Seq(classOf[String], classOf[String], classOf[Int], classOf[String], classOf[Option[String]], classOf[Boolean], classOf[Boolean], classOf[Boolean], classOf[Boolean]), "GET", """ ----------------------------------------------------------------------
 Spaces
 ----------------------------------------------------------------------""", _prefix + """spaces""")
)
                      
    
}
                          
}
        

// @LINE:249
// @LINE:247
// @LINE:246
// @LINE:241
// @LINE:240
// @LINE:239
// @LINE:238
// @LINE:237
// @LINE:236
// @LINE:234
// @LINE:233
// @LINE:232
// @LINE:227
// @LINE:226
package securesocial.controllers.ref {


// @LINE:239
// @LINE:238
// @LINE:237
// @LINE:236
// @LINE:234
// @LINE:233
// @LINE:232
class ReverseRegistration {
    

// @LINE:232
def startSignUp(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.Registration.startSignUp(), HandlerDef(this, "securesocial.controllers.Registration", "startSignUp", Seq(), "GET", """ ----------------------------------------------------------------------
 USER REGISTRATION
 ----------------------------------------------------------------------""", _prefix + """signup""")
)
                      

// @LINE:233
def handleStartSignUp(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.Registration.handleStartSignUp(), HandlerDef(this, "securesocial.controllers.Registration", "handleStartSignUp", Seq(), "POST", """""", _prefix + """signup""")
)
                      

// @LINE:237
def handleStartResetPassword(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.Registration.handleStartResetPassword(), HandlerDef(this, "securesocial.controllers.Registration", "handleStartResetPassword", Seq(), "POST", """""", _prefix + """reset""")
)
                      

// @LINE:238
def resetPassword(token:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.Registration.resetPassword(token), HandlerDef(this, "securesocial.controllers.Registration", "resetPassword", Seq(classOf[String]), "GET", """""", _prefix + """reset/$token<[^/]+>""")
)
                      

// @LINE:234
def signUp(token:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.Registration.signUp(token), HandlerDef(this, "securesocial.controllers.Registration", "signUp", Seq(classOf[String]), "GET", """""", _prefix + """signup/$token<[^/]+>""")
)
                      

// @LINE:236
def startResetPassword(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.Registration.startResetPassword(), HandlerDef(this, "securesocial.controllers.Registration", "startResetPassword", Seq(), "GET", """""", _prefix + """reset""")
)
                      

// @LINE:239
def handleResetPassword(token:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.Registration.handleResetPassword(token), HandlerDef(this, "securesocial.controllers.Registration", "handleResetPassword", Seq(classOf[String]), "POST", """""", _prefix + """reset/$token<[^/]+>""")
)
                      
    
}
                          

// @LINE:249
// @LINE:247
// @LINE:246
class ReverseProviderController {
    

// @LINE:249
def notAuthorized(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.ProviderController.notAuthorized(), HandlerDef(this, "securesocial.controllers.ProviderController", "notAuthorized", Seq(), "GET", """""", _prefix + """not-authorized""")
)
                      

// @LINE:246
def authenticate(provider:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.ProviderController.authenticate(provider), HandlerDef(this, "securesocial.controllers.ProviderController", "authenticate", Seq(classOf[String]), "GET", """ ----------------------------------------------------------------------
 PROVIDERS ENTRY POINTS
 ----------------------------------------------------------------------""", _prefix + """authenticate/$provider<[^/]+>""")
)
                      

// @LINE:247
def authenticateByPost(provider:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.ProviderController.authenticateByPost(provider), HandlerDef(this, "securesocial.controllers.ProviderController", "authenticateByPost", Seq(classOf[String]), "POST", """""", _prefix + """authenticate/$provider<[^/]+>""")
)
                      
    
}
                          

// @LINE:241
// @LINE:240
class ReversePasswordChange {
    

// @LINE:241
def handlePasswordChange(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.PasswordChange.handlePasswordChange(), HandlerDef(this, "securesocial.controllers.PasswordChange", "handlePasswordChange", Seq(), "POST", """""", _prefix + """password""")
)
                      

// @LINE:240
def page(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.PasswordChange.page(), HandlerDef(this, "securesocial.controllers.PasswordChange", "page", Seq(), "GET", """""", _prefix + """password""")
)
                      
    
}
                          

// @LINE:227
// @LINE:226
class ReverseLoginPage {
    

// @LINE:227
def logout(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.LoginPage.logout(), HandlerDef(this, "securesocial.controllers.LoginPage", "logout", Seq(), "GET", """""", _prefix + """logout""")
)
                      

// @LINE:226
def login(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   securesocial.controllers.LoginPage.login(), HandlerDef(this, "securesocial.controllers.LoginPage", "login", Seq(), "GET", """ ----------------------------------------------------------------------
 SECURE SOCIAL
 ----------------------------------------------------------------------
 ----------------------------------------------------------------------
 LOGIN PAGES
 ----------------------------------------------------------------------""", _prefix + """login""")
)
                      
    
}
                          
}
        

// @LINE:978
// @LINE:977
// @LINE:966
// @LINE:965
// @LINE:964
// @LINE:963
// @LINE:962
// @LINE:961
// @LINE:960
// @LINE:959
// @LINE:958
// @LINE:957
// @LINE:956
// @LINE:955
// @LINE:954
// @LINE:953
// @LINE:952
// @LINE:951
// @LINE:950
// @LINE:949
// @LINE:948
// @LINE:947
// @LINE:946
// @LINE:945
// @LINE:944
// @LINE:943
// @LINE:942
// @LINE:941
// @LINE:940
// @LINE:939
// @LINE:938
// @LINE:936
// @LINE:926
// @LINE:919
// @LINE:917
// @LINE:916
// @LINE:915
// @LINE:914
// @LINE:913
// @LINE:912
// @LINE:911
// @LINE:910
// @LINE:909
// @LINE:908
// @LINE:906
// @LINE:905
// @LINE:904
// @LINE:903
// @LINE:902
// @LINE:901
// @LINE:900
// @LINE:899
// @LINE:898
// @LINE:896
// @LINE:895
// @LINE:893
// @LINE:892
// @LINE:891
// @LINE:885
// @LINE:884
// @LINE:883
// @LINE:882
// @LINE:881
// @LINE:880
// @LINE:879
// @LINE:878
// @LINE:877
// @LINE:876
// @LINE:875
// @LINE:874
// @LINE:868
// @LINE:867
// @LINE:861
// @LINE:860
// @LINE:858
// @LINE:857
// @LINE:856
// @LINE:855
// @LINE:854
// @LINE:851
// @LINE:850
// @LINE:849
// @LINE:848
// @LINE:847
// @LINE:840
// @LINE:836
// @LINE:835
// @LINE:833
// @LINE:832
// @LINE:830
// @LINE:829
// @LINE:828
// @LINE:820
// @LINE:803
// @LINE:802
// @LINE:791
// @LINE:790
// @LINE:789
// @LINE:788
// @LINE:787
// @LINE:786
// @LINE:785
// @LINE:784
// @LINE:783
// @LINE:782
// @LINE:781
// @LINE:780
// @LINE:779
// @LINE:778
// @LINE:777
// @LINE:776
// @LINE:771
// @LINE:770
// @LINE:769
// @LINE:768
// @LINE:767
// @LINE:766
// @LINE:765
// @LINE:760
// @LINE:759
// @LINE:758
// @LINE:757
// @LINE:756
// @LINE:751
// @LINE:750
// @LINE:749
// @LINE:748
// @LINE:747
// @LINE:746
// @LINE:745
// @LINE:739
// @LINE:738
// @LINE:737
// @LINE:736
// @LINE:731
// @LINE:730
// @LINE:729
// @LINE:728
// @LINE:727
// @LINE:722
// @LINE:721
// @LINE:720
// @LINE:715
// @LINE:714
// @LINE:713
// @LINE:712
// @LINE:711
// @LINE:710
// @LINE:709
// @LINE:708
// @LINE:707
// @LINE:706
// @LINE:705
// @LINE:704
// @LINE:703
// @LINE:702
// @LINE:701
// @LINE:700
// @LINE:699
// @LINE:698
// @LINE:697
// @LINE:696
// @LINE:695
// @LINE:694
// @LINE:693
// @LINE:692
// @LINE:691
// @LINE:690
// @LINE:689
// @LINE:688
// @LINE:687
// @LINE:686
// @LINE:681
// @LINE:680
// @LINE:679
// @LINE:674
// @LINE:673
// @LINE:672
// @LINE:671
// @LINE:670
// @LINE:669
// @LINE:668
// @LINE:667
// @LINE:666
// @LINE:665
// @LINE:664
// @LINE:659
// @LINE:658
// @LINE:657
// @LINE:656
// @LINE:655
// @LINE:654
// @LINE:653
// @LINE:652
// @LINE:651
// @LINE:650
// @LINE:649
// @LINE:648
// @LINE:647
// @LINE:646
// @LINE:645
// @LINE:644
// @LINE:631
// @LINE:630
// @LINE:629
// @LINE:628
// @LINE:627
// @LINE:626
// @LINE:625
// @LINE:624
// @LINE:623
// @LINE:622
// @LINE:621
// @LINE:620
// @LINE:619
// @LINE:618
// @LINE:617
// @LINE:616
// @LINE:615
// @LINE:614
// @LINE:613
// @LINE:612
// @LINE:611
// @LINE:610
// @LINE:609
// @LINE:608
// @LINE:607
// @LINE:606
// @LINE:605
// @LINE:604
// @LINE:603
// @LINE:602
// @LINE:601
// @LINE:600
// @LINE:599
// @LINE:598
// @LINE:597
// @LINE:596
// @LINE:595
// @LINE:594
// @LINE:593
// @LINE:592
// @LINE:591
// @LINE:590
// @LINE:589
// @LINE:588
// @LINE:587
// @LINE:586
// @LINE:585
// @LINE:584
// @LINE:583
// @LINE:582
// @LINE:581
// @LINE:580
// @LINE:579
// @LINE:578
// @LINE:577
// @LINE:576
// @LINE:575
// @LINE:574
// @LINE:573
// @LINE:572
// @LINE:571
// @LINE:570
// @LINE:569
// @LINE:568
// @LINE:566
// @LINE:565
// @LINE:564
// @LINE:563
// @LINE:562
// @LINE:561
// @LINE:560
// @LINE:554
// @LINE:553
// @LINE:552
// @LINE:551
// @LINE:550
// @LINE:549
// @LINE:548
// @LINE:547
// @LINE:546
// @LINE:545
// @LINE:544
// @LINE:543
// @LINE:542
// @LINE:541
// @LINE:539
// @LINE:538
// @LINE:537
// @LINE:536
// @LINE:535
// @LINE:533
// @LINE:532
// @LINE:530
// @LINE:529
// @LINE:527
// @LINE:526
// @LINE:524
// @LINE:523
// @LINE:522
// @LINE:521
// @LINE:520
// @LINE:518
// @LINE:517
// @LINE:516
// @LINE:511
// @LINE:500
// @LINE:499
// @LINE:498
// @LINE:497
// @LINE:496
// @LINE:495
// @LINE:494
// @LINE:493
// @LINE:492
// @LINE:491
// @LINE:490
// @LINE:489
// @LINE:488
// @LINE:483
// @LINE:482
// @LINE:481
// @LINE:480
// @LINE:479
// @LINE:478
// @LINE:477
// @LINE:476
// @LINE:475
// @LINE:474
// @LINE:473
// @LINE:472
// @LINE:471
// @LINE:470
// @LINE:469
// @LINE:468
// @LINE:467
// @LINE:466
// @LINE:465
// @LINE:464
// @LINE:463
// @LINE:462
// @LINE:461
// @LINE:460
// @LINE:454
// @LINE:448
// @LINE:447
// @LINE:446
// @LINE:441
// @LINE:439
// @LINE:438
// @LINE:436
// @LINE:435
// @LINE:434
// @LINE:433
// @LINE:432
// @LINE:426
// @LINE:425
// @LINE:424
// @LINE:423
// @LINE:422
// @LINE:421
// @LINE:420
// @LINE:418
// @LINE:417
// @LINE:416
// @LINE:415
// @LINE:413
// @LINE:412
// @LINE:410
// @LINE:408
// @LINE:407
// @LINE:403
// @LINE:402
// @LINE:401
// @LINE:400
// @LINE:399
// @LINE:398
// @LINE:397
// @LINE:396
// @LINE:395
// @LINE:394
// @LINE:393
// @LINE:392
// @LINE:391
// @LINE:390
// @LINE:389
// @LINE:388
// @LINE:387
// @LINE:386
// @LINE:385
// @LINE:384
// @LINE:383
// @LINE:382
// @LINE:381
// @LINE:380
// @LINE:379
// @LINE:378
// @LINE:377
// @LINE:376
// @LINE:375
// @LINE:374
// @LINE:373
// @LINE:372
// @LINE:371
// @LINE:370
// @LINE:369
// @LINE:368
// @LINE:367
// @LINE:366
// @LINE:365
// @LINE:364
// @LINE:363
// @LINE:362
// @LINE:361
// @LINE:360
// @LINE:359
// @LINE:354
// @LINE:353
// @LINE:352
// @LINE:351
// @LINE:350
// @LINE:349
// @LINE:348
// @LINE:347
// @LINE:346
// @LINE:345
// @LINE:341
// @LINE:339
// @LINE:338
// @LINE:336
// @LINE:335
// @LINE:334
// @LINE:333
// @LINE:332
// @LINE:331
// @LINE:330
// @LINE:329
// @LINE:325
// @LINE:324
// @LINE:322
// @LINE:321
// @LINE:320
// @LINE:319
// @LINE:318
// @LINE:317
// @LINE:314
// @LINE:313
// @LINE:312
// @LINE:311
// @LINE:306
// @LINE:305
// @LINE:304
// @LINE:303
// @LINE:302
// @LINE:301
// @LINE:300
// @LINE:299
// @LINE:279
// @LINE:7
package api.ref {


// @LINE:657
// @LINE:656
// @LINE:655
// @LINE:654
// @LINE:653
// @LINE:652
// @LINE:651
// @LINE:650
// @LINE:649
// @LINE:648
// @LINE:647
// @LINE:646
// @LINE:644
class ReversePreviews {
    

// @LINE:651
def editAnnotation(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).editAnnotation(id), HandlerDef(this, "api.Previews", "editAnnotation", Seq(classOf[UUID]), "POST", """""", _prefix + """api/previews/$id<[^/]+>/annotationEdit""")
)
                      

// @LINE:652
def listAnnotations(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).listAnnotations(id), HandlerDef(this, "api.Previews", "listAnnotations", Seq(classOf[UUID]), "GET", """""", _prefix + """api/previews/$id<[^/]+>/annotationsList""")
)
                      

// @LINE:650
def attachAnnotation(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).attachAnnotation(id), HandlerDef(this, "api.Previews", "attachAnnotation", Seq(classOf[UUID]), "POST", """""", _prefix + """api/previews/$id<[^/]+>/annotationAdd""")
)
                      

// @LINE:648
def uploadMetadata(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).uploadMetadata(id), HandlerDef(this, "api.Previews", "uploadMetadata", Seq(classOf[UUID]), "POST", """""", _prefix + """api/previews/$id<[^/]+>/metadata""")
)
                      

// @LINE:649
def getMetadata(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).getMetadata(id), HandlerDef(this, "api.Previews", "getMetadata", Seq(classOf[UUID]), "GET", """""", _prefix + """api/previews/$id<[^/]+>/metadata""")
)
                      

// @LINE:653
def setTitle(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).setTitle(id), HandlerDef(this, "api.Previews", "setTitle", Seq(classOf[UUID]), "POST", """""", _prefix + """api/previews/$id<[^/]+>/title""")
)
                      

// @LINE:644
def downloadPreview(preview_id:UUID, datasetid:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).downloadPreview(preview_id, datasetid), HandlerDef(this, "api.Previews", "downloadPreview", Seq(classOf[UUID], classOf[UUID]), "GET", """ ----------------------------------------------------------------------
 PREVIEWS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/previews/$preview_id<[^/]+>/textures/dataset/$datasetid<[^/]+>/json""")
)
                      

// @LINE:657
def upload(iipKey:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).upload(iipKey), HandlerDef(this, "api.Previews", "upload", Seq(classOf[String]), "POST", """""", _prefix + """api/previews""")
)
                      

// @LINE:654
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).list(), HandlerDef(this, "api.Previews", "list", Seq(), "GET", """""", _prefix + """api/previews""")
)
                      

// @LINE:656
def removePreview(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).removePreview(id), HandlerDef(this, "api.Previews", "removePreview", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/previews/$id<[^/]+>""")
)
                      

// @LINE:655
def download(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).download(id), HandlerDef(this, "api.Previews", "download", Seq(classOf[UUID]), "GET", """""", _prefix + """api/previews/$id<[^/]+>""")
)
                      

// @LINE:647
def attachTile(dzi_id:UUID, tile_id:UUID, level:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).attachTile(dzi_id, tile_id, level), HandlerDef(this, "api.Previews", "attachTile", Seq(classOf[UUID], classOf[UUID], classOf[String]), "POST", """""", _prefix + """api/previews/$dzi_id<[^/]+>/tiles/$tile_id<[^/]+>/$level<[^/]+>""")
)
                      

// @LINE:646
def getTile(dzi_id_dir:String, level:String, filename:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).getTile(dzi_id_dir, level, filename), HandlerDef(this, "api.Previews", "getTile", Seq(classOf[String], classOf[String], classOf[String]), "GET", """""", _prefix + """api/previews/$dzi_id_dir<[^/]+>/$level<[^/]+>/$filename<[^/]+>""")
)
                      
    
}
                          

// @LINE:926
class ReverseNotebooks {
    

// @LINE:926
def submit(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Notebooks]).submit(), HandlerDef(this, "api.Notebooks", "submit", Seq(), "POST", """ ----------------------------------------------------------------------
 NOTEBOOKS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/notebook/submit""")
)
                      
    
}
                          

// @LINE:885
// @LINE:884
// @LINE:883
// @LINE:882
// @LINE:881
// @LINE:880
// @LINE:879
// @LINE:878
// @LINE:877
// @LINE:876
// @LINE:875
// @LINE:874
class ReverseProxy {
    

// @LINE:883
def delete(endpoint_key:String, pathSuffix:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).delete(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "delete", Seq(classOf[String], classOf[String]), "DELETE", """""", _prefix + """api/proxy/$endpoint_key<[^/]+>""")
)
                      

// @LINE:877
def post(endpoint_key:String, pathSuffix:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).post(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "post", Seq(classOf[String], classOf[String]), "POST", """""", _prefix + """api/proxy/$endpoint_key<[^/]+>""")
)
                      

// @LINE:874
def get(endpoint_key:String, pathSuffix:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).get(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "get", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """api/proxy/$endpoint_key<[^/]+>""")
)
                      

// @LINE:880
def put(endpoint_key:String, pathSuffix:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).put(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "put", Seq(classOf[String], classOf[String]), "PUT", """""", _prefix + """api/proxy/$endpoint_key<[^/]+>""")
)
                      
    
}
                          

// @LINE:478
// @LINE:341
// @LINE:339
// @LINE:338
// @LINE:336
// @LINE:335
// @LINE:334
// @LINE:333
// @LINE:332
// @LINE:331
// @LINE:330
// @LINE:329
// @LINE:325
// @LINE:324
class ReverseMetadata {
    

// @LINE:329
def getDefinitions(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getDefinitions(), HandlerDef(this, "api.Metadata", "getDefinitions", Seq(), "GET", """""", _prefix + """api/metadata/definitions""")
)
                      

// @LINE:333
def getUrl(in_url:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getUrl(in_url), HandlerDef(this, "api.Metadata", "getUrl", Seq(classOf[String]), "GET", """""", _prefix + """api/metadata/url/$in_url<.+>""")
)
                      

// @LINE:341
def getRepository(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getRepository(id), HandlerDef(this, "api.Metadata", "getRepository", Seq(classOf[String]), "GET", """""", _prefix + """api/metadata/repository/$id<[^/]+>""")
)
                      

// @LINE:338
def listPeople(term:String, limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).listPeople(term, limit), HandlerDef(this, "api.Metadata", "listPeople", Seq(classOf[String], classOf[Int]), "GET", """""", _prefix + """api/metadata/people""")
)
                      

// @LINE:331
def getAutocompleteName(filter:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getAutocompleteName(filter), HandlerDef(this, "api.Metadata", "getAutocompleteName", Seq(classOf[String]), "GET", """""", _prefix + """api/metadata/autocompletenames""")
)
                      

// @LINE:336
def deleteDefinition(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).deleteDefinition(id), HandlerDef(this, "api.Metadata", "deleteDefinition", Seq(classOf[UUID]), "DELETE", """""", _prefix + """admin/metadata/definitions/$id<[^/]+>""")
)
                      

// @LINE:324
def addUserMetadata(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).addUserMetadata(), HandlerDef(this, "api.Metadata", "addUserMetadata", Seq(), "POST", """""", _prefix + """api/metadata.jsonld""")
)
                      

// @LINE:335
def addDefinition(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).addDefinition(), HandlerDef(this, "api.Metadata", "addDefinition", Seq(), "POST", """""", _prefix + """api/metadata/definitions""")
)
                      

// @LINE:332
def getDefinition(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getDefinition(id), HandlerDef(this, "api.Metadata", "getDefinition", Seq(classOf[UUID]), "GET", """""", _prefix + """api/metadata/definitions/$id<[^/]+>""")
)
                      

// @LINE:330
def getDefinitionsDistinctName(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getDefinitionsDistinctName(), HandlerDef(this, "api.Metadata", "getDefinitionsDistinctName", Seq(), "GET", """""", _prefix + """api/metadata/distinctdefinitions""")
)
                      

// @LINE:339
def getPerson(pid:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getPerson(pid), HandlerDef(this, "api.Metadata", "getPerson", Seq(classOf[String]), "GET", """""", _prefix + """api/metadata/people/$pid<[^/]+>""")
)
                      

// @LINE:478
def addDefinitionToSpace(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).addDefinitionToSpace(id), HandlerDef(this, "api.Metadata", "addDefinitionToSpace", Seq(classOf[UUID]), "POST", """""", _prefix + """api/spaces/$id<[^/]+>/metadata""")
)
                      

// @LINE:334
def editDefinition(id:UUID, spaceId:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).editDefinition(id, spaceId), HandlerDef(this, "api.Metadata", "editDefinition", Seq(classOf[UUID], classOf[Option[String]]), "PUT", """""", _prefix + """api/metadata/definitions/$id<[^/]+>""")
)
                      

// @LINE:325
def removeMetadata(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).removeMetadata(id), HandlerDef(this, "api.Metadata", "removeMetadata", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/metadata.jsonld/$id<[^/]+>""")
)
                      
    
}
                          

// @LINE:625
// @LINE:586
// @LINE:583
// @LINE:582
// @LINE:581
// @LINE:579
class ReverseFolders {
    

// @LINE:582
def deleteFolder(parentDatasetId:UUID, folderId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).deleteFolder(parentDatasetId, folderId), HandlerDef(this, "api.Folders", "deleteFolder", Seq(classOf[UUID], classOf[UUID]), "DELETE", """""", _prefix + """api/datasets/$parentDatasetId<[^/]+>/deleteFolder/$folderId<[^/]+>""")
)
                      

// @LINE:586
def getAllFoldersByDatasetId(datasetId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).getAllFoldersByDatasetId(datasetId), HandlerDef(this, "api.Folders", "getAllFoldersByDatasetId", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/$datasetId<[^/]+>/folders""")
)
                      

// @LINE:583
def updateFolderName(parentDatasetId:UUID, folderId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).updateFolderName(parentDatasetId, folderId), HandlerDef(this, "api.Folders", "updateFolderName", Seq(classOf[UUID], classOf[UUID]), "PUT", """""", _prefix + """api/datasets/$parentDatasetId<[^/]+>/updateName/$folderId<[^/]+>""")
)
                      

// @LINE:625
def createFolder(parentDatasetId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).createFolder(parentDatasetId), HandlerDef(this, "api.Folders", "createFolder", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$parentDatasetId<[^/]+>/newFolder""")
)
                      

// @LINE:581
def moveFileToDataset(datasetId:UUID, fromFolder:UUID, fileId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).moveFileToDataset(datasetId, fromFolder, fileId), HandlerDef(this, "api.Folders", "moveFileToDataset", Seq(classOf[UUID], classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/datasets/$datasetId<[^/]+>/moveToDataset/$fromFolder<[^/]+>/$fileId<[^/]+>""")
)
                      

// @LINE:579
def moveFileBetweenFolders(datasetId:UUID, toFolder:UUID, fileId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).moveFileBetweenFolders(datasetId, toFolder, fileId), HandlerDef(this, "api.Folders", "moveFileBetweenFolders", Seq(classOf[UUID], classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/datasets/$datasetId<[^/]+>/moveFile/$toFolder<[^/]+>/$fileId<[^/]+>""")
)
                      
    
}
                          

// @LINE:306
// @LINE:305
// @LINE:304
// @LINE:303
// @LINE:302
// @LINE:301
// @LINE:300
// @LINE:299
// @LINE:279
class ReverseAdmin {
    

// @LINE:303
def deleteAllData(resetAll:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).deleteAllData(resetAll), HandlerDef(this, "api.Admin", "deleteAllData", Seq(classOf[Boolean]), "DELETE", """""", _prefix + """api/delete-data""")
)
                      

// @LINE:304
def submitAppearance(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).submitAppearance(), HandlerDef(this, "api.Admin", "submitAppearance", Seq(), "POST", """""", _prefix + """api/changeAppearance""")
)
                      

// @LINE:279
def sensorsConfig(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).sensorsConfig(), HandlerDef(this, "api.Admin", "sensorsConfig", Seq(), "POST", """""", _prefix + """api/sensors/config""")
)
                      

// @LINE:305
def reindex(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).reindex(), HandlerDef(this, "api.Admin", "reindex", Seq(), "POST", """""", _prefix + """api/reindex""")
)
                      

// @LINE:299
def mail(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).mail(), HandlerDef(this, "api.Admin", "mail", Seq(), "POST", """----------------------------------------------------------------------
 ADMIN
----------------------------------------------------------------------""", _prefix + """api/admin/mail""")
)
                      

// @LINE:306
def updateConfiguration(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).updateConfiguration(), HandlerDef(this, "api.Admin", "updateConfiguration", Seq(), "POST", """""", _prefix + """api/admin/configuration""")
)
                      

// @LINE:301
def users(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).users(), HandlerDef(this, "api.Admin", "users", Seq(), "POST", """""", _prefix + """api/admin/users""")
)
                      

// @LINE:300
def emailZipUpload(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).emailZipUpload(), HandlerDef(this, "api.Admin", "emailZipUpload", Seq(), "POST", """""", _prefix + """api/admin/mailZip""")
)
                      
    
}
                          

// @LINE:791
// @LINE:789
// @LINE:788
// @LINE:787
// @LINE:786
// @LINE:785
// @LINE:784
// @LINE:783
// @LINE:782
// @LINE:781
// @LINE:780
// @LINE:779
// @LINE:778
// @LINE:777
// @LINE:776
class ReverseUsers {
    

// @LINE:780
def updateName(id:UUID, firstName:String, lastName:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).updateName(id, firstName, lastName), HandlerDef(this, "api.Users", "updateName", Seq(classOf[UUID], classOf[String], classOf[String]), "POST", """""", _prefix + """api/users/$id<[^/]+>/updateName""")
)
                      

// @LINE:783
def keysAdd(name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).keysAdd(name), HandlerDef(this, "api.Users", "keysAdd", Seq(classOf[String]), "POST", """""", _prefix + """api/users/keys""")
)
                      

// @LINE:791
def findById(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).findById(id), HandlerDef(this, "api.Users", "findById", Seq(classOf[UUID]), "GET", """""", _prefix + """api/users/$id<[^/]+>""")
)
                      

// @LINE:781
def keysList(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).keysList(), HandlerDef(this, "api.Users", "keysList", Seq(), "GET", """""", _prefix + """api/users/keys""")
)
                      

// @LINE:779
def getUser(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).getUser(), HandlerDef(this, "api.Users", "getUser", Seq(), "GET", """""", _prefix + """api/me""")
)
                      

// @LINE:776
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).list(), HandlerDef(this, "api.Users", "list", Seq(), "GET", """ ----------------------------------------------------------------------
 USERS API
 ----------------------------------------------------------------------""", _prefix + """api/users""")
)
                      

// @LINE:786
def createNewListInUser(email:String, field:String, fieldList:List[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).createNewListInUser(email, field, fieldList), HandlerDef(this, "api.Users", "createNewListInUser", Seq(classOf[String], classOf[String], classOf[List[String]]), "POST", """""", _prefix + """api/users/createNewListInUser""")
)
                      

// @LINE:777
def findByEmail(email:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).findByEmail(email), HandlerDef(this, "api.Users", "findByEmail", Seq(classOf[String]), "GET", """""", _prefix + """api/users/email/$email<[^/]+>""")
)
                      

// @LINE:785
def addUserDatasetView(email:String, dataset:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).addUserDatasetView(email, dataset), HandlerDef(this, "api.Users", "addUserDatasetView", Seq(classOf[String], classOf[UUID]), "POST", """""", _prefix + """api/users/addUserDatasetView""")
)
                      

// @LINE:789
def unfollow(followeeUUID:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).unfollow(followeeUUID), HandlerDef(this, "api.Users", "unfollow", Seq(classOf[UUID]), "POST", """""", _prefix + """api/users/unfollow""")
)
                      

// @LINE:778
def deleteUser(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).deleteUser(id), HandlerDef(this, "api.Users", "deleteUser", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/users/$id<[^/]+>""")
)
                      

// @LINE:788
def follow(followeeUUID:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).follow(followeeUUID), HandlerDef(this, "api.Users", "follow", Seq(classOf[UUID]), "POST", """""", _prefix + """api/users/follow""")
)
                      

// @LINE:782
def keysGet(name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).keysGet(name), HandlerDef(this, "api.Users", "keysGet", Seq(classOf[String]), "GET", """""", _prefix + """api/users/keys/$name<[^/]+>""")
)
                      

// @LINE:784
def keysDelete(key:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).keysDelete(key), HandlerDef(this, "api.Users", "keysDelete", Seq(classOf[String]), "DELETE", """""", _prefix + """api/users/keys/$key<[^/]+>""")
)
                      
    
}
                          

// @LINE:354
// @LINE:353
// @LINE:352
// @LINE:351
// @LINE:350
// @LINE:349
// @LINE:348
// @LINE:347
// @LINE:346
// @LINE:345
class ReverseLogos {
    

// @LINE:354
def deletePath(path:String, name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).deletePath(path, name), HandlerDef(this, "api.Logos", "deletePath", Seq(classOf[String], classOf[String]), "DELETE", """""", _prefix + """api/logos/$path<[^/]+>/$name<[^/]+>""")
)
                      

// @LINE:346
def upload(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).upload(), HandlerDef(this, "api.Logos", "upload", Seq(), "POST", """""", _prefix + """api/logos""")
)
                      

// @LINE:349
def downloadId(id:UUID, default:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).downloadId(id, default), HandlerDef(this, "api.Logos", "downloadId", Seq(classOf[UUID], classOf[Option[String]]), "GET", """""", _prefix + """api/logos/$id<[^/]+>/blob""")
)
                      

// @LINE:345
def list(path:Option[String], name:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).list(path, name), HandlerDef(this, "api.Logos", "list", Seq(classOf[Option[String]], classOf[Option[String]]), "GET", """ ----------------------------------------------------------------------
 LOGOS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/logos""")
)
                      

// @LINE:350
def deleteId(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).deleteId(id), HandlerDef(this, "api.Logos", "deleteId", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/logos/$id<[^/]+>""")
)
                      

// @LINE:348
def putId(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).putId(id), HandlerDef(this, "api.Logos", "putId", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/logos/$id<[^/]+>""")
)
                      

// @LINE:353
def downloadPath(path:String, name:String, default:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).downloadPath(path, name, default), HandlerDef(this, "api.Logos", "downloadPath", Seq(classOf[String], classOf[String], classOf[Option[String]]), "GET", """""", _prefix + """api/logos/$path<[^/]+>/$name<[^/]+>/blob""")
)
                      

// @LINE:347
def getId(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).getId(id), HandlerDef(this, "api.Logos", "getId", Seq(classOf[UUID]), "GET", """""", _prefix + """api/logos/$id<[^/]+>""")
)
                      

// @LINE:351
def getPath(path:String, name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).getPath(path, name), HandlerDef(this, "api.Logos", "getPath", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """api/logos/$path<[^/]+>/$name<[^/]+>""")
)
                      

// @LINE:352
def putPath(path:String, name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).putPath(path, name), HandlerDef(this, "api.Logos", "putPath", Seq(classOf[String], classOf[String]), "PUT", """""", _prefix + """api/logos/$path<[^/]+>/$name<[^/]+>""")
)
                      
    
}
                          

// @LINE:766
class ReverseGeometry {
    

// @LINE:766
def uploadGeometry(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Geometry]).uploadGeometry(), HandlerDef(this, "api.Geometry", "uploadGeometry", Seq(), "POST", """""", _prefix + """api/geometries""")
)
                      
    
}
                          

// @LINE:674
// @LINE:673
// @LINE:672
// @LINE:671
// @LINE:670
// @LINE:669
// @LINE:668
// @LINE:667
// @LINE:666
// @LINE:665
// @LINE:664
class ReverseSections {
    

// @LINE:667
def attachThumbnail(id:UUID, thumbnail_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).attachThumbnail(id, thumbnail_id), HandlerDef(this, "api.Sections", "attachThumbnail", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/sections/$id<[^/]+>/thumbnails/$thumbnail_id<[^/]+>""")
)
                      

// @LINE:674
def delete(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).delete(id), HandlerDef(this, "api.Sections", "delete", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/sections/$id<[^/]+>""")
)
                      

// @LINE:670
def removeTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).removeTags(id), HandlerDef(this, "api.Sections", "removeTags", Seq(classOf[UUID]), "POST", """""", _prefix + """api/sections/$id<[^/]+>/tags/remove""")
)
                      

// @LINE:671
def removeAllTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).removeAllTags(id), HandlerDef(this, "api.Sections", "removeAllTags", Seq(classOf[UUID]), "POST", """""", _prefix + """api/sections/$id<[^/]+>/tags/remove_all""")
)
                      

// @LINE:673
def get(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).get(id), HandlerDef(this, "api.Sections", "get", Seq(classOf[UUID]), "GET", """""", _prefix + """api/sections/$id<[^/]+>""")
)
                      

// @LINE:669
def addTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).addTags(id), HandlerDef(this, "api.Sections", "addTags", Seq(classOf[UUID]), "POST", """""", _prefix + """api/sections/$id<[^/]+>/tags""")
)
                      

// @LINE:664
def add(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).add(), HandlerDef(this, "api.Sections", "add", Seq(), "POST", """ ----------------------------------------------------------------------
 SECTIONS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/sections""")
)
                      

// @LINE:668
def getTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).getTags(id), HandlerDef(this, "api.Sections", "getTags", Seq(classOf[UUID]), "GET", """""", _prefix + """api/sections/$id<[^/]+>/tags""")
)
                      

// @LINE:666
def description(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).description(id), HandlerDef(this, "api.Sections", "description", Seq(classOf[UUID]), "POST", """""", _prefix + """api/sections/$id<[^/]+>/description""")
)
                      

// @LINE:665
def comment(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).comment(id), HandlerDef(this, "api.Sections", "comment", Seq(classOf[UUID]), "POST", """""", _prefix + """api/sections/$id<[^/]+>/comments""")
)
                      
    
}
                          

// @LINE:771
class ReverseInstitutions {
    

// @LINE:771
def addinstitution(institution:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Institutions]).addinstitution(institution), HandlerDef(this, "api.Institutions", "addinstitution", Seq(classOf[String]), "POST", """""", _prefix + """api/institutions/addinstitution""")
)
                      
    
}
                          

// @LINE:739
// @LINE:738
// @LINE:737
// @LINE:736
class ReverseComments {
    

// @LINE:739
def editComment(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Comments]).editComment(id), HandlerDef(this, "api.Comments", "editComment", Seq(classOf[UUID]), "POST", """""", _prefix + """api/comment/$id<[^/]+>/editComment""")
)
                      

// @LINE:737
def comment(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Comments]).comment(id), HandlerDef(this, "api.Comments", "comment", Seq(classOf[UUID]), "POST", """""", _prefix + """api/comment/$id<[^/]+>""")
)
                      

// @LINE:736
def mentionInComment(userID:UUID, resourceID:UUID, resourceName:String, resourceType:String, commenterId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Comments]).mentionInComment(userID, resourceID, resourceName, resourceType, commenterId), HandlerDef(this, "api.Comments", "mentionInComment", Seq(classOf[UUID], classOf[UUID], classOf[String], classOf[String], classOf[UUID]), "POST", """ ----------------------------------------------------------------------
 COMMENTS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/comment/mention""")
)
                      

// @LINE:738
def removeComment(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Comments]).removeComment(id), HandlerDef(this, "api.Comments", "removeComment", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/comment/$id<[^/]+>/removeComment""")
)
                      
    
}
                          

// @LINE:977
class ReverseTree {
    

// @LINE:977
def getChildrenOfNode(nodeId:Option[String], nodeType:String, mine:Boolean, shared:Boolean, public:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Tree]).getChildrenOfNode(nodeId, nodeType, mine, shared, public), HandlerDef(this, "api.Tree", "getChildrenOfNode", Seq(classOf[Option[String]], classOf[String], classOf[Boolean], classOf[Boolean], classOf[Boolean]), "GET", """""", _prefix + """api/tree/getChildrenOfNode""")
)
                      
    
}
                          

// @LINE:868
// @LINE:867
class ReverseVocabularyTerms {
    

// @LINE:868
def get(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.VocabularyTerms]).get(id), HandlerDef(this, "api.VocabularyTerms", "get", Seq(classOf[UUID]), "GET", """""", _prefix + """api/vocabterms/$id<[^/]+>""")
)
                      

// @LINE:867
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.VocabularyTerms]).list(), HandlerDef(this, "api.VocabularyTerms", "list", Seq(), "GET", """""", _prefix + """api/vocabterms/list""")
)
                      
    
}
                          

// @LINE:959
// @LINE:956
// @LINE:645
// @LINE:454
// @LINE:446
// @LINE:441
// @LINE:439
// @LINE:438
// @LINE:436
// @LINE:435
// @LINE:434
// @LINE:433
// @LINE:432
// @LINE:403
// @LINE:402
// @LINE:401
// @LINE:400
// @LINE:399
// @LINE:398
// @LINE:397
// @LINE:396
// @LINE:395
// @LINE:394
// @LINE:393
// @LINE:392
// @LINE:391
// @LINE:390
// @LINE:389
// @LINE:388
// @LINE:387
// @LINE:386
// @LINE:385
// @LINE:384
// @LINE:383
// @LINE:382
// @LINE:381
// @LINE:380
// @LINE:379
// @LINE:378
// @LINE:377
// @LINE:376
// @LINE:375
// @LINE:374
// @LINE:373
// @LINE:372
// @LINE:371
// @LINE:370
// @LINE:369
// @LINE:368
// @LINE:367
// @LINE:366
// @LINE:365
// @LINE:364
// @LINE:363
// @LINE:362
// @LINE:361
// @LINE:360
// @LINE:359
// @LINE:322
// @LINE:321
// @LINE:320
class ReverseFiles {
    

// @LINE:398
def updateFileName(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).updateFileName(id), HandlerDef(this, "api.Files", "updateFileName", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/files/$id<[^/]+>/filename""")
)
                      

// @LINE:380
def updateMetadata(id:UUID, extractor_id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).updateMetadata(id, extractor_id), HandlerDef(this, "api.Files", "updateMetadata", Seq(classOf[UUID], classOf[String]), "POST", """""", _prefix + """api/files/$id<[^/]+>/updateMetadata""")
)
                      

// @LINE:374
def getRDFUserMetadata(id:UUID, mappingNum:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getRDFUserMetadata(id, mappingNum), HandlerDef(this, "api.Files", "getRDFUserMetadata", Seq(classOf[UUID], classOf[String]), "GET", """""", _prefix + """api/files/rdfUserMetadata/$id<[^/]+>""")
)
                      

// @LINE:435
def getPreviews(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getPreviews(id), HandlerDef(this, "api.Files", "getPreviews", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/getPreviews""")
)
                      

// @LINE:360
def attachTexture(three_d_file_id:UUID, texture_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).attachTexture(three_d_file_id, texture_id), HandlerDef(this, "api.Files", "attachTexture", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/files/$three_d_file_id<[^/]+>/3dTextures/$texture_id<[^/]+>""")
)
                      

// @LINE:396
def updateLicense(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).updateLicense(id), HandlerDef(this, "api.Files", "updateLicense", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/license""")
)
                      

// @LINE:366
def searchFilesUserMetadata(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).searchFilesUserMetadata(), HandlerDef(this, "api.Files", "searchFilesUserMetadata", Seq(), "POST", """""", _prefix + """api/files/searchusermetadata""")
)
                      

// @LINE:368
def uploadToDataset(id:UUID, showPreviews:String, originalZipFile:String, flags:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).uploadToDataset(id, showPreviews, originalZipFile, flags), HandlerDef(this, "api.Files", "uploadToDataset", Seq(classOf[UUID], classOf[String], classOf[String], classOf[String]), "POST", """""", _prefix + """api/uploadToDataset/withFlags/$id<[^/]+>/$flags<[^/]+>""")
)
                      

// @LINE:322
def removeMetadataJsonLD(id:UUID, extractor:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).removeMetadataJsonLD(id, extractor), HandlerDef(this, "api.Files", "removeMetadataJsonLD", Seq(classOf[UUID], classOf[Option[String]]), "DELETE", """""", _prefix + """api/files/$id<[^/]+>/metadata.jsonld""")
)
                      

// @LINE:361
def attachThumbnail(file_id:UUID, thumbnail_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).attachThumbnail(file_id, thumbnail_id), HandlerDef(this, "api.Files", "attachThumbnail", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/files/$file_id<[^/]+>/thumbnails/$thumbnail_id<[^/]+>""")
)
                      

// @LINE:446
def dumpFilesMetadata(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).dumpFilesMetadata(), HandlerDef(this, "api.Files", "dumpFilesMetadata", Seq(), "POST", """ ----------------------------------------------------------------------
 DANGEROUS ENDPOINTS
 ----------------------------------------------------------------------""", _prefix + """api/dumpFilesMd""")
)
                      

// @LINE:381
def addVersusMetadata(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).addVersusMetadata(id), HandlerDef(this, "api.Files", "addVersusMetadata", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/versus_metadata""")
)
                      

// @LINE:372
def sendJob(fileId:UUID, fileType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).sendJob(fileId, fileType), HandlerDef(this, "api.Files", "sendJob", Seq(classOf[UUID], classOf[String]), "POST", """""", _prefix + """api/files/sendJob/$fileId<[^/]+>/$fileType<[^/]+>""")
)
                      

// @LINE:399
def reindex(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).reindex(id), HandlerDef(this, "api.Files", "reindex", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/reindex""")
)
                      

// @LINE:401
def paths(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).paths(id), HandlerDef(this, "api.Files", "paths", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/paths""")
)
                      

// @LINE:436
def isBeingProcessed(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).isBeingProcessed(id), HandlerDef(this, "api.Files", "isBeingProcessed", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/isBeingProcessed""")
)
                      

// @LINE:379
def getMetadataDefinitions(id:UUID, space:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getMetadataDefinitions(id, space), HandlerDef(this, "api.Files", "getMetadataDefinitions", Seq(classOf[UUID], classOf[Option[String]]), "GET", """""", _prefix + """api/files/$id<[^/]+>/metadataDefinitions""")
)
                      

// @LINE:376
def removeFile(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).removeFile(id), HandlerDef(this, "api.Files", "removeFile", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/remove""")
)
                      

// @LINE:382
def getVersusMetadataJSON(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getVersusMetadataJSON(id), HandlerDef(this, "api.Files", "getVersusMetadataJSON", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/versus_metadata""")
)
                      

// @LINE:400
def users(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).users(id), HandlerDef(this, "api.Files", "users", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/users""")
)
                      

// @LINE:393
def removeTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).removeTags(id), HandlerDef(this, "api.Files", "removeTags", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/tags/remove""")
)
                      

// @LINE:378
def addMetadata(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).addMetadata(id), HandlerDef(this, "api.Files", "addMetadata", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/metadata""")
)
                      

// @LINE:432
def attachPreview(id:UUID, p_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).attachPreview(id, p_id), HandlerDef(this, "api.Files", "attachPreview", Seq(classOf[UUID], classOf[UUID]), "POST", """ ----------------------------------------------------------------------
 PREVIEWS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/files/$id<[^/]+>/previews/$p_id<[^/]+>""")
)
                      

// @LINE:394
def removeAllTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).removeAllTags(id), HandlerDef(this, "api.Files", "removeAllTags", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/tags/remove_all""")
)
                      

// @LINE:433
def getWithPreviewUrls(file:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getWithPreviewUrls(file), HandlerDef(this, "api.Files", "getWithPreviewUrls", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/previewJson/$file<[^/]+>""")
)
                      

// @LINE:438
def getTexture(three_d_file_id:UUID, filename:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getTexture(three_d_file_id, filename), HandlerDef(this, "api.Files", "getTexture", Seq(classOf[UUID], classOf[String]), "GET", """GET		   /api/files/:three_d_file_id/:filename			                        @api.Files.getGeometry(three_d_file_id: UUID, filename)""", _prefix + """api/files/$three_d_file_id<[^/]+>/$filename<[^/]+>""")
)
                      

// @LINE:645
def downloadByDatasetAndFilename(dataset_id:UUID, filename:String, preview_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).downloadByDatasetAndFilename(dataset_id, filename, preview_id), HandlerDef(this, "api.Files", "downloadByDatasetAndFilename", Seq(classOf[UUID], classOf[String], classOf[UUID]), "GET", """""", _prefix + """api/previews/$preview_id<[^/]+>/textures/dataset/$dataset_id<[^/]+>//$filename<[^/]+>""")
)
                      

// @LINE:377
def get(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).get(id), HandlerDef(this, "api.Files", "get", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/metadata""")
)
                      

// @LINE:402
def changeOwner(id:UUID, ownerId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).changeOwner(id, ownerId), HandlerDef(this, "api.Files", "changeOwner", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/changeOwner/$ownerId<[^/]+>""")
)
                      

// @LINE:362
def attachQueryThumbnail(file_id:UUID, thumbnail_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).attachQueryThumbnail(file_id, thumbnail_id), HandlerDef(this, "api.Files", "attachQueryThumbnail", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/queries/$file_id<[^/]+>/thumbnails/$thumbnail_id<[^/]+>""")
)
                      

// @LINE:373
def getRDFURLsForFile(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getRDFURLsForFile(id), HandlerDef(this, "api.Files", "getRDFURLsForFile", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/getRDFURLsForFile/$id<[^/]+>""")
)
                      

// @LINE:392
def addTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).addTags(id), HandlerDef(this, "api.Files", "addTags", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/tags""")
)
                      

// @LINE:367
def searchFilesGeneralMetadata(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).searchFilesGeneralMetadata(), HandlerDef(this, "api.Files", "searchFilesGeneralMetadata", Seq(), "POST", """""", _prefix + """api/files/searchmetadata""")
)
                      

// @LINE:384
def addUserMetadata(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).addUserMetadata(id), HandlerDef(this, "api.Files", "addUserMetadata", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/usermetadata""")
)
                      

// @LINE:391
def getTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getTags(id), HandlerDef(this, "api.Files", "getTags", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/tags""")
)
                      

// @LINE:364
def upload(showPreviews:String, originalZipFile:String, flags:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).upload(showPreviews, originalZipFile, flags), HandlerDef(this, "api.Files", "upload", Seq(classOf[String], classOf[String], classOf[String]), "POST", """""", _prefix + """api/files""")
)
                      

// @LINE:389
def comment(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).comment(id), HandlerDef(this, "api.Files", "comment", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/comment""")
)
                      

// @LINE:363
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).list(), HandlerDef(this, "api.Files", "list", Seq(), "GET", """""", _prefix + """api/files""")
)
                      

// @LINE:434
def filePreviewsList(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).filePreviewsList(id), HandlerDef(this, "api.Files", "filePreviewsList", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/listpreviews""")
)
                      

// @LINE:359
def attachGeometry(three_d_file_id:UUID, geometry_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).attachGeometry(three_d_file_id, geometry_id), HandlerDef(this, "api.Files", "attachGeometry", Seq(classOf[UUID], classOf[UUID]), "POST", """ ----------------------------------------------------------------------
 FILES ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/files/$three_d_file_id<[^/]+>/geometries/$geometry_id<[^/]+>""")
)
                      

// @LINE:320
def addMetadataJsonLD(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).addMetadataJsonLD(id), HandlerDef(this, "api.Files", "addMetadataJsonLD", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/metadata.jsonld""")
)
                      

// @LINE:371
def uploadIntermediate(idAndFlags:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).uploadIntermediate(idAndFlags), HandlerDef(this, "api.Files", "uploadIntermediate", Seq(classOf[String]), "POST", """""", _prefix + """api/files/uploadIntermediate/$idAndFlags<[^/]+>""")
)
                      

// @LINE:375
def download(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).download(id), HandlerDef(this, "api.Files", "download", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/blob""")
)
                      

// @LINE:386
def getXMLMetadataJSON(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getXMLMetadataJSON(id), HandlerDef(this, "api.Files", "getXMLMetadataJSON", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/xmlmetadatajson""")
)
                      

// @LINE:370
def updateDescription(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).updateDescription(id), HandlerDef(this, "api.Files", "updateDescription", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/files/$id<[^/]+>/updateDescription""")
)
                      

// @LINE:403
def getOwner(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getOwner(id), HandlerDef(this, "api.Files", "getOwner", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/getOwner""")
)
                      

// @LINE:956
def uploadToDatasetWithDescription(id:UUID, showPreviews:String, originalZipFile:String, flags:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).uploadToDatasetWithDescription(id, showPreviews, originalZipFile, flags), HandlerDef(this, "api.Files", "uploadToDatasetWithDescription", Seq(classOf[UUID], classOf[String], classOf[String], classOf[String]), "POST", """""", _prefix + """t2c2/uploadToDataset/$id<[^/]+>""")
)
                      

// @LINE:388
def unfollow(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).unfollow(id), HandlerDef(this, "api.Files", "unfollow", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/unfollow""")
)
                      

// @LINE:439
def downloadquery(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).downloadquery(id), HandlerDef(this, "api.Files", "downloadquery", Seq(classOf[UUID]), "GET", """""", _prefix + """api/queries/$id<[^/]+>""")
)
                      

// @LINE:321
def getMetadataJsonLD(id:UUID, extractor:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getMetadataJsonLD(id, extractor), HandlerDef(this, "api.Files", "getMetadataJsonLD", Seq(classOf[UUID], classOf[Option[String]]), "GET", """""", _prefix + """api/files/$id<[^/]+>/metadata.jsonld""")
)
                      

// @LINE:397
def extract(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).extract(id), HandlerDef(this, "api.Files", "extract", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/extracted_metadata""")
)
                      

// @LINE:387
def follow(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).follow(id), HandlerDef(this, "api.Files", "follow", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$id<[^/]+>/follow""")
)
                      

// @LINE:385
def getTechnicalMetadataJSON(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getTechnicalMetadataJSON(id), HandlerDef(this, "api.Files", "getTechnicalMetadataJSON", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/technicalmetadatajson""")
)
                      

// @LINE:383
def getUserMetadataJSON(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getUserMetadataJSON(id), HandlerDef(this, "api.Files", "getUserMetadataJSON", Seq(classOf[UUID]), "GET", """""", _prefix + """api/files/$id<[^/]+>/usermetadatajson""")
)
                      
    
}
                          

// @LINE:7
class ReverseApiHelp {
    

// @LINE:7
def options(path:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.ApiHelp.options(path), HandlerDef(this, "api.ApiHelp", "options", Seq(classOf[String]), "OPTIONS", """ ----------------------------------------------------------------------
 ROUTES
 This file defines all application routes (Higher priority routes first)
 ----------------------------------------------------------------------
make this the first rule
OPTIONS       /*path                                                                   @controllers.Application.options(path)""", _prefix + """$path<.+>""")
)
                      
    
}
                          

// @LINE:722
// @LINE:721
// @LINE:720
class ReverseThumbnails {
    

// @LINE:722
def removeThumbnail(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Thumbnails]).removeThumbnail(id), HandlerDef(this, "api.Thumbnails", "removeThumbnail", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/thumbnails/$id<[^/]+>""")
)
                      

// @LINE:721
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Thumbnails]).list(), HandlerDef(this, "api.Thumbnails", "list", Seq(), "GET", """""", _prefix + """api/thumbnails""")
)
                      

// @LINE:720
def uploadThumbnail(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Thumbnails]).uploadThumbnail(), HandlerDef(this, "api.Thumbnails", "uploadThumbnail", Seq(), "POST", """ ----------------------------------------------------------------------
 THUMBNAILS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/fileThumbnail""")
)
                      
    
}
                          

// @LINE:659
// @LINE:658
class ReverseIndexes {
    

// @LINE:659
def features(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Indexes]).features(), HandlerDef(this, "api.Indexes", "features", Seq(), "POST", """""", _prefix + """api/indexes/features""")
)
                      

// @LINE:658
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Indexes]).index(), HandlerDef(this, "api.Indexes", "index", Seq(), "POST", """""", _prefix + """api/indexes""")
)
                      
    
}
                          

// @LINE:554
// @LINE:553
// @LINE:552
// @LINE:551
// @LINE:550
// @LINE:549
// @LINE:548
// @LINE:547
// @LINE:546
// @LINE:545
// @LINE:544
// @LINE:543
// @LINE:542
// @LINE:541
// @LINE:539
// @LINE:538
// @LINE:537
// @LINE:536
// @LINE:535
// @LINE:533
// @LINE:530
// @LINE:529
// @LINE:527
// @LINE:526
// @LINE:524
// @LINE:522
// @LINE:521
// @LINE:520
// @LINE:518
// @LINE:517
// @LINE:516
// @LINE:511
// @LINE:500
// @LINE:499
// @LINE:498
// @LINE:497
// @LINE:496
// @LINE:495
// @LINE:494
// @LINE:493
// @LINE:492
// @LINE:491
// @LINE:490
// @LINE:489
// @LINE:488
class ReverseCollections {
    

// @LINE:553
def changeOwnerDatasetOnly(ds_id:UUID, userId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).changeOwnerDatasetOnly(ds_id, userId), HandlerDef(this, "api.Collections", "changeOwnerDatasetOnly", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/collections/$ds_id<[^/]+>/changeOwnerDataset/$userId<[^/]+>""")
)
                      

// @LINE:511
def uploadZipToSpace(spaceId:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).uploadZipToSpace(spaceId), HandlerDef(this, "api.Collections", "uploadZipToSpace", Seq(classOf[Option[String]]), "POST", """GET            /api/collections/trash                                                   @api.Collections.listCollectionsInTrash()
POST           /api/collections/uploadZip                                               @api.Collections.uploadZip()""", _prefix + """api/collections/uploadZipToSpace""")
)
                      

// @LINE:548
def getParentCollections(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getParentCollections(coll_id), HandlerDef(this, "api.Collections", "getParentCollections", Seq(classOf[UUID]), "GET", """""", _prefix + """api/collections/$coll_id<[^/]+>/getParentCollections""")
)
                      

// @LINE:492
def createCollection(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).createCollection(), HandlerDef(this, "api.Collections", "createCollection", Seq(), "POST", """""", _prefix + """api/collections""")
)
                      

// @LINE:491
def listPossibleParents(currentCollectionId:String, title:Option[String], date:Option[String], limit:Int, exact:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).listPossibleParents(currentCollectionId, title, date, limit, exact), HandlerDef(this, "api.Collections", "listPossibleParents", Seq(classOf[String], classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]), "GET", """""", _prefix + """api/collections/possibleParents""")
)
                      

// @LINE:530
def reindex(coll_id:UUID, recursive:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).reindex(coll_id, recursive), HandlerDef(this, "api.Collections", "reindex", Seq(classOf[UUID], classOf[Boolean]), "POST", """""", _prefix + """api/collections/$coll_id<[^/]+>/reindex""")
)
                      

// @LINE:550
def removeFromSpaceAllowed(coll_id:UUID, space_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeFromSpaceAllowed(coll_id, space_id), HandlerDef(this, "api.Collections", "removeFromSpaceAllowed", Seq(classOf[UUID], classOf[UUID]), "GET", """""", _prefix + """api/collections/$coll_id<[^/]+>/removeFromSpaceAllowed/$space_id<[^/]+>""")
)
                      

// @LINE:549
def attachSubCollection(coll_id:UUID, sub_coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).attachSubCollection(coll_id, sub_coll_id), HandlerDef(this, "api.Collections", "attachSubCollection", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/collections/$coll_id<[^/]+>/addSubCollection/$sub_coll_id<[^/]+>""")
)
                      

// @LINE:544
def createCollectionWithParent(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).createCollectionWithParent(), HandlerDef(this, "api.Collections", "createCollectionWithParent", Seq(), "POST", """""", _prefix + """api/collections/newCollectionWithParent""")
)
                      

// @LINE:545
def getChildCollectionIds(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getChildCollectionIds(coll_id), HandlerDef(this, "api.Collections", "getChildCollectionIds", Seq(classOf[UUID]), "GET", """""", _prefix + """api/collections/$coll_id<[^/]+>/getChildCollectionIds""")
)
                      

// @LINE:497
def listCollectionsInTrash(limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).listCollectionsInTrash(limit), HandlerDef(this, "api.Collections", "listCollectionsInTrash", Seq(classOf[Int]), "GET", """""", _prefix + """api/collections/listTrash""")
)
                      

// @LINE:516
def moveChildCollection(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).moveChildCollection(id), HandlerDef(this, "api.Collections", "moveChildCollection", Seq(classOf[UUID]), "POST", """DELETE         /api/collections/deleteAll                                               @api.Collections.removeAllCollectionsOfUser()
GET            /api/collections/:id/download                                            @api.Collections.download(id: UUID, bagit : Boolean ?= true, compression: Int ?= -1)
POST           /api/collections/:fileId/createFromZip                                   @api.Collections.createFromZip(fileId : UUID)
POST           /api/collections/:id/copyCollectionToSpace/:spaceId                      @api.Collections.copyCollectionToSpace(id : UUID, spaceId : UUID)""", _prefix + """api/collections/$id<[^/]+>/moveChildCollection""")
)
                      

// @LINE:518
def moveDatasetToNewCollection(oldCollection:String, dataset:String, newCollection:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).moveDatasetToNewCollection(oldCollection, dataset, newCollection), HandlerDef(this, "api.Collections", "moveDatasetToNewCollection", Seq(classOf[String], classOf[String], classOf[String]), "POST", """""", _prefix + """api/collections/$oldCollection<[^/]+>/moveDataset/$dataset<[^/]+>/newCollection/$newCollection<[^/]+>""")
)
                      

// @LINE:499
def clearOldCollectionsTrash(days:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).clearOldCollectionsTrash(days), HandlerDef(this, "api.Collections", "clearOldCollectionsTrash", Seq(classOf[Int]), "DELETE", """""", _prefix + """api/collections/clearOldCollectionsTrash""")
)
                      

// @LINE:538
def updateCollectionName(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).updateCollectionName(coll_id), HandlerDef(this, "api.Collections", "updateCollectionName", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/collections/$coll_id<[^/]+>/title""")
)
                      

// @LINE:539
def updateCollectionDescription(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).updateCollectionDescription(coll_id), HandlerDef(this, "api.Collections", "updateCollectionDescription", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/collections/$coll_id<[^/]+>/description""")
)
                      

// @LINE:488
def list(title:Option[String], date:Option[String], limit:Int, exact:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).list(title, date, limit, exact), HandlerDef(this, "api.Collections", "list", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]), "GET", """ ----------------------------------------------------------------------
 COLLECTIONS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/collections""")
)
                      

// @LINE:541
def removeSubCollection(coll_id:UUID, sub_coll_id:UUID, ignoreNotFound:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeSubCollection(coll_id, sub_coll_id, ignoreNotFound), HandlerDef(this, "api.Collections", "removeSubCollection", Seq(classOf[UUID], classOf[UUID], classOf[String]), "POST", """""", _prefix + """api/collections/$coll_id<[^/]+>/removeSubCollection/$sub_coll_id<[^/]+>""")
)
                      

// @LINE:543
def unsetRootSpace(coll_id:UUID, spaceId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).unsetRootSpace(coll_id, spaceId), HandlerDef(this, "api.Collections", "unsetRootSpace", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/collections/$coll_id<[^/]+>/unsetRootFlag/$spaceId<[^/]+>""")
)
                      

// @LINE:526
def removeDataset(coll_id:UUID, ds_id:UUID, ignoreNotFound:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeDataset(coll_id, ds_id, ignoreNotFound), HandlerDef(this, "api.Collections", "removeDataset", Seq(classOf[UUID], classOf[UUID], classOf[String]), "POST", """ deprecrated use DELETE""", _prefix + """api/collections/$coll_id<[^/]+>/datasetsRemove/$ds_id<[^/]+>/$ignoreNotFound<[^/]+>""")
)
                      

// @LINE:536
def attachPreview(c_id:UUID, p_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).attachPreview(c_id, p_id), HandlerDef(this, "api.Collections", "attachPreview", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/collections/$c_id<[^/]+>/previews/$p_id<[^/]+>""")
)
                      

// @LINE:517
def removeCollectionAndContents(collectionId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeCollectionAndContents(collectionId), HandlerDef(this, "api.Collections", "removeCollectionAndContents", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/collections/$collectionId<[^/]+>/deleteCollectionAndContents""")
)
                      

// @LINE:546
def getChildCollections(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getChildCollections(coll_id), HandlerDef(this, "api.Collections", "getChildCollections", Seq(classOf[UUID]), "GET", """""", _prefix + """api/collections/$coll_id<[^/]+>/getChildCollections""")
)
                      

// @LINE:500
def restoreCollection(collectionId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).restoreCollection(collectionId), HandlerDef(this, "api.Collections", "restoreCollection", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/collections/restore/$collectionId<[^/]+>""")
)
                      

// @LINE:551
def changeOwner(coll_id:UUID, userId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).changeOwner(coll_id, userId), HandlerDef(this, "api.Collections", "changeOwner", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/collections/$coll_id<[^/]+>/changeOwner/$userId<[^/]+>""")
)
                      

// @LINE:529
def removeCollection(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeCollection(coll_id), HandlerDef(this, "api.Collections", "removeCollection", Seq(classOf[UUID]), "POST", """ deprecrated use DELETE""", _prefix + """api/collections/$coll_id<[^/]+>/remove""")
)
                      

// @LINE:495
def getAllCollections(limit:Int, showAll:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getAllCollections(limit, showAll), HandlerDef(this, "api.Collections", "getAllCollections", Seq(classOf[Int], classOf[Boolean]), "GET", """""", _prefix + """api/collections/allCollections""")
)
                      

// @LINE:494
def getTopLevelCollections(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getTopLevelCollections(), HandlerDef(this, "api.Collections", "getTopLevelCollections", Seq(), "GET", """""", _prefix + """api/collections/topLevelCollections""")
)
                      

// @LINE:489
def listCanEdit(title:Option[String], date:Option[String], limit:Int, exact:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).listCanEdit(title, date, limit, exact), HandlerDef(this, "api.Collections", "listCanEdit", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]), "GET", """""", _prefix + """api/collections/canEdit""")
)
                      

// @LINE:537
def getCollection(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getCollection(coll_id), HandlerDef(this, "api.Collections", "getCollection", Seq(classOf[UUID]), "GET", """""", _prefix + """api/collections/$coll_id<[^/]+>""")
)
                      

// @LINE:542
def setRootSpace(coll_id:UUID, spaceId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).setRootSpace(coll_id, spaceId), HandlerDef(this, "api.Collections", "setRootSpace", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/collections/$coll_id<[^/]+>/rootFlag/$spaceId<[^/]+>""")
)
                      

// @LINE:496
def download(id:UUID, compression:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).download(id, compression), HandlerDef(this, "api.Collections", "download", Seq(classOf[UUID], classOf[Int]), "GET", """""", _prefix + """api/collections/$id<[^/]+>/download""")
)
                      

// @LINE:493
def getRootCollections(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getRootCollections(), HandlerDef(this, "api.Collections", "getRootCollections", Seq(), "GET", """""", _prefix + """api/collections/rootCollections""")
)
                      

// @LINE:554
def changeOwnerFileOnly(file_id:UUID, userId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).changeOwnerFileOnly(file_id, userId), HandlerDef(this, "api.Collections", "changeOwnerFileOnly", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/collections/$file_id<[^/]+>/changeOwnerFile/$userId<[^/]+>""")
)
                      

// @LINE:524
def attachDataset(coll_id:UUID, ds_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).attachDataset(coll_id, ds_id), HandlerDef(this, "api.Collections", "attachDataset", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/collections/$coll_id<[^/]+>/datasets/$ds_id<[^/]+>""")
)
                      

// @LINE:490
def addDatasetToCollectionOptions(ds_id:UUID, title:Option[String], date:Option[String], limit:Int, exact:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).addDatasetToCollectionOptions(ds_id, title, date, limit, exact), HandlerDef(this, "api.Collections", "addDatasetToCollectionOptions", Seq(classOf[UUID], classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]), "GET", """""", _prefix + """api/collections/datasetPossibleParents/$ds_id<[^/]+>""")
)
                      

// @LINE:552
def changeOwnerCollectionOnly(coll_id:UUID, userId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).changeOwnerCollectionOnly(coll_id, userId), HandlerDef(this, "api.Collections", "changeOwnerCollectionOnly", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/collections/$coll_id<[^/]+>/changeOwnerCollection/$userId<[^/]+>""")
)
                      

// @LINE:535
def deleteCollectionAndContents(coll_id:UUID, userid:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).deleteCollectionAndContents(coll_id, userid), HandlerDef(this, "api.Collections", "deleteCollectionAndContents", Seq(classOf[UUID], classOf[UUID]), "DELETE", """""", _prefix + """api/collections/$coll_id<[^/]+>/deleteContents/$userid<[^/]+>""")
)
                      

// @LINE:547
def getParentCollectionIds(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getParentCollectionIds(coll_id), HandlerDef(this, "api.Collections", "getParentCollectionIds", Seq(classOf[UUID]), "GET", """""", _prefix + """api/collections/$coll_id<[^/]+>/getParentCollectionIds""")
)
                      

// @LINE:498
def emptyTrash(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).emptyTrash(), HandlerDef(this, "api.Collections", "emptyTrash", Seq(), "DELETE", """""", _prefix + """api/collections/emptyTrash""")
)
                      

// @LINE:522
def unfollow(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).unfollow(id), HandlerDef(this, "api.Collections", "unfollow", Seq(classOf[UUID]), "POST", """""", _prefix + """api/collections/$id<[^/]+>/unfollow""")
)
                      

// @LINE:521
def follow(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).follow(id), HandlerDef(this, "api.Collections", "follow", Seq(classOf[UUID]), "POST", """""", _prefix + """api/collections/$id<[^/]+>/follow""")
)
                      
    
}
                          

// @LINE:960
// @LINE:631
// @LINE:630
// @LINE:629
// @LINE:628
// @LINE:627
// @LINE:626
// @LINE:624
// @LINE:623
// @LINE:622
// @LINE:621
// @LINE:620
// @LINE:619
// @LINE:618
// @LINE:617
// @LINE:616
// @LINE:615
// @LINE:614
// @LINE:613
// @LINE:612
// @LINE:611
// @LINE:610
// @LINE:609
// @LINE:608
// @LINE:607
// @LINE:606
// @LINE:605
// @LINE:604
// @LINE:603
// @LINE:602
// @LINE:601
// @LINE:600
// @LINE:599
// @LINE:598
// @LINE:597
// @LINE:596
// @LINE:595
// @LINE:594
// @LINE:593
// @LINE:592
// @LINE:591
// @LINE:590
// @LINE:589
// @LINE:588
// @LINE:587
// @LINE:585
// @LINE:584
// @LINE:580
// @LINE:578
// @LINE:577
// @LINE:576
// @LINE:575
// @LINE:574
// @LINE:573
// @LINE:572
// @LINE:571
// @LINE:570
// @LINE:569
// @LINE:568
// @LINE:566
// @LINE:565
// @LINE:564
// @LINE:563
// @LINE:562
// @LINE:561
// @LINE:560
// @LINE:532
// @LINE:523
// @LINE:448
// @LINE:447
// @LINE:319
// @LINE:318
// @LINE:317
class ReverseDatasets {
    

// @LINE:624
def detachAndDeleteDataset(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).detachAndDeleteDataset(id), HandlerDef(this, "api.Datasets", "detachAndDeleteDataset", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/detachdelete""")
)
                      

// @LINE:588
def getRDFURLsForDataset(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getRDFURLsForDataset(id), HandlerDef(this, "api.Datasets", "getRDFURLsForDataset", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/getRDFURLsForDataset/$id<[^/]+>""")
)
                      

// @LINE:589
def getRDFUserMetadata(id:UUID, mappingNum:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getRDFUserMetadata(id, mappingNum), HandlerDef(this, "api.Datasets", "getRDFUserMetadata", Seq(classOf[UUID], classOf[String]), "GET", """""", _prefix + """api/datasets/rdfUserMetadata/$id<[^/]+>""")
)
                      

// @LINE:574
def listDatasetsInTrash(limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listDatasetsInTrash(limit), HandlerDef(this, "api.Datasets", "listDatasetsInTrash", Seq(classOf[Int]), "GET", """""", _prefix + """api/datasets/listTrash""")
)
                      

// @LINE:631
def hasAttachedVocabulary(datasetId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).hasAttachedVocabulary(datasetId), HandlerDef(this, "api.Datasets", "hasAttachedVocabulary", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/hasVocabulary/$datasetId<[^/]+>""")
)
                      

// @LINE:620
def getPreviews(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getPreviews(id), HandlerDef(this, "api.Datasets", "getPreviews", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/getPreviews""")
)
                      

// @LINE:603
def reindex(id:UUID, recursive:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).reindex(id, recursive), HandlerDef(this, "api.Datasets", "reindex", Seq(classOf[UUID], classOf[Boolean]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/reindex""")
)
                      

// @LINE:572
def searchDatasetsGeneralMetadata(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).searchDatasetsGeneralMetadata(), HandlerDef(this, "api.Datasets", "searchDatasetsGeneralMetadata", Seq(), "POST", """""", _prefix + """api/datasets/searchmetadata""")
)
                      

// @LINE:606
def removeTag(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeTag(id), HandlerDef(this, "api.Datasets", "removeTag", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/removeTag""")
)
                      

// @LINE:618
def updateLicense(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).updateLicense(id), HandlerDef(this, "api.Datasets", "updateLicense", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/license""")
)
                      

// @LINE:562
def listMoveFileToDataset(file_id:UUID, title:Option[String], limit:Int, exact:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listMoveFileToDataset(file_id, title, limit, exact), HandlerDef(this, "api.Datasets", "listMoveFileToDataset", Seq(classOf[UUID], classOf[Option[String]], classOf[Int], classOf[Boolean]), "GET", """""", _prefix + """api/datasets/moveFileToDataset""")
)
                      

// @LINE:621
def attachExistingFile(ds_id:UUID, file_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).attachExistingFile(ds_id, file_id), HandlerDef(this, "api.Datasets", "attachExistingFile", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/datasets/$ds_id<[^/]+>/files/$file_id<[^/]+>""")
)
                      

// @LINE:597
def datasetAllFilesList(id:UUID, max:Option[Int]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).datasetAllFilesList(id, max), HandlerDef(this, "api.Datasets", "datasetAllFilesList", Seq(classOf[UUID], classOf[Option[Int]]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/listAllFiles""")
)
                      

// @LINE:319
def removeMetadataJsonLD(id:UUID, extractor:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeMetadataJsonLD(id, extractor), HandlerDef(this, "api.Datasets", "removeMetadataJsonLD", Seq(classOf[UUID], classOf[Option[String]]), "DELETE", """""", _prefix + """api/datasets/$id<[^/]+>/metadata.jsonld""")
)
                      

// @LINE:578
def detachFile(ds_id:UUID, file_id:UUID, ignoreNotFound:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).detachFile(ds_id, file_id, ignoreNotFound), HandlerDef(this, "api.Datasets", "detachFile", Seq(classOf[UUID], classOf[UUID], classOf[String]), "POST", """""", _prefix + """api/datasets/$ds_id<[^/]+>/filesRemove/$file_id<[^/]+>/$ignoreNotFound<[^/]+>""")
)
                      

// @LINE:627
def addFileEvent(id:UUID, inFolder:Boolean, fileCount:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addFileEvent(id, inFolder, fileCount), HandlerDef(this, "api.Datasets", "addFileEvent", Seq(classOf[UUID], classOf[Boolean], classOf[Int]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/addFileEvent""")
)
                      

// @LINE:570
def attachMultipleFiles(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).attachMultipleFiles(), HandlerDef(this, "api.Datasets", "attachMultipleFiles", Seq(), "POST", """""", _prefix + """api/datasets/attachmultiple""")
)
                      

// @LINE:616
def moveCreator(id:UUID, creator:String, newPos:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).moveCreator(id, creator, newPos), HandlerDef(this, "api.Datasets", "moveCreator", Seq(classOf[UUID], classOf[String], classOf[Int]), "PUT", """""", _prefix + """api/datasets/$id<[^/]+>/creator/reorder""")
)
                      

// @LINE:560
def list(title:Option[String], date:Option[String], limit:Int, exact:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).list(title, date, limit, exact), HandlerDef(this, "api.Datasets", "list", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]), "GET", """ ----------------------------------------------------------------------
 DATASETS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/datasets""")
)
                      

// @LINE:448
def dumpDatasetGroupings(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).dumpDatasetGroupings(), HandlerDef(this, "api.Datasets", "dumpDatasetGroupings", Seq(), "POST", """""", _prefix + """api/dumpDatasetGroupings""")
)
                      

// @LINE:619
def isBeingProcessed(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).isBeingProcessed(id), HandlerDef(this, "api.Datasets", "isBeingProcessed", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/isBeingProcessed""")
)
                      

// @LINE:590
def getMetadataDefinitions(id:UUID, space:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getMetadataDefinitions(id, space), HandlerDef(this, "api.Datasets", "getMetadataDefinitions", Seq(classOf[UUID], classOf[Option[String]]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/metadata""")
)
                      

// @LINE:523
def listInCollection(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listInCollection(coll_id), HandlerDef(this, "api.Datasets", "listInCollection", Seq(classOf[UUID]), "GET", """""", _prefix + """api/collections/$coll_id<[^/]+>/datasets""")
)
                      

// @LINE:614
def addCreator(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addCreator(id), HandlerDef(this, "api.Datasets", "addCreator", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/creator""")
)
                      

// @LINE:566
def restoreDataset(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).restoreDataset(id), HandlerDef(this, "api.Datasets", "restoreDataset", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/datasets/restore/$id<[^/]+>""")
)
                      

// @LINE:580
def moveFileBetweenDatasets(datasetId:UUID, toDataset:UUID, fileId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).moveFileBetweenDatasets(datasetId, toDataset, fileId), HandlerDef(this, "api.Datasets", "moveFileBetweenDatasets", Seq(classOf[UUID], classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/datasets/$datasetId<[^/]+>/moveBetweenDatasets/$toDataset<[^/]+>/$fileId<[^/]+>""")
)
                      

// @LINE:628
def users(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).users(id), HandlerDef(this, "api.Datasets", "users", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/users""")
)
                      

// @LINE:609
def removeTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeTags(id), HandlerDef(this, "api.Datasets", "removeTags", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/tags/remove""")
)
                      

// @LINE:564
def createEmptyVocabularyForDataset(datasetId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).createEmptyVocabularyForDataset(datasetId), HandlerDef(this, "api.Datasets", "createEmptyVocabularyForDataset", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/newVocabulary/$datasetId<[^/]+>""")
)
                      

// @LINE:591
def addMetadata(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addMetadata(id), HandlerDef(this, "api.Datasets", "addMetadata", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/metadata""")
)
                      

// @LINE:599
def uploadToDatasetFile(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).uploadToDatasetFile(id), HandlerDef(this, "api.Datasets", "uploadToDatasetFile", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/files""")
)
                      

// @LINE:610
def removeAllTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeAllTags(id), HandlerDef(this, "api.Datasets", "removeAllTags", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/tags/remove_all""")
)
                      

// @LINE:585
def copyDatasetToSpaceNotAuthor(datasetId:UUID, spaceId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).copyDatasetToSpaceNotAuthor(datasetId, spaceId), HandlerDef(this, "api.Datasets", "copyDatasetToSpaceNotAuthor", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/datasets/$datasetId<[^/]+>/copyDatasetToSpaceNotAuthor/$spaceId<[^/]+>""")
)
                      

// @LINE:584
def copyDatasetToSpace(datasetId:UUID, spaceId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).copyDatasetToSpace(datasetId, spaceId), HandlerDef(this, "api.Datasets", "copyDatasetToSpace", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/datasets/$datasetId<[^/]+>/copyDatasetToSpace/$spaceId<[^/]+>""")
)
                      

// @LINE:622
def get(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).get(id), HandlerDef(this, "api.Datasets", "get", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>""")
)
                      

// @LINE:563
def createDataset(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).createDataset(), HandlerDef(this, "api.Datasets", "createDataset", Seq(), "POST", """""", _prefix + """api/datasets""")
)
                      

// @LINE:629
def changeOwner(datasetId:UUID, userId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).changeOwner(datasetId, userId), HandlerDef(this, "api.Datasets", "changeOwner", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/datasets/$datasetId<[^/]+>/changeOwner/$userId<[^/]+>""")
)
                      

// @LINE:571
def searchDatasetsUserMetadata(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).searchDatasetsUserMetadata(), HandlerDef(this, "api.Datasets", "searchDatasetsUserMetadata", Seq(), "POST", """""", _prefix + """api/datasets/searchusermetadata""")
)
                      

// @LINE:608
def addTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addTags(id), HandlerDef(this, "api.Datasets", "addTags", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/tags""")
)
                      

// @LINE:615
def removeCreator(id:UUID, creator:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeCreator(id, creator), HandlerDef(this, "api.Datasets", "removeCreator", Seq(classOf[UUID], classOf[String]), "DELETE", """""", _prefix + """api/datasets/$id<[^/]+>/creator/remove""")
)
                      

// @LINE:592
def addUserMetadata(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addUserMetadata(id), HandlerDef(this, "api.Datasets", "addUserMetadata", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/usermetadata""")
)
                      

// @LINE:561
def listCanEdit(title:Option[String], date:Option[String], limit:Int, exact:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listCanEdit(title, date, limit, exact), HandlerDef(this, "api.Datasets", "listCanEdit", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]), "GET", """""", _prefix + """api/datasets/canEdit""")
)
                      

// @LINE:569
def createEmptyDataset(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).createEmptyDataset(), HandlerDef(this, "api.Datasets", "createEmptyDataset", Seq(), "POST", """""", _prefix + """api/datasets/createempty""")
)
                      

// @LINE:607
def getTags(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getTags(id), HandlerDef(this, "api.Datasets", "getTags", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/tags""")
)
                      

// @LINE:612
def updateName(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).updateName(id), HandlerDef(this, "api.Datasets", "updateName", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/datasets/$id<[^/]+>/title""")
)
                      

// @LINE:602
def comment(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).comment(id), HandlerDef(this, "api.Datasets", "comment", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/comment""")
)
                      

// @LINE:601
def download(id:UUID, compression:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).download(id, compression), HandlerDef(this, "api.Datasets", "download", Seq(classOf[UUID], classOf[Int]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/download""")
)
                      

// @LINE:600
def uploadToDatasetJSON(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).uploadToDatasetJSON(id), HandlerDef(this, "api.Datasets", "uploadToDatasetJSON", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/urls""")
)
                      

// @LINE:565
def markDatasetAsTrash(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).markDatasetAsTrash(id), HandlerDef(this, "api.Datasets", "markDatasetAsTrash", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/datasets/trash/$id<[^/]+>""")
)
                      

// @LINE:596
def datasetFilesList(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).datasetFilesList(id), HandlerDef(this, "api.Datasets", "datasetFilesList", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/listFiles""")
)
                      

// @LINE:317
def addMetadataJsonLD(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addMetadataJsonLD(id), HandlerDef(this, "api.Datasets", "addMetadataJsonLD", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/metadata.jsonld""")
)
                      

// @LINE:626
def updateAccess(id:UUID, aceess:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).updateAccess(id, aceess), HandlerDef(this, "api.Datasets", "updateAccess", Seq(classOf[UUID], classOf[String]), "PUT", """""", _prefix + """api/datasets/$id<[^/]+>/access""")
)
                      

// @LINE:594
def getXMLMetadataJSON(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getXMLMetadataJSON(id), HandlerDef(this, "api.Datasets", "getXMLMetadataJSON", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/xmlmetadatajson""")
)
                      

// @LINE:613
def updateDescription(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).updateDescription(id), HandlerDef(this, "api.Datasets", "updateDescription", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/datasets/$id<[^/]+>/description""")
)
                      

// @LINE:617
def updateInformation(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).updateInformation(id), HandlerDef(this, "api.Datasets", "updateInformation", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/editing""")
)
                      

// @LINE:623
def deleteDataset(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).deleteDataset(id), HandlerDef(this, "api.Datasets", "deleteDataset", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/datasets/$id<[^/]+>""")
)
                      

// @LINE:568
def emptyTrash(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).emptyTrash(), HandlerDef(this, "api.Datasets", "emptyTrash", Seq(), "DELETE", """GET            /api/datasets/trash                                                      @api.Datasets.listDatasetsInTrash()""", _prefix + """api/datasets/emptytrash""")
)
                      

// @LINE:605
def unfollow(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).unfollow(id), HandlerDef(this, "api.Datasets", "unfollow", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/unfollow""")
)
                      

// @LINE:318
def getMetadataJsonLD(id:UUID, extractor:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getMetadataJsonLD(id, extractor), HandlerDef(this, "api.Datasets", "getMetadataJsonLD", Seq(classOf[UUID], classOf[Option[String]]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/metadata.jsonld""")
)
                      

// @LINE:604
def follow(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).follow(id), HandlerDef(this, "api.Datasets", "follow", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/follow""")
)
                      

// @LINE:593
def getTechnicalMetadataJSON(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getTechnicalMetadataJSON(id), HandlerDef(this, "api.Datasets", "getTechnicalMetadataJSON", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/technicalmetadatajson""")
)
                      

// @LINE:447
def dumpDatasetsMetadata(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).dumpDatasetsMetadata(), HandlerDef(this, "api.Datasets", "dumpDatasetsMetadata", Seq(), "POST", """""", _prefix + """api/dumpDatasetsMd""")
)
                      

// @LINE:630
def moveDataset(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).moveDataset(id), HandlerDef(this, "api.Datasets", "moveDataset", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$id<[^/]+>/moveCollection""")
)
                      

// @LINE:576
def clearOldDatasetsTrash(days:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).clearOldDatasetsTrash(days), HandlerDef(this, "api.Datasets", "clearOldDatasetsTrash", Seq(classOf[Int]), "DELETE", """""", _prefix + """api/datasets/clearOldDatasetsTrash""")
)
                      

// @LINE:573
def listOutsideCollection(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listOutsideCollection(coll_id), HandlerDef(this, "api.Datasets", "listOutsideCollection", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/listOutsideCollection/$coll_id<[^/]+>""")
)
                      

// @LINE:595
def getUserMetadataJSON(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getUserMetadataJSON(id), HandlerDef(this, "api.Datasets", "getUserMetadataJSON", Seq(classOf[UUID]), "GET", """""", _prefix + """api/datasets/$id<[^/]+>/usermetadatajson""")
)
                      
    
}
                          

// @LINE:751
// @LINE:750
// @LINE:749
// @LINE:748
// @LINE:747
// @LINE:746
// @LINE:745
class ReverseSelected {
    

// @LINE:750
def clearAll(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).clearAll(), HandlerDef(this, "api.Selected", "clearAll", Seq(), "POST", """""", _prefix + """api/selected/clear""")
)
                      

// @LINE:749
def downloadAll(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).downloadAll(), HandlerDef(this, "api.Selected", "downloadAll", Seq(), "GET", """""", _prefix + """api/selected/files""")
)
                      

// @LINE:747
def remove(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).remove(), HandlerDef(this, "api.Selected", "remove", Seq(), "POST", """""", _prefix + """api/selected/remove""")
)
                      

// @LINE:748
def deleteAll(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).deleteAll(), HandlerDef(this, "api.Selected", "deleteAll", Seq(), "DELETE", """""", _prefix + """api/selected/files""")
)
                      

// @LINE:746
def add(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).add(), HandlerDef(this, "api.Selected", "add", Seq(), "POST", """""", _prefix + """api/selected""")
)
                      

// @LINE:745
def get(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).get(), HandlerDef(this, "api.Selected", "get", Seq(), "GET", """""", _prefix + """api/selected""")
)
                      

// @LINE:751
def tagAll(tags:List[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).tagAll(tags), HandlerDef(this, "api.Selected", "tagAll", Seq(classOf[List[String]]), "POST", """""", _prefix + """api/selected/tag""")
)
                      
    
}
                          

// @LINE:426
// @LINE:425
// @LINE:424
// @LINE:423
// @LINE:422
// @LINE:421
// @LINE:420
// @LINE:418
// @LINE:417
// @LINE:416
// @LINE:415
// @LINE:413
// @LINE:412
// @LINE:410
// @LINE:408
// @LINE:407
class ReverseExtractions {
    

// @LINE:410
def addExtractorInfo(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).addExtractorInfo(), HandlerDef(this, "api.Extractions", "addExtractorInfo", Seq(), "POST", """GET            /api/extractors/:id                                                      @api.Extractions.getExtractorInfo(id: UUID)""", _prefix + """api/extractors""")
)
                      

// @LINE:416
def getExtractorServersIP(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getExtractorServersIP(), HandlerDef(this, "api.Extractions", "getExtractorServersIP", Seq(), "GET", """""", _prefix + """api/extractions/servers_ips""")
)
                      

// @LINE:415
def getDTSRequests(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getDTSRequests(), HandlerDef(this, "api.Extractions", "getDTSRequests", Seq(), "GET", """""", _prefix + """api/extractions/requests""")
)
                      

// @LINE:422
def multipleUploadByURL(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).multipleUploadByURL(), HandlerDef(this, "api.Extractions", "multipleUploadByURL", Seq(), "POST", """""", _prefix + """api/extractions/multiple_uploadby_url""")
)
                      

// @LINE:425
def fetch(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).fetch(id), HandlerDef(this, "api.Extractions", "fetch", Seq(classOf[UUID]), "GET", """""", _prefix + """api/extractions/$id<[^/]+>/metadata""")
)
                      

// @LINE:423
def uploadExtract(showPreviews:String, extract:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).uploadExtract(showPreviews, extract), HandlerDef(this, "api.Extractions", "uploadExtract", Seq(classOf[String], classOf[Boolean]), "POST", """""", _prefix + """api/extractions/upload_file""")
)
                      

// @LINE:408
def getExtractorInfo(name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getExtractorInfo(name), HandlerDef(this, "api.Extractions", "getExtractorInfo", Seq(classOf[String]), "GET", """""", _prefix + """api/extractors/$name<[^/]+>""")
)
                      

// @LINE:412
def submitFileToExtractor(file_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).submitFileToExtractor(file_id), HandlerDef(this, "api.Extractions", "submitFileToExtractor", Seq(classOf[UUID]), "POST", """""", _prefix + """api/files/$file_id<[^/]+>/extractions""")
)
                      

// @LINE:424
def checkExtractorsStatus(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).checkExtractorsStatus(id), HandlerDef(this, "api.Extractions", "checkExtractorsStatus", Seq(classOf[UUID]), "GET", """""", _prefix + """api/extractions/$id<[^/]+>/status""")
)
                      

// @LINE:413
def submitDatasetToExtractor(ds_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).submitDatasetToExtractor(ds_id), HandlerDef(this, "api.Extractions", "submitDatasetToExtractor", Seq(classOf[UUID]), "POST", """""", _prefix + """api/datasets/$ds_id<[^/]+>/extractions""")
)
                      

// @LINE:418
def getExtractorInputTypes(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getExtractorInputTypes(), HandlerDef(this, "api.Extractions", "getExtractorInputTypes", Seq(), "GET", """""", _prefix + """api/extractions/supported_input_types""")
)
                      

// @LINE:407
def listExtractors(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).listExtractors(), HandlerDef(this, "api.Extractions", "listExtractors", Seq(), "GET", """ ----------------------------------------------------------------------
 EXTRACTORS ENDPOINTS
 ----------------------------------------------------------------------""", _prefix + """api/extractors""")
)
                      

// @LINE:420
def getExtractorDetails(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getExtractorDetails(), HandlerDef(this, "api.Extractions", "getExtractorDetails", Seq(), "GET", """API for temporary fix for BD-289""", _prefix + """api/extractions/extractors_details""")
)
                      

// @LINE:421
def uploadByURL(extract:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).uploadByURL(extract), HandlerDef(this, "api.Extractions", "uploadByURL", Seq(classOf[Boolean]), "POST", """""", _prefix + """api/extractions/upload_url""")
)
                      

// @LINE:426
def checkExtractionsStatuses(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).checkExtractionsStatuses(id), HandlerDef(this, "api.Extractions", "checkExtractionsStatuses", Seq(classOf[UUID]), "GET", """""", _prefix + """api/extractions/$id<[^/]+>/statuses""")
)
                      

// @LINE:417
def getExtractorNames(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getExtractorNames(), HandlerDef(this, "api.Extractions", "getExtractorNames", Seq(), "GET", """""", _prefix + """api/extractions/extractors_names""")
)
                      
    
}
                          

// @LINE:840
class ReverseEvents {
    

// @LINE:840
def sendExceptionEmail(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Events]).sendExceptionEmail(), HandlerDef(this, "api.Events", "sendExceptionEmail", Seq(), "POST", """""", _prefix + """sendEmail""")
)
                      
    
}
                          

// @LINE:715
// @LINE:714
// @LINE:713
// @LINE:712
// @LINE:711
// @LINE:710
// @LINE:709
// @LINE:708
// @LINE:707
// @LINE:706
// @LINE:705
// @LINE:704
// @LINE:703
// @LINE:702
// @LINE:701
// @LINE:700
// @LINE:699
// @LINE:698
// @LINE:697
// @LINE:696
// @LINE:695
// @LINE:694
// @LINE:693
// @LINE:692
// @LINE:691
// @LINE:690
// @LINE:689
// @LINE:688
// @LINE:687
// @LINE:686
class ReverseGeostreams {
    

// @LINE:708
def getStream(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.getStream(id), HandlerDef(this, "api.Geostreams", "getStream", Seq(classOf[String]), "GET", """""", _prefix + """api/geostreams/streams/$id<[^/]+>""")
)
                      

// @LINE:692
def binDatapoints(time:String, depth:Double, raw:Boolean, since:Option[String], until:Option[String], geocode:Option[String], stream_id:Option[String], sensor_id:Option[String], sources:List[String], attributes:List[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.binDatapoints(time, depth, raw, since, until, geocode, stream_id, sensor_id, sources, attributes), HandlerDef(this, "api.Geostreams", "binDatapoints", Seq(classOf[String], classOf[Double], classOf[Boolean], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[List[String]], classOf[List[String]]), "GET", """""", _prefix + """api/geostreams/datapoints/bin/$time<[^/]+>/$depth<[^/]+>""")
)
                      

// @LINE:698
def updateStatisticsStreamSensor(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.updateStatisticsStreamSensor(), HandlerDef(this, "api.Geostreams", "updateStatisticsStreamSensor", Seq(), "GET", """""", _prefix + """api/geostreams/sensors/update""")
)
                      

// @LINE:694
def cacheListAction(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.cacheListAction(), HandlerDef(this, "api.Geostreams", "cacheListAction", Seq(), "GET", """""", _prefix + """api/geostreams/cache""")
)
                      

// @LINE:686
def addDatapoint(invalidateCache:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.addDatapoint(invalidateCache), HandlerDef(this, "api.Geostreams", "addDatapoint", Seq(classOf[Boolean]), "POST", """ ----------------------------------------------------------------------
 GEOSTREAMS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/geostreams/datapoints""")
)
                      

// @LINE:702
def getSensorStreams(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.getSensorStreams(id), HandlerDef(this, "api.Geostreams", "getSensorStreams", Seq(classOf[String]), "GET", """""", _prefix + """api/geostreams/sensors/$id<[^/]+>/streams""")
)
                      

// @LINE:704
def searchSensors(geocode:Option[String], sensor_name:Option[String], geojson:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.searchSensors(geocode, sensor_name, geojson), HandlerDef(this, "api.Geostreams", "searchSensors", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]), "GET", """""", _prefix + """api/geostreams/sensors""")
)
                      

// @LINE:701
def getSensorStatistics(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.getSensorStatistics(id), HandlerDef(this, "api.Geostreams", "getSensorStatistics", Seq(classOf[String]), "GET", """""", _prefix + """api/geostreams/sensors/$id<[^/]+>/stats""")
)
                      

// @LINE:706
def createStream(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.createStream(), HandlerDef(this, "api.Geostreams", "createStream", Seq(), "POST", """""", _prefix + """api/geostreams/streams""")
)
                      

// @LINE:687
def addDatapoints(invalidateCache:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.addDatapoints(invalidateCache), HandlerDef(this, "api.Geostreams", "addDatapoints", Seq(classOf[Boolean]), "POST", """""", _prefix + """api/geostreams/datapoints/bulk""")
)
                      

// @LINE:696
def cacheFetchAction(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.cacheFetchAction(id), HandlerDef(this, "api.Geostreams", "cacheFetchAction", Seq(classOf[String]), "GET", """""", _prefix + """api/geostreams/cache/$id<[^/]+>""")
)
                      

// @LINE:715
def getConfig(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.getConfig(), HandlerDef(this, "api.Geostreams", "getConfig", Seq(), "GET", """""", _prefix + """api/geostreams/config""")
)
                      

// @LINE:689
def searchDatapoints(operator:String, since:Option[String], until:Option[String], geocode:Option[String], stream_id:Option[String], sensor_id:Option[String], sources:List[String], attributes:List[String], format:String, semi:Option[String], onlyCount:Boolean, window_start:Option[String], window_end:Option[String], binning:String, geojson:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.searchDatapoints(operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson), HandlerDef(this, "api.Geostreams", "searchDatapoints", Seq(classOf[String], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[List[String]], classOf[List[String]], classOf[String], classOf[Option[String]], classOf[Boolean], classOf[Option[String]], classOf[Option[String]], classOf[String], classOf[Option[String]]), "GET", """""", _prefix + """api/geostreams/datapoints""")
)
                      

// @LINE:714
def counts(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.counts(), HandlerDef(this, "api.Geostreams", "counts", Seq(), "GET", """""", _prefix + """api/geostreams/counts""")
)
                      

// @LINE:709
def patchStreamMetadata(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.patchStreamMetadata(id), HandlerDef(this, "api.Geostreams", "patchStreamMetadata", Seq(classOf[String]), "PUT", """""", _prefix + """api/geostreams/streams/$id<[^/]+>""")
)
                      

// @LINE:697
def createSensor(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.createSensor(), HandlerDef(this, "api.Geostreams", "createSensor", Seq(), "POST", """""", _prefix + """api/geostreams/sensors""")
)
                      

// @LINE:703
def updateStatisticsSensor(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.updateStatisticsSensor(id), HandlerDef(this, "api.Geostreams", "updateStatisticsSensor", Seq(classOf[String]), "GET", """""", _prefix + """api/geostreams/sensors/$id<[^/]+>/update""")
)
                      

// @LINE:713
def deleteAll(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.deleteAll(), HandlerDef(this, "api.Geostreams", "deleteAll", Seq(), "DELETE", """""", _prefix + """api/geostreams/dropall""")
)
                      

// @LINE:688
def deleteDatapoint(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.deleteDatapoint(id), HandlerDef(this, "api.Geostreams", "deleteDatapoint", Seq(classOf[String]), "DELETE", """""", _prefix + """api/geostreams/datapoints/$id<[^/]+>""")
)
                      

// @LINE:699
def getSensor(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.getSensor(id), HandlerDef(this, "api.Geostreams", "getSensor", Seq(classOf[String]), "GET", """""", _prefix + """api/geostreams/sensors/$id<[^/]+>""")
)
                      

// @LINE:711
def searchStreams(geocode:Option[String], stream_name:Option[String], geojson:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.searchStreams(geocode, stream_name, geojson), HandlerDef(this, "api.Geostreams", "searchStreams", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]), "GET", """""", _prefix + """api/geostreams/streams""")
)
                      

// @LINE:705
def deleteSensor(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.deleteSensor(id), HandlerDef(this, "api.Geostreams", "deleteSensor", Seq(classOf[String]), "DELETE", """""", _prefix + """api/geostreams/sensors/$id<[^/]+>""")
)
                      

// @LINE:712
def deleteStream(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.deleteStream(id), HandlerDef(this, "api.Geostreams", "deleteStream", Seq(classOf[String]), "DELETE", """""", _prefix + """api/geostreams/streams/$id<[^/]+>""")
)
                      

// @LINE:700
def updateSensorMetadata(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.updateSensorMetadata(id), HandlerDef(this, "api.Geostreams", "updateSensorMetadata", Seq(classOf[String]), "PUT", """""", _prefix + """api/geostreams/sensors/$id<[^/]+>""")
)
                      

// @LINE:695
def cacheInvalidateAction(sensor_id:Option[String], stream_id:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.cacheInvalidateAction(sensor_id, stream_id), HandlerDef(this, "api.Geostreams", "cacheInvalidateAction", Seq(classOf[Option[String]], classOf[Option[String]]), "GET", """""", _prefix + """api/geostreams/cache/invalidate""")
)
                      

// @LINE:710
def updateStatisticsStream(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.updateStatisticsStream(id), HandlerDef(this, "api.Geostreams", "updateStatisticsStream", Seq(classOf[String]), "GET", """""", _prefix + """api/geostreams/streams/$id<[^/]+>/update""")
)
                      

// @LINE:693
def getDatapoint(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Geostreams.getDatapoint(id), HandlerDef(this, "api.Geostreams", "getDatapoint", Seq(classOf[String]), "GET", """""", _prefix + """api/geostreams/datapoints/$id<[^/]+>""")
)
                      
    
}
                          

// @LINE:978
class ReverseFullTree {
    

// @LINE:978
def getChildrenOfNode(nodeId:Option[String], nodeType:String, role:Option[String]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.FullTree]).getChildrenOfNode(nodeId, nodeType, role), HandlerDef(this, "api.FullTree", "getChildrenOfNode", Seq(classOf[Option[String]], classOf[String], classOf[Option[String]]), "GET", """""", _prefix + """api/fulltree/getChildrenOfNode""")
)
                      
    
}
                          

// @LINE:836
// @LINE:835
// @LINE:833
// @LINE:832
// @LINE:830
// @LINE:829
// @LINE:828
// @LINE:820
// @LINE:790
class ReverseCurationObjects {
    

// @LINE:828
def findMatchmakingRepositories(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).findMatchmakingRepositories(id), HandlerDef(this, "api.CurationObjects", "findMatchmakingRepositories", Seq(classOf[UUID]), "POST", """""", _prefix + """spaces/curations/$id<[^/]+>/matchmaker""")
)
                      

// @LINE:790
def getCurationObjectOre(curationId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).getCurationObjectOre(curationId), HandlerDef(this, "api.CurationObjects", "getCurationObjectOre", Seq(classOf[UUID]), "GET", """""", _prefix + """api/curations/$curationId<[^/]+>/ore""")
)
                      

// @LINE:836
def getMetadataDefinitions(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).getMetadataDefinitions(id), HandlerDef(this, "api.CurationObjects", "getMetadataDefinitions", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/curations/$id<[^/]+>/metadataDefinitions""")
)
                      

// @LINE:832
def deleteCurationFile(id:UUID, parentId:UUID, curationFileId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).deleteCurationFile(id, parentId, curationFileId), HandlerDef(this, "api.CurationObjects", "deleteCurationFile", Seq(classOf[UUID], classOf[UUID], classOf[UUID]), "DELETE", """""", _prefix + """spaces/curations/$id<[^/]+>/files/$curationFileId<[^/]+>""")
)
                      

// @LINE:835
def getMetadataDefinitionsByFile(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).getMetadataDefinitionsByFile(id), HandlerDef(this, "api.CurationObjects", "getMetadataDefinitionsByFile", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/curations/$id<[^/]+>/getMetadataDefinitionsByFile""")
)
                      

// @LINE:820
def retractCurationObject(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).retractCurationObject(id), HandlerDef(this, "api.CurationObjects", "retractCurationObject", Seq(classOf[UUID]), "DELETE", """""", _prefix + """spaces/curations/retract/$id<[^/]+>""")
)
                      

// @LINE:830
def savePublishedObject(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).savePublishedObject(id), HandlerDef(this, "api.CurationObjects", "savePublishedObject", Seq(classOf[UUID]), "POST", """""", _prefix + """spaces/curations/$id<[^/]+>/status""")
)
                      

// @LINE:829
def getCurationFiles(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).getCurationFiles(id), HandlerDef(this, "api.CurationObjects", "getCurationFiles", Seq(classOf[UUID]), "GET", """""", _prefix + """spaces/curations/$id<[^/]+>/curationFile""")
)
                      

// @LINE:833
def deleteCurationFolder(id:UUID, parentId:UUID, curationFolderId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).deleteCurationFolder(id, parentId, curationFolderId), HandlerDef(this, "api.CurationObjects", "deleteCurationFolder", Seq(classOf[UUID], classOf[UUID], classOf[UUID]), "DELETE", """""", _prefix + """spaces/curations/$id<[^/]+>/folders/$curationFolderId<[^/]+>""")
)
                      
    
}
                          

// @LINE:731
// @LINE:730
// @LINE:729
// @LINE:728
// @LINE:727
class ReverseSensors {
    

// @LINE:728
def add(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Sensors.add(), HandlerDef(this, "api.Sensors", "add", Seq(), "POST", """""", _prefix + """api/sensors""")
)
                      

// @LINE:727
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Sensors.list(), HandlerDef(this, "api.Sensors", "list", Seq(), "GET", """ ----------------------------------------------------------------------
 SENSORS ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/sensors""")
)
                      

// @LINE:731
def delete(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Sensors.delete(id), HandlerDef(this, "api.Sensors", "delete", Seq(classOf[String]), "DELETE", """""", _prefix + """api/sensors/$id<[^/]+>""")
)
                      

// @LINE:730
def search(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Sensors.search(), HandlerDef(this, "api.Sensors", "search", Seq(), "GET", """""", _prefix + """api/sensors/search""")
)
                      

// @LINE:729
def get(id:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   api.Sensors.get(id), HandlerDef(this, "api.Sensors", "get", Seq(classOf[String]), "GET", """""", _prefix + """api/sensors/$id<[^/]+>""")
)
                      
    
}
                          

// @LINE:769
// @LINE:768
// @LINE:681
// @LINE:680
// @LINE:679
class ReverseSearch {
    

// @LINE:679
def searchJson(query:String, grouping:String, from:Option[Int], size:Option[Int]): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Search]).searchJson(query, grouping, from, size), HandlerDef(this, "api.Search", "searchJson", Seq(classOf[String], classOf[String], classOf[Option[Int]], classOf[Option[Int]]), "GET", """ ----------------------------------------------------------------------
 SEARCH ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/search/json""")
)
                      

// @LINE:680
def searchMultimediaIndex(section_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Search]).searchMultimediaIndex(section_id), HandlerDef(this, "api.Search", "searchMultimediaIndex", Seq(classOf[UUID]), "GET", """""", _prefix + """api/search/multimediasearch""")
)
                      

// @LINE:769
def querySPARQL(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Search]).querySPARQL(), HandlerDef(this, "api.Search", "querySPARQL", Seq(), "POST", """""", _prefix + """api/sparqlquery""")
)
                      

// @LINE:681
def search(query:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Search]).search(query), HandlerDef(this, "api.Search", "search", Seq(classOf[String]), "GET", """""", _prefix + """api/search""")
)
                      
    
}
                          

// @LINE:966
// @LINE:965
// @LINE:964
// @LINE:963
// @LINE:962
// @LINE:961
// @LINE:958
// @LINE:957
// @LINE:955
// @LINE:954
// @LINE:953
// @LINE:952
// @LINE:951
// @LINE:950
// @LINE:949
// @LINE:948
// @LINE:947
// @LINE:946
// @LINE:945
// @LINE:944
// @LINE:943
// @LINE:942
// @LINE:941
// @LINE:940
// @LINE:939
// @LINE:938
// @LINE:936
// @LINE:919
// @LINE:917
// @LINE:916
// @LINE:915
// @LINE:914
// @LINE:912
// @LINE:911
// @LINE:909
// @LINE:908
// @LINE:906
// @LINE:905
// @LINE:901
// @LINE:900
class ReverseT2C2 {
    

// @LINE:914
def attachVocabToDataset(vocab_id:UUID, dataset_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).attachVocabToDataset(vocab_id, dataset_id), HandlerDef(this, "api.T2C2", "attachVocabToDataset", Seq(classOf[UUID], classOf[UUID]), "PUT", """""", _prefix + """t2c2/templates/$vocab_id<[^/]+>/attachToDataset/$dataset_id<[^/]+>""")
)
                      

// @LINE:919
def listCollectionsCanEdit(id:UUID, limit:Integer): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).listCollectionsCanEdit(id, limit), HandlerDef(this, "api.T2C2", "listCollectionsCanEdit", Seq(classOf[UUID], classOf[Integer]), "GET", """""", _prefix + """api/t2c2/spaces/$id<[^/]+>/collectionsCanEdit""")
)
                      

// @LINE:963
def getKeysValuesFromLastDataset(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getKeysValuesFromLastDataset(), HandlerDef(this, "api.T2C2", "getKeysValuesFromLastDataset", Seq(), "GET", """""", _prefix + """t2c2/getKeyValuesLastDataset""")
)
                      

// @LINE:951
def getWhoICanUploadFor(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getWhoICanUploadFor(), HandlerDef(this, "api.T2C2", "getWhoICanUploadFor", Seq(), "GET", """""", _prefix + """api/t2c2/getUsersICanUploadFor""")
)
                      

// @LINE:900
def listMyDatasets(limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).listMyDatasets(limit), HandlerDef(this, "api.T2C2", "listMyDatasets", Seq(classOf[Int]), "GET", """""", _prefix + """api/t2c2/listMyDatasets""")
)
                      

// @LINE:917
def detachVocabFromFile(vocab_id:UUID, file_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).detachVocabFromFile(vocab_id, file_id), HandlerDef(this, "api.T2C2", "detachVocabFromFile", Seq(classOf[UUID], classOf[UUID]), "PUT", """""", _prefix + """t2c2/templates/$vocab_id<[^/]+>/detachFromFile/$file_id<[^/]+>""")
)
                      

// @LINE:955
def getNumUsersOfSpace(spaceId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getNumUsersOfSpace(spaceId), HandlerDef(this, "api.T2C2", "getNumUsersOfSpace", Seq(classOf[UUID]), "GET", """""", _prefix + """api/t2c2/getNumUsersOfSpace/$spaceId<[^/]+>""")
)
                      

// @LINE:952
def getSpacesOfUser(userId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getSpacesOfUser(userId), HandlerDef(this, "api.T2C2", "getSpacesOfUser", Seq(classOf[UUID]), "GET", """""", _prefix + """api/t2c2/getSpacesOfUser/$userId<[^/]+>""")
)
                      

// @LINE:965
def getKeysValuesFromDatasetId(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getKeysValuesFromDatasetId(id), HandlerDef(this, "api.T2C2", "getKeysValuesFromDatasetId", Seq(classOf[UUID]), "GET", """""", _prefix + """t2c2/getKeyValuesForDatasetId/$id<[^/]+>""")
)
                      

// @LINE:939
def getAllCollectionsWithDatasetIdsAndFiles(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsWithDatasetIdsAndFiles(), HandlerDef(this, "api.T2C2", "getAllCollectionsWithDatasetIdsAndFiles", Seq(), "GET", """""", _prefix + """t2c2/collections/allCollectionDatasetFiles""")
)
                      

// @LINE:915
def detachVocabFromDataset(vocab_id:UUID, dataset_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).detachVocabFromDataset(vocab_id, dataset_id), HandlerDef(this, "api.T2C2", "detachVocabFromDataset", Seq(classOf[UUID], classOf[UUID]), "PUT", """""", _prefix + """t2c2/templates/$vocab_id<[^/]+>/detachFromDataset/$dataset_id<[^/]+>""")
)
                      

// @LINE:958
def getDatasetWithAttachedVocab(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getDatasetWithAttachedVocab(id), HandlerDef(this, "api.T2C2", "getDatasetWithAttachedVocab", Seq(classOf[UUID]), "GET", """""", _prefix + """t2c2/datasets/getDatasetAndTemplate/$id<[^/]+>""")
)
                      

// @LINE:941
def getLevelOfTree(currentId:Option[String], currentType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getLevelOfTree(currentId, currentType), HandlerDef(this, "api.T2C2", "getLevelOfTree", Seq(classOf[Option[String]], classOf[String]), "GET", """""", _prefix + """t2c2/collections/getLevelOfTree""")
)
                      

// @LINE:957
def getTemplateFromLastDataset(limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getTemplateFromLastDataset(limit), HandlerDef(this, "api.T2C2", "getTemplateFromLastDataset", Seq(classOf[Int]), "GET", """""", _prefix + """t2c2/templates/lastTemplate""")
)
                      

// @LINE:901
def getVocabulary(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getVocabulary(id), HandlerDef(this, "api.T2C2", "getVocabulary", Seq(classOf[UUID]), "GET", """""", _prefix + """t2c2/templates/getExperimentTemplateById/$id<[^/]+>""")
)
                      

// @LINE:944
def getLevelOfTreeNotSharedInSpace(currentId:Option[String], currentType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getLevelOfTreeNotSharedInSpace(currentId, currentType), HandlerDef(this, "api.T2C2", "getLevelOfTreeNotSharedInSpace", Seq(classOf[Option[String]], classOf[String]), "GET", """""", _prefix + """t2c2/collections/getLevelOfTreeNotShared""")
)
                      

// @LINE:966
def getVocabIdNameFromTag(tag:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getVocabIdNameFromTag(tag), HandlerDef(this, "api.T2C2", "getVocabIdNameFromTag", Seq(classOf[String]), "GET", """""", _prefix + """t2c2/getIdNameFromTag/$tag<[^/]+>""")
)
                      

// @LINE:948
def createEmptyDataset(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).createEmptyDataset(), HandlerDef(this, "api.T2C2", "createEmptyDataset", Seq(), "POST", """""", _prefix + """t2c2/datasets/createEmpty""")
)
                      

// @LINE:962
def getAllCollectionsWithDatasetIds(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsWithDatasetIds(), HandlerDef(this, "api.T2C2", "getAllCollectionsWithDatasetIds", Seq(), "GET", """""", _prefix + """api/t2c2/allCollectionsWithDatasetIds""")
)
                      

// @LINE:912
def makeTemplatePrivate(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).makeTemplatePrivate(id), HandlerDef(this, "api.T2C2", "makeTemplatePrivate", Seq(classOf[UUID]), "PUT", """""", _prefix + """t2c2/templates/makePrivate/$id<[^/]+>""")
)
                      

// @LINE:946
def getAllCollectionsForFullTree(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsForFullTree(), HandlerDef(this, "api.T2C2", "getAllCollectionsForFullTree", Seq(), "GET", """""", _prefix + """t2c2/collections/collectionsForFullTree""")
)
                      

// @LINE:954
def getUsersOfSpace(spaceId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getUsersOfSpace(spaceId), HandlerDef(this, "api.T2C2", "getUsersOfSpace", Seq(classOf[UUID]), "GET", """""", _prefix + """api/t2c2/getUsersOfSpace/$spaceId<[^/]+>""")
)
                      

// @LINE:943
def getLevelOfTreeSharedWithOthers(currentId:Option[String], currentType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getLevelOfTreeSharedWithOthers(currentId, currentType), HandlerDef(this, "api.T2C2", "getLevelOfTreeSharedWithOthers", Seq(classOf[Option[String]], classOf[String]), "GET", """""", _prefix + """t2c2/collections/getLevelOfTreeSharedWithOthers""")
)
                      

// @LINE:906
def getVocabByFileId(file_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getVocabByFileId(file_id), HandlerDef(this, "api.T2C2", "getVocabByFileId", Seq(classOf[UUID]), "GET", """""", _prefix + """t2c2/templates/getByFileId/$file_id<[^/]+>""")
)
                      

// @LINE:938
def getAllCollectionsOfUser(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsOfUser(), HandlerDef(this, "api.T2C2", "getAllCollectionsOfUser", Seq(), "GET", """GET         /t2c2/dashboard                                                                             @controllers.T2C2.index()""", _prefix + """t2c2/collections/allCollection""")
)
                      

// @LINE:945
def getLevelOfTreeInSpace(currentId:Option[String], currentType:String, spaceId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getLevelOfTreeInSpace(currentId, currentType, spaceId), HandlerDef(this, "api.T2C2", "getLevelOfTreeInSpace", Seq(classOf[Option[String]], classOf[String], classOf[String]), "GET", """""", _prefix + """t2c2/collections/getLevelOfTreeInSpace""")
)
                      

// @LINE:936
def findYoungestChild(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).findYoungestChild(), HandlerDef(this, "api.T2C2", "findYoungestChild", Seq(), "POST", """GET         /t2c2/dragAndDrop                                                                           @controllers.T2C2.dragAndDrop()""", _prefix + """t2c2/findYoungestChild""")
)
                      

// @LINE:916
def attachVocabToFile(vocab_id:UUID, file_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).attachVocabToFile(vocab_id, file_id), HandlerDef(this, "api.T2C2", "attachVocabToFile", Seq(classOf[UUID], classOf[UUID]), "PUT", """""", _prefix + """t2c2/templates/$vocab_id<[^/]+>/attachToFile/$file_id<[^/]+>""")
)
                      

// @LINE:947
def getAllCollectionsOfUserNotSharedInSpace(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsOfUserNotSharedInSpace(), HandlerDef(this, "api.T2C2", "getAllCollectionsOfUserNotSharedInSpace", Seq(), "GET", """""", _prefix + """t2c2/collections/allCollectionsNotSharedInSpace""")
)
                      

// @LINE:905
def getVocabByDatasetId(dataset_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getVocabByDatasetId(dataset_id), HandlerDef(this, "api.T2C2", "getVocabByDatasetId", Seq(classOf[UUID]), "GET", """""", _prefix + """t2c2/templates/getByDatasetId/$dataset_id<[^/]+>""")
)
                      

// @LINE:909
def listMySpaces(limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).listMySpaces(limit), HandlerDef(this, "api.T2C2", "listMySpaces", Seq(classOf[Int]), "GET", """""", _prefix + """api/t2c2/listMySpaces""")
)
                      

// @LINE:961
def getDatasetsInCollectionWithColId(coll_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getDatasetsInCollectionWithColId(coll_id), HandlerDef(this, "api.T2C2", "getDatasetsInCollectionWithColId", Seq(classOf[UUID]), "GET", """""", _prefix + """api/t2c2/collections/$coll_id<[^/]+>/datasetsWithParentColId""")
)
                      

// @LINE:940
def getAllCollectionsForTree(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsForTree(), HandlerDef(this, "api.T2C2", "getAllCollectionsForTree", Seq(), "GET", """""", _prefix + """t2c2/collections/collectionsForTree""")
)
                      

// @LINE:953
def getSpacesUserHasAccessTo(userId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getSpacesUserHasAccessTo(userId), HandlerDef(this, "api.T2C2", "getSpacesUserHasAccessTo", Seq(classOf[UUID]), "GET", """""", _prefix + """api/t2c2/getSpacesUserCanEdit/$userId<[^/]+>""")
)
                      

// @LINE:942
def getLevelOfTreeSharedWithMe(currentId:Option[String], currentType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getLevelOfTreeSharedWithMe(currentId, currentType), HandlerDef(this, "api.T2C2", "getLevelOfTreeSharedWithMe", Seq(classOf[Option[String]], classOf[String]), "GET", """""", _prefix + """t2c2/collections/getLevelOfTreeSharedWithMe""")
)
                      

// @LINE:949
def moveKeysToTermsTemplates(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).moveKeysToTermsTemplates(), HandlerDef(this, "api.T2C2", "moveKeysToTermsTemplates", Seq(), "PUT", """""", _prefix + """t2c2/templates/moveKeysToTerms""")
)
                      

// @LINE:950
def bulkDelete(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).bulkDelete(), HandlerDef(this, "api.T2C2", "bulkDelete", Seq(), "DELETE", """""", _prefix + """api/t2c2/bulkDelete""")
)
                      

// @LINE:911
def makeTemplatePublic(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).makeTemplatePublic(id), HandlerDef(this, "api.T2C2", "makeTemplatePublic", Seq(classOf[UUID]), "PUT", """""", _prefix + """t2c2/templates/makePublic/$id<[^/]+>""")
)
                      

// @LINE:964
def getKeysValuesFromLastDatasets(limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getKeysValuesFromLastDatasets(limit), HandlerDef(this, "api.T2C2", "getKeysValuesFromLastDatasets", Seq(classOf[Int]), "GET", """""", _prefix + """t2c2/getKeyValuesForLastDatasets""")
)
                      
    
}
                          

// @LINE:770
class ReverseProjects {
    

// @LINE:770
def addproject(project:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Projects]).addproject(project), HandlerDef(this, "api.Projects", "addproject", Seq(classOf[String]), "POST", """""", _prefix + """api/projects/addproject""")
)
                      
    
}
                          

// @LINE:913
// @LINE:910
// @LINE:904
// @LINE:903
// @LINE:902
// @LINE:899
// @LINE:898
// @LINE:896
// @LINE:895
// @LINE:893
// @LINE:892
// @LINE:891
// @LINE:861
// @LINE:860
// @LINE:858
// @LINE:857
// @LINE:856
// @LINE:855
// @LINE:854
// @LINE:851
// @LINE:850
// @LINE:849
// @LINE:848
// @LINE:847
class ReverseVocabularies {
    

// @LINE:910
def removeVocabulary(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).removeVocabulary(id), HandlerDef(this, "api.Vocabularies", "removeVocabulary", Seq(classOf[UUID]), "DELETE", """""", _prefix + """t2c2/templates/deleteTemplate/$id<[^/]+>""")
)
                      

// @LINE:851
def getPublicVocabularies(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getPublicVocabularies(), HandlerDef(this, "api.Vocabularies", "getPublicVocabularies", Seq(), "GET", """""", _prefix + """api/vocabularies/getPublic""")
)
                      

// @LINE:903
def getByTag(containsAll:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getByTag(containsAll), HandlerDef(this, "api.Vocabularies", "getByTag", Seq(classOf[Boolean]), "POST", """""", _prefix + """t2c2/templates/findByTag""")
)
                      

// @LINE:896
def listAll(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).listAll(), HandlerDef(this, "api.Vocabularies", "listAll", Seq(), "GET", """""", _prefix + """t2c2/templates/allExperimentTemplates""")
)
                      

// @LINE:904
def getBySingleTag(tag:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getBySingleTag(tag), HandlerDef(this, "api.Vocabularies", "getBySingleTag", Seq(classOf[String]), "GET", """""", _prefix + """t2c2/templates/findByTag/$tag<[^/]+>""")
)
                      

// @LINE:848
def createVocabularyFromJson(isPublic:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).createVocabularyFromJson(isPublic), HandlerDef(this, "api.Vocabularies", "createVocabularyFromJson", Seq(classOf[Boolean]), "POST", """""", _prefix + """api/vocabularies/createVocabularyFromJson""")
)
                      

// @LINE:849
def createVocabularyFromForm(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).createVocabularyFromForm(), HandlerDef(this, "api.Vocabularies", "createVocabularyFromForm", Seq(), "POST", """""", _prefix + """api/vocabularies/createVocabulary""")
)
                      

// @LINE:854
def get(vocab_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).get(vocab_id), HandlerDef(this, "api.Vocabularies", "get", Seq(classOf[UUID]), "GET", """""", _prefix + """api/vocabularies/$vocab_id<[^/]+>""")
)
                      

// @LINE:858
def getByNameAndAuthor(name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getByNameAndAuthor(name), HandlerDef(this, "api.Vocabularies", "getByNameAndAuthor", Seq(classOf[String]), "GET", """""", _prefix + """api/vocabularies/$name<[^/]+>/getByNameAuthor""")
)
                      

// @LINE:847
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).list(), HandlerDef(this, "api.Vocabularies", "list", Seq(), "GET", """""", _prefix + """api/vocabularies/list""")
)
                      

// @LINE:855
def editVocabulary(vocab_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).editVocabulary(vocab_id), HandlerDef(this, "api.Vocabularies", "editVocabulary", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/editVocabulary/$vocab_id<[^/]+>""")
)
                      

// @LINE:899
def getAllTagsOfAllVocabularies(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getAllTagsOfAllVocabularies(), HandlerDef(this, "api.Vocabularies", "getAllTagsOfAllVocabularies", Seq(), "GET", """""", _prefix + """t2c2/templates/allTags""")
)
                      

// @LINE:856
def getByName(name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getByName(name), HandlerDef(this, "api.Vocabularies", "getByName", Seq(classOf[String]), "GET", """""", _prefix + """api/vocabularies/$name<[^/]+>""")
)
                      

// @LINE:860
def addToSpace(vocab_id:UUID, space_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).addToSpace(vocab_id, space_id), HandlerDef(this, "api.Vocabularies", "addToSpace", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/vocabularies/$vocab_id<[^/]+>/addToSpace/$space_id<[^/]+>""")
)
                      

// @LINE:861
def removeFromSpace(vocab_id:UUID, space_id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).removeFromSpace(vocab_id, space_id), HandlerDef(this, "api.Vocabularies", "removeFromSpace", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/vocabuliaries/$vocab_id<[^/]+>/removeFromSpace/$space_id<[^/]+>""")
)
                      

// @LINE:850
def getByAuthor(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getByAuthor(), HandlerDef(this, "api.Vocabularies", "getByAuthor", Seq(), "GET", """""", _prefix + """api/vocabularies/getByAuthor""")
)
                      
    
}
                          

// @LINE:765
class ReverseZoomIt {
    

// @LINE:765
def uploadTile(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ZoomIt]).uploadTile(), HandlerDef(this, "api.ZoomIt", "uploadTile", Seq(), "POST", """ ----------------------------------------------------------------------
 MISC./OTHER ENDPOINTS
 ----------------------------------------------------------------------""", _prefix + """api/tiles""")
)
                      
    
}
                          

// @LINE:767
class ReverseThreeDTexture {
    

// @LINE:767
def uploadTexture(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ThreeDTexture]).uploadTexture(), HandlerDef(this, "api.ThreeDTexture", "uploadTexture", Seq(), "POST", """""", _prefix + """api/3dTextures""")
)
                      
    
}
                          

// @LINE:483
// @LINE:482
// @LINE:481
// @LINE:480
// @LINE:479
// @LINE:477
// @LINE:476
// @LINE:475
// @LINE:474
// @LINE:473
// @LINE:472
// @LINE:471
// @LINE:470
// @LINE:469
// @LINE:468
// @LINE:467
// @LINE:466
// @LINE:465
// @LINE:464
// @LINE:463
// @LINE:462
// @LINE:461
// @LINE:460
class ReverseSpaces {
    

// @LINE:474
def addDatasetToSpace(spaceId:UUID, datasetId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).addDatasetToSpace(spaceId, datasetId), HandlerDef(this, "api.Spaces", "addDatasetToSpace", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/spaces/$spaceId<[^/]+>/addDatasetToSpace/$datasetId<[^/]+>""")
)
                      

// @LINE:464
def removeSpace(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).removeSpace(id), HandlerDef(this, "api.Spaces", "removeSpace", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/spaces/$id<[^/]+>""")
)
                      

// @LINE:482
def listCollectionsCanEdit(id:UUID, limit:Integer): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).listCollectionsCanEdit(id, limit), HandlerDef(this, "api.Spaces", "listCollectionsCanEdit", Seq(classOf[UUID], classOf[Integer]), "GET", """""", _prefix + """api/spaces/$id<[^/]+>/collectionsCanEdit""")
)
                      

// @LINE:466
def removeDataset(spaceId:UUID, datasetId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).removeDataset(spaceId, datasetId), HandlerDef(this, "api.Spaces", "removeDataset", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/spaces/$spaceId<[^/]+>/removeDataset/$datasetId<[^/]+>""")
)
                      

// @LINE:465
def removeCollection(spaceId:UUID, collectionId:UUID, removeDatasets:Boolean): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).removeCollection(spaceId, collectionId, removeDatasets), HandlerDef(this, "api.Spaces", "removeCollection", Seq(classOf[UUID], classOf[UUID], classOf[Boolean]), "POST", """""", _prefix + """api/spaces/$spaceId<[^/]+>/removeCollection/$collectionId<[^/]+>""")
)
                      

// @LINE:463
def createSpace(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).createSpace(), HandlerDef(this, "api.Spaces", "createSpace", Seq(), "POST", """""", _prefix + """api/spaces""")
)
                      

// @LINE:470
def updateUsers(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).updateUsers(id), HandlerDef(this, "api.Spaces", "updateUsers", Seq(classOf[UUID]), "POST", """""", _prefix + """api/spaces/$id<[^/]+>/updateUsers""")
)
                      

// @LINE:468
def copyContentsToNewSpace(spaceId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).copyContentsToNewSpace(spaceId), HandlerDef(this, "api.Spaces", "copyContentsToNewSpace", Seq(classOf[UUID]), "POST", """""", _prefix + """api/spaces/$spaceId<[^/]+>/copyContentsToNewSpace""")
)
                      

// @LINE:472
def rejectRequest(id:UUID, user:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).rejectRequest(id, user), HandlerDef(this, "api.Spaces", "rejectRequest", Seq(classOf[UUID], classOf[String]), "POST", """""", _prefix + """api/spaces/$id<[^/]+>/rejectRequest""")
)
                      

// @LINE:473
def removeUser(id:UUID, removeUser:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).removeUser(id, removeUser), HandlerDef(this, "api.Spaces", "removeUser", Seq(classOf[UUID], classOf[String]), "POST", """""", _prefix + """api/spaces/$id<[^/]+>/removeUser""")
)
                      

// @LINE:475
def addCollectionToSpace(spaceId:UUID, collectionId:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).addCollectionToSpace(spaceId, collectionId), HandlerDef(this, "api.Spaces", "addCollectionToSpace", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/spaces/$spaceId<[^/]+>/addCollectionToSpace/$collectionId<[^/]+>""")
)
                      

// @LINE:462
def get(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).get(id), HandlerDef(this, "api.Spaces", "get", Seq(classOf[UUID]), "GET", """""", _prefix + """api/spaces/$id<[^/]+>""")
)
                      

// @LINE:461
def listCanEdit(title:Option[String], date:Option[String], limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).listCanEdit(title, date, limit), HandlerDef(this, "api.Spaces", "listCanEdit", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int]), "GET", """""", _prefix + """api/spaces/canEdit""")
)
                      

// @LINE:471
def acceptRequest(id:UUID, user:String, role:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).acceptRequest(id, user, role), HandlerDef(this, "api.Spaces", "acceptRequest", Seq(classOf[UUID], classOf[String], classOf[String]), "POST", """""", _prefix + """api/spaces/$id<[^/]+>/acceptRequest""")
)
                      

// @LINE:469
def updateSpace(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).updateSpace(id), HandlerDef(this, "api.Spaces", "updateSpace", Seq(classOf[UUID]), "POST", """""", _prefix + """api/spaces/$id<[^/]+>/update""")
)
                      

// @LINE:480
def listDatasets(id:UUID, limit:Integer): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).listDatasets(id, limit), HandlerDef(this, "api.Spaces", "listDatasets", Seq(classOf[UUID], classOf[Integer]), "GET", """""", _prefix + """api/spaces/$id<[^/]+>/datasets""")
)
                      

// @LINE:479
def verifySpace(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).verifySpace(id), HandlerDef(this, "api.Spaces", "verifySpace", Seq(classOf[UUID]), "PUT", """""", _prefix + """api/spaces/$id<[^/]+>/verify""")
)
                      

// @LINE:460
def list(title:Option[String], date:Option[String], limit:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).list(title, date, limit), HandlerDef(this, "api.Spaces", "list", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int]), "GET", """ ----------------------------------------------------------------------
 SPACES ENDPOINT
 ----------------------------------------------------------------------""", _prefix + """api/spaces""")
)
                      

// @LINE:477
def unfollow(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).unfollow(id), HandlerDef(this, "api.Spaces", "unfollow", Seq(classOf[UUID]), "POST", """""", _prefix + """api/spaces/$id<[^/]+>/unfollow""")
)
                      

// @LINE:483
def getUserSpaceRoleMap(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).getUserSpaceRoleMap(id), HandlerDef(this, "api.Spaces", "getUserSpaceRoleMap", Seq(classOf[UUID]), "GET", """""", _prefix + """api/spaces/$id<[^/]+>/userSpaceRoleMap""")
)
                      

// @LINE:476
def follow(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).follow(id), HandlerDef(this, "api.Spaces", "follow", Seq(classOf[UUID]), "POST", """""", _prefix + """api/spaces/$id<[^/]+>/follow""")
)
                      

// @LINE:481
def listCollections(id:UUID, limit:Integer): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).listCollections(id, limit), HandlerDef(this, "api.Spaces", "listCollections", Seq(classOf[UUID], classOf[Integer]), "GET", """""", _prefix + """api/spaces/$id<[^/]+>/collections""")
)
                      

// @LINE:467
def copyContentsToSpace(spaceId:UUID, id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).copyContentsToSpace(spaceId, id), HandlerDef(this, "api.Spaces", "copyContentsToSpace", Seq(classOf[UUID], classOf[UUID]), "POST", """""", _prefix + """api/spaces/$spaceId<[^/]+>/copyContentsToSpace/$id<[^/]+>""")
)
                      
    
}
                          

// @LINE:760
// @LINE:759
// @LINE:758
// @LINE:757
// @LINE:756
class ReverseRelations {
    

// @LINE:760
def delete(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Relations]).delete(id), HandlerDef(this, "api.Relations", "delete", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/relations/$id<[^/]+>""")
)
                      

// @LINE:758
def get(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Relations]).get(id), HandlerDef(this, "api.Relations", "get", Seq(classOf[UUID]), "GET", """""", _prefix + """api/relations/$id<[^/]+>""")
)
                      

// @LINE:757
def findTargets(sourceId:String, sourceType:String, targetType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Relations]).findTargets(sourceId, sourceType, targetType), HandlerDef(this, "api.Relations", "findTargets", Seq(classOf[String], classOf[String], classOf[String]), "GET", """""", _prefix + """api/relations/search""")
)
                      

// @LINE:759
def add(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Relations]).add(), HandlerDef(this, "api.Relations", "add", Seq(), "POST", """""", _prefix + """api/relations""")
)
                      

// @LINE:756
def list(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Relations]).list(), HandlerDef(this, "api.Relations", "list", Seq(), "GET", """ ----------------------------------------------------------------------
 RELATIONS API
 ----------------------------------------------------------------------""", _prefix + """api/relations""")
)
                      
    
}
                          

// @LINE:803
// @LINE:802
class ReverseStatus {
    

// @LINE:803
def status(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Status]).status(), HandlerDef(this, "api.Status", "status", Seq(), "GET", """""", _prefix + """api/status""")
)
                      

// @LINE:802
def version(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Status]).version(), HandlerDef(this, "api.Status", "version", Seq(), "GET", """ ----------------------------------------------------------------------
 Clowder STATUS API
 ----------------------------------------------------------------------""", _prefix + """api/version""")
)
                      
    
}
                          

// @LINE:314
// @LINE:313
// @LINE:312
// @LINE:311
class ReverseContextLD {
    

// @LINE:313
def getContextByName(name:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ContextLD]).getContextByName(name), HandlerDef(this, "api.ContextLD", "getContextByName", Seq(classOf[String]), "GET", """""", _prefix + """api/contexts/$name<[^/]+>/context.json""")
)
                      

// @LINE:314
def removeById(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ContextLD]).removeById(id), HandlerDef(this, "api.ContextLD", "removeById", Seq(classOf[UUID]), "DELETE", """""", _prefix + """api/contexts/$id<[^/]+>""")
)
                      

// @LINE:312
def getContextById(id:UUID): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ContextLD]).getContextById(id), HandlerDef(this, "api.ContextLD", "getContextById", Seq(classOf[UUID]), "GET", """""", _prefix + """api/contexts/$id<[^/]+>""")
)
                      

// @LINE:311
def addContext(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ContextLD]).addContext(), HandlerDef(this, "api.ContextLD", "addContext", Seq(), "POST", """----------------------------------------------------------------------
 JSON-LD METADATA
----------------------------------------------------------------------""", _prefix + """api/contexts""")
)
                      
    
}
                          
}
        
    