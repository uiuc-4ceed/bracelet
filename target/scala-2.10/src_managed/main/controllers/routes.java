// @SOURCE:/Users/stevek/4ceed-clowder/conf/routes
// @HASH:8de3ef234bb2a5fe5ca89814b4a202b0871d3dd3
// @DATE:Thu Aug 29 17:10:48 CDT 2019

package controllers;

public class routes {
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static final controllers.ReverseMetadata Metadata = new controllers.ReverseMetadata();
public static final controllers.ReverseToolManager ToolManager = new controllers.ReverseToolManager();
public static final controllers.ReverseFolders Folders = new controllers.ReverseFolders();
public static final controllers.ReverseAdmin Admin = new controllers.ReverseAdmin();
public static final controllers.ReverseUsers Users = new controllers.ReverseUsers();
public static final controllers.ReverseExtractionInfo ExtractionInfo = new controllers.ReverseExtractionInfo();
public static final controllers.ReverseExtractors Extractors = new controllers.ReverseExtractors();
public static final controllers.ReverseError Error = new controllers.ReverseError();
public static final controllers.ReverseDataAnalysis DataAnalysis = new controllers.ReverseDataAnalysis();
public static final controllers.ReverseFiles Files = new controllers.ReverseFiles();
public static final controllers.ReversePreviewers Previewers = new controllers.ReversePreviewers();
public static final controllers.ReverseProfile Profile = new controllers.ReverseProfile();
public static final controllers.ReverseNotebook Notebook = new controllers.ReverseNotebook();
public static final controllers.ReverseCollections Collections = new controllers.ReverseCollections();
public static final controllers.ReverseRegistration Registration = new controllers.ReverseRegistration();
public static final controllers.ReverseDatasets Datasets = new controllers.ReverseDatasets();
public static final controllers.ReverseLogin Login = new controllers.ReverseLogin();
public static final controllers.ReverseSelected Selected = new controllers.ReverseSelected();
public static final controllers.ReverseEvents Events = new controllers.ReverseEvents();
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();
public static final controllers.ReverseGeostreams Geostreams = new controllers.ReverseGeostreams();
public static final controllers.ReverseTags Tags = new controllers.ReverseTags();
public static final controllers.ReverseRSS RSS = new controllers.ReverseRSS();
public static final controllers.ReverseCurationObjects CurationObjects = new controllers.ReverseCurationObjects();
public static final controllers.ReverseSearch Search = new controllers.ReverseSearch();
public static final controllers.ReverseT2C2 T2C2 = new controllers.ReverseT2C2();
public static final controllers.ReverseVocabularies Vocabularies = new controllers.ReverseVocabularies();
public static final controllers.ReverseSpaces Spaces = new controllers.ReverseSpaces();
public static class javascript {
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();
public static final controllers.javascript.ReverseMetadata Metadata = new controllers.javascript.ReverseMetadata();
public static final controllers.javascript.ReverseToolManager ToolManager = new controllers.javascript.ReverseToolManager();
public static final controllers.javascript.ReverseFolders Folders = new controllers.javascript.ReverseFolders();
public static final controllers.javascript.ReverseAdmin Admin = new controllers.javascript.ReverseAdmin();
public static final controllers.javascript.ReverseUsers Users = new controllers.javascript.ReverseUsers();
public static final controllers.javascript.ReverseExtractionInfo ExtractionInfo = new controllers.javascript.ReverseExtractionInfo();
public static final controllers.javascript.ReverseExtractors Extractors = new controllers.javascript.ReverseExtractors();
public static final controllers.javascript.ReverseError Error = new controllers.javascript.ReverseError();
public static final controllers.javascript.ReverseDataAnalysis DataAnalysis = new controllers.javascript.ReverseDataAnalysis();
public static final controllers.javascript.ReverseFiles Files = new controllers.javascript.ReverseFiles();
public static final controllers.javascript.ReversePreviewers Previewers = new controllers.javascript.ReversePreviewers();
public static final controllers.javascript.ReverseProfile Profile = new controllers.javascript.ReverseProfile();
public static final controllers.javascript.ReverseNotebook Notebook = new controllers.javascript.ReverseNotebook();
public static final controllers.javascript.ReverseCollections Collections = new controllers.javascript.ReverseCollections();
public static final controllers.javascript.ReverseRegistration Registration = new controllers.javascript.ReverseRegistration();
public static final controllers.javascript.ReverseDatasets Datasets = new controllers.javascript.ReverseDatasets();
public static final controllers.javascript.ReverseLogin Login = new controllers.javascript.ReverseLogin();
public static final controllers.javascript.ReverseSelected Selected = new controllers.javascript.ReverseSelected();
public static final controllers.javascript.ReverseEvents Events = new controllers.javascript.ReverseEvents();
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
public static final controllers.javascript.ReverseGeostreams Geostreams = new controllers.javascript.ReverseGeostreams();
public static final controllers.javascript.ReverseTags Tags = new controllers.javascript.ReverseTags();
public static final controllers.javascript.ReverseRSS RSS = new controllers.javascript.ReverseRSS();
public static final controllers.javascript.ReverseCurationObjects CurationObjects = new controllers.javascript.ReverseCurationObjects();
public static final controllers.javascript.ReverseSearch Search = new controllers.javascript.ReverseSearch();
public static final controllers.javascript.ReverseT2C2 T2C2 = new controllers.javascript.ReverseT2C2();
public static final controllers.javascript.ReverseVocabularies Vocabularies = new controllers.javascript.ReverseVocabularies();
public static final controllers.javascript.ReverseSpaces Spaces = new controllers.javascript.ReverseSpaces();
}
public static class ref {
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();
public static final controllers.ref.ReverseMetadata Metadata = new controllers.ref.ReverseMetadata();
public static final controllers.ref.ReverseToolManager ToolManager = new controllers.ref.ReverseToolManager();
public static final controllers.ref.ReverseFolders Folders = new controllers.ref.ReverseFolders();
public static final controllers.ref.ReverseAdmin Admin = new controllers.ref.ReverseAdmin();
public static final controllers.ref.ReverseUsers Users = new controllers.ref.ReverseUsers();
public static final controllers.ref.ReverseExtractionInfo ExtractionInfo = new controllers.ref.ReverseExtractionInfo();
public static final controllers.ref.ReverseExtractors Extractors = new controllers.ref.ReverseExtractors();
public static final controllers.ref.ReverseError Error = new controllers.ref.ReverseError();
public static final controllers.ref.ReverseDataAnalysis DataAnalysis = new controllers.ref.ReverseDataAnalysis();
public static final controllers.ref.ReverseFiles Files = new controllers.ref.ReverseFiles();
public static final controllers.ref.ReversePreviewers Previewers = new controllers.ref.ReversePreviewers();
public static final controllers.ref.ReverseProfile Profile = new controllers.ref.ReverseProfile();
public static final controllers.ref.ReverseNotebook Notebook = new controllers.ref.ReverseNotebook();
public static final controllers.ref.ReverseCollections Collections = new controllers.ref.ReverseCollections();
public static final controllers.ref.ReverseRegistration Registration = new controllers.ref.ReverseRegistration();
public static final controllers.ref.ReverseDatasets Datasets = new controllers.ref.ReverseDatasets();
public static final controllers.ref.ReverseLogin Login = new controllers.ref.ReverseLogin();
public static final controllers.ref.ReverseSelected Selected = new controllers.ref.ReverseSelected();
public static final controllers.ref.ReverseEvents Events = new controllers.ref.ReverseEvents();
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
public static final controllers.ref.ReverseGeostreams Geostreams = new controllers.ref.ReverseGeostreams();
public static final controllers.ref.ReverseTags Tags = new controllers.ref.ReverseTags();
public static final controllers.ref.ReverseRSS RSS = new controllers.ref.ReverseRSS();
public static final controllers.ref.ReverseCurationObjects CurationObjects = new controllers.ref.ReverseCurationObjects();
public static final controllers.ref.ReverseSearch Search = new controllers.ref.ReverseSearch();
public static final controllers.ref.ReverseT2C2 T2C2 = new controllers.ref.ReverseT2C2();
public static final controllers.ref.ReverseVocabularies Vocabularies = new controllers.ref.ReverseVocabularies();
public static final controllers.ref.ReverseSpaces Spaces = new controllers.ref.ReverseSpaces();
}
}
          