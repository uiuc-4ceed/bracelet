// @SOURCE:/Users/stevek/4ceed-clowder/conf/routes
// @HASH:8de3ef234bb2a5fe5ca89814b4a202b0871d3dd3
// @DATE:Thu Aug 29 17:10:48 CDT 2019


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import models._
import Binders._

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:7
private[this] lazy val api_ApiHelp_options0 = Route("OPTIONS", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),DynamicPart("path", """.+""",false))))
        

// @LINE:8
private[this] lazy val controllers_Application_untrail1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),DynamicPart("path", """.+""",false),StaticPart("/"))))
        

// @LINE:14
private[this] lazy val controllers_Application_index2 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:15
private[this] lazy val controllers_Application_about3 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("about"))))
        

// @LINE:16
private[this] lazy val controllers_Application_tos4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("tos"))))
        

// @LINE:17
private[this] lazy val controllers_Application_email5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("email"))))
        

// @LINE:22
private[this] lazy val controllers_Assets_at6 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("contexts/metadata.jsonld"))))
        

// @LINE:23
private[this] lazy val controllers_Assets_at7 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/img/glyphicons-halflings-white.png"))))
        

// @LINE:24
private[this] lazy val controllers_Assets_at8 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/img/glyphicons-halflings.png"))))
        

// @LINE:25
private[this] lazy val controllers_Assets_at9 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        

// @LINE:30
private[this] lazy val controllers_Users_getUsers10 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users"))))
        

// @LINE:31
private[this] lazy val controllers_Users_acceptTermsOfServices11 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/acceptTermsOfServices"))))
        

// @LINE:32
private[this] lazy val controllers_Login_isLoggedIn12 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login/isLoggedIn"))))
        

// @LINE:33
private[this] lazy val controllers_Application_onlyGoogle13 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login/isLoggedInWithGoogle"))))
        

// @LINE:34
private[this] lazy val controllers_Users_sendEmail14 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/sendEmail"))))
        

// @LINE:35
private[this] lazy val controllers_Login_ldap15 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("ldap"))))
        

// @LINE:36
private[this] lazy val controllers_Login_ldapAuthenticate16 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("ldap/authenticate"))))
        

// @LINE:41
private[this] lazy val controllers_Profile_viewProfile17 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("profile/viewProfile"))))
        

// @LINE:43
private[this] lazy val controllers_Profile_viewProfileUUID18 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("profile/viewProfile/"),DynamicPart("uuid", """[^/]+""",true))))
        

// @LINE:44
private[this] lazy val controllers_Profile_editProfile19 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("profile/editProfile"))))
        

// @LINE:45
private[this] lazy val controllers_Profile_submitChanges20 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("profile/submitChanges"))))
        

// @LINE:46
private[this] lazy val controllers_Profile_viewProfileUUID21 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("profile/"),DynamicPart("uuid", """[^/]+""",true))))
        

// @LINE:51
private[this] lazy val controllers_Error_authenticationRequired22 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("error/authenticationRequired"))))
        

// @LINE:52
private[this] lazy val controllers_Error_authenticationRequiredMessage23 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("error/authenticationRequiredMessage/"),DynamicPart("msg", """[^/]+""",true),StaticPart("/"),DynamicPart("url", """[^/]+""",true))))
        

// @LINE:53
private[this] lazy val controllers_Error_incorrectPermissions24 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("error/noPermissions"))))
        

// @LINE:54
private[this] lazy val controllers_Error_notActivated25 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("error/notActivated"))))
        

// @LINE:55
private[this] lazy val controllers_Error_notAuthorized26 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("error/notAuthorized"))))
        

// @LINE:60
private[this] lazy val controllers_ExtractionInfo_getDTSRequests27 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("extractions/requests"))))
        

// @LINE:61
private[this] lazy val controllers_ExtractionInfo_getExtractorServersIP28 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("extractions/servers_ips"))))
        

// @LINE:62
private[this] lazy val controllers_ExtractionInfo_getExtractorNames29 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("extractions/extractors_names"))))
        

// @LINE:63
private[this] lazy val controllers_ExtractionInfo_getExtractorInputTypes30 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("extractions/supported_input_types"))))
        

// @LINE:68
private[this] lazy val controllers_ExtractionInfo_getBookmarkletPage31 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("bookmarklet"))))
        

// @LINE:69
private[this] lazy val controllers_Application_bookmarklet32 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("bookmarklet.js"))))
        

// @LINE:74
private[this] lazy val controllers_ExtractionInfo_getExtensionPage33 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("extensions/dts/chrome"))))
        

// @LINE:76
private[this] lazy val controllers_Files_extractFile34 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("extraction/form"))))
        

// @LINE:77
private[this] lazy val controllers_Files_uploadExtract35 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("extraction/upload"))))
        

// @LINE:83
private[this] lazy val controllers_Files_list36 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files"))))
        

// @LINE:84
private[this] lazy val controllers_Files_uploadFile37 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files/new"))))
        

// @LINE:86
private[this] lazy val controllers_Files_metadataSearch38 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files/metadataSearch"))))
        

// @LINE:87
private[this] lazy val controllers_Files_generalMetadataSearch39 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files/generalMetadataSearch"))))
        

// @LINE:88
private[this] lazy val controllers_Files_followingFiles40 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files/following"))))
        

// @LINE:89
private[this] lazy val controllers_Files_file41 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:90
private[this] lazy val controllers_Files_filePreview42 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("filesPreview/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:91
private[this] lazy val controllers_Files_download43 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/blob"))))
        

// @LINE:92
private[this] lazy val controllers_Files_downloadAsFormat44 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/download/"),DynamicPart("format", """[^/]+""",true))))
        

// @LINE:95
private[this] lazy val controllers_Files_upload45 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("upload"))))
        

// @LINE:96
private[this] lazy val controllers_Files_uploadAndExtract46 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("uploadAndExtract"))))
        

// @LINE:97
private[this] lazy val controllers_Files_uploadSelectQuery47 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("uploadSelectQuery"))))
        

// @LINE:99
private[this] lazy val controllers_Files_uploaddnd48 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("uploaddnd/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:101
private[this] lazy val controllers_Files_uploadDragDrop49 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("uploadDragDrop"))))
        

// @LINE:102
private[this] lazy val controllers_Files_thumbnail50 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("fileThumbnail/"),DynamicPart("id", """[^/]+""",true),StaticPart("/blob"))))
        

// @LINE:103
private[this] lazy val controllers_Files_fileBySection51 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("file_by_section/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:104
private[this] lazy val controllers_Extractors_submitFileExtraction52 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files/"),DynamicPart("file_id", """[^/]+""",true),StaticPart("/extractions"))))
        

// @LINE:109
private[this] lazy val controllers_Datasets_list53 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets"))))
        

// @LINE:110
private[this] lazy val controllers_Datasets_sortedListInSpace54 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/sorted"))))
        

// @LINE:111
private[this] lazy val controllers_Datasets_newDataset55 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/new"))))
        

// @LINE:112
private[this] lazy val controllers_Datasets_createStep256 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/createStep2"))))
        

// @LINE:113
private[this] lazy val controllers_Datasets_metadataSearch57 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/metadataSearch"))))
        

// @LINE:114
private[this] lazy val controllers_Datasets_generalMetadataSearch58 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/generalMetadataSearch"))))
        

// @LINE:115
private[this] lazy val controllers_Datasets_followingDatasets59 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/following"))))
        

// @LINE:116
private[this] lazy val controllers_ToolManager_toolManager60 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("toolManager"))))
        

// @LINE:117
private[this] lazy val controllers_ToolManager_refreshToolSidebar61 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/refreshToolList"))))
        

// @LINE:118
private[this] lazy val controllers_ToolManager_getLaunchableTools62 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/launchableTools"))))
        

// @LINE:119
private[this] lazy val controllers_ToolManager_uploadDatasetToTool63 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/uploadToTool"))))
        

// @LINE:120
private[this] lazy val controllers_ToolManager_getInstances64 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/getInstances"))))
        

// @LINE:121
private[this] lazy val controllers_ToolManager_removeInstance65 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/removeInstance"))))
        

// @LINE:122
private[this] lazy val controllers_ToolManager_launchTool66 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/launchTool"))))
        

// @LINE:123
private[this] lazy val controllers_Datasets_dataset67 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:124
private[this] lazy val controllers_Datasets_getUpdatedFilesAndFolders68 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/"),DynamicPart("datasetId", """[^/]+""",true),StaticPart("/updatedFilesAndFolders"))))
        

// @LINE:125
private[this] lazy val controllers_Datasets_users69 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/users"))))
        

// @LINE:126
private[this] lazy val controllers_Datasets_datasetBySection70 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets_by_section/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:127
private[this] lazy val controllers_Datasets_submit71 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("dataset/submit"))))
        

// @LINE:128
private[this] lazy val controllers_Datasets_addFiles72 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/addFiles"))))
        

// @LINE:129
private[this] lazy val controllers_Folders_addFiles73 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/"),DynamicPart("folderId", """[^/]+""",true),StaticPart("/addFiles"))))
        

// @LINE:130
private[this] lazy val controllers_Folders_createFolder74 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/"),DynamicPart("parentDatasetId", """[^/]+""",true),StaticPart("/newFolder"))))
        

// @LINE:131
private[this] lazy val controllers_Extractors_submitDatasetExtraction75 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/extractions"))))
        

// @LINE:136
private[this] lazy val controllers_Collections_list76 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collections"))))
        

// @LINE:137
private[this] lazy val controllers_Collections_sortedListInSpace77 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collections/sorted"))))
        

// @LINE:138
private[this] lazy val controllers_Collections_listChildCollections78 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collections/listChildCollections"))))
        

// @LINE:139
private[this] lazy val controllers_Collections_newCollection79 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collections/new"))))
        

// @LINE:140
private[this] lazy val controllers_Collections_newCollectionWithParent80 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/newchildCollection"))))
        

// @LINE:141
private[this] lazy val controllers_Collections_followingCollections81 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collections/following"))))
        

// @LINE:142
private[this] lazy val controllers_Collections_submit82 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collection/submit"))))
        

// @LINE:143
private[this] lazy val controllers_Collections_createStep283 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collection/createStep2"))))
        

// @LINE:144
private[this] lazy val controllers_Collections_collection84 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collection/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:145
private[this] lazy val controllers_Collections_users85 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collection/"),DynamicPart("id", """[^/]+""",true),StaticPart("/users"))))
        

// @LINE:146
private[this] lazy val controllers_Collections_previews86 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collections/"),DynamicPart("collection_id", """[^/]+""",true),StaticPart("/previews"))))
        

// @LINE:147
private[this] lazy val controllers_Collections_getUpdatedDatasets87 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collection/"),DynamicPart("id", """[^/]+""",true),StaticPart("/datasets"))))
        

// @LINE:148
private[this] lazy val controllers_Collections_getUpdatedChildCollections88 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("collection/"),DynamicPart("id", """[^/]+""",true),StaticPart("/childCollections"))))
        

// @LINE:152
private[this] lazy val controllers_Spaces_list89 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces"))))
        

// @LINE:153
private[this] lazy val controllers_Spaces_newSpace90 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/new"))))
        

// @LINE:154
private[this] lazy val controllers_Spaces_copySpace91 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/copy/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:155
private[this] lazy val controllers_Spaces_followingSpaces92 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/following"))))
        

// @LINE:156
private[this] lazy val controllers_Spaces_getSpace93 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:157
private[this] lazy val controllers_Spaces_updateSpace94 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/updateSpace"))))
        

// @LINE:158
private[this] lazy val controllers_Spaces_addRequest95 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/addRequest"))))
        

// @LINE:159
private[this] lazy val controllers_Spaces_manageUsers96 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/users"))))
        

// @LINE:160
private[this] lazy val controllers_Spaces_selectExtractors97 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/extractors"))))
        

// @LINE:161
private[this] lazy val controllers_Spaces_updateExtractors98 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/extractors"))))
        

// @LINE:162
private[this] lazy val controllers_Spaces_inviteToSpace99 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/invite"))))
        

// @LINE:163
private[this] lazy val controllers_Metadata_getMetadataBySpace100 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:164
private[this] lazy val controllers_Spaces_submit101 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/submit"))))
        

// @LINE:165
private[this] lazy val controllers_Spaces_submitCopy102 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/submitCopy"))))
        

// @LINE:170
private[this] lazy val controllers_Geostreams_map103 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("geostreams"))))
        

// @LINE:171
private[this] lazy val controllers_Geostreams_list104 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("geostreams/sensors"))))
        

// @LINE:172
private[this] lazy val controllers_Geostreams_newSensor105 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("geostreams/sensors/new"))))
        

// @LINE:173
private[this] lazy val controllers_Geostreams_edit106 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("geostreams/sensors/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:178
private[this] lazy val controllers_Metadata_search107 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("metadata/search"))))
        

// @LINE:179
private[this] lazy val controllers_Metadata_view108 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("metadata/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:180
private[this] lazy val controllers_Metadata_file109 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files/"),DynamicPart("file_id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:181
private[this] lazy val controllers_Metadata_dataset110 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("datasets/"),DynamicPart("dataset_id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:183
private[this] lazy val controllers_Tags_search111 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("tags/search"))))
        

// @LINE:184
private[this] lazy val controllers_Tags_tagListOrdered112 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("tags/list/ordered"))))
        

// @LINE:185
private[this] lazy val controllers_Tags_tagListWeighted113 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("tags/list/weighted"))))
        

// @LINE:190
private[this] lazy val controllers_Search_search114 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("search"))))
        

// @LINE:191
private[this] lazy val controllers_Search_multimediasearch115 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("multimediasearch"))))
        

// @LINE:193
private[this] lazy val controllers_Search_advanced116 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("advanced"))))
        

// @LINE:194
private[this] lazy val controllers_Search_SearchByText117 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("SearchByText"))))
        

// @LINE:195
private[this] lazy val controllers_Search_uploadquery118 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("uploadquery"))))
        

// @LINE:196
private[this] lazy val controllers_Search_searchbyURL119 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("searchbyURL"))))
        

// @LINE:197
private[this] lazy val controllers_Search_callSearchMultimediaIndexView120 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("searchbyfeature/"),DynamicPart("section_id", """[^/]+""",true))))
        

// @LINE:198
private[this] lazy val controllers_Search_findSimilarToExistingFile121 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/similar"))))
        

// @LINE:199
private[this] lazy val controllers_Search_findSimilarToQueryFile122 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("queries/"),DynamicPart("id", """[^/]+""",true),StaticPart("/similar"))))
        

// @LINE:200
private[this] lazy val controllers_Search_findSimilarWeightedIndexes123 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("queries/similarWeightedIndexes"))))
        

// @LINE:205
private[this] lazy val controllers_DataAnalysis_listSessions124 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("analysisSessions"))))
        

// @LINE:208
private[this] lazy val controllers_DataAnalysis_terminate125 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("terminateSession/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:210
private[this] lazy val controllers_Notebook_listNotebooks126 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("notebooks"))))
        

// @LINE:211
private[this] lazy val controllers_Notebook_viewNotebook127 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("notebook/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:212
private[this] lazy val controllers_Notebook_editNotebook128 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("editNotebook/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:213
private[this] lazy val controllers_Notebook_deleteNotebook129 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("deleteNotebook/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:218
private[this] lazy val controllers_Previewers_list130 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("previewers/list"))))
        

// @LINE:226
private[this] lazy val securesocial_controllers_LoginPage_login131 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:227
private[this] lazy val securesocial_controllers_LoginPage_logout132 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("logout"))))
        

// @LINE:232
private[this] lazy val securesocial_controllers_Registration_startSignUp133 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("signup"))))
        

// @LINE:233
private[this] lazy val securesocial_controllers_Registration_handleStartSignUp134 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("signup"))))
        

// @LINE:234
private[this] lazy val securesocial_controllers_Registration_signUp135 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("signup/"),DynamicPart("token", """[^/]+""",true))))
        

// @LINE:235
private[this] lazy val controllers_Registration_handleSignUp136 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("signup/"),DynamicPart("token", """[^/]+""",true))))
        

// @LINE:236
private[this] lazy val securesocial_controllers_Registration_startResetPassword137 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("reset"))))
        

// @LINE:237
private[this] lazy val securesocial_controllers_Registration_handleStartResetPassword138 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("reset"))))
        

// @LINE:238
private[this] lazy val securesocial_controllers_Registration_resetPassword139 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("reset/"),DynamicPart("token", """[^/]+""",true))))
        

// @LINE:239
private[this] lazy val securesocial_controllers_Registration_handleResetPassword140 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("reset/"),DynamicPart("token", """[^/]+""",true))))
        

// @LINE:240
private[this] lazy val securesocial_controllers_PasswordChange_page141 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("password"))))
        

// @LINE:241
private[this] lazy val securesocial_controllers_PasswordChange_handlePasswordChange142 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("password"))))
        

// @LINE:246
private[this] lazy val securesocial_controllers_ProviderController_authenticate143 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("authenticate/"),DynamicPart("provider", """[^/]+""",true))))
        

// @LINE:247
private[this] lazy val securesocial_controllers_ProviderController_authenticateByPost144 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("authenticate/"),DynamicPart("provider", """[^/]+""",true))))
        

// @LINE:249
private[this] lazy val securesocial_controllers_ProviderController_notAuthorized145 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("not-authorized"))))
        

// @LINE:254
private[this] lazy val controllers_Admin_customize146 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/customize"))))
        

// @LINE:255
private[this] lazy val controllers_Admin_tos147 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/tos"))))
        

// @LINE:256
private[this] lazy val controllers_Admin_users148 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/users"))))
        

// @LINE:257
private[this] lazy val controllers_Admin_adminIndex149 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/indexAdmin"))))
        

// @LINE:258
private[this] lazy val controllers_Admin_reindexFiles150 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/reindexFiles"))))
        

// @LINE:259
private[this] lazy val controllers_Admin_getAdapters151 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/adapters"))))
        

// @LINE:260
private[this] lazy val controllers_Admin_getExtractors152 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/extractors"))))
        

// @LINE:261
private[this] lazy val controllers_Admin_getMeasures153 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/measures"))))
        

// @LINE:262
private[this] lazy val controllers_Admin_getIndexers154 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/indexers"))))
        

// @LINE:263
private[this] lazy val controllers_Admin_getIndexes155 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/indexes"))))
        

// @LINE:264
private[this] lazy val controllers_Admin_getSections156 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/sections"))))
        

// @LINE:265
private[this] lazy val controllers_Admin_getMetadataDefinitions157 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/metadata/definitions"))))
        

// @LINE:266
private[this] lazy val controllers_Admin_createIndex158 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/createIndex"))))
        

// @LINE:267
private[this] lazy val controllers_Admin_buildIndex159 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/index/"),DynamicPart("id", """[^/]+""",true),StaticPart("/build"))))
        

// @LINE:268
private[this] lazy val controllers_Admin_deleteIndex160 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/index/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:269
private[this] lazy val controllers_Admin_deleteAllIndexes161 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/index"))))
        

// @LINE:270
private[this] lazy val controllers_Admin_listRoles162 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/roles"))))
        

// @LINE:271
private[this] lazy val controllers_Admin_createRole163 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/roles/new"))))
        

// @LINE:272
private[this] lazy val controllers_Admin_submitCreateRole164 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/roles/submitNew"))))
        

// @LINE:273
private[this] lazy val controllers_Admin_removeRole165 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/roles/delete/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:274
private[this] lazy val controllers_Admin_editRole166 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/roles/"),DynamicPart("id", """[^/]+""",true),StaticPart("/edit"))))
        

// @LINE:275
private[this] lazy val controllers_Admin_updateRole167 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/roles/update"))))
        

// @LINE:276
private[this] lazy val controllers_Extractors_listAllExtractions168 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/extractions"))))
        

// @LINE:277
private[this] lazy val controllers_Admin_viewDumpers169 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/dataDumps"))))
        

// @LINE:278
private[this] lazy val controllers_Admin_sensors170 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/sensors"))))
        

// @LINE:279
private[this] lazy val api_Admin_sensorsConfig171 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sensors/config"))))
        

// @LINE:284
private[this] lazy val controllers_Application_javascriptRoutes172 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("javascriptRoutes"))))
        

// @LINE:289
private[this] lazy val controllers_Application_swagger173 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("swagger"))))
        

// @LINE:290
private[this] lazy val controllers_Application_swaggerUI174 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("swaggerUI"))))
        

// @LINE:299
private[this] lazy val api_Admin_mail175 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/admin/mail"))))
        

// @LINE:300
private[this] lazy val api_Admin_emailZipUpload176 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/admin/mailZip"))))
        

// @LINE:301
private[this] lazy val api_Admin_users177 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/admin/users"))))
        

// @LINE:302
private[this] lazy val api_Admin_sensorsConfig178 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sensors/config"))))
        

// @LINE:303
private[this] lazy val api_Admin_deleteAllData179 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/delete-data"))))
        

// @LINE:304
private[this] lazy val api_Admin_submitAppearance180 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/changeAppearance"))))
        

// @LINE:305
private[this] lazy val api_Admin_reindex181 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/reindex"))))
        

// @LINE:306
private[this] lazy val api_Admin_updateConfiguration182 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/admin/configuration"))))
        

// @LINE:311
private[this] lazy val api_ContextLD_addContext183 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/contexts"))))
        

// @LINE:312
private[this] lazy val api_ContextLD_getContextById184 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/contexts/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:313
private[this] lazy val api_ContextLD_getContextByName185 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/contexts/"),DynamicPart("name", """[^/]+""",true),StaticPart("/context.json"))))
        

// @LINE:314
private[this] lazy val api_ContextLD_removeById186 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/contexts/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:317
private[this] lazy val api_Datasets_addMetadataJsonLD187 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata.jsonld"))))
        

// @LINE:318
private[this] lazy val api_Datasets_getMetadataJsonLD188 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata.jsonld"))))
        

// @LINE:319
private[this] lazy val api_Datasets_removeMetadataJsonLD189 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata.jsonld"))))
        

// @LINE:320
private[this] lazy val api_Files_addMetadataJsonLD190 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata.jsonld"))))
        

// @LINE:321
private[this] lazy val api_Files_getMetadataJsonLD191 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata.jsonld"))))
        

// @LINE:322
private[this] lazy val api_Files_removeMetadataJsonLD192 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata.jsonld"))))
        

// @LINE:324
private[this] lazy val api_Metadata_addUserMetadata193 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata.jsonld"))))
        

// @LINE:325
private[this] lazy val api_Metadata_removeMetadata194 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata.jsonld/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:329
private[this] lazy val api_Metadata_getDefinitions195 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata/definitions"))))
        

// @LINE:330
private[this] lazy val api_Metadata_getDefinitionsDistinctName196 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata/distinctdefinitions"))))
        

// @LINE:331
private[this] lazy val api_Metadata_getAutocompleteName197 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata/autocompletenames"))))
        

// @LINE:332
private[this] lazy val api_Metadata_getDefinition198 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata/definitions/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:333
private[this] lazy val api_Metadata_getUrl199 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata/url/"),DynamicPart("in_url", """.+""",false))))
        

// @LINE:334
private[this] lazy val api_Metadata_editDefinition200 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata/definitions/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:335
private[this] lazy val api_Metadata_addDefinition201 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata/definitions"))))
        

// @LINE:336
private[this] lazy val api_Metadata_deleteDefinition202 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/metadata/definitions/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:338
private[this] lazy val api_Metadata_listPeople203 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata/people"))))
        

// @LINE:339
private[this] lazy val api_Metadata_getPerson204 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata/people/"),DynamicPart("pid", """[^/]+""",true))))
        

// @LINE:341
private[this] lazy val api_Metadata_getRepository205 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/metadata/repository/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:345
private[this] lazy val api_Logos_list206 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/logos"))))
        

// @LINE:346
private[this] lazy val api_Logos_upload207 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/logos"))))
        

// @LINE:347
private[this] lazy val api_Logos_getId208 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/logos/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:348
private[this] lazy val api_Logos_putId209 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/logos/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:349
private[this] lazy val api_Logos_downloadId210 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/logos/"),DynamicPart("id", """[^/]+""",true),StaticPart("/blob"))))
        

// @LINE:350
private[this] lazy val api_Logos_deleteId211 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/logos/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:351
private[this] lazy val api_Logos_getPath212 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/logos/"),DynamicPart("path", """[^/]+""",true),StaticPart("/"),DynamicPart("name", """[^/]+""",true))))
        

// @LINE:352
private[this] lazy val api_Logos_putPath213 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/logos/"),DynamicPart("path", """[^/]+""",true),StaticPart("/"),DynamicPart("name", """[^/]+""",true))))
        

// @LINE:353
private[this] lazy val api_Logos_downloadPath214 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/logos/"),DynamicPart("path", """[^/]+""",true),StaticPart("/"),DynamicPart("name", """[^/]+""",true),StaticPart("/blob"))))
        

// @LINE:354
private[this] lazy val api_Logos_deletePath215 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/logos/"),DynamicPart("path", """[^/]+""",true),StaticPart("/"),DynamicPart("name", """[^/]+""",true))))
        

// @LINE:359
private[this] lazy val api_Files_attachGeometry216 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("three_d_file_id", """[^/]+""",true),StaticPart("/geometries/"),DynamicPart("geometry_id", """[^/]+""",true))))
        

// @LINE:360
private[this] lazy val api_Files_attachTexture217 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("three_d_file_id", """[^/]+""",true),StaticPart("/3dTextures/"),DynamicPart("texture_id", """[^/]+""",true))))
        

// @LINE:361
private[this] lazy val api_Files_attachThumbnail218 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("file_id", """[^/]+""",true),StaticPart("/thumbnails/"),DynamicPart("thumbnail_id", """[^/]+""",true))))
        

// @LINE:362
private[this] lazy val api_Files_attachQueryThumbnail219 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/queries/"),DynamicPart("file_id", """[^/]+""",true),StaticPart("/thumbnails/"),DynamicPart("thumbnail_id", """[^/]+""",true))))
        

// @LINE:363
private[this] lazy val api_Files_list220 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files"))))
        

// @LINE:364
private[this] lazy val api_Files_upload221 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files"))))
        

// @LINE:365
private[this] lazy val api_Files_upload222 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/withFlags/"),DynamicPart("flags", """[^/]+""",true))))
        

// @LINE:366
private[this] lazy val api_Files_searchFilesUserMetadata223 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/searchusermetadata"))))
        

// @LINE:367
private[this] lazy val api_Files_searchFilesGeneralMetadata224 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/searchmetadata"))))
        

// @LINE:368
private[this] lazy val api_Files_uploadToDataset225 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/uploadToDataset/withFlags/"),DynamicPart("id", """[^/]+""",true),StaticPart("/"),DynamicPart("flags", """[^/]+""",true))))
        

// @LINE:369
private[this] lazy val api_Files_uploadToDataset226 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/uploadToDataset/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:370
private[this] lazy val api_Files_updateDescription227 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/updateDescription"))))
        

// @LINE:371
private[this] lazy val api_Files_uploadIntermediate228 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/uploadIntermediate/"),DynamicPart("idAndFlags", """[^/]+""",true))))
        

// @LINE:372
private[this] lazy val api_Files_sendJob229 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/sendJob/"),DynamicPart("fileId", """[^/]+""",true),StaticPart("/"),DynamicPart("fileType", """[^/]+""",true))))
        

// @LINE:373
private[this] lazy val api_Files_getRDFURLsForFile230 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/getRDFURLsForFile/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:374
private[this] lazy val api_Files_getRDFUserMetadata231 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/rdfUserMetadata/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:375
private[this] lazy val api_Files_download232 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/blob"))))
        

// @LINE:376
private[this] lazy val api_Files_removeFile233 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/remove"))))
        

// @LINE:377
private[this] lazy val api_Files_get234 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:378
private[this] lazy val api_Files_addMetadata235 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:379
private[this] lazy val api_Files_getMetadataDefinitions236 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadataDefinitions"))))
        

// @LINE:380
private[this] lazy val api_Files_updateMetadata237 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/updateMetadata"))))
        

// @LINE:381
private[this] lazy val api_Files_addVersusMetadata238 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/versus_metadata"))))
        

// @LINE:382
private[this] lazy val api_Files_getVersusMetadataJSON239 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/versus_metadata"))))
        

// @LINE:383
private[this] lazy val api_Files_getUserMetadataJSON240 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/usermetadatajson"))))
        

// @LINE:384
private[this] lazy val api_Files_addUserMetadata241 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/usermetadata"))))
        

// @LINE:385
private[this] lazy val api_Files_getTechnicalMetadataJSON242 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/technicalmetadatajson"))))
        

// @LINE:386
private[this] lazy val api_Files_getXMLMetadataJSON243 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/xmlmetadatajson"))))
        

// @LINE:387
private[this] lazy val api_Files_follow244 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/follow"))))
        

// @LINE:388
private[this] lazy val api_Files_unfollow245 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/unfollow"))))
        

// @LINE:389
private[this] lazy val api_Files_comment246 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/comment"))))
        

// @LINE:390
private[this] lazy val api_Files_removeFile247 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:391
private[this] lazy val api_Files_getTags248 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags"))))
        

// @LINE:392
private[this] lazy val api_Files_addTags249 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags"))))
        

// @LINE:393
private[this] lazy val api_Files_removeTags250 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags/remove"))))
        

// @LINE:394
private[this] lazy val api_Files_removeAllTags251 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags/remove_all"))))
        

// @LINE:395
private[this] lazy val api_Files_removeTags252 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags"))))
        

// @LINE:396
private[this] lazy val api_Files_updateLicense253 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/license"))))
        

// @LINE:397
private[this] lazy val api_Files_extract254 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/extracted_metadata"))))
        

// @LINE:398
private[this] lazy val api_Files_updateFileName255 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/filename"))))
        

// @LINE:399
private[this] lazy val api_Files_reindex256 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/reindex"))))
        

// @LINE:400
private[this] lazy val api_Files_users257 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/users"))))
        

// @LINE:401
private[this] lazy val api_Files_paths258 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/paths"))))
        

// @LINE:402
private[this] lazy val api_Files_changeOwner259 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/changeOwner/"),DynamicPart("ownerId", """[^/]+""",true))))
        

// @LINE:403
private[this] lazy val api_Files_getOwner260 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/getOwner"))))
        

// @LINE:407
private[this] lazy val api_Extractions_listExtractors261 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractors"))))
        

// @LINE:408
private[this] lazy val api_Extractions_getExtractorInfo262 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractors/"),DynamicPart("name", """[^/]+""",true))))
        

// @LINE:410
private[this] lazy val api_Extractions_addExtractorInfo263 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractors"))))
        

// @LINE:412
private[this] lazy val api_Extractions_submitFileToExtractor264 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("file_id", """[^/]+""",true),StaticPart("/extractions"))))
        

// @LINE:413
private[this] lazy val api_Extractions_submitDatasetToExtractor265 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("ds_id", """[^/]+""",true),StaticPart("/extractions"))))
        

// @LINE:415
private[this] lazy val api_Extractions_getDTSRequests266 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/requests"))))
        

// @LINE:416
private[this] lazy val api_Extractions_getExtractorServersIP267 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/servers_ips"))))
        

// @LINE:417
private[this] lazy val api_Extractions_getExtractorNames268 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/extractors_names"))))
        

// @LINE:418
private[this] lazy val api_Extractions_getExtractorInputTypes269 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/supported_input_types"))))
        

// @LINE:420
private[this] lazy val api_Extractions_getExtractorDetails270 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/extractors_details"))))
        

// @LINE:421
private[this] lazy val api_Extractions_uploadByURL271 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/upload_url"))))
        

// @LINE:422
private[this] lazy val api_Extractions_multipleUploadByURL272 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/multiple_uploadby_url"))))
        

// @LINE:423
private[this] lazy val api_Extractions_uploadExtract273 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/upload_file"))))
        

// @LINE:424
private[this] lazy val api_Extractions_checkExtractorsStatus274 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/"),DynamicPart("id", """[^/]+""",true),StaticPart("/status"))))
        

// @LINE:425
private[this] lazy val api_Extractions_fetch275 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:426
private[this] lazy val api_Extractions_checkExtractionsStatuses276 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/extractions/"),DynamicPart("id", """[^/]+""",true),StaticPart("/statuses"))))
        

// @LINE:432
private[this] lazy val api_Files_attachPreview277 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/previews/"),DynamicPart("p_id", """[^/]+""",true))))
        

// @LINE:433
private[this] lazy val api_Files_getWithPreviewUrls278 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/previewJson/"),DynamicPart("file", """[^/]+""",true))))
        

// @LINE:434
private[this] lazy val api_Files_filePreviewsList279 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/listpreviews"))))
        

// @LINE:435
private[this] lazy val api_Files_getPreviews280 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/getPreviews"))))
        

// @LINE:436
private[this] lazy val api_Files_isBeingProcessed281 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/isBeingProcessed"))))
        

// @LINE:438
private[this] lazy val api_Files_getTexture282 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("three_d_file_id", """[^/]+""",true),StaticPart("/"),DynamicPart("filename", """[^/]+""",true))))
        

// @LINE:439
private[this] lazy val api_Files_downloadquery283 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/queries/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:441
private[this] lazy val api_Files_download284 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:446
private[this] lazy val api_Files_dumpFilesMetadata285 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/dumpFilesMd"))))
        

// @LINE:447
private[this] lazy val api_Datasets_dumpDatasetsMetadata286 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/dumpDatasetsMd"))))
        

// @LINE:448
private[this] lazy val api_Datasets_dumpDatasetGroupings287 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/dumpDatasetGroupings"))))
        

// @LINE:454
private[this] lazy val api_Files_getTexture288 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/files/"),DynamicPart("three_d_file_id", """[^/]+""",true),StaticPart("/"),DynamicPart("filename", """[^/]+""",true))))
        

// @LINE:460
private[this] lazy val api_Spaces_list289 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces"))))
        

// @LINE:461
private[this] lazy val api_Spaces_listCanEdit290 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/canEdit"))))
        

// @LINE:462
private[this] lazy val api_Spaces_get291 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:463
private[this] lazy val api_Spaces_createSpace292 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces"))))
        

// @LINE:464
private[this] lazy val api_Spaces_removeSpace293 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:465
private[this] lazy val api_Spaces_removeCollection294 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("spaceId", """[^/]+""",true),StaticPart("/removeCollection/"),DynamicPart("collectionId", """[^/]+""",true))))
        

// @LINE:466
private[this] lazy val api_Spaces_removeDataset295 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("spaceId", """[^/]+""",true),StaticPart("/removeDataset/"),DynamicPart("datasetId", """[^/]+""",true))))
        

// @LINE:467
private[this] lazy val api_Spaces_copyContentsToSpace296 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("spaceId", """[^/]+""",true),StaticPart("/copyContentsToSpace/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:468
private[this] lazy val api_Spaces_copyContentsToNewSpace297 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("spaceId", """[^/]+""",true),StaticPart("/copyContentsToNewSpace"))))
        

// @LINE:469
private[this] lazy val api_Spaces_updateSpace298 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/update"))))
        

// @LINE:470
private[this] lazy val api_Spaces_updateUsers299 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/updateUsers"))))
        

// @LINE:471
private[this] lazy val api_Spaces_acceptRequest300 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/acceptRequest"))))
        

// @LINE:472
private[this] lazy val api_Spaces_rejectRequest301 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/rejectRequest"))))
        

// @LINE:473
private[this] lazy val api_Spaces_removeUser302 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/removeUser"))))
        

// @LINE:474
private[this] lazy val api_Spaces_addDatasetToSpace303 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("spaceId", """[^/]+""",true),StaticPart("/addDatasetToSpace/"),DynamicPart("datasetId", """[^/]+""",true))))
        

// @LINE:475
private[this] lazy val api_Spaces_addCollectionToSpace304 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("spaceId", """[^/]+""",true),StaticPart("/addCollectionToSpace/"),DynamicPart("collectionId", """[^/]+""",true))))
        

// @LINE:476
private[this] lazy val api_Spaces_follow305 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/follow"))))
        

// @LINE:477
private[this] lazy val api_Spaces_unfollow306 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/unfollow"))))
        

// @LINE:478
private[this] lazy val api_Metadata_addDefinitionToSpace307 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:479
private[this] lazy val api_Spaces_verifySpace308 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/verify"))))
        

// @LINE:480
private[this] lazy val api_Spaces_listDatasets309 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/datasets"))))
        

// @LINE:481
private[this] lazy val api_Spaces_listCollections310 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/collections"))))
        

// @LINE:482
private[this] lazy val api_Spaces_listCollectionsCanEdit311 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/collectionsCanEdit"))))
        

// @LINE:483
private[this] lazy val api_Spaces_getUserSpaceRoleMap312 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/userSpaceRoleMap"))))
        

// @LINE:488
private[this] lazy val api_Collections_list313 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections"))))
        

// @LINE:489
private[this] lazy val api_Collections_listCanEdit314 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/canEdit"))))
        

// @LINE:490
private[this] lazy val api_Collections_addDatasetToCollectionOptions315 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/datasetPossibleParents/"),DynamicPart("ds_id", """[^/]+""",true))))
        

// @LINE:491
private[this] lazy val api_Collections_listPossibleParents316 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/possibleParents"))))
        

// @LINE:492
private[this] lazy val api_Collections_createCollection317 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections"))))
        

// @LINE:493
private[this] lazy val api_Collections_getRootCollections318 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/rootCollections"))))
        

// @LINE:494
private[this] lazy val api_Collections_getTopLevelCollections319 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/topLevelCollections"))))
        

// @LINE:495
private[this] lazy val api_Collections_getAllCollections320 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/allCollections"))))
        

// @LINE:496
private[this] lazy val api_Collections_download321 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/download"))))
        

// @LINE:497
private[this] lazy val api_Collections_listCollectionsInTrash322 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/listTrash"))))
        

// @LINE:498
private[this] lazy val api_Collections_emptyTrash323 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/emptyTrash"))))
        

// @LINE:499
private[this] lazy val api_Collections_clearOldCollectionsTrash324 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/clearOldCollectionsTrash"))))
        

// @LINE:500
private[this] lazy val api_Collections_restoreCollection325 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/restore/"),DynamicPart("collectionId", """[^/]+""",true))))
        

// @LINE:511
private[this] lazy val api_Collections_uploadZipToSpace326 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/uploadZipToSpace"))))
        

// @LINE:516
private[this] lazy val api_Collections_moveChildCollection327 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/moveChildCollection"))))
        

// @LINE:517
private[this] lazy val api_Collections_removeCollectionAndContents328 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("collectionId", """[^/]+""",true),StaticPart("/deleteCollectionAndContents"))))
        

// @LINE:518
private[this] lazy val api_Collections_moveDatasetToNewCollection329 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("oldCollection", """[^/]+""",true),StaticPart("/moveDataset/"),DynamicPart("dataset", """[^/]+""",true),StaticPart("/newCollection/"),DynamicPart("newCollection", """[^/]+""",true))))
        

// @LINE:520
private[this] lazy val api_Collections_list330 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/list"))))
        

// @LINE:521
private[this] lazy val api_Collections_follow331 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/follow"))))
        

// @LINE:522
private[this] lazy val api_Collections_unfollow332 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/unfollow"))))
        

// @LINE:523
private[this] lazy val api_Datasets_listInCollection333 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/datasets"))))
        

// @LINE:524
private[this] lazy val api_Collections_attachDataset334 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/datasets/"),DynamicPart("ds_id", """[^/]+""",true))))
        

// @LINE:526
private[this] lazy val api_Collections_removeDataset335 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/datasetsRemove/"),DynamicPart("ds_id", """[^/]+""",true),StaticPart("/"),DynamicPart("ignoreNotFound", """[^/]+""",true))))
        

// @LINE:527
private[this] lazy val api_Collections_removeDataset336 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/datasets/"),DynamicPart("ds_id", """[^/]+""",true))))
        

// @LINE:529
private[this] lazy val api_Collections_removeCollection337 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/remove"))))
        

// @LINE:530
private[this] lazy val api_Collections_reindex338 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/reindex"))))
        

// @LINE:532
private[this] lazy val api_Datasets_listInCollection339 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/getDatasets"))))
        

// @LINE:533
private[this] lazy val api_Collections_removeCollection340 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true))))
        

// @LINE:535
private[this] lazy val api_Collections_deleteCollectionAndContents341 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/deleteContents/"),DynamicPart("userid", """[^/]+""",true))))
        

// @LINE:536
private[this] lazy val api_Collections_attachPreview342 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("c_id", """[^/]+""",true),StaticPart("/previews/"),DynamicPart("p_id", """[^/]+""",true))))
        

// @LINE:537
private[this] lazy val api_Collections_getCollection343 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true))))
        

// @LINE:538
private[this] lazy val api_Collections_updateCollectionName344 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/title"))))
        

// @LINE:539
private[this] lazy val api_Collections_updateCollectionDescription345 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/description"))))
        

// @LINE:541
private[this] lazy val api_Collections_removeSubCollection346 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/removeSubCollection/"),DynamicPart("sub_coll_id", """[^/]+""",true))))
        

// @LINE:542
private[this] lazy val api_Collections_setRootSpace347 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/rootFlag/"),DynamicPart("spaceId", """[^/]+""",true))))
        

// @LINE:543
private[this] lazy val api_Collections_unsetRootSpace348 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/unsetRootFlag/"),DynamicPart("spaceId", """[^/]+""",true))))
        

// @LINE:544
private[this] lazy val api_Collections_createCollectionWithParent349 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/newCollectionWithParent"))))
        

// @LINE:545
private[this] lazy val api_Collections_getChildCollectionIds350 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/getChildCollectionIds"))))
        

// @LINE:546
private[this] lazy val api_Collections_getChildCollections351 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/getChildCollections"))))
        

// @LINE:547
private[this] lazy val api_Collections_getParentCollectionIds352 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/getParentCollectionIds"))))
        

// @LINE:548
private[this] lazy val api_Collections_getParentCollections353 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/getParentCollections"))))
        

// @LINE:549
private[this] lazy val api_Collections_attachSubCollection354 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/addSubCollection/"),DynamicPart("sub_coll_id", """[^/]+""",true))))
        

// @LINE:550
private[this] lazy val api_Collections_removeFromSpaceAllowed355 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/removeFromSpaceAllowed/"),DynamicPart("space_id", """[^/]+""",true))))
        

// @LINE:551
private[this] lazy val api_Collections_changeOwner356 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/changeOwner/"),DynamicPart("userId", """[^/]+""",true))))
        

// @LINE:552
private[this] lazy val api_Collections_changeOwnerCollectionOnly357 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/changeOwnerCollection/"),DynamicPart("userId", """[^/]+""",true))))
        

// @LINE:553
private[this] lazy val api_Collections_changeOwnerDatasetOnly358 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("ds_id", """[^/]+""",true),StaticPart("/changeOwnerDataset/"),DynamicPart("userId", """[^/]+""",true))))
        

// @LINE:554
private[this] lazy val api_Collections_changeOwnerFileOnly359 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/collections/"),DynamicPart("file_id", """[^/]+""",true),StaticPart("/changeOwnerFile/"),DynamicPart("userId", """[^/]+""",true))))
        

// @LINE:560
private[this] lazy val api_Datasets_list360 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets"))))
        

// @LINE:561
private[this] lazy val api_Datasets_listCanEdit361 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/canEdit"))))
        

// @LINE:562
private[this] lazy val api_Datasets_listMoveFileToDataset362 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/moveFileToDataset"))))
        

// @LINE:563
private[this] lazy val api_Datasets_createDataset363 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets"))))
        

// @LINE:564
private[this] lazy val api_Datasets_createEmptyVocabularyForDataset364 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/newVocabulary/"),DynamicPart("datasetId", """[^/]+""",true))))
        

// @LINE:565
private[this] lazy val api_Datasets_markDatasetAsTrash365 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/trash/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:566
private[this] lazy val api_Datasets_restoreDataset366 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/restore/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:568
private[this] lazy val api_Datasets_emptyTrash367 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/emptytrash"))))
        

// @LINE:569
private[this] lazy val api_Datasets_createEmptyDataset368 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/createempty"))))
        

// @LINE:570
private[this] lazy val api_Datasets_attachMultipleFiles369 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/attachmultiple"))))
        

// @LINE:571
private[this] lazy val api_Datasets_searchDatasetsUserMetadata370 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/searchusermetadata"))))
        

// @LINE:572
private[this] lazy val api_Datasets_searchDatasetsGeneralMetadata371 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/searchmetadata"))))
        

// @LINE:573
private[this] lazy val api_Datasets_listOutsideCollection372 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/listOutsideCollection/"),DynamicPart("coll_id", """[^/]+""",true))))
        

// @LINE:574
private[this] lazy val api_Datasets_listDatasetsInTrash373 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/listTrash"))))
        

// @LINE:575
private[this] lazy val api_Datasets_emptyTrash374 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/emptyTrash"))))
        

// @LINE:576
private[this] lazy val api_Datasets_clearOldDatasetsTrash375 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/clearOldDatasetsTrash"))))
        

// @LINE:577
private[this] lazy val api_Datasets_restoreDataset376 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/restore/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:578
private[this] lazy val api_Datasets_detachFile377 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("ds_id", """[^/]+""",true),StaticPart("/filesRemove/"),DynamicPart("file_id", """[^/]+""",true),StaticPart("/"),DynamicPart("ignoreNotFound", """[^/]+""",true))))
        

// @LINE:579
private[this] lazy val api_Folders_moveFileBetweenFolders378 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("datasetId", """[^/]+""",true),StaticPart("/moveFile/"),DynamicPart("toFolder", """[^/]+""",true),StaticPart("/"),DynamicPart("fileId", """[^/]+""",true))))
        

// @LINE:580
private[this] lazy val api_Datasets_moveFileBetweenDatasets379 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("datasetId", """[^/]+""",true),StaticPart("/moveBetweenDatasets/"),DynamicPart("toDataset", """[^/]+""",true),StaticPart("/"),DynamicPart("fileId", """[^/]+""",true))))
        

// @LINE:581
private[this] lazy val api_Folders_moveFileToDataset380 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("datasetId", """[^/]+""",true),StaticPart("/moveToDataset/"),DynamicPart("fromFolder", """[^/]+""",true),StaticPart("/"),DynamicPart("fileId", """[^/]+""",true))))
        

// @LINE:582
private[this] lazy val api_Folders_deleteFolder381 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("parentDatasetId", """[^/]+""",true),StaticPart("/deleteFolder/"),DynamicPart("folderId", """[^/]+""",true))))
        

// @LINE:583
private[this] lazy val api_Folders_updateFolderName382 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("parentDatasetId", """[^/]+""",true),StaticPart("/updateName/"),DynamicPart("folderId", """[^/]+""",true))))
        

// @LINE:584
private[this] lazy val api_Datasets_copyDatasetToSpace383 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("datasetId", """[^/]+""",true),StaticPart("/copyDatasetToSpace/"),DynamicPart("spaceId", """[^/]+""",true))))
        

// @LINE:585
private[this] lazy val api_Datasets_copyDatasetToSpaceNotAuthor384 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("datasetId", """[^/]+""",true),StaticPart("/copyDatasetToSpaceNotAuthor/"),DynamicPart("spaceId", """[^/]+""",true))))
        

// @LINE:586
private[this] lazy val api_Folders_getAllFoldersByDatasetId385 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("datasetId", """[^/]+""",true),StaticPart("/folders"))))
        

// @LINE:587
private[this] lazy val api_Datasets_detachFile386 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("ds_id", """[^/]+""",true),StaticPart("/"),DynamicPart("file_id", """[^/]+""",true))))
        

// @LINE:588
private[this] lazy val api_Datasets_getRDFURLsForDataset387 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/getRDFURLsForDataset/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:589
private[this] lazy val api_Datasets_getRDFUserMetadata388 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/rdfUserMetadata/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:590
private[this] lazy val api_Datasets_getMetadataDefinitions389 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:591
private[this] lazy val api_Datasets_addMetadata390 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:592
private[this] lazy val api_Datasets_addUserMetadata391 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/usermetadata"))))
        

// @LINE:593
private[this] lazy val api_Datasets_getTechnicalMetadataJSON392 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/technicalmetadatajson"))))
        

// @LINE:594
private[this] lazy val api_Datasets_getXMLMetadataJSON393 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/xmlmetadatajson"))))
        

// @LINE:595
private[this] lazy val api_Datasets_getUserMetadataJSON394 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/usermetadatajson"))))
        

// @LINE:596
private[this] lazy val api_Datasets_datasetFilesList395 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/listFiles"))))
        

// @LINE:597
private[this] lazy val api_Datasets_datasetAllFilesList396 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/listAllFiles"))))
        

// @LINE:598
private[this] lazy val api_Datasets_datasetAllFilesList397 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/files"))))
        

// @LINE:599
private[this] lazy val api_Datasets_uploadToDatasetFile398 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/files"))))
        

// @LINE:600
private[this] lazy val api_Datasets_uploadToDatasetJSON399 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/urls"))))
        

// @LINE:601
private[this] lazy val api_Datasets_download400 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/download"))))
        

// @LINE:602
private[this] lazy val api_Datasets_comment401 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/comment"))))
        

// @LINE:603
private[this] lazy val api_Datasets_reindex402 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/reindex"))))
        

// @LINE:604
private[this] lazy val api_Datasets_follow403 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/follow"))))
        

// @LINE:605
private[this] lazy val api_Datasets_unfollow404 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/unfollow"))))
        

// @LINE:606
private[this] lazy val api_Datasets_removeTag405 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/removeTag"))))
        

// @LINE:607
private[this] lazy val api_Datasets_getTags406 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags"))))
        

// @LINE:608
private[this] lazy val api_Datasets_addTags407 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags"))))
        

// @LINE:609
private[this] lazy val api_Datasets_removeTags408 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags/remove"))))
        

// @LINE:610
private[this] lazy val api_Datasets_removeAllTags409 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags/remove_all"))))
        

// @LINE:611
private[this] lazy val api_Datasets_removeTags410 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags"))))
        

// @LINE:612
private[this] lazy val api_Datasets_updateName411 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/title"))))
        

// @LINE:613
private[this] lazy val api_Datasets_updateDescription412 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/description"))))
        

// @LINE:614
private[this] lazy val api_Datasets_addCreator413 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/creator"))))
        

// @LINE:615
private[this] lazy val api_Datasets_removeCreator414 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/creator/remove"))))
        

// @LINE:616
private[this] lazy val api_Datasets_moveCreator415 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/creator/reorder"))))
        

// @LINE:617
private[this] lazy val api_Datasets_updateInformation416 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/editing"))))
        

// @LINE:618
private[this] lazy val api_Datasets_updateLicense417 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/license"))))
        

// @LINE:619
private[this] lazy val api_Datasets_isBeingProcessed418 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/isBeingProcessed"))))
        

// @LINE:620
private[this] lazy val api_Datasets_getPreviews419 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/getPreviews"))))
        

// @LINE:621
private[this] lazy val api_Datasets_attachExistingFile420 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("ds_id", """[^/]+""",true),StaticPart("/files/"),DynamicPart("file_id", """[^/]+""",true))))
        

// @LINE:622
private[this] lazy val api_Datasets_get421 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:623
private[this] lazy val api_Datasets_deleteDataset422 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:624
private[this] lazy val api_Datasets_detachAndDeleteDataset423 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/detachdelete"))))
        

// @LINE:625
private[this] lazy val api_Folders_createFolder424 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("parentDatasetId", """[^/]+""",true),StaticPart("/newFolder"))))
        

// @LINE:626
private[this] lazy val api_Datasets_updateAccess425 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/access"))))
        

// @LINE:627
private[this] lazy val api_Datasets_addFileEvent426 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/addFileEvent"))))
        

// @LINE:628
private[this] lazy val api_Datasets_users427 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/users"))))
        

// @LINE:629
private[this] lazy val api_Datasets_changeOwner428 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("datasetId", """[^/]+""",true),StaticPart("/changeOwner/"),DynamicPart("userId", """[^/]+""",true))))
        

// @LINE:630
private[this] lazy val api_Datasets_moveDataset429 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/"),DynamicPart("id", """[^/]+""",true),StaticPart("/moveCollection"))))
        

// @LINE:631
private[this] lazy val api_Datasets_hasAttachedVocabulary430 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/datasets/hasVocabulary/"),DynamicPart("datasetId", """[^/]+""",true))))
        

// @LINE:637
private[this] lazy val controllers_Vocabularies_newVocabulary431 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("vocabularies/new"))))
        

// @LINE:638
private[this] lazy val controllers_Vocabularies_submit432 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("vocabularies/submit"))))
        

// @LINE:639
private[this] lazy val controllers_Vocabularies_vocabulary433 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("vocabularies/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:644
private[this] lazy val api_Previews_downloadPreview434 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("preview_id", """[^/]+""",true),StaticPart("/textures/dataset/"),DynamicPart("datasetid", """[^/]+""",true),StaticPart("/json"))))
        

// @LINE:645
private[this] lazy val api_Files_downloadByDatasetAndFilename435 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("preview_id", """[^/]+""",true),StaticPart("/textures/dataset/"),DynamicPart("dataset_id", """[^/]+""",true),StaticPart("//"),DynamicPart("filename", """[^/]+""",true))))
        

// @LINE:646
private[this] lazy val api_Previews_getTile436 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("dzi_id_dir", """[^/]+""",true),StaticPart("/"),DynamicPart("level", """[^/]+""",true),StaticPart("/"),DynamicPart("filename", """[^/]+""",true))))
        

// @LINE:647
private[this] lazy val api_Previews_attachTile437 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("dzi_id", """[^/]+""",true),StaticPart("/tiles/"),DynamicPart("tile_id", """[^/]+""",true),StaticPart("/"),DynamicPart("level", """[^/]+""",true))))
        

// @LINE:648
private[this] lazy val api_Previews_uploadMetadata438 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:649
private[this] lazy val api_Previews_getMetadata439 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadata"))))
        

// @LINE:650
private[this] lazy val api_Previews_attachAnnotation440 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("id", """[^/]+""",true),StaticPart("/annotationAdd"))))
        

// @LINE:651
private[this] lazy val api_Previews_editAnnotation441 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("id", """[^/]+""",true),StaticPart("/annotationEdit"))))
        

// @LINE:652
private[this] lazy val api_Previews_listAnnotations442 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("id", """[^/]+""",true),StaticPart("/annotationsList"))))
        

// @LINE:653
private[this] lazy val api_Previews_setTitle443 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("id", """[^/]+""",true),StaticPart("/title"))))
        

// @LINE:654
private[this] lazy val api_Previews_list444 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews"))))
        

// @LINE:655
private[this] lazy val api_Previews_download445 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:656
private[this] lazy val api_Previews_removePreview446 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:657
private[this] lazy val api_Previews_upload447 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/previews"))))
        

// @LINE:658
private[this] lazy val api_Indexes_index448 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/indexes"))))
        

// @LINE:659
private[this] lazy val api_Indexes_features449 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/indexes/features"))))
        

// @LINE:664
private[this] lazy val api_Sections_add450 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections"))))
        

// @LINE:665
private[this] lazy val api_Sections_comment451 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/comments"))))
        

// @LINE:666
private[this] lazy val api_Sections_description452 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/description"))))
        

// @LINE:667
private[this] lazy val api_Sections_attachThumbnail453 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/thumbnails/"),DynamicPart("thumbnail_id", """[^/]+""",true))))
        

// @LINE:668
private[this] lazy val api_Sections_getTags454 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags"))))
        

// @LINE:669
private[this] lazy val api_Sections_addTags455 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags"))))
        

// @LINE:670
private[this] lazy val api_Sections_removeTags456 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags/remove"))))
        

// @LINE:671
private[this] lazy val api_Sections_removeAllTags457 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags/remove_all"))))
        

// @LINE:672
private[this] lazy val api_Sections_removeTags458 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections/"),DynamicPart("id", """[^/]+""",true),StaticPart("/tags"))))
        

// @LINE:673
private[this] lazy val api_Sections_get459 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:674
private[this] lazy val api_Sections_delete460 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sections/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:679
private[this] lazy val api_Search_searchJson461 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/search/json"))))
        

// @LINE:680
private[this] lazy val api_Search_searchMultimediaIndex462 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/search/multimediasearch"))))
        

// @LINE:681
private[this] lazy val api_Search_search463 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/search"))))
        

// @LINE:686
private[this] lazy val api_Geostreams_addDatapoint464 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/datapoints"))))
        

// @LINE:687
private[this] lazy val api_Geostreams_addDatapoints465 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/datapoints/bulk"))))
        

// @LINE:688
private[this] lazy val api_Geostreams_deleteDatapoint466 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/datapoints/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:689
private[this] lazy val api_Geostreams_searchDatapoints467 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/datapoints"))))
        

// @LINE:690
private[this] lazy val api_Geostreams_searchDatapoints468 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/datapoints/averages"))))
        

// @LINE:691
private[this] lazy val api_Geostreams_searchDatapoints469 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/datapoints/trends"))))
        

// @LINE:692
private[this] lazy val api_Geostreams_binDatapoints470 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/datapoints/bin/"),DynamicPart("time", """[^/]+""",true),StaticPart("/"),DynamicPart("depth", """[^/]+""",true))))
        

// @LINE:693
private[this] lazy val api_Geostreams_getDatapoint471 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/datapoints/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:694
private[this] lazy val api_Geostreams_cacheListAction472 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/cache"))))
        

// @LINE:695
private[this] lazy val api_Geostreams_cacheInvalidateAction473 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/cache/invalidate"))))
        

// @LINE:696
private[this] lazy val api_Geostreams_cacheFetchAction474 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/cache/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:697
private[this] lazy val api_Geostreams_createSensor475 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/sensors"))))
        

// @LINE:698
private[this] lazy val api_Geostreams_updateStatisticsStreamSensor476 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/sensors/update"))))
        

// @LINE:699
private[this] lazy val api_Geostreams_getSensor477 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/sensors/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:700
private[this] lazy val api_Geostreams_updateSensorMetadata478 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/sensors/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:701
private[this] lazy val api_Geostreams_getSensorStatistics479 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/sensors/"),DynamicPart("id", """[^/]+""",true),StaticPart("/stats"))))
        

// @LINE:702
private[this] lazy val api_Geostreams_getSensorStreams480 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/sensors/"),DynamicPart("id", """[^/]+""",true),StaticPart("/streams"))))
        

// @LINE:703
private[this] lazy val api_Geostreams_updateStatisticsSensor481 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/sensors/"),DynamicPart("id", """[^/]+""",true),StaticPart("/update"))))
        

// @LINE:704
private[this] lazy val api_Geostreams_searchSensors482 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/sensors"))))
        

// @LINE:705
private[this] lazy val api_Geostreams_deleteSensor483 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/sensors/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:706
private[this] lazy val api_Geostreams_createStream484 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/streams"))))
        

// @LINE:707
private[this] lazy val api_Geostreams_updateStatisticsStreamSensor485 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/streams/update"))))
        

// @LINE:708
private[this] lazy val api_Geostreams_getStream486 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/streams/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:709
private[this] lazy val api_Geostreams_patchStreamMetadata487 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/streams/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:710
private[this] lazy val api_Geostreams_updateStatisticsStream488 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/streams/"),DynamicPart("id", """[^/]+""",true),StaticPart("/update"))))
        

// @LINE:711
private[this] lazy val api_Geostreams_searchStreams489 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/streams"))))
        

// @LINE:712
private[this] lazy val api_Geostreams_deleteStream490 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/streams/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:713
private[this] lazy val api_Geostreams_deleteAll491 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/dropall"))))
        

// @LINE:714
private[this] lazy val api_Geostreams_counts492 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/counts"))))
        

// @LINE:715
private[this] lazy val api_Geostreams_getConfig493 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geostreams/config"))))
        

// @LINE:720
private[this] lazy val api_Thumbnails_uploadThumbnail494 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/fileThumbnail"))))
        

// @LINE:721
private[this] lazy val api_Thumbnails_list495 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/thumbnails"))))
        

// @LINE:722
private[this] lazy val api_Thumbnails_removeThumbnail496 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/thumbnails/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:727
private[this] lazy val api_Sensors_list497 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sensors"))))
        

// @LINE:728
private[this] lazy val api_Sensors_add498 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sensors"))))
        

// @LINE:729
private[this] lazy val api_Sensors_get499 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sensors/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:730
private[this] lazy val api_Sensors_search500 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sensors/search"))))
        

// @LINE:731
private[this] lazy val api_Sensors_delete501 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sensors/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:736
private[this] lazy val api_Comments_mentionInComment502 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comment/mention"))))
        

// @LINE:737
private[this] lazy val api_Comments_comment503 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comment/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:738
private[this] lazy val api_Comments_removeComment504 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comment/"),DynamicPart("id", """[^/]+""",true),StaticPart("/removeComment"))))
        

// @LINE:739
private[this] lazy val api_Comments_editComment505 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/comment/"),DynamicPart("id", """[^/]+""",true),StaticPart("/editComment"))))
        

// @LINE:744
private[this] lazy val controllers_Selected_get506 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("selected"))))
        

// @LINE:745
private[this] lazy val api_Selected_get507 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/selected"))))
        

// @LINE:746
private[this] lazy val api_Selected_add508 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/selected"))))
        

// @LINE:747
private[this] lazy val api_Selected_remove509 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/selected/remove"))))
        

// @LINE:748
private[this] lazy val api_Selected_deleteAll510 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/selected/files"))))
        

// @LINE:749
private[this] lazy val api_Selected_downloadAll511 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/selected/files"))))
        

// @LINE:750
private[this] lazy val api_Selected_clearAll512 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/selected/clear"))))
        

// @LINE:751
private[this] lazy val api_Selected_tagAll513 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/selected/tag"))))
        

// @LINE:756
private[this] lazy val api_Relations_list514 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/relations"))))
        

// @LINE:757
private[this] lazy val api_Relations_findTargets515 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/relations/search"))))
        

// @LINE:758
private[this] lazy val api_Relations_get516 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/relations/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:759
private[this] lazy val api_Relations_add517 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/relations"))))
        

// @LINE:760
private[this] lazy val api_Relations_delete518 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/relations/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:765
private[this] lazy val api_ZoomIt_uploadTile519 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/tiles"))))
        

// @LINE:766
private[this] lazy val api_Geometry_uploadGeometry520 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/geometries"))))
        

// @LINE:767
private[this] lazy val api_ThreeDTexture_uploadTexture521 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/3dTextures"))))
        

// @LINE:768
private[this] lazy val api_Search_search522 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/search"))))
        

// @LINE:769
private[this] lazy val api_Search_querySPARQL523 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/sparqlquery"))))
        

// @LINE:770
private[this] lazy val api_Projects_addproject524 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/projects/addproject"))))
        

// @LINE:771
private[this] lazy val api_Institutions_addinstitution525 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/institutions/addinstitution"))))
        

// @LINE:776
private[this] lazy val api_Users_list526 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users"))))
        

// @LINE:777
private[this] lazy val api_Users_findByEmail527 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/email/"),DynamicPart("email", """[^/]+""",true))))
        

// @LINE:778
private[this] lazy val api_Users_deleteUser528 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:779
private[this] lazy val api_Users_getUser529 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/me"))))
        

// @LINE:780
private[this] lazy val api_Users_updateName530 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/"),DynamicPart("id", """[^/]+""",true),StaticPart("/updateName"))))
        

// @LINE:781
private[this] lazy val api_Users_keysList531 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/keys"))))
        

// @LINE:782
private[this] lazy val api_Users_keysGet532 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/keys/"),DynamicPart("name", """[^/]+""",true))))
        

// @LINE:783
private[this] lazy val api_Users_keysAdd533 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/keys"))))
        

// @LINE:784
private[this] lazy val api_Users_keysDelete534 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/keys/"),DynamicPart("key", """[^/]+""",true))))
        

// @LINE:785
private[this] lazy val api_Users_addUserDatasetView535 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/addUserDatasetView"))))
        

// @LINE:786
private[this] lazy val api_Users_createNewListInUser536 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/createNewListInUser"))))
        

// @LINE:787
private[this] lazy val api_Users_createNewListInUser537 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/createNewListInUser"))))
        

// @LINE:788
private[this] lazy val api_Users_follow538 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/follow"))))
        

// @LINE:789
private[this] lazy val api_Users_unfollow539 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/unfollow"))))
        

// @LINE:790
private[this] lazy val api_CurationObjects_getCurationObjectOre540 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/curations/"),DynamicPart("curationId", """[^/]+""",true),StaticPart("/ore"))))
        

// @LINE:791
private[this] lazy val api_Users_findById541 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/users/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:796
private[this] lazy val controllers_Users_getFollowers542 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/followers"))))
        

// @LINE:797
private[this] lazy val controllers_Users_getFollowing543 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/following"))))
        

// @LINE:802
private[this] lazy val api_Status_version544 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/version"))))
        

// @LINE:803
private[this] lazy val api_Status_status545 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/status"))))
        

// @LINE:808
private[this] lazy val controllers_RSS_siteRSS546 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("rss"))))
        

// @LINE:809
private[this] lazy val controllers_RSS_siteRSSOfType547 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("rss/"),DynamicPart("etype", """[^/]+""",true))))
        

// @LINE:815
private[this] lazy val controllers_CurationObjects_newCO548 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("dataset/"),DynamicPart("id", """[^/]+""",true),StaticPart("/curations/new"))))
        

// @LINE:816
private[this] lazy val controllers_CurationObjects_list549 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("space/curations"))))
        

// @LINE:817
private[this] lazy val controllers_CurationObjects_submit550 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("dataset/"),DynamicPart("id", """[^/]+""",true),StaticPart("/curations/spaces/"),DynamicPart("spaceId", """[^/]+""",true),StaticPart("/submit"))))
        

// @LINE:818
private[this] lazy val controllers_Spaces_stagingArea551 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/stagingArea"))))
        

// @LINE:819
private[this] lazy val controllers_CurationObjects_getCurationObject552 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:820
private[this] lazy val api_CurationObjects_retractCurationObject553 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/retract/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:821
private[this] lazy val controllers_CurationObjects_editCuration554 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/edit"))))
        

// @LINE:822
private[this] lazy val controllers_CurationObjects_updateCuration555 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/spaces/"),DynamicPart("spaceId", """[^/]+""",true),StaticPart("/update"))))
        

// @LINE:823
private[this] lazy val controllers_CurationObjects_deleteCuration556 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:824
private[this] lazy val controllers_CurationObjects_compareToRepository557 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/compareToRepository"))))
        

// @LINE:825
private[this] lazy val controllers_CurationObjects_submitRepositorySelection558 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/submitRepositorySelection"))))
        

// @LINE:826
private[this] lazy val controllers_CurationObjects_sendToRepository559 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/sendToRepository"))))
        

// @LINE:827
private[this] lazy val controllers_CurationObjects_findMatchingRepositories560 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/matchmaker"))))
        

// @LINE:828
private[this] lazy val api_CurationObjects_findMatchmakingRepositories561 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/matchmaker"))))
        

// @LINE:829
private[this] lazy val api_CurationObjects_getCurationFiles562 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/curationFile"))))
        

// @LINE:830
private[this] lazy val api_CurationObjects_savePublishedObject563 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/status"))))
        

// @LINE:831
private[this] lazy val controllers_CurationObjects_getStatusFromRepository564 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/getStatusFromRepository"))))
        

// @LINE:832
private[this] lazy val api_CurationObjects_deleteCurationFile565 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/files/"),DynamicPart("curationFileId", """[^/]+""",true))))
        

// @LINE:833
private[this] lazy val api_CurationObjects_deleteCurationFolder566 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/folders/"),DynamicPart("curationFolderId", """[^/]+""",true))))
        

// @LINE:834
private[this] lazy val controllers_CurationObjects_getUpdatedFilesAndFolders567 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/updatedFilesAndFolders"))))
        

// @LINE:835
private[this] lazy val api_CurationObjects_getMetadataDefinitionsByFile568 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/getMetadataDefinitionsByFile"))))
        

// @LINE:836
private[this] lazy val api_CurationObjects_getMetadataDefinitions569 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("spaces/curations/"),DynamicPart("id", """[^/]+""",true),StaticPart("/metadataDefinitions"))))
        

// @LINE:837
private[this] lazy val controllers_CurationObjects_getPublishedData570 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("publishedData"))))
        

// @LINE:838
private[this] lazy val controllers_CurationObjects_getPublishedData571 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("publishedData/"),DynamicPart("space", """[^/]+""",true))))
        

// @LINE:840
private[this] lazy val api_Events_sendExceptionEmail572 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("sendEmail"))))
        

// @LINE:841
private[this] lazy val controllers_Events_getEvents573 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("getEvents"))))
        

// @LINE:847
private[this] lazy val api_Vocabularies_list574 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabularies/list"))))
        

// @LINE:848
private[this] lazy val api_Vocabularies_createVocabularyFromJson575 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabularies/createVocabularyFromJson"))))
        

// @LINE:849
private[this] lazy val api_Vocabularies_createVocabularyFromForm576 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabularies/createVocabulary"))))
        

// @LINE:850
private[this] lazy val api_Vocabularies_getByAuthor577 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabularies/getByAuthor"))))
        

// @LINE:851
private[this] lazy val api_Vocabularies_getPublicVocabularies578 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabularies/getPublic"))))
        

// @LINE:854
private[this] lazy val api_Vocabularies_get579 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabularies/"),DynamicPart("vocab_id", """[^/]+""",true))))
        

// @LINE:855
private[this] lazy val api_Vocabularies_editVocabulary580 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/editVocabulary/"),DynamicPart("vocab_id", """[^/]+""",true))))
        

// @LINE:856
private[this] lazy val api_Vocabularies_getByName581 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabularies/"),DynamicPart("name", """[^/]+""",true))))
        

// @LINE:857
private[this] lazy val api_Vocabularies_editVocabulary582 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabularies/"),DynamicPart("vocab_id", """[^/]+""",true),StaticPart("/editVocabulary"))))
        

// @LINE:858
private[this] lazy val api_Vocabularies_getByNameAndAuthor583 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabularies/"),DynamicPart("name", """[^/]+""",true),StaticPart("/getByNameAuthor"))))
        

// @LINE:860
private[this] lazy val api_Vocabularies_addToSpace584 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabularies/"),DynamicPart("vocab_id", """[^/]+""",true),StaticPart("/addToSpace/"),DynamicPart("space_id", """[^/]+""",true))))
        

// @LINE:861
private[this] lazy val api_Vocabularies_removeFromSpace585 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabuliaries/"),DynamicPart("vocab_id", """[^/]+""",true),StaticPart("/removeFromSpace/"),DynamicPart("space_id", """[^/]+""",true))))
        

// @LINE:867
private[this] lazy val api_VocabularyTerms_list586 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabterms/list"))))
        

// @LINE:868
private[this] lazy val api_VocabularyTerms_get587 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/vocabterms/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:874
private[this] lazy val api_Proxy_get588 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true))))
        

// @LINE:875
private[this] lazy val api_Proxy_get589 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true),StaticPart("/"))))
        

// @LINE:876
private[this] lazy val api_Proxy_get590 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true),StaticPart("/"),DynamicPart("pathSuffix", """.+""",false))))
        

// @LINE:877
private[this] lazy val api_Proxy_post591 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true))))
        

// @LINE:878
private[this] lazy val api_Proxy_post592 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true),StaticPart("/"))))
        

// @LINE:879
private[this] lazy val api_Proxy_post593 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true),StaticPart("/"),DynamicPart("pathSuffix", """.+""",false))))
        

// @LINE:880
private[this] lazy val api_Proxy_put594 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true))))
        

// @LINE:881
private[this] lazy val api_Proxy_put595 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true),StaticPart("/"))))
        

// @LINE:882
private[this] lazy val api_Proxy_put596 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true),StaticPart("/"),DynamicPart("pathSuffix", """.+""",false))))
        

// @LINE:883
private[this] lazy val api_Proxy_delete597 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true))))
        

// @LINE:884
private[this] lazy val api_Proxy_delete598 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true),StaticPart("/"))))
        

// @LINE:885
private[this] lazy val api_Proxy_delete599 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/proxy/"),DynamicPart("endpoint_key", """[^/]+""",true),StaticPart("/"),DynamicPart("pathSuffix", """.+""",false))))
        

// @LINE:891
private[this] lazy val api_Vocabularies_createVocabularyFromJson600 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/createExperimentTemplate"))))
        

// @LINE:892
private[this] lazy val api_Vocabularies_createVocabularyFromJson601 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/createExperimentTemplateFromJson"))))
        

// @LINE:893
private[this] lazy val api_Vocabularies_createVocabularyFromJson602 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/createExperimentTemplateDefaultValues"))))
        

// @LINE:895
private[this] lazy val api_Vocabularies_list603 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/listExperimentTemplates"))))
        

// @LINE:896
private[this] lazy val api_Vocabularies_listAll604 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/allExperimentTemplates"))))
        

// @LINE:898
private[this] lazy val api_Vocabularies_getPublicVocabularies605 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/getPublic"))))
        

// @LINE:899
private[this] lazy val api_Vocabularies_getAllTagsOfAllVocabularies606 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/allTags"))))
        

// @LINE:900
private[this] lazy val api_T2C2_listMyDatasets607 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/listMyDatasets"))))
        

// @LINE:901
private[this] lazy val api_T2C2_getVocabulary608 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/getExperimentTemplateById/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:902
private[this] lazy val api_Vocabularies_getByName609 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/getExperimentTemplateByName/"),DynamicPart("name", """[^/]+""",true))))
        

// @LINE:903
private[this] lazy val api_Vocabularies_getByTag610 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/findByTag"))))
        

// @LINE:904
private[this] lazy val api_Vocabularies_getBySingleTag611 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/findByTag/"),DynamicPart("tag", """[^/]+""",true))))
        

// @LINE:905
private[this] lazy val api_T2C2_getVocabByDatasetId612 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/getByDatasetId/"),DynamicPart("dataset_id", """[^/]+""",true))))
        

// @LINE:906
private[this] lazy val api_T2C2_getVocabByFileId613 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/getByFileId/"),DynamicPart("file_id", """[^/]+""",true))))
        

// @LINE:908
private[this] lazy val api_T2C2_listMyDatasets614 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/listMyDatasets"))))
        

// @LINE:909
private[this] lazy val api_T2C2_listMySpaces615 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/listMySpaces"))))
        

// @LINE:910
private[this] lazy val api_Vocabularies_removeVocabulary616 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/deleteTemplate/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:911
private[this] lazy val api_T2C2_makeTemplatePublic617 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/makePublic/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:912
private[this] lazy val api_T2C2_makeTemplatePrivate618 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/makePrivate/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:913
private[this] lazy val api_Vocabularies_editVocabulary619 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/editTemplate/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:914
private[this] lazy val api_T2C2_attachVocabToDataset620 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/"),DynamicPart("vocab_id", """[^/]+""",true),StaticPart("/attachToDataset/"),DynamicPart("dataset_id", """[^/]+""",true))))
        

// @LINE:915
private[this] lazy val api_T2C2_detachVocabFromDataset621 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/"),DynamicPart("vocab_id", """[^/]+""",true),StaticPart("/detachFromDataset/"),DynamicPart("dataset_id", """[^/]+""",true))))
        

// @LINE:916
private[this] lazy val api_T2C2_attachVocabToFile622 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/"),DynamicPart("vocab_id", """[^/]+""",true),StaticPart("/attachToFile/"),DynamicPart("file_id", """[^/]+""",true))))
        

// @LINE:917
private[this] lazy val api_T2C2_detachVocabFromFile623 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/"),DynamicPart("vocab_id", """[^/]+""",true),StaticPart("/detachFromFile/"),DynamicPart("file_id", """[^/]+""",true))))
        

// @LINE:919
private[this] lazy val api_T2C2_listCollectionsCanEdit624 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/spaces/"),DynamicPart("id", """[^/]+""",true),StaticPart("/collectionsCanEdit"))))
        

// @LINE:921
private[this] lazy val controllers_T2C2_newTemplate625 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/new"))))
        

// @LINE:926
private[this] lazy val api_Notebooks_submit626 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/notebook/submit"))))
        

// @LINE:934
private[this] lazy val controllers_T2C2_uploadFile627 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/zipUpload"))))
        

// @LINE:936
private[this] lazy val api_T2C2_findYoungestChild628 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/findYoungestChild"))))
        

// @LINE:938
private[this] lazy val api_T2C2_getAllCollectionsOfUser629 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/allCollection"))))
        

// @LINE:939
private[this] lazy val api_T2C2_getAllCollectionsWithDatasetIdsAndFiles630 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/allCollectionDatasetFiles"))))
        

// @LINE:940
private[this] lazy val api_T2C2_getAllCollectionsForTree631 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/collectionsForTree"))))
        

// @LINE:941
private[this] lazy val api_T2C2_getLevelOfTree632 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/getLevelOfTree"))))
        

// @LINE:942
private[this] lazy val api_T2C2_getLevelOfTreeSharedWithMe633 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/getLevelOfTreeSharedWithMe"))))
        

// @LINE:943
private[this] lazy val api_T2C2_getLevelOfTreeSharedWithOthers634 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/getLevelOfTreeSharedWithOthers"))))
        

// @LINE:944
private[this] lazy val api_T2C2_getLevelOfTreeNotSharedInSpace635 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/getLevelOfTreeNotShared"))))
        

// @LINE:945
private[this] lazy val api_T2C2_getLevelOfTreeInSpace636 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/getLevelOfTreeInSpace"))))
        

// @LINE:946
private[this] lazy val api_T2C2_getAllCollectionsForFullTree637 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/collectionsForFullTree"))))
        

// @LINE:947
private[this] lazy val api_T2C2_getAllCollectionsOfUserNotSharedInSpace638 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/allCollectionsNotSharedInSpace"))))
        

// @LINE:948
private[this] lazy val api_T2C2_createEmptyDataset639 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/datasets/createEmpty"))))
        

// @LINE:949
private[this] lazy val api_T2C2_moveKeysToTermsTemplates640 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/moveKeysToTerms"))))
        

// @LINE:950
private[this] lazy val api_T2C2_bulkDelete641 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/bulkDelete"))))
        

// @LINE:951
private[this] lazy val api_T2C2_getWhoICanUploadFor642 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/getUsersICanUploadFor"))))
        

// @LINE:952
private[this] lazy val api_T2C2_getSpacesOfUser643 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/getSpacesOfUser/"),DynamicPart("userId", """[^/]+""",true))))
        

// @LINE:953
private[this] lazy val api_T2C2_getSpacesUserHasAccessTo644 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/getSpacesUserCanEdit/"),DynamicPart("userId", """[^/]+""",true))))
        

// @LINE:954
private[this] lazy val api_T2C2_getUsersOfSpace645 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/getUsersOfSpace/"),DynamicPart("spaceId", """[^/]+""",true))))
        

// @LINE:955
private[this] lazy val api_T2C2_getNumUsersOfSpace646 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/getNumUsersOfSpace/"),DynamicPart("spaceId", """[^/]+""",true))))
        

// @LINE:956
private[this] lazy val api_Files_uploadToDatasetWithDescription647 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/uploadToDataset/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:957
private[this] lazy val api_T2C2_getTemplateFromLastDataset648 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/templates/lastTemplate"))))
        

// @LINE:958
private[this] lazy val api_T2C2_getDatasetWithAttachedVocab649 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/datasets/getDatasetAndTemplate/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:959
private[this] lazy val api_Files_updateDescription650 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/files/"),DynamicPart("id", """[^/]+""",true),StaticPart("/updateDescription"))))
        

// @LINE:960
private[this] lazy val api_Datasets_listInCollection651 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/datasets"))))
        

// @LINE:961
private[this] lazy val api_T2C2_getDatasetsInCollectionWithColId652 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/collections/"),DynamicPart("coll_id", """[^/]+""",true),StaticPart("/datasetsWithParentColId"))))
        

// @LINE:962
private[this] lazy val api_T2C2_getAllCollectionsWithDatasetIds653 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/t2c2/allCollectionsWithDatasetIds"))))
        

// @LINE:963
private[this] lazy val api_T2C2_getKeysValuesFromLastDataset654 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/getKeyValuesLastDataset"))))
        

// @LINE:964
private[this] lazy val api_T2C2_getKeysValuesFromLastDatasets655 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/getKeyValuesForLastDatasets"))))
        

// @LINE:965
private[this] lazy val api_T2C2_getKeysValuesFromDatasetId656 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/getKeyValuesForDatasetId/"),DynamicPart("id", """[^/]+""",true))))
        

// @LINE:966
private[this] lazy val api_T2C2_getVocabIdNameFromTag657 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/getIdNameFromTag/"),DynamicPart("tag", """[^/]+""",true))))
        

// @LINE:968
private[this] lazy val controllers_T2C2_uploader658 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/uploader"))))
        

// @LINE:970
private[this] lazy val controllers_T2C2_zipUploader659 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("t2c2/zipUploader"))))
        

// @LINE:977
private[this] lazy val api_Tree_getChildrenOfNode660 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/tree/getChildrenOfNode"))))
        

// @LINE:978
private[this] lazy val api_FullTree_getChildrenOfNode661 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("api/fulltree/getChildrenOfNode"))))
        
def documentation = List(("""OPTIONS""", prefix + (if(prefix.endsWith("/")) "" else "/") + """$path<.+>""","""api.ApiHelp.options(path:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """$path<.+>/""","""@controllers.Application@.untrail(path:String)"""),("""GET""", prefix,"""@controllers.Application@.index"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """about""","""@controllers.Application@.about"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """tos""","""@controllers.Application@.tos(redirect:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """email""","""@controllers.Application@.email(subject:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """contexts/metadata.jsonld""","""controllers.Assets.at(path:String = "/public", file:String = "/jsonld/contexts/metadata.jsonld")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/img/glyphicons-halflings-white.png""","""controllers.Assets.at(path:String = "/public", file:String = "/images/glyphicons-halflings-white.png")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/img/glyphicons-halflings.png""","""controllers.Assets.at(path:String = "/public", file:String = "/images/glyphicons-halflings.png")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users""","""@controllers.Users@.getUsers(when:String ?= "", id:String ?= "", limit:Int ?= 24)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/acceptTermsOfServices""","""@controllers.Users@.acceptTermsOfServices(redirect:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login/isLoggedIn""","""@controllers.Login@.isLoggedIn"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login/isLoggedInWithGoogle""","""@controllers.Application@.onlyGoogle"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/sendEmail""","""@controllers.Users@.sendEmail(subject:String, from:String, recipient:String, body:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """ldap""","""@controllers.Login@.ldap(redirecturl:String, token:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """ldap/authenticate""","""@controllers.Login@.ldapAuthenticate(uid:String, password:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """profile/viewProfile""","""@controllers.Profile@.viewProfile(email:Option[String])"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """profile/viewProfile/$uuid<[^/]+>""","""@controllers.Profile@.viewProfileUUID(uuid:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """profile/editProfile""","""@controllers.Profile@.editProfile"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """profile/submitChanges""","""@controllers.Profile@.submitChanges"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """profile/$uuid<[^/]+>""","""@controllers.Profile@.viewProfileUUID(uuid:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """error/authenticationRequired""","""@controllers.Error@.authenticationRequired"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """error/authenticationRequiredMessage/$msg<[^/]+>/$url<[^/]+>""","""@controllers.Error@.authenticationRequiredMessage(msg:String, url:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """error/noPermissions""","""@controllers.Error@.incorrectPermissions(msg:String ?= null)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """error/notActivated""","""@controllers.Error@.notActivated"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """error/notAuthorized""","""@controllers.Error@.notAuthorized(msg:String, id:String, resourceType:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """extractions/requests""","""@controllers.ExtractionInfo@.getDTSRequests()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """extractions/servers_ips""","""@controllers.ExtractionInfo@.getExtractorServersIP()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """extractions/extractors_names""","""@controllers.ExtractionInfo@.getExtractorNames()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """extractions/supported_input_types""","""@controllers.ExtractionInfo@.getExtractorInputTypes()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """bookmarklet""","""@controllers.ExtractionInfo@.getBookmarkletPage()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """bookmarklet.js""","""@controllers.Application@.bookmarklet"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """extensions/dts/chrome""","""@controllers.ExtractionInfo@.getExtensionPage()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """extraction/form""","""@controllers.Files@.extractFile"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """extraction/upload""","""@controllers.Files@.uploadExtract()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files""","""@controllers.Files@.list(when:String ?= "", date:String ?= "", size:Int ?= 12, mode:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files/new""","""@controllers.Files@.uploadFile"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files/metadataSearch""","""@controllers.Files@.metadataSearch"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files/generalMetadataSearch""","""@controllers.Files@.generalMetadataSearch"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files/following""","""@controllers.Files@.followingFiles(index:Int ?= 0, size:Int ?= 12, mode:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files/$id<[^/]+>""","""@controllers.Files@.file(id:UUID, dataset:Option[String] ?= None, space:Option[String] ?= None, folder:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """filesPreview/$id<[^/]+>""","""@controllers.Files@.filePreview(id:UUID, dataset:Option[String] ?= None, space:Option[String] ?= None, folder:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files/$id<[^/]+>/blob""","""@controllers.Files@.download(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files/$id<[^/]+>/download/$format<[^/]+>""","""@controllers.Files@.downloadAsFormat(id:UUID, format:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """upload""","""@controllers.Files@.upload"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """uploadAndExtract""","""@controllers.Files@.uploadAndExtract"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """uploadSelectQuery""","""@controllers.Files@.uploadSelectQuery"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """uploaddnd/$id<[^/]+>""","""@controllers.Files@.uploaddnd(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """uploadDragDrop""","""@controllers.Files@.uploadDragDrop"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """fileThumbnail/$id<[^/]+>/blob""","""@controllers.Files@.thumbnail(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """file_by_section/$id<[^/]+>""","""@controllers.Files@.fileBySection(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files/$file_id<[^/]+>/extractions""","""@controllers.Extractors@.submitFileExtraction(file_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets""","""@controllers.Datasets@.list(when:String ?= "", date:String ?= "", size:Int ?= 12, space:Option[String] ?= None, status:Option[String] ?= None, mode:String ?= "", owner:Option[String] ?= None, showPublic:Boolean ?= true, showOnlyShared:Boolean ?= false, showTrash:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/sorted""","""@controllers.Datasets@.sortedListInSpace(space:String, offset:Integer, limit:Integer, showPublic:Boolean ?= true)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/new""","""@controllers.Datasets@.newDataset(space:Option[String], collection:Option[String])"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/createStep2""","""@controllers.Datasets@.createStep2(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/metadataSearch""","""@controllers.Datasets@.metadataSearch"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/generalMetadataSearch""","""@controllers.Datasets@.generalMetadataSearch"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/following""","""@controllers.Datasets@.followingDatasets(index:Int ?= 0, size:Int ?= 12, mode:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """toolManager""","""@controllers.ToolManager@.toolManager"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/refreshToolList""","""@controllers.ToolManager@.refreshToolSidebar(datasetid:UUID, datasetName:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/launchableTools""","""@controllers.ToolManager@.getLaunchableTools"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/uploadToTool""","""@controllers.ToolManager@.uploadDatasetToTool(instanceID:UUID, datasetID:UUID, datasetName:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/getInstances""","""@controllers.ToolManager@.getInstances"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/removeInstance""","""@controllers.ToolManager@.removeInstance(toolType:String, instanceID:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/launchTool""","""@controllers.ToolManager@.launchTool(sessionName:String, ttype:String, datasetId:UUID, datasetName:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/$id<[^/]+>""","""@controllers.Datasets@.dataset(id:UUID, space:Option[String] ?= None, limit:Int ?= 20)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/$datasetId<[^/]+>/updatedFilesAndFolders""","""@controllers.Datasets@.getUpdatedFilesAndFolders(datasetId:UUID, limit:Int ?= 20, pageIndex:Int ?= 0, space:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/$id<[^/]+>/users""","""@controllers.Datasets@.users(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets_by_section/$id<[^/]+>""","""@controllers.Datasets@.datasetBySection(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """dataset/submit""","""@controllers.Datasets@.submit(folderId:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/$id<[^/]+>/addFiles""","""@controllers.Datasets@.addFiles(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/$id<[^/]+>/$folderId<[^/]+>/addFiles""","""@controllers.Folders@.addFiles(id:UUID, folderId:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/$parentDatasetId<[^/]+>/newFolder""","""@controllers.Folders@.createFolder(parentDatasetId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/$id<[^/]+>/extractions""","""@controllers.Extractors@.submitDatasetExtraction(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collections""","""@controllers.Collections@.list(when:String ?= "", date:String ?= "", size:Int ?= 12, space:Option[String] ?= None, mode:String ?= "", owner:Option[String] ?= None, showPublic:Boolean ?= true, showOnlyShared:Boolean ?= false, showTrash:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collections/sorted""","""@controllers.Collections@.sortedListInSpace(space:String, offset:Integer, limit:Integer, showPublic:Boolean ?= true)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collections/listChildCollections""","""@controllers.Collections@.listChildCollections(parentCollectionId:String, when:String ?= "", date:String ?= "", size:Int ?= 12, space:Option[String] ?= None, mode:String ?= "", owner:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collections/new""","""@controllers.Collections@.newCollection(space:Option[String])"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collections/$id<[^/]+>/newchildCollection""","""@controllers.Collections@.newCollectionWithParent(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collections/following""","""@controllers.Collections@.followingCollections(index:Int ?= 0, size:Int ?= 12, mode:String ?= "")"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collection/submit""","""@controllers.Collections@.submit"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collection/createStep2""","""@controllers.Collections@.createStep2"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collection/$id<[^/]+>""","""@controllers.Collections@.collection(id:UUID, limit:Int ?= 12)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collection/$id<[^/]+>/users""","""@controllers.Collections@.users(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collections/$collection_id<[^/]+>/previews""","""@controllers.Collections@.previews(collection_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collection/$id<[^/]+>/datasets""","""@controllers.Collections@.getUpdatedDatasets(id:UUID, index:Int ?= 0, limit:Int ?= 12)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """collection/$id<[^/]+>/childCollections""","""@controllers.Collections@.getUpdatedChildCollections(id:UUID, index:Int ?= 0, limit:Int ?= 12)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces""","""@controllers.Spaces@.list(when:String ?= "", date:String ?= "", size:Int ?= 12, mode:String ?= "", owner:Option[String] ?= None, showAll:Boolean ?= true, showPublic:Boolean ?= true, onlyTrial:Boolean ?= false, showOnlyShared:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/new""","""@controllers.Spaces@.newSpace"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/copy/$id<[^/]+>""","""@controllers.Spaces@.copySpace(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/following""","""@controllers.Spaces@.followingSpaces(index:Int ?= 0, size:Int ?= 12, mode:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/$id<[^/]+>""","""@controllers.Spaces@.getSpace(id:UUID, size:Int ?= 9, direction:String ?= "desc")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/$id<[^/]+>/updateSpace""","""@controllers.Spaces@.updateSpace(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/$id<[^/]+>/addRequest""","""@controllers.Spaces@.addRequest(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/$id<[^/]+>/users""","""@controllers.Spaces@.manageUsers(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/$id<[^/]+>/extractors""","""@controllers.Spaces@.selectExtractors(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/$id<[^/]+>/extractors""","""@controllers.Spaces@.updateExtractors(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/$id<[^/]+>/invite""","""@controllers.Spaces@.inviteToSpace(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/$id<[^/]+>/metadata""","""@controllers.Metadata@.getMetadataBySpace(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/submit""","""@controllers.Spaces@.submit"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/submitCopy""","""@controllers.Spaces@.submitCopy"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """geostreams""","""@controllers.Geostreams@.map"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """geostreams/sensors""","""@controllers.Geostreams@.list"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """geostreams/sensors/new""","""@controllers.Geostreams@.newSensor"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """geostreams/sensors/$id<[^/]+>""","""@controllers.Geostreams@.edit(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """metadata/search""","""@controllers.Metadata@.search()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """metadata/$id<[^/]+>""","""@controllers.Metadata@.view(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files/$file_id<[^/]+>/metadata""","""@controllers.Metadata@.file(file_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """datasets/$dataset_id<[^/]+>/metadata""","""@controllers.Metadata@.dataset(dataset_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """tags/search""","""@controllers.Tags@.search(tag:String, start:String ?= "", size:Integer ?= 12, mode:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """tags/list/ordered""","""@controllers.Tags@.tagListOrdered"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """tags/list/weighted""","""@controllers.Tags@.tagListWeighted"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """search""","""@controllers.Search@.search(query:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """multimediasearch""","""@controllers.Search@.multimediasearch"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """advanced""","""@controllers.Search@.advanced"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """SearchByText""","""@controllers.Search@.SearchByText(query:String ?= "")"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """uploadquery""","""@controllers.Search@.uploadquery"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """searchbyURL""","""@controllers.Search@.searchbyURL(query:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """searchbyfeature/$section_id<[^/]+>""","""@controllers.Search@.callSearchMultimediaIndexView(section_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """files/$id<[^/]+>/similar""","""@controllers.Search@.findSimilarToExistingFile(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """queries/$id<[^/]+>/similar""","""@controllers.Search@.findSimilarToQueryFile(id:UUID, typeToSearch:String, sectionsSelected:List[String] ?= List.empty )"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """queries/similarWeightedIndexes""","""@controllers.Search@.findSimilarWeightedIndexes()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """analysisSessions""","""@controllers.DataAnalysis@.listSessions"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """terminateSession/$id<[^/]+>""","""@controllers.DataAnalysis@.terminate(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """notebooks""","""@controllers.Notebook@.listNotebooks"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """notebook/$id<[^/]+>""","""@controllers.Notebook@.viewNotebook(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """editNotebook/$id<[^/]+>""","""@controllers.Notebook@.editNotebook(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """deleteNotebook/$id<[^/]+>""","""@controllers.Notebook@.deleteNotebook(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """previewers/list""","""controllers.Previewers.list"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""securesocial.controllers.LoginPage.login"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """logout""","""securesocial.controllers.LoginPage.logout"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """signup""","""securesocial.controllers.Registration.startSignUp"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """signup""","""securesocial.controllers.Registration.handleStartSignUp"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """signup/$token<[^/]+>""","""securesocial.controllers.Registration.signUp(token:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """signup/$token<[^/]+>""","""@controllers.Registration@.handleSignUp(token:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """reset""","""securesocial.controllers.Registration.startResetPassword"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """reset""","""securesocial.controllers.Registration.handleStartResetPassword"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """reset/$token<[^/]+>""","""securesocial.controllers.Registration.resetPassword(token:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """reset/$token<[^/]+>""","""securesocial.controllers.Registration.handleResetPassword(token:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """password""","""securesocial.controllers.PasswordChange.page"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """password""","""securesocial.controllers.PasswordChange.handlePasswordChange"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """authenticate/$provider<[^/]+>""","""securesocial.controllers.ProviderController.authenticate(provider:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """authenticate/$provider<[^/]+>""","""securesocial.controllers.ProviderController.authenticateByPost(provider:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """not-authorized""","""securesocial.controllers.ProviderController.notAuthorized"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/customize""","""@controllers.Admin@.customize"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/tos""","""@controllers.Admin@.tos"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/users""","""@controllers.Admin@.users"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/indexAdmin""","""@controllers.Admin@.adminIndex"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/reindexFiles""","""@controllers.Admin@.reindexFiles"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/adapters""","""@controllers.Admin@.getAdapters"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/extractors""","""@controllers.Admin@.getExtractors"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/measures""","""@controllers.Admin@.getMeasures"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/indexers""","""@controllers.Admin@.getIndexers"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/indexes""","""@controllers.Admin@.getIndexes"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/sections""","""@controllers.Admin@.getSections"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/metadata/definitions""","""@controllers.Admin@.getMetadataDefinitions"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/createIndex""","""@controllers.Admin@.createIndex"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/index/$id<[^/]+>/build""","""@controllers.Admin@.buildIndex(id:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/index/$id<[^/]+>""","""@controllers.Admin@.deleteIndex(id:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/index""","""@controllers.Admin@.deleteAllIndexes"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/roles""","""@controllers.Admin@.listRoles"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/roles/new""","""@controllers.Admin@.createRole"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/roles/submitNew""","""@controllers.Admin@.submitCreateRole"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/roles/delete/$id<[^/]+>""","""@controllers.Admin@.removeRole(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/roles/$id<[^/]+>/edit""","""@controllers.Admin@.editRole(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/roles/update""","""@controllers.Admin@.updateRole"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/extractions""","""@controllers.Extractors@.listAllExtractions"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/dataDumps""","""@controllers.Admin@.viewDumpers"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/sensors""","""@controllers.Admin@.sensors"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sensors/config""","""@api.Admin@.sensorsConfig"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """javascriptRoutes""","""@controllers.Application@.javascriptRoutes"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """swagger""","""@controllers.Application@.swagger"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """swaggerUI""","""@controllers.Application@.swaggerUI"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/admin/mail""","""@api.Admin@.mail"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/admin/mailZip""","""@api.Admin@.emailZipUpload"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/admin/users""","""@api.Admin@.users"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sensors/config""","""@api.Admin@.sensorsConfig"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/delete-data""","""@api.Admin@.deleteAllData(resetAll:Boolean ?= false)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/changeAppearance""","""@api.Admin@.submitAppearance"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/reindex""","""@api.Admin@.reindex"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/admin/configuration""","""@api.Admin@.updateConfiguration"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/contexts""","""@api.ContextLD@.addContext()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/contexts/$id<[^/]+>""","""@api.ContextLD@.getContextById(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/contexts/$name<[^/]+>/context.json""","""@api.ContextLD@.getContextByName(name:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/contexts/$id<[^/]+>""","""@api.ContextLD@.removeById(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/metadata.jsonld""","""@api.Datasets@.addMetadataJsonLD(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/metadata.jsonld""","""@api.Datasets@.getMetadataJsonLD(id:UUID, extractor:Option[String] ?= None)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/metadata.jsonld""","""@api.Datasets@.removeMetadataJsonLD(id:UUID, extractor:Option[String] ?= None)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/metadata.jsonld""","""@api.Files@.addMetadataJsonLD(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/metadata.jsonld""","""@api.Files@.getMetadataJsonLD(id:UUID, extractor:Option[String] ?= None)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/metadata.jsonld""","""@api.Files@.removeMetadataJsonLD(id:UUID, extractor:Option[String] ?= None)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata.jsonld""","""@api.Metadata@.addUserMetadata()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata.jsonld/$id<[^/]+>""","""@api.Metadata@.removeMetadata(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata/definitions""","""@api.Metadata@.getDefinitions()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata/distinctdefinitions""","""@api.Metadata@.getDefinitionsDistinctName()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata/autocompletenames""","""@api.Metadata@.getAutocompleteName(filter:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata/definitions/$id<[^/]+>""","""@api.Metadata@.getDefinition(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata/url/$in_url<.+>""","""@api.Metadata@.getUrl(in_url:String)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata/definitions/$id<[^/]+>""","""@api.Metadata@.editDefinition(id:UUID, spaceId:Option[String] ?= None)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata/definitions""","""@api.Metadata@.addDefinition()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/metadata/definitions/$id<[^/]+>""","""@api.Metadata@.deleteDefinition(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata/people""","""@api.Metadata@.listPeople(term:String, limit:Int)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata/people/$pid<[^/]+>""","""@api.Metadata@.getPerson(pid:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/metadata/repository/$id<[^/]+>""","""@api.Metadata@.getRepository(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/logos""","""@api.Logos@.list(path:Option[String] ?= None, name:Option[String] ?= None)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/logos""","""@api.Logos@.upload"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/logos/$id<[^/]+>""","""@api.Logos@.getId(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/logos/$id<[^/]+>""","""@api.Logos@.putId(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/logos/$id<[^/]+>/blob""","""@api.Logos@.downloadId(id:UUID, default:Option[String] ?= None)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/logos/$id<[^/]+>""","""@api.Logos@.deleteId(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/logos/$path<[^/]+>/$name<[^/]+>""","""@api.Logos@.getPath(path:String, name:String)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/logos/$path<[^/]+>/$name<[^/]+>""","""@api.Logos@.putPath(path:String, name:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/logos/$path<[^/]+>/$name<[^/]+>/blob""","""@api.Logos@.downloadPath(path:String, name:String, default:Option[String] ?= None)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/logos/$path<[^/]+>/$name<[^/]+>""","""@api.Logos@.deletePath(path:String, name:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$three_d_file_id<[^/]+>/geometries/$geometry_id<[^/]+>""","""@api.Files@.attachGeometry(three_d_file_id:UUID, geometry_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$three_d_file_id<[^/]+>/3dTextures/$texture_id<[^/]+>""","""@api.Files@.attachTexture(three_d_file_id:UUID, texture_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$file_id<[^/]+>/thumbnails/$thumbnail_id<[^/]+>""","""@api.Files@.attachThumbnail(file_id:UUID, thumbnail_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/queries/$file_id<[^/]+>/thumbnails/$thumbnail_id<[^/]+>""","""@api.Files@.attachQueryThumbnail(file_id:UUID, thumbnail_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files""","""@api.Files@.list"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files""","""@api.Files@.upload(showPreviews:String ?= "DatasetLevel", originalZipFile:String ?= "", flags:String ?= "")"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/withFlags/$flags<[^/]+>""","""@api.Files@.upload(showPreviews:String ?= "DatasetLevel", originalZipFile:String ?= "", flags:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/searchusermetadata""","""@api.Files@.searchFilesUserMetadata"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/searchmetadata""","""@api.Files@.searchFilesGeneralMetadata"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/uploadToDataset/withFlags/$id<[^/]+>/$flags<[^/]+>""","""@api.Files@.uploadToDataset(id:UUID, showPreviews:String ?= "DatasetLevel", originalZipFile:String ?= "", flags:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/uploadToDataset/$id<[^/]+>""","""@api.Files@.uploadToDataset(id:UUID, showPreviews:String ?= "DatasetLevel", originalZipFile:String ?= "", flags:String ?= "")"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/updateDescription""","""@api.Files@.updateDescription(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/uploadIntermediate/$idAndFlags<[^/]+>""","""@api.Files@.uploadIntermediate(idAndFlags:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/sendJob/$fileId<[^/]+>/$fileType<[^/]+>""","""@api.Files@.sendJob(fileId:UUID, fileType:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/getRDFURLsForFile/$id<[^/]+>""","""@api.Files@.getRDFURLsForFile(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/rdfUserMetadata/$id<[^/]+>""","""@api.Files@.getRDFUserMetadata(id:UUID, mappingNum:String ?= "1")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/blob""","""@api.Files@.download(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/remove""","""@api.Files@.removeFile(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/metadata""","""@api.Files@.get(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/metadata""","""@api.Files@.addMetadata(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/metadataDefinitions""","""@api.Files@.getMetadataDefinitions(id:UUID, space:Option[String] ?= None)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/updateMetadata""","""@api.Files@.updateMetadata(id:UUID, extractor_id:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/versus_metadata""","""@api.Files@.addVersusMetadata(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/versus_metadata""","""@api.Files@.getVersusMetadataJSON(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/usermetadatajson""","""@api.Files@.getUserMetadataJSON(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/usermetadata""","""@api.Files@.addUserMetadata(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/technicalmetadatajson""","""@api.Files@.getTechnicalMetadataJSON(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/xmlmetadatajson""","""@api.Files@.getXMLMetadataJSON(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/follow""","""@api.Files@.follow(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/unfollow""","""@api.Files@.unfollow(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/comment""","""@api.Files@.comment(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>""","""@api.Files@.removeFile(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/tags""","""@api.Files@.getTags(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/tags""","""@api.Files@.addTags(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/tags/remove""","""@api.Files@.removeTags(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/tags/remove_all""","""@api.Files@.removeAllTags(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/tags""","""@api.Files@.removeTags(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/license""","""@api.Files@.updateLicense(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/extracted_metadata""","""@api.Files@.extract(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/filename""","""@api.Files@.updateFileName(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/reindex""","""@api.Files@.reindex(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/users""","""@api.Files@.users(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/paths""","""@api.Files@.paths(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/changeOwner/$ownerId<[^/]+>""","""@api.Files@.changeOwner(id:UUID, ownerId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/getOwner""","""@api.Files@.getOwner(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractors""","""@api.Extractions@.listExtractors()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractors/$name<[^/]+>""","""@api.Extractions@.getExtractorInfo(name:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractors""","""@api.Extractions@.addExtractorInfo()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$file_id<[^/]+>/extractions""","""@api.Extractions@.submitFileToExtractor(file_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$ds_id<[^/]+>/extractions""","""@api.Extractions@.submitDatasetToExtractor(ds_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/requests""","""@api.Extractions@.getDTSRequests()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/servers_ips""","""@api.Extractions@.getExtractorServersIP()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/extractors_names""","""@api.Extractions@.getExtractorNames()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/supported_input_types""","""@api.Extractions@.getExtractorInputTypes()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/extractors_details""","""@api.Extractions@.getExtractorDetails()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/upload_url""","""@api.Extractions@.uploadByURL(extract:Boolean ?= true)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/multiple_uploadby_url""","""@api.Extractions@.multipleUploadByURL()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/upload_file""","""@api.Extractions@.uploadExtract(showPreviews:String ?= "DatasetLevel", extract:Boolean ?= true)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/$id<[^/]+>/status""","""@api.Extractions@.checkExtractorsStatus(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/$id<[^/]+>/metadata""","""@api.Extractions@.fetch(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/extractions/$id<[^/]+>/statuses""","""@api.Extractions@.checkExtractionsStatuses(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/previews/$p_id<[^/]+>""","""@api.Files@.attachPreview(id:UUID, p_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/previewJson/$file<[^/]+>""","""@api.Files@.getWithPreviewUrls(file:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/listpreviews""","""@api.Files@.filePreviewsList(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/getPreviews""","""@api.Files@.getPreviews(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>/isBeingProcessed""","""@api.Files@.isBeingProcessed(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$three_d_file_id<[^/]+>/$filename<[^/]+>""","""@api.Files@.getTexture(three_d_file_id:UUID, filename:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/queries/$id<[^/]+>""","""@api.Files@.downloadquery(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$id<[^/]+>""","""@api.Files@.download(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/dumpFilesMd""","""@api.Files@.dumpFilesMetadata"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/dumpDatasetsMd""","""@api.Datasets@.dumpDatasetsMetadata"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/dumpDatasetGroupings""","""@api.Datasets@.dumpDatasetGroupings"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/files/$three_d_file_id<[^/]+>/$filename<[^/]+>""","""@api.Files@.getTexture(three_d_file_id:UUID, filename:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces""","""@api.Spaces@.list(title:Option[String] ?= None, date:Option[String] ?= None, limit:Int ?= 12)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/canEdit""","""@api.Spaces@.listCanEdit(title:Option[String] ?= None, date:Option[String] ?= None, limit:Int ?= 12)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>""","""@api.Spaces@.get(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces""","""@api.Spaces@.createSpace()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>""","""@api.Spaces@.removeSpace(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$spaceId<[^/]+>/removeCollection/$collectionId<[^/]+>""","""@api.Spaces@.removeCollection(spaceId:UUID, collectionId:UUID, removeDatasets:Boolean)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$spaceId<[^/]+>/removeDataset/$datasetId<[^/]+>""","""@api.Spaces@.removeDataset(spaceId:UUID, datasetId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$spaceId<[^/]+>/copyContentsToSpace/$id<[^/]+>""","""@api.Spaces@.copyContentsToSpace(spaceId:UUID, id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$spaceId<[^/]+>/copyContentsToNewSpace""","""@api.Spaces@.copyContentsToNewSpace(spaceId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/update""","""@api.Spaces@.updateSpace(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/updateUsers""","""@api.Spaces@.updateUsers(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/acceptRequest""","""@api.Spaces@.acceptRequest(id:UUID, user:String, role:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/rejectRequest""","""@api.Spaces@.rejectRequest(id:UUID, user:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/removeUser""","""@api.Spaces@.removeUser(id:UUID, removeUser:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$spaceId<[^/]+>/addDatasetToSpace/$datasetId<[^/]+>""","""@api.Spaces@.addDatasetToSpace(spaceId:UUID, datasetId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$spaceId<[^/]+>/addCollectionToSpace/$collectionId<[^/]+>""","""@api.Spaces@.addCollectionToSpace(spaceId:UUID, collectionId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/follow""","""@api.Spaces@.follow(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/unfollow""","""@api.Spaces@.unfollow(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/metadata""","""@api.Metadata@.addDefinitionToSpace(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/verify""","""@api.Spaces@.verifySpace(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/datasets""","""@api.Spaces@.listDatasets(id:UUID, limit:Integer ?= 0)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/collections""","""@api.Spaces@.listCollections(id:UUID, limit:Integer ?= 0)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/collectionsCanEdit""","""@api.Spaces@.listCollectionsCanEdit(id:UUID, limit:Integer ?= 0)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/spaces/$id<[^/]+>/userSpaceRoleMap""","""@api.Spaces@.getUserSpaceRoleMap(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections""","""@api.Collections@.list(title:Option[String] ?= None, date:Option[String] ?= None, limit:Int ?= 12, exact:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/canEdit""","""@api.Collections@.listCanEdit(title:Option[String] ?= None, date:Option[String] ?= None, limit:Int ?= 12, exact:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/datasetPossibleParents/$ds_id<[^/]+>""","""@api.Collections@.addDatasetToCollectionOptions(ds_id:UUID, title:Option[String] ?= None, date:Option[String] ?= None, limit:Int ?= 12, exact:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/possibleParents""","""@api.Collections@.listPossibleParents(currentCollectionId:String, title:Option[String] ?= None, date:Option[String] ?= None, limit:Int ?= 12, exact:Boolean ?= false)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections""","""@api.Collections@.createCollection()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/rootCollections""","""@api.Collections@.getRootCollections"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/topLevelCollections""","""@api.Collections@.getTopLevelCollections"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/allCollections""","""@api.Collections@.getAllCollections(limit:Int ?= 0, showAll:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$id<[^/]+>/download""","""@api.Collections@.download(id:UUID, compression:Int ?= -1)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/listTrash""","""@api.Collections@.listCollectionsInTrash(limit:Int ?= 12)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/emptyTrash""","""@api.Collections@.emptyTrash()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/clearOldCollectionsTrash""","""@api.Collections@.clearOldCollectionsTrash(days:Int ?= 30)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/restore/$collectionId<[^/]+>""","""@api.Collections@.restoreCollection(collectionId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/uploadZipToSpace""","""@api.Collections@.uploadZipToSpace(spaceId:Option[String] ?= None)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$id<[^/]+>/moveChildCollection""","""@api.Collections@.moveChildCollection(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$collectionId<[^/]+>/deleteCollectionAndContents""","""@api.Collections@.removeCollectionAndContents(collectionId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$oldCollection<[^/]+>/moveDataset/$dataset<[^/]+>/newCollection/$newCollection<[^/]+>""","""@api.Collections@.moveDatasetToNewCollection(oldCollection:String, dataset:String, newCollection:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/list""","""@api.Collections@.list(title:Option[String] ?= None, date:Option[String] ?= None, limit:Int ?= 12, exact:Boolean ?= false)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$id<[^/]+>/follow""","""@api.Collections@.follow(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$id<[^/]+>/unfollow""","""@api.Collections@.unfollow(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/datasets""","""@api.Datasets@.listInCollection(coll_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/datasets/$ds_id<[^/]+>""","""@api.Collections@.attachDataset(coll_id:UUID, ds_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/datasetsRemove/$ds_id<[^/]+>/$ignoreNotFound<[^/]+>""","""@api.Collections@.removeDataset(coll_id:UUID, ds_id:UUID, ignoreNotFound:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/datasets/$ds_id<[^/]+>""","""@api.Collections@.removeDataset(coll_id:UUID, ds_id:UUID, ignoreNotFound:String ?= "True")"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/remove""","""@api.Collections@.removeCollection(coll_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/reindex""","""@api.Collections@.reindex(coll_id:UUID, recursive:Boolean ?= true)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/getDatasets""","""@api.Datasets@.listInCollection(coll_id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>""","""@api.Collections@.removeCollection(coll_id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/deleteContents/$userid<[^/]+>""","""@api.Collections@.deleteCollectionAndContents(coll_id:UUID, userid:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$c_id<[^/]+>/previews/$p_id<[^/]+>""","""@api.Collections@.attachPreview(c_id:UUID, p_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>""","""@api.Collections@.getCollection(coll_id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/title""","""@api.Collections@.updateCollectionName(coll_id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/description""","""@api.Collections@.updateCollectionDescription(coll_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/removeSubCollection/$sub_coll_id<[^/]+>""","""@api.Collections@.removeSubCollection(coll_id:UUID, sub_coll_id:UUID, ignoreNotFound:String ?= "True")"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/rootFlag/$spaceId<[^/]+>""","""@api.Collections@.setRootSpace(coll_id:UUID, spaceId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/unsetRootFlag/$spaceId<[^/]+>""","""@api.Collections@.unsetRootSpace(coll_id:UUID, spaceId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/newCollectionWithParent""","""@api.Collections@.createCollectionWithParent"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/getChildCollectionIds""","""@api.Collections@.getChildCollectionIds(coll_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/getChildCollections""","""@api.Collections@.getChildCollections(coll_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/getParentCollectionIds""","""@api.Collections@.getParentCollectionIds(coll_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/getParentCollections""","""@api.Collections@.getParentCollections(coll_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/addSubCollection/$sub_coll_id<[^/]+>""","""@api.Collections@.attachSubCollection(coll_id:UUID, sub_coll_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/removeFromSpaceAllowed/$space_id<[^/]+>""","""@api.Collections@.removeFromSpaceAllowed(coll_id:UUID, space_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/changeOwner/$userId<[^/]+>""","""@api.Collections@.changeOwner(coll_id:UUID, userId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$coll_id<[^/]+>/changeOwnerCollection/$userId<[^/]+>""","""@api.Collections@.changeOwnerCollectionOnly(coll_id:UUID, userId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$ds_id<[^/]+>/changeOwnerDataset/$userId<[^/]+>""","""@api.Collections@.changeOwnerDatasetOnly(ds_id:UUID, userId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/collections/$file_id<[^/]+>/changeOwnerFile/$userId<[^/]+>""","""@api.Collections@.changeOwnerFileOnly(file_id:UUID, userId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets""","""@api.Datasets@.list(title:Option[String] ?= None, date:Option[String] ?= None, limit:Int ?= 12, exact:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/canEdit""","""@api.Datasets@.listCanEdit(title:Option[String] ?= None, date:Option[String] ?= None, limit:Int ?= 12, exact:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/moveFileToDataset""","""@api.Datasets@.listMoveFileToDataset(file_id:UUID, title:Option[String] ?= None, limit:Int ?= 12, exact:Boolean ?= false)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets""","""@api.Datasets@.createDataset"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/newVocabulary/$datasetId<[^/]+>""","""@api.Datasets@.createEmptyVocabularyForDataset(datasetId:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/trash/$id<[^/]+>""","""@api.Datasets@.markDatasetAsTrash(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/restore/$id<[^/]+>""","""@api.Datasets@.restoreDataset(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/emptytrash""","""@api.Datasets@.emptyTrash()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/createempty""","""@api.Datasets@.createEmptyDataset"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/attachmultiple""","""@api.Datasets@.attachMultipleFiles"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/searchusermetadata""","""@api.Datasets@.searchDatasetsUserMetadata"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/searchmetadata""","""@api.Datasets@.searchDatasetsGeneralMetadata"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/listOutsideCollection/$coll_id<[^/]+>""","""@api.Datasets@.listOutsideCollection(coll_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/listTrash""","""@api.Datasets@.listDatasetsInTrash(limit:Int ?= 12)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/emptyTrash""","""@api.Datasets@.emptyTrash()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/clearOldDatasetsTrash""","""@api.Datasets@.clearOldDatasetsTrash(days:Int ?= 30)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/restore/$id<[^/]+>""","""@api.Datasets@.restoreDataset(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$ds_id<[^/]+>/filesRemove/$file_id<[^/]+>/$ignoreNotFound<[^/]+>""","""@api.Datasets@.detachFile(ds_id:UUID, file_id:UUID, ignoreNotFound:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$datasetId<[^/]+>/moveFile/$toFolder<[^/]+>/$fileId<[^/]+>""","""@api.Folders@.moveFileBetweenFolders(datasetId:UUID, toFolder:UUID, fileId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$datasetId<[^/]+>/moveBetweenDatasets/$toDataset<[^/]+>/$fileId<[^/]+>""","""@api.Datasets@.moveFileBetweenDatasets(datasetId:UUID, toDataset:UUID, fileId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$datasetId<[^/]+>/moveToDataset/$fromFolder<[^/]+>/$fileId<[^/]+>""","""@api.Folders@.moveFileToDataset(datasetId:UUID, fromFolder:UUID, fileId:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$parentDatasetId<[^/]+>/deleteFolder/$folderId<[^/]+>""","""@api.Folders@.deleteFolder(parentDatasetId:UUID, folderId:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$parentDatasetId<[^/]+>/updateName/$folderId<[^/]+>""","""@api.Folders@.updateFolderName(parentDatasetId:UUID, folderId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$datasetId<[^/]+>/copyDatasetToSpace/$spaceId<[^/]+>""","""@api.Datasets@.copyDatasetToSpace(datasetId:UUID, spaceId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$datasetId<[^/]+>/copyDatasetToSpaceNotAuthor/$spaceId<[^/]+>""","""@api.Datasets@.copyDatasetToSpaceNotAuthor(datasetId:UUID, spaceId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$datasetId<[^/]+>/folders""","""@api.Folders@.getAllFoldersByDatasetId(datasetId:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$ds_id<[^/]+>/$file_id<[^/]+>""","""@api.Datasets@.detachFile(ds_id:UUID, file_id:UUID, ignoreNotFound:String ?= "True")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/getRDFURLsForDataset/$id<[^/]+>""","""@api.Datasets@.getRDFURLsForDataset(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/rdfUserMetadata/$id<[^/]+>""","""@api.Datasets@.getRDFUserMetadata(id:UUID, mappingNum:String ?= "1")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/metadata""","""@api.Datasets@.getMetadataDefinitions(id:UUID, space:Option[String] ?= None)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/metadata""","""@api.Datasets@.addMetadata(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/usermetadata""","""@api.Datasets@.addUserMetadata(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/technicalmetadatajson""","""@api.Datasets@.getTechnicalMetadataJSON(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/xmlmetadatajson""","""@api.Datasets@.getXMLMetadataJSON(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/usermetadatajson""","""@api.Datasets@.getUserMetadataJSON(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/listFiles""","""@api.Datasets@.datasetFilesList(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/listAllFiles""","""@api.Datasets@.datasetAllFilesList(id:UUID, max:Option[Int] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/files""","""@api.Datasets@.datasetAllFilesList(id:UUID, max:Option[Int] ?= None)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/files""","""@api.Datasets@.uploadToDatasetFile(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/urls""","""@api.Datasets@.uploadToDatasetJSON(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/download""","""@api.Datasets@.download(id:UUID, compression:Int ?= -1)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/comment""","""@api.Datasets@.comment(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/reindex""","""@api.Datasets@.reindex(id:UUID, recursive:Boolean ?= true)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/follow""","""@api.Datasets@.follow(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/unfollow""","""@api.Datasets@.unfollow(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/removeTag""","""@api.Datasets@.removeTag(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/tags""","""@api.Datasets@.getTags(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/tags""","""@api.Datasets@.addTags(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/tags/remove""","""@api.Datasets@.removeTags(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/tags/remove_all""","""@api.Datasets@.removeAllTags(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/tags""","""@api.Datasets@.removeTags(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/title""","""@api.Datasets@.updateName(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/description""","""@api.Datasets@.updateDescription(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/creator""","""@api.Datasets@.addCreator(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/creator/remove""","""@api.Datasets@.removeCreator(id:UUID, creator:String)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/creator/reorder""","""@api.Datasets@.moveCreator(id:UUID, creator:String, newPos:Int)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/editing""","""@api.Datasets@.updateInformation(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/license""","""@api.Datasets@.updateLicense(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/isBeingProcessed""","""@api.Datasets@.isBeingProcessed(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/getPreviews""","""@api.Datasets@.getPreviews(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$ds_id<[^/]+>/files/$file_id<[^/]+>""","""@api.Datasets@.attachExistingFile(ds_id:UUID, file_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>""","""@api.Datasets@.get(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>""","""@api.Datasets@.deleteDataset(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/detachdelete""","""@api.Datasets@.detachAndDeleteDataset(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$parentDatasetId<[^/]+>/newFolder""","""@api.Folders@.createFolder(parentDatasetId:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/access""","""@api.Datasets@.updateAccess(id:UUID, aceess:String ?= "PRIVATE")"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/addFileEvent""","""@api.Datasets@.addFileEvent(id:UUID, inFolder:Boolean, fileCount:Int)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/users""","""@api.Datasets@.users(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$datasetId<[^/]+>/changeOwner/$userId<[^/]+>""","""@api.Datasets@.changeOwner(datasetId:UUID, userId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/$id<[^/]+>/moveCollection""","""@api.Datasets@.moveDataset(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/datasets/hasVocabulary/$datasetId<[^/]+>""","""@api.Datasets@.hasAttachedVocabulary(datasetId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """vocabularies/new""","""@controllers.Vocabularies@.newVocabulary(space:Option[String])"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """vocabularies/submit""","""@controllers.Vocabularies@.submit()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """vocabularies/$id<[^/]+>""","""@controllers.Vocabularies@.vocabulary(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$preview_id<[^/]+>/textures/dataset/$datasetid<[^/]+>/json""","""@api.Previews@.downloadPreview(preview_id:UUID, datasetid:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$preview_id<[^/]+>/textures/dataset/$dataset_id<[^/]+>//$filename<[^/]+>""","""@api.Files@.downloadByDatasetAndFilename(dataset_id:UUID, filename:String, preview_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$dzi_id_dir<[^/]+>/$level<[^/]+>/$filename<[^/]+>""","""@api.Previews@.getTile(dzi_id_dir:String, level:String, filename:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$dzi_id<[^/]+>/tiles/$tile_id<[^/]+>/$level<[^/]+>""","""@api.Previews@.attachTile(dzi_id:UUID, tile_id:UUID, level:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$id<[^/]+>/metadata""","""@api.Previews@.uploadMetadata(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$id<[^/]+>/metadata""","""@api.Previews@.getMetadata(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$id<[^/]+>/annotationAdd""","""@api.Previews@.attachAnnotation(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$id<[^/]+>/annotationEdit""","""@api.Previews@.editAnnotation(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$id<[^/]+>/annotationsList""","""@api.Previews@.listAnnotations(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$id<[^/]+>/title""","""@api.Previews@.setTitle(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews""","""@api.Previews@.list"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$id<[^/]+>""","""@api.Previews@.download(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews/$id<[^/]+>""","""@api.Previews@.removePreview(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/previews""","""@api.Previews@.upload(iipKey:String ?= "")"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/indexes""","""@api.Indexes@.index"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/indexes/features""","""@api.Indexes@.features"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections""","""@api.Sections@.add"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections/$id<[^/]+>/comments""","""@api.Sections@.comment(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections/$id<[^/]+>/description""","""@api.Sections@.description(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections/$id<[^/]+>/thumbnails/$thumbnail_id<[^/]+>""","""@api.Sections@.attachThumbnail(id:UUID, thumbnail_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections/$id<[^/]+>/tags""","""@api.Sections@.getTags(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections/$id<[^/]+>/tags""","""@api.Sections@.addTags(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections/$id<[^/]+>/tags/remove""","""@api.Sections@.removeTags(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections/$id<[^/]+>/tags/remove_all""","""@api.Sections@.removeAllTags(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections/$id<[^/]+>/tags""","""@api.Sections@.removeTags(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections/$id<[^/]+>""","""@api.Sections@.get(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sections/$id<[^/]+>""","""@api.Sections@.delete(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/search/json""","""@api.Search@.searchJson(query:String ?= "", grouping:String ?= "AND", from:Option[Int], size:Option[Int])"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/search/multimediasearch""","""@api.Search@.searchMultimediaIndex(section_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/search""","""@api.Search@.search(query:String ?= "")"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/datapoints""","""api.Geostreams.addDatapoint(invalidateCache:Boolean ?= true)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/datapoints/bulk""","""api.Geostreams.addDatapoints(invalidateCache:Boolean ?= true)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/datapoints/$id<[^/]+>""","""api.Geostreams.deleteDatapoint(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/datapoints""","""api.Geostreams.searchDatapoints(operator:String = "", since:Option[String] ?= None, until:Option[String] ?= None, geocode:Option[String] ?= None, stream_id:Option[String] ?= None, sensor_id:Option[String] ?= None, sources:List[String] ?= List.empty, attributes:List[String] ?= List.empty, format:String ?= "json", semi:Option[String], onlyCount:Boolean ?= false, window_start:Option[String] = None, window_end:Option[String] = None, binning:String ?= "semi", geojson:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/datapoints/averages""","""api.Geostreams.searchDatapoints(operator:String = "averages", since:Option[String] ?= None, until:Option[String] ?= None, geocode:Option[String] ?= None, stream_id:Option[String] ?= None, sensor_id:Option[String] ?= None, sources:List[String] ?= List.empty, attributes:List[String] ?= List.empty, format:String ?= "json", semi:Option[String], onlyCount:Boolean ?= false, window_start:Option[String] = None, window_end:Option[String] = None, binning:String ?= "semi", geojson:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/datapoints/trends""","""api.Geostreams.searchDatapoints(operator:String = "trends", since:Option[String] ?= None, until:Option[String] ?= None, geocode:Option[String] ?= None, stream_id:Option[String] ?= None, sensor_id:Option[String] ?= None, sources:List[String] ?= List.empty, attributes:List[String] ?= List.empty, format:String ?= "json", semi:Option[String], onlyCount:Boolean ?= false, window_start:Option[String] ?= None, window_end:Option[String] ?= None, binning:String ?= "semi", geojson:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/datapoints/bin/$time<[^/]+>/$depth<[^/]+>""","""api.Geostreams.binDatapoints(time:String, depth:Double, raw:Boolean ?= false, since:Option[String] ?= None, until:Option[String] ?= None, geocode:Option[String] ?= None, stream_id:Option[String] ?= None, sensor_id:Option[String] ?= None, sources:List[String] ?= List.empty, attributes:List[String] ?= List.empty)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/datapoints/$id<[^/]+>""","""api.Geostreams.getDatapoint(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/cache""","""api.Geostreams.cacheListAction"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/cache/invalidate""","""api.Geostreams.cacheInvalidateAction(sensor_id:Option[String] ?= None, stream_id:Option[String] ?= None)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/cache/$id<[^/]+>""","""api.Geostreams.cacheFetchAction(id:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/sensors""","""api.Geostreams.createSensor"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/sensors/update""","""api.Geostreams.updateStatisticsStreamSensor()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/sensors/$id<[^/]+>""","""api.Geostreams.getSensor(id:String)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/sensors/$id<[^/]+>""","""api.Geostreams.updateSensorMetadata(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/sensors/$id<[^/]+>/stats""","""api.Geostreams.getSensorStatistics(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/sensors/$id<[^/]+>/streams""","""api.Geostreams.getSensorStreams(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/sensors/$id<[^/]+>/update""","""api.Geostreams.updateStatisticsSensor(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/sensors""","""api.Geostreams.searchSensors(geocode:Option[String] ?= None, sensor_name:Option[String] ?= None, geojson:Option[String] ?= None)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/sensors/$id<[^/]+>""","""api.Geostreams.deleteSensor(id:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/streams""","""api.Geostreams.createStream"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/streams/update""","""api.Geostreams.updateStatisticsStreamSensor()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/streams/$id<[^/]+>""","""api.Geostreams.getStream(id:String)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/streams/$id<[^/]+>""","""api.Geostreams.patchStreamMetadata(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/streams/$id<[^/]+>/update""","""api.Geostreams.updateStatisticsStream(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/streams""","""api.Geostreams.searchStreams(geocode:Option[String] ?= None, stream_name:Option[String] ?= None, geojson:Option[String] ?= None)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/streams/$id<[^/]+>""","""api.Geostreams.deleteStream(id:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/dropall""","""api.Geostreams.deleteAll"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/counts""","""api.Geostreams.counts"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geostreams/config""","""api.Geostreams.getConfig"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/fileThumbnail""","""@api.Thumbnails@.uploadThumbnail"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/thumbnails""","""@api.Thumbnails@.list"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/thumbnails/$id<[^/]+>""","""@api.Thumbnails@.removeThumbnail(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sensors""","""api.Sensors.list"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sensors""","""api.Sensors.add"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sensors/$id<[^/]+>""","""api.Sensors.get(id:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sensors/search""","""api.Sensors.search"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sensors/$id<[^/]+>""","""api.Sensors.delete(id:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comment/mention""","""@api.Comments@.mentionInComment(userID:UUID, resourceID:UUID, resourceName:String, resourceType:String, commenterId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comment/$id<[^/]+>""","""@api.Comments@.comment(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comment/$id<[^/]+>/removeComment""","""@api.Comments@.removeComment(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/comment/$id<[^/]+>/editComment""","""@api.Comments@.editComment(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """selected""","""@controllers.Selected@.get"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/selected""","""@api.Selected@.get"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/selected""","""@api.Selected@.add"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/selected/remove""","""@api.Selected@.remove"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/selected/files""","""@api.Selected@.deleteAll"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/selected/files""","""@api.Selected@.downloadAll"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/selected/clear""","""@api.Selected@.clearAll"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/selected/tag""","""@api.Selected@.tagAll(tags:List[String])"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/relations""","""@api.Relations@.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/relations/search""","""@api.Relations@.findTargets(sourceId:String, sourceType:String, targetType:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/relations/$id<[^/]+>""","""@api.Relations@.get(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/relations""","""@api.Relations@.add()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/relations/$id<[^/]+>""","""@api.Relations@.delete(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/tiles""","""@api.ZoomIt@.uploadTile"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/geometries""","""@api.Geometry@.uploadGeometry"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/3dTextures""","""@api.ThreeDTexture@.uploadTexture"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/search""","""@api.Search@.search(query:String ?= "")"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/sparqlquery""","""@api.Search@.querySPARQL"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/projects/addproject""","""@api.Projects@.addproject(project:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/institutions/addinstitution""","""@api.Institutions@.addinstitution(institution:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users""","""@api.Users@.list"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/email/$email<[^/]+>""","""@api.Users@.findByEmail(email:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/$id<[^/]+>""","""@api.Users@.deleteUser(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/me""","""@api.Users@.getUser()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/$id<[^/]+>/updateName""","""@api.Users@.updateName(id:UUID, firstName:String, lastName:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/keys""","""@api.Users@.keysList()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/keys/$name<[^/]+>""","""@api.Users@.keysGet(name:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/keys""","""@api.Users@.keysAdd(name:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/keys/$key<[^/]+>""","""@api.Users@.keysDelete(key:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/addUserDatasetView""","""@api.Users@.addUserDatasetView(email:String, dataset:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/createNewListInUser""","""@api.Users@.createNewListInUser(email:String, field:String, fieldList:List[String])"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/createNewListInUser""","""@api.Users@.createNewListInUser(email:String, field:String, fieldList:List[String])"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/follow""","""@api.Users@.follow(followeeUUID:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/unfollow""","""@api.Users@.unfollow(followeeUUID:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/curations/$curationId<[^/]+>/ore""","""@api.CurationObjects@.getCurationObjectOre(curationId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/users/$id<[^/]+>""","""@api.Users@.findById(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/followers""","""@controllers.Users@.getFollowers(index:Int ?= 0, limit:Int ?= 12)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/following""","""@controllers.Users@.getFollowing(index:Int ?= 0, limit:Int ?= 12)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/version""","""@api.Status@.version"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/status""","""@api.Status@.status"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """rss""","""@controllers.RSS@.siteRSS(limit:Option[Int])"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """rss/$etype<[^/]+>""","""@controllers.RSS@.siteRSSOfType(limit:Option[Int], etype:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """dataset/$id<[^/]+>/curations/new""","""@controllers.CurationObjects@.newCO(id:UUID, space:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """space/curations""","""@controllers.CurationObjects@.list(when:String ?= "", date:String ?= "", limit:Int ?= 4, space:Option[String] ?= None)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """dataset/$id<[^/]+>/curations/spaces/$spaceId<[^/]+>/submit""","""@controllers.CurationObjects@.submit(id:UUID, spaceId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/$id<[^/]+>/stagingArea""","""@controllers.Spaces@.stagingArea(id:UUID, index:Int ?= 0, limit:Int ?= 12)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>""","""@controllers.CurationObjects@.getCurationObject(id:UUID, limit:Int ?= 5)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/retract/$id<[^/]+>""","""@api.CurationObjects@.retractCurationObject(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/edit""","""@controllers.CurationObjects@.editCuration(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/spaces/$spaceId<[^/]+>/update""","""@controllers.CurationObjects@.updateCuration(id:UUID, spaceId:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>""","""@controllers.CurationObjects@.deleteCuration(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/compareToRepository""","""@controllers.CurationObjects@.compareToRepository(id:UUID, repository:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/submitRepositorySelection""","""@controllers.CurationObjects@.submitRepositorySelection(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/sendToRepository""","""@controllers.CurationObjects@.sendToRepository(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/matchmaker""","""@controllers.CurationObjects@.findMatchingRepositories(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/matchmaker""","""@api.CurationObjects@.findMatchmakingRepositories(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/curationFile""","""@api.CurationObjects@.getCurationFiles(id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/status""","""@api.CurationObjects@.savePublishedObject(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/getStatusFromRepository""","""@controllers.CurationObjects@.getStatusFromRepository(id:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/files/$curationFileId<[^/]+>""","""@api.CurationObjects@.deleteCurationFile(id:UUID, parentId:UUID, curationFileId:UUID)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/folders/$curationFolderId<[^/]+>""","""@api.CurationObjects@.deleteCurationFolder(id:UUID, parentId:UUID, curationFolderId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/updatedFilesAndFolders""","""@controllers.CurationObjects@.getUpdatedFilesAndFolders(id:UUID, curationFolderId:String, limit:Int, pageIndex:Int)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/getMetadataDefinitionsByFile""","""@api.CurationObjects@.getMetadataDefinitionsByFile(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """spaces/curations/$id<[^/]+>/metadataDefinitions""","""@api.CurationObjects@.getMetadataDefinitions(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """publishedData""","""@controllers.CurationObjects@.getPublishedData(space:String = "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """publishedData/$space<[^/]+>""","""@controllers.CurationObjects@.getPublishedData(space:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """sendEmail""","""@api.Events@.sendExceptionEmail()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """getEvents""","""@controllers.Events@.getEvents(index:Int)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabularies/list""","""@api.Vocabularies@.list()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabularies/createVocabularyFromJson""","""@api.Vocabularies@.createVocabularyFromJson(isPublic:Boolean ?= false)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabularies/createVocabulary""","""@api.Vocabularies@.createVocabularyFromForm()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabularies/getByAuthor""","""@api.Vocabularies@.getByAuthor()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabularies/getPublic""","""@api.Vocabularies@.getPublicVocabularies()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabularies/$vocab_id<[^/]+>""","""@api.Vocabularies@.get(vocab_id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/editVocabulary/$vocab_id<[^/]+>""","""@api.Vocabularies@.editVocabulary(vocab_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabularies/$name<[^/]+>""","""@api.Vocabularies@.getByName(name:String)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabularies/$vocab_id<[^/]+>/editVocabulary""","""@api.Vocabularies@.editVocabulary(vocab_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabularies/$name<[^/]+>/getByNameAuthor""","""@api.Vocabularies@.getByNameAndAuthor(name:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabularies/$vocab_id<[^/]+>/addToSpace/$space_id<[^/]+>""","""@api.Vocabularies@.addToSpace(vocab_id:UUID, space_id:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabuliaries/$vocab_id<[^/]+>/removeFromSpace/$space_id<[^/]+>""","""@api.Vocabularies@.removeFromSpace(vocab_id:UUID, space_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabterms/list""","""@api.VocabularyTerms@.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/vocabterms/$id<[^/]+>""","""@api.VocabularyTerms@.get(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>""","""@api.Proxy@.get(endpoint_key:String, pathSuffix:String = null)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>/""","""@api.Proxy@.get(endpoint_key:String, pathSuffix:String = null)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>/$pathSuffix<.+>""","""@api.Proxy@.get(endpoint_key:String, pathSuffix:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>""","""@api.Proxy@.post(endpoint_key:String, pathSuffix:String = null)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>/""","""@api.Proxy@.post(endpoint_key:String, pathSuffix:String = null)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>/$pathSuffix<.+>""","""@api.Proxy@.post(endpoint_key:String, pathSuffix:String)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>""","""@api.Proxy@.put(endpoint_key:String, pathSuffix:String = null)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>/""","""@api.Proxy@.put(endpoint_key:String, pathSuffix:String = null)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>/$pathSuffix<.+>""","""@api.Proxy@.put(endpoint_key:String, pathSuffix:String)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>""","""@api.Proxy@.delete(endpoint_key:String, pathSuffix:String = null)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>/""","""@api.Proxy@.delete(endpoint_key:String, pathSuffix:String = null)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/proxy/$endpoint_key<[^/]+>/$pathSuffix<.+>""","""@api.Proxy@.delete(endpoint_key:String, pathSuffix:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/createExperimentTemplate""","""@api.Vocabularies@.createVocabularyFromJson(isPublic:Boolean ?= false)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/createExperimentTemplateFromJson""","""@api.Vocabularies@.createVocabularyFromJson(isPublic:Boolean ?= false)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/createExperimentTemplateDefaultValues""","""@api.Vocabularies@.createVocabularyFromJson(isPublic:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/listExperimentTemplates""","""@api.Vocabularies@.list()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/allExperimentTemplates""","""@api.Vocabularies@.listAll()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/getPublic""","""@api.Vocabularies@.getPublicVocabularies()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/allTags""","""@api.Vocabularies@.getAllTagsOfAllVocabularies()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/listMyDatasets""","""@api.T2C2@.listMyDatasets(limit:Int ?= 20)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/getExperimentTemplateById/$id<[^/]+>""","""@api.T2C2@.getVocabulary(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/getExperimentTemplateByName/$name<[^/]+>""","""@api.Vocabularies@.getByName(name:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/findByTag""","""@api.Vocabularies@.getByTag(containsAll:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/findByTag/$tag<[^/]+>""","""@api.Vocabularies@.getBySingleTag(tag:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/getByDatasetId/$dataset_id<[^/]+>""","""@api.T2C2@.getVocabByDatasetId(dataset_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/getByFileId/$file_id<[^/]+>""","""@api.T2C2@.getVocabByFileId(file_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/listMyDatasets""","""@api.T2C2@.listMyDatasets(limit:Int ?= 20)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/listMySpaces""","""@api.T2C2@.listMySpaces(limit:Int ?= 20)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/deleteTemplate/$id<[^/]+>""","""@api.Vocabularies@.removeVocabulary(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/makePublic/$id<[^/]+>""","""@api.T2C2@.makeTemplatePublic(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/makePrivate/$id<[^/]+>""","""@api.T2C2@.makeTemplatePrivate(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/editTemplate/$id<[^/]+>""","""@api.Vocabularies@.editVocabulary(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/$vocab_id<[^/]+>/attachToDataset/$dataset_id<[^/]+>""","""@api.T2C2@.attachVocabToDataset(vocab_id:UUID, dataset_id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/$vocab_id<[^/]+>/detachFromDataset/$dataset_id<[^/]+>""","""@api.T2C2@.detachVocabFromDataset(vocab_id:UUID, dataset_id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/$vocab_id<[^/]+>/attachToFile/$file_id<[^/]+>""","""@api.T2C2@.attachVocabToFile(vocab_id:UUID, file_id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/$vocab_id<[^/]+>/detachFromFile/$file_id<[^/]+>""","""@api.T2C2@.detachVocabFromFile(vocab_id:UUID, file_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/spaces/$id<[^/]+>/collectionsCanEdit""","""@api.T2C2@.listCollectionsCanEdit(id:UUID, limit:Integer ?= 0)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/new""","""@controllers.T2C2@.newTemplate(space:Option[String])"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/notebook/submit""","""@api.Notebooks@.submit()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/zipUpload""","""@controllers.T2C2@.uploadFile()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/findYoungestChild""","""@api.T2C2@.findYoungestChild()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/allCollection""","""@api.T2C2@.getAllCollectionsOfUser()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/allCollectionDatasetFiles""","""@api.T2C2@.getAllCollectionsWithDatasetIdsAndFiles()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/collectionsForTree""","""@api.T2C2@.getAllCollectionsForTree()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/getLevelOfTree""","""@api.T2C2@.getLevelOfTree(currentId:Option[String] ?= None, currentType:String ?= "collection")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/getLevelOfTreeSharedWithMe""","""@api.T2C2@.getLevelOfTreeSharedWithMe(currentId:Option[String] ?= None, currentType:String ?= "collection")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/getLevelOfTreeSharedWithOthers""","""@api.T2C2@.getLevelOfTreeSharedWithOthers(currentId:Option[String] ?= None, currentType:String ?= "collection")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/getLevelOfTreeNotShared""","""@api.T2C2@.getLevelOfTreeNotSharedInSpace(currentId:Option[String] ?= None, currentType:String ?= "collection")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/getLevelOfTreeInSpace""","""@api.T2C2@.getLevelOfTreeInSpace(currentId:Option[String] ?= None, currentType:String ?= "collection", spaceId:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/collectionsForFullTree""","""@api.T2C2@.getAllCollectionsForFullTree()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/allCollectionsNotSharedInSpace""","""@api.T2C2@.getAllCollectionsOfUserNotSharedInSpace()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/datasets/createEmpty""","""@api.T2C2@.createEmptyDataset()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/moveKeysToTerms""","""@api.T2C2@.moveKeysToTermsTemplates()"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/bulkDelete""","""@api.T2C2@.bulkDelete()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/getUsersICanUploadFor""","""@api.T2C2@.getWhoICanUploadFor()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/getSpacesOfUser/$userId<[^/]+>""","""@api.T2C2@.getSpacesOfUser(userId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/getSpacesUserCanEdit/$userId<[^/]+>""","""@api.T2C2@.getSpacesUserHasAccessTo(userId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/getUsersOfSpace/$spaceId<[^/]+>""","""@api.T2C2@.getUsersOfSpace(spaceId:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/getNumUsersOfSpace/$spaceId<[^/]+>""","""@api.T2C2@.getNumUsersOfSpace(spaceId:UUID)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/uploadToDataset/$id<[^/]+>""","""@api.Files@.uploadToDatasetWithDescription(id:UUID, showPreviews:String ?= "DatasetLevel", originalZipFile:String ?= "", flags:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/templates/lastTemplate""","""@api.T2C2@.getTemplateFromLastDataset(limit:Int ?= 10)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/datasets/getDatasetAndTemplate/$id<[^/]+>""","""@api.T2C2@.getDatasetWithAttachedVocab(id:UUID)"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/files/$id<[^/]+>/updateDescription""","""@api.Files@.updateDescription(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/collections/$coll_id<[^/]+>/datasets""","""@api.Datasets@.listInCollection(coll_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/collections/$coll_id<[^/]+>/datasetsWithParentColId""","""@api.T2C2@.getDatasetsInCollectionWithColId(coll_id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/t2c2/allCollectionsWithDatasetIds""","""@api.T2C2@.getAllCollectionsWithDatasetIds()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/getKeyValuesLastDataset""","""@api.T2C2@.getKeysValuesFromLastDataset()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/getKeyValuesForLastDatasets""","""@api.T2C2@.getKeysValuesFromLastDatasets(limit:Int ?= 10)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/getKeyValuesForDatasetId/$id<[^/]+>""","""@api.T2C2@.getKeysValuesFromDatasetId(id:UUID)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/getIdNameFromTag/$tag<[^/]+>""","""@api.T2C2@.getVocabIdNameFromTag(tag:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/uploader""","""@controllers.T2C2@.uploader()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """t2c2/zipUploader""","""@controllers.T2C2@.zipUploader()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/tree/getChildrenOfNode""","""@api.Tree@.getChildrenOfNode(nodeId:Option[String], nodeType:String, mine:Boolean ?= true, shared:Boolean ?= false, public:Boolean ?= false)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """api/fulltree/getChildrenOfNode""","""@api.FullTree@.getChildrenOfNode(nodeId:Option[String], nodeType:String, role:Option[String])""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:7
case api_ApiHelp_options0(params) => {
   call(params.fromPath[String]("path", None)) { (path) =>
        invokeHandler(api.ApiHelp.options(path), HandlerDef(this, "api.ApiHelp", "options", Seq(classOf[String]),"OPTIONS", """ ----------------------------------------------------------------------
 ROUTES
 This file defines all application routes (Higher priority routes first)
 ----------------------------------------------------------------------
make this the first rule
OPTIONS       /*path                                                                   @controllers.Application.options(path)""", Routes.prefix + """$path<.+>"""))
   }
}
        

// @LINE:8
case controllers_Application_untrail1(params) => {
   call(params.fromPath[String]("path", None)) { (path) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).untrail(path), HandlerDef(this, "controllers.Application", "untrail", Seq(classOf[String]),"GET", """""", Routes.prefix + """$path<.+>/"""))
   }
}
        

// @LINE:14
case controllers_Application_index2(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).index, HandlerDef(this, "controllers.Application", "index", Nil,"GET", """""", Routes.prefix + """"""))
   }
}
        

// @LINE:15
case controllers_Application_about3(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).about, HandlerDef(this, "controllers.Application", "about", Nil,"GET", """""", Routes.prefix + """about"""))
   }
}
        

// @LINE:16
case controllers_Application_tos4(params) => {
   call(params.fromQuery[Option[String]]("redirect", Some(None))) { (redirect) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).tos(redirect), HandlerDef(this, "controllers.Application", "tos", Seq(classOf[Option[String]]),"GET", """""", Routes.prefix + """tos"""))
   }
}
        

// @LINE:17
case controllers_Application_email5(params) => {
   call(params.fromQuery[String]("subject", Some(""))) { (subject) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).email(subject), HandlerDef(this, "controllers.Application", "email", Seq(classOf[String]),"GET", """""", Routes.prefix + """email"""))
   }
}
        

// @LINE:22
case controllers_Assets_at6(params) => {
   call(Param[String]("path", Right("/public")), Param[String]("file", Right("/jsonld/contexts/metadata.jsonld"))) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ ----------------------------------------------------------------------
 Map static resources from the /public folder to the /assets URL path
 ----------------------------------------------------------------------""", Routes.prefix + """contexts/metadata.jsonld"""))
   }
}
        

// @LINE:23
case controllers_Assets_at7(params) => {
   call(Param[String]("path", Right("/public")), Param[String]("file", Right("/images/glyphicons-halflings-white.png"))) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """assets/img/glyphicons-halflings-white.png"""))
   }
}
        

// @LINE:24
case controllers_Assets_at8(params) => {
   call(Param[String]("path", Right("/public")), Param[String]("file", Right("/images/glyphicons-halflings.png"))) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """assets/img/glyphicons-halflings.png"""))
   }
}
        

// @LINE:25
case controllers_Assets_at9(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        

// @LINE:30
case controllers_Users_getUsers10(params) => {
   call(params.fromQuery[String]("when", Some("")), params.fromQuery[String]("id", Some("")), params.fromQuery[Int]("limit", Some(24))) { (when, id, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Users]).getUsers(when, id, limit), HandlerDef(this, "controllers.Users", "getUsers", Seq(classOf[String], classOf[String], classOf[Int]),"GET", """ ----------------------------------------------------------------------
 USERS
 ----------------------------------------------------------------------""", Routes.prefix + """users"""))
   }
}
        

// @LINE:31
case controllers_Users_acceptTermsOfServices11(params) => {
   call(params.fromQuery[Option[String]]("redirect", Some(None))) { (redirect) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Users]).acceptTermsOfServices(redirect), HandlerDef(this, "controllers.Users", "acceptTermsOfServices", Seq(classOf[Option[String]]),"GET", """""", Routes.prefix + """users/acceptTermsOfServices"""))
   }
}
        

// @LINE:32
case controllers_Login_isLoggedIn12(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Login]).isLoggedIn, HandlerDef(this, "controllers.Login", "isLoggedIn", Nil,"GET", """""", Routes.prefix + """login/isLoggedIn"""))
   }
}
        

// @LINE:33
case controllers_Application_onlyGoogle13(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).onlyGoogle, HandlerDef(this, "controllers.Application", "onlyGoogle", Nil,"GET", """""", Routes.prefix + """login/isLoggedInWithGoogle"""))
   }
}
        

// @LINE:34
case controllers_Users_sendEmail14(params) => {
   call(params.fromQuery[String]("subject", None), params.fromQuery[String]("from", None), params.fromQuery[String]("recipient", None), params.fromQuery[String]("body", None)) { (subject, from, recipient, body) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Users]).sendEmail(subject, from, recipient, body), HandlerDef(this, "controllers.Users", "sendEmail", Seq(classOf[String], classOf[String], classOf[String], classOf[String]),"POST", """""", Routes.prefix + """users/sendEmail"""))
   }
}
        

// @LINE:35
case controllers_Login_ldap15(params) => {
   call(params.fromQuery[String]("redirecturl", None), params.fromQuery[String]("token", None)) { (redirecturl, token) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Login]).ldap(redirecturl, token), HandlerDef(this, "controllers.Login", "ldap", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """ldap"""))
   }
}
        

// @LINE:36
case controllers_Login_ldapAuthenticate16(params) => {
   call(params.fromQuery[String]("uid", None), params.fromQuery[String]("password", None)) { (uid, password) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Login]).ldapAuthenticate(uid, password), HandlerDef(this, "controllers.Login", "ldapAuthenticate", Seq(classOf[String], classOf[String]),"POST", """""", Routes.prefix + """ldap/authenticate"""))
   }
}
        

// @LINE:41
case controllers_Profile_viewProfile17(params) => {
   call(params.fromQuery[Option[String]]("email", None)) { (email) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Profile]).viewProfile(email), HandlerDef(this, "controllers.Profile", "viewProfile", Seq(classOf[Option[String]]),"GET", """ ----------------------------------------------------------------------
 PROFILE
 ----------------------------------------------------------------------
 deprecated use profile/:uuid""", Routes.prefix + """profile/viewProfile"""))
   }
}
        

// @LINE:43
case controllers_Profile_viewProfileUUID18(params) => {
   call(params.fromPath[UUID]("uuid", None)) { (uuid) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Profile]).viewProfileUUID(uuid), HandlerDef(this, "controllers.Profile", "viewProfileUUID", Seq(classOf[UUID]),"GET", """ deprecated use profile/:uuid""", Routes.prefix + """profile/viewProfile/$uuid<[^/]+>"""))
   }
}
        

// @LINE:44
case controllers_Profile_editProfile19(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Profile]).editProfile, HandlerDef(this, "controllers.Profile", "editProfile", Nil,"GET", """""", Routes.prefix + """profile/editProfile"""))
   }
}
        

// @LINE:45
case controllers_Profile_submitChanges20(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Profile]).submitChanges, HandlerDef(this, "controllers.Profile", "submitChanges", Nil,"POST", """""", Routes.prefix + """profile/submitChanges"""))
   }
}
        

// @LINE:46
case controllers_Profile_viewProfileUUID21(params) => {
   call(params.fromPath[UUID]("uuid", None)) { (uuid) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Profile]).viewProfileUUID(uuid), HandlerDef(this, "controllers.Profile", "viewProfileUUID", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """profile/$uuid<[^/]+>"""))
   }
}
        

// @LINE:51
case controllers_Error_authenticationRequired22(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Error]).authenticationRequired, HandlerDef(this, "controllers.Error", "authenticationRequired", Nil,"GET", """ ----------------------------------------------------------------------
 ERRORS
 ----------------------------------------------------------------------""", Routes.prefix + """error/authenticationRequired"""))
   }
}
        

// @LINE:52
case controllers_Error_authenticationRequiredMessage23(params) => {
   call(params.fromPath[String]("msg", None), params.fromPath[String]("url", None)) { (msg, url) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Error]).authenticationRequiredMessage(msg, url), HandlerDef(this, "controllers.Error", "authenticationRequiredMessage", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """error/authenticationRequiredMessage/$msg<[^/]+>/$url<[^/]+>"""))
   }
}
        

// @LINE:53
case controllers_Error_incorrectPermissions24(params) => {
   call(params.fromQuery[String]("msg", Some(null))) { (msg) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Error]).incorrectPermissions(msg), HandlerDef(this, "controllers.Error", "incorrectPermissions", Seq(classOf[String]),"GET", """""", Routes.prefix + """error/noPermissions"""))
   }
}
        

// @LINE:54
case controllers_Error_notActivated25(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Error]).notActivated, HandlerDef(this, "controllers.Error", "notActivated", Nil,"GET", """""", Routes.prefix + """error/notActivated"""))
   }
}
        

// @LINE:55
case controllers_Error_notAuthorized26(params) => {
   call(params.fromQuery[String]("msg", None), params.fromQuery[String]("id", None), params.fromQuery[String]("resourceType", None)) { (msg, id, resourceType) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Error]).notAuthorized(msg, id, resourceType), HandlerDef(this, "controllers.Error", "notAuthorized", Seq(classOf[String], classOf[String], classOf[String]),"GET", """""", Routes.prefix + """error/notAuthorized"""))
   }
}
        

// @LINE:60
case controllers_ExtractionInfo_getDTSRequests27(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getDTSRequests(), HandlerDef(this, "controllers.ExtractionInfo", "getDTSRequests", Nil,"GET", """ ----------------------------------------------------------------------
 DTS INFORMATION
 ----------------------------------------------------------------------""", Routes.prefix + """extractions/requests"""))
   }
}
        

// @LINE:61
case controllers_ExtractionInfo_getExtractorServersIP28(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getExtractorServersIP(), HandlerDef(this, "controllers.ExtractionInfo", "getExtractorServersIP", Nil,"GET", """""", Routes.prefix + """extractions/servers_ips"""))
   }
}
        

// @LINE:62
case controllers_ExtractionInfo_getExtractorNames29(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getExtractorNames(), HandlerDef(this, "controllers.ExtractionInfo", "getExtractorNames", Nil,"GET", """""", Routes.prefix + """extractions/extractors_names"""))
   }
}
        

// @LINE:63
case controllers_ExtractionInfo_getExtractorInputTypes30(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getExtractorInputTypes(), HandlerDef(this, "controllers.ExtractionInfo", "getExtractorInputTypes", Nil,"GET", """""", Routes.prefix + """extractions/supported_input_types"""))
   }
}
        

// @LINE:68
case controllers_ExtractionInfo_getBookmarkletPage31(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getBookmarkletPage(), HandlerDef(this, "controllers.ExtractionInfo", "getBookmarkletPage", Nil,"GET", """ ----------------------------------------------------------------------
 DTS BOOKMARKLET
 ----------------------------------------------------------------------v""", Routes.prefix + """bookmarklet"""))
   }
}
        

// @LINE:69
case controllers_Application_bookmarklet32(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).bookmarklet, HandlerDef(this, "controllers.Application", "bookmarklet", Nil,"GET", """""", Routes.prefix + """bookmarklet.js"""))
   }
}
        

// @LINE:74
case controllers_ExtractionInfo_getExtensionPage33(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ExtractionInfo]).getExtensionPage(), HandlerDef(this, "controllers.ExtractionInfo", "getExtensionPage", Nil,"GET", """ ----------------------------------------------------------------------
 DTS CHROME EXTENSIONS
 ----------------------------------------------------------------------""", Routes.prefix + """extensions/dts/chrome"""))
   }
}
        

// @LINE:76
case controllers_Files_extractFile34(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).extractFile, HandlerDef(this, "controllers.Files", "extractFile", Nil,"GET", """This may not require; This is only for testing. It will change in subsequent version""", Routes.prefix + """extraction/form"""))
   }
}
        

// @LINE:77
case controllers_Files_uploadExtract35(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploadExtract(), HandlerDef(this, "controllers.Files", "uploadExtract", Nil,"POST", """""", Routes.prefix + """extraction/upload"""))
   }
}
        

// @LINE:83
case controllers_Files_list36(params) => {
   call(params.fromQuery[String]("when", Some("")), params.fromQuery[String]("date", Some("")), params.fromQuery[Int]("size", Some(12)), params.fromQuery[String]("mode", Some(""))) { (when, date, size, mode) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).list(when, date, size, mode), HandlerDef(this, "controllers.Files", "list", Seq(classOf[String], classOf[String], classOf[Int], classOf[String]),"GET", """ ----------------------------------------------------------------------
 FILES
 ----------------------------------------------------------------------""", Routes.prefix + """files"""))
   }
}
        

// @LINE:84
case controllers_Files_uploadFile37(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploadFile, HandlerDef(this, "controllers.Files", "uploadFile", Nil,"GET", """""", Routes.prefix + """files/new"""))
   }
}
        

// @LINE:86
case controllers_Files_metadataSearch38(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).metadataSearch, HandlerDef(this, "controllers.Files", "metadataSearch", Nil,"GET", """GET            /files/newZipCollection                                                  @controllers.Files.uploadZipFile""", Routes.prefix + """files/metadataSearch"""))
   }
}
        

// @LINE:87
case controllers_Files_generalMetadataSearch39(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).generalMetadataSearch, HandlerDef(this, "controllers.Files", "generalMetadataSearch", Nil,"GET", """""", Routes.prefix + """files/generalMetadataSearch"""))
   }
}
        

// @LINE:88
case controllers_Files_followingFiles40(params) => {
   call(params.fromQuery[Int]("index", Some(0)), params.fromQuery[Int]("size", Some(12)), params.fromQuery[String]("mode", Some(""))) { (index, size, mode) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).followingFiles(index, size, mode), HandlerDef(this, "controllers.Files", "followingFiles", Seq(classOf[Int], classOf[Int], classOf[String]),"GET", """""", Routes.prefix + """files/following"""))
   }
}
        

// @LINE:89
case controllers_Files_file41(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("dataset", Some(None)), params.fromQuery[Option[String]]("space", Some(None)), params.fromQuery[Option[String]]("folder", Some(None))) { (id, dataset, space, folder) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).file(id, dataset, space, folder), HandlerDef(this, "controllers.Files", "file", Seq(classOf[UUID], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]),"GET", """""", Routes.prefix + """files/$id<[^/]+>"""))
   }
}
        

// @LINE:90
case controllers_Files_filePreview42(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("dataset", Some(None)), params.fromQuery[Option[String]]("space", Some(None)), params.fromQuery[Option[String]]("folder", Some(None))) { (id, dataset, space, folder) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).filePreview(id, dataset, space, folder), HandlerDef(this, "controllers.Files", "filePreview", Seq(classOf[UUID], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]),"GET", """""", Routes.prefix + """filesPreview/$id<[^/]+>"""))
   }
}
        

// @LINE:91
case controllers_Files_download43(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).download(id), HandlerDef(this, "controllers.Files", "download", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """files/$id<[^/]+>/blob"""))
   }
}
        

// @LINE:92
case controllers_Files_downloadAsFormat44(params) => {
   call(params.fromPath[UUID]("id", None), params.fromPath[String]("format", None)) { (id, format) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).downloadAsFormat(id, format), HandlerDef(this, "controllers.Files", "downloadAsFormat", Seq(classOf[UUID], classOf[String]),"GET", """""", Routes.prefix + """files/$id<[^/]+>/download/$format<[^/]+>"""))
   }
}
        

// @LINE:95
case controllers_Files_upload45(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).upload, HandlerDef(this, "controllers.Files", "upload", Nil,"POST", """GET           /queries/:id/blob				                                        @controllers.Files.downloadquery(id: UUID)
GET           /files/:id/similar                                                       @controllers.Files.findSimilar(id: UUID)""", Routes.prefix + """upload"""))
   }
}
        

// @LINE:96
case controllers_Files_uploadAndExtract46(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploadAndExtract, HandlerDef(this, "controllers.Files", "uploadAndExtract", Nil,"POST", """""", Routes.prefix + """uploadAndExtract"""))
   }
}
        

// @LINE:97
case controllers_Files_uploadSelectQuery47(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploadSelectQuery, HandlerDef(this, "controllers.Files", "uploadSelectQuery", Nil,"POST", """""", Routes.prefix + """uploadSelectQuery"""))
   }
}
        

// @LINE:99
case controllers_Files_uploaddnd48(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploaddnd(id), HandlerDef(this, "controllers.Files", "uploaddnd", Seq(classOf[UUID]),"POST", """POST          /uploadAjax					                                            @controllers.Files.uploadAjax""", Routes.prefix + """uploaddnd/$id<[^/]+>"""))
   }
}
        

// @LINE:101
case controllers_Files_uploadDragDrop49(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).uploadDragDrop, HandlerDef(this, "controllers.Files", "uploadDragDrop", Nil,"POST", """POST  	       /reactiveUpload					                                        @controllers.Files.reactiveUpload""", Routes.prefix + """uploadDragDrop"""))
   }
}
        

// @LINE:102
case controllers_Files_thumbnail50(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).thumbnail(id), HandlerDef(this, "controllers.Files", "thumbnail", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """fileThumbnail/$id<[^/]+>/blob"""))
   }
}
        

// @LINE:103
case controllers_Files_fileBySection51(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Files]).fileBySection(id), HandlerDef(this, "controllers.Files", "fileBySection", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """file_by_section/$id<[^/]+>"""))
   }
}
        

// @LINE:104
case controllers_Extractors_submitFileExtraction52(params) => {
   call(params.fromPath[UUID]("file_id", None)) { (file_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Extractors]).submitFileExtraction(file_id), HandlerDef(this, "controllers.Extractors", "submitFileExtraction", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """files/$file_id<[^/]+>/extractions"""))
   }
}
        

// @LINE:109
case controllers_Datasets_list53(params) => {
   call(params.fromQuery[String]("when", Some("")), params.fromQuery[String]("date", Some("")), params.fromQuery[Int]("size", Some(12)), params.fromQuery[Option[String]]("space", Some(None)), params.fromQuery[Option[String]]("status", Some(None)), params.fromQuery[String]("mode", Some("")), params.fromQuery[Option[String]]("owner", Some(None)), params.fromQuery[Boolean]("showPublic", Some(true)), params.fromQuery[Boolean]("showOnlyShared", Some(false)), params.fromQuery[Boolean]("showTrash", Some(false))) { (when, date, size, space, status, mode, owner, showPublic, showOnlyShared, showTrash) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).list(when, date, size, space, status, mode, owner, showPublic, showOnlyShared, showTrash), HandlerDef(this, "controllers.Datasets", "list", Seq(classOf[String], classOf[String], classOf[Int], classOf[Option[String]], classOf[Option[String]], classOf[String], classOf[Option[String]], classOf[Boolean], classOf[Boolean], classOf[Boolean]),"GET", """ ----------------------------------------------------------------------
 DATASETS
 ----------------------------------------------------------------------""", Routes.prefix + """datasets"""))
   }
}
        

// @LINE:110
case controllers_Datasets_sortedListInSpace54(params) => {
   call(params.fromQuery[String]("space", None), params.fromQuery[Integer]("offset", None), params.fromQuery[Integer]("limit", None), params.fromQuery[Boolean]("showPublic", Some(true))) { (space, offset, limit, showPublic) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).sortedListInSpace(space, offset, limit, showPublic), HandlerDef(this, "controllers.Datasets", "sortedListInSpace", Seq(classOf[String], classOf[Integer], classOf[Integer], classOf[Boolean]),"GET", """""", Routes.prefix + """datasets/sorted"""))
   }
}
        

// @LINE:111
case controllers_Datasets_newDataset55(params) => {
   call(params.fromQuery[Option[String]]("space", None), params.fromQuery[Option[String]]("collection", None)) { (space, collection) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).newDataset(space, collection), HandlerDef(this, "controllers.Datasets", "newDataset", Seq(classOf[Option[String]], classOf[Option[String]]),"GET", """""", Routes.prefix + """datasets/new"""))
   }
}
        

// @LINE:112
case controllers_Datasets_createStep256(params) => {
   call(params.fromQuery[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).createStep2(id), HandlerDef(this, "controllers.Datasets", "createStep2", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """datasets/createStep2"""))
   }
}
        

// @LINE:113
case controllers_Datasets_metadataSearch57(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).metadataSearch, HandlerDef(this, "controllers.Datasets", "metadataSearch", Nil,"GET", """""", Routes.prefix + """datasets/metadataSearch"""))
   }
}
        

// @LINE:114
case controllers_Datasets_generalMetadataSearch58(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).generalMetadataSearch, HandlerDef(this, "controllers.Datasets", "generalMetadataSearch", Nil,"GET", """""", Routes.prefix + """datasets/generalMetadataSearch"""))
   }
}
        

// @LINE:115
case controllers_Datasets_followingDatasets59(params) => {
   call(params.fromQuery[Int]("index", Some(0)), params.fromQuery[Int]("size", Some(12)), params.fromQuery[String]("mode", Some(""))) { (index, size, mode) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).followingDatasets(index, size, mode), HandlerDef(this, "controllers.Datasets", "followingDatasets", Seq(classOf[Int], classOf[Int], classOf[String]),"GET", """""", Routes.prefix + """datasets/following"""))
   }
}
        

// @LINE:116
case controllers_ToolManager_toolManager60(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).toolManager, HandlerDef(this, "controllers.ToolManager", "toolManager", Nil,"GET", """""", Routes.prefix + """toolManager"""))
   }
}
        

// @LINE:117
case controllers_ToolManager_refreshToolSidebar61(params) => {
   call(params.fromQuery[UUID]("datasetid", None), params.fromQuery[String]("datasetName", None)) { (datasetid, datasetName) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).refreshToolSidebar(datasetid, datasetName), HandlerDef(this, "controllers.ToolManager", "refreshToolSidebar", Seq(classOf[UUID], classOf[String]),"GET", """""", Routes.prefix + """datasets/refreshToolList"""))
   }
}
        

// @LINE:118
case controllers_ToolManager_getLaunchableTools62(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).getLaunchableTools, HandlerDef(this, "controllers.ToolManager", "getLaunchableTools", Nil,"GET", """""", Routes.prefix + """datasets/launchableTools"""))
   }
}
        

// @LINE:119
case controllers_ToolManager_uploadDatasetToTool63(params) => {
   call(params.fromQuery[UUID]("instanceID", None), params.fromQuery[UUID]("datasetID", None), params.fromQuery[String]("datasetName", None)) { (instanceID, datasetID, datasetName) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).uploadDatasetToTool(instanceID, datasetID, datasetName), HandlerDef(this, "controllers.ToolManager", "uploadDatasetToTool", Seq(classOf[UUID], classOf[UUID], classOf[String]),"GET", """""", Routes.prefix + """datasets/uploadToTool"""))
   }
}
        

// @LINE:120
case controllers_ToolManager_getInstances64(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).getInstances, HandlerDef(this, "controllers.ToolManager", "getInstances", Nil,"GET", """""", Routes.prefix + """datasets/getInstances"""))
   }
}
        

// @LINE:121
case controllers_ToolManager_removeInstance65(params) => {
   call(params.fromQuery[String]("toolType", None), params.fromQuery[UUID]("instanceID", None)) { (toolType, instanceID) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).removeInstance(toolType, instanceID), HandlerDef(this, "controllers.ToolManager", "removeInstance", Seq(classOf[String], classOf[UUID]),"GET", """""", Routes.prefix + """datasets/removeInstance"""))
   }
}
        

// @LINE:122
case controllers_ToolManager_launchTool66(params) => {
   call(params.fromQuery[String]("sessionName", None), params.fromQuery[String]("ttype", None), params.fromQuery[UUID]("datasetId", None), params.fromQuery[String]("datasetName", None)) { (sessionName, ttype, datasetId, datasetName) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.ToolManager]).launchTool(sessionName, ttype, datasetId, datasetName), HandlerDef(this, "controllers.ToolManager", "launchTool", Seq(classOf[String], classOf[String], classOf[UUID], classOf[String]),"GET", """""", Routes.prefix + """datasets/launchTool"""))
   }
}
        

// @LINE:123
case controllers_Datasets_dataset67(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("space", Some(None)), params.fromQuery[Int]("limit", Some(20))) { (id, space, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).dataset(id, space, limit), HandlerDef(this, "controllers.Datasets", "dataset", Seq(classOf[UUID], classOf[Option[String]], classOf[Int]),"GET", """""", Routes.prefix + """datasets/$id<[^/]+>"""))
   }
}
        

// @LINE:124
case controllers_Datasets_getUpdatedFilesAndFolders68(params) => {
   call(params.fromPath[UUID]("datasetId", None), params.fromQuery[Int]("limit", Some(20)), params.fromQuery[Int]("pageIndex", Some(0)), params.fromQuery[Option[String]]("space", Some(None))) { (datasetId, limit, pageIndex, space) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).getUpdatedFilesAndFolders(datasetId, limit, pageIndex, space), HandlerDef(this, "controllers.Datasets", "getUpdatedFilesAndFolders", Seq(classOf[UUID], classOf[Int], classOf[Int], classOf[Option[String]]),"POST", """""", Routes.prefix + """datasets/$datasetId<[^/]+>/updatedFilesAndFolders"""))
   }
}
        

// @LINE:125
case controllers_Datasets_users69(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).users(id), HandlerDef(this, "controllers.Datasets", "users", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """datasets/$id<[^/]+>/users"""))
   }
}
        

// @LINE:126
case controllers_Datasets_datasetBySection70(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).datasetBySection(id), HandlerDef(this, "controllers.Datasets", "datasetBySection", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """datasets_by_section/$id<[^/]+>"""))
   }
}
        

// @LINE:127
case controllers_Datasets_submit71(params) => {
   call(params.fromQuery[Option[String]]("folderId", Some(None))) { (folderId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).submit(folderId), HandlerDef(this, "controllers.Datasets", "submit", Seq(classOf[Option[String]]),"POST", """""", Routes.prefix + """dataset/submit"""))
   }
}
        

// @LINE:128
case controllers_Datasets_addFiles72(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Datasets]).addFiles(id), HandlerDef(this, "controllers.Datasets", "addFiles", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """datasets/$id<[^/]+>/addFiles"""))
   }
}
        

// @LINE:129
case controllers_Folders_addFiles73(params) => {
   call(params.fromPath[UUID]("id", None), params.fromPath[String]("folderId", None)) { (id, folderId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Folders]).addFiles(id, folderId), HandlerDef(this, "controllers.Folders", "addFiles", Seq(classOf[UUID], classOf[String]),"GET", """""", Routes.prefix + """datasets/$id<[^/]+>/$folderId<[^/]+>/addFiles"""))
   }
}
        

// @LINE:130
case controllers_Folders_createFolder74(params) => {
   call(params.fromPath[UUID]("parentDatasetId", None)) { (parentDatasetId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Folders]).createFolder(parentDatasetId), HandlerDef(this, "controllers.Folders", "createFolder", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """datasets/$parentDatasetId<[^/]+>/newFolder"""))
   }
}
        

// @LINE:131
case controllers_Extractors_submitDatasetExtraction75(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Extractors]).submitDatasetExtraction(id), HandlerDef(this, "controllers.Extractors", "submitDatasetExtraction", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """datasets/$id<[^/]+>/extractions"""))
   }
}
        

// @LINE:136
case controllers_Collections_list76(params) => {
   call(params.fromQuery[String]("when", Some("")), params.fromQuery[String]("date", Some("")), params.fromQuery[Int]("size", Some(12)), params.fromQuery[Option[String]]("space", Some(None)), params.fromQuery[String]("mode", Some("")), params.fromQuery[Option[String]]("owner", Some(None)), params.fromQuery[Boolean]("showPublic", Some(true)), params.fromQuery[Boolean]("showOnlyShared", Some(false)), params.fromQuery[Boolean]("showTrash", Some(false))) { (when, date, size, space, mode, owner, showPublic, showOnlyShared, showTrash) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).list(when, date, size, space, mode, owner, showPublic, showOnlyShared, showTrash), HandlerDef(this, "controllers.Collections", "list", Seq(classOf[String], classOf[String], classOf[Int], classOf[Option[String]], classOf[String], classOf[Option[String]], classOf[Boolean], classOf[Boolean], classOf[Boolean]),"GET", """ ----------------------------------------------------------------------
 COLLECTIONS
 ----------------------------------------------------------------------""", Routes.prefix + """collections"""))
   }
}
        

// @LINE:137
case controllers_Collections_sortedListInSpace77(params) => {
   call(params.fromQuery[String]("space", None), params.fromQuery[Integer]("offset", None), params.fromQuery[Integer]("limit", None), params.fromQuery[Boolean]("showPublic", Some(true))) { (space, offset, limit, showPublic) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).sortedListInSpace(space, offset, limit, showPublic), HandlerDef(this, "controllers.Collections", "sortedListInSpace", Seq(classOf[String], classOf[Integer], classOf[Integer], classOf[Boolean]),"GET", """""", Routes.prefix + """collections/sorted"""))
   }
}
        

// @LINE:138
case controllers_Collections_listChildCollections78(params) => {
   call(params.fromQuery[String]("parentCollectionId", None), params.fromQuery[String]("when", Some("")), params.fromQuery[String]("date", Some("")), params.fromQuery[Int]("size", Some(12)), params.fromQuery[Option[String]]("space", Some(None)), params.fromQuery[String]("mode", Some("")), params.fromQuery[Option[String]]("owner", Some(None))) { (parentCollectionId, when, date, size, space, mode, owner) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).listChildCollections(parentCollectionId, when, date, size, space, mode, owner), HandlerDef(this, "controllers.Collections", "listChildCollections", Seq(classOf[String], classOf[String], classOf[String], classOf[Int], classOf[Option[String]], classOf[String], classOf[Option[String]]),"GET", """""", Routes.prefix + """collections/listChildCollections"""))
   }
}
        

// @LINE:139
case controllers_Collections_newCollection79(params) => {
   call(params.fromQuery[Option[String]]("space", None)) { (space) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).newCollection(space), HandlerDef(this, "controllers.Collections", "newCollection", Seq(classOf[Option[String]]),"GET", """""", Routes.prefix + """collections/new"""))
   }
}
        

// @LINE:140
case controllers_Collections_newCollectionWithParent80(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).newCollectionWithParent(id), HandlerDef(this, "controllers.Collections", "newCollectionWithParent", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """collections/$id<[^/]+>/newchildCollection"""))
   }
}
        

// @LINE:141
case controllers_Collections_followingCollections81(params) => {
   call(params.fromQuery[Int]("index", Some(0)), params.fromQuery[Int]("size", Some(12)), params.fromQuery[String]("mode", Some(""))) { (index, size, mode) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).followingCollections(index, size, mode), HandlerDef(this, "controllers.Collections", "followingCollections", Seq(classOf[Int], classOf[Int], classOf[String]),"GET", """""", Routes.prefix + """collections/following"""))
   }
}
        

// @LINE:142
case controllers_Collections_submit82(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).submit, HandlerDef(this, "controllers.Collections", "submit", Nil,"POST", """""", Routes.prefix + """collection/submit"""))
   }
}
        

// @LINE:143
case controllers_Collections_createStep283(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).createStep2, HandlerDef(this, "controllers.Collections", "createStep2", Nil,"GET", """""", Routes.prefix + """collection/createStep2"""))
   }
}
        

// @LINE:144
case controllers_Collections_collection84(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Int]("limit", Some(12))) { (id, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).collection(id, limit), HandlerDef(this, "controllers.Collections", "collection", Seq(classOf[UUID], classOf[Int]),"GET", """""", Routes.prefix + """collection/$id<[^/]+>"""))
   }
}
        

// @LINE:145
case controllers_Collections_users85(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).users(id), HandlerDef(this, "controllers.Collections", "users", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """collection/$id<[^/]+>/users"""))
   }
}
        

// @LINE:146
case controllers_Collections_previews86(params) => {
   call(params.fromPath[UUID]("collection_id", None)) { (collection_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).previews(collection_id), HandlerDef(this, "controllers.Collections", "previews", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """collections/$collection_id<[^/]+>/previews"""))
   }
}
        

// @LINE:147
case controllers_Collections_getUpdatedDatasets87(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Int]("index", Some(0)), params.fromQuery[Int]("limit", Some(12))) { (id, index, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).getUpdatedDatasets(id, index, limit), HandlerDef(this, "controllers.Collections", "getUpdatedDatasets", Seq(classOf[UUID], classOf[Int], classOf[Int]),"GET", """""", Routes.prefix + """collection/$id<[^/]+>/datasets"""))
   }
}
        

// @LINE:148
case controllers_Collections_getUpdatedChildCollections88(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Int]("index", Some(0)), params.fromQuery[Int]("limit", Some(12))) { (id, index, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Collections]).getUpdatedChildCollections(id, index, limit), HandlerDef(this, "controllers.Collections", "getUpdatedChildCollections", Seq(classOf[UUID], classOf[Int], classOf[Int]),"GET", """""", Routes.prefix + """collection/$id<[^/]+>/childCollections"""))
   }
}
        

// @LINE:152
case controllers_Spaces_list89(params) => {
   call(params.fromQuery[String]("when", Some("")), params.fromQuery[String]("date", Some("")), params.fromQuery[Int]("size", Some(12)), params.fromQuery[String]("mode", Some("")), params.fromQuery[Option[String]]("owner", Some(None)), params.fromQuery[Boolean]("showAll", Some(true)), params.fromQuery[Boolean]("showPublic", Some(true)), params.fromQuery[Boolean]("onlyTrial", Some(false)), params.fromQuery[Boolean]("showOnlyShared", Some(false))) { (when, date, size, mode, owner, showAll, showPublic, onlyTrial, showOnlyShared) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).list(when, date, size, mode, owner, showAll, showPublic, onlyTrial, showOnlyShared), HandlerDef(this, "controllers.Spaces", "list", Seq(classOf[String], classOf[String], classOf[Int], classOf[String], classOf[Option[String]], classOf[Boolean], classOf[Boolean], classOf[Boolean], classOf[Boolean]),"GET", """ ----------------------------------------------------------------------
 Spaces
 ----------------------------------------------------------------------""", Routes.prefix + """spaces"""))
   }
}
        

// @LINE:153
case controllers_Spaces_newSpace90(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).newSpace, HandlerDef(this, "controllers.Spaces", "newSpace", Nil,"GET", """""", Routes.prefix + """spaces/new"""))
   }
}
        

// @LINE:154
case controllers_Spaces_copySpace91(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).copySpace(id), HandlerDef(this, "controllers.Spaces", "copySpace", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/copy/$id<[^/]+>"""))
   }
}
        

// @LINE:155
case controllers_Spaces_followingSpaces92(params) => {
   call(params.fromQuery[Int]("index", Some(0)), params.fromQuery[Int]("size", Some(12)), params.fromQuery[String]("mode", Some(""))) { (index, size, mode) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).followingSpaces(index, size, mode), HandlerDef(this, "controllers.Spaces", "followingSpaces", Seq(classOf[Int], classOf[Int], classOf[String]),"GET", """""", Routes.prefix + """spaces/following"""))
   }
}
        

// @LINE:156
case controllers_Spaces_getSpace93(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Int]("size", Some(9)), params.fromQuery[String]("direction", Some("desc"))) { (id, size, direction) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).getSpace(id, size, direction), HandlerDef(this, "controllers.Spaces", "getSpace", Seq(classOf[UUID], classOf[Int], classOf[String]),"GET", """""", Routes.prefix + """spaces/$id<[^/]+>"""))
   }
}
        

// @LINE:157
case controllers_Spaces_updateSpace94(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).updateSpace(id), HandlerDef(this, "controllers.Spaces", "updateSpace", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/$id<[^/]+>/updateSpace"""))
   }
}
        

// @LINE:158
case controllers_Spaces_addRequest95(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).addRequest(id), HandlerDef(this, "controllers.Spaces", "addRequest", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/$id<[^/]+>/addRequest"""))
   }
}
        

// @LINE:159
case controllers_Spaces_manageUsers96(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).manageUsers(id), HandlerDef(this, "controllers.Spaces", "manageUsers", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/$id<[^/]+>/users"""))
   }
}
        

// @LINE:160
case controllers_Spaces_selectExtractors97(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).selectExtractors(id), HandlerDef(this, "controllers.Spaces", "selectExtractors", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/$id<[^/]+>/extractors"""))
   }
}
        

// @LINE:161
case controllers_Spaces_updateExtractors98(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).updateExtractors(id), HandlerDef(this, "controllers.Spaces", "updateExtractors", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """spaces/$id<[^/]+>/extractors"""))
   }
}
        

// @LINE:162
case controllers_Spaces_inviteToSpace99(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).inviteToSpace(id), HandlerDef(this, "controllers.Spaces", "inviteToSpace", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """spaces/$id<[^/]+>/invite"""))
   }
}
        

// @LINE:163
case controllers_Metadata_getMetadataBySpace100(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Metadata]).getMetadataBySpace(id), HandlerDef(this, "controllers.Metadata", "getMetadataBySpace", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/$id<[^/]+>/metadata"""))
   }
}
        

// @LINE:164
case controllers_Spaces_submit101(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).submit, HandlerDef(this, "controllers.Spaces", "submit", Nil,"POST", """""", Routes.prefix + """spaces/submit"""))
   }
}
        

// @LINE:165
case controllers_Spaces_submitCopy102(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).submitCopy, HandlerDef(this, "controllers.Spaces", "submitCopy", Nil,"POST", """""", Routes.prefix + """spaces/submitCopy"""))
   }
}
        

// @LINE:170
case controllers_Geostreams_map103(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Geostreams]).map, HandlerDef(this, "controllers.Geostreams", "map", Nil,"GET", """ ----------------------------------------------------------------------
 SENSORS
 ----------------------------------------------------------------------""", Routes.prefix + """geostreams"""))
   }
}
        

// @LINE:171
case controllers_Geostreams_list104(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Geostreams]).list, HandlerDef(this, "controllers.Geostreams", "list", Nil,"GET", """""", Routes.prefix + """geostreams/sensors"""))
   }
}
        

// @LINE:172
case controllers_Geostreams_newSensor105(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Geostreams]).newSensor, HandlerDef(this, "controllers.Geostreams", "newSensor", Nil,"GET", """""", Routes.prefix + """geostreams/sensors/new"""))
   }
}
        

// @LINE:173
case controllers_Geostreams_edit106(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Geostreams]).edit(id), HandlerDef(this, "controllers.Geostreams", "edit", Seq(classOf[String]),"GET", """""", Routes.prefix + """geostreams/sensors/$id<[^/]+>"""))
   }
}
        

// @LINE:178
case controllers_Metadata_search107(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Metadata]).search(), HandlerDef(this, "controllers.Metadata", "search", Nil,"GET", """ ----------------------------------------------------------------------
 JSON-LD Metadata
 ----------------------------------------------------------------------""", Routes.prefix + """metadata/search"""))
   }
}
        

// @LINE:179
case controllers_Metadata_view108(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Metadata]).view(id), HandlerDef(this, "controllers.Metadata", "view", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """metadata/$id<[^/]+>"""))
   }
}
        

// @LINE:180
case controllers_Metadata_file109(params) => {
   call(params.fromPath[UUID]("file_id", None)) { (file_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Metadata]).file(file_id), HandlerDef(this, "controllers.Metadata", "file", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """files/$file_id<[^/]+>/metadata"""))
   }
}
        

// @LINE:181
case controllers_Metadata_dataset110(params) => {
   call(params.fromPath[UUID]("dataset_id", None)) { (dataset_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Metadata]).dataset(dataset_id), HandlerDef(this, "controllers.Metadata", "dataset", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """datasets/$dataset_id<[^/]+>/metadata"""))
   }
}
        

// @LINE:183
case controllers_Tags_search111(params) => {
   call(params.fromQuery[String]("tag", None), params.fromQuery[String]("start", Some("")), params.fromQuery[Integer]("size", Some(12)), params.fromQuery[String]("mode", Some(""))) { (tag, start, size, mode) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Tags]).search(tag, start, size, mode), HandlerDef(this, "controllers.Tags", "search", Seq(classOf[String], classOf[String], classOf[Integer], classOf[String]),"GET", """ Tags""", Routes.prefix + """tags/search"""))
   }
}
        

// @LINE:184
case controllers_Tags_tagListOrdered112(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Tags]).tagListOrdered, HandlerDef(this, "controllers.Tags", "tagListOrdered", Nil,"GET", """""", Routes.prefix + """tags/list/ordered"""))
   }
}
        

// @LINE:185
case controllers_Tags_tagListWeighted113(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Tags]).tagListWeighted, HandlerDef(this, "controllers.Tags", "tagListWeighted", Nil,"GET", """""", Routes.prefix + """tags/list/weighted"""))
   }
}
        

// @LINE:190
case controllers_Search_search114(params) => {
   call(params.fromQuery[String]("query", Some(""))) { (query) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).search(query), HandlerDef(this, "controllers.Search", "search", Seq(classOf[String]),"GET", """ ----------------------------------------------------------------------

 ----------------------------------------------------------------------""", Routes.prefix + """search"""))
   }
}
        

// @LINE:191
case controllers_Search_multimediasearch115(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).multimediasearch, HandlerDef(this, "controllers.Search", "multimediasearch", Nil,"GET", """""", Routes.prefix + """multimediasearch"""))
   }
}
        

// @LINE:193
case controllers_Search_advanced116(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).advanced, HandlerDef(this, "controllers.Search", "advanced", Nil,"GET", """GET           /multimediaserach1                                                       @controllers.Search.multimediasearch1(f, id: UUID)""", Routes.prefix + """advanced"""))
   }
}
        

// @LINE:194
case controllers_Search_SearchByText117(params) => {
   call(params.fromQuery[String]("query", Some(""))) { (query) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).SearchByText(query), HandlerDef(this, "controllers.Search", "SearchByText", Seq(classOf[String]),"GET", """""", Routes.prefix + """SearchByText"""))
   }
}
        

// @LINE:195
case controllers_Search_uploadquery118(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).uploadquery, HandlerDef(this, "controllers.Search", "uploadquery", Nil,"POST", """""", Routes.prefix + """uploadquery"""))
   }
}
        

// @LINE:196
case controllers_Search_searchbyURL119(params) => {
   call(params.fromQuery[String]("query", Some(""))) { (query) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).searchbyURL(query), HandlerDef(this, "controllers.Search", "searchbyURL", Seq(classOf[String]),"GET", """""", Routes.prefix + """searchbyURL"""))
   }
}
        

// @LINE:197
case controllers_Search_callSearchMultimediaIndexView120(params) => {
   call(params.fromPath[UUID]("section_id", None)) { (section_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).callSearchMultimediaIndexView(section_id), HandlerDef(this, "controllers.Search", "callSearchMultimediaIndexView", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """searchbyfeature/$section_id<[^/]+>"""))
   }
}
        

// @LINE:198
case controllers_Search_findSimilarToExistingFile121(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).findSimilarToExistingFile(id), HandlerDef(this, "controllers.Search", "findSimilarToExistingFile", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """files/$id<[^/]+>/similar"""))
   }
}
        

// @LINE:199
case controllers_Search_findSimilarToQueryFile122(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("typeToSearch", None), params.fromQuery[List[String]]("sectionsSelected", Some(List.empty ))) { (id, typeToSearch, sectionsSelected) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).findSimilarToQueryFile(id, typeToSearch, sectionsSelected), HandlerDef(this, "controllers.Search", "findSimilarToQueryFile", Seq(classOf[UUID], classOf[String], classOf[List[String]]),"GET", """""", Routes.prefix + """queries/$id<[^/]+>/similar"""))
   }
}
        

// @LINE:200
case controllers_Search_findSimilarWeightedIndexes123(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Search]).findSimilarWeightedIndexes(), HandlerDef(this, "controllers.Search", "findSimilarWeightedIndexes", Nil,"POST", """""", Routes.prefix + """queries/similarWeightedIndexes"""))
   }
}
        

// @LINE:205
case controllers_DataAnalysis_listSessions124(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.DataAnalysis]).listSessions, HandlerDef(this, "controllers.DataAnalysis", "listSessions", Nil,"GET", """ ----------------------------------------------------------------------
 Jupyter Integration
 ----------------------------------------------------------------------""", Routes.prefix + """analysisSessions"""))
   }
}
        

// @LINE:208
case controllers_DataAnalysis_terminate125(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.DataAnalysis]).terminate(id), HandlerDef(this, "controllers.DataAnalysis", "terminate", Seq(classOf[UUID]),"GET", """GET            /analysis                                                                 @controllers.DataAnalysis.reserveSession
GET            /startContainer                                                           @controllers.DataAnalysis.startContainer(nameOfSession: String ?="", hours: String ?="", minutes: String ?="")""", Routes.prefix + """terminateSession/$id<[^/]+>"""))
   }
}
        

// @LINE:210
case controllers_Notebook_listNotebooks126(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Notebook]).listNotebooks, HandlerDef(this, "controllers.Notebook", "listNotebooks", Nil,"GET", """""", Routes.prefix + """notebooks"""))
   }
}
        

// @LINE:211
case controllers_Notebook_viewNotebook127(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Notebook]).viewNotebook(id), HandlerDef(this, "controllers.Notebook", "viewNotebook", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """notebook/$id<[^/]+>"""))
   }
}
        

// @LINE:212
case controllers_Notebook_editNotebook128(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Notebook]).editNotebook(id), HandlerDef(this, "controllers.Notebook", "editNotebook", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """editNotebook/$id<[^/]+>"""))
   }
}
        

// @LINE:213
case controllers_Notebook_deleteNotebook129(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Notebook]).deleteNotebook(id), HandlerDef(this, "controllers.Notebook", "deleteNotebook", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """deleteNotebook/$id<[^/]+>"""))
   }
}
        

// @LINE:218
case controllers_Previewers_list130(params) => {
   call { 
        invokeHandler(controllers.Previewers.list, HandlerDef(this, "controllers.Previewers", "list", Nil,"GET", """ ----------------------------------------------------------------------
 PREVIEWERS
 ----------------------------------------------------------------------""", Routes.prefix + """previewers/list"""))
   }
}
        

// @LINE:226
case securesocial_controllers_LoginPage_login131(params) => {
   call { 
        invokeHandler(securesocial.controllers.LoginPage.login, HandlerDef(this, "securesocial.controllers.LoginPage", "login", Nil,"GET", """ ----------------------------------------------------------------------
 SECURE SOCIAL
 ----------------------------------------------------------------------
 ----------------------------------------------------------------------
 LOGIN PAGES
 ----------------------------------------------------------------------""", Routes.prefix + """login"""))
   }
}
        

// @LINE:227
case securesocial_controllers_LoginPage_logout132(params) => {
   call { 
        invokeHandler(securesocial.controllers.LoginPage.logout, HandlerDef(this, "securesocial.controllers.LoginPage", "logout", Nil,"GET", """""", Routes.prefix + """logout"""))
   }
}
        

// @LINE:232
case securesocial_controllers_Registration_startSignUp133(params) => {
   call { 
        invokeHandler(securesocial.controllers.Registration.startSignUp, HandlerDef(this, "securesocial.controllers.Registration", "startSignUp", Nil,"GET", """ ----------------------------------------------------------------------
 USER REGISTRATION
 ----------------------------------------------------------------------""", Routes.prefix + """signup"""))
   }
}
        

// @LINE:233
case securesocial_controllers_Registration_handleStartSignUp134(params) => {
   call { 
        invokeHandler(securesocial.controllers.Registration.handleStartSignUp, HandlerDef(this, "securesocial.controllers.Registration", "handleStartSignUp", Nil,"POST", """""", Routes.prefix + """signup"""))
   }
}
        

// @LINE:234
case securesocial_controllers_Registration_signUp135(params) => {
   call(params.fromPath[String]("token", None)) { (token) =>
        invokeHandler(securesocial.controllers.Registration.signUp(token), HandlerDef(this, "securesocial.controllers.Registration", "signUp", Seq(classOf[String]),"GET", """""", Routes.prefix + """signup/$token<[^/]+>"""))
   }
}
        

// @LINE:235
case controllers_Registration_handleSignUp136(params) => {
   call(params.fromPath[String]("token", None)) { (token) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Registration]).handleSignUp(token), HandlerDef(this, "controllers.Registration", "handleSignUp", Seq(classOf[String]),"POST", """""", Routes.prefix + """signup/$token<[^/]+>"""))
   }
}
        

// @LINE:236
case securesocial_controllers_Registration_startResetPassword137(params) => {
   call { 
        invokeHandler(securesocial.controllers.Registration.startResetPassword, HandlerDef(this, "securesocial.controllers.Registration", "startResetPassword", Nil,"GET", """""", Routes.prefix + """reset"""))
   }
}
        

// @LINE:237
case securesocial_controllers_Registration_handleStartResetPassword138(params) => {
   call { 
        invokeHandler(securesocial.controllers.Registration.handleStartResetPassword, HandlerDef(this, "securesocial.controllers.Registration", "handleStartResetPassword", Nil,"POST", """""", Routes.prefix + """reset"""))
   }
}
        

// @LINE:238
case securesocial_controllers_Registration_resetPassword139(params) => {
   call(params.fromPath[String]("token", None)) { (token) =>
        invokeHandler(securesocial.controllers.Registration.resetPassword(token), HandlerDef(this, "securesocial.controllers.Registration", "resetPassword", Seq(classOf[String]),"GET", """""", Routes.prefix + """reset/$token<[^/]+>"""))
   }
}
        

// @LINE:239
case securesocial_controllers_Registration_handleResetPassword140(params) => {
   call(params.fromPath[String]("token", None)) { (token) =>
        invokeHandler(securesocial.controllers.Registration.handleResetPassword(token), HandlerDef(this, "securesocial.controllers.Registration", "handleResetPassword", Seq(classOf[String]),"POST", """""", Routes.prefix + """reset/$token<[^/]+>"""))
   }
}
        

// @LINE:240
case securesocial_controllers_PasswordChange_page141(params) => {
   call { 
        invokeHandler(securesocial.controllers.PasswordChange.page, HandlerDef(this, "securesocial.controllers.PasswordChange", "page", Nil,"GET", """""", Routes.prefix + """password"""))
   }
}
        

// @LINE:241
case securesocial_controllers_PasswordChange_handlePasswordChange142(params) => {
   call { 
        invokeHandler(securesocial.controllers.PasswordChange.handlePasswordChange, HandlerDef(this, "securesocial.controllers.PasswordChange", "handlePasswordChange", Nil,"POST", """""", Routes.prefix + """password"""))
   }
}
        

// @LINE:246
case securesocial_controllers_ProviderController_authenticate143(params) => {
   call(params.fromPath[String]("provider", None)) { (provider) =>
        invokeHandler(securesocial.controllers.ProviderController.authenticate(provider), HandlerDef(this, "securesocial.controllers.ProviderController", "authenticate", Seq(classOf[String]),"GET", """ ----------------------------------------------------------------------
 PROVIDERS ENTRY POINTS
 ----------------------------------------------------------------------""", Routes.prefix + """authenticate/$provider<[^/]+>"""))
   }
}
        

// @LINE:247
case securesocial_controllers_ProviderController_authenticateByPost144(params) => {
   call(params.fromPath[String]("provider", None)) { (provider) =>
        invokeHandler(securesocial.controllers.ProviderController.authenticateByPost(provider), HandlerDef(this, "securesocial.controllers.ProviderController", "authenticateByPost", Seq(classOf[String]),"POST", """""", Routes.prefix + """authenticate/$provider<[^/]+>"""))
   }
}
        

// @LINE:249
case securesocial_controllers_ProviderController_notAuthorized145(params) => {
   call { 
        invokeHandler(securesocial.controllers.ProviderController.notAuthorized, HandlerDef(this, "securesocial.controllers.ProviderController", "notAuthorized", Nil,"GET", """""", Routes.prefix + """not-authorized"""))
   }
}
        

// @LINE:254
case controllers_Admin_customize146(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).customize, HandlerDef(this, "controllers.Admin", "customize", Nil,"GET", """ ----------------------------------------------------------------------
 ADMIN PAGES
 ----------------------------------------------------------------------""", Routes.prefix + """admin/customize"""))
   }
}
        

// @LINE:255
case controllers_Admin_tos147(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).tos, HandlerDef(this, "controllers.Admin", "tos", Nil,"GET", """""", Routes.prefix + """admin/tos"""))
   }
}
        

// @LINE:256
case controllers_Admin_users148(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).users, HandlerDef(this, "controllers.Admin", "users", Nil,"GET", """""", Routes.prefix + """admin/users"""))
   }
}
        

// @LINE:257
case controllers_Admin_adminIndex149(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).adminIndex, HandlerDef(this, "controllers.Admin", "adminIndex", Nil,"GET", """""", Routes.prefix + """admin/indexAdmin"""))
   }
}
        

// @LINE:258
case controllers_Admin_reindexFiles150(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).reindexFiles, HandlerDef(this, "controllers.Admin", "reindexFiles", Nil,"GET", """""", Routes.prefix + """admin/reindexFiles"""))
   }
}
        

// @LINE:259
case controllers_Admin_getAdapters151(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getAdapters, HandlerDef(this, "controllers.Admin", "getAdapters", Nil,"GET", """""", Routes.prefix + """admin/adapters"""))
   }
}
        

// @LINE:260
case controllers_Admin_getExtractors152(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getExtractors, HandlerDef(this, "controllers.Admin", "getExtractors", Nil,"GET", """""", Routes.prefix + """admin/extractors"""))
   }
}
        

// @LINE:261
case controllers_Admin_getMeasures153(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getMeasures, HandlerDef(this, "controllers.Admin", "getMeasures", Nil,"GET", """""", Routes.prefix + """admin/measures"""))
   }
}
        

// @LINE:262
case controllers_Admin_getIndexers154(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getIndexers, HandlerDef(this, "controllers.Admin", "getIndexers", Nil,"GET", """""", Routes.prefix + """admin/indexers"""))
   }
}
        

// @LINE:263
case controllers_Admin_getIndexes155(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getIndexes, HandlerDef(this, "controllers.Admin", "getIndexes", Nil,"GET", """""", Routes.prefix + """admin/indexes"""))
   }
}
        

// @LINE:264
case controllers_Admin_getSections156(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getSections, HandlerDef(this, "controllers.Admin", "getSections", Nil,"GET", """""", Routes.prefix + """admin/sections"""))
   }
}
        

// @LINE:265
case controllers_Admin_getMetadataDefinitions157(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).getMetadataDefinitions, HandlerDef(this, "controllers.Admin", "getMetadataDefinitions", Nil,"GET", """""", Routes.prefix + """admin/metadata/definitions"""))
   }
}
        

// @LINE:266
case controllers_Admin_createIndex158(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).createIndex, HandlerDef(this, "controllers.Admin", "createIndex", Nil,"POST", """""", Routes.prefix + """admin/createIndex"""))
   }
}
        

// @LINE:267
case controllers_Admin_buildIndex159(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).buildIndex(id), HandlerDef(this, "controllers.Admin", "buildIndex", Seq(classOf[String]),"POST", """""", Routes.prefix + """admin/index/$id<[^/]+>/build"""))
   }
}
        

// @LINE:268
case controllers_Admin_deleteIndex160(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).deleteIndex(id), HandlerDef(this, "controllers.Admin", "deleteIndex", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """admin/index/$id<[^/]+>"""))
   }
}
        

// @LINE:269
case controllers_Admin_deleteAllIndexes161(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).deleteAllIndexes, HandlerDef(this, "controllers.Admin", "deleteAllIndexes", Nil,"DELETE", """""", Routes.prefix + """admin/index"""))
   }
}
        

// @LINE:270
case controllers_Admin_listRoles162(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).listRoles, HandlerDef(this, "controllers.Admin", "listRoles", Nil,"GET", """""", Routes.prefix + """admin/roles"""))
   }
}
        

// @LINE:271
case controllers_Admin_createRole163(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).createRole, HandlerDef(this, "controllers.Admin", "createRole", Nil,"GET", """""", Routes.prefix + """admin/roles/new"""))
   }
}
        

// @LINE:272
case controllers_Admin_submitCreateRole164(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).submitCreateRole, HandlerDef(this, "controllers.Admin", "submitCreateRole", Nil,"POST", """""", Routes.prefix + """admin/roles/submitNew"""))
   }
}
        

// @LINE:273
case controllers_Admin_removeRole165(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).removeRole(id), HandlerDef(this, "controllers.Admin", "removeRole", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """admin/roles/delete/$id<[^/]+>"""))
   }
}
        

// @LINE:274
case controllers_Admin_editRole166(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).editRole(id), HandlerDef(this, "controllers.Admin", "editRole", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """admin/roles/$id<[^/]+>/edit"""))
   }
}
        

// @LINE:275
case controllers_Admin_updateRole167(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).updateRole, HandlerDef(this, "controllers.Admin", "updateRole", Nil,"POST", """""", Routes.prefix + """admin/roles/update"""))
   }
}
        

// @LINE:276
case controllers_Extractors_listAllExtractions168(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Extractors]).listAllExtractions, HandlerDef(this, "controllers.Extractors", "listAllExtractions", Nil,"GET", """""", Routes.prefix + """admin/extractions"""))
   }
}
        

// @LINE:277
case controllers_Admin_viewDumpers169(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).viewDumpers, HandlerDef(this, "controllers.Admin", "viewDumpers", Nil,"GET", """""", Routes.prefix + """admin/dataDumps"""))
   }
}
        

// @LINE:278
case controllers_Admin_sensors170(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Admin]).sensors, HandlerDef(this, "controllers.Admin", "sensors", Nil,"GET", """""", Routes.prefix + """admin/sensors"""))
   }
}
        

// @LINE:279
case api_Admin_sensorsConfig171(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).sensorsConfig, HandlerDef(this, "api.Admin", "sensorsConfig", Nil,"POST", """""", Routes.prefix + """api/sensors/config"""))
   }
}
        

// @LINE:284
case controllers_Application_javascriptRoutes172(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).javascriptRoutes, HandlerDef(this, "controllers.Application", "javascriptRoutes", Nil,"GET", """ ----------------------------------------------------------------------
 JAVASCRIPT ENDPOINTS
 ----------------------------------------------------------------------""", Routes.prefix + """javascriptRoutes"""))
   }
}
        

// @LINE:289
case controllers_Application_swagger173(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).swagger, HandlerDef(this, "controllers.Application", "swagger", Nil,"GET", """ ----------------------------------------------------------------------
 API DOCUMENTATION USING SWAGGER
 ----------------------------------------------------------------------""", Routes.prefix + """swagger"""))
   }
}
        

// @LINE:290
case controllers_Application_swaggerUI174(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Application]).swaggerUI, HandlerDef(this, "controllers.Application", "swaggerUI", Nil,"GET", """""", Routes.prefix + """swaggerUI"""))
   }
}
        

// @LINE:299
case api_Admin_mail175(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).mail, HandlerDef(this, "api.Admin", "mail", Nil,"POST", """----------------------------------------------------------------------
 ADMIN
----------------------------------------------------------------------""", Routes.prefix + """api/admin/mail"""))
   }
}
        

// @LINE:300
case api_Admin_emailZipUpload176(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).emailZipUpload, HandlerDef(this, "api.Admin", "emailZipUpload", Nil,"POST", """""", Routes.prefix + """api/admin/mailZip"""))
   }
}
        

// @LINE:301
case api_Admin_users177(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).users, HandlerDef(this, "api.Admin", "users", Nil,"POST", """""", Routes.prefix + """api/admin/users"""))
   }
}
        

// @LINE:302
case api_Admin_sensorsConfig178(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).sensorsConfig, HandlerDef(this, "api.Admin", "sensorsConfig", Nil,"POST", """""", Routes.prefix + """api/sensors/config"""))
   }
}
        

// @LINE:303
case api_Admin_deleteAllData179(params) => {
   call(params.fromQuery[Boolean]("resetAll", Some(false))) { (resetAll) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).deleteAllData(resetAll), HandlerDef(this, "api.Admin", "deleteAllData", Seq(classOf[Boolean]),"DELETE", """""", Routes.prefix + """api/delete-data"""))
   }
}
        

// @LINE:304
case api_Admin_submitAppearance180(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).submitAppearance, HandlerDef(this, "api.Admin", "submitAppearance", Nil,"POST", """""", Routes.prefix + """api/changeAppearance"""))
   }
}
        

// @LINE:305
case api_Admin_reindex181(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).reindex, HandlerDef(this, "api.Admin", "reindex", Nil,"POST", """""", Routes.prefix + """api/reindex"""))
   }
}
        

// @LINE:306
case api_Admin_updateConfiguration182(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Admin]).updateConfiguration, HandlerDef(this, "api.Admin", "updateConfiguration", Nil,"POST", """""", Routes.prefix + """api/admin/configuration"""))
   }
}
        

// @LINE:311
case api_ContextLD_addContext183(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ContextLD]).addContext(), HandlerDef(this, "api.ContextLD", "addContext", Nil,"POST", """----------------------------------------------------------------------
 JSON-LD METADATA
----------------------------------------------------------------------""", Routes.prefix + """api/contexts"""))
   }
}
        

// @LINE:312
case api_ContextLD_getContextById184(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ContextLD]).getContextById(id), HandlerDef(this, "api.ContextLD", "getContextById", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/contexts/$id<[^/]+>"""))
   }
}
        

// @LINE:313
case api_ContextLD_getContextByName185(params) => {
   call(params.fromPath[String]("name", None)) { (name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ContextLD]).getContextByName(name), HandlerDef(this, "api.ContextLD", "getContextByName", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/contexts/$name<[^/]+>/context.json"""))
   }
}
        

// @LINE:314
case api_ContextLD_removeById186(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ContextLD]).removeById(id), HandlerDef(this, "api.ContextLD", "removeById", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/contexts/$id<[^/]+>"""))
   }
}
        

// @LINE:317
case api_Datasets_addMetadataJsonLD187(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addMetadataJsonLD(id), HandlerDef(this, "api.Datasets", "addMetadataJsonLD", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/metadata.jsonld"""))
   }
}
        

// @LINE:318
case api_Datasets_getMetadataJsonLD188(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("extractor", Some(None))) { (id, extractor) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getMetadataJsonLD(id, extractor), HandlerDef(this, "api.Datasets", "getMetadataJsonLD", Seq(classOf[UUID], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/metadata.jsonld"""))
   }
}
        

// @LINE:319
case api_Datasets_removeMetadataJsonLD189(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("extractor", Some(None))) { (id, extractor) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeMetadataJsonLD(id, extractor), HandlerDef(this, "api.Datasets", "removeMetadataJsonLD", Seq(classOf[UUID], classOf[Option[String]]),"DELETE", """""", Routes.prefix + """api/datasets/$id<[^/]+>/metadata.jsonld"""))
   }
}
        

// @LINE:320
case api_Files_addMetadataJsonLD190(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).addMetadataJsonLD(id), HandlerDef(this, "api.Files", "addMetadataJsonLD", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/metadata.jsonld"""))
   }
}
        

// @LINE:321
case api_Files_getMetadataJsonLD191(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("extractor", Some(None))) { (id, extractor) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getMetadataJsonLD(id, extractor), HandlerDef(this, "api.Files", "getMetadataJsonLD", Seq(classOf[UUID], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/metadata.jsonld"""))
   }
}
        

// @LINE:322
case api_Files_removeMetadataJsonLD192(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("extractor", Some(None))) { (id, extractor) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).removeMetadataJsonLD(id, extractor), HandlerDef(this, "api.Files", "removeMetadataJsonLD", Seq(classOf[UUID], classOf[Option[String]]),"DELETE", """""", Routes.prefix + """api/files/$id<[^/]+>/metadata.jsonld"""))
   }
}
        

// @LINE:324
case api_Metadata_addUserMetadata193(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).addUserMetadata(), HandlerDef(this, "api.Metadata", "addUserMetadata", Nil,"POST", """""", Routes.prefix + """api/metadata.jsonld"""))
   }
}
        

// @LINE:325
case api_Metadata_removeMetadata194(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).removeMetadata(id), HandlerDef(this, "api.Metadata", "removeMetadata", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/metadata.jsonld/$id<[^/]+>"""))
   }
}
        

// @LINE:329
case api_Metadata_getDefinitions195(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getDefinitions(), HandlerDef(this, "api.Metadata", "getDefinitions", Nil,"GET", """""", Routes.prefix + """api/metadata/definitions"""))
   }
}
        

// @LINE:330
case api_Metadata_getDefinitionsDistinctName196(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getDefinitionsDistinctName(), HandlerDef(this, "api.Metadata", "getDefinitionsDistinctName", Nil,"GET", """""", Routes.prefix + """api/metadata/distinctdefinitions"""))
   }
}
        

// @LINE:331
case api_Metadata_getAutocompleteName197(params) => {
   call(params.fromQuery[String]("filter", None)) { (filter) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getAutocompleteName(filter), HandlerDef(this, "api.Metadata", "getAutocompleteName", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/metadata/autocompletenames"""))
   }
}
        

// @LINE:332
case api_Metadata_getDefinition198(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getDefinition(id), HandlerDef(this, "api.Metadata", "getDefinition", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/metadata/definitions/$id<[^/]+>"""))
   }
}
        

// @LINE:333
case api_Metadata_getUrl199(params) => {
   call(params.fromPath[String]("in_url", None)) { (in_url) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getUrl(in_url), HandlerDef(this, "api.Metadata", "getUrl", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/metadata/url/$in_url<.+>"""))
   }
}
        

// @LINE:334
case api_Metadata_editDefinition200(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("spaceId", Some(None))) { (id, spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).editDefinition(id, spaceId), HandlerDef(this, "api.Metadata", "editDefinition", Seq(classOf[UUID], classOf[Option[String]]),"PUT", """""", Routes.prefix + """api/metadata/definitions/$id<[^/]+>"""))
   }
}
        

// @LINE:335
case api_Metadata_addDefinition201(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).addDefinition(), HandlerDef(this, "api.Metadata", "addDefinition", Nil,"POST", """""", Routes.prefix + """api/metadata/definitions"""))
   }
}
        

// @LINE:336
case api_Metadata_deleteDefinition202(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).deleteDefinition(id), HandlerDef(this, "api.Metadata", "deleteDefinition", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """admin/metadata/definitions/$id<[^/]+>"""))
   }
}
        

// @LINE:338
case api_Metadata_listPeople203(params) => {
   call(params.fromQuery[String]("term", None), params.fromQuery[Int]("limit", None)) { (term, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).listPeople(term, limit), HandlerDef(this, "api.Metadata", "listPeople", Seq(classOf[String], classOf[Int]),"GET", """""", Routes.prefix + """api/metadata/people"""))
   }
}
        

// @LINE:339
case api_Metadata_getPerson204(params) => {
   call(params.fromPath[String]("pid", None)) { (pid) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getPerson(pid), HandlerDef(this, "api.Metadata", "getPerson", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/metadata/people/$pid<[^/]+>"""))
   }
}
        

// @LINE:341
case api_Metadata_getRepository205(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).getRepository(id), HandlerDef(this, "api.Metadata", "getRepository", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/metadata/repository/$id<[^/]+>"""))
   }
}
        

// @LINE:345
case api_Logos_list206(params) => {
   call(params.fromQuery[Option[String]]("path", Some(None)), params.fromQuery[Option[String]]("name", Some(None))) { (path, name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).list(path, name), HandlerDef(this, "api.Logos", "list", Seq(classOf[Option[String]], classOf[Option[String]]),"GET", """ ----------------------------------------------------------------------
 LOGOS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/logos"""))
   }
}
        

// @LINE:346
case api_Logos_upload207(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).upload, HandlerDef(this, "api.Logos", "upload", Nil,"POST", """""", Routes.prefix + """api/logos"""))
   }
}
        

// @LINE:347
case api_Logos_getId208(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).getId(id), HandlerDef(this, "api.Logos", "getId", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/logos/$id<[^/]+>"""))
   }
}
        

// @LINE:348
case api_Logos_putId209(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).putId(id), HandlerDef(this, "api.Logos", "putId", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/logos/$id<[^/]+>"""))
   }
}
        

// @LINE:349
case api_Logos_downloadId210(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("default", Some(None))) { (id, default) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).downloadId(id, default), HandlerDef(this, "api.Logos", "downloadId", Seq(classOf[UUID], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/logos/$id<[^/]+>/blob"""))
   }
}
        

// @LINE:350
case api_Logos_deleteId211(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).deleteId(id), HandlerDef(this, "api.Logos", "deleteId", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/logos/$id<[^/]+>"""))
   }
}
        

// @LINE:351
case api_Logos_getPath212(params) => {
   call(params.fromPath[String]("path", None), params.fromPath[String]("name", None)) { (path, name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).getPath(path, name), HandlerDef(this, "api.Logos", "getPath", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """api/logos/$path<[^/]+>/$name<[^/]+>"""))
   }
}
        

// @LINE:352
case api_Logos_putPath213(params) => {
   call(params.fromPath[String]("path", None), params.fromPath[String]("name", None)) { (path, name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).putPath(path, name), HandlerDef(this, "api.Logos", "putPath", Seq(classOf[String], classOf[String]),"PUT", """""", Routes.prefix + """api/logos/$path<[^/]+>/$name<[^/]+>"""))
   }
}
        

// @LINE:353
case api_Logos_downloadPath214(params) => {
   call(params.fromPath[String]("path", None), params.fromPath[String]("name", None), params.fromQuery[Option[String]]("default", Some(None))) { (path, name, default) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).downloadPath(path, name, default), HandlerDef(this, "api.Logos", "downloadPath", Seq(classOf[String], classOf[String], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/logos/$path<[^/]+>/$name<[^/]+>/blob"""))
   }
}
        

// @LINE:354
case api_Logos_deletePath215(params) => {
   call(params.fromPath[String]("path", None), params.fromPath[String]("name", None)) { (path, name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Logos]).deletePath(path, name), HandlerDef(this, "api.Logos", "deletePath", Seq(classOf[String], classOf[String]),"DELETE", """""", Routes.prefix + """api/logos/$path<[^/]+>/$name<[^/]+>"""))
   }
}
        

// @LINE:359
case api_Files_attachGeometry216(params) => {
   call(params.fromPath[UUID]("three_d_file_id", None), params.fromPath[UUID]("geometry_id", None)) { (three_d_file_id, geometry_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).attachGeometry(three_d_file_id, geometry_id), HandlerDef(this, "api.Files", "attachGeometry", Seq(classOf[UUID], classOf[UUID]),"POST", """ ----------------------------------------------------------------------
 FILES ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/files/$three_d_file_id<[^/]+>/geometries/$geometry_id<[^/]+>"""))
   }
}
        

// @LINE:360
case api_Files_attachTexture217(params) => {
   call(params.fromPath[UUID]("three_d_file_id", None), params.fromPath[UUID]("texture_id", None)) { (three_d_file_id, texture_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).attachTexture(three_d_file_id, texture_id), HandlerDef(this, "api.Files", "attachTexture", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$three_d_file_id<[^/]+>/3dTextures/$texture_id<[^/]+>"""))
   }
}
        

// @LINE:361
case api_Files_attachThumbnail218(params) => {
   call(params.fromPath[UUID]("file_id", None), params.fromPath[UUID]("thumbnail_id", None)) { (file_id, thumbnail_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).attachThumbnail(file_id, thumbnail_id), HandlerDef(this, "api.Files", "attachThumbnail", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$file_id<[^/]+>/thumbnails/$thumbnail_id<[^/]+>"""))
   }
}
        

// @LINE:362
case api_Files_attachQueryThumbnail219(params) => {
   call(params.fromPath[UUID]("file_id", None), params.fromPath[UUID]("thumbnail_id", None)) { (file_id, thumbnail_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).attachQueryThumbnail(file_id, thumbnail_id), HandlerDef(this, "api.Files", "attachQueryThumbnail", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/queries/$file_id<[^/]+>/thumbnails/$thumbnail_id<[^/]+>"""))
   }
}
        

// @LINE:363
case api_Files_list220(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).list, HandlerDef(this, "api.Files", "list", Nil,"GET", """""", Routes.prefix + """api/files"""))
   }
}
        

// @LINE:364
case api_Files_upload221(params) => {
   call(params.fromQuery[String]("showPreviews", Some("DatasetLevel")), params.fromQuery[String]("originalZipFile", Some("")), params.fromQuery[String]("flags", Some(""))) { (showPreviews, originalZipFile, flags) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).upload(showPreviews, originalZipFile, flags), HandlerDef(this, "api.Files", "upload", Seq(classOf[String], classOf[String], classOf[String]),"POST", """""", Routes.prefix + """api/files"""))
   }
}
        

// @LINE:365
case api_Files_upload222(params) => {
   call(params.fromQuery[String]("showPreviews", Some("DatasetLevel")), params.fromQuery[String]("originalZipFile", Some("")), params.fromPath[String]("flags", None)) { (showPreviews, originalZipFile, flags) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).upload(showPreviews, originalZipFile, flags), HandlerDef(this, "api.Files", "upload", Seq(classOf[String], classOf[String], classOf[String]),"POST", """""", Routes.prefix + """api/files/withFlags/$flags<[^/]+>"""))
   }
}
        

// @LINE:366
case api_Files_searchFilesUserMetadata223(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).searchFilesUserMetadata, HandlerDef(this, "api.Files", "searchFilesUserMetadata", Nil,"POST", """""", Routes.prefix + """api/files/searchusermetadata"""))
   }
}
        

// @LINE:367
case api_Files_searchFilesGeneralMetadata224(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).searchFilesGeneralMetadata, HandlerDef(this, "api.Files", "searchFilesGeneralMetadata", Nil,"POST", """""", Routes.prefix + """api/files/searchmetadata"""))
   }
}
        

// @LINE:368
case api_Files_uploadToDataset225(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("showPreviews", Some("DatasetLevel")), params.fromQuery[String]("originalZipFile", Some("")), params.fromPath[String]("flags", None)) { (id, showPreviews, originalZipFile, flags) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).uploadToDataset(id, showPreviews, originalZipFile, flags), HandlerDef(this, "api.Files", "uploadToDataset", Seq(classOf[UUID], classOf[String], classOf[String], classOf[String]),"POST", """""", Routes.prefix + """api/uploadToDataset/withFlags/$id<[^/]+>/$flags<[^/]+>"""))
   }
}
        

// @LINE:369
case api_Files_uploadToDataset226(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("showPreviews", Some("DatasetLevel")), params.fromQuery[String]("originalZipFile", Some("")), params.fromQuery[String]("flags", Some(""))) { (id, showPreviews, originalZipFile, flags) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).uploadToDataset(id, showPreviews, originalZipFile, flags), HandlerDef(this, "api.Files", "uploadToDataset", Seq(classOf[UUID], classOf[String], classOf[String], classOf[String]),"POST", """""", Routes.prefix + """api/uploadToDataset/$id<[^/]+>"""))
   }
}
        

// @LINE:370
case api_Files_updateDescription227(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).updateDescription(id), HandlerDef(this, "api.Files", "updateDescription", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/files/$id<[^/]+>/updateDescription"""))
   }
}
        

// @LINE:371
case api_Files_uploadIntermediate228(params) => {
   call(params.fromPath[String]("idAndFlags", None)) { (idAndFlags) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).uploadIntermediate(idAndFlags), HandlerDef(this, "api.Files", "uploadIntermediate", Seq(classOf[String]),"POST", """""", Routes.prefix + """api/files/uploadIntermediate/$idAndFlags<[^/]+>"""))
   }
}
        

// @LINE:372
case api_Files_sendJob229(params) => {
   call(params.fromPath[UUID]("fileId", None), params.fromPath[String]("fileType", None)) { (fileId, fileType) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).sendJob(fileId, fileType), HandlerDef(this, "api.Files", "sendJob", Seq(classOf[UUID], classOf[String]),"POST", """""", Routes.prefix + """api/files/sendJob/$fileId<[^/]+>/$fileType<[^/]+>"""))
   }
}
        

// @LINE:373
case api_Files_getRDFURLsForFile230(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getRDFURLsForFile(id), HandlerDef(this, "api.Files", "getRDFURLsForFile", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/getRDFURLsForFile/$id<[^/]+>"""))
   }
}
        

// @LINE:374
case api_Files_getRDFUserMetadata231(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("mappingNum", Some("1"))) { (id, mappingNum) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getRDFUserMetadata(id, mappingNum), HandlerDef(this, "api.Files", "getRDFUserMetadata", Seq(classOf[UUID], classOf[String]),"GET", """""", Routes.prefix + """api/files/rdfUserMetadata/$id<[^/]+>"""))
   }
}
        

// @LINE:375
case api_Files_download232(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).download(id), HandlerDef(this, "api.Files", "download", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/blob"""))
   }
}
        

// @LINE:376
case api_Files_removeFile233(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).removeFile(id), HandlerDef(this, "api.Files", "removeFile", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/remove"""))
   }
}
        

// @LINE:377
case api_Files_get234(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).get(id), HandlerDef(this, "api.Files", "get", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/metadata"""))
   }
}
        

// @LINE:378
case api_Files_addMetadata235(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).addMetadata(id), HandlerDef(this, "api.Files", "addMetadata", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/metadata"""))
   }
}
        

// @LINE:379
case api_Files_getMetadataDefinitions236(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("space", Some(None))) { (id, space) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getMetadataDefinitions(id, space), HandlerDef(this, "api.Files", "getMetadataDefinitions", Seq(classOf[UUID], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/metadataDefinitions"""))
   }
}
        

// @LINE:380
case api_Files_updateMetadata237(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("extractor_id", None)) { (id, extractor_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).updateMetadata(id, extractor_id), HandlerDef(this, "api.Files", "updateMetadata", Seq(classOf[UUID], classOf[String]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/updateMetadata"""))
   }
}
        

// @LINE:381
case api_Files_addVersusMetadata238(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).addVersusMetadata(id), HandlerDef(this, "api.Files", "addVersusMetadata", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/versus_metadata"""))
   }
}
        

// @LINE:382
case api_Files_getVersusMetadataJSON239(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getVersusMetadataJSON(id), HandlerDef(this, "api.Files", "getVersusMetadataJSON", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/versus_metadata"""))
   }
}
        

// @LINE:383
case api_Files_getUserMetadataJSON240(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getUserMetadataJSON(id), HandlerDef(this, "api.Files", "getUserMetadataJSON", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/usermetadatajson"""))
   }
}
        

// @LINE:384
case api_Files_addUserMetadata241(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).addUserMetadata(id), HandlerDef(this, "api.Files", "addUserMetadata", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/usermetadata"""))
   }
}
        

// @LINE:385
case api_Files_getTechnicalMetadataJSON242(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getTechnicalMetadataJSON(id), HandlerDef(this, "api.Files", "getTechnicalMetadataJSON", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/technicalmetadatajson"""))
   }
}
        

// @LINE:386
case api_Files_getXMLMetadataJSON243(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getXMLMetadataJSON(id), HandlerDef(this, "api.Files", "getXMLMetadataJSON", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/xmlmetadatajson"""))
   }
}
        

// @LINE:387
case api_Files_follow244(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).follow(id), HandlerDef(this, "api.Files", "follow", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/follow"""))
   }
}
        

// @LINE:388
case api_Files_unfollow245(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).unfollow(id), HandlerDef(this, "api.Files", "unfollow", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/unfollow"""))
   }
}
        

// @LINE:389
case api_Files_comment246(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).comment(id), HandlerDef(this, "api.Files", "comment", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/comment"""))
   }
}
        

// @LINE:390
case api_Files_removeFile247(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).removeFile(id), HandlerDef(this, "api.Files", "removeFile", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/files/$id<[^/]+>"""))
   }
}
        

// @LINE:391
case api_Files_getTags248(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getTags(id), HandlerDef(this, "api.Files", "getTags", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/tags"""))
   }
}
        

// @LINE:392
case api_Files_addTags249(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).addTags(id), HandlerDef(this, "api.Files", "addTags", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/tags"""))
   }
}
        

// @LINE:393
case api_Files_removeTags250(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).removeTags(id), HandlerDef(this, "api.Files", "removeTags", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/tags/remove"""))
   }
}
        

// @LINE:394
case api_Files_removeAllTags251(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).removeAllTags(id), HandlerDef(this, "api.Files", "removeAllTags", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/tags/remove_all"""))
   }
}
        

// @LINE:395
case api_Files_removeTags252(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).removeTags(id), HandlerDef(this, "api.Files", "removeTags", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/files/$id<[^/]+>/tags"""))
   }
}
        

// @LINE:396
case api_Files_updateLicense253(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).updateLicense(id), HandlerDef(this, "api.Files", "updateLicense", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/license"""))
   }
}
        

// @LINE:397
case api_Files_extract254(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).extract(id), HandlerDef(this, "api.Files", "extract", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/extracted_metadata"""))
   }
}
        

// @LINE:398
case api_Files_updateFileName255(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).updateFileName(id), HandlerDef(this, "api.Files", "updateFileName", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/files/$id<[^/]+>/filename"""))
   }
}
        

// @LINE:399
case api_Files_reindex256(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).reindex(id), HandlerDef(this, "api.Files", "reindex", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/reindex"""))
   }
}
        

// @LINE:400
case api_Files_users257(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).users(id), HandlerDef(this, "api.Files", "users", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/users"""))
   }
}
        

// @LINE:401
case api_Files_paths258(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).paths(id), HandlerDef(this, "api.Files", "paths", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/paths"""))
   }
}
        

// @LINE:402
case api_Files_changeOwner259(params) => {
   call(params.fromPath[UUID]("id", None), params.fromPath[UUID]("ownerId", None)) { (id, ownerId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).changeOwner(id, ownerId), HandlerDef(this, "api.Files", "changeOwner", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$id<[^/]+>/changeOwner/$ownerId<[^/]+>"""))
   }
}
        

// @LINE:403
case api_Files_getOwner260(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getOwner(id), HandlerDef(this, "api.Files", "getOwner", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/getOwner"""))
   }
}
        

// @LINE:407
case api_Extractions_listExtractors261(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).listExtractors(), HandlerDef(this, "api.Extractions", "listExtractors", Nil,"GET", """ ----------------------------------------------------------------------
 EXTRACTORS ENDPOINTS
 ----------------------------------------------------------------------""", Routes.prefix + """api/extractors"""))
   }
}
        

// @LINE:408
case api_Extractions_getExtractorInfo262(params) => {
   call(params.fromPath[String]("name", None)) { (name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getExtractorInfo(name), HandlerDef(this, "api.Extractions", "getExtractorInfo", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/extractors/$name<[^/]+>"""))
   }
}
        

// @LINE:410
case api_Extractions_addExtractorInfo263(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).addExtractorInfo(), HandlerDef(this, "api.Extractions", "addExtractorInfo", Nil,"POST", """GET            /api/extractors/:id                                                      @api.Extractions.getExtractorInfo(id: UUID)""", Routes.prefix + """api/extractors"""))
   }
}
        

// @LINE:412
case api_Extractions_submitFileToExtractor264(params) => {
   call(params.fromPath[UUID]("file_id", None)) { (file_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).submitFileToExtractor(file_id), HandlerDef(this, "api.Extractions", "submitFileToExtractor", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/files/$file_id<[^/]+>/extractions"""))
   }
}
        

// @LINE:413
case api_Extractions_submitDatasetToExtractor265(params) => {
   call(params.fromPath[UUID]("ds_id", None)) { (ds_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).submitDatasetToExtractor(ds_id), HandlerDef(this, "api.Extractions", "submitDatasetToExtractor", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$ds_id<[^/]+>/extractions"""))
   }
}
        

// @LINE:415
case api_Extractions_getDTSRequests266(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getDTSRequests(), HandlerDef(this, "api.Extractions", "getDTSRequests", Nil,"GET", """""", Routes.prefix + """api/extractions/requests"""))
   }
}
        

// @LINE:416
case api_Extractions_getExtractorServersIP267(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getExtractorServersIP(), HandlerDef(this, "api.Extractions", "getExtractorServersIP", Nil,"GET", """""", Routes.prefix + """api/extractions/servers_ips"""))
   }
}
        

// @LINE:417
case api_Extractions_getExtractorNames268(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getExtractorNames(), HandlerDef(this, "api.Extractions", "getExtractorNames", Nil,"GET", """""", Routes.prefix + """api/extractions/extractors_names"""))
   }
}
        

// @LINE:418
case api_Extractions_getExtractorInputTypes269(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getExtractorInputTypes(), HandlerDef(this, "api.Extractions", "getExtractorInputTypes", Nil,"GET", """""", Routes.prefix + """api/extractions/supported_input_types"""))
   }
}
        

// @LINE:420
case api_Extractions_getExtractorDetails270(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).getExtractorDetails(), HandlerDef(this, "api.Extractions", "getExtractorDetails", Nil,"GET", """API for temporary fix for BD-289""", Routes.prefix + """api/extractions/extractors_details"""))
   }
}
        

// @LINE:421
case api_Extractions_uploadByURL271(params) => {
   call(params.fromQuery[Boolean]("extract", Some(true))) { (extract) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).uploadByURL(extract), HandlerDef(this, "api.Extractions", "uploadByURL", Seq(classOf[Boolean]),"POST", """""", Routes.prefix + """api/extractions/upload_url"""))
   }
}
        

// @LINE:422
case api_Extractions_multipleUploadByURL272(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).multipleUploadByURL(), HandlerDef(this, "api.Extractions", "multipleUploadByURL", Nil,"POST", """""", Routes.prefix + """api/extractions/multiple_uploadby_url"""))
   }
}
        

// @LINE:423
case api_Extractions_uploadExtract273(params) => {
   call(params.fromQuery[String]("showPreviews", Some("DatasetLevel")), params.fromQuery[Boolean]("extract", Some(true))) { (showPreviews, extract) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).uploadExtract(showPreviews, extract), HandlerDef(this, "api.Extractions", "uploadExtract", Seq(classOf[String], classOf[Boolean]),"POST", """""", Routes.prefix + """api/extractions/upload_file"""))
   }
}
        

// @LINE:424
case api_Extractions_checkExtractorsStatus274(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).checkExtractorsStatus(id), HandlerDef(this, "api.Extractions", "checkExtractorsStatus", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/extractions/$id<[^/]+>/status"""))
   }
}
        

// @LINE:425
case api_Extractions_fetch275(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).fetch(id), HandlerDef(this, "api.Extractions", "fetch", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/extractions/$id<[^/]+>/metadata"""))
   }
}
        

// @LINE:426
case api_Extractions_checkExtractionsStatuses276(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Extractions]).checkExtractionsStatuses(id), HandlerDef(this, "api.Extractions", "checkExtractionsStatuses", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/extractions/$id<[^/]+>/statuses"""))
   }
}
        

// @LINE:432
case api_Files_attachPreview277(params) => {
   call(params.fromPath[UUID]("id", None), params.fromPath[UUID]("p_id", None)) { (id, p_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).attachPreview(id, p_id), HandlerDef(this, "api.Files", "attachPreview", Seq(classOf[UUID], classOf[UUID]),"POST", """ ----------------------------------------------------------------------
 PREVIEWS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/files/$id<[^/]+>/previews/$p_id<[^/]+>"""))
   }
}
        

// @LINE:433
case api_Files_getWithPreviewUrls278(params) => {
   call(params.fromPath[UUID]("file", None)) { (file) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getWithPreviewUrls(file), HandlerDef(this, "api.Files", "getWithPreviewUrls", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/previewJson/$file<[^/]+>"""))
   }
}
        

// @LINE:434
case api_Files_filePreviewsList279(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).filePreviewsList(id), HandlerDef(this, "api.Files", "filePreviewsList", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/listpreviews"""))
   }
}
        

// @LINE:435
case api_Files_getPreviews280(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getPreviews(id), HandlerDef(this, "api.Files", "getPreviews", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/getPreviews"""))
   }
}
        

// @LINE:436
case api_Files_isBeingProcessed281(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).isBeingProcessed(id), HandlerDef(this, "api.Files", "isBeingProcessed", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/files/$id<[^/]+>/isBeingProcessed"""))
   }
}
        

// @LINE:438
case api_Files_getTexture282(params) => {
   call(params.fromPath[UUID]("three_d_file_id", None), params.fromPath[String]("filename", None)) { (three_d_file_id, filename) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getTexture(three_d_file_id, filename), HandlerDef(this, "api.Files", "getTexture", Seq(classOf[UUID], classOf[String]),"GET", """GET		   /api/files/:three_d_file_id/:filename			                        @api.Files.getGeometry(three_d_file_id: UUID, filename)""", Routes.prefix + """api/files/$three_d_file_id<[^/]+>/$filename<[^/]+>"""))
   }
}
        

// @LINE:439
case api_Files_downloadquery283(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).downloadquery(id), HandlerDef(this, "api.Files", "downloadquery", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/queries/$id<[^/]+>"""))
   }
}
        

// @LINE:441
case api_Files_download284(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).download(id), HandlerDef(this, "api.Files", "download", Seq(classOf[UUID]),"GET", """ TODO please use /api/files/:id/blob to get the actual data see MMDB-1685""", Routes.prefix + """api/files/$id<[^/]+>"""))
   }
}
        

// @LINE:446
case api_Files_dumpFilesMetadata285(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).dumpFilesMetadata, HandlerDef(this, "api.Files", "dumpFilesMetadata", Nil,"POST", """ ----------------------------------------------------------------------
 DANGEROUS ENDPOINTS
 ----------------------------------------------------------------------""", Routes.prefix + """api/dumpFilesMd"""))
   }
}
        

// @LINE:447
case api_Datasets_dumpDatasetsMetadata286(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).dumpDatasetsMetadata, HandlerDef(this, "api.Datasets", "dumpDatasetsMetadata", Nil,"POST", """""", Routes.prefix + """api/dumpDatasetsMd"""))
   }
}
        

// @LINE:448
case api_Datasets_dumpDatasetGroupings287(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).dumpDatasetGroupings, HandlerDef(this, "api.Datasets", "dumpDatasetGroupings", Nil,"POST", """""", Routes.prefix + """api/dumpDatasetGroupings"""))
   }
}
        

// @LINE:454
case api_Files_getTexture288(params) => {
   call(params.fromPath[UUID]("three_d_file_id", None), params.fromPath[String]("filename", None)) { (three_d_file_id, filename) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).getTexture(three_d_file_id, filename), HandlerDef(this, "api.Files", "getTexture", Seq(classOf[UUID], classOf[String]),"GET", """----------------------------------------------------------------------
 3D FILES
----------------------------------------------------------------------
GET		/api/files/:three_d_file_id/:filename			    @api.Files.getGeometry(three_d_file_id: UUID, filename)""", Routes.prefix + """api/files/$three_d_file_id<[^/]+>/$filename<[^/]+>"""))
   }
}
        

// @LINE:460
case api_Spaces_list289(params) => {
   call(params.fromQuery[Option[String]]("title", Some(None)), params.fromQuery[Option[String]]("date", Some(None)), params.fromQuery[Int]("limit", Some(12))) { (title, date, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).list(title, date, limit), HandlerDef(this, "api.Spaces", "list", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int]),"GET", """ ----------------------------------------------------------------------
 SPACES ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/spaces"""))
   }
}
        

// @LINE:461
case api_Spaces_listCanEdit290(params) => {
   call(params.fromQuery[Option[String]]("title", Some(None)), params.fromQuery[Option[String]]("date", Some(None)), params.fromQuery[Int]("limit", Some(12))) { (title, date, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).listCanEdit(title, date, limit), HandlerDef(this, "api.Spaces", "listCanEdit", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int]),"GET", """""", Routes.prefix + """api/spaces/canEdit"""))
   }
}
        

// @LINE:462
case api_Spaces_get291(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).get(id), HandlerDef(this, "api.Spaces", "get", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/spaces/$id<[^/]+>"""))
   }
}
        

// @LINE:463
case api_Spaces_createSpace292(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).createSpace(), HandlerDef(this, "api.Spaces", "createSpace", Nil,"POST", """""", Routes.prefix + """api/spaces"""))
   }
}
        

// @LINE:464
case api_Spaces_removeSpace293(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).removeSpace(id), HandlerDef(this, "api.Spaces", "removeSpace", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/spaces/$id<[^/]+>"""))
   }
}
        

// @LINE:465
case api_Spaces_removeCollection294(params) => {
   call(params.fromPath[UUID]("spaceId", None), params.fromPath[UUID]("collectionId", None), params.fromQuery[Boolean]("removeDatasets", None)) { (spaceId, collectionId, removeDatasets) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).removeCollection(spaceId, collectionId, removeDatasets), HandlerDef(this, "api.Spaces", "removeCollection", Seq(classOf[UUID], classOf[UUID], classOf[Boolean]),"POST", """""", Routes.prefix + """api/spaces/$spaceId<[^/]+>/removeCollection/$collectionId<[^/]+>"""))
   }
}
        

// @LINE:466
case api_Spaces_removeDataset295(params) => {
   call(params.fromPath[UUID]("spaceId", None), params.fromPath[UUID]("datasetId", None)) { (spaceId, datasetId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).removeDataset(spaceId, datasetId), HandlerDef(this, "api.Spaces", "removeDataset", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/spaces/$spaceId<[^/]+>/removeDataset/$datasetId<[^/]+>"""))
   }
}
        

// @LINE:467
case api_Spaces_copyContentsToSpace296(params) => {
   call(params.fromPath[UUID]("spaceId", None), params.fromPath[UUID]("id", None)) { (spaceId, id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).copyContentsToSpace(spaceId, id), HandlerDef(this, "api.Spaces", "copyContentsToSpace", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/spaces/$spaceId<[^/]+>/copyContentsToSpace/$id<[^/]+>"""))
   }
}
        

// @LINE:468
case api_Spaces_copyContentsToNewSpace297(params) => {
   call(params.fromPath[UUID]("spaceId", None)) { (spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).copyContentsToNewSpace(spaceId), HandlerDef(this, "api.Spaces", "copyContentsToNewSpace", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/spaces/$spaceId<[^/]+>/copyContentsToNewSpace"""))
   }
}
        

// @LINE:469
case api_Spaces_updateSpace298(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).updateSpace(id), HandlerDef(this, "api.Spaces", "updateSpace", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/spaces/$id<[^/]+>/update"""))
   }
}
        

// @LINE:470
case api_Spaces_updateUsers299(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).updateUsers(id), HandlerDef(this, "api.Spaces", "updateUsers", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/spaces/$id<[^/]+>/updateUsers"""))
   }
}
        

// @LINE:471
case api_Spaces_acceptRequest300(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("user", None), params.fromQuery[String]("role", None)) { (id, user, role) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).acceptRequest(id, user, role), HandlerDef(this, "api.Spaces", "acceptRequest", Seq(classOf[UUID], classOf[String], classOf[String]),"POST", """""", Routes.prefix + """api/spaces/$id<[^/]+>/acceptRequest"""))
   }
}
        

// @LINE:472
case api_Spaces_rejectRequest301(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("user", None)) { (id, user) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).rejectRequest(id, user), HandlerDef(this, "api.Spaces", "rejectRequest", Seq(classOf[UUID], classOf[String]),"POST", """""", Routes.prefix + """api/spaces/$id<[^/]+>/rejectRequest"""))
   }
}
        

// @LINE:473
case api_Spaces_removeUser302(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("removeUser", None)) { (id, removeUser) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).removeUser(id, removeUser), HandlerDef(this, "api.Spaces", "removeUser", Seq(classOf[UUID], classOf[String]),"POST", """""", Routes.prefix + """api/spaces/$id<[^/]+>/removeUser"""))
   }
}
        

// @LINE:474
case api_Spaces_addDatasetToSpace303(params) => {
   call(params.fromPath[UUID]("spaceId", None), params.fromPath[UUID]("datasetId", None)) { (spaceId, datasetId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).addDatasetToSpace(spaceId, datasetId), HandlerDef(this, "api.Spaces", "addDatasetToSpace", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/spaces/$spaceId<[^/]+>/addDatasetToSpace/$datasetId<[^/]+>"""))
   }
}
        

// @LINE:475
case api_Spaces_addCollectionToSpace304(params) => {
   call(params.fromPath[UUID]("spaceId", None), params.fromPath[UUID]("collectionId", None)) { (spaceId, collectionId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).addCollectionToSpace(spaceId, collectionId), HandlerDef(this, "api.Spaces", "addCollectionToSpace", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/spaces/$spaceId<[^/]+>/addCollectionToSpace/$collectionId<[^/]+>"""))
   }
}
        

// @LINE:476
case api_Spaces_follow305(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).follow(id), HandlerDef(this, "api.Spaces", "follow", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/spaces/$id<[^/]+>/follow"""))
   }
}
        

// @LINE:477
case api_Spaces_unfollow306(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).unfollow(id), HandlerDef(this, "api.Spaces", "unfollow", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/spaces/$id<[^/]+>/unfollow"""))
   }
}
        

// @LINE:478
case api_Metadata_addDefinitionToSpace307(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Metadata]).addDefinitionToSpace(id), HandlerDef(this, "api.Metadata", "addDefinitionToSpace", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/spaces/$id<[^/]+>/metadata"""))
   }
}
        

// @LINE:479
case api_Spaces_verifySpace308(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).verifySpace(id), HandlerDef(this, "api.Spaces", "verifySpace", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/spaces/$id<[^/]+>/verify"""))
   }
}
        

// @LINE:480
case api_Spaces_listDatasets309(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Integer]("limit", Some(0))) { (id, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).listDatasets(id, limit), HandlerDef(this, "api.Spaces", "listDatasets", Seq(classOf[UUID], classOf[Integer]),"GET", """""", Routes.prefix + """api/spaces/$id<[^/]+>/datasets"""))
   }
}
        

// @LINE:481
case api_Spaces_listCollections310(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Integer]("limit", Some(0))) { (id, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).listCollections(id, limit), HandlerDef(this, "api.Spaces", "listCollections", Seq(classOf[UUID], classOf[Integer]),"GET", """""", Routes.prefix + """api/spaces/$id<[^/]+>/collections"""))
   }
}
        

// @LINE:482
case api_Spaces_listCollectionsCanEdit311(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Integer]("limit", Some(0))) { (id, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).listCollectionsCanEdit(id, limit), HandlerDef(this, "api.Spaces", "listCollectionsCanEdit", Seq(classOf[UUID], classOf[Integer]),"GET", """""", Routes.prefix + """api/spaces/$id<[^/]+>/collectionsCanEdit"""))
   }
}
        

// @LINE:483
case api_Spaces_getUserSpaceRoleMap312(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Spaces]).getUserSpaceRoleMap(id), HandlerDef(this, "api.Spaces", "getUserSpaceRoleMap", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/spaces/$id<[^/]+>/userSpaceRoleMap"""))
   }
}
        

// @LINE:488
case api_Collections_list313(params) => {
   call(params.fromQuery[Option[String]]("title", Some(None)), params.fromQuery[Option[String]]("date", Some(None)), params.fromQuery[Int]("limit", Some(12)), params.fromQuery[Boolean]("exact", Some(false))) { (title, date, limit, exact) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).list(title, date, limit, exact), HandlerDef(this, "api.Collections", "list", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]),"GET", """ ----------------------------------------------------------------------
 COLLECTIONS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/collections"""))
   }
}
        

// @LINE:489
case api_Collections_listCanEdit314(params) => {
   call(params.fromQuery[Option[String]]("title", Some(None)), params.fromQuery[Option[String]]("date", Some(None)), params.fromQuery[Int]("limit", Some(12)), params.fromQuery[Boolean]("exact", Some(false))) { (title, date, limit, exact) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).listCanEdit(title, date, limit, exact), HandlerDef(this, "api.Collections", "listCanEdit", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]),"GET", """""", Routes.prefix + """api/collections/canEdit"""))
   }
}
        

// @LINE:490
case api_Collections_addDatasetToCollectionOptions315(params) => {
   call(params.fromPath[UUID]("ds_id", None), params.fromQuery[Option[String]]("title", Some(None)), params.fromQuery[Option[String]]("date", Some(None)), params.fromQuery[Int]("limit", Some(12)), params.fromQuery[Boolean]("exact", Some(false))) { (ds_id, title, date, limit, exact) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).addDatasetToCollectionOptions(ds_id, title, date, limit, exact), HandlerDef(this, "api.Collections", "addDatasetToCollectionOptions", Seq(classOf[UUID], classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]),"GET", """""", Routes.prefix + """api/collections/datasetPossibleParents/$ds_id<[^/]+>"""))
   }
}
        

// @LINE:491
case api_Collections_listPossibleParents316(params) => {
   call(params.fromQuery[String]("currentCollectionId", None), params.fromQuery[Option[String]]("title", Some(None)), params.fromQuery[Option[String]]("date", Some(None)), params.fromQuery[Int]("limit", Some(12)), params.fromQuery[Boolean]("exact", Some(false))) { (currentCollectionId, title, date, limit, exact) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).listPossibleParents(currentCollectionId, title, date, limit, exact), HandlerDef(this, "api.Collections", "listPossibleParents", Seq(classOf[String], classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]),"GET", """""", Routes.prefix + """api/collections/possibleParents"""))
   }
}
        

// @LINE:492
case api_Collections_createCollection317(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).createCollection(), HandlerDef(this, "api.Collections", "createCollection", Nil,"POST", """""", Routes.prefix + """api/collections"""))
   }
}
        

// @LINE:493
case api_Collections_getRootCollections318(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getRootCollections, HandlerDef(this, "api.Collections", "getRootCollections", Nil,"GET", """""", Routes.prefix + """api/collections/rootCollections"""))
   }
}
        

// @LINE:494
case api_Collections_getTopLevelCollections319(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getTopLevelCollections, HandlerDef(this, "api.Collections", "getTopLevelCollections", Nil,"GET", """""", Routes.prefix + """api/collections/topLevelCollections"""))
   }
}
        

// @LINE:495
case api_Collections_getAllCollections320(params) => {
   call(params.fromQuery[Int]("limit", Some(0)), params.fromQuery[Boolean]("showAll", Some(false))) { (limit, showAll) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getAllCollections(limit, showAll), HandlerDef(this, "api.Collections", "getAllCollections", Seq(classOf[Int], classOf[Boolean]),"GET", """""", Routes.prefix + """api/collections/allCollections"""))
   }
}
        

// @LINE:496
case api_Collections_download321(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Int]("compression", Some(-1))) { (id, compression) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).download(id, compression), HandlerDef(this, "api.Collections", "download", Seq(classOf[UUID], classOf[Int]),"GET", """""", Routes.prefix + """api/collections/$id<[^/]+>/download"""))
   }
}
        

// @LINE:497
case api_Collections_listCollectionsInTrash322(params) => {
   call(params.fromQuery[Int]("limit", Some(12))) { (limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).listCollectionsInTrash(limit), HandlerDef(this, "api.Collections", "listCollectionsInTrash", Seq(classOf[Int]),"GET", """""", Routes.prefix + """api/collections/listTrash"""))
   }
}
        

// @LINE:498
case api_Collections_emptyTrash323(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).emptyTrash(), HandlerDef(this, "api.Collections", "emptyTrash", Nil,"DELETE", """""", Routes.prefix + """api/collections/emptyTrash"""))
   }
}
        

// @LINE:499
case api_Collections_clearOldCollectionsTrash324(params) => {
   call(params.fromQuery[Int]("days", Some(30))) { (days) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).clearOldCollectionsTrash(days), HandlerDef(this, "api.Collections", "clearOldCollectionsTrash", Seq(classOf[Int]),"DELETE", """""", Routes.prefix + """api/collections/clearOldCollectionsTrash"""))
   }
}
        

// @LINE:500
case api_Collections_restoreCollection325(params) => {
   call(params.fromPath[UUID]("collectionId", None)) { (collectionId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).restoreCollection(collectionId), HandlerDef(this, "api.Collections", "restoreCollection", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/collections/restore/$collectionId<[^/]+>"""))
   }
}
        

// @LINE:511
case api_Collections_uploadZipToSpace326(params) => {
   call(params.fromQuery[Option[String]]("spaceId", Some(None))) { (spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).uploadZipToSpace(spaceId), HandlerDef(this, "api.Collections", "uploadZipToSpace", Seq(classOf[Option[String]]),"POST", """GET            /api/collections/trash                                                   @api.Collections.listCollectionsInTrash()
POST           /api/collections/uploadZip                                               @api.Collections.uploadZip()""", Routes.prefix + """api/collections/uploadZipToSpace"""))
   }
}
        

// @LINE:516
case api_Collections_moveChildCollection327(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).moveChildCollection(id), HandlerDef(this, "api.Collections", "moveChildCollection", Seq(classOf[UUID]),"POST", """DELETE         /api/collections/deleteAll                                               @api.Collections.removeAllCollectionsOfUser()
GET            /api/collections/:id/download                                            @api.Collections.download(id: UUID, bagit : Boolean ?= true, compression: Int ?= -1)
POST           /api/collections/:fileId/createFromZip                                   @api.Collections.createFromZip(fileId : UUID)
POST           /api/collections/:id/copyCollectionToSpace/:spaceId                      @api.Collections.copyCollectionToSpace(id : UUID, spaceId : UUID)""", Routes.prefix + """api/collections/$id<[^/]+>/moveChildCollection"""))
   }
}
        

// @LINE:517
case api_Collections_removeCollectionAndContents328(params) => {
   call(params.fromPath[UUID]("collectionId", None)) { (collectionId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeCollectionAndContents(collectionId), HandlerDef(this, "api.Collections", "removeCollectionAndContents", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/collections/$collectionId<[^/]+>/deleteCollectionAndContents"""))
   }
}
        

// @LINE:518
case api_Collections_moveDatasetToNewCollection329(params) => {
   call(params.fromPath[String]("oldCollection", None), params.fromPath[String]("dataset", None), params.fromPath[String]("newCollection", None)) { (oldCollection, dataset, newCollection) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).moveDatasetToNewCollection(oldCollection, dataset, newCollection), HandlerDef(this, "api.Collections", "moveDatasetToNewCollection", Seq(classOf[String], classOf[String], classOf[String]),"POST", """""", Routes.prefix + """api/collections/$oldCollection<[^/]+>/moveDataset/$dataset<[^/]+>/newCollection/$newCollection<[^/]+>"""))
   }
}
        

// @LINE:520
case api_Collections_list330(params) => {
   call(params.fromQuery[Option[String]]("title", Some(None)), params.fromQuery[Option[String]]("date", Some(None)), params.fromQuery[Int]("limit", Some(12)), params.fromQuery[Boolean]("exact", Some(false))) { (title, date, limit, exact) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).list(title, date, limit, exact), HandlerDef(this, "api.Collections", "list", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]),"GET", """ deprecrated""", Routes.prefix + """api/collections/list"""))
   }
}
        

// @LINE:521
case api_Collections_follow331(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).follow(id), HandlerDef(this, "api.Collections", "follow", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$id<[^/]+>/follow"""))
   }
}
        

// @LINE:522
case api_Collections_unfollow332(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).unfollow(id), HandlerDef(this, "api.Collections", "unfollow", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$id<[^/]+>/unfollow"""))
   }
}
        

// @LINE:523
case api_Datasets_listInCollection333(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listInCollection(coll_id), HandlerDef(this, "api.Datasets", "listInCollection", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/datasets"""))
   }
}
        

// @LINE:524
case api_Collections_attachDataset334(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("ds_id", None)) { (coll_id, ds_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).attachDataset(coll_id, ds_id), HandlerDef(this, "api.Collections", "attachDataset", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/datasets/$ds_id<[^/]+>"""))
   }
}
        

// @LINE:526
case api_Collections_removeDataset335(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("ds_id", None), params.fromPath[String]("ignoreNotFound", None)) { (coll_id, ds_id, ignoreNotFound) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeDataset(coll_id, ds_id, ignoreNotFound), HandlerDef(this, "api.Collections", "removeDataset", Seq(classOf[UUID], classOf[UUID], classOf[String]),"POST", """ deprecrated use DELETE""", Routes.prefix + """api/collections/$coll_id<[^/]+>/datasetsRemove/$ds_id<[^/]+>/$ignoreNotFound<[^/]+>"""))
   }
}
        

// @LINE:527
case api_Collections_removeDataset336(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("ds_id", None), params.fromQuery[String]("ignoreNotFound", Some("True"))) { (coll_id, ds_id, ignoreNotFound) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeDataset(coll_id, ds_id, ignoreNotFound), HandlerDef(this, "api.Collections", "removeDataset", Seq(classOf[UUID], classOf[UUID], classOf[String]),"DELETE", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/datasets/$ds_id<[^/]+>"""))
   }
}
        

// @LINE:529
case api_Collections_removeCollection337(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeCollection(coll_id), HandlerDef(this, "api.Collections", "removeCollection", Seq(classOf[UUID]),"POST", """ deprecrated use DELETE""", Routes.prefix + """api/collections/$coll_id<[^/]+>/remove"""))
   }
}
        

// @LINE:530
case api_Collections_reindex338(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromQuery[Boolean]("recursive", Some(true))) { (coll_id, recursive) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).reindex(coll_id, recursive), HandlerDef(this, "api.Collections", "reindex", Seq(classOf[UUID], classOf[Boolean]),"POST", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/reindex"""))
   }
}
        

// @LINE:532
case api_Datasets_listInCollection339(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listInCollection(coll_id), HandlerDef(this, "api.Datasets", "listInCollection", Seq(classOf[UUID]),"GET", """ deprecrated use datasets""", Routes.prefix + """api/collections/$coll_id<[^/]+>/getDatasets"""))
   }
}
        

// @LINE:533
case api_Collections_removeCollection340(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeCollection(coll_id), HandlerDef(this, "api.Collections", "removeCollection", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>"""))
   }
}
        

// @LINE:535
case api_Collections_deleteCollectionAndContents341(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("userid", None)) { (coll_id, userid) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).deleteCollectionAndContents(coll_id, userid), HandlerDef(this, "api.Collections", "deleteCollectionAndContents", Seq(classOf[UUID], classOf[UUID]),"DELETE", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/deleteContents/$userid<[^/]+>"""))
   }
}
        

// @LINE:536
case api_Collections_attachPreview342(params) => {
   call(params.fromPath[UUID]("c_id", None), params.fromPath[UUID]("p_id", None)) { (c_id, p_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).attachPreview(c_id, p_id), HandlerDef(this, "api.Collections", "attachPreview", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$c_id<[^/]+>/previews/$p_id<[^/]+>"""))
   }
}
        

// @LINE:537
case api_Collections_getCollection343(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getCollection(coll_id), HandlerDef(this, "api.Collections", "getCollection", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>"""))
   }
}
        

// @LINE:538
case api_Collections_updateCollectionName344(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).updateCollectionName(coll_id), HandlerDef(this, "api.Collections", "updateCollectionName", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/title"""))
   }
}
        

// @LINE:539
case api_Collections_updateCollectionDescription345(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).updateCollectionDescription(coll_id), HandlerDef(this, "api.Collections", "updateCollectionDescription", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/description"""))
   }
}
        

// @LINE:541
case api_Collections_removeSubCollection346(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("sub_coll_id", None), params.fromQuery[String]("ignoreNotFound", Some("True"))) { (coll_id, sub_coll_id, ignoreNotFound) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeSubCollection(coll_id, sub_coll_id, ignoreNotFound), HandlerDef(this, "api.Collections", "removeSubCollection", Seq(classOf[UUID], classOf[UUID], classOf[String]),"POST", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/removeSubCollection/$sub_coll_id<[^/]+>"""))
   }
}
        

// @LINE:542
case api_Collections_setRootSpace347(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("spaceId", None)) { (coll_id, spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).setRootSpace(coll_id, spaceId), HandlerDef(this, "api.Collections", "setRootSpace", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/rootFlag/$spaceId<[^/]+>"""))
   }
}
        

// @LINE:543
case api_Collections_unsetRootSpace348(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("spaceId", None)) { (coll_id, spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).unsetRootSpace(coll_id, spaceId), HandlerDef(this, "api.Collections", "unsetRootSpace", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/unsetRootFlag/$spaceId<[^/]+>"""))
   }
}
        

// @LINE:544
case api_Collections_createCollectionWithParent349(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).createCollectionWithParent, HandlerDef(this, "api.Collections", "createCollectionWithParent", Nil,"POST", """""", Routes.prefix + """api/collections/newCollectionWithParent"""))
   }
}
        

// @LINE:545
case api_Collections_getChildCollectionIds350(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getChildCollectionIds(coll_id), HandlerDef(this, "api.Collections", "getChildCollectionIds", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/getChildCollectionIds"""))
   }
}
        

// @LINE:546
case api_Collections_getChildCollections351(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getChildCollections(coll_id), HandlerDef(this, "api.Collections", "getChildCollections", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/getChildCollections"""))
   }
}
        

// @LINE:547
case api_Collections_getParentCollectionIds352(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getParentCollectionIds(coll_id), HandlerDef(this, "api.Collections", "getParentCollectionIds", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/getParentCollectionIds"""))
   }
}
        

// @LINE:548
case api_Collections_getParentCollections353(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).getParentCollections(coll_id), HandlerDef(this, "api.Collections", "getParentCollections", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/getParentCollections"""))
   }
}
        

// @LINE:549
case api_Collections_attachSubCollection354(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("sub_coll_id", None)) { (coll_id, sub_coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).attachSubCollection(coll_id, sub_coll_id), HandlerDef(this, "api.Collections", "attachSubCollection", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/addSubCollection/$sub_coll_id<[^/]+>"""))
   }
}
        

// @LINE:550
case api_Collections_removeFromSpaceAllowed355(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("space_id", None)) { (coll_id, space_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).removeFromSpaceAllowed(coll_id, space_id), HandlerDef(this, "api.Collections", "removeFromSpaceAllowed", Seq(classOf[UUID], classOf[UUID]),"GET", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/removeFromSpaceAllowed/$space_id<[^/]+>"""))
   }
}
        

// @LINE:551
case api_Collections_changeOwner356(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("userId", None)) { (coll_id, userId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).changeOwner(coll_id, userId), HandlerDef(this, "api.Collections", "changeOwner", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/changeOwner/$userId<[^/]+>"""))
   }
}
        

// @LINE:552
case api_Collections_changeOwnerCollectionOnly357(params) => {
   call(params.fromPath[UUID]("coll_id", None), params.fromPath[UUID]("userId", None)) { (coll_id, userId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).changeOwnerCollectionOnly(coll_id, userId), HandlerDef(this, "api.Collections", "changeOwnerCollectionOnly", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$coll_id<[^/]+>/changeOwnerCollection/$userId<[^/]+>"""))
   }
}
        

// @LINE:553
case api_Collections_changeOwnerDatasetOnly358(params) => {
   call(params.fromPath[UUID]("ds_id", None), params.fromPath[UUID]("userId", None)) { (ds_id, userId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).changeOwnerDatasetOnly(ds_id, userId), HandlerDef(this, "api.Collections", "changeOwnerDatasetOnly", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$ds_id<[^/]+>/changeOwnerDataset/$userId<[^/]+>"""))
   }
}
        

// @LINE:554
case api_Collections_changeOwnerFileOnly359(params) => {
   call(params.fromPath[UUID]("file_id", None), params.fromPath[UUID]("userId", None)) { (file_id, userId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Collections]).changeOwnerFileOnly(file_id, userId), HandlerDef(this, "api.Collections", "changeOwnerFileOnly", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/collections/$file_id<[^/]+>/changeOwnerFile/$userId<[^/]+>"""))
   }
}
        

// @LINE:560
case api_Datasets_list360(params) => {
   call(params.fromQuery[Option[String]]("title", Some(None)), params.fromQuery[Option[String]]("date", Some(None)), params.fromQuery[Int]("limit", Some(12)), params.fromQuery[Boolean]("exact", Some(false))) { (title, date, limit, exact) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).list(title, date, limit, exact), HandlerDef(this, "api.Datasets", "list", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]),"GET", """ ----------------------------------------------------------------------
 DATASETS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/datasets"""))
   }
}
        

// @LINE:561
case api_Datasets_listCanEdit361(params) => {
   call(params.fromQuery[Option[String]]("title", Some(None)), params.fromQuery[Option[String]]("date", Some(None)), params.fromQuery[Int]("limit", Some(12)), params.fromQuery[Boolean]("exact", Some(false))) { (title, date, limit, exact) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listCanEdit(title, date, limit, exact), HandlerDef(this, "api.Datasets", "listCanEdit", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Int], classOf[Boolean]),"GET", """""", Routes.prefix + """api/datasets/canEdit"""))
   }
}
        

// @LINE:562
case api_Datasets_listMoveFileToDataset362(params) => {
   call(params.fromQuery[UUID]("file_id", None), params.fromQuery[Option[String]]("title", Some(None)), params.fromQuery[Int]("limit", Some(12)), params.fromQuery[Boolean]("exact", Some(false))) { (file_id, title, limit, exact) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listMoveFileToDataset(file_id, title, limit, exact), HandlerDef(this, "api.Datasets", "listMoveFileToDataset", Seq(classOf[UUID], classOf[Option[String]], classOf[Int], classOf[Boolean]),"GET", """""", Routes.prefix + """api/datasets/moveFileToDataset"""))
   }
}
        

// @LINE:563
case api_Datasets_createDataset363(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).createDataset, HandlerDef(this, "api.Datasets", "createDataset", Nil,"POST", """""", Routes.prefix + """api/datasets"""))
   }
}
        

// @LINE:564
case api_Datasets_createEmptyVocabularyForDataset364(params) => {
   call(params.fromPath[UUID]("datasetId", None)) { (datasetId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).createEmptyVocabularyForDataset(datasetId), HandlerDef(this, "api.Datasets", "createEmptyVocabularyForDataset", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/newVocabulary/$datasetId<[^/]+>"""))
   }
}
        

// @LINE:565
case api_Datasets_markDatasetAsTrash365(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).markDatasetAsTrash(id), HandlerDef(this, "api.Datasets", "markDatasetAsTrash", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/datasets/trash/$id<[^/]+>"""))
   }
}
        

// @LINE:566
case api_Datasets_restoreDataset366(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).restoreDataset(id), HandlerDef(this, "api.Datasets", "restoreDataset", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/datasets/restore/$id<[^/]+>"""))
   }
}
        

// @LINE:568
case api_Datasets_emptyTrash367(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).emptyTrash(), HandlerDef(this, "api.Datasets", "emptyTrash", Nil,"DELETE", """GET            /api/datasets/trash                                                      @api.Datasets.listDatasetsInTrash()""", Routes.prefix + """api/datasets/emptytrash"""))
   }
}
        

// @LINE:569
case api_Datasets_createEmptyDataset368(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).createEmptyDataset, HandlerDef(this, "api.Datasets", "createEmptyDataset", Nil,"POST", """""", Routes.prefix + """api/datasets/createempty"""))
   }
}
        

// @LINE:570
case api_Datasets_attachMultipleFiles369(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).attachMultipleFiles, HandlerDef(this, "api.Datasets", "attachMultipleFiles", Nil,"POST", """""", Routes.prefix + """api/datasets/attachmultiple"""))
   }
}
        

// @LINE:571
case api_Datasets_searchDatasetsUserMetadata370(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).searchDatasetsUserMetadata, HandlerDef(this, "api.Datasets", "searchDatasetsUserMetadata", Nil,"POST", """""", Routes.prefix + """api/datasets/searchusermetadata"""))
   }
}
        

// @LINE:572
case api_Datasets_searchDatasetsGeneralMetadata371(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).searchDatasetsGeneralMetadata, HandlerDef(this, "api.Datasets", "searchDatasetsGeneralMetadata", Nil,"POST", """""", Routes.prefix + """api/datasets/searchmetadata"""))
   }
}
        

// @LINE:573
case api_Datasets_listOutsideCollection372(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listOutsideCollection(coll_id), HandlerDef(this, "api.Datasets", "listOutsideCollection", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/listOutsideCollection/$coll_id<[^/]+>"""))
   }
}
        

// @LINE:574
case api_Datasets_listDatasetsInTrash373(params) => {
   call(params.fromQuery[Int]("limit", Some(12))) { (limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listDatasetsInTrash(limit), HandlerDef(this, "api.Datasets", "listDatasetsInTrash", Seq(classOf[Int]),"GET", """""", Routes.prefix + """api/datasets/listTrash"""))
   }
}
        

// @LINE:575
case api_Datasets_emptyTrash374(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).emptyTrash(), HandlerDef(this, "api.Datasets", "emptyTrash", Nil,"DELETE", """""", Routes.prefix + """api/datasets/emptyTrash"""))
   }
}
        

// @LINE:576
case api_Datasets_clearOldDatasetsTrash375(params) => {
   call(params.fromQuery[Int]("days", Some(30))) { (days) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).clearOldDatasetsTrash(days), HandlerDef(this, "api.Datasets", "clearOldDatasetsTrash", Seq(classOf[Int]),"DELETE", """""", Routes.prefix + """api/datasets/clearOldDatasetsTrash"""))
   }
}
        

// @LINE:577
case api_Datasets_restoreDataset376(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).restoreDataset(id), HandlerDef(this, "api.Datasets", "restoreDataset", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/datasets/restore/$id<[^/]+>"""))
   }
}
        

// @LINE:578
case api_Datasets_detachFile377(params) => {
   call(params.fromPath[UUID]("ds_id", None), params.fromPath[UUID]("file_id", None), params.fromPath[String]("ignoreNotFound", None)) { (ds_id, file_id, ignoreNotFound) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).detachFile(ds_id, file_id, ignoreNotFound), HandlerDef(this, "api.Datasets", "detachFile", Seq(classOf[UUID], classOf[UUID], classOf[String]),"POST", """""", Routes.prefix + """api/datasets/$ds_id<[^/]+>/filesRemove/$file_id<[^/]+>/$ignoreNotFound<[^/]+>"""))
   }
}
        

// @LINE:579
case api_Folders_moveFileBetweenFolders378(params) => {
   call(params.fromPath[UUID]("datasetId", None), params.fromPath[UUID]("toFolder", None), params.fromPath[UUID]("fileId", None)) { (datasetId, toFolder, fileId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).moveFileBetweenFolders(datasetId, toFolder, fileId), HandlerDef(this, "api.Folders", "moveFileBetweenFolders", Seq(classOf[UUID], classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$datasetId<[^/]+>/moveFile/$toFolder<[^/]+>/$fileId<[^/]+>"""))
   }
}
        

// @LINE:580
case api_Datasets_moveFileBetweenDatasets379(params) => {
   call(params.fromPath[UUID]("datasetId", None), params.fromPath[UUID]("toDataset", None), params.fromPath[UUID]("fileId", None)) { (datasetId, toDataset, fileId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).moveFileBetweenDatasets(datasetId, toDataset, fileId), HandlerDef(this, "api.Datasets", "moveFileBetweenDatasets", Seq(classOf[UUID], classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$datasetId<[^/]+>/moveBetweenDatasets/$toDataset<[^/]+>/$fileId<[^/]+>"""))
   }
}
        

// @LINE:581
case api_Folders_moveFileToDataset380(params) => {
   call(params.fromPath[UUID]("datasetId", None), params.fromPath[UUID]("fromFolder", None), params.fromPath[UUID]("fileId", None)) { (datasetId, fromFolder, fileId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).moveFileToDataset(datasetId, fromFolder, fileId), HandlerDef(this, "api.Folders", "moveFileToDataset", Seq(classOf[UUID], classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$datasetId<[^/]+>/moveToDataset/$fromFolder<[^/]+>/$fileId<[^/]+>"""))
   }
}
        

// @LINE:582
case api_Folders_deleteFolder381(params) => {
   call(params.fromPath[UUID]("parentDatasetId", None), params.fromPath[UUID]("folderId", None)) { (parentDatasetId, folderId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).deleteFolder(parentDatasetId, folderId), HandlerDef(this, "api.Folders", "deleteFolder", Seq(classOf[UUID], classOf[UUID]),"DELETE", """""", Routes.prefix + """api/datasets/$parentDatasetId<[^/]+>/deleteFolder/$folderId<[^/]+>"""))
   }
}
        

// @LINE:583
case api_Folders_updateFolderName382(params) => {
   call(params.fromPath[UUID]("parentDatasetId", None), params.fromPath[UUID]("folderId", None)) { (parentDatasetId, folderId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).updateFolderName(parentDatasetId, folderId), HandlerDef(this, "api.Folders", "updateFolderName", Seq(classOf[UUID], classOf[UUID]),"PUT", """""", Routes.prefix + """api/datasets/$parentDatasetId<[^/]+>/updateName/$folderId<[^/]+>"""))
   }
}
        

// @LINE:584
case api_Datasets_copyDatasetToSpace383(params) => {
   call(params.fromPath[UUID]("datasetId", None), params.fromPath[UUID]("spaceId", None)) { (datasetId, spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).copyDatasetToSpace(datasetId, spaceId), HandlerDef(this, "api.Datasets", "copyDatasetToSpace", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$datasetId<[^/]+>/copyDatasetToSpace/$spaceId<[^/]+>"""))
   }
}
        

// @LINE:585
case api_Datasets_copyDatasetToSpaceNotAuthor384(params) => {
   call(params.fromPath[UUID]("datasetId", None), params.fromPath[UUID]("spaceId", None)) { (datasetId, spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).copyDatasetToSpaceNotAuthor(datasetId, spaceId), HandlerDef(this, "api.Datasets", "copyDatasetToSpaceNotAuthor", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$datasetId<[^/]+>/copyDatasetToSpaceNotAuthor/$spaceId<[^/]+>"""))
   }
}
        

// @LINE:586
case api_Folders_getAllFoldersByDatasetId385(params) => {
   call(params.fromPath[UUID]("datasetId", None)) { (datasetId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).getAllFoldersByDatasetId(datasetId), HandlerDef(this, "api.Folders", "getAllFoldersByDatasetId", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/$datasetId<[^/]+>/folders"""))
   }
}
        

// @LINE:587
case api_Datasets_detachFile386(params) => {
   call(params.fromPath[UUID]("ds_id", None), params.fromPath[UUID]("file_id", None), params.fromQuery[String]("ignoreNotFound", Some("True"))) { (ds_id, file_id, ignoreNotFound) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).detachFile(ds_id, file_id, ignoreNotFound), HandlerDef(this, "api.Datasets", "detachFile", Seq(classOf[UUID], classOf[UUID], classOf[String]),"DELETE", """""", Routes.prefix + """api/datasets/$ds_id<[^/]+>/$file_id<[^/]+>"""))
   }
}
        

// @LINE:588
case api_Datasets_getRDFURLsForDataset387(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getRDFURLsForDataset(id), HandlerDef(this, "api.Datasets", "getRDFURLsForDataset", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/getRDFURLsForDataset/$id<[^/]+>"""))
   }
}
        

// @LINE:589
case api_Datasets_getRDFUserMetadata388(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("mappingNum", Some("1"))) { (id, mappingNum) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getRDFUserMetadata(id, mappingNum), HandlerDef(this, "api.Datasets", "getRDFUserMetadata", Seq(classOf[UUID], classOf[String]),"GET", """""", Routes.prefix + """api/datasets/rdfUserMetadata/$id<[^/]+>"""))
   }
}
        

// @LINE:590
case api_Datasets_getMetadataDefinitions389(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[String]]("space", Some(None))) { (id, space) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getMetadataDefinitions(id, space), HandlerDef(this, "api.Datasets", "getMetadataDefinitions", Seq(classOf[UUID], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/metadata"""))
   }
}
        

// @LINE:591
case api_Datasets_addMetadata390(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addMetadata(id), HandlerDef(this, "api.Datasets", "addMetadata", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/metadata"""))
   }
}
        

// @LINE:592
case api_Datasets_addUserMetadata391(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addUserMetadata(id), HandlerDef(this, "api.Datasets", "addUserMetadata", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/usermetadata"""))
   }
}
        

// @LINE:593
case api_Datasets_getTechnicalMetadataJSON392(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getTechnicalMetadataJSON(id), HandlerDef(this, "api.Datasets", "getTechnicalMetadataJSON", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/technicalmetadatajson"""))
   }
}
        

// @LINE:594
case api_Datasets_getXMLMetadataJSON393(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getXMLMetadataJSON(id), HandlerDef(this, "api.Datasets", "getXMLMetadataJSON", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/xmlmetadatajson"""))
   }
}
        

// @LINE:595
case api_Datasets_getUserMetadataJSON394(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getUserMetadataJSON(id), HandlerDef(this, "api.Datasets", "getUserMetadataJSON", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/usermetadatajson"""))
   }
}
        

// @LINE:596
case api_Datasets_datasetFilesList395(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).datasetFilesList(id), HandlerDef(this, "api.Datasets", "datasetFilesList", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/listFiles"""))
   }
}
        

// @LINE:597
case api_Datasets_datasetAllFilesList396(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[Int]]("max", Some(None))) { (id, max) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).datasetAllFilesList(id, max), HandlerDef(this, "api.Datasets", "datasetAllFilesList", Seq(classOf[UUID], classOf[Option[Int]]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/listAllFiles"""))
   }
}
        

// @LINE:598
case api_Datasets_datasetAllFilesList397(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Option[Int]]("max", Some(None))) { (id, max) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).datasetAllFilesList(id, max), HandlerDef(this, "api.Datasets", "datasetAllFilesList", Seq(classOf[UUID], classOf[Option[Int]]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/files"""))
   }
}
        

// @LINE:599
case api_Datasets_uploadToDatasetFile398(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).uploadToDatasetFile(id), HandlerDef(this, "api.Datasets", "uploadToDatasetFile", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/files"""))
   }
}
        

// @LINE:600
case api_Datasets_uploadToDatasetJSON399(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).uploadToDatasetJSON(id), HandlerDef(this, "api.Datasets", "uploadToDatasetJSON", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/urls"""))
   }
}
        

// @LINE:601
case api_Datasets_download400(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Int]("compression", Some(-1))) { (id, compression) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).download(id, compression), HandlerDef(this, "api.Datasets", "download", Seq(classOf[UUID], classOf[Int]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/download"""))
   }
}
        

// @LINE:602
case api_Datasets_comment401(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).comment(id), HandlerDef(this, "api.Datasets", "comment", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/comment"""))
   }
}
        

// @LINE:603
case api_Datasets_reindex402(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Boolean]("recursive", Some(true))) { (id, recursive) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).reindex(id, recursive), HandlerDef(this, "api.Datasets", "reindex", Seq(classOf[UUID], classOf[Boolean]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/reindex"""))
   }
}
        

// @LINE:604
case api_Datasets_follow403(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).follow(id), HandlerDef(this, "api.Datasets", "follow", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/follow"""))
   }
}
        

// @LINE:605
case api_Datasets_unfollow404(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).unfollow(id), HandlerDef(this, "api.Datasets", "unfollow", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/unfollow"""))
   }
}
        

// @LINE:606
case api_Datasets_removeTag405(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeTag(id), HandlerDef(this, "api.Datasets", "removeTag", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/removeTag"""))
   }
}
        

// @LINE:607
case api_Datasets_getTags406(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getTags(id), HandlerDef(this, "api.Datasets", "getTags", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/tags"""))
   }
}
        

// @LINE:608
case api_Datasets_addTags407(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addTags(id), HandlerDef(this, "api.Datasets", "addTags", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/tags"""))
   }
}
        

// @LINE:609
case api_Datasets_removeTags408(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeTags(id), HandlerDef(this, "api.Datasets", "removeTags", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/tags/remove"""))
   }
}
        

// @LINE:610
case api_Datasets_removeAllTags409(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeAllTags(id), HandlerDef(this, "api.Datasets", "removeAllTags", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/tags/remove_all"""))
   }
}
        

// @LINE:611
case api_Datasets_removeTags410(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeTags(id), HandlerDef(this, "api.Datasets", "removeTags", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/datasets/$id<[^/]+>/tags"""))
   }
}
        

// @LINE:612
case api_Datasets_updateName411(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).updateName(id), HandlerDef(this, "api.Datasets", "updateName", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/datasets/$id<[^/]+>/title"""))
   }
}
        

// @LINE:613
case api_Datasets_updateDescription412(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).updateDescription(id), HandlerDef(this, "api.Datasets", "updateDescription", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/datasets/$id<[^/]+>/description"""))
   }
}
        

// @LINE:614
case api_Datasets_addCreator413(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addCreator(id), HandlerDef(this, "api.Datasets", "addCreator", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/creator"""))
   }
}
        

// @LINE:615
case api_Datasets_removeCreator414(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("creator", None)) { (id, creator) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).removeCreator(id, creator), HandlerDef(this, "api.Datasets", "removeCreator", Seq(classOf[UUID], classOf[String]),"DELETE", """""", Routes.prefix + """api/datasets/$id<[^/]+>/creator/remove"""))
   }
}
        

// @LINE:616
case api_Datasets_moveCreator415(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("creator", None), params.fromQuery[Int]("newPos", None)) { (id, creator, newPos) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).moveCreator(id, creator, newPos), HandlerDef(this, "api.Datasets", "moveCreator", Seq(classOf[UUID], classOf[String], classOf[Int]),"PUT", """""", Routes.prefix + """api/datasets/$id<[^/]+>/creator/reorder"""))
   }
}
        

// @LINE:617
case api_Datasets_updateInformation416(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).updateInformation(id), HandlerDef(this, "api.Datasets", "updateInformation", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/editing"""))
   }
}
        

// @LINE:618
case api_Datasets_updateLicense417(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).updateLicense(id), HandlerDef(this, "api.Datasets", "updateLicense", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/license"""))
   }
}
        

// @LINE:619
case api_Datasets_isBeingProcessed418(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).isBeingProcessed(id), HandlerDef(this, "api.Datasets", "isBeingProcessed", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/isBeingProcessed"""))
   }
}
        

// @LINE:620
case api_Datasets_getPreviews419(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).getPreviews(id), HandlerDef(this, "api.Datasets", "getPreviews", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/getPreviews"""))
   }
}
        

// @LINE:621
case api_Datasets_attachExistingFile420(params) => {
   call(params.fromPath[UUID]("ds_id", None), params.fromPath[UUID]("file_id", None)) { (ds_id, file_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).attachExistingFile(ds_id, file_id), HandlerDef(this, "api.Datasets", "attachExistingFile", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$ds_id<[^/]+>/files/$file_id<[^/]+>"""))
   }
}
        

// @LINE:622
case api_Datasets_get421(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).get(id), HandlerDef(this, "api.Datasets", "get", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>"""))
   }
}
        

// @LINE:623
case api_Datasets_deleteDataset422(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).deleteDataset(id), HandlerDef(this, "api.Datasets", "deleteDataset", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/datasets/$id<[^/]+>"""))
   }
}
        

// @LINE:624
case api_Datasets_detachAndDeleteDataset423(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).detachAndDeleteDataset(id), HandlerDef(this, "api.Datasets", "detachAndDeleteDataset", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/detachdelete"""))
   }
}
        

// @LINE:625
case api_Folders_createFolder424(params) => {
   call(params.fromPath[UUID]("parentDatasetId", None)) { (parentDatasetId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Folders]).createFolder(parentDatasetId), HandlerDef(this, "api.Folders", "createFolder", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$parentDatasetId<[^/]+>/newFolder"""))
   }
}
        

// @LINE:626
case api_Datasets_updateAccess425(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("aceess", Some("PRIVATE"))) { (id, aceess) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).updateAccess(id, aceess), HandlerDef(this, "api.Datasets", "updateAccess", Seq(classOf[UUID], classOf[String]),"PUT", """""", Routes.prefix + """api/datasets/$id<[^/]+>/access"""))
   }
}
        

// @LINE:627
case api_Datasets_addFileEvent426(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Boolean]("inFolder", None), params.fromQuery[Int]("fileCount", None)) { (id, inFolder, fileCount) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).addFileEvent(id, inFolder, fileCount), HandlerDef(this, "api.Datasets", "addFileEvent", Seq(classOf[UUID], classOf[Boolean], classOf[Int]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/addFileEvent"""))
   }
}
        

// @LINE:628
case api_Datasets_users427(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).users(id), HandlerDef(this, "api.Datasets", "users", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/$id<[^/]+>/users"""))
   }
}
        

// @LINE:629
case api_Datasets_changeOwner428(params) => {
   call(params.fromPath[UUID]("datasetId", None), params.fromPath[UUID]("userId", None)) { (datasetId, userId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).changeOwner(datasetId, userId), HandlerDef(this, "api.Datasets", "changeOwner", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$datasetId<[^/]+>/changeOwner/$userId<[^/]+>"""))
   }
}
        

// @LINE:630
case api_Datasets_moveDataset429(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).moveDataset(id), HandlerDef(this, "api.Datasets", "moveDataset", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/datasets/$id<[^/]+>/moveCollection"""))
   }
}
        

// @LINE:631
case api_Datasets_hasAttachedVocabulary430(params) => {
   call(params.fromPath[UUID]("datasetId", None)) { (datasetId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).hasAttachedVocabulary(datasetId), HandlerDef(this, "api.Datasets", "hasAttachedVocabulary", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/datasets/hasVocabulary/$datasetId<[^/]+>"""))
   }
}
        

// @LINE:637
case controllers_Vocabularies_newVocabulary431(params) => {
   call(params.fromQuery[Option[String]]("space", None)) { (space) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Vocabularies]).newVocabulary(space), HandlerDef(this, "controllers.Vocabularies", "newVocabulary", Seq(classOf[Option[String]]),"GET", """""", Routes.prefix + """vocabularies/new"""))
   }
}
        

// @LINE:638
case controllers_Vocabularies_submit432(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Vocabularies]).submit(), HandlerDef(this, "controllers.Vocabularies", "submit", Nil,"POST", """""", Routes.prefix + """vocabularies/submit"""))
   }
}
        

// @LINE:639
case controllers_Vocabularies_vocabulary433(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Vocabularies]).vocabulary(id), HandlerDef(this, "controllers.Vocabularies", "vocabulary", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """vocabularies/$id<[^/]+>"""))
   }
}
        

// @LINE:644
case api_Previews_downloadPreview434(params) => {
   call(params.fromPath[UUID]("preview_id", None), params.fromPath[UUID]("datasetid", None)) { (preview_id, datasetid) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).downloadPreview(preview_id, datasetid), HandlerDef(this, "api.Previews", "downloadPreview", Seq(classOf[UUID], classOf[UUID]),"GET", """ ----------------------------------------------------------------------
 PREVIEWS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/previews/$preview_id<[^/]+>/textures/dataset/$datasetid<[^/]+>/json"""))
   }
}
        

// @LINE:645
case api_Files_downloadByDatasetAndFilename435(params) => {
   call(params.fromPath[UUID]("dataset_id", None), params.fromPath[String]("filename", None), params.fromPath[UUID]("preview_id", None)) { (dataset_id, filename, preview_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).downloadByDatasetAndFilename(dataset_id, filename, preview_id), HandlerDef(this, "api.Files", "downloadByDatasetAndFilename", Seq(classOf[UUID], classOf[String], classOf[UUID]),"GET", """""", Routes.prefix + """api/previews/$preview_id<[^/]+>/textures/dataset/$dataset_id<[^/]+>//$filename<[^/]+>"""))
   }
}
        

// @LINE:646
case api_Previews_getTile436(params) => {
   call(params.fromPath[String]("dzi_id_dir", None), params.fromPath[String]("level", None), params.fromPath[String]("filename", None)) { (dzi_id_dir, level, filename) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).getTile(dzi_id_dir, level, filename), HandlerDef(this, "api.Previews", "getTile", Seq(classOf[String], classOf[String], classOf[String]),"GET", """""", Routes.prefix + """api/previews/$dzi_id_dir<[^/]+>/$level<[^/]+>/$filename<[^/]+>"""))
   }
}
        

// @LINE:647
case api_Previews_attachTile437(params) => {
   call(params.fromPath[UUID]("dzi_id", None), params.fromPath[UUID]("tile_id", None), params.fromPath[String]("level", None)) { (dzi_id, tile_id, level) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).attachTile(dzi_id, tile_id, level), HandlerDef(this, "api.Previews", "attachTile", Seq(classOf[UUID], classOf[UUID], classOf[String]),"POST", """""", Routes.prefix + """api/previews/$dzi_id<[^/]+>/tiles/$tile_id<[^/]+>/$level<[^/]+>"""))
   }
}
        

// @LINE:648
case api_Previews_uploadMetadata438(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).uploadMetadata(id), HandlerDef(this, "api.Previews", "uploadMetadata", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/previews/$id<[^/]+>/metadata"""))
   }
}
        

// @LINE:649
case api_Previews_getMetadata439(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).getMetadata(id), HandlerDef(this, "api.Previews", "getMetadata", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/previews/$id<[^/]+>/metadata"""))
   }
}
        

// @LINE:650
case api_Previews_attachAnnotation440(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).attachAnnotation(id), HandlerDef(this, "api.Previews", "attachAnnotation", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/previews/$id<[^/]+>/annotationAdd"""))
   }
}
        

// @LINE:651
case api_Previews_editAnnotation441(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).editAnnotation(id), HandlerDef(this, "api.Previews", "editAnnotation", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/previews/$id<[^/]+>/annotationEdit"""))
   }
}
        

// @LINE:652
case api_Previews_listAnnotations442(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).listAnnotations(id), HandlerDef(this, "api.Previews", "listAnnotations", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/previews/$id<[^/]+>/annotationsList"""))
   }
}
        

// @LINE:653
case api_Previews_setTitle443(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).setTitle(id), HandlerDef(this, "api.Previews", "setTitle", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/previews/$id<[^/]+>/title"""))
   }
}
        

// @LINE:654
case api_Previews_list444(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).list, HandlerDef(this, "api.Previews", "list", Nil,"GET", """""", Routes.prefix + """api/previews"""))
   }
}
        

// @LINE:655
case api_Previews_download445(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).download(id), HandlerDef(this, "api.Previews", "download", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/previews/$id<[^/]+>"""))
   }
}
        

// @LINE:656
case api_Previews_removePreview446(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).removePreview(id), HandlerDef(this, "api.Previews", "removePreview", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/previews/$id<[^/]+>"""))
   }
}
        

// @LINE:657
case api_Previews_upload447(params) => {
   call(params.fromQuery[String]("iipKey", Some(""))) { (iipKey) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Previews]).upload(iipKey), HandlerDef(this, "api.Previews", "upload", Seq(classOf[String]),"POST", """""", Routes.prefix + """api/previews"""))
   }
}
        

// @LINE:658
case api_Indexes_index448(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Indexes]).index, HandlerDef(this, "api.Indexes", "index", Nil,"POST", """""", Routes.prefix + """api/indexes"""))
   }
}
        

// @LINE:659
case api_Indexes_features449(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Indexes]).features, HandlerDef(this, "api.Indexes", "features", Nil,"POST", """""", Routes.prefix + """api/indexes/features"""))
   }
}
        

// @LINE:664
case api_Sections_add450(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).add, HandlerDef(this, "api.Sections", "add", Nil,"POST", """ ----------------------------------------------------------------------
 SECTIONS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/sections"""))
   }
}
        

// @LINE:665
case api_Sections_comment451(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).comment(id), HandlerDef(this, "api.Sections", "comment", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/sections/$id<[^/]+>/comments"""))
   }
}
        

// @LINE:666
case api_Sections_description452(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).description(id), HandlerDef(this, "api.Sections", "description", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/sections/$id<[^/]+>/description"""))
   }
}
        

// @LINE:667
case api_Sections_attachThumbnail453(params) => {
   call(params.fromPath[UUID]("id", None), params.fromPath[UUID]("thumbnail_id", None)) { (id, thumbnail_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).attachThumbnail(id, thumbnail_id), HandlerDef(this, "api.Sections", "attachThumbnail", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/sections/$id<[^/]+>/thumbnails/$thumbnail_id<[^/]+>"""))
   }
}
        

// @LINE:668
case api_Sections_getTags454(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).getTags(id), HandlerDef(this, "api.Sections", "getTags", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/sections/$id<[^/]+>/tags"""))
   }
}
        

// @LINE:669
case api_Sections_addTags455(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).addTags(id), HandlerDef(this, "api.Sections", "addTags", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/sections/$id<[^/]+>/tags"""))
   }
}
        

// @LINE:670
case api_Sections_removeTags456(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).removeTags(id), HandlerDef(this, "api.Sections", "removeTags", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/sections/$id<[^/]+>/tags/remove"""))
   }
}
        

// @LINE:671
case api_Sections_removeAllTags457(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).removeAllTags(id), HandlerDef(this, "api.Sections", "removeAllTags", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/sections/$id<[^/]+>/tags/remove_all"""))
   }
}
        

// @LINE:672
case api_Sections_removeTags458(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).removeTags(id), HandlerDef(this, "api.Sections", "removeTags", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/sections/$id<[^/]+>/tags"""))
   }
}
        

// @LINE:673
case api_Sections_get459(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).get(id), HandlerDef(this, "api.Sections", "get", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/sections/$id<[^/]+>"""))
   }
}
        

// @LINE:674
case api_Sections_delete460(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Sections]).delete(id), HandlerDef(this, "api.Sections", "delete", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/sections/$id<[^/]+>"""))
   }
}
        

// @LINE:679
case api_Search_searchJson461(params) => {
   call(params.fromQuery[String]("query", Some("")), params.fromQuery[String]("grouping", Some("AND")), params.fromQuery[Option[Int]]("from", None), params.fromQuery[Option[Int]]("size", None)) { (query, grouping, from, size) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Search]).searchJson(query, grouping, from, size), HandlerDef(this, "api.Search", "searchJson", Seq(classOf[String], classOf[String], classOf[Option[Int]], classOf[Option[Int]]),"GET", """ ----------------------------------------------------------------------
 SEARCH ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/search/json"""))
   }
}
        

// @LINE:680
case api_Search_searchMultimediaIndex462(params) => {
   call(params.fromQuery[UUID]("section_id", None)) { (section_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Search]).searchMultimediaIndex(section_id), HandlerDef(this, "api.Search", "searchMultimediaIndex", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/search/multimediasearch"""))
   }
}
        

// @LINE:681
case api_Search_search463(params) => {
   call(params.fromQuery[String]("query", Some(""))) { (query) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Search]).search(query), HandlerDef(this, "api.Search", "search", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/search"""))
   }
}
        

// @LINE:686
case api_Geostreams_addDatapoint464(params) => {
   call(params.fromQuery[Boolean]("invalidateCache", Some(true))) { (invalidateCache) =>
        invokeHandler(api.Geostreams.addDatapoint(invalidateCache), HandlerDef(this, "api.Geostreams", "addDatapoint", Seq(classOf[Boolean]),"POST", """ ----------------------------------------------------------------------
 GEOSTREAMS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/geostreams/datapoints"""))
   }
}
        

// @LINE:687
case api_Geostreams_addDatapoints465(params) => {
   call(params.fromQuery[Boolean]("invalidateCache", Some(true))) { (invalidateCache) =>
        invokeHandler(api.Geostreams.addDatapoints(invalidateCache), HandlerDef(this, "api.Geostreams", "addDatapoints", Seq(classOf[Boolean]),"POST", """""", Routes.prefix + """api/geostreams/datapoints/bulk"""))
   }
}
        

// @LINE:688
case api_Geostreams_deleteDatapoint466(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.deleteDatapoint(id), HandlerDef(this, "api.Geostreams", "deleteDatapoint", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/geostreams/datapoints/$id<[^/]+>"""))
   }
}
        

// @LINE:689
case api_Geostreams_searchDatapoints467(params) => {
   call(Param[String]("operator", Right("")), params.fromQuery[Option[String]]("since", Some(None)), params.fromQuery[Option[String]]("until", Some(None)), params.fromQuery[Option[String]]("geocode", Some(None)), params.fromQuery[Option[String]]("stream_id", Some(None)), params.fromQuery[Option[String]]("sensor_id", Some(None)), params.fromQuery[List[String]]("sources", Some(List.empty)), params.fromQuery[List[String]]("attributes", Some(List.empty)), params.fromQuery[String]("format", Some("json")), params.fromQuery[Option[String]]("semi", None), params.fromQuery[Boolean]("onlyCount", Some(false)), Param[Option[String]]("window_start", Right(None)), Param[Option[String]]("window_end", Right(None)), params.fromQuery[String]("binning", Some("semi")), params.fromQuery[Option[String]]("geojson", Some(None))) { (operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson) =>
        invokeHandler(api.Geostreams.searchDatapoints(operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson), HandlerDef(this, "api.Geostreams", "searchDatapoints", Seq(classOf[String], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[List[String]], classOf[List[String]], classOf[String], classOf[Option[String]], classOf[Boolean], classOf[Option[String]], classOf[Option[String]], classOf[String], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/geostreams/datapoints"""))
   }
}
        

// @LINE:690
case api_Geostreams_searchDatapoints468(params) => {
   call(Param[String]("operator", Right("averages")), params.fromQuery[Option[String]]("since", Some(None)), params.fromQuery[Option[String]]("until", Some(None)), params.fromQuery[Option[String]]("geocode", Some(None)), params.fromQuery[Option[String]]("stream_id", Some(None)), params.fromQuery[Option[String]]("sensor_id", Some(None)), params.fromQuery[List[String]]("sources", Some(List.empty)), params.fromQuery[List[String]]("attributes", Some(List.empty)), params.fromQuery[String]("format", Some("json")), params.fromQuery[Option[String]]("semi", None), params.fromQuery[Boolean]("onlyCount", Some(false)), Param[Option[String]]("window_start", Right(None)), Param[Option[String]]("window_end", Right(None)), params.fromQuery[String]("binning", Some("semi")), params.fromQuery[Option[String]]("geojson", Some(None))) { (operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson) =>
        invokeHandler(api.Geostreams.searchDatapoints(operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson), HandlerDef(this, "api.Geostreams", "searchDatapoints", Seq(classOf[String], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[List[String]], classOf[List[String]], classOf[String], classOf[Option[String]], classOf[Boolean], classOf[Option[String]], classOf[Option[String]], classOf[String], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/geostreams/datapoints/averages"""))
   }
}
        

// @LINE:691
case api_Geostreams_searchDatapoints469(params) => {
   call(Param[String]("operator", Right("trends")), params.fromQuery[Option[String]]("since", Some(None)), params.fromQuery[Option[String]]("until", Some(None)), params.fromQuery[Option[String]]("geocode", Some(None)), params.fromQuery[Option[String]]("stream_id", Some(None)), params.fromQuery[Option[String]]("sensor_id", Some(None)), params.fromQuery[List[String]]("sources", Some(List.empty)), params.fromQuery[List[String]]("attributes", Some(List.empty)), params.fromQuery[String]("format", Some("json")), params.fromQuery[Option[String]]("semi", None), params.fromQuery[Boolean]("onlyCount", Some(false)), params.fromQuery[Option[String]]("window_start", Some(None)), params.fromQuery[Option[String]]("window_end", Some(None)), params.fromQuery[String]("binning", Some("semi")), params.fromQuery[Option[String]]("geojson", Some(None))) { (operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson) =>
        invokeHandler(api.Geostreams.searchDatapoints(operator, since, until, geocode, stream_id, sensor_id, sources, attributes, format, semi, onlyCount, window_start, window_end, binning, geojson), HandlerDef(this, "api.Geostreams", "searchDatapoints", Seq(classOf[String], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[List[String]], classOf[List[String]], classOf[String], classOf[Option[String]], classOf[Boolean], classOf[Option[String]], classOf[Option[String]], classOf[String], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/geostreams/datapoints/trends"""))
   }
}
        

// @LINE:692
case api_Geostreams_binDatapoints470(params) => {
   call(params.fromPath[String]("time", None), params.fromPath[Double]("depth", None), params.fromQuery[Boolean]("raw", Some(false)), params.fromQuery[Option[String]]("since", Some(None)), params.fromQuery[Option[String]]("until", Some(None)), params.fromQuery[Option[String]]("geocode", Some(None)), params.fromQuery[Option[String]]("stream_id", Some(None)), params.fromQuery[Option[String]]("sensor_id", Some(None)), params.fromQuery[List[String]]("sources", Some(List.empty)), params.fromQuery[List[String]]("attributes", Some(List.empty))) { (time, depth, raw, since, until, geocode, stream_id, sensor_id, sources, attributes) =>
        invokeHandler(api.Geostreams.binDatapoints(time, depth, raw, since, until, geocode, stream_id, sensor_id, sources, attributes), HandlerDef(this, "api.Geostreams", "binDatapoints", Seq(classOf[String], classOf[Double], classOf[Boolean], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[Option[String]], classOf[List[String]], classOf[List[String]]),"GET", """""", Routes.prefix + """api/geostreams/datapoints/bin/$time<[^/]+>/$depth<[^/]+>"""))
   }
}
        

// @LINE:693
case api_Geostreams_getDatapoint471(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.getDatapoint(id), HandlerDef(this, "api.Geostreams", "getDatapoint", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/geostreams/datapoints/$id<[^/]+>"""))
   }
}
        

// @LINE:694
case api_Geostreams_cacheListAction472(params) => {
   call { 
        invokeHandler(api.Geostreams.cacheListAction, HandlerDef(this, "api.Geostreams", "cacheListAction", Nil,"GET", """""", Routes.prefix + """api/geostreams/cache"""))
   }
}
        

// @LINE:695
case api_Geostreams_cacheInvalidateAction473(params) => {
   call(params.fromQuery[Option[String]]("sensor_id", Some(None)), params.fromQuery[Option[String]]("stream_id", Some(None))) { (sensor_id, stream_id) =>
        invokeHandler(api.Geostreams.cacheInvalidateAction(sensor_id, stream_id), HandlerDef(this, "api.Geostreams", "cacheInvalidateAction", Seq(classOf[Option[String]], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/geostreams/cache/invalidate"""))
   }
}
        

// @LINE:696
case api_Geostreams_cacheFetchAction474(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.cacheFetchAction(id), HandlerDef(this, "api.Geostreams", "cacheFetchAction", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/geostreams/cache/$id<[^/]+>"""))
   }
}
        

// @LINE:697
case api_Geostreams_createSensor475(params) => {
   call { 
        invokeHandler(api.Geostreams.createSensor, HandlerDef(this, "api.Geostreams", "createSensor", Nil,"POST", """""", Routes.prefix + """api/geostreams/sensors"""))
   }
}
        

// @LINE:698
case api_Geostreams_updateStatisticsStreamSensor476(params) => {
   call { 
        invokeHandler(api.Geostreams.updateStatisticsStreamSensor(), HandlerDef(this, "api.Geostreams", "updateStatisticsStreamSensor", Nil,"GET", """""", Routes.prefix + """api/geostreams/sensors/update"""))
   }
}
        

// @LINE:699
case api_Geostreams_getSensor477(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.getSensor(id), HandlerDef(this, "api.Geostreams", "getSensor", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/geostreams/sensors/$id<[^/]+>"""))
   }
}
        

// @LINE:700
case api_Geostreams_updateSensorMetadata478(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.updateSensorMetadata(id), HandlerDef(this, "api.Geostreams", "updateSensorMetadata", Seq(classOf[String]),"PUT", """""", Routes.prefix + """api/geostreams/sensors/$id<[^/]+>"""))
   }
}
        

// @LINE:701
case api_Geostreams_getSensorStatistics479(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.getSensorStatistics(id), HandlerDef(this, "api.Geostreams", "getSensorStatistics", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/geostreams/sensors/$id<[^/]+>/stats"""))
   }
}
        

// @LINE:702
case api_Geostreams_getSensorStreams480(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.getSensorStreams(id), HandlerDef(this, "api.Geostreams", "getSensorStreams", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/geostreams/sensors/$id<[^/]+>/streams"""))
   }
}
        

// @LINE:703
case api_Geostreams_updateStatisticsSensor481(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.updateStatisticsSensor(id), HandlerDef(this, "api.Geostreams", "updateStatisticsSensor", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/geostreams/sensors/$id<[^/]+>/update"""))
   }
}
        

// @LINE:704
case api_Geostreams_searchSensors482(params) => {
   call(params.fromQuery[Option[String]]("geocode", Some(None)), params.fromQuery[Option[String]]("sensor_name", Some(None)), params.fromQuery[Option[String]]("geojson", Some(None))) { (geocode, sensor_name, geojson) =>
        invokeHandler(api.Geostreams.searchSensors(geocode, sensor_name, geojson), HandlerDef(this, "api.Geostreams", "searchSensors", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/geostreams/sensors"""))
   }
}
        

// @LINE:705
case api_Geostreams_deleteSensor483(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.deleteSensor(id), HandlerDef(this, "api.Geostreams", "deleteSensor", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/geostreams/sensors/$id<[^/]+>"""))
   }
}
        

// @LINE:706
case api_Geostreams_createStream484(params) => {
   call { 
        invokeHandler(api.Geostreams.createStream, HandlerDef(this, "api.Geostreams", "createStream", Nil,"POST", """""", Routes.prefix + """api/geostreams/streams"""))
   }
}
        

// @LINE:707
case api_Geostreams_updateStatisticsStreamSensor485(params) => {
   call { 
        invokeHandler(api.Geostreams.updateStatisticsStreamSensor(), HandlerDef(this, "api.Geostreams", "updateStatisticsStreamSensor", Nil,"GET", """""", Routes.prefix + """api/geostreams/streams/update"""))
   }
}
        

// @LINE:708
case api_Geostreams_getStream486(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.getStream(id), HandlerDef(this, "api.Geostreams", "getStream", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/geostreams/streams/$id<[^/]+>"""))
   }
}
        

// @LINE:709
case api_Geostreams_patchStreamMetadata487(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.patchStreamMetadata(id), HandlerDef(this, "api.Geostreams", "patchStreamMetadata", Seq(classOf[String]),"PUT", """""", Routes.prefix + """api/geostreams/streams/$id<[^/]+>"""))
   }
}
        

// @LINE:710
case api_Geostreams_updateStatisticsStream488(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.updateStatisticsStream(id), HandlerDef(this, "api.Geostreams", "updateStatisticsStream", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/geostreams/streams/$id<[^/]+>/update"""))
   }
}
        

// @LINE:711
case api_Geostreams_searchStreams489(params) => {
   call(params.fromQuery[Option[String]]("geocode", Some(None)), params.fromQuery[Option[String]]("stream_name", Some(None)), params.fromQuery[Option[String]]("geojson", Some(None))) { (geocode, stream_name, geojson) =>
        invokeHandler(api.Geostreams.searchStreams(geocode, stream_name, geojson), HandlerDef(this, "api.Geostreams", "searchStreams", Seq(classOf[Option[String]], classOf[Option[String]], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/geostreams/streams"""))
   }
}
        

// @LINE:712
case api_Geostreams_deleteStream490(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Geostreams.deleteStream(id), HandlerDef(this, "api.Geostreams", "deleteStream", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/geostreams/streams/$id<[^/]+>"""))
   }
}
        

// @LINE:713
case api_Geostreams_deleteAll491(params) => {
   call { 
        invokeHandler(api.Geostreams.deleteAll, HandlerDef(this, "api.Geostreams", "deleteAll", Nil,"DELETE", """""", Routes.prefix + """api/geostreams/dropall"""))
   }
}
        

// @LINE:714
case api_Geostreams_counts492(params) => {
   call { 
        invokeHandler(api.Geostreams.counts, HandlerDef(this, "api.Geostreams", "counts", Nil,"GET", """""", Routes.prefix + """api/geostreams/counts"""))
   }
}
        

// @LINE:715
case api_Geostreams_getConfig493(params) => {
   call { 
        invokeHandler(api.Geostreams.getConfig, HandlerDef(this, "api.Geostreams", "getConfig", Nil,"GET", """""", Routes.prefix + """api/geostreams/config"""))
   }
}
        

// @LINE:720
case api_Thumbnails_uploadThumbnail494(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Thumbnails]).uploadThumbnail, HandlerDef(this, "api.Thumbnails", "uploadThumbnail", Nil,"POST", """ ----------------------------------------------------------------------
 THUMBNAILS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/fileThumbnail"""))
   }
}
        

// @LINE:721
case api_Thumbnails_list495(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Thumbnails]).list, HandlerDef(this, "api.Thumbnails", "list", Nil,"GET", """""", Routes.prefix + """api/thumbnails"""))
   }
}
        

// @LINE:722
case api_Thumbnails_removeThumbnail496(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Thumbnails]).removeThumbnail(id), HandlerDef(this, "api.Thumbnails", "removeThumbnail", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/thumbnails/$id<[^/]+>"""))
   }
}
        

// @LINE:727
case api_Sensors_list497(params) => {
   call { 
        invokeHandler(api.Sensors.list, HandlerDef(this, "api.Sensors", "list", Nil,"GET", """ ----------------------------------------------------------------------
 SENSORS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/sensors"""))
   }
}
        

// @LINE:728
case api_Sensors_add498(params) => {
   call { 
        invokeHandler(api.Sensors.add, HandlerDef(this, "api.Sensors", "add", Nil,"POST", """""", Routes.prefix + """api/sensors"""))
   }
}
        

// @LINE:729
case api_Sensors_get499(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Sensors.get(id), HandlerDef(this, "api.Sensors", "get", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/sensors/$id<[^/]+>"""))
   }
}
        

// @LINE:730
case api_Sensors_search500(params) => {
   call { 
        invokeHandler(api.Sensors.search, HandlerDef(this, "api.Sensors", "search", Nil,"GET", """""", Routes.prefix + """api/sensors/search"""))
   }
}
        

// @LINE:731
case api_Sensors_delete501(params) => {
   call(params.fromPath[String]("id", None)) { (id) =>
        invokeHandler(api.Sensors.delete(id), HandlerDef(this, "api.Sensors", "delete", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/sensors/$id<[^/]+>"""))
   }
}
        

// @LINE:736
case api_Comments_mentionInComment502(params) => {
   call(params.fromQuery[UUID]("userID", None), params.fromQuery[UUID]("resourceID", None), params.fromQuery[String]("resourceName", None), params.fromQuery[String]("resourceType", None), params.fromQuery[UUID]("commenterId", None)) { (userID, resourceID, resourceName, resourceType, commenterId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Comments]).mentionInComment(userID, resourceID, resourceName, resourceType, commenterId), HandlerDef(this, "api.Comments", "mentionInComment", Seq(classOf[UUID], classOf[UUID], classOf[String], classOf[String], classOf[UUID]),"POST", """ ----------------------------------------------------------------------
 COMMENTS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/comment/mention"""))
   }
}
        

// @LINE:737
case api_Comments_comment503(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Comments]).comment(id), HandlerDef(this, "api.Comments", "comment", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/comment/$id<[^/]+>"""))
   }
}
        

// @LINE:738
case api_Comments_removeComment504(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Comments]).removeComment(id), HandlerDef(this, "api.Comments", "removeComment", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/comment/$id<[^/]+>/removeComment"""))
   }
}
        

// @LINE:739
case api_Comments_editComment505(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Comments]).editComment(id), HandlerDef(this, "api.Comments", "editComment", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/comment/$id<[^/]+>/editComment"""))
   }
}
        

// @LINE:744
case controllers_Selected_get506(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Selected]).get, HandlerDef(this, "controllers.Selected", "get", Nil,"GET", """ ----------------------------------------------------------------------
 SELECTIONS API
 ----------------------------------------------------------------------""", Routes.prefix + """selected"""))
   }
}
        

// @LINE:745
case api_Selected_get507(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).get, HandlerDef(this, "api.Selected", "get", Nil,"GET", """""", Routes.prefix + """api/selected"""))
   }
}
        

// @LINE:746
case api_Selected_add508(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).add, HandlerDef(this, "api.Selected", "add", Nil,"POST", """""", Routes.prefix + """api/selected"""))
   }
}
        

// @LINE:747
case api_Selected_remove509(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).remove, HandlerDef(this, "api.Selected", "remove", Nil,"POST", """""", Routes.prefix + """api/selected/remove"""))
   }
}
        

// @LINE:748
case api_Selected_deleteAll510(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).deleteAll, HandlerDef(this, "api.Selected", "deleteAll", Nil,"DELETE", """""", Routes.prefix + """api/selected/files"""))
   }
}
        

// @LINE:749
case api_Selected_downloadAll511(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).downloadAll, HandlerDef(this, "api.Selected", "downloadAll", Nil,"GET", """""", Routes.prefix + """api/selected/files"""))
   }
}
        

// @LINE:750
case api_Selected_clearAll512(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).clearAll, HandlerDef(this, "api.Selected", "clearAll", Nil,"POST", """""", Routes.prefix + """api/selected/clear"""))
   }
}
        

// @LINE:751
case api_Selected_tagAll513(params) => {
   call(params.fromQuery[List[String]]("tags", None)) { (tags) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Selected]).tagAll(tags), HandlerDef(this, "api.Selected", "tagAll", Seq(classOf[List[String]]),"POST", """""", Routes.prefix + """api/selected/tag"""))
   }
}
        

// @LINE:756
case api_Relations_list514(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Relations]).list(), HandlerDef(this, "api.Relations", "list", Nil,"GET", """ ----------------------------------------------------------------------
 RELATIONS API
 ----------------------------------------------------------------------""", Routes.prefix + """api/relations"""))
   }
}
        

// @LINE:757
case api_Relations_findTargets515(params) => {
   call(params.fromQuery[String]("sourceId", None), params.fromQuery[String]("sourceType", None), params.fromQuery[String]("targetType", None)) { (sourceId, sourceType, targetType) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Relations]).findTargets(sourceId, sourceType, targetType), HandlerDef(this, "api.Relations", "findTargets", Seq(classOf[String], classOf[String], classOf[String]),"GET", """""", Routes.prefix + """api/relations/search"""))
   }
}
        

// @LINE:758
case api_Relations_get516(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Relations]).get(id), HandlerDef(this, "api.Relations", "get", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/relations/$id<[^/]+>"""))
   }
}
        

// @LINE:759
case api_Relations_add517(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Relations]).add(), HandlerDef(this, "api.Relations", "add", Nil,"POST", """""", Routes.prefix + """api/relations"""))
   }
}
        

// @LINE:760
case api_Relations_delete518(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Relations]).delete(id), HandlerDef(this, "api.Relations", "delete", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/relations/$id<[^/]+>"""))
   }
}
        

// @LINE:765
case api_ZoomIt_uploadTile519(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ZoomIt]).uploadTile, HandlerDef(this, "api.ZoomIt", "uploadTile", Nil,"POST", """ ----------------------------------------------------------------------
 MISC./OTHER ENDPOINTS
 ----------------------------------------------------------------------""", Routes.prefix + """api/tiles"""))
   }
}
        

// @LINE:766
case api_Geometry_uploadGeometry520(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Geometry]).uploadGeometry, HandlerDef(this, "api.Geometry", "uploadGeometry", Nil,"POST", """""", Routes.prefix + """api/geometries"""))
   }
}
        

// @LINE:767
case api_ThreeDTexture_uploadTexture521(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.ThreeDTexture]).uploadTexture, HandlerDef(this, "api.ThreeDTexture", "uploadTexture", Nil,"POST", """""", Routes.prefix + """api/3dTextures"""))
   }
}
        

// @LINE:768
case api_Search_search522(params) => {
   call(params.fromQuery[String]("query", Some(""))) { (query) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Search]).search(query), HandlerDef(this, "api.Search", "search", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/search"""))
   }
}
        

// @LINE:769
case api_Search_querySPARQL523(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Search]).querySPARQL, HandlerDef(this, "api.Search", "querySPARQL", Nil,"POST", """""", Routes.prefix + """api/sparqlquery"""))
   }
}
        

// @LINE:770
case api_Projects_addproject524(params) => {
   call(params.fromQuery[String]("project", None)) { (project) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Projects]).addproject(project), HandlerDef(this, "api.Projects", "addproject", Seq(classOf[String]),"POST", """""", Routes.prefix + """api/projects/addproject"""))
   }
}
        

// @LINE:771
case api_Institutions_addinstitution525(params) => {
   call(params.fromQuery[String]("institution", None)) { (institution) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Institutions]).addinstitution(institution), HandlerDef(this, "api.Institutions", "addinstitution", Seq(classOf[String]),"POST", """""", Routes.prefix + """api/institutions/addinstitution"""))
   }
}
        

// @LINE:776
case api_Users_list526(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).list, HandlerDef(this, "api.Users", "list", Nil,"GET", """ ----------------------------------------------------------------------
 USERS API
 ----------------------------------------------------------------------""", Routes.prefix + """api/users"""))
   }
}
        

// @LINE:777
case api_Users_findByEmail527(params) => {
   call(params.fromPath[String]("email", None)) { (email) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).findByEmail(email), HandlerDef(this, "api.Users", "findByEmail", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/users/email/$email<[^/]+>"""))
   }
}
        

// @LINE:778
case api_Users_deleteUser528(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).deleteUser(id), HandlerDef(this, "api.Users", "deleteUser", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """api/users/$id<[^/]+>"""))
   }
}
        

// @LINE:779
case api_Users_getUser529(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).getUser(), HandlerDef(this, "api.Users", "getUser", Nil,"GET", """""", Routes.prefix + """api/me"""))
   }
}
        

// @LINE:780
case api_Users_updateName530(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("firstName", None), params.fromQuery[String]("lastName", None)) { (id, firstName, lastName) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).updateName(id, firstName, lastName), HandlerDef(this, "api.Users", "updateName", Seq(classOf[UUID], classOf[String], classOf[String]),"POST", """""", Routes.prefix + """api/users/$id<[^/]+>/updateName"""))
   }
}
        

// @LINE:781
case api_Users_keysList531(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).keysList(), HandlerDef(this, "api.Users", "keysList", Nil,"GET", """""", Routes.prefix + """api/users/keys"""))
   }
}
        

// @LINE:782
case api_Users_keysGet532(params) => {
   call(params.fromPath[String]("name", None)) { (name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).keysGet(name), HandlerDef(this, "api.Users", "keysGet", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/users/keys/$name<[^/]+>"""))
   }
}
        

// @LINE:783
case api_Users_keysAdd533(params) => {
   call(params.fromQuery[String]("name", None)) { (name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).keysAdd(name), HandlerDef(this, "api.Users", "keysAdd", Seq(classOf[String]),"POST", """""", Routes.prefix + """api/users/keys"""))
   }
}
        

// @LINE:784
case api_Users_keysDelete534(params) => {
   call(params.fromPath[String]("key", None)) { (key) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).keysDelete(key), HandlerDef(this, "api.Users", "keysDelete", Seq(classOf[String]),"DELETE", """""", Routes.prefix + """api/users/keys/$key<[^/]+>"""))
   }
}
        

// @LINE:785
case api_Users_addUserDatasetView535(params) => {
   call(params.fromQuery[String]("email", None), params.fromQuery[UUID]("dataset", None)) { (email, dataset) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).addUserDatasetView(email, dataset), HandlerDef(this, "api.Users", "addUserDatasetView", Seq(classOf[String], classOf[UUID]),"POST", """""", Routes.prefix + """api/users/addUserDatasetView"""))
   }
}
        

// @LINE:786
case api_Users_createNewListInUser536(params) => {
   call(params.fromQuery[String]("email", None), params.fromQuery[String]("field", None), params.fromQuery[List[String]]("fieldList", None)) { (email, field, fieldList) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).createNewListInUser(email, field, fieldList), HandlerDef(this, "api.Users", "createNewListInUser", Seq(classOf[String], classOf[String], classOf[List[String]]),"POST", """""", Routes.prefix + """api/users/createNewListInUser"""))
   }
}
        

// @LINE:787
case api_Users_createNewListInUser537(params) => {
   call(params.fromQuery[String]("email", None), params.fromQuery[String]("field", None), params.fromQuery[List[String]]("fieldList", None)) { (email, field, fieldList) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).createNewListInUser(email, field, fieldList), HandlerDef(this, "api.Users", "createNewListInUser", Seq(classOf[String], classOf[String], classOf[List[String]]),"POST", """""", Routes.prefix + """api/users/createNewListInUser"""))
   }
}
        

// @LINE:788
case api_Users_follow538(params) => {
   call(params.fromQuery[UUID]("followeeUUID", None)) { (followeeUUID) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).follow(followeeUUID), HandlerDef(this, "api.Users", "follow", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/users/follow"""))
   }
}
        

// @LINE:789
case api_Users_unfollow539(params) => {
   call(params.fromQuery[UUID]("followeeUUID", None)) { (followeeUUID) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).unfollow(followeeUUID), HandlerDef(this, "api.Users", "unfollow", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """api/users/unfollow"""))
   }
}
        

// @LINE:790
case api_CurationObjects_getCurationObjectOre540(params) => {
   call(params.fromPath[UUID]("curationId", None)) { (curationId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).getCurationObjectOre(curationId), HandlerDef(this, "api.CurationObjects", "getCurationObjectOre", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/curations/$curationId<[^/]+>/ore"""))
   }
}
        

// @LINE:791
case api_Users_findById541(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Users]).findById(id), HandlerDef(this, "api.Users", "findById", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/users/$id<[^/]+>"""))
   }
}
        

// @LINE:796
case controllers_Users_getFollowers542(params) => {
   call(params.fromQuery[Int]("index", Some(0)), params.fromQuery[Int]("limit", Some(12))) { (index, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Users]).getFollowers(index, limit), HandlerDef(this, "controllers.Users", "getFollowers", Seq(classOf[Int], classOf[Int]),"GET", """ ----------------------------------------------------------------------
 USERS Controller
 ----------------------------------------------------------------------""", Routes.prefix + """users/followers"""))
   }
}
        

// @LINE:797
case controllers_Users_getFollowing543(params) => {
   call(params.fromQuery[Int]("index", Some(0)), params.fromQuery[Int]("limit", Some(12))) { (index, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Users]).getFollowing(index, limit), HandlerDef(this, "controllers.Users", "getFollowing", Seq(classOf[Int], classOf[Int]),"GET", """""", Routes.prefix + """users/following"""))
   }
}
        

// @LINE:802
case api_Status_version544(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Status]).version, HandlerDef(this, "api.Status", "version", Nil,"GET", """ ----------------------------------------------------------------------
 Clowder STATUS API
 ----------------------------------------------------------------------""", Routes.prefix + """api/version"""))
   }
}
        

// @LINE:803
case api_Status_status545(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Status]).status, HandlerDef(this, "api.Status", "status", Nil,"GET", """""", Routes.prefix + """api/status"""))
   }
}
        

// @LINE:808
case controllers_RSS_siteRSS546(params) => {
   call(params.fromQuery[Option[Int]]("limit", None)) { (limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.RSS]).siteRSS(limit), HandlerDef(this, "controllers.RSS", "siteRSS", Seq(classOf[Option[Int]]),"GET", """ ----------------------------------------------------------------------
 RSS
 ----------------------------------------------------------------------""", Routes.prefix + """rss"""))
   }
}
        

// @LINE:809
case controllers_RSS_siteRSSOfType547(params) => {
   call(params.fromQuery[Option[Int]]("limit", None), params.fromPath[String]("etype", None)) { (limit, etype) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.RSS]).siteRSSOfType(limit, etype), HandlerDef(this, "controllers.RSS", "siteRSSOfType", Seq(classOf[Option[Int]], classOf[String]),"GET", """""", Routes.prefix + """rss/$etype<[^/]+>"""))
   }
}
        

// @LINE:815
case controllers_CurationObjects_newCO548(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("space", Some(""))) { (id, space) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).newCO(id, space), HandlerDef(this, "controllers.CurationObjects", "newCO", Seq(classOf[UUID], classOf[String]),"GET", """""", Routes.prefix + """dataset/$id<[^/]+>/curations/new"""))
   }
}
        

// @LINE:816
case controllers_CurationObjects_list549(params) => {
   call(params.fromQuery[String]("when", Some("")), params.fromQuery[String]("date", Some("")), params.fromQuery[Int]("limit", Some(4)), params.fromQuery[Option[String]]("space", Some(None))) { (when, date, limit, space) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).list(when, date, limit, space), HandlerDef(this, "controllers.CurationObjects", "list", Seq(classOf[String], classOf[String], classOf[Int], classOf[Option[String]]),"GET", """""", Routes.prefix + """space/curations"""))
   }
}
        

// @LINE:817
case controllers_CurationObjects_submit550(params) => {
   call(params.fromPath[UUID]("id", None), params.fromPath[UUID]("spaceId", None)) { (id, spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).submit(id, spaceId), HandlerDef(this, "controllers.CurationObjects", "submit", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """dataset/$id<[^/]+>/curations/spaces/$spaceId<[^/]+>/submit"""))
   }
}
        

// @LINE:818
case controllers_Spaces_stagingArea551(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Int]("index", Some(0)), params.fromQuery[Int]("limit", Some(12))) { (id, index, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Spaces]).stagingArea(id, index, limit), HandlerDef(this, "controllers.Spaces", "stagingArea", Seq(classOf[UUID], classOf[Int], classOf[Int]),"GET", """""", Routes.prefix + """spaces/$id<[^/]+>/stagingArea"""))
   }
}
        

// @LINE:819
case controllers_CurationObjects_getCurationObject552(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Int]("limit", Some(5))) { (id, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).getCurationObject(id, limit), HandlerDef(this, "controllers.CurationObjects", "getCurationObject", Seq(classOf[UUID], classOf[Int]),"GET", """""", Routes.prefix + """spaces/curations/$id<[^/]+>"""))
   }
}
        

// @LINE:820
case api_CurationObjects_retractCurationObject553(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).retractCurationObject(id), HandlerDef(this, "api.CurationObjects", "retractCurationObject", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """spaces/curations/retract/$id<[^/]+>"""))
   }
}
        

// @LINE:821
case controllers_CurationObjects_editCuration554(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).editCuration(id), HandlerDef(this, "controllers.CurationObjects", "editCuration", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/edit"""))
   }
}
        

// @LINE:822
case controllers_CurationObjects_updateCuration555(params) => {
   call(params.fromPath[UUID]("id", None), params.fromPath[UUID]("spaceId", None)) { (id, spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).updateCuration(id, spaceId), HandlerDef(this, "controllers.CurationObjects", "updateCuration", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/spaces/$spaceId<[^/]+>/update"""))
   }
}
        

// @LINE:823
case controllers_CurationObjects_deleteCuration556(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).deleteCuration(id), HandlerDef(this, "controllers.CurationObjects", "deleteCuration", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """spaces/curations/$id<[^/]+>"""))
   }
}
        

// @LINE:824
case controllers_CurationObjects_compareToRepository557(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("repository", None)) { (id, repository) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).compareToRepository(id, repository), HandlerDef(this, "controllers.CurationObjects", "compareToRepository", Seq(classOf[UUID], classOf[String]),"GET", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/compareToRepository"""))
   }
}
        

// @LINE:825
case controllers_CurationObjects_submitRepositorySelection558(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).submitRepositorySelection(id), HandlerDef(this, "controllers.CurationObjects", "submitRepositorySelection", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/submitRepositorySelection"""))
   }
}
        

// @LINE:826
case controllers_CurationObjects_sendToRepository559(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).sendToRepository(id), HandlerDef(this, "controllers.CurationObjects", "sendToRepository", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/sendToRepository"""))
   }
}
        

// @LINE:827
case controllers_CurationObjects_findMatchingRepositories560(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).findMatchingRepositories(id), HandlerDef(this, "controllers.CurationObjects", "findMatchingRepositories", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/matchmaker"""))
   }
}
        

// @LINE:828
case api_CurationObjects_findMatchmakingRepositories561(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).findMatchmakingRepositories(id), HandlerDef(this, "api.CurationObjects", "findMatchmakingRepositories", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/matchmaker"""))
   }
}
        

// @LINE:829
case api_CurationObjects_getCurationFiles562(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).getCurationFiles(id), HandlerDef(this, "api.CurationObjects", "getCurationFiles", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/curationFile"""))
   }
}
        

// @LINE:830
case api_CurationObjects_savePublishedObject563(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).savePublishedObject(id), HandlerDef(this, "api.CurationObjects", "savePublishedObject", Seq(classOf[UUID]),"POST", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/status"""))
   }
}
        

// @LINE:831
case controllers_CurationObjects_getStatusFromRepository564(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).getStatusFromRepository(id), HandlerDef(this, "controllers.CurationObjects", "getStatusFromRepository", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/getStatusFromRepository"""))
   }
}
        

// @LINE:832
case api_CurationObjects_deleteCurationFile565(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[UUID]("parentId", None), params.fromPath[UUID]("curationFileId", None)) { (id, parentId, curationFileId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).deleteCurationFile(id, parentId, curationFileId), HandlerDef(this, "api.CurationObjects", "deleteCurationFile", Seq(classOf[UUID], classOf[UUID], classOf[UUID]),"DELETE", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/files/$curationFileId<[^/]+>"""))
   }
}
        

// @LINE:833
case api_CurationObjects_deleteCurationFolder566(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[UUID]("parentId", None), params.fromPath[UUID]("curationFolderId", None)) { (id, parentId, curationFolderId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).deleteCurationFolder(id, parentId, curationFolderId), HandlerDef(this, "api.CurationObjects", "deleteCurationFolder", Seq(classOf[UUID], classOf[UUID], classOf[UUID]),"DELETE", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/folders/$curationFolderId<[^/]+>"""))
   }
}
        

// @LINE:834
case controllers_CurationObjects_getUpdatedFilesAndFolders567(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("curationFolderId", None), params.fromQuery[Int]("limit", None), params.fromQuery[Int]("pageIndex", None)) { (id, curationFolderId, limit, pageIndex) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).getUpdatedFilesAndFolders(id, curationFolderId, limit, pageIndex), HandlerDef(this, "controllers.CurationObjects", "getUpdatedFilesAndFolders", Seq(classOf[UUID], classOf[String], classOf[Int], classOf[Int]),"GET", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/updatedFilesAndFolders"""))
   }
}
        

// @LINE:835
case api_CurationObjects_getMetadataDefinitionsByFile568(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).getMetadataDefinitionsByFile(id), HandlerDef(this, "api.CurationObjects", "getMetadataDefinitionsByFile", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/getMetadataDefinitionsByFile"""))
   }
}
        

// @LINE:836
case api_CurationObjects_getMetadataDefinitions569(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.CurationObjects]).getMetadataDefinitions(id), HandlerDef(this, "api.CurationObjects", "getMetadataDefinitions", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """spaces/curations/$id<[^/]+>/metadataDefinitions"""))
   }
}
        

// @LINE:837
case controllers_CurationObjects_getPublishedData570(params) => {
   call(Param[String]("space", Right(""))) { (space) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).getPublishedData(space), HandlerDef(this, "controllers.CurationObjects", "getPublishedData", Seq(classOf[String]),"GET", """""", Routes.prefix + """publishedData"""))
   }
}
        

// @LINE:838
case controllers_CurationObjects_getPublishedData571(params) => {
   call(params.fromPath[String]("space", None)) { (space) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.CurationObjects]).getPublishedData(space), HandlerDef(this, "controllers.CurationObjects", "getPublishedData", Seq(classOf[String]),"GET", """""", Routes.prefix + """publishedData/$space<[^/]+>"""))
   }
}
        

// @LINE:840
case api_Events_sendExceptionEmail572(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Events]).sendExceptionEmail(), HandlerDef(this, "api.Events", "sendExceptionEmail", Nil,"POST", """""", Routes.prefix + """sendEmail"""))
   }
}
        

// @LINE:841
case controllers_Events_getEvents573(params) => {
   call(params.fromQuery[Int]("index", None)) { (index) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.Events]).getEvents(index), HandlerDef(this, "controllers.Events", "getEvents", Seq(classOf[Int]),"GET", """""", Routes.prefix + """getEvents"""))
   }
}
        

// @LINE:847
case api_Vocabularies_list574(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).list(), HandlerDef(this, "api.Vocabularies", "list", Nil,"GET", """""", Routes.prefix + """api/vocabularies/list"""))
   }
}
        

// @LINE:848
case api_Vocabularies_createVocabularyFromJson575(params) => {
   call(params.fromQuery[Boolean]("isPublic", Some(false))) { (isPublic) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).createVocabularyFromJson(isPublic), HandlerDef(this, "api.Vocabularies", "createVocabularyFromJson", Seq(classOf[Boolean]),"POST", """""", Routes.prefix + """api/vocabularies/createVocabularyFromJson"""))
   }
}
        

// @LINE:849
case api_Vocabularies_createVocabularyFromForm576(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).createVocabularyFromForm(), HandlerDef(this, "api.Vocabularies", "createVocabularyFromForm", Nil,"POST", """""", Routes.prefix + """api/vocabularies/createVocabulary"""))
   }
}
        

// @LINE:850
case api_Vocabularies_getByAuthor577(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getByAuthor(), HandlerDef(this, "api.Vocabularies", "getByAuthor", Nil,"GET", """""", Routes.prefix + """api/vocabularies/getByAuthor"""))
   }
}
        

// @LINE:851
case api_Vocabularies_getPublicVocabularies578(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getPublicVocabularies(), HandlerDef(this, "api.Vocabularies", "getPublicVocabularies", Nil,"GET", """""", Routes.prefix + """api/vocabularies/getPublic"""))
   }
}
        

// @LINE:854
case api_Vocabularies_get579(params) => {
   call(params.fromPath[UUID]("vocab_id", None)) { (vocab_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).get(vocab_id), HandlerDef(this, "api.Vocabularies", "get", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/vocabularies/$vocab_id<[^/]+>"""))
   }
}
        

// @LINE:855
case api_Vocabularies_editVocabulary580(params) => {
   call(params.fromPath[UUID]("vocab_id", None)) { (vocab_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).editVocabulary(vocab_id), HandlerDef(this, "api.Vocabularies", "editVocabulary", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/editVocabulary/$vocab_id<[^/]+>"""))
   }
}
        

// @LINE:856
case api_Vocabularies_getByName581(params) => {
   call(params.fromPath[String]("name", None)) { (name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getByName(name), HandlerDef(this, "api.Vocabularies", "getByName", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/vocabularies/$name<[^/]+>"""))
   }
}
        

// @LINE:857
case api_Vocabularies_editVocabulary582(params) => {
   call(params.fromPath[UUID]("vocab_id", None)) { (vocab_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).editVocabulary(vocab_id), HandlerDef(this, "api.Vocabularies", "editVocabulary", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """api/vocabularies/$vocab_id<[^/]+>/editVocabulary"""))
   }
}
        

// @LINE:858
case api_Vocabularies_getByNameAndAuthor583(params) => {
   call(params.fromPath[String]("name", None)) { (name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getByNameAndAuthor(name), HandlerDef(this, "api.Vocabularies", "getByNameAndAuthor", Seq(classOf[String]),"GET", """""", Routes.prefix + """api/vocabularies/$name<[^/]+>/getByNameAuthor"""))
   }
}
        

// @LINE:860
case api_Vocabularies_addToSpace584(params) => {
   call(params.fromPath[UUID]("vocab_id", None), params.fromPath[UUID]("space_id", None)) { (vocab_id, space_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).addToSpace(vocab_id, space_id), HandlerDef(this, "api.Vocabularies", "addToSpace", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/vocabularies/$vocab_id<[^/]+>/addToSpace/$space_id<[^/]+>"""))
   }
}
        

// @LINE:861
case api_Vocabularies_removeFromSpace585(params) => {
   call(params.fromPath[UUID]("vocab_id", None), params.fromPath[UUID]("space_id", None)) { (vocab_id, space_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).removeFromSpace(vocab_id, space_id), HandlerDef(this, "api.Vocabularies", "removeFromSpace", Seq(classOf[UUID], classOf[UUID]),"POST", """""", Routes.prefix + """api/vocabuliaries/$vocab_id<[^/]+>/removeFromSpace/$space_id<[^/]+>"""))
   }
}
        

// @LINE:867
case api_VocabularyTerms_list586(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.VocabularyTerms]).list(), HandlerDef(this, "api.VocabularyTerms", "list", Nil,"GET", """""", Routes.prefix + """api/vocabterms/list"""))
   }
}
        

// @LINE:868
case api_VocabularyTerms_get587(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.VocabularyTerms]).get(id), HandlerDef(this, "api.VocabularyTerms", "get", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/vocabterms/$id<[^/]+>"""))
   }
}
        

// @LINE:874
case api_Proxy_get588(params) => {
   call(params.fromPath[String]("endpoint_key", None), Param[String]("pathSuffix", Right(null))) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).get(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "get", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>"""))
   }
}
        

// @LINE:875
case api_Proxy_get589(params) => {
   call(params.fromPath[String]("endpoint_key", None), Param[String]("pathSuffix", Right(null))) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).get(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "get", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>/"""))
   }
}
        

// @LINE:876
case api_Proxy_get590(params) => {
   call(params.fromPath[String]("endpoint_key", None), params.fromPath[String]("pathSuffix", None)) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).get(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "get", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>/$pathSuffix<.+>"""))
   }
}
        

// @LINE:877
case api_Proxy_post591(params) => {
   call(params.fromPath[String]("endpoint_key", None), Param[String]("pathSuffix", Right(null))) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).post(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "post", Seq(classOf[String], classOf[String]),"POST", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>"""))
   }
}
        

// @LINE:878
case api_Proxy_post592(params) => {
   call(params.fromPath[String]("endpoint_key", None), Param[String]("pathSuffix", Right(null))) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).post(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "post", Seq(classOf[String], classOf[String]),"POST", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>/"""))
   }
}
        

// @LINE:879
case api_Proxy_post593(params) => {
   call(params.fromPath[String]("endpoint_key", None), params.fromPath[String]("pathSuffix", None)) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).post(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "post", Seq(classOf[String], classOf[String]),"POST", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>/$pathSuffix<.+>"""))
   }
}
        

// @LINE:880
case api_Proxy_put594(params) => {
   call(params.fromPath[String]("endpoint_key", None), Param[String]("pathSuffix", Right(null))) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).put(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "put", Seq(classOf[String], classOf[String]),"PUT", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>"""))
   }
}
        

// @LINE:881
case api_Proxy_put595(params) => {
   call(params.fromPath[String]("endpoint_key", None), Param[String]("pathSuffix", Right(null))) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).put(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "put", Seq(classOf[String], classOf[String]),"PUT", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>/"""))
   }
}
        

// @LINE:882
case api_Proxy_put596(params) => {
   call(params.fromPath[String]("endpoint_key", None), params.fromPath[String]("pathSuffix", None)) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).put(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "put", Seq(classOf[String], classOf[String]),"PUT", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>/$pathSuffix<.+>"""))
   }
}
        

// @LINE:883
case api_Proxy_delete597(params) => {
   call(params.fromPath[String]("endpoint_key", None), Param[String]("pathSuffix", Right(null))) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).delete(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "delete", Seq(classOf[String], classOf[String]),"DELETE", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>"""))
   }
}
        

// @LINE:884
case api_Proxy_delete598(params) => {
   call(params.fromPath[String]("endpoint_key", None), Param[String]("pathSuffix", Right(null))) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).delete(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "delete", Seq(classOf[String], classOf[String]),"DELETE", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>/"""))
   }
}
        

// @LINE:885
case api_Proxy_delete599(params) => {
   call(params.fromPath[String]("endpoint_key", None), params.fromPath[String]("pathSuffix", None)) { (endpoint_key, pathSuffix) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Proxy]).delete(endpoint_key, pathSuffix), HandlerDef(this, "api.Proxy", "delete", Seq(classOf[String], classOf[String]),"DELETE", """""", Routes.prefix + """api/proxy/$endpoint_key<[^/]+>/$pathSuffix<.+>"""))
   }
}
        

// @LINE:891
case api_Vocabularies_createVocabularyFromJson600(params) => {
   call(params.fromQuery[Boolean]("isPublic", Some(false))) { (isPublic) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).createVocabularyFromJson(isPublic), HandlerDef(this, "api.Vocabularies", "createVocabularyFromJson", Seq(classOf[Boolean]),"POST", """""", Routes.prefix + """t2c2/templates/createExperimentTemplate"""))
   }
}
        

// @LINE:892
case api_Vocabularies_createVocabularyFromJson601(params) => {
   call(params.fromQuery[Boolean]("isPublic", Some(false))) { (isPublic) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).createVocabularyFromJson(isPublic), HandlerDef(this, "api.Vocabularies", "createVocabularyFromJson", Seq(classOf[Boolean]),"POST", """""", Routes.prefix + """t2c2/templates/createExperimentTemplateFromJson"""))
   }
}
        

// @LINE:893
case api_Vocabularies_createVocabularyFromJson602(params) => {
   call(params.fromQuery[Boolean]("isPublic", Some(false))) { (isPublic) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).createVocabularyFromJson(isPublic), HandlerDef(this, "api.Vocabularies", "createVocabularyFromJson", Seq(classOf[Boolean]),"POST", """""", Routes.prefix + """t2c2/templates/createExperimentTemplateDefaultValues"""))
   }
}
        

// @LINE:895
case api_Vocabularies_list603(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).list(), HandlerDef(this, "api.Vocabularies", "list", Nil,"GET", """GET         /t2c2/templates/findByName                                       @api.Vocabularies.getByName(tag : String)""", Routes.prefix + """t2c2/templates/listExperimentTemplates"""))
   }
}
        

// @LINE:896
case api_Vocabularies_listAll604(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).listAll(), HandlerDef(this, "api.Vocabularies", "listAll", Nil,"GET", """""", Routes.prefix + """t2c2/templates/allExperimentTemplates"""))
   }
}
        

// @LINE:898
case api_Vocabularies_getPublicVocabularies605(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getPublicVocabularies(), HandlerDef(this, "api.Vocabularies", "getPublicVocabularies", Nil,"GET", """POST        /t2c2/templates/getExperimentTemplateByDescription                                         @api.Vocabularies.getByTag(containsAll : Boolean ?= false)""", Routes.prefix + """t2c2/templates/getPublic"""))
   }
}
        

// @LINE:899
case api_Vocabularies_getAllTagsOfAllVocabularies606(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getAllTagsOfAllVocabularies(), HandlerDef(this, "api.Vocabularies", "getAllTagsOfAllVocabularies", Nil,"GET", """""", Routes.prefix + """t2c2/templates/allTags"""))
   }
}
        

// @LINE:900
case api_T2C2_listMyDatasets607(params) => {
   call(params.fromQuery[Int]("limit", Some(20))) { (limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).listMyDatasets(limit), HandlerDef(this, "api.T2C2", "listMyDatasets", Seq(classOf[Int]),"GET", """""", Routes.prefix + """api/t2c2/listMyDatasets"""))
   }
}
        

// @LINE:901
case api_T2C2_getVocabulary608(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getVocabulary(id), HandlerDef(this, "api.T2C2", "getVocabulary", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """t2c2/templates/getExperimentTemplateById/$id<[^/]+>"""))
   }
}
        

// @LINE:902
case api_Vocabularies_getByName609(params) => {
   call(params.fromPath[String]("name", None)) { (name) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getByName(name), HandlerDef(this, "api.Vocabularies", "getByName", Seq(classOf[String]),"GET", """""", Routes.prefix + """t2c2/templates/getExperimentTemplateByName/$name<[^/]+>"""))
   }
}
        

// @LINE:903
case api_Vocabularies_getByTag610(params) => {
   call(params.fromQuery[Boolean]("containsAll", Some(false))) { (containsAll) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getByTag(containsAll), HandlerDef(this, "api.Vocabularies", "getByTag", Seq(classOf[Boolean]),"POST", """""", Routes.prefix + """t2c2/templates/findByTag"""))
   }
}
        

// @LINE:904
case api_Vocabularies_getBySingleTag611(params) => {
   call(params.fromPath[String]("tag", None)) { (tag) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).getBySingleTag(tag), HandlerDef(this, "api.Vocabularies", "getBySingleTag", Seq(classOf[String]),"GET", """""", Routes.prefix + """t2c2/templates/findByTag/$tag<[^/]+>"""))
   }
}
        

// @LINE:905
case api_T2C2_getVocabByDatasetId612(params) => {
   call(params.fromPath[UUID]("dataset_id", None)) { (dataset_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getVocabByDatasetId(dataset_id), HandlerDef(this, "api.T2C2", "getVocabByDatasetId", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """t2c2/templates/getByDatasetId/$dataset_id<[^/]+>"""))
   }
}
        

// @LINE:906
case api_T2C2_getVocabByFileId613(params) => {
   call(params.fromPath[UUID]("file_id", None)) { (file_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getVocabByFileId(file_id), HandlerDef(this, "api.T2C2", "getVocabByFileId", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """t2c2/templates/getByFileId/$file_id<[^/]+>"""))
   }
}
        

// @LINE:908
case api_T2C2_listMyDatasets614(params) => {
   call(params.fromQuery[Int]("limit", Some(20))) { (limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).listMyDatasets(limit), HandlerDef(this, "api.T2C2", "listMyDatasets", Seq(classOf[Int]),"GET", """""", Routes.prefix + """api/t2c2/listMyDatasets"""))
   }
}
        

// @LINE:909
case api_T2C2_listMySpaces615(params) => {
   call(params.fromQuery[Int]("limit", Some(20))) { (limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).listMySpaces(limit), HandlerDef(this, "api.T2C2", "listMySpaces", Seq(classOf[Int]),"GET", """""", Routes.prefix + """api/t2c2/listMySpaces"""))
   }
}
        

// @LINE:910
case api_Vocabularies_removeVocabulary616(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).removeVocabulary(id), HandlerDef(this, "api.Vocabularies", "removeVocabulary", Seq(classOf[UUID]),"DELETE", """""", Routes.prefix + """t2c2/templates/deleteTemplate/$id<[^/]+>"""))
   }
}
        

// @LINE:911
case api_T2C2_makeTemplatePublic617(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).makeTemplatePublic(id), HandlerDef(this, "api.T2C2", "makeTemplatePublic", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """t2c2/templates/makePublic/$id<[^/]+>"""))
   }
}
        

// @LINE:912
case api_T2C2_makeTemplatePrivate618(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).makeTemplatePrivate(id), HandlerDef(this, "api.T2C2", "makeTemplatePrivate", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """t2c2/templates/makePrivate/$id<[^/]+>"""))
   }
}
        

// @LINE:913
case api_Vocabularies_editVocabulary619(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Vocabularies]).editVocabulary(id), HandlerDef(this, "api.Vocabularies", "editVocabulary", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """t2c2/templates/editTemplate/$id<[^/]+>"""))
   }
}
        

// @LINE:914
case api_T2C2_attachVocabToDataset620(params) => {
   call(params.fromPath[UUID]("vocab_id", None), params.fromPath[UUID]("dataset_id", None)) { (vocab_id, dataset_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).attachVocabToDataset(vocab_id, dataset_id), HandlerDef(this, "api.T2C2", "attachVocabToDataset", Seq(classOf[UUID], classOf[UUID]),"PUT", """""", Routes.prefix + """t2c2/templates/$vocab_id<[^/]+>/attachToDataset/$dataset_id<[^/]+>"""))
   }
}
        

// @LINE:915
case api_T2C2_detachVocabFromDataset621(params) => {
   call(params.fromPath[UUID]("vocab_id", None), params.fromPath[UUID]("dataset_id", None)) { (vocab_id, dataset_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).detachVocabFromDataset(vocab_id, dataset_id), HandlerDef(this, "api.T2C2", "detachVocabFromDataset", Seq(classOf[UUID], classOf[UUID]),"PUT", """""", Routes.prefix + """t2c2/templates/$vocab_id<[^/]+>/detachFromDataset/$dataset_id<[^/]+>"""))
   }
}
        

// @LINE:916
case api_T2C2_attachVocabToFile622(params) => {
   call(params.fromPath[UUID]("vocab_id", None), params.fromPath[UUID]("file_id", None)) { (vocab_id, file_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).attachVocabToFile(vocab_id, file_id), HandlerDef(this, "api.T2C2", "attachVocabToFile", Seq(classOf[UUID], classOf[UUID]),"PUT", """""", Routes.prefix + """t2c2/templates/$vocab_id<[^/]+>/attachToFile/$file_id<[^/]+>"""))
   }
}
        

// @LINE:917
case api_T2C2_detachVocabFromFile623(params) => {
   call(params.fromPath[UUID]("vocab_id", None), params.fromPath[UUID]("file_id", None)) { (vocab_id, file_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).detachVocabFromFile(vocab_id, file_id), HandlerDef(this, "api.T2C2", "detachVocabFromFile", Seq(classOf[UUID], classOf[UUID]),"PUT", """""", Routes.prefix + """t2c2/templates/$vocab_id<[^/]+>/detachFromFile/$file_id<[^/]+>"""))
   }
}
        

// @LINE:919
case api_T2C2_listCollectionsCanEdit624(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[Integer]("limit", Some(0))) { (id, limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).listCollectionsCanEdit(id, limit), HandlerDef(this, "api.T2C2", "listCollectionsCanEdit", Seq(classOf[UUID], classOf[Integer]),"GET", """""", Routes.prefix + """api/t2c2/spaces/$id<[^/]+>/collectionsCanEdit"""))
   }
}
        

// @LINE:921
case controllers_T2C2_newTemplate625(params) => {
   call(params.fromQuery[Option[String]]("space", None)) { (space) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.T2C2]).newTemplate(space), HandlerDef(this, "controllers.T2C2", "newTemplate", Seq(classOf[Option[String]]),"GET", """""", Routes.prefix + """t2c2/templates/new"""))
   }
}
        

// @LINE:926
case api_Notebooks_submit626(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Notebooks]).submit(), HandlerDef(this, "api.Notebooks", "submit", Nil,"POST", """ ----------------------------------------------------------------------
 NOTEBOOKS ENDPOINT
 ----------------------------------------------------------------------""", Routes.prefix + """api/notebook/submit"""))
   }
}
        

// @LINE:934
case controllers_T2C2_uploadFile627(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.T2C2]).uploadFile(), HandlerDef(this, "controllers.T2C2", "uploadFile", Nil,"GET", """POST        /t2c2/postDragDrop                                                               @controllers.T2C2.postDragDrop
POST        /t2c2/uploadSingleFile                                                                            @controllers.Files.uploadSingleFile""", Routes.prefix + """t2c2/zipUpload"""))
   }
}
        

// @LINE:936
case api_T2C2_findYoungestChild628(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).findYoungestChild(), HandlerDef(this, "api.T2C2", "findYoungestChild", Nil,"POST", """GET         /t2c2/dragAndDrop                                                                           @controllers.T2C2.dragAndDrop()""", Routes.prefix + """t2c2/findYoungestChild"""))
   }
}
        

// @LINE:938
case api_T2C2_getAllCollectionsOfUser629(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsOfUser(), HandlerDef(this, "api.T2C2", "getAllCollectionsOfUser", Nil,"GET", """GET         /t2c2/dashboard                                                                             @controllers.T2C2.index()""", Routes.prefix + """t2c2/collections/allCollection"""))
   }
}
        

// @LINE:939
case api_T2C2_getAllCollectionsWithDatasetIdsAndFiles630(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsWithDatasetIdsAndFiles(), HandlerDef(this, "api.T2C2", "getAllCollectionsWithDatasetIdsAndFiles", Nil,"GET", """""", Routes.prefix + """t2c2/collections/allCollectionDatasetFiles"""))
   }
}
        

// @LINE:940
case api_T2C2_getAllCollectionsForTree631(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsForTree(), HandlerDef(this, "api.T2C2", "getAllCollectionsForTree", Nil,"GET", """""", Routes.prefix + """t2c2/collections/collectionsForTree"""))
   }
}
        

// @LINE:941
case api_T2C2_getLevelOfTree632(params) => {
   call(params.fromQuery[Option[String]]("currentId", Some(None)), params.fromQuery[String]("currentType", Some("collection"))) { (currentId, currentType) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getLevelOfTree(currentId, currentType), HandlerDef(this, "api.T2C2", "getLevelOfTree", Seq(classOf[Option[String]], classOf[String]),"GET", """""", Routes.prefix + """t2c2/collections/getLevelOfTree"""))
   }
}
        

// @LINE:942
case api_T2C2_getLevelOfTreeSharedWithMe633(params) => {
   call(params.fromQuery[Option[String]]("currentId", Some(None)), params.fromQuery[String]("currentType", Some("collection"))) { (currentId, currentType) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getLevelOfTreeSharedWithMe(currentId, currentType), HandlerDef(this, "api.T2C2", "getLevelOfTreeSharedWithMe", Seq(classOf[Option[String]], classOf[String]),"GET", """""", Routes.prefix + """t2c2/collections/getLevelOfTreeSharedWithMe"""))
   }
}
        

// @LINE:943
case api_T2C2_getLevelOfTreeSharedWithOthers634(params) => {
   call(params.fromQuery[Option[String]]("currentId", Some(None)), params.fromQuery[String]("currentType", Some("collection"))) { (currentId, currentType) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getLevelOfTreeSharedWithOthers(currentId, currentType), HandlerDef(this, "api.T2C2", "getLevelOfTreeSharedWithOthers", Seq(classOf[Option[String]], classOf[String]),"GET", """""", Routes.prefix + """t2c2/collections/getLevelOfTreeSharedWithOthers"""))
   }
}
        

// @LINE:944
case api_T2C2_getLevelOfTreeNotSharedInSpace635(params) => {
   call(params.fromQuery[Option[String]]("currentId", Some(None)), params.fromQuery[String]("currentType", Some("collection"))) { (currentId, currentType) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getLevelOfTreeNotSharedInSpace(currentId, currentType), HandlerDef(this, "api.T2C2", "getLevelOfTreeNotSharedInSpace", Seq(classOf[Option[String]], classOf[String]),"GET", """""", Routes.prefix + """t2c2/collections/getLevelOfTreeNotShared"""))
   }
}
        

// @LINE:945
case api_T2C2_getLevelOfTreeInSpace636(params) => {
   call(params.fromQuery[Option[String]]("currentId", Some(None)), params.fromQuery[String]("currentType", Some("collection")), params.fromQuery[String]("spaceId", None)) { (currentId, currentType, spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getLevelOfTreeInSpace(currentId, currentType, spaceId), HandlerDef(this, "api.T2C2", "getLevelOfTreeInSpace", Seq(classOf[Option[String]], classOf[String], classOf[String]),"GET", """""", Routes.prefix + """t2c2/collections/getLevelOfTreeInSpace"""))
   }
}
        

// @LINE:946
case api_T2C2_getAllCollectionsForFullTree637(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsForFullTree(), HandlerDef(this, "api.T2C2", "getAllCollectionsForFullTree", Nil,"GET", """""", Routes.prefix + """t2c2/collections/collectionsForFullTree"""))
   }
}
        

// @LINE:947
case api_T2C2_getAllCollectionsOfUserNotSharedInSpace638(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsOfUserNotSharedInSpace(), HandlerDef(this, "api.T2C2", "getAllCollectionsOfUserNotSharedInSpace", Nil,"GET", """""", Routes.prefix + """t2c2/collections/allCollectionsNotSharedInSpace"""))
   }
}
        

// @LINE:948
case api_T2C2_createEmptyDataset639(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).createEmptyDataset(), HandlerDef(this, "api.T2C2", "createEmptyDataset", Nil,"POST", """""", Routes.prefix + """t2c2/datasets/createEmpty"""))
   }
}
        

// @LINE:949
case api_T2C2_moveKeysToTermsTemplates640(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).moveKeysToTermsTemplates(), HandlerDef(this, "api.T2C2", "moveKeysToTermsTemplates", Nil,"PUT", """""", Routes.prefix + """t2c2/templates/moveKeysToTerms"""))
   }
}
        

// @LINE:950
case api_T2C2_bulkDelete641(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).bulkDelete(), HandlerDef(this, "api.T2C2", "bulkDelete", Nil,"DELETE", """""", Routes.prefix + """api/t2c2/bulkDelete"""))
   }
}
        

// @LINE:951
case api_T2C2_getWhoICanUploadFor642(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getWhoICanUploadFor(), HandlerDef(this, "api.T2C2", "getWhoICanUploadFor", Nil,"GET", """""", Routes.prefix + """api/t2c2/getUsersICanUploadFor"""))
   }
}
        

// @LINE:952
case api_T2C2_getSpacesOfUser643(params) => {
   call(params.fromPath[UUID]("userId", None)) { (userId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getSpacesOfUser(userId), HandlerDef(this, "api.T2C2", "getSpacesOfUser", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/t2c2/getSpacesOfUser/$userId<[^/]+>"""))
   }
}
        

// @LINE:953
case api_T2C2_getSpacesUserHasAccessTo644(params) => {
   call(params.fromPath[UUID]("userId", None)) { (userId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getSpacesUserHasAccessTo(userId), HandlerDef(this, "api.T2C2", "getSpacesUserHasAccessTo", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/t2c2/getSpacesUserCanEdit/$userId<[^/]+>"""))
   }
}
        

// @LINE:954
case api_T2C2_getUsersOfSpace645(params) => {
   call(params.fromPath[UUID]("spaceId", None)) { (spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getUsersOfSpace(spaceId), HandlerDef(this, "api.T2C2", "getUsersOfSpace", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/t2c2/getUsersOfSpace/$spaceId<[^/]+>"""))
   }
}
        

// @LINE:955
case api_T2C2_getNumUsersOfSpace646(params) => {
   call(params.fromPath[UUID]("spaceId", None)) { (spaceId) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getNumUsersOfSpace(spaceId), HandlerDef(this, "api.T2C2", "getNumUsersOfSpace", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/t2c2/getNumUsersOfSpace/$spaceId<[^/]+>"""))
   }
}
        

// @LINE:956
case api_Files_uploadToDatasetWithDescription647(params) => {
   call(params.fromPath[UUID]("id", None), params.fromQuery[String]("showPreviews", Some("DatasetLevel")), params.fromQuery[String]("originalZipFile", Some("")), params.fromQuery[String]("flags", Some(""))) { (id, showPreviews, originalZipFile, flags) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).uploadToDatasetWithDescription(id, showPreviews, originalZipFile, flags), HandlerDef(this, "api.Files", "uploadToDatasetWithDescription", Seq(classOf[UUID], classOf[String], classOf[String], classOf[String]),"POST", """""", Routes.prefix + """t2c2/uploadToDataset/$id<[^/]+>"""))
   }
}
        

// @LINE:957
case api_T2C2_getTemplateFromLastDataset648(params) => {
   call(params.fromQuery[Int]("limit", Some(10))) { (limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getTemplateFromLastDataset(limit), HandlerDef(this, "api.T2C2", "getTemplateFromLastDataset", Seq(classOf[Int]),"GET", """""", Routes.prefix + """t2c2/templates/lastTemplate"""))
   }
}
        

// @LINE:958
case api_T2C2_getDatasetWithAttachedVocab649(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getDatasetWithAttachedVocab(id), HandlerDef(this, "api.T2C2", "getDatasetWithAttachedVocab", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """t2c2/datasets/getDatasetAndTemplate/$id<[^/]+>"""))
   }
}
        

// @LINE:959
case api_Files_updateDescription650(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Files]).updateDescription(id), HandlerDef(this, "api.Files", "updateDescription", Seq(classOf[UUID]),"PUT", """""", Routes.prefix + """t2c2/files/$id<[^/]+>/updateDescription"""))
   }
}
        

// @LINE:960
case api_Datasets_listInCollection651(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Datasets]).listInCollection(coll_id), HandlerDef(this, "api.Datasets", "listInCollection", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """t2c2/collections/$coll_id<[^/]+>/datasets"""))
   }
}
        

// @LINE:961
case api_T2C2_getDatasetsInCollectionWithColId652(params) => {
   call(params.fromPath[UUID]("coll_id", None)) { (coll_id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getDatasetsInCollectionWithColId(coll_id), HandlerDef(this, "api.T2C2", "getDatasetsInCollectionWithColId", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """api/t2c2/collections/$coll_id<[^/]+>/datasetsWithParentColId"""))
   }
}
        

// @LINE:962
case api_T2C2_getAllCollectionsWithDatasetIds653(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getAllCollectionsWithDatasetIds(), HandlerDef(this, "api.T2C2", "getAllCollectionsWithDatasetIds", Nil,"GET", """""", Routes.prefix + """api/t2c2/allCollectionsWithDatasetIds"""))
   }
}
        

// @LINE:963
case api_T2C2_getKeysValuesFromLastDataset654(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getKeysValuesFromLastDataset(), HandlerDef(this, "api.T2C2", "getKeysValuesFromLastDataset", Nil,"GET", """""", Routes.prefix + """t2c2/getKeyValuesLastDataset"""))
   }
}
        

// @LINE:964
case api_T2C2_getKeysValuesFromLastDatasets655(params) => {
   call(params.fromQuery[Int]("limit", Some(10))) { (limit) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getKeysValuesFromLastDatasets(limit), HandlerDef(this, "api.T2C2", "getKeysValuesFromLastDatasets", Seq(classOf[Int]),"GET", """""", Routes.prefix + """t2c2/getKeyValuesForLastDatasets"""))
   }
}
        

// @LINE:965
case api_T2C2_getKeysValuesFromDatasetId656(params) => {
   call(params.fromPath[UUID]("id", None)) { (id) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getKeysValuesFromDatasetId(id), HandlerDef(this, "api.T2C2", "getKeysValuesFromDatasetId", Seq(classOf[UUID]),"GET", """""", Routes.prefix + """t2c2/getKeyValuesForDatasetId/$id<[^/]+>"""))
   }
}
        

// @LINE:966
case api_T2C2_getVocabIdNameFromTag657(params) => {
   call(params.fromPath[String]("tag", None)) { (tag) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.T2C2]).getVocabIdNameFromTag(tag), HandlerDef(this, "api.T2C2", "getVocabIdNameFromTag", Seq(classOf[String]),"GET", """""", Routes.prefix + """t2c2/getIdNameFromTag/$tag<[^/]+>"""))
   }
}
        

// @LINE:968
case controllers_T2C2_uploader658(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.T2C2]).uploader(), HandlerDef(this, "controllers.T2C2", "uploader", Nil,"GET", """GET         /t2c2/docs   																				@controllers.T2C2.getDocs()""", Routes.prefix + """t2c2/uploader"""))
   }
}
        

// @LINE:970
case controllers_T2C2_zipUploader659(params) => {
   call { 
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[controllers.T2C2]).zipUploader(), HandlerDef(this, "controllers.T2C2", "zipUploader", Nil,"GET", """""", Routes.prefix + """t2c2/zipUploader"""))
   }
}
        

// @LINE:977
case api_Tree_getChildrenOfNode660(params) => {
   call(params.fromQuery[Option[String]]("nodeId", None), params.fromQuery[String]("nodeType", None), params.fromQuery[Boolean]("mine", Some(true)), params.fromQuery[Boolean]("shared", Some(false)), params.fromQuery[Boolean]("public", Some(false))) { (nodeId, nodeType, mine, shared, public) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.Tree]).getChildrenOfNode(nodeId, nodeType, mine, shared, public), HandlerDef(this, "api.Tree", "getChildrenOfNode", Seq(classOf[Option[String]], classOf[String], classOf[Boolean], classOf[Boolean], classOf[Boolean]),"GET", """""", Routes.prefix + """api/tree/getChildrenOfNode"""))
   }
}
        

// @LINE:978
case api_FullTree_getChildrenOfNode661(params) => {
   call(params.fromQuery[Option[String]]("nodeId", None), params.fromQuery[String]("nodeType", None), params.fromQuery[Option[String]]("role", None)) { (nodeId, nodeType, role) =>
        invokeHandler(play.api.Play.maybeApplication.map(_.global).getOrElse(play.api.DefaultGlobal).getControllerInstance(classOf[api.FullTree]).getChildrenOfNode(nodeId, nodeType, role), HandlerDef(this, "api.FullTree", "getChildrenOfNode", Seq(classOf[Option[String]], classOf[String], classOf[Option[String]]),"GET", """""", Routes.prefix + """api/fulltree/getChildrenOfNode"""))
   }
}
        
}

}
     