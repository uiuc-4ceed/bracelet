BRACELET Curator 
=====

These are the instructions to run standalone BRACELET Curator. To setup the whole 4CeeD software suite, please use the [4CeeD Framework](https://gitlab.com/uiuc-4ceed/4CeeD) repository.

BRACELET Curator is based on [Clowder](https://opensource.ncsa.illinois.edu/bitbucket/projects/CATS/repos/clowder/browse)

## Prerequisites
- JDK (7 or later)
- Docker (to build Docker image, 1.9.1 or later)
- Clowder's dependencies (please refer to [Clowder's README](Clowder_README.md))

## Quickstart
```
docker-compose -f docker-compose.yml
```
## Build Curator Docker image

Build Docker image:
```
docker build -t [Your Org.]/bracelet .
```

Push newly built image to Docker Hub:
```
docker push [Your Org.]/bracelet
```

